/*
********************************************************************************

Software License Agreement

Copyright © 2015 Microchip Technology Inc. and its licensors.  All
rights reserved.

Microchip licenses to you the right to: (1) install Software on a single
computer and use the Software with Microchip microcontrollers and
digital signal controllers ("Microchip Product"); and (2) at your
own discretion and risk, use, modify, copy and distribute the device
driver files of the Software that are provided to you in Source Code;
provided that such Device Drivers are only used with Microchip Products
and that no open source or free software is incorporated into the Device
Drivers without Microchip's prior written consent in each instance.

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY
WARRANTY OF MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A
PARTICULAR PURPOSE. IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE
LIABLE OR OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY,
CONTRIBUTION, BREACH OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY ANY
DIRECT OR INDIRECT DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO ANY
INCIDENTAL, SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR
LOST DATA, COST OF PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY,
SERVICES, ANY CLAIMS BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY
DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

********************************************************************************
*/
//
//  ConnectionLogTableViewController.swift
//  mchp_ble_discover
//
//  Created by JM on 3/4/15.
//  Copyright (c) 2015 JM. All rights reserved.
//

import UIKit
import CoreData

class BLEConnectionLogTableViewController: UITableViewController {

    var appResults: [AnyObject] = [AnyObject]()
    fileprivate var logoView: UIView?
    fileprivate var appDelegate: AppDelegate?
    fileprivate var appContext: NSManagedObjectContext?
    fileprivate var appRequest: NSFetchRequest<NSFetchRequestResult>?
    
    @IBOutlet weak var clearButtonOutlet: UIBarButtonItem!
    @IBAction func cancelButton(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clearbutton(_ sender: AnyObject) {
        clearButtonOutlet.isEnabled = false
        for object in appResults {
            appContext?.delete(object as! NSManagedObject)
        }
        appResults.removeAll(keepingCapacity: false)
        
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        clearButtonOutlet.isEnabled = false
        
        self.tableView.backgroundView = UIImageView(image: UIImage(named: "BackgroundImage.png"))
        self.tableView.backgroundView?.alpha = 0.4
        
        logoView = UIImageView(image: UIImage(named: "microchip_logo.png"))
        logoView!.frame = CGRect(x: self.view.frame.midX-(266/2), y: self.view.frame.midY-(128/2), width: 266, height: 128)
        logoView!.alpha = 0.2
        self.tableView.backgroundView?.addSubview(logoView!)
        self.tableView.backgroundView?.bringSubview(toFront: logoView!)
        
        self.navigationController?.navigationBar.backgroundColor = UIColor.blue
        
        self.tableView.tableFooterView = UIView()
        
        //self.navigationController?.navigationBar.backgroundColor = UIColor.blueColor()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        appDelegate = (UIApplication.shared.delegate as! AppDelegate)
        appContext = appDelegate!.managedObjectContext!
        appRequest = NSFetchRequest(entityName: "BLEPeripherals")
        
        appRequest!.returnsObjectsAsFaults = false
        appResults = try! appContext!.fetch(appRequest!)
        
    }

    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        logoView!.removeFromSuperview()
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        logoView = UIImageView(image: UIImage(named: "microchip_logo.png"))
        logoView!.frame = CGRect(x: self.view.frame.midX-(266/2), y: self.view.frame.midY-(128/2), width: 266, height: 128)
        logoView!.alpha = 0.2
        self.tableView.backgroundView?.addSubview(logoView!)
        self.tableView.backgroundView?.bringSubview(toFront: logoView!)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        if (appResults.count > 0) {
            clearButtonOutlet.isEnabled = true
        }
        
        return appResults.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "mCell", for: indexPath) 
        
        if (appResults.count > indexPath.row) {
            cell.backgroundColor = UIColor.white.withAlphaComponent(0.6)
            cell.textLabel?.adjustsFontSizeToFitWidth = true
            cell.detailTextLabel?.adjustsFontSizeToFitWidth = true
            
            // Configure the cell...
            cell.textLabel?.text = (appResults[appResults.count - 1 - indexPath.row] as! NSManagedObject).value(forKey: "name") as? String
            let timeStamp = (appResults[appResults.count - 1 - indexPath.row] as! NSManagedObject).value(forKey: "timeStamp") as? Date
            cell.detailTextLabel?.text = "\(timeStamp!)"
        }

        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
