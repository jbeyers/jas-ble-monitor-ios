/*
********************************************************************************

Software License Agreement

Copyright © 2015 Microchip Technology Inc. and its licensors.  All
rights reserved.

Microchip licenses to you the right to: (1) install Software on a single
computer and use the Software with Microchip microcontrollers and
digital signal controllers ("Microchip Product"); and (2) at your
own discretion and risk, use, modify, copy and distribute the device
driver files of the Software that are provided to you in Source Code;
provided that such Device Drivers are only used with Microchip Products
and that no open source or free software is incorporated into the Device
Drivers without Microchip's prior written consent in each instance.

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY
WARRANTY OF MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A
PARTICULAR PURPOSE. IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE
LIABLE OR OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY,
CONTRIBUTION, BREACH OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY ANY
DIRECT OR INDIRECT DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO ANY
INCIDENTAL, SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR
LOST DATA, COST OF PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY,
SERVICES, ANY CLAIMS BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY
DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

********************************************************************************
*/
//
//  ConnectionTableViewController.swift
//  mchp_ble_discover
//
//  Created by JM on 2/24/15.
//  Copyright (c) 2015 JM. All rights reserved.
//

import UIKit
import Foundation
import CoreBluetooth

extension Double {
    func format(_ f: String) -> String {
        return NSString(format: "%\(f)f" as NSString, self) as String
    }
}

class BLEConnectionTableViewController: UITableViewController, CBCentralManagerDelegate, CBPeripheralDelegate {

    var cbCentralManager: CBCentralManager!
    var peripheralInstance: CBPeripheral!
    var peripheralName: String?

    fileprivate var localNotification = UILocalNotification()
    fileprivate var selectedIndexPath: IndexPath?
    fileprivate var logoView: UIView?
    fileprivate var knob: Knob!
    fileprivate var ledCharacteristic: CBCharacteristic?
    fileprivate var batteryVolt: Double = 0
    
    fileprivate let mchpSensorServiceUUID = CBUUID(string: "ad11cf40-063f-11e5-be3e-0002a5d5c51b")
    fileprivate let mchpLightCharacteristicUUID = CBUUID(string: "bf3fbd80-063f-11e5-9e69-0002a5d5c501")
    fileprivate let mchpPotCharacteristicUUID = CBUUID(string: "bf3fbd80-063f-11e5-9e69-0002a5d5c502")
    fileprivate let mchpSwitchCharacteristicUUID = CBUUID(string: "bf3fbd80-063f-11e5-9e69-0002a5d5c503")
    fileprivate let mchpLEDCharacteristicUUID = CBUUID(string: "bf3fbd80-063f-11e5-9e69-0002a5d5c503")
    fileprivate let mchpTemperatureCharacteristicUUID = CBUUID(string: "bf3fbd80-063f-11e5-9e69-0002a5d5c504")
    fileprivate let mchpBatteryCharacteristicUUID = CBUUID(string: "bf3fbd80-063f-11e5-9e69-0002a5d5c505")
    
    @IBOutlet weak var switchImage: UIImageView!
    @IBOutlet weak var lightLabel: UILabel!
    @IBOutlet weak var potlabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var ledSliderOutlet: UISlider!
    @IBOutlet weak var knobView: UIView!
    @IBOutlet weak var lightsensorView: LightSensorView!
    @IBOutlet weak var batteryView: BatteryView!
    @IBOutlet weak var batteryLabel: UILabel!
    @IBOutlet weak var tempView: UIProgressView!
    
    @IBAction func ledSlider(_ sender: AnyObject) {
        var bytesData = [UInt8] ()
        var sliderValue: UInt16 = 0x7000 - UInt16(Float(0x7000) * ledSliderOutlet.value)
        
        if sliderValue < 500 {
            sliderValue = 500
        }
        
        if sliderValue == 0x7000 {
            sliderValue = 0
        }
        
        let sliderString = String(format:"%X", sliderValue)
        var stringLength = 4 - sliderString.characters.count
        for _ in 0..<stringLength {
            bytesData.append(48)
        }
        for codeUnit in sliderString.utf8 {
            bytesData.append(codeUnit)
        }
        print (sliderString)
        let sliderString2 = String(format:"%X", (sliderValue/2))
        stringLength = 4 - sliderString2.characters.count
        bytesData.append(44)
        for _ in 0..<stringLength {
            bytesData.append(48)
        }
        for codeUnit1 in sliderString2.utf8 {
            bytesData.append(codeUnit1)
        }
        print(sliderString2)
        //let writeData = Data(bytes: UnsafePointer<UInt8>(&bytesData), count: bytesData.count)
        let writeData = Data(bytes: &bytesData, count: bytesData.count)
        if (ledCharacteristic != nil) {
            if ((ledCharacteristic!.properties.rawValue & CBCharacteristicProperties.write.rawValue) != 0) {
                peripheralInstance!.writeValue(writeData, for: ledCharacteristic!, type: CBCharacteristicWriteType.withResponse)
            }
            else {
                peripheralInstance!.writeValue(writeData, for: ledCharacteristic!, type: CBCharacteristicWriteType.withoutResponse)
            }
        }
        
    }
    
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {

    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        
        localNotification.alertBody = "\(peripheral.name ?? "Device") : Connected"
        localNotification.fireDate = Date(timeIntervalSinceNow: 1)
        UIApplication.shared.scheduleLocalNotification(localNotification)
        peripheral.delegate = self
        peripheral.discoverServices([mchpSensorServiceUUID])
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        localNotification.alertBody = "\(peripheral.name ?? "Device") : Disconnected"
        localNotification.fireDate = Date(timeIntervalSinceNow: 1)
        UIApplication.shared.scheduleLocalNotification(localNotification)
        
        if let navigationController = self.navigationController {
            navigationController.popViewController(animated: true)
        }
    }
    
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        let alertController = UIAlertController(title: "BLE Sensor App", message: "Failed to connect to the peripheral! Check if the peripheral is functioning properly and try to reconnect.", preferredStyle: UIAlertControllerStyle.alert)
        let alertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) -> Void in
            if let navigationController = self.navigationController {
                navigationController.popViewController(animated: true)
            }
        })
        alertController.addAction(alertAction)
        alertController.popoverPresentationController?.sourceView = self.view
        alertController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 3.0, y: self.view.bounds.size.height / 3.0, width: 1.0, height: 1.0)
        present(alertController, animated: true, completion: nil)
        cbCentralManager.cancelPeripheralConnection(peripheral)
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        if (peripheral.services!.isEmpty) {
            let alertController = UIAlertController(title: "BLE Sensor App", message: "Failed to discover BLE services and characteristics on the peripheral.", preferredStyle: UIAlertControllerStyle.alert)
            let alertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                if let navigationController = self.navigationController {
                    navigationController.popViewController(animated: true)
                }
            })
            alertController.addAction(alertAction)
            alertController.popoverPresentationController?.sourceView = self.view
            alertController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 3.0, y: self.view.bounds.size.height / 3.0, width: 1.0, height: 1.0)
            present(alertController, animated: true, completion: nil)
            cbCentralManager.cancelPeripheralConnection(peripheral)
            return
        }
        
        for service in peripheral.services! {
            peripheral.discoverCharacteristics(nil, for: service )
        }
    
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        for characteristic in service.characteristics! {
                peripheral.setNotifyValue(true, for: characteristic )
                if ((characteristic ).uuid == mchpLEDCharacteristicUUID) {
                    ledCharacteristic = characteristic
                }

            
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateNotificationStateFor characteristic: CBCharacteristic, error: Error?) {
    }
    
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        let sensorValueString = (("\(characteristic.value!)").replacingOccurrences(of: "<", with: "", options: NSString.CompareOptions.literal, range: nil)).replacingOccurrences(of: ">", with: "", options: NSString.CompareOptions.literal, range: nil)
        
        switch characteristic.uuid {
        case mchpLightCharacteristicUUID:
            var sensorValue: Double = 0
            var bytesData = [UInt8] (repeating: 0, count: characteristic.value!.count)
            (characteristic.value! as NSData).getBytes(&bytesData, length: characteristic.value!.count)
            (characteristic.value! as NSData).getBytes(&sensorValue, length: MemoryLayout<UInt16>.size)
            sensorValue = Double((UInt16(bytesData[0]) << 8) + UInt16(bytesData[1])) / 1000

            var floatValue: Float = 0.0
            if (batteryVolt != 0.0) {
                floatValue = ( Float( sensorValue ) / Float( batteryVolt ))
            } else {
                floatValue = ( Float( sensorValue ) / 3.3 )
            }
            
            lightsensorView.fillColor = UIColor(white: CGFloat(floatValue), alpha: 1)
            lightsensorView.setNeedsDisplay()
            var lux: Double = (Double((1/(1 - floatValue)) - 1) * 500 / 12)
            if (lux > 10000) {
                lux = 10000
            }
            if (lux < 1000) {
                lightLabel.text = lux.format(".0") + "lux"
            } else {
                lightLabel.text = (lux/1000).format(".0") + "Klux"
            }
            break
            
        case mchpPotCharacteristicUUID:
            var sensorValue: Double = 0
            var bytesData = [UInt8] (repeating: 0, count: characteristic.value!.count)
            (characteristic.value! as NSData).getBytes(&bytesData, length: characteristic.value!.count)
            (characteristic.value! as NSData).getBytes(&sensorValue, length: MemoryLayout<UInt16>.size)
            sensorValue = Double((UInt16(bytesData[0]) << 8) + UInt16(bytesData[1])) / 1000

            if (batteryVolt != 0.0) {
                knob.value = ( Float( sensorValue ) / Float( batteryVolt ))
            } else {
                knob.value = ( Float( sensorValue ) / 3.3 )
            }
            
            
            potlabel.text = (sensorValue).format(".2") + "V"
            break
            
        case mchpSwitchCharacteristicUUID:
            if sensorValueString == "0001" {
                switchImage.image = UIImage(named: "BulbOnIcon")
            } else {
                switchImage.image = UIImage(named: "BulbOffIcon")
            }
            break
            
        case mchpTemperatureCharacteristicUUID:
            var tempValue: UInt16 = 0
            var bytesData = [UInt8] (repeating: 0, count: characteristic.value!.count)
            (characteristic.value! as NSData).getBytes(&bytesData, length: characteristic.value!.count)
            (characteristic.value! as NSData).getBytes(&tempValue, length: MemoryLayout<UInt16>.size)
            let tempVal = Double((UInt16(bytesData[0]) << 8) + UInt16(bytesData[1])) / 1000 * 4096
            
            if (batteryVolt != 0.0) {
                tempValue = UInt16(tempVal / batteryVolt)
            } else {
                tempValue = UInt16(tempVal / 3.4)
            }
            
            

            if (tempValue < 1154 || tempValue > 2613) {
                tempView.progress = 0
                temperatureLabel.text = "..."
            }
            else {
                var temp = (((Double(tempValue) / 1459 * 90) - 91.2) * 9 / 5) + 32 - 10
                if temp.isNaN {
                    temp = 0
                }
                tempView.progress = Float(temp) / 150.0
                temperatureLabel.text = (Double(temp)).format(".2") + "F"
            }
            
            break

        case mchpBatteryCharacteristicUUID:
            var batteryValue: Double = 0
            var bytesData = [UInt8] (repeating: 0, count: characteristic.value!.count)
            (characteristic.value! as NSData).getBytes(&bytesData, length: characteristic.value!.count)
            (characteristic.value! as NSData).getBytes(&batteryValue, length: MemoryLayout<UInt16>.size)
            batteryValue = Double((UInt16(bytesData[0]) << 8) + UInt16(bytesData[1]))/1000

            batteryVolt = batteryValue
            batteryView.percent = UInt8(( (Float( batteryValue ) - 1.9) / (3.3-1.9) ) * 100)
            if batteryView.percent > 100 {
                batteryView.percent = 100
            }
            
            batteryLabel.text = "\(batteryView.percent)%"
            batteryView.setNeedsDisplay()
            break
            
        default:
            break
            
        }
        
    }    
    
    func charPairToByte(_ strIn:String) -> UInt8
    {
        var byte:UInt8 = 0
        for c in strIn.characters
        {
            var number:UInt8 = 0
            byte = byte << 4
            switch(c)
            {
            case "0":
                number = 0
            case "1":
                number = 1
            case "2":
                number = 2
            case "3":
                number = 3
            case "4":
                number = 4
            case "5":
                number = 5
            case "6":
                number = 6
            case "7":
                number = 7
            case "8":
                number = 8
            case "9":
                number = 9
            case "A":
                number = 10
            case "B":
                number = 11
            case "C":
                number = 12
            case "D":
                number = 13
            case "E":
                number = 14
            case "F":
                number = 15
            default:
                print("bad char \(c)")
            }
            byte = byte | number
        }
        
        return byte
    }
    
    func hexStringToByteArray(_ stringIn: String) -> Array<UInt8>
    {
        var hexString = ""
        for character in stringIn.characters
        {
            print("\(character)")
            if (character != " ")
            {
                hexString.append(character)
            }
        }
        
        hexString = hexString.uppercased()
        var bytes = [UInt8]()
        var stringLength = hexString.characters.count
        print("\(stringLength)")
        if (stringLength % 2 != 0)
        {
            stringLength -= 1;
        }
        
        for character in hexString.characters {
            let byte:UInt8 = charPairToByte(String(character))
            print("\(byte)")
            bytes.append(byte)
        }
        
        /*
        for var i:Int = 0; i < stringLength; i += 2
        {
            //let sub = hexString.substringWithRange(Range<String.Index>(start: advance(hexString.startIndex, i), end: advance(hexString.startIndex, i+2))) //[i..<(i+2)]
            let sub = hexString.substringWithRange(Range<String.Index>(start: hexString.startIndex.advancedBy(i), end: hexString.startIndex.advancedBy(i+2))) //[i..<(i+2)]
            let byte:UInt8 = charPairToByte(sub)
            print("\(sub)")
            bytes.append(byte)
        }
        */
        
        
        return bytes
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.backgroundView = UIImageView(image: UIImage(named: "BackgroundImage.png"))
        self.tableView.backgroundView?.alpha = 0.4
        
        logoView = UIImageView(image: UIImage(named: "microchip_logo.png"))
        logoView!.frame = CGRect(x: self.view.frame.midX-(266/2), y: self.view.frame.midY-(128/2), width: 266, height: 128)
        logoView!.alpha = 0.2
        self.tableView.backgroundView?.addSubview(logoView!)
        self.tableView.backgroundView?.bringSubview(toFront: logoView!)
        self.navigationController?.navigationBar.backgroundColor = UIColor.blue

        knob = Knob(frame: knobView.bounds)
        knob.addTarget(self, action: #selector(BLEConnectionTableViewController.knobValueChanged(_:)), for: .valueChanged)
        knobView.addSubview(knob)
        
        view.tintColor = UIColor.red
        
        knob.value = 0
        tempView.progress = 0
        
        peripheralInstance.delegate = self
        cbCentralManager.delegate = self
        cbCentralManager.connect(peripheralInstance!, options: nil)
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    
    func knobValueChanged(_ knob: Knob) {

    }
    
    override func viewWillAppear(_ animated: Bool) {
        peripheralInstance.delegate = self
        
        if (peripheralInstance.state == CBPeripheralState.connected) {
            tableView.reloadData()
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if (self.isMovingFromParentViewController) {
            if (peripheralInstance.state == CBPeripheralState.connected) {
                cbCentralManager.cancelPeripheralConnection(peripheralInstance!)
            }
        }
    }
    
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        logoView!.removeFromSuperview()
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        logoView = UIImageView(image: UIImage(named: "microchip_logo.png"))
        logoView!.frame = CGRect(x: self.view.frame.midX-(266/2), y: self.view.frame.midY-(128/2), width: 266, height: 128)
        logoView!.alpha = 0.2
        self.tableView.backgroundView?.addSubview(logoView!)
        self.tableView.backgroundView?.bringSubview(toFront: logoView!)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.white.withAlphaComponent(0.6)
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        //notifyTitle.title = nil
        
        // Get the new view controller using [segue destinationViewController].
        var thirdScreen = segue.destinationViewController as! CharacteristicTableViewController
        
        // Pass the selected object to the new view controller.
        if let indexPath = self.tableView.indexPathForSelectedRow() {
            thirdScreen.delegate = self
            selectedIndexPath = indexPath
            thirdScreen.peripheralInstance = peripheralInstance
            thirdScreen.characteristicInstance = peripheralAttributes[indexPath.section].characteristics[indexPath.row]
        }
        
    }
    */
    

}
