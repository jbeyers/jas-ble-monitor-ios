/*
********************************************************************************

Software License Agreement

Copyright © 2015 Microchip Technology Inc. and its licensors.  All
rights reserved.

Microchip licenses to you the right to: (1) install Software on a single
computer and use the Software with Microchip microcontrollers and
digital signal controllers ("Microchip Product"); and (2) at your
own discretion and risk, use, modify, copy and distribute the device
driver files of the Software that are provided to you in Source Code;
provided that such Device Drivers are only used with Microchip Products
and that no open source or free software is incorporated into the Device
Drivers without Microchip's prior written consent in each instance.

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY
WARRANTY OF MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A
PARTICULAR PURPOSE. IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE
LIABLE OR OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY,
CONTRIBUTION, BREACH OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY ANY
DIRECT OR INDIRECT DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO ANY
INCIDENTAL, SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR
LOST DATA, COST OF PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY,
SERVICES, ANY CLAIMS BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY
DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

********************************************************************************
*/
//
//  PeripheralsTableViewController.swift
//  mchp_ble_discover
//
//  Created by JM on 2/24/15.
//  Copyright (c) 2015 JM. All rights reserved.
//

import UIKit
import CoreData
import Foundation
import CoreLocation
import CoreBluetooth

class BLEPeripheralsTableViewController: UITableViewController, UIPopoverPresentationControllerDelegate, CBCentralManagerDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var peripheralsIndicator: UIActivityIndicatorView!
    @IBOutlet weak var bannerView: UIImageView!
    
    fileprivate let mchpSensorServiceUUID = CBUUID(string: "ad11cf40-063f-11e5-be3e-0002a5d5c51b")
    fileprivate var cbCentralManager: CBCentralManager!
    fileprivate var peripheralInstance: CBPeripheral?
    fileprivate var peripheralDict = [String: BLEPeripheralsStructure]()
    fileprivate var previousPeripheralRSSIValue: Int = 0
    fileprivate var rssiTime: Date = Date()
    fileprivate var localTimer: Timer = Timer()
    fileprivate var localTimer2: Timer = Timer()
    fileprivate var logoView: UIView?
    fileprivate var bluetoothEnabled: Bool = false
    fileprivate var alertControllerBluetooth: UIAlertController?
    fileprivate var firstIndexPath: IndexPath?
    fileprivate var localNotification = UILocalNotification()
    fileprivate var beaconId: String?
    
    fileprivate let locationManager = CLLocationManager()
    fileprivate let beaconRegion = CLBeaconRegion(proximityUUID: UUID(uuidString: "ad11cf40-063f-11e5-be3e-0002a5d5c51b")!, identifier: "MCHP")
    
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return false
    }
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        
        if #available(iOS 10.0, *) {
            switch (central.state) {
            case CBManagerState.poweredOff:
                bluetoothEnabled = false
                peripheralDict.removeAll()
                tableView.reloadData()
                peripheralsIndicator.startAnimating()
                
                alertControllerBluetooth = UIAlertController(title: "BLE Sensor App", message: "Bluetooth is turned off.\nTurn Bluetooth on to use the app.", preferredStyle: UIAlertControllerStyle.actionSheet)
                alertControllerBluetooth!.popoverPresentationController?.sourceView = self.view
                alertControllerBluetooth!.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 2.0, y: self.view.bounds.size.height / 1.0, width: 1.0, height: 1.0)
                alertControllerBluetooth!.popoverPresentationController?.delegate = self
                present(alertControllerBluetooth!, animated: true, completion: nil)
                break
                
            case CBManagerState.poweredOn:
                bluetoothEnabled = true
                alertControllerBluetooth?.dismiss(animated: true, completion: nil)
                
                cbCentralManager.scanForPeripherals(withServices: [], options: [CBCentralManagerScanOptionAllowDuplicatesKey: true])
                break
                
            case CBManagerState.resetting:
                break
                
            case CBManagerState.unauthorized:
                break
                
            case CBManagerState.unknown:
                break
                
            case CBManagerState.unsupported:
                break
            }
        }
        else {
            switch central.state.rawValue{
            case 4:
                bluetoothEnabled = false
                peripheralDict.removeAll()
                tableView.reloadData()
                peripheralsIndicator.startAnimating()
                
                alertControllerBluetooth = UIAlertController(title: "BLE Sensor App", message: "Bluetooth is turned off.\nTurn Bluetooth on to use the app.", preferredStyle: UIAlertControllerStyle.actionSheet)
                alertControllerBluetooth!.popoverPresentationController?.sourceView = self.view
                alertControllerBluetooth!.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 2.0, y: self.view.bounds.size.height / 1.0, width: 1.0, height: 1.0)
                alertControllerBluetooth!.popoverPresentationController?.delegate = self
                present(alertControllerBluetooth!, animated: true, completion: nil)
                break
                
            case 5:
                bluetoothEnabled = true
                alertControllerBluetooth?.dismiss(animated: true, completion: nil)
                
                cbCentralManager.scanForPeripherals(withServices: [], options: [CBCentralManagerScanOptionAllowDuplicatesKey: true])
                break
                
            case 1:
                break
                
            case 3:
                break
                
            case 0:
                break
                
            case 2:
                break
            default:
                break
            }
            
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        //self.peripheralInstance = peripheral
        let peripheralConnectable: AnyObject = advertisementData["kCBAdvDataIsConnectable"]! as AnyObject
        if ((peripheral.state == CBPeripheralState.disconnected) && (peripheralConnectable as! NSNumber == 1)) {
            if (advertisementData.index(forKey: "kCBAdvDataManufacturerData") != nil) {
                var msd = String(describing: advertisementData["kCBAdvDataManufacturerData"]!).uppercased().replacingOccurrences(of: " ", with: "")
                //print(msd)
                let hdrId = msd.substring(with: (msd.characters.index(msd.startIndex, offsetBy: 1) ..< msd.characters.index(msd.startIndex, offsetBy: 9)))
                //print(hdrId)
                if ((msd.range(of: mchpSensorServiceUUID.uuidString.replacingOccurrences(of: "-", with: "")) != nil) && (hdrId == "CD00FE14")){
                    var peripheralName: String = String()
                    if (advertisementData.index(forKey: "kCBAdvDataLocalName") != nil) {
                        peripheralName = advertisementData["kCBAdvDataLocalName"] as! String
                    }
                    if (peripheralName == "" || peripheralName.isEmpty) {
                        
                        if (peripheral.name == nil || peripheral.name!.isEmpty) {
                            peripheralName = "Unknown"
                        } else {
                            peripheralName = peripheral.name!
                        }
                    }
                    msd = msd.replacingCharacters(in: (msd.characters.index(msd.startIndex, offsetBy: 1) ..< msd.characters.index(msd.startIndex, offsetBy: 9)), with: "")
                    //peripheralDict.updateValue(PeripheralsStructure(peripheralInstance: peripheral, peripheralMSD:msd, peripheralRSSI: RSSI, timeStamp: Date()), forKey: peripheralName)
                    peripheralDict.updateValue(BLEPeripheralsStructure(BLEperipheralInstance: peripheral, BLEperipheralMSD:msd, BLEperipheralRSSI: RSSI, BLEtimeStamp: Date()), forKey: peripheralName)
                    peripheralsIndicator.stopAnimating()
                    tableView.reloadData()
                }
            }
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {
        let discoveredBeacons = beacons.filter{ $0.proximity != CLProximity.unknown }
        if (discoveredBeacons.count > 0) {
            let nearestBeacon = discoveredBeacons[0] as CLBeacon
            
            //if nearestBeacon.proximity == CLProximity.Immediate {
                beaconId = ("<" + nearestBeacon.proximityUUID.uuidString.replacingOccurrences(of: "-", with: "") + String(format: "%4X", nearestBeacon.major.uint16Value)  + String(format: "%4X", nearestBeacon.minor.uint16Value) + ">").replacingOccurrences(of: " ", with: "0")
                //print(beaconId!)
                
                /*
                if beaconId != nil {
                    for keyDict in Array(peripheralDict.keys) {
                        
                        if peripheralDict[keyDict]?.peripheralMSD == beaconId! {
                            //localTimer2.invalidate()

                            self.peripheralInstance = peripheralDict[keyDict]!.peripheralInstance
                            performSegueWithIdentifier("ConnectionTableViewController", sender: self)
                        }
                    }
                    
                }
                */
            //}
            
            
        }
    }
    
    /*
    func locationManager(manager: CLLocationManager, didEnterRegion region: CLRegion) {
        localNotification.alertBody = "iBeacon found"
        localNotification.fireDate = NSDate(timeIntervalSinceNow: 1)
        UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
    }
    */
    /*
    func locationManager(manager: CLLocationManager, didExitRegion region: CLRegion) {
        localNotification.alertBody = "BLE Sensor App: iBeacon lost"
        localNotification.fireDate = NSDate(timeIntervalSinceNow: 0)
        UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
    }
    */
    
    func interruptLocalTimer2() {
        if beaconId != nil {
            for keyDict in Array(peripheralDict.keys) {
                if peripheralDict[keyDict]?.BLEperipheralMSD == beaconId! {
                    localTimer2.invalidate()
                    self.peripheralInstance = peripheralDict[keyDict]!.BLEperipheralInstance
                    performSegue(withIdentifier: "ConnectionTableViewController", sender: self)
                }
            }
            
        }
        /*else {
            localTimer2 = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: Selector("interruptLocalTimer2"), userInfo: nil, repeats: false)
        }*/
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.backgroundView = UIImageView(image: UIImage(named: "BackgroundImage.png"))
        self.tableView.backgroundView?.alpha = 0.4
        
        logoView = UIImageView(image: UIImage(named: "microchip_logo.png"))
        logoView!.frame = CGRect(x: self.view.frame.midX-(266/2), y: self.view.frame.midY-(128/2), width: 266, height: 128)
        logoView!.alpha = 0.2
        self.tableView.backgroundView?.addSubview(logoView!)
        self.tableView.backgroundView?.bringSubview(toFront: logoView!)
        
        self.navigationController?.navigationBar.backgroundColor = UIColor.blue
        
        
        let registerNotigication = UIApplication.instancesRespond(to: #selector(UIApplication.registerUserNotificationSettings(_:)))
        if registerNotigication {
            let types: UIUserNotificationType = [UIUserNotificationType.alert, UIUserNotificationType.sound]
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: types, categories: nil))
        }
        
        
        
        locationManager.delegate = self
        
        if (CLLocationManager.authorizationStatus() != CLAuthorizationStatus.authorizedAlways) {
            locationManager.requestAlwaysAuthorization()
        }
        locationManager.pausesLocationUpdatesAutomatically = false
        locationManager.startMonitoring(for: beaconRegion)
        locationManager.startRangingBeacons(in: beaconRegion)
        locationManager.startUpdatingLocation()
        
        //locationManager.delegate = self
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        let leftBarButtonItem = UIBarButtonItem(customView: leftMenuView())
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
        
    }
    
    func homeButtonClicked(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func leftMenuView() -> UIView{
        
        let LEFT_MENU_FRAME     = CGRect(x: 0, y: 0, width: 60, height: 40)
        let HOME_BUTTON_FRAME   = CGRect(x: 0, y: 0, width: 60, height: 40)
        
        // Home button
        let homeButton = UIButton(frame: HOME_BUTTON_FRAME)
        homeButton.addTarget(self, action: #selector(self.homeButtonClicked), for: UIControlEvents.touchUpInside)
        homeButton.setTitle("Home", for: UIControlState())
        homeButton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 15)
        homeButton.setTitleColor(UIColor.white, for: UIControlState())
        
        let leftMenuView = UIView(frame: LEFT_MENU_FRAME)
        leftMenuView.addSubview(homeButton)
        
        return leftMenuView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        peripheralDict.removeAll()
        tableView.reloadData()
        peripheralsIndicator.startAnimating()
        cbCentralManager = CBCentralManager(delegate: self, queue: nil)
        if (peripheralInstance != nil) {
            cbCentralManager.cancelPeripheralConnection(peripheralInstance!)
        }
        
        let bannerTouch = UITapGestureRecognizer(target: self, action: #selector(BLEPeripheralsTableViewController.bannerTouchEvent))
        bannerTouch.numberOfTapsRequired = 1
        bannerTouch.numberOfTouchesRequired = 1
        bannerView.addGestureRecognizer(bannerTouch)
        
        localTimer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(BLEPeripheralsTableViewController.interruptLocalTimer), userInfo: nil, repeats: true)
        localTimer2 = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(BLEPeripheralsTableViewController.interruptLocalTimer2), userInfo: nil, repeats: true)
    }
    
    func bannerTouchEvent() {
        if (UIApplication.shared.canOpenURL(URL(string: "http://www.microchip.com/bluetooth")!)) {
            UIApplication.shared.openURL(URL(string: "http://www.microchip.com/bluetooth")!)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        beaconId = ""
        localTimer.invalidate()
    }
    
    
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        logoView!.removeFromSuperview()
        
        if !bluetoothEnabled {
            alertControllerBluetooth?.dismiss(animated: true, completion: nil)
        }
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        logoView = UIImageView(image: UIImage(named: "microchip_logo.png"))
        logoView!.frame = CGRect(x: self.view.frame.midX-(266/2), y: self.view.frame.midY-(128/2), width: 266, height: 128)
        logoView!.alpha = 0.2
        self.tableView.backgroundView?.addSubview(logoView!)
        self.tableView.backgroundView?.bringSubview(toFront: logoView!)
        
        if !bluetoothEnabled {
            alertControllerBluetooth = UIAlertController(title: "BLE Sensor App", message: "Bluetooth is turned off.\nTurn Bluetooth on to use the app.", preferredStyle: UIAlertControllerStyle.actionSheet)
            alertControllerBluetooth!.popoverPresentationController?.sourceView = self.view
            alertControllerBluetooth!.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 2.0, y: self.view.bounds.size.height / 1.0, width: 1.0, height: 1.0)
            alertControllerBluetooth!.popoverPresentationController?.delegate = self
            present(alertControllerBluetooth!, animated: true, completion: nil)
        }
    }
    
    func interruptLocalTimer() {
        for keyDict in Array(peripheralDict.keys) {
            if ((peripheralDict[keyDict]!.BLEtimeStamp!).timeIntervalSinceNow < -15.0) {
                peripheralDict.removeValue(forKey: keyDict)
                if (peripheralDict.count == 0) {
                    tableView.reloadData()
                    peripheralsIndicator.startAnimating()
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return peripheralDict.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "jCell", for: indexPath) 
        
        if(indexPath.row == 0) {
            firstIndexPath = indexPath
        }
        
        if (peripheralDict.count > indexPath.row) {
            cell.textLabel?.adjustsFontSizeToFitWidth = true
            cell.detailTextLabel?.adjustsFontSizeToFitWidth = true
            cell.backgroundColor = UIColor.white.withAlphaComponent(0.6)
            
            // Configure the cell...
            var iconImage: UIImage? = UIImage(named: "rssiStrengthNil")
            cell.textLabel?.text = Array(peripheralDict.keys)[indexPath.row]
            
            if (abs(rssiTime.timeIntervalSince(peripheralDict[Array(peripheralDict.keys)[indexPath.row]]!.BLEtimeStamp! as Date)) > 2) {
                
                rssiTime = Date()
                
                let peripheralRSSIValue = peripheralDict[Array(peripheralDict.keys)[indexPath.row]]!.BLEperipheralRSSI!


                if (peripheralRSSIValue.intValue < -27 && peripheralRSSIValue.intValue > -110) {
                
                    if (peripheralRSSIValue.intValue <= -27 && peripheralRSSIValue.intValue > -60 ) {
                        iconImage = UIImage(named: "rssiStrength100")
                    }
                    
                    if (peripheralRSSIValue.intValue <= -60 && peripheralRSSIValue.intValue > -70 ) {
                        iconImage = UIImage(named: "rssiStrength75")
                    }
                    
                    if (peripheralRSSIValue.intValue <= -70 && peripheralRSSIValue.intValue > -80 ) {
                        iconImage = UIImage(named: "rssiStrength50")
                    }
                    
                    if (peripheralRSSIValue.intValue <= -80 && peripheralRSSIValue.intValue > -90 ) {
                        iconImage = UIImage(named: "rssiStrength25")
                    }
                    
                    if (peripheralRSSIValue.intValue <= -90 && peripheralRSSIValue.intValue > -110 ) {
                        iconImage = UIImage(named: "rssiStrength0")
                    }
                    
                    cell.detailTextLabel?.text = "RSSI: \(peripheralRSSIValue)dB"
                    cell.imageView?.image = iconImage
                }
            }
            
            
        }
        
        return cell
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the specified item to be editable.
    return true
    }
    */
    
    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    if editingStyle == .Delete {
    // Delete the row from the data source
    tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
    } else if editingStyle == .Insert {
    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
    }
    */
    
    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
    
    }
    */
    
    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the item to be re-orderable.
    return true
    }
    */
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        if (segue.destination.isKind(of: BLEConnectionTableViewController.self)) {
            let secondScreen = segue.destination as! BLEConnectionTableViewController
            
            //locationManager.stopRangingBeaconsInRegion(beaconRegion)
            
            // Pass the selected object to the new view controller.
            if let indexPath = self.tableView.indexPathForSelectedRow {
                cbCentralManager.stopScan()
                peripheralInstance = peripheralDict[Array(peripheralDict.keys)[indexPath.row]]?.BLEperipheralInstance
                secondScreen.peripheralName = Array(peripheralDict.keys)[indexPath.row]
                secondScreen.peripheralInstance = peripheralInstance
                secondScreen.cbCentralManager = cbCentralManager
                peripheralDict.removeAll()
                
                let appDelegate: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
                appDelegate.pStoreConfig = "BLESensor";
                let appContext: NSManagedObjectContext = appDelegate.managedObjectContext!
                let appUsers: AnyObject = NSEntityDescription.insertNewObject(forEntityName: "BLEPeripherals", into: appContext)
                appUsers.setValue(peripheralInstance!.name, forKey: "name")
                appUsers.setValue(Date(), forKey: "timeStamp")
                do {
                    try appContext.save()
                } catch _ {
                }
                
            } else {
                cbCentralManager.stopScan()
                secondScreen.peripheralName = self.peripheralInstance?.name
                secondScreen.peripheralInstance = self.peripheralInstance
                secondScreen.cbCentralManager = cbCentralManager
                peripheralDict.removeAll()
                
                let appDelegate: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
                appDelegate.pStoreConfig = "BLESensor";
                let appContext: NSManagedObjectContext = appDelegate.managedObjectContext!
                let appUsers: AnyObject = NSEntityDescription.insertNewObject(forEntityName: "BLEPeripherals", into: appContext)
                appUsers.setValue(peripheralInstance!.name, forKey: "name")
                appUsers.setValue(Date(), forKey: "timeStamp")
                do {
                    try appContext.save()
                } catch _ {
                }
            }
        }
    }
    
}
