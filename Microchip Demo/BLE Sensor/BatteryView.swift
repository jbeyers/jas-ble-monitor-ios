//
//  BatteryView.swift
//  SensorDemo
//
//  Created by Jayanth Murthy - C13538 on 11/9/15.
//  Copyright © 2015 Jayanth Murthy - C13538. All rights reserved.
//


import UIKit

@IBDesignable
class BatteryView: UIButton {
    
    @IBInspectable var fillColor: UIColor = UIColor.black
    @IBInspectable var percent: UInt8 = 100
    
    override func draw(_ rect: CGRect) {
        
        let path = UIBezierPath(rect: CGRect(x: 0, y: 0, width: ((self.frame.width-5) * CGFloat(percent))/100, height: self.frame.height))

        fillColor.setFill()
        path.fill()
        
        let context = UIGraphicsGetCurrentContext()
        context?.setLineWidth(3.0)
        UIColor(red: 48.0/255, green: 131.0/255, blue: 251/255, alpha: 1).set()
        //tintColor!.set()
        //UIColor.blueColor().set()
        context?.addRect(CGRect(x: 0, y: 0, width: self.frame.width-5, height: self.frame.height))
        context?.addRect(CGRect(x: self.frame.width-5, y: self.frame.height/2 - 5, width: 5, height: 10))
        context?.strokePath()
        
        /*
        //set up the width and height variables
        //for the horizontal stroke
        let plusHeight: CGFloat = 3.0
        let plusWidth: CGFloat = min(bounds.width, bounds.height) * 0.6
        
        //create the path
        var plusPath = UIBezierPath()
        
        //set the path's line width to the height of the stroke
        plusPath.lineWidth = plusHeight
        
        //move the initial point of the path
        //to the start of the horizontal stroke
        plusPath.moveToPoint(CGPoint(
        x:bounds.width/2 - plusWidth/2 + 0.5,
        y:bounds.height/2 + 0.5))
        
        //add a point to the path at the end of the stroke
        plusPath.addLineToPoint(CGPoint(
        x:bounds.width/2 + plusWidth/2 + 0.5,
        y:bounds.height/2 + 0.5))
        
        //Vertical Line
        if isAddButton {
        //move to the start of the vertical stroke
        plusPath.moveToPoint(CGPoint(
        x:bounds.width/2 + 0.5,
        y:bounds.height/2 - plusWidth/2 + 0.5))
        
        //add the end point to the vertical stroke
        plusPath.addLineToPoint(CGPoint(
        x:bounds.width/2 + 0.5,
        y:bounds.height/2 + plusWidth/2 + 0.5))
        }
        
        //set the stroke color
        UIColor.whiteColor().setStroke()
        
        //draw the stroke
        plusPath.stroke()
        */
        
    }
    
}
