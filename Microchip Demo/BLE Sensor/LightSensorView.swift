//
//  LightSensorView.swift
//  SensorDemo
//
//  Created by JM on 6/1/15.
//  Copyright (c) 2015 JM. All rights reserved.
//

import UIKit

@IBDesignable
class LightSensorView: UIButton {
    
    @IBInspectable var fillColor: UIColor = UIColor.black
    
    override func draw(_ rect: CGRect) {

        let path = UIBezierPath(ovalIn: rect)
        fillColor.setFill()
        path.fill()
        
        let context = UIGraphicsGetCurrentContext()
        context?.setLineWidth(3.0)
        UIColor(red: 48.0/255, green: 131.0/255, blue: 251/255, alpha: 1).set()
        //tintColor!.set()
        //UIColor.blueColor().set()
        //CGContextAddArc(context, (frame.size.width)/2, (frame.size.height)/2, (frame.size.width - 4)/2, 0.0, CGFloat(M_PI * 2.0), 1)
        let center = CGPoint(x: frame.size.width/2, y: frame.size.height/2)
        let radius = (frame.size.width - 4)/2
        context?.addArc(center: center, radius: radius, startAngle: 0, endAngle: CGFloat(.pi * 2.0), clockwise: true)
        context?.strokePath()
        
        
        /*
        //set up the width and height variables
        //for the horizontal stroke
        let plusHeight: CGFloat = 3.0
        let plusWidth: CGFloat = min(bounds.width, bounds.height) * 0.6
        
        //create the path
        var plusPath = UIBezierPath()
        
        //set the path's line width to the height of the stroke
        plusPath.lineWidth = plusHeight
        
        //move the initial point of the path
        //to the start of the horizontal stroke
        plusPath.moveToPoint(CGPoint(
            x:bounds.width/2 - plusWidth/2 + 0.5,
            y:bounds.height/2 + 0.5))
        
        //add a point to the path at the end of the stroke
        plusPath.addLineToPoint(CGPoint(
            x:bounds.width/2 + plusWidth/2 + 0.5,
            y:bounds.height/2 + 0.5))
        
        //Vertical Line
        if isAddButton {
            //move to the start of the vertical stroke
            plusPath.moveToPoint(CGPoint(
                x:bounds.width/2 + 0.5,
                y:bounds.height/2 - plusWidth/2 + 0.5))
            
            //add the end point to the vertical stroke
            plusPath.addLineToPoint(CGPoint(
                x:bounds.width/2 + 0.5,
                y:bounds.height/2 + plusWidth/2 + 0.5))
        }
        
        //set the stroke color
        UIColor.whiteColor().setStroke()
        
        //draw the stroke
        plusPath.stroke()
        */
        
    }
    
}
