//
//  HomeViewController.m
//  BLETR
//
//  Created by ILYAS on 23/11/15.
//  Copyright © 2015 ISSC. All rights reserved.
//

#import "HomeViewController.h"
#import "ConnectViewController.h"
#import "AppDelegate.h"
//#import "SettingTableViewController.h"
#import "ISMenuViewController.h"
//#import "BM78_BLE_UART-Swift.h"

@interface HomeViewController () {
    ConnectViewController *connect;
}

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    /*UIImage *image = [UIImage imageNamed:@"microchiplogo.png"];
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 40, 40)];
    imgView.image = image;
    self.navigationItem.titleView = imgView;*/
    self.navigationItem.title = @"Back";
    self.navigationController.navigationBar.tintColor = [UIColor redColor];
    self.navigationController.navigationBarHidden = YES;
    connect = [[ConnectViewController alloc] initWithNibName:@"ConnectViewController" bundle:nil];
    
    if ([UIScreen mainScreen].bounds.size.height > 480) {
        _scrollView.contentSize = CGSizeMake([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    }
    else {
        _scrollView.contentSize = CGSizeMake([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height+170);
    }
    [self prepareScrollView];
}

- (void)prepareScrollView {
    
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat xVal = 40;
    CGFloat yVal = width-130;
    CGFloat lab_Width = 130;
    CGFloat lab_val = 80;
    CGFloat lab_xVal = 20;
    CGFloat lab_yVal = width-150;
    
    UIButton *btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn1.frame = CGRectMake(xVal, 10, 90, 90);
    [btn1 setImage:[UIImage imageNamed:@"atmelConnectAppIcon"] forState:UIControlStateNormal];
    [btn1 addTarget:self action:@selector(onClickBLEDevices:) forControlEvents:UIControlEventTouchUpInside];

    UILabel *lab1 = [[UILabel alloc]initWithFrame:CGRectMake(lab_xVal, btn1.frame.origin.y+lab_val, lab_Width, 40)];
    [lab1 setFont:[UIFont systemFontOfSize:15.0f]];
    [lab1 setTextAlignment:NSTextAlignmentCenter];
    lab1.text = @"Bluetooth Smart";
    
    [_scrollView addSubview:lab1];
    [_scrollView addSubview:btn1];
    
    UIButton *btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn2.frame = CGRectMake(yVal, 10, 90, 90);
    [btn2 setImage:[UIImage imageNamed:@"beaconAppIcon"] forState:UIControlStateNormal];
    [btn2 addTarget:self action:@selector(onClickBeacon:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *lab2 = [[UILabel alloc]initWithFrame:CGRectMake(lab_yVal, btn2.frame.origin.y+lab_val, lab_Width, 40)];
    [lab2 setFont:[UIFont systemFontOfSize:15.0f]];
    [lab2 setTextAlignment:NSTextAlignmentCenter];
    lab2.text = @"Beacon Ranging";
    [_scrollView addSubview:lab2];
    [_scrollView addSubview:btn2];
    
    UIButton *btn3 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn3.frame = CGRectMake(xVal, 140, 90, 90);
    [btn3 setImage:[UIImage imageNamed:@"BLEIcon1"] forState:UIControlStateNormal];
    [btn3 addTarget:self action:@selector(onclickButtonBM70:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *lab3 = [[UILabel alloc]initWithFrame:CGRectMake(lab_xVal, btn3.frame.origin.y+lab_val, lab_Width, 40)];
    [lab3 setFont:[UIFont systemFontOfSize:15.0f]];
    [lab3 setTextAlignment:NSTextAlignmentCenter];
    lab3.text = @"BM 70";
    [_scrollView addSubview:lab3];
    [_scrollView addSubview:btn3];
    
    UIButton *btn4 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn4.frame = CGRectMake(yVal, 140, 90, 90);
    [btn4 setImage:[UIImage imageNamed:@"BLEIcon2"] forState:UIControlStateNormal];
    [btn4 addTarget:self action:@selector(onclickButtonBM78:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *lab4 = [[UILabel alloc]initWithFrame:CGRectMake(lab_yVal, btn4.frame.origin.y+lab_val, lab_Width, 40)];
    [lab4 setFont:[UIFont systemFontOfSize:15.0f]];
    [lab4 setTextAlignment:NSTextAlignmentCenter];
    lab4.text = @"BM 78";
    [_scrollView addSubview:lab4];
    [_scrollView addSubview:btn4];
    
    UIButton *btn5 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn5.frame = CGRectMake(xVal, 270, 90, 90);
    [btn5 setImage:[UIImage imageNamed:@"BLEIcon4"] forState:UIControlStateNormal];
    [btn5 addTarget:self action:@selector(onClickHealth:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *lab5 = [[UILabel alloc]initWithFrame:CGRectMake(lab_xVal, btn5.frame.origin.y+lab_val, lab_Width, 40)];
    [lab5 setFont:[UIFont systemFontOfSize:15.0f]];
    [lab5 setTextAlignment:NSTextAlignmentCenter];
    lab5.text = @"Health Widget";
    [_scrollView addSubview:lab5];
    [_scrollView addSubview:btn5];
    
    UIButton *btn6 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn6.frame = CGRectMake(yVal, 270, 90, 90);
    [btn6 setImage:[UIImage imageNamed:@"BLEIcon5"] forState:UIControlStateNormal];
    [btn6 addTarget:self action:@selector(onClickiBeacon:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *lab6 = [[UILabel alloc]initWithFrame:CGRectMake(lab_yVal, btn6.frame.origin.y+lab_val, lab_Width, 40)];
    [lab6 setFont:[UIFont systemFontOfSize:15.0f]];
    [lab6 setTextAlignment:NSTextAlignmentCenter];
    lab6.text = @"Beacon Things";
    [_scrollView addSubview:lab6];
    [_scrollView addSubview:btn6];
    
    UIButton *btn7 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn7.frame = CGRectMake(xVal, 400, 90, 90);
    [btn7 setImage:[UIImage imageNamed:@"Smart-Discovery"] forState:UIControlStateNormal];
    [btn7 addTarget:self action:@selector(onClickSmartDiscovery:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *lab7 = [[UILabel alloc]initWithFrame:CGRectMake(lab_xVal, btn7.frame.origin.y+lab_val, lab_Width, 40)];
    [lab7 setFont:[UIFont systemFontOfSize:15.0f]];
    [lab7 setTextAlignment:NSTextAlignmentCenter];
    lab7.text = @"Smart Discovery";
    [_scrollView addSubview:lab7];
    [_scrollView addSubview:btn7];
    
    UIButton *btn8 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn8.frame = CGRectMake(yVal, 400, 90, 90);
    [btn8 setImage:[UIImage imageNamed:@"BLESensor"] forState:UIControlStateNormal];
    [btn8 addTarget:self action:@selector(onClickBLESensor:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *lab8 = [[UILabel alloc]initWithFrame:CGRectMake(lab_yVal, btn8.frame.origin.y+lab_val, lab_Width, 40)];
    [lab8 setFont:[UIFont systemFontOfSize:15.0f]];
    [lab8 setTextAlignment:NSTextAlignmentCenter];
    lab8.text = @"BLE Sensor";
    [_scrollView addSubview:lab8];
    [_scrollView addSubview:btn8];
    
    UIButton *btn9 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn9.frame = CGRectMake(xVal, 530, 90, 90);
    [btn9 setImage:[UIImage imageNamed:@"Provision"] forState:UIControlStateNormal];
    [btn9 addTarget:self action:@selector(onClickBLEProvision:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *lab9 = [[UILabel alloc]initWithFrame:CGRectMake(lab_xVal, btn9.frame.origin.y+lab_val, lab_Width, 40)];
    [lab9 setFont:[UIFont systemFontOfSize:15.0f]];
    [lab9 setTextAlignment:NSTextAlignmentCenter];
    lab9.text = @"BLE Provisioner";
    [_scrollView addSubview:lab9];
    [_scrollView addSubview:btn9];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.toolbarHidden = YES;
    
    _versionLab.text = [NSString stringWithFormat:@"MBD v%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onclickButtonBM70:(id)sender {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.deviceType  = BLE_BM70;
    appDelegate.isBLE78 = NO;
    
    [[UINavigationBar appearance] setBackIndicatorImage:nil];
    [[UINavigationBar appearance] setBackIndicatorTransitionMaskImage:nil];
    [[UINavigationBar appearance] setTintColor:[UIColor redColor]];
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController pushViewController:connect animated:YES];
}

- (IBAction)onclickButtonBM78:(id)sender {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.deviceType  = BLE_BM78;
    appDelegate.isBLE78 = YES;
    
    [[UINavigationBar appearance] setBackIndicatorImage:nil];
    [[UINavigationBar appearance] setBackIndicatorTransitionMaskImage:nil];
    [[UINavigationBar appearance] setTintColor:[UIColor redColor]];
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController pushViewController:connect animated:YES];
}

/*- (IBAction)onclickMFiAudio:(id)sender {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.deviceType  = MFI_AUDIO;
    appDelegate.isBLE78 = NO;
    
    SettingTableViewController *tableViewController = [[SettingTableViewController alloc] initWithNibName:@"SettingTableViewController" bundle:nil];
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController pushViewController:tableViewController animated:YES];
    [tableViewController release];
}*/

- (IBAction)onClickHealth:(id)sender {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.deviceType  = BLE_BM78;
    appDelegate.isBLE78 = YES;
    
    ISMenuViewController *menuView = [[ISMenuViewController alloc] init];
    [[UINavigationBar appearance] setBackIndicatorImage:nil];
    [[UINavigationBar appearance] setBackIndicatorTransitionMaskImage:nil];
    [[UINavigationBar appearance] setTintColor:[UIColor redColor]];
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController pushViewController:menuView animated:YES];
    [menuView release];
}

- (IBAction)onClickiBeacon:(id)sender {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.deviceType  = BLE_BM78;
    appDelegate.isBLE78 = YES;
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"beacon" bundle:nil];
    UITabBarController *tbc = [storyboard instantiateViewControllerWithIdentifier:@"iBeaconTab"];
    tbc.selectedIndex=0;
    
    [[UINavigationBar appearance] setBackIndicatorImage:nil];
    [[UINavigationBar appearance] setBackIndicatorTransitionMaskImage:nil];
    [[UINavigationBar appearance] setTintColor:[UIColor redColor]];
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController pushViewController:tbc animated:YES];
    for(UIViewController *v in tbc.viewControllers)
        v = nil;
    tbc = nil;
    storyboard = nil;
}

- (IBAction)onClickBLEDevices:(id)sender {      //Atmel
    
    NSString * storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    UIViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"BLEDeviceListVC"];
    
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:vc];
    nav.navigationBar.barTintColor = [UIColor colorWithRed:255.0/255.0 green:20.0/255.0 blue:12.0/255.0 alpha:1.0];
    nav.navigationBar.barStyle = UIBarStyleDefault;
    [self.navigationController presentViewController:nav animated:YES completion:nil];
}


- (IBAction)onClickBeacon:(id)sender {      //Atmel
  
    NSString * storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    UIViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"BeaconRanging"];
    
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:vc];
    nav.navigationBar.barTintColor = [UIColor colorWithRed:255.0/255.0 green:20.0/255.0 blue:12.0/255.0 alpha:1.0];
    nav.navigationBar.barStyle = UIBarStyleDefault;
    [self.navigationController presentViewController:nav animated:YES completion:nil];
}

- (IBAction)onClickSmartDiscovery:(id)sender {
    
    NSString * storyboardName = @"SmartMain";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    UITabBarController * vc = [storyboard instantiateViewControllerWithIdentifier:@"Smart_TabBarController"];
    
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:vc];
    nav.navigationBar.barTintColor = [UIColor colorWithRed:255.0/255.0 green:20.0/255.0 blue:12.0/255.0 alpha:1.0];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    nav.navigationBar.barStyle = UIBarStyleDefault;
    [self.navigationController pushViewController:vc animated:YES];
    //[self.navigationController presentViewController:nav animated:NO completion:nil];
}

- (IBAction)onClickBLESensor:(id)sender {
    
    NSString * storyboardName = @"SensorMain";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    UIViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"BLESensors"];
    
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:vc];
    nav.navigationBar.barTintColor = [UIColor colorWithRed:255.0/255.0 green:20.0/255.0 blue:12.0/255.0 alpha:1.0];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    nav.navigationBar.barStyle = UIBarStyleDefault;
    [self.navigationController presentViewController:nav animated:YES completion:nil];
}

- (IBAction)onClickBLEProvision:(id)sender {
    
    NSString * storyboardName = @"Provision";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    UIViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"MasterView"];
    
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:vc];
    nav.navigationBar.barTintColor = [UIColor colorWithRed:255.0/255.0 green:20.0/255.0 blue:12.0/255.0 alpha:1.0];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    nav.navigationBar.barStyle = UIBarStyleDefault;
    [self.navigationController presentViewController:nav animated:YES completion:nil];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
