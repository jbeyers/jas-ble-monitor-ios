//
//  ViewController.h
//  BLEDKAPP
//
//  Created by D500 user on 12/9/25.
//  Copyright (c) 2012 ISSC Technologies Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyPeripheral.h"
#import "DataTransparentViewController.h"
#import "DeviceInfoViewController.h"
#import "ProprietaryViewController.h"

@interface BLEViewController : UIViewController //<CBPeripheralManagerDelegate>
{
    

    UIBarButtonItem *disconnectButton;
    CBPeripheralManager *pmgr;
    UIBarButtonItem *leftButton;
    IBOutlet UIButton *proprietaryBtn;
    IBOutlet UILabel *proprietaryLabel;
}
#define SERVICE_UUID_GAP                   [CBUUID UUIDWithString:@"1800"]
#define SERVICE_UUID_GATT                  [CBUUID UUIDWithString:@"1801"]

@property(retain) MyPeripheral    *connectedPeripheral;
@property(retain) DataTransparentViewController *transparentPage;
@property(retain) DeviceInfoViewController *deviceInfoPage;
@property(retain) ProprietaryViewController *proprietaryPage;

- (IBAction)enterTransparentPage:(id)sender;
- (IBAction)enterProprietaryPage:(id)sender;
- (IBAction)enterDeviceInfoPage:(id)sender;
@end
