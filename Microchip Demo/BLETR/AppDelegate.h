//
//  AppDelegate.h
//  BLETR
//
//  Created by D500 user on 13/5/7.
//  Copyright (c) 2013年 ISSC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "AppDelegateProtocol.h"

typedef enum {
    BLE_BM70 = 0,
    BLE_BM78,
    MFI_AUDIO,
}DEVICE_TYPE;

//@class ConnectViewController;
@class HomeViewController;
@class BeaconDataObject;

@interface AppDelegate : UIResponder <UIApplicationDelegate, UINavigationControllerDelegate, AppDelegateProtocol>
{
    BOOL _pageTransition;
    BOOL isBLE78;
    BeaconDataObject *theAppDataObject;
}
@property (strong, nonatomic) UIWindow *window;

@property(strong, nonatomic) HomeViewController *viewController;
@property(nonatomic, retain) UINavigationController *navigationController;
@property(assign) BOOL pageTransition;
@property(nonatomic,readwrite) NSInteger deviceType;
@property(nonatomic,readwrite) BOOL isBLE78;

// Health app
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic , retain) BeaconDataObject *theAppDataObject;
@property (nonatomic, retain)NSString *pStoreConfig;

#ifdef USE_HEALTH_KIT
@property (nonatomic) HKHealthStore *healthStore;
- (BOOL)isHealthKitAvailable;
#endif
- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
