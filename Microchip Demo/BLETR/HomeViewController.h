//
//  HomeViewController.h
//  BLETR
//
//  Created by ILYAS on 23/11/15.
//  Copyright © 2015 ISSC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController : UIViewController

@property(nonatomic,retain)IBOutlet UILabel *versionLab;

@property(nonatomic, retain)IBOutlet UIScrollView *scrollView;

@property(nonatomic, strong)IBOutlet UIButton *btSmart;
@property(nonatomic, strong)IBOutlet UIButton *btBeRan;
@property(nonatomic, strong)IBOutlet UIButton *btBm70;
@property(nonatomic, strong)IBOutlet UIButton *btBm78;
@property(nonatomic, strong)IBOutlet UIButton *btAud;
@property(nonatomic, strong)IBOutlet UIButton *btHeal;
@property(nonatomic, strong)IBOutlet UIButton *btBeTh;
@property(nonatomic, strong)IBOutlet UIButton *btDisc;
@property(nonatomic, strong)IBOutlet UIButton *btSens;

- (IBAction)onclickButtonBM70:(id)sender;

- (IBAction)onclickButtonBM78:(id)sender;

@end
