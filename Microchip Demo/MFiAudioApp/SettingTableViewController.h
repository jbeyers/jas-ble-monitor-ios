//
//  SettingTableViewController.h
//  MFiAudioAPP
//
//  Created by D500 user on 13/4/24.
//  Copyright (c) 2013年 ISSC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ISAudioControlManager.h"
#import "GPIODemoViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "ISSCTableAlertView.h"
#import "RecordViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "CBController.h"

//#define APP_DEMO_VERSION    1
#define APP_VERSION [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]
#define ModifiedIO

//@interface SettingTableViewController : UITableViewController<UITextFieldDelegate, ISAudioControlManagerDelegate, AVAudioPlayerDelegate, TableAlertViewDelegate , CBControllerDelegate>
@interface SettingTableViewController : UITableViewController<UITextFieldDelegate, AVAudioPlayerDelegate, TableAlertViewDelegate , CBControllerDelegate>
{
    NSString *fwVersion;
    NSMutableArray *_queuedTasks;
    UIBarButtonItem *_applyGPIO;
    AVAudioPlayer *findAccessoryPlayer;
    UIButton    *findAccessoryButton;
    UIButton    *forceAlertButton;
    NSArray *findAccessoryTones;
    MyPeripheral *controlPeripheral;
    CBController *_cbController;
    //==================================//
    NSMutableData *_events;
    NSMutableArray *_waitingACK;
    uint8_t       _dspParamRecord;
    NSData        *_tts_data;
    TTS_COMMAND_TYPE _tts_type;
    TTS_INSTRUCTION_PARAMETER _tts_ins_type;
    TTS_VOICE_DATA_PARAMETER _tts_data_type;
    int           _tts_data_index;
    ISAudioControlManagerState _managerState;
    
}
@property (retain) NSMutableArray *dataSource;
@property (retain) NSMutableArray *gpioList;
@property (retain) UITableViewController *equalizerTableView;
@property (retain) UITableViewController *gpioControlTableView;
@property (retain) NSArray *equalizerModeOptions;
@property (retain) NSIndexPath *selectedEQMode;
//@property (retain) ISAudioControlManager *audioControl;
@property (retain) UIBarButtonItem *connectBarButtonItem;
@property (retain) UIBarButtonItem *disconnectBarButtonItem;
#ifdef APP_DEMO_VERSION
@property (retain) GPIODemoViewController *gpioViewController;
#endif
@property (retain) RecordViewController *recordViewController;

//ML added
#if 1
@property (retain) UITableViewController *LE_DeviceListTableView;
@property (retain) NSMutableArray *LE_DeviceList;
@property (nonatomic) BOOL CB_Initial;
@property (nonatomic , strong) NSString *LE_DeviceName;
@property (nonatomic) NSInteger SelectedLEDeviceListIndex;
@property (assign) BOOL DeviceIsSelect;
@property (assign) BOOL NRIsEnabled;    //Read device NR Setting
@property (assign) uint8_t SyncDeviceEQ;
@property (assign) int GPIOIndex;
@property (assign) BOOL GPIOIndexDir;
@property (assign) BOOL GPIOIndexValue;
#endif

@property (assign) BOOL didChangeEqMode;

- (void)noiseCancellationTxValueChange:(id)sender;
- (void)noiseCancellationRxValueChange:(id)sender;
- (void)ringFindAccessoryTone:(id)sender;
- (void)ForceBuzzerAlert:(id)sender;
- (void)checkCell:(UITableViewCell *)cell;
- (void)uncheckCell:(UITableViewCell *)cell;
- (void)applyGPIOChange;
- (void)queueTask:(SEL)selector target:(id)target argument:(void *)arg1,...;
- (void)checkQueuedTask;
@end
