//
//  ToggleSwitchTableViewCell.h
//  MFiAudioAPP
//
//  Created by D500 user on 13/4/24.
//  Copyright (c) 2013年 ISSC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ToggleSwitchTableViewCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UISwitch *stateSwitch;
@property (retain, nonatomic) IBOutlet UILabel *itemLabel;


@property (retain, nonatomic) IBOutlet UIImageView *customImageView;
@property (assign) char cellType;
@end
