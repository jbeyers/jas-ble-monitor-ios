//
//  InformationTableViewCell.h
//  MFiAudioAPP
//
//  Created by D500 user on 13/4/30.
//  Copyright (c) 2013年 ISSC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InformationTableViewCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UILabel *itemLabel;
@property (retain, nonatomic) IBOutlet UILabel *informationLabel;

@end
