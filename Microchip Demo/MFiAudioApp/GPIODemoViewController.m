//
//  GPIODemoViewController.m
//  MFiAudioAPP
//
//  Created by D500 user on 13/6/6.
//  Copyright (c) 2013�?ISSC. All rights reserved.
//
#import "AppDelegate.h"
#import "GPIODemoViewController.h"

#define indexGPIOLED1      1
#define indexGPIOButton1   5
#define indexGPIOLED2      14
#define indexGPIOButton2   23
#define valueGPIOButton1Pressed     FALSE
#define valueGPIOButton2Pressed     FALSE
#define buttonPressedColor  [UIColor colorWithRed:152.0/255.0 green:152.0/255.0 blue:0 alpha:1.0]
#define buttonReleasedColor [UIColor colorWithRed:243.0/255.0 green:152.0/255.0 blue:0 alpha:1.0]
#define buttonSideColor     [UIColor colorWithRed:170.0/255.0 green:105.0/255.0 blue:0 alpha:1.0]
#define LEDON   TRUE
#define LEDOFF  FALSE

@interface GPIODemoViewController ()

@end

@implementation GPIODemoViewController
@synthesize audioControl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _inputButton1Status = _inputButton2Status = ~valueGPIOButton1Pressed;
        _LEDSwitch1Status = _LEDSwitch2Status = LEDOFF;
        LEDPatternTimer = nil;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }

    // Do any additional setup after loading the view from its nib.
    
 /*   UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(200, 28, 57, 57)];
    [titleLabel setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Icon_old.png"]]];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];//aaa
    self.navigationItem.titleView = titleLabel;
    [titleLabel release];
   */ 
    _inputButton1.faceColor = buttonReleasedColor;
    _inputButton1.faceColorSelected = buttonPressedColor;
    _inputButton1.sideColor = buttonSideColor;
    _inputButton1.radius = 6.0;
    _inputButton1.margin = 7.0;
    _inputButton1.depth = 6.0;
    
    _inputButton1.titleLabel.font = [UIFont boldSystemFontOfSize:16];
    [_inputButton1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
   // [_inputButton1 setEnabled:NO];
    
    _inputButton2.faceColor = buttonReleasedColor;
    _inputButton2.faceColorSelected = buttonPressedColor;
    _inputButton2.sideColor = buttonSideColor;
    _inputButton2.radius = 6.0;
    _inputButton2.margin = 7.0;
    _inputButton2.depth = 6.0;
    
    _inputButton2.titleLabel.font = [UIFont boldSystemFontOfSize:16];
    [_inputButton2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
  //  [_inputButton2 setEnabled:NO];
    
    UIBarButtonItem *LEDPattern1Button = [[UIBarButtonItem alloc] initWithTitle:@"LED Pattern1" style:UIBarButtonItemStyleBordered target:self action:@selector(LEDcontrollerPattern1)];
    
    UIBarButtonItem *LEDPattern2Button = [[UIBarButtonItem alloc] initWithTitle:@"LED Pattern2" style:UIBarButtonItemStyleBordered target:self action:@selector(LEDcontrollerPattern2)];
    UIBarButtonItem *LEDStopButton = [[UIBarButtonItem alloc] initWithTitle:@"Stop Pattern" style:UIBarButtonItemStyleBordered target:self action:@selector(LEDcontrollerPatternStop)];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [[appDelegate navigationController] setToolbarHidden:NO animated:NO];
    [self setToolbarItems:[NSArray arrayWithObjects:LEDPattern1Button, LEDPattern2Button, LEDStopButton, nil]];
    
}

- (void)viewDidAppear:(BOOL)animated {
    
    [self synchronizeIOState:NO];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [[appDelegate navigationController] setToolbarHidden:NO animated:NO];
    [audioControl setGpioControlDelegate:self];
}

- (void)viewDidDisappear:(BOOL)animated {
    [self LEDcontrollerPatternStop:NO];
}

- (void)setLEDImage:(UIImageView *)imageView status:(BOOL)status {
    if (status == LEDON)
        [imageView setImage:[UIImage imageNamed:@"ledblue.png"]];
    else
        [imageView setImage:[UIImage imageNamed:@"ledblack.png"]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_inputButton1 release];
    [_inputButton2 release];
    [_LEDSwitch1 release];
    [_LEDSwitch2 release];
    [_LED1Image release];
    [_LED2Image release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setInputButton1:nil];
    [self setInputButton2:nil];
    [self setLEDSwitch1:nil];
    [self setLEDSwitch2:nil];
    [self setLED1Image:nil];
    [self setLED2Image:nil];
    [super viewDidUnload];
}

- (void)synchronizeIOState {
    [self synchronizeIOState:NO];
}

- (void)synchronizeIOState:(BOOL)error {
    if (_inputButton1Status == valueGPIOButton1Pressed)
        [_inputButton1 setSelected:YES];
    else
        [_inputButton1 setSelected:NO];
    if (_inputButton2Status == valueGPIOButton2Pressed)
        [_inputButton2 setSelected:YES];
    else
        [_inputButton2 setSelected:NO];
    [_LEDSwitch1 setOn:_LEDSwitch1Status];
    [_LEDSwitch2 setOn:_LEDSwitch2Status];
    [self setLEDImage:_LED1Image status:_LEDSwitch1Status];
    [self setLEDImage:_LED2Image status:_LEDSwitch2Status];
    ISGPIOControlProperty *ioPropertyLED1 = [[ISGPIOControlProperty alloc] init];
    ioPropertyLED1.inOut = YES;
    ioPropertyLED1.index = indexGPIOLED1;
    ioPropertyLED1.value = _LEDSwitch1Status;
    ISGPIOControlProperty *ioPropertyLED2 = [[ISGPIOControlProperty alloc] init];
    ioPropertyLED2.inOut = YES;
    ioPropertyLED2.index = indexGPIOLED2;
    ioPropertyLED2.value = _LEDSwitch2Status;
    ISGPIOControlProperty *ioPropertyButton1 = [[ISGPIOControlProperty alloc] init];
    ioPropertyButton1.inOut = NO;
    ioPropertyButton1.index = indexGPIOButton1;
    ISGPIOControlProperty *ioPropertyButton2 = [[ISGPIOControlProperty alloc] init];
    ioPropertyButton2.inOut = NO;
    ioPropertyButton2.index = indexGPIOButton2;
    if (!error) {
        if (audioControl.managerState == ISAudioControlManagerStateNormal)
            [audioControl gpioControl:[NSArray arrayWithObjects:ioPropertyLED1, ioPropertyLED2, ioPropertyButton1, ioPropertyButton2, nil]];
    }
}

- (IBAction)LEDSwitchValueChange1:(id)sender {
    
    ISGPIOControlProperty *ioProperty = [[[ISGPIOControlProperty alloc] init] autorelease];
    UISwitch *LEDSwitch = (UISwitch *)sender;
    ioProperty.inOut = YES;
    ioProperty.index = indexGPIOLED1;
    ioProperty.value = LEDSwitch.on;
    _LEDSwitch1Status = ioProperty.value;
    
    [audioControl gpioControl:[NSArray arrayWithObjects:ioProperty, nil]];
    [self setLEDImage:_LED1Image status:_LEDSwitch1Status];
}

- (IBAction)LEDSwitchValueChange2:(id)sender {
    ISGPIOControlProperty *ioProperty = [[[ISGPIOControlProperty alloc] init] autorelease];
    UISwitch *LEDSwitch = (UISwitch *)sender;
    ioProperty.inOut = YES;
    ioProperty.index = indexGPIOLED2;
    ioProperty.value = LEDSwitch.on;
    _LEDSwitch2Status = ioProperty.value;
    [audioControl gpioControl:[NSArray arrayWithObjects:ioProperty, nil]];
    [self setLEDImage:_LED2Image status:_LEDSwitch2Status];
}

- (void)LEDcontrollerPattern1 {
    static BOOL LEDToggle = TRUE;
   // NSLog(@"LEDcontrollerPattern1");
    if (LEDPatternTimer) {
        [LEDPatternTimer invalidate];
        LEDPatternTimer = nil;
    }
    ISGPIOControlProperty *ioPropertyLED1 = [[[ISGPIOControlProperty alloc] init] autorelease];
    ioPropertyLED1.inOut = YES;
    ioPropertyLED1.index = indexGPIOLED1;
    
    ISGPIOControlProperty *ioPropertyLED2 = [[[ISGPIOControlProperty alloc] init] autorelease];
    ioPropertyLED2.inOut = YES;
    ioPropertyLED2.index = indexGPIOLED2;
    
    
    if (LEDToggle) {
        ioPropertyLED1.value = LEDON;
        ioPropertyLED2.value = LEDOFF;
    }
    else {
        ioPropertyLED1.value = LEDOFF;
        ioPropertyLED2.value = LEDON;
    }
    if (audioControl.managerState == ISAudioControlManagerStateNormal) {
        LEDPatternTimer = [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(LEDcontrollerPattern1) userInfo:nil repeats:NO];
        [audioControl gpioControl:[NSArray arrayWithObjects:ioPropertyLED1, ioPropertyLED2, nil]];
        [self setLEDImage:_LED1Image status:ioPropertyLED1.value];
        [self setLEDImage:_LED2Image status:ioPropertyLED2.value];
        LEDToggle = !LEDToggle;
    }
}

- (void)LEDcontrollerPattern2 {
    static BOOL LEDToggle = TRUE;
    if (LEDPatternTimer) {
        [LEDPatternTimer invalidate];
        LEDPatternTimer = nil;
    }
        
    ISGPIOControlProperty *ioPropertyLED1 = [[[ISGPIOControlProperty alloc] init] autorelease];
    ioPropertyLED1.inOut = YES;
    ioPropertyLED1.index = indexGPIOLED1;
    
    ISGPIOControlProperty *ioPropertyLED2 = [[[ISGPIOControlProperty alloc] init] autorelease];
    ioPropertyLED2.inOut = YES;
    ioPropertyLED2.index = indexGPIOLED2;
    
    
    if (LEDToggle) {
        ioPropertyLED1.value = LEDON;
        ioPropertyLED2.value = LEDON;
    }
    else {
        ioPropertyLED1.value = LEDOFF;
        ioPropertyLED2.value = LEDOFF;
    }
    
    if (audioControl.managerState == ISAudioControlManagerStateNormal) {
        LEDPatternTimer = [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(LEDcontrollerPattern2) userInfo:nil repeats:NO];
        [audioControl gpioControl:[NSArray arrayWithObjects:ioPropertyLED1, ioPropertyLED2, nil]];
        [self setLEDImage:_LED1Image status:ioPropertyLED1.value];
        [self setLEDImage:_LED2Image status:ioPropertyLED2.value];
        LEDToggle = !LEDToggle;
    }
}

- (void)LEDcontrollerPatternStop {
    [self LEDcontrollerPatternStop:NO];
}

- (void)LEDcontrollerPatternStop:(BOOL)error {
    if (LEDPatternTimer != nil) {
        [LEDPatternTimer invalidate];
        LEDPatternTimer = nil;
    }
    _LEDSwitch1Status = _LEDSwitch2Status = LEDOFF;
    [self synchronizeIOState:error];
}

- (void)updateButtonDetection:(NSArray *)ioState {
    NSLog(@"[GPIODemoViewController] updateButtonDetection, count=%d", [ioState count]);
    ISGPIOControlProperty *ioProperty = nil;
    for (uint8_t i = 0; i < [ioState count]; i++) {
        ioProperty = [ioState objectAtIndex:i];
        NSLog(@"[GPIODemoViewController] updateButtonDetection, index=%d, value = %d", ioProperty.index, ioProperty.value);
        if (ioProperty.index == indexGPIOButton1) {
            _inputButton1Status = ioProperty.value;
            if (ioProperty.value == valueGPIOButton1Pressed)
            {
                NSLog(@"button1 set selected");
                [_inputButton1 setSelected:YES];
            }
            else
                [_inputButton1 setSelected:NO];
        }
        else if (ioProperty.index == indexGPIOButton2) {
            _inputButton2Status = ioProperty.value;
            if (ioProperty.value == valueGPIOButton2Pressed) {
                NSLog(@"button2 set selected");
                [_inputButton2 setSelected:YES];
            }
            else
                [_inputButton2 setSelected:NO];
        }
    }
}

- (void)ISAudioControlManager:(ISAudioControlManager *)manager reportErrorForcmdID:(uint8_t)cmdID error:(NSError *)error {
    NSLog(@"[GPIODemoViewController] reportErrorForcmdID, cmdID=%d", cmdID);
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error Occurs!" message:[NSString stringWithFormat:@"%@\n Command id = %02x ", error.domain, cmdID] delegate:self cancelButtonTitle:@"Close" otherButtonTitles: nil];
    [alert show];
    [alert release];
    [self LEDcontrollerPatternStop:YES];
}
@end
