//
//  ISAudioControlManager.m
//  MFiAudioAPP
//
//  Created by D500 user on 13/5/10.
//  Copyright (c) 2013�?ISSC. All rights reserved.
//

#import "ISAudioControlManager.h"
#import <CoreTelephony/CTCallCenter.h>
#import <CoreTelephony/CTCall.h>

#define START_BYTE 0xAA

@implementation ISGPIOControlProperty

@end

@interface ACKTimer : NSObject
@property (retain) NSTimer *timer;
@property (assign) uint8_t commandID;
- (id)initWithCommandID:(uint8_t)cmdID timer:(NSTimer *)timer;
@end

@implementation ACKTimer

- (id)initWithCommandID:(uint8_t)cmdID timer:(NSTimer *)timer {
    self = [super init];
    if (self) {
        self.timer = timer;
        self.commandID = cmdID;
    }
    return self;
}
@end

@interface ISAudioControlManager () {
    BOOL send;
    BOOL TTS_cancel;
    BOOL Clear_TTS_Buffer;
    NSMutableArray *tts_buffer;
    CTCallCenter *call_center;
}

@end

@implementation ISAudioControlManager

@synthesize delegate;
@synthesize managerState = _managerState;
@synthesize gpioControlDelegate;
@synthesize didChangeEqMode;

- (id)init {
    self = [super init];
    if (self) {
        //dataPath = [[MFiDataPath alloc] initWithProtocolString:@"com.issc.datapath"];
        didChangeEqMode = TRUE;
        
        _events = [[NSMutableData alloc] init];
        // _agendumEQMode = EQ_MODE_INVALID;
        _waitingACK = [[NSMutableArray alloc] init];
        send = NO;
        TTS_cancel = NO;
        Clear_TTS_Buffer = NO;
        _tts_data = nil;
        tts_buffer = [[NSMutableArray array] retain];
        call_center = [[CTCallCenter alloc] init];
        __block id week_self = self;
        [call_center setCallEventHandler:^(CTCall *call) {
            NSString *callState = call.callState;
            NSLog(@"%@",callState );
            if (_managerState != ISAudioControlManagerStateNormal) {
                return;
            }
            if ([callState isEqualToString:CTCallStateDialing]) {
                if ([tts_buffer count]>0) {
                    [tts_buffer removeAllObjects];
                }
                if (_tts_data) {
                    TTS_cancel = YES;
                }
                else {
                    //[week_self clearTTSBuffer];
                }
            }
            else if ([callState isEqualToString:CTCallStateIncoming]) {
                if ([tts_buffer count]>0) {
                    [tts_buffer removeAllObjects];
                }
                if (_tts_data) {
                    TTS_cancel = YES;
                }
                else {
                    //[week_self clearTTSBuffer];
                }
            }
            else if ([callState isEqualToString:CTCallStateConnected]) {
                
            }
            else if ([callState isEqualToString:CTCallStateDisconnected]) {
                if ([tts_buffer count]>0) {
                    [tts_buffer removeAllObjects];
                }
                if (_tts_data) {
                    TTS_cancel = YES;
                }
                else {
                    //[week_self clearTTSBuffer];
                }
                
            }
        }];
    }
    return self;
}

- (void)dealloc {
    [tts_buffer release];
    tts_buffer = nil;
    [call_center release];
    [super dealloc];
}

- (void)ACKTimeoutOfCommand:(NSNumber *)cmdID {
    NSLog(@"[ISAudioControlManager] ACKTimeoutOfCommand = %02x", [cmdID unsignedCharValue]);
    /* UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"ACK Timeout" message:[NSString stringWithFormat:@"Command id = %02x no ACK",[cmdID unsignedCharValue]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
     [self removeCheckResponseTimerForCommand:[cmdID unsignedCharValue]];
     [alert show];
     [alert release];
     */
    [self removeCheckResponseTimerForCommand:[cmdID unsignedCharValue]];
    NSError *error = [[[NSError alloc] initWithDomain:@"ACK Timeout" code:-1 userInfo:nil] autorelease];
    if ([cmdID unsignedCharValue] == AUDIO_COMMAND_ID_GPIO_CONTROL) {
        if (gpioControlDelegate && [(NSObject *)gpioControlDelegate respondsToSelector:@selector(ISAudioControlManager:reportErrorForcmdID:error:)]) {
            [gpioControlDelegate ISAudioControlManager:self reportErrorForcmdID:[cmdID unsignedCharValue] error:error];
        }
    }
    else if ([cmdID unsignedCharValue] == AUDIO_COMMAND_ID_EQ_MODE_SETTING) {
        didChangeEqMode = TRUE;
        
    }
    else if ([cmdID unsignedCharValue] == AUDIO_COMMAND_ID_TTS) {
        [_tts_data release];
        _tts_data = nil;
        _tts_data_index = 0;
        if ([tts_buffer count]>0) {
            [tts_buffer removeAllObjects];
        }
    }
    else if (delegate && [(NSObject *)delegate respondsToSelector:@selector(ISAudioControlManager:reportErrorForcmdID:error:)]) {
        [delegate ISAudioControlManager:self reportErrorForcmdID:[cmdID unsignedCharValue] error:error];
    }
}

- (void)checkResponseForCommand:(uint8_t)cmdID timeout:(NSTimeInterval)timeout {
    //   static uint32_t identifier = 0;
    //   identifier++;
    //NSLog(@"[ISAudioControlManager] checkResponseForCommand %02x",cmdID);
    NSMethodSignature *sgn = [self methodSignatureForSelector:@selector(ACKTimeoutOfCommand:)];
    NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:sgn];
    [invocation setTarget:self];
    [invocation setSelector:@selector(ACKTimeoutOfCommand:)];
    NSNumber *arg = [[[NSNumber alloc] initWithUnsignedChar:cmdID] autorelease];
    [invocation setArgument:&arg atIndex:2];
    // [invocation setArgument:[[NSNumber alloc] initWithUnsignedChar:cmdID] atIndex:2];
    [invocation retainArguments];
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:timeout invocation:invocation repeats:NO];
    ACKTimer *obj = [[ACKTimer alloc] initWithCommandID:cmdID timer:timer];
    [_waitingACK addObject:obj];
}

- (BOOL)removeCheckResponseTimerForCommand:(uint8_t)cmdID {
    ACKTimer *obj = nil;
    NSLog(@"[ISAudiocontrolManager] removeCheckResponseTimerForCommand, cmdID = %02x, count = %d", cmdID, [_waitingACK count]);
    for (int i=0; i<[_waitingACK count]; i++) {
        obj = [_waitingACK objectAtIndex:i];
        NSLog(@"[ISAudiocontrolManager] removeCheckResponseTimerForCommand, waitingACK = %02x", obj.commandID);
        if (obj.commandID == cmdID) {
            NSLog(@"[ISAudiocontrolManager] removeCheckResponseTimerForCommand,found");
            [obj.timer invalidate];
            [_waitingACK removeObject:obj];
            return TRUE;
        }
    }
    return FALSE;
}

- (id<ISAudioControlManagerDelegate>)delegate {
    return delegate;
}

- (void)setDelegate:(id<ISAudioControlManagerDelegate>)aDelegate {
    delegate = aDelegate;
    if (delegate && [(NSObject *)delegate respondsToSelector:@selector(ISAudiocontrolManager:didUpdateState:)])
        [delegate ISAudiocontrolManager:self didUpdateState:_managerState];
    [self setGpioControlDelegate:aDelegate];
}

- (unsigned char)calculateChecksum:(unsigned char *)data dataLength:(unsigned short)length {
    unsigned char checksum = 0x00;
    while (length--) {
        checksum += *data++;
    }
    return (0x100 - checksum);
}

- (void)sendCommand:(unsigned char*)command length:(int)length {
    
}

- (void)returnEventAck: (uint8_t)cmdID {
    unsigned char buf[25];
    memset(buf, 0x00, sizeof(buf));
    AUDIO_COMMAND_HEADER *header = (AUDIO_COMMAND_HEADER *)buf;
    header->start_byte = START_BYTE;
    header->commandID = AUDIO_COMMAND_ID_EVENT_ACK;
    unsigned char idx = COMMAND_HEADER_SIZE;
    
    buf[idx++] = cmdID;
    
    header->length_high = 0x00;
    header->length_low = idx - COMMAND_HEADER_SIZE + 1;
    buf[idx] = [self calculateChecksum:&buf[1] dataLength:idx-1];
    [self sendCommand:buf length:idx+1];
}

- (void)setEventMaskSetting: (NSData *)maskTable {
    unsigned char buf[25];
    memset(buf, 0x00, sizeof(buf));
    AUDIO_COMMAND_HEADER *header = (AUDIO_COMMAND_HEADER *)buf;
    header->start_byte = START_BYTE;
    header->commandID = AUDIO_COMMAND_ID_EVENT_MSASK_SETTING;
    unsigned char idx = COMMAND_HEADER_SIZE;
    
    buf[idx++] = 0xf7; //enable caller id event
    buf[idx++] = 0xff;
    buf[idx++] = 0xfe;
    buf[idx++] = 0xff;
    
    header->length_high = 0x00;
    header->length_low = idx - COMMAND_HEADER_SIZE + 1;
    buf[idx] = [self calculateChecksum:&buf[1] dataLength:idx-1];
    [self sendCommand:buf length:idx+1];
}

- (void)changeDeviceName: (NSData *)deviceName {
    //  NSMutableData *data = [[NSMutableData alloc] init];
    unsigned char buf[50];
    memset(buf, 0x00, sizeof(buf));
    AUDIO_COMMAND_HEADER *header = (AUDIO_COMMAND_HEADER *)buf;
    header->start_byte = START_BYTE;
    header->commandID = AUDIO_COMMAND_ID_CHANGE_DEVICE_NAME;
    
    unsigned char idx = COMMAND_HEADER_SIZE;
    if ([deviceName length] > 32)
        [deviceName getBytes:&buf[idx] length:32];
    else
        [deviceName getBytes:&buf[idx]];
    idx += 32;
    header->length_high = 0x00;
    header->length_low = idx - COMMAND_HEADER_SIZE + 1;
    buf[idx] = [self calculateChecksum:&buf[1] dataLength:idx-1];
    
    [self sendCommand:buf length:idx+1];
    //    NSMutableData *data = [[NSMutableData alloc] initWithBytes:buf length:idx+1];
    //    [dataPath writeData:data];
    //    [data release];
}

- (void)DSPControlShortCommand:(unsigned char)cmd {
    unsigned char buf[25];
    memset(buf, 0x00, sizeof(buf));
    AUDIO_COMMAND_HEADER *header = (AUDIO_COMMAND_HEADER *)buf;
    header->start_byte = START_BYTE;
    header->commandID = AUDIO_COMMAND_ID_DSP_CONTROL;
    unsigned char idx = COMMAND_HEADER_SIZE;
    
    //    buf[idx++] = 0x00; //DSP short command
    buf[idx++] = cmd;
    
    header->length_high = 0x00;
    header->length_low = idx - COMMAND_HEADER_SIZE + 1;
    buf[idx] = [self calculateChecksum:&buf[1] dataLength:idx-1];
    _dspParamRecord = cmd;
    [self sendCommand:buf length:idx+1];
}

- (void)EQModeControlCommand:(uint8_t) mode {
    unsigned char buf[25];
    memset(buf, 0x00, sizeof(buf));
    AUDIO_COMMAND_HEADER *header = (AUDIO_COMMAND_HEADER *)buf;
    header->start_byte = START_BYTE;
    header->commandID = AUDIO_COMMAND_ID_EQ_MODE_SETTING;
    unsigned char idx = COMMAND_HEADER_SIZE;
    
    buf[idx++] = mode;
    //   buf[idx++] = 0x00;
    //   buf[idx++] = 0x00;
    
    header->length_high = 0x00;
    header->length_low = idx - COMMAND_HEADER_SIZE + 1;
    buf[idx] = [self calculateChecksum:&buf[1] dataLength:idx-1];
    didChangeEqMode = FALSE;
    [self sendCommand:buf length:idx+1];
}

- (void)setNoiseCancellationTxValue:(BOOL)bEnable {
    if (bEnable)
        [self DSPControlShortCommand:DSP_COMMAND_TXNR_ENABLE];
    else
        [self DSPControlShortCommand:DSP_COMMAND_TXNR_DISABLE];
    
}

- (void)setNoiseCancellationRxValue:(BOOL)bEnable {
    if (bEnable)
        [self DSPControlShortCommand:DSP_COMMAND_RXNR_ENABLE];
    else
        [self DSPControlShortCommand:DSP_COMMAND_RXNR_DISABLE];
}

- (void)setEqualizerMode: (uint8_t)equalizerMode {
    [self EQModeControlCommand:equalizerMode];
}

- (void)getFirmwareVersion {
    unsigned char buf[25];
    memset(buf, 0x00, sizeof(buf));
    AUDIO_COMMAND_HEADER *header = (AUDIO_COMMAND_HEADER *)buf;
    header->start_byte = START_BYTE;
    header->commandID = AUDIO_COMMAND_ID_GET_VERSION;
    
    unsigned char idx = COMMAND_HEADER_SIZE;
    buf[idx++] = 0x01; //read f/w version type
    header->length_high = 0x00;
    header->length_low = idx - COMMAND_HEADER_SIZE + 1;
    buf[idx] = [self calculateChecksum:&buf[1] dataLength:idx-1];
    
    [self sendCommand:buf length:idx+1];
}

- (void)gpioControl:(NSArray *)control {
    NSLog(@"[ISAudioControlManager] gpioControl");
    uint8_t buf[25];
    memset(buf, 0x00, sizeof(buf));
    GPIO_COMMAND_BODY *body = (GPIO_COMMAND_BODY *)buf;
    body->header.start_byte = START_BYTE;
    body->header.commandID = AUDIO_COMMAND_ID_GPIO_CONTROL;
    body->mask[0] = body->mask[1] = body->mask[2] = body->mask[3] = 0xff;
    uint8_t byte_idx = 0;
    uint8_t bit_idx = 0;
    ISGPIOControlProperty *ioProperty = nil;
    for (uint8_t i = 0; i < [control count]; i++) {
        ioProperty = [control objectAtIndex:i];
        //NSLog(@"[ISAudioControlManager] gpioControl inOut=%d, value=%d, index=%d", ioProperty.inOut, ioProperty.value, ioProperty.index);
        byte_idx = ioProperty.index/8;
        bit_idx = ioProperty.index%8;
        body->mask[byte_idx] &= ~(0x01 << bit_idx);
        if (ioProperty.inOut == TRUE) {
            body->inOut[byte_idx] |= 0x01 << bit_idx;
            if (ioProperty.value)
                body->value[byte_idx] |= 0x01 << bit_idx;
        }
    }
    
    uint8_t idx = sizeof(GPIO_COMMAND_BODY);
    body->header.length_high = 0x00;
    body->header.length_low = idx - COMMAND_HEADER_SIZE + 1;
    buf[idx] = [self calculateChecksum:&buf[1] dataLength:idx-1];
    
    [self sendCommand:buf length:idx+1];
}

- (void)sendTTSData:(NSData *)data priority:(int)priority{
    if (data) {
        @synchronized (self) {
            TTS_cancel = NO;
        }
        _tts_data = [data retain];
        unsigned char buf[25];
        memset(buf, 0x00, sizeof(buf));
        AUDIO_COMMAND_HEADER *header = (AUDIO_COMMAND_HEADER *)buf;
        header->start_byte = START_BYTE;
        header->commandID = AUDIO_COMMAND_ID_TTS;
        
        unsigned char idx = COMMAND_HEADER_SIZE;
        _tts_type = TTS_COMMAND_INSTRUCTION;
        if (priority == 1) {
            _tts_ins_type = TTS_INSTRUCTION_PARAMETER_HIGH;
        }
        else if (priority == 2) {
            _tts_ins_type = TTS_INSTRUCTION_PARAMETER_LOW;
        }
        else {
            _tts_ins_type = TTS_INSTRUCTION_PARAMETER_LOW;
        }
        buf[idx++] = _tts_type;
        buf[idx++] = _tts_ins_type;
        header->length_high = 0x00;
        header->length_low = idx - COMMAND_HEADER_SIZE + 1;
        buf[idx] = [self calculateChecksum:&buf[1] dataLength:idx-1];
        
        [self sendCommand:buf length:idx+1];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ttsState" object:@"TTS_COMMAND_INSTRUCTION"];
    }
}
- (BOOL)canSendTTS {
    if ([tts_buffer count]>0) {
        return NO;
    }
    if (_tts_data) {
        return NO;
    }
    return YES;
}

#pragma mark - MFiDataPathDelegate
- (void)accessoryDidDisconnect {
    NSLog(@"[AudioControl] accessoryDidDisconnect");
    _managerState = ISAudioControlManagerStateNotConnected;
    _tts_data = nil;
    [tts_buffer removeAllObjects];
    if (delegate && [(NSObject *)delegate respondsToSelector:@selector(ISAudiocontrolManager:didUpdateState:)])
        [delegate ISAudiocontrolManager:self didUpdateState:_managerState];
}

- (void)accessoryDidConnect {
    NSLog(@"[AudioControl] accessoryDidConnect");

}

- (void)MFiDataReceived {
    
}

- (void)parseEvents {
    unsigned char start = 0x00;
    
    NSRange range = NSMakeRange(0, 1);
    [_events getBytes:&start length:1];
    
    while (start != START_BYTE && [_events length]) {
        [_events replaceBytesInRange:range withBytes:NULL length:0];
        [_events getBytes:&start length:1];
    }
    if ([_events length] < COMMAND_HEADER_SIZE)
    {
        NSLog(@"[parseEvents] %d return1", [_events length]);
        return;
    }
    char buf[10];
    [_events getBytes:buf length:COMMAND_HEADER_SIZE];
    AUDIO_COMMAND_HEADER *header = (AUDIO_COMMAND_HEADER *)buf;
    short payloadLen = (header->length_high << 8 | header->length_low);
    short eventLen = payloadLen + COMMAND_HEADER_SIZE;
    //check if event is complete
    if ([_events length] < payloadLen +2) //+ start byte AA and checksum
    {
        NSLog(@"[parseEvents] return2");
        return;
    }
    
    unsigned char *event = malloc(eventLen);
    [_events getBytes:event length:eventLen];
    unsigned char checksum = [self calculateChecksum:event+1 dataLength:eventLen-2]; //minus start_byte and checksum
    range = NSMakeRange(0, eventLen);
    [_events replaceBytesInRange:range withBytes:NULL length:0];
    if (checksum != event[eventLen-1]) {
        
        NSLog(@"checksum error");
        return;
    }
    NSError *error = nil;
    if (header->commandID != AUDIO_EVENT_ID_ACK) {
        [self returnEventAck:header->commandID];
    }
    switch (header->commandID) {
        case AUDIO_EVENT_ID_ACK:
        {
            uint8_t cmdID = event[COMMAND_HEADER_SIZE];
            uint8_t status = event[COMMAND_HEADER_SIZE+1];
            NSLog(@"received ACK, cmdID = 0x%02x, status = 0x%02x",cmdID, status);
            BOOL ack = [self removeCheckResponseTimerForCommand:cmdID];
            if (status)
                error = [[[NSError alloc] initWithDomain:@"execute command fail" code:status userInfo:nil] autorelease];
            switch (cmdID) {
                case AUDIO_COMMAND_ID_EVENT_MSASK_SETTING:
                    if (ack && delegate && [(NSObject *)delegate respondsToSelector:@selector(ISAudioControlManager:didSetEventMaskSetting:)])
                        [delegate ISAudioControlManager:self didSetEventMaskSetting:error];
                    break;
                case AUDIO_COMMAND_ID_DSP_CONTROL:
                    switch (_dspParamRecord) {
                        case DSP_COMMAND_RXNR_DISABLE:
                        case DSP_COMMAND_RXNR_ENABLE:
                            if (ack && delegate && [(NSObject *)delegate respondsToSelector:@selector(ISAudioControlManager:didSetNoiseCancellationRxValue:)])
                                [delegate ISAudioControlManager:self didSetNoiseCancellationRxValue:error];
                            break;
                        case DSP_COMMAND_TXNR_DISABLE:
                        case DSP_COMMAND_TXNR_ENABLE:
                            if (ack && delegate && [(NSObject *)delegate respondsToSelector:@selector(ISAudioControlManager:didSetNoiseCancellationTxValue:)])
                                [delegate ISAudioControlManager:self didSetNoiseCancellationTxValue:error];
                            break;
                        default:
                            break;
                    }
                    
                    break;
                case AUDIO_COMMAND_ID_EQ_MODE_SETTING:
                    didChangeEqMode = TRUE;
                    
                    break;
                case AUDIO_COMMAND_ID_CHANGE_DEVICE_NAME:
                {
                    
                    if (ack && delegate && [(NSObject *)delegate respondsToSelector:@selector(ISAudioControlManager:didChangeDeviceName:)]) {
                        [delegate ISAudioControlManager:self didChangeDeviceName:error];
                    }
                }
                    break;
                case AUDIO_COMMAND_ID_TTS:
                {
                    if (_tts_type == TTS_COMMAND_INSTRUCTION) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"ttsState" object:[NSString stringWithFormat:@"event TTS_COMMAND_INSTRUCTION p=%d",_tts_ins_type]];
                        if (_tts_ins_type == TTS_INSTRUCTION_PARAMETER_STOP) {
                            Clear_TTS_Buffer = NO;
                            TTS_cancel = NO;
                            [_tts_data release];
                            _tts_data = nil;
                            _tts_data_index = 0;
                        }
                        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(clear) object:nil];
                        return;
                    }
                    else {         //TTS_COMMAND_VOICE_DATA
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"ttsState" object:[NSString stringWithFormat:@"event TTS_VOICE_DATA,status = %d",status]];
                        if (status != 0) {
                            //[self performSelector:@selector(clearTTSBuffer) withObject:nil afterDelay:3.0];
                            if (status == 1 /*&& TTS_cancel*/) {
                                @synchronized (self) {
                                    if (!Clear_TTS_Buffer) {
                                        //[self clearTTSBuffer];
                                    }
                                    else {
                                        Clear_TTS_Buffer = NO;
                                    }
                                    if ([tts_buffer count]>0) {
                                        [tts_buffer removeAllObjects];
                                    }
                                    TTS_cancel = NO;
                                    [_tts_data release];
                                    _tts_data = nil;
                                    _tts_data_index = 0;
                                }
                            }
                            else if  (status == 4) {
                                [self performSelector:@selector(clear) withObject:nil afterDelay:3.0];
                            }
                            return;
                        }
                        else {
                            [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(clear) object:nil];
                            switch (_tts_data_type) {
                                case TTS_VOICE_DATA_PARAMETER_CANCEL: {
                                    @synchronized (self) {
                                        TTS_cancel = NO;
                                    }
                                    Clear_TTS_Buffer = NO;
                                }
                                    break;
                                case TTS_VOICE_DATA_PARAMETER_SINGLE:
                                case TTS_VOICE_DATA_PARAMETER_END: {
                                    [_tts_data release];
                                    _tts_data = nil;
                                    _tts_data_index = 0;
                                    if (delegate && [delegate respondsToSelector:@selector(ISAudioControlManager:didSendTTSData:)]) {
                                        [delegate ISAudioControlManager:self didSendTTSData:error];
                                    }
                                    if (TTS_cancel) {
                                        /*_tts_data_type = TTS_VOICE_DATA_PARAMETER_CANCEL;
                                        _tts_type = TTS_COMMAND_VOICE_DATA;
                                        unsigned char buf[480+COMMAND_HEADER_SIZE+1];
                                        memset(buf, 0x00, sizeof(buf));
                                        AUDIO_COMMAND_HEADER *header = (AUDIO_COMMAND_HEADER *)buf;
                                        header->start_byte = START_BYTE;
                                        header->commandID = AUDIO_COMMAND_ID_TTS;
                                        unsigned int idx = COMMAND_HEADER_SIZE;
                                        buf[idx++] = _tts_type;
                                        buf[idx++] = _tts_data_type;
                                        header->length_high = 0x00;
                                        header->length_low = idx - COMMAND_HEADER_SIZE + 1;
                                        buf[idx] = [self calculateChecksum:&buf[1] dataLength:idx-1];
                                        [self sendCommand:buf length:idx+1];*/
                                        [self clearTTSBuffer];
                                    }
                                }
                                    break;
                                case TTS_VOICE_DATA_PARAMETER_START:
                                case TTS_VOICE_DATA_PARAMETER_CONT: {
                                    int start_index = _tts_data_index;
                                    if (TTS_cancel) {
                                        start_index = [_tts_data length] -1;
                                        _tts_data_type = TTS_VOICE_DATA_PARAMETER_END;
                                        _tts_data_index = [_tts_data length] -1;
                                    }
                                    else if ([_tts_data length]-_tts_data_index> 480) {
                                        _tts_data_type = TTS_VOICE_DATA_PARAMETER_CONT;
                                        _tts_data_index = _tts_data_index+480;
                                    }
                                    else if ([tts_buffer count]>0){
                                        _tts_data_type = TTS_VOICE_DATA_PARAMETER_CONT;
                                        _tts_data_index = [_tts_data length];
                                    }
                                    else {
                                        _tts_data_type = TTS_VOICE_DATA_PARAMETER_END;
                                        _tts_data_index = [_tts_data length];
                                    }
                                    
                                    _tts_type = TTS_COMMAND_VOICE_DATA;
                                    unsigned char buf[480+COMMAND_HEADER_SIZE+1];
                                    memset(buf, 0x00, sizeof(buf));
                                    AUDIO_COMMAND_HEADER *header = (AUDIO_COMMAND_HEADER *)buf;
                                    header->start_byte = START_BYTE;
                                    header->commandID = AUDIO_COMMAND_ID_TTS;
                                    
                                    unsigned int idx = COMMAND_HEADER_SIZE;
                                    buf[idx++] = _tts_type;
                                    buf[idx++] = _tts_data_type;
                                    [_tts_data getBytes:&buf[idx] range:NSMakeRange(start_index, _tts_data_index-start_index)];
                                    idx = idx + (_tts_data_index-start_index);
                                    header->length_high = (unsigned char)((idx - COMMAND_HEADER_SIZE + 1)>>8);
                                    header->length_low = (unsigned char)((idx - COMMAND_HEADER_SIZE + 1)&0x00ff);
                                    buf[idx] = [self calculateChecksum:&buf[1] dataLength:idx-1];
                                    NSLog(@"start_index= %d _tts_data_index = %d idx = %02x",start_index,_tts_data_index,idx);
                                    [self sendCommand:buf length:idx+1];
                                    if ([tts_buffer count]>0) {
                                        [_tts_data release];
                                        _tts_data = [[tts_buffer objectAtIndex:0] retain];
                                        _tts_data_index = 0;
                                        [tts_buffer removeObjectAtIndex:0];
                                    }
                                }
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
                    break;
                default:
                    break;
            }
            
            break;
        }
        case AUDIO_EVENT_ID_VERSION_REPLY:
        {
            NSString *version = [NSString stringWithFormat:@"%d.%d%d", event[COMMAND_HEADER_SIZE+1], event[COMMAND_HEADER_SIZE+2]>>4, event[COMMAND_HEADER_SIZE+2]&0x0f];
            NSLog(@"received version, type=%02x, version=%@",event[COMMAND_HEADER_SIZE], version);
            
            if (delegate && [(NSObject *)delegate respondsToSelector:@selector(ISAudioControlManager:didGetFirmwareVersion:error:)])
                [delegate ISAudioControlManager:self didGetFirmwareVersion:version error:nil];
            
            /*   UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"received Version event" message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
             [alert show];
             [alert release];*/
            break;
        }
        case AUDIO_EVENT_ID_CALLER_ID:
        {
            /*
            __block id week_self = self;
            if ([tts_buffer count]>0) {
                [tts_buffer removeAllObjects];
            }
            dispatch_async(dispatch_queue_create("call", NULL), ^{
                NSData *data = [NSData dataWithBytesNoCopy:event length:eventLen];
                NSData *callerData = [data subdataWithRange:NSMakeRange(COMMAND_HEADER_SIZE+1, eventLen - COMMAND_HEADER_SIZE-2)];
                NSString *caller = [[[NSString alloc] initWithData:callerData encoding:NSUTF8StringEncoding] autorelease];
                NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
                NSArray *stringArray = [caller componentsSeparatedByString:@" "];
                NSString *file = [NSString stringWithFormat:@"G_%@",[@"Call from" MD5String]];
                NSData *t_data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:file ofType:@""]];
                [tts_buffer addObject:t_data];
                for (NSString *sub in stringArray) {
                    //NSString *sub = [caller substringWithRange:NSMakeRange(i, 1)];
                    if ([sub rangeOfCharacterFromSet:notDigits].location == NSNotFound) {
                        for (int i = 0 ; i < sub.length; i++) {
                            NSString *n_file = [NSString stringWithFormat:@"G_%@",[[sub substringWithRange:NSMakeRange(i, 1)] MD5String]];
                            t_data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:n_file ofType:@""]];
                            [tts_buffer addObject:t_data];
                        }
                    }
                    else {
                        NSArray *cach = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
                        NSString *cache = [cach objectAtIndex:0];
                        NSString *file = [NSString stringWithFormat:@"G_%@",[sub MD5String]];
                        NSString *tempPath = [cache stringByAppendingPathComponent:file];
                        NSData *v_data = [NSData dataWithContentsOfFile:tempPath];
                        if (v_data) {
                            [tts_buffer addObject:v_data];
                        }
                        else {
                            send = NO;
                            NSLog(@"Start Synthesize");
                            [GoogleTTSAPI textToSpeechWithText:sub andLanguage:@"en" success:^(NSData *data){
                                //[MBProgressHUD hideHUDForView:self.view animated:YES];
                                NSLog(@"End Synthesize");
                                dispatch_async(dispatch_queue_create("temp", NULL), ^{
                                    NSLog(@"Start Mp3ToWav");
                                    [TTMp3ToWav processFile:data completeBlock:^(NSString *data){
                                        NSLog(@"End Mp3ToWav");
                                        [Fixsimc processFile:data completeBlock:^(NSData *p_data){
                                            //NSLog(@"%@",[data description]);
                                            dispatch_async(dispatch_queue_create("temp", NULL), ^{
                                                [tts_buffer addObject:p_data];
                                                [p_data writeToFile:tempPath atomically:NO];
                                                send = YES;
                                            });
                                        }];
                                    }];
                                });
                            } failure:^(NSError *error) {
                                send = YES;
                            }];
                            while (!send) { } // wait to send tts data;
                        }
                    }
                }
                NSLog(@"callerData = %@,callerID = %@ isMainThread = %@",[callerData description],caller,[NSThread isMainThread]?@"YES":@"NO");
                if (_tts_data) {
                    @synchronized (self) {
                        TTS_cancel = YES;
                    }
                }
                else {
                    @synchronized (self) {
                        TTS_cancel = NO;
                    }
                }
                dispatch_async(dispatch_queue_create("temp", NULL), ^{
                    while (TTS_cancel) { } // wait to send tts data;
                    NSData *temp_d =[tts_buffer objectAtIndex:0];
                    [tts_buffer removeObjectAtIndex:0];
                    [week_self sendTTSData:temp_d priority:1];
                });
                NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
                [def setObject:[NSString stringWithFormat:@"Call from %@",caller] forKey:@"lastTTS"];
                [def synchronize];
            });
             */
            break;
        }
        case AUDIO_EVENT_ID_EQ_MDOE:
            
            if (delegate && [(NSObject *)delegate respondsToSelector:@selector(ISAudioControlManager:didSetEqualizerMode:)])
                [delegate ISAudioControlManager:self didSetEqualizerMode:nil];
            break;
        case AUDIO_EVENT_ID_GPIO_STATUS:
            NSLog(@"[ISAudioControlManager] receive GPIO status event");
            ISGPIOControlProperty *ioProperty = nil;
            if (delegate && [(NSObject *)delegate respondsToSelector:@selector(ISAudioControlManager:didUpdateGPIOStatus:)]) {
                NSMutableArray *ioStatus = [[[NSMutableArray alloc] init] autorelease];
                GPIO_EVENT_BODY *gpio = (GPIO_EVENT_BODY *)event;
                for (uint8_t byte_idx = 0; byte_idx < 4; byte_idx++) {
                    for (uint8_t bit_idx = 0; bit_idx < 8; bit_idx++) {
                        if ((gpio->mask[byte_idx] & (0x01 << bit_idx)) == 0x00) {
                            ioProperty = [[[ISGPIOControlProperty alloc] init] autorelease];
                            ioProperty.index = 8*byte_idx + bit_idx;
                            if (gpio->value[byte_idx] & (0x01 << bit_idx))
                                ioProperty.value = TRUE;
                            else
                                ioProperty.value = FALSE;
                            [ioStatus addObject:ioProperty];
                        }
                    }
                }
                [delegate ISAudioControlManager:self didUpdateGPIOStatus:ioStatus];
            }
            break;
        case AUDIO_EVENT_ID_TTS: {
            /*
            uint8_t status = event[COMMAND_HEADER_SIZE];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ttsState" object:[NSString stringWithFormat:@"event AUDIO_EVENT_ID_TTS,status = %d",status]];
            if (status == 0x01) {
                _tts_data_index = 0;
                if ([_tts_data length]< 480) {
                    _tts_data_type = TTS_VOICE_DATA_PARAMETER_SINGLE;
                    _tts_data_index = [_tts_data length];
                }
                else {
                    _tts_data_type = TTS_VOICE_DATA_PARAMETER_START;
                    _tts_data_index = 480;
                }
                _tts_type = TTS_COMMAND_VOICE_DATA;
                unsigned char buf[480+COMMAND_HEADER_SIZE+1];
                memset(buf, 0x00, sizeof(buf));
                AUDIO_COMMAND_HEADER *header = (AUDIO_COMMAND_HEADER *)buf;
                header->start_byte = START_BYTE;
                header->commandID = AUDIO_COMMAND_ID_TTS;
                
                unsigned int idx = COMMAND_HEADER_SIZE;
                buf[idx++] = _tts_type;
                buf[idx++] = _tts_data_type;
                [_tts_data getBytes:&buf[idx] length:_tts_data_index];
                idx+=_tts_data_index;
                header->length_high = (unsigned char)((idx - COMMAND_HEADER_SIZE + 1)>>8);
                header->length_low = (unsigned char)((idx - COMMAND_HEADER_SIZE + 1)&0x00ff);
                buf[idx] = [self calculateChecksum:&buf[1] dataLength:idx-1];
                NSLog(@"_tts_data_index = %d idx = %02x",_tts_data_index,idx);
                [self sendCommand:buf length:idx+1];
                
            }
             */
        }
            break;
        default:
            NSLog(@"command id = 0x%02x", header->commandID);
            break;
    }
    if ([_events length] >= COMMAND_HEADER_SIZE)
        [self parseEvents];
}

- (void)playLastTTS {
    /*
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSString *lastTTS = [def objectForKey:@"lastTTS"];
    __block id week_self = self;
    // if TTS is speaking, we don't do anything
    if ([tts_buffer count]>0) {
        return;
    }
    if (_tts_data) {
        return;
    }
    if (lastTTS) {
        dispatch_async(dispatch_queue_create("call", NULL), ^{
            if ([lastTTS hasPrefix:@"Call from "]) {  //begin with 'call from', the last TTS is a call. We need to get the caller ID
                NSString *caller = [lastTTS stringByReplacingOccurrencesOfString:@"Call from " withString:@""];
                NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
                NSArray *stringArray = [caller componentsSeparatedByString:@" "];
                NSString *file = [NSString stringWithFormat:@"G_%@",[@"Call from" MD5String]];
                NSData *t_data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:file ofType:@""]];
                [tts_buffer addObject:t_data];
                for (NSString *sub in stringArray) {
                    if ([sub rangeOfCharacterFromSet:notDigits].location == NSNotFound) {
                        for (int i = 0 ; i < sub.length; i++) {
                            NSString *n_file = [NSString stringWithFormat:@"G_%@",[[sub substringWithRange:NSMakeRange(i, 1)] MD5String]];
                            t_data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:n_file ofType:@""]];
                            [tts_buffer addObject:t_data];
                        }
                    }
                    else {
                        NSArray *cach = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
                        NSString *cache = [cach objectAtIndex:0];
                        NSString *file = [NSString stringWithFormat:@"G_%@",[sub MD5String]];
                        NSString *tempPath = [cache stringByAppendingPathComponent:file];
                        NSData *v_data = [NSData dataWithContentsOfFile:tempPath];
                        if (v_data) {
                            [tts_buffer addObject:v_data];
                        }
                        else {
                            // we don't need this part, the data must in cache
                        }
                    }
                }
            }
            else {  //the last TTS is a part of text, just speek it
                NSArray *cach = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
                NSString *cache = [cach objectAtIndex:0];
                NSString *file = [NSString stringWithFormat:@"G_%@",[lastTTS MD5String]];
                NSString *tempPath = [cache stringByAppendingPathComponent:file];
                NSData *v_data = [NSData dataWithContentsOfFile:tempPath];
                if (v_data) {
                    [tts_buffer addObject:v_data];
                }
                else {
                    // we don't need this part, the data must in cache
                }
                
            }
            dispatch_async(dispatch_queue_create("temp", NULL), ^{
                //while (TTS_cancel) { } // wait to send tts data;
                if ([tts_buffer count]>0) {
                    NSData *temp_d =[tts_buffer objectAtIndex:0];
                    [tts_buffer removeObjectAtIndex:0];
                    [week_self sendTTSData:temp_d priority:2];
                }
            });
            
        });
    }
     */
}

- (void)clearTTSBuffer {
    /*
    Clear_TTS_Buffer = YES;
    _tts_type = TTS_COMMAND_INSTRUCTION;
    _tts_ins_type = TTS_INSTRUCTION_PARAMETER_STOP;
    unsigned char buf[480+COMMAND_HEADER_SIZE+1];
    memset(buf, 0x00, sizeof(buf));
    AUDIO_COMMAND_HEADER *header = (AUDIO_COMMAND_HEADER *)buf;
    header->start_byte = START_BYTE;
    header->commandID = AUDIO_COMMAND_ID_TTS;
    unsigned int idx = COMMAND_HEADER_SIZE;
    buf[idx++] = _tts_type;
    buf[idx++] = _tts_ins_type;
    header->length_high = 0x00;
    header->length_low = idx - COMMAND_HEADER_SIZE + 1;
    buf[idx] = [self calculateChecksum:&buf[1] dataLength:idx-1];
    [self sendCommand:buf length:idx+1];
     */

}

- (void)clear {
    /*
    @synchronized (self) {
        if ([tts_buffer count]>0) {
            [tts_buffer removeAllObjects];
        }
        [self clearTTSBuffer];
        TTS_cancel = NO;
        [_tts_data release];
        _tts_data = nil;
        _tts_data_index = 0;
    }
     */
}

- (void)sendSupportTTS {
    /*
    unsigned char buf[480+COMMAND_HEADER_SIZE+1];
    memset(buf, 0x00, sizeof(buf));
    AUDIO_COMMAND_HEADER *header = (AUDIO_COMMAND_HEADER *)buf;
    header->start_byte = START_BYTE;
    header->commandID = AUDIO_COMMAND_ID_BTM_UTILITY_FUNCTION;
    unsigned int idx = COMMAND_HEADER_SIZE;
    buf[idx++] = 0x05;
    buf[idx++] = 0x01;
    header->length_high = 0x00;
    header->length_low = idx - COMMAND_HEADER_SIZE + 1;
    buf[idx] = [self calculateChecksum:&buf[1] dataLength:idx-1];
     */
}
@end
