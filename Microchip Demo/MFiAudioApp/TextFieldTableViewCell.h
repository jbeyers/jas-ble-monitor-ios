//
//  TextFieldTableViewCell.h
//  MFiAudioAPP
//
//  Created by D500 user on 13/4/26.
//  Copyright (c) 2013年 ISSC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextFieldTableViewCell : UITableViewCell<UITextFieldDelegate>
{
    
}
@property (retain, nonatomic) IBOutlet UILabel *itemLabel;
@property (retain, nonatomic) IBOutlet UITextField *textField;

@end
