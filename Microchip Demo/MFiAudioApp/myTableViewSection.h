//
//  myTableViewSection.h
//  MFiAudioAPP
//
//  Created by D500 user on 13/4/25.
//  Copyright (c) 2013年 ISSC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface myTableViewSection : NSObject

@property (retain) NSString *sectionTitle;
@property (retain) NSMutableArray *cells;
@end
