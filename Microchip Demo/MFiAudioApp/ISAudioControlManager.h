//
//  ISAudioControlManager.h
//  MFiAudioAPP
//
//  Created by D500 user on 13/5/10.
//  Copyright (c) 2013�?ISSC. All rights reserved.
//

#import <Foundation/Foundation.h>

#define iAP_PROTOCOL_STRING @"com.issc-tech.ISSCAudioWidget"
//#define iAP_PROTOCOL_STRING @"com.issc.datapath"
typedef enum {
    AUDIO_COMMAND_ID_EVENT_MSASK_SETTING  = 0x03,
    AUDIO_COMMAND_ID_CHANGE_DEVICE_NAME   = 0x05,
    AUDIO_COMMAND_ID_DSP_CONTROL          = 0x1D,
    AUDIO_COMMAND_ID_EQ_MODE_SETTING      = 0x1C,
    AUDIO_COMMAND_ID_GET_VERSION          = 0x08,
    AUDIO_COMMAND_ID_GPIO_CONTROL         = 0x1E,
    AUDIO_COMMAND_ID_BTM_UTILITY_FUNCTION = 0x13,
    AUDIO_COMMAND_ID_EVENT_ACK            = 0x14,
    AUDIO_COMMAND_ID_TTS                  = 0x20,
    AUDIO_COMMAND_ID_MMI                  = 0x02,
}AUDIO_COMMAND_ID;

typedef enum {
    AUDIO_EVENT_ID_ACK            = 0x00,
    AUDIO_EVENT_ID_CALLER_ID      = 0x03,
    AUDIO_EVENT_ID_VERSION_REPLY  = 0x18,
    AUDIO_EVENT_ID_EQ_MDOE        = 0x10,
    AUDIO_EVENT_ID_GPIO_STATUS    = 0x27,
    AUDIO_EVENT_ID_TTS            = 0x2A,
    AUDIO_EVENT_ID_SoundEffect_Status = 0x36,
}AUDIO_EVENT_ID;

typedef enum {
    DSP_COMMAND_RXNR_DISABLE      = 0x19,  //= 0x09,
    DSP_COMMAND_RXNR_ENABLE       = 0x18, // = 0x0A,
    DSP_COMMAND_TXNR_DISABLE      = 0x1c, // = 0x0B,
    DSP_COMMAND_TXNR_ENABLE       = 0x1b, // = 0x0C,
}DSP_COMMAND;

typedef struct _AUDIO_COMMAND_HEADER {
    char start_byte;
    char length_high;
    char length_low;
    char commandID;
}__attribute__((packed)) AUDIO_COMMAND_HEADER;
#define COMMAND_HEADER_SIZE sizeof(AUDIO_COMMAND_HEADER)

typedef struct _GPIO_COMMAND_BODY {
    AUDIO_COMMAND_HEADER header;
    uint8_t mask[4];
    uint8_t inOut[4];
    uint8_t value[4];
}__attribute__((packed)) GPIO_COMMAND_BODY;

typedef struct _GPIO_EVENT_BODY {
    AUDIO_COMMAND_HEADER header;
    uint8_t mask[4];
    uint8_t value[4];
}__attribute__((packed)) GPIO_EVENT_BODY;

typedef enum {
    EQ_MODE_OFF         = 0x00,
    EQ_MODE_SOFT,
    EQ_MODE_BASS,
    EQ_MODE_TREBLE,
    EQ_MODE_POP,
    EQ_MODE_ROCK,
    EQ_MODE_CLASSIC,
    EQ_MODE_JAZZ,
    EQ_MODE_DANCE,
    EQ_MODE_RNB,
    EQ_MODE_USER1,
    EQ_MODE_USER2,
    EQ_MODE_INVALID = 0xFF
}EQ_MODE_ENUM;

typedef enum {
    ISAudioControlManagerStateNotConnected = 0,
    ISAudioControlManagerStateSessionFail,
    ISAudioControlManagerStateNormal,
    ISAudioControlManagerStateUnkonwError
}ISAudioControlManagerState;

typedef enum {
    TTS_COMMAND_INSTRUCTION       = 0x00,
    TTS_COMMAND_VOICE_DATA        = 0x01,
}TTS_COMMAND_TYPE;

typedef enum {
    TTS_VOICE_DATA_PARAMETER_SINGLE       = 0x00,
    TTS_VOICE_DATA_PARAMETER_START        = 0x01,
    TTS_VOICE_DATA_PARAMETER_CONT         = 0x02,
    TTS_VOICE_DATA_PARAMETER_END          = 0x03,
    TTS_VOICE_DATA_PARAMETER_CANCEL       = 0x04,
}TTS_VOICE_DATA_PARAMETER;

typedef enum {
    TTS_INSTRUCTION_PARAMETER_STOP        = 0x00,
    TTS_INSTRUCTION_PARAMETER_HIGH        = 0x01,
    TTS_INSTRUCTION_PARAMETER_LOW         = 0x02,
}TTS_INSTRUCTION_PARAMETER;

@class ISGPIOControlProperty;
@protocol ISAudioControlManagerDelegate;
@interface ISAudioControlManager : NSObject
{
@private
    ISAudioControlManagerState _managerState;
    NSMutableData *_events;
  //  uint8_t     _agendumEQMode;
    NSMutableArray *_waitingACK;
    uint8_t       _dspParamRecord;
    NSData        *_tts_data;
    TTS_COMMAND_TYPE _tts_type;
    TTS_INSTRUCTION_PARAMETER _tts_ins_type;
    TTS_VOICE_DATA_PARAMETER _tts_data_type;
    int           _tts_data_index;
}

@property (assign) id<ISAudioControlManagerDelegate> delegate;
@property (assign) id<ISAudioControlManagerDelegate> gpioControlDelegate;
@property (assign, readonly) ISAudioControlManagerState managerState;
@property (assign) BOOL didChangeEqMode;
- (void)parseEvents;
- (void)changeDeviceName: (NSData *)deviceName;
- (void)setNoiseCancellationTxValue:(BOOL)bEnable;
- (void)setNoiseCancellationRxValue:(BOOL)bEnable;
- (void)setEqualizerMode: (uint8_t)equalizerMode;
- (void)getFirmwareVersion;
- (void)gpioControl:(NSArray *)control;
- (void)setEventMaskSetting: (NSData *)maskTable; //maskTable is reserved for future
- (void)returnEventAck: (uint8_t)cmdID;
- (void)sendTTSData:(NSData *)data priority:(int)priority;
- (void)playLastTTS;
- (BOOL)canSendTTS;
- (void)sendSupportTTS;
@end

@protocol ISAudioControlManagerDelegate<NSObject>
@optional
- (void)ISAudiocontrolManager:(ISAudioControlManager *)manager didUpdateState:(ISAudioControlManagerState) state;
- (void)ISAudioControlManager:(ISAudioControlManager *)manager didChangeDeviceName:(NSError *)error;
- (void)ISAudioControlManager:(ISAudioControlManager *)manager didSetNoiseCancellationTxValue:(NSError *)error;
- (void)ISAudioControlManager:(ISAudioControlManager *)manager didSetNoiseCancellationRxValue:(NSError *)error;
- (void)ISAudioControlManager:(ISAudioControlManager *)manager didSetEqualizerMode:(NSError *)error;
- (void)ISAudioControlManager:(ISAudioControlManager *)manager didGetFirmwareVersion:(NSString *)version error:(NSError *)error;
- (void)ISAudioControlManager:(ISAudioControlManager *)manager didGPIOControl:(NSError *)error;
- (void)ISAudioControlManager:(ISAudioControlManager *)manager didUpdateGPIOStatus:(NSArray *)ioStatus;
- (void)ISAudioControlManager:(ISAudioControlManager *)manager reportErrorForcmdID:(uint8_t)cmdID error:(NSError *)error;
- (void)ISAudioControlManager:(ISAudioControlManager *)manager didSetEventMaskSetting:(NSError *)error;
- (void)ISAudioControlManager:(ISAudioControlManager *)manager didSendTTSData:(NSError *)error;

@end

@interface ISGPIOControlProperty : NSObject
@property (assign) BOOL inOut; //Output = TRUE, input = FALSE
@property (assign) BOOL value;
@property (assign) uint8_t index;
@end
