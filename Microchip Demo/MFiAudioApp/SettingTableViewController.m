//
//  SettingTableViewController.m
//  MFiAudioAPP
//
//  Created by D500 user on 13/4/24.
//  Copyright (c) 2013�?ISSC. All rights reserved.
//  v0.95 increase the length of device name to 32 bytes
//

#import "SettingTableViewController.h"
#import "ToggleSwitchTableViewCell.h"
#import "TextFieldTableViewCell.h"
#import "InformationTableViewCell.h"
#import "ButtonTableViewCell.h"
#import "myTableViewSection.h"
#import "myTableViewCell.h"
//#import "cellType.h"
#import "AppDelegate.h"
#import "MediaPlayer/MPVolumeView.h"
//#import "TTRootViewController.h"

#define zEQModeOff  @"OFF"
#define zEQModeSoft    @"Soft Mode"
#define zEQModeBass    @"Bass Mode"
#define zEQModeTreble   @"Treble Mode"
#define zEQModePop      @"Pop Mode"
#define zEQModeRock     @"Rock Mode"
#define zEQModeClassic  @"Classic Mode"
#define zEQModeJazz     @"Jazz Mode"
#define zEQModeDance    @"Dance Mode"
#define zEQModeRnB      @"R&B Mode"
#define zEQModeCustom   @"Custom Mode"
//#define zEQModeLiveMusic    @"Live Music"

#define zSettingItemDeviceName  @"Microchip Audio"
#define zSettingItemTXNR        @"NR Enable"
//#define zSettingItemRXNR        @"RX NR"
#define zSettingItemEqualizer   @"Equalizer"
#define zSettingItemGpioControl @"IO Control"
#define zSettingItemFwVersion   @"F/W Version"
#define zSettingItemRecord      @"Record"
#define zSettingItemFindAccessory @"Find My Accessory"

#define zPreferenceKeyDeviceName    @"Device Name For Preference"
#define zPreferenceKeyEqualizer     @"Equalizer For Preference"
#define zPreferenceKeyTXNR          @"TX NR For Preference"
#define zPreferenceKeyRXNR          @"RX NR For Preference"

//#define zGPIOLabel1     @"P0_1"
#define zGPIOLabel1     @"P0_4"     //GPIO Output
#define zGPIOLable2     @"P0_5"     //GPIO Input
//#define zGPIOLable3     @"P1_6"
#define zGPIOLable3     @"P2_4"     //GPIO Output
#define zGPIOLable4     @"P2_7"     //GPIO Input
//#define indexGPIO1      1
#define indexGPIO1      4
#define indexGPIO2      5
//#define indexGPIO3      14
#define indexGPIO3      20
#define indexGPIO4      23

enum {
    SETTING_ID_DEVICE_NAME  = 0x01,
    SETTING_ID_TXNR         = 0x02,
    SETTING_ID_RXNR         = 0x03,
    SETTING_ID_EQUALIZER    = 0x04,
    SETTING_ID_GPIO_CONTROL = 0x05,
    SETTING_ID_FW_VERSION   = 0x06,
    SETTING_ID_FIND_ACCESSORY,
    SETTING_ID_FIND_ACCESSORY_VOLUME,
    SETTING_ID_RECORD_VOICE,
    SETTING_ID_APP_VERSION,
    SETTING_ID_SERIAL_NUMBER,
    SETTING_ID_TTS,
    SETTING_ID_BUZZER,
    SETTING_ID_GPIO_CONTROL1,
    SETTING_ID_GPIO_CONTROL2,
    SETTING_ID_GPIO_CONTROL3,
};

@interface ACKTimer1 : NSObject
@property (retain) NSTimer *timer;
@property (assign) uint8_t commandID;
- (id)initWithCommandID:(uint8_t)cmdID timer:(NSTimer *)timer;
@end

@implementation ACKTimer1

- (id)initWithCommandID:(uint8_t)cmdID timer:(NSTimer *)timer {
    self = [super init];
    if (self) {
        self.timer = timer;
        self.commandID = cmdID;
    }
    return self;
}
@end

@interface SettingTableViewController ()
@end

@implementation SettingTableViewController
@synthesize dataSource;
@synthesize gpioList;
@synthesize gpioControlTableView;
@synthesize equalizerTableView;
@synthesize equalizerModeOptions;
@synthesize selectedEQMode;
//@synthesize audioControl;
@synthesize connectBarButtonItem;
@synthesize disconnectBarButtonItem;
#ifdef APP_DEMO_VERSION
@synthesize gpioViewController;
#endif
@synthesize recordViewController;
#if 1
@synthesize LE_DeviceListTableView;
@synthesize LE_DeviceList;
@synthesize CB_Initial;
@synthesize LE_DeviceName;
@synthesize SelectedLEDeviceListIndex;
@synthesize DeviceIsSelect;
@synthesize NRIsEnabled;
@synthesize SyncDeviceEQ;
@synthesize GPIOIndex;
@synthesize GPIOIndexDir;
@synthesize GPIOIndexValue;
#endif
@synthesize didChangeEqMode;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    LE_DeviceListTableView = [[UITableViewController alloc] initWithStyle:UITableViewStylePlain];
    //[LE_DeviceListTableView.tableView setBackgroundView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iSSC_background.png"]]];
    LE_DeviceListTableView.tableView.dataSource = self;
    LE_DeviceListTableView.tableView.delegate = self;
    LE_DeviceListTableView.title = @"Select Device";
    
    LE_DeviceList = [[NSMutableArray alloc] init];
    
    SelectedLEDeviceListIndex = 0;
    LE_DeviceName = nil;
    DeviceIsSelect = FALSE;
    
    _cbController = [[CBController alloc] init];
    [_cbController setDelegate:self];
    
    _events = [[NSMutableData alloc] init];
    
    [self performSelector:@selector(startScan) withObject:nil afterDelay:2.0];
    
    didChangeEqMode = TRUE;
    
    GPIOIndex = -1;
    GPIOIndexDir = YES;
    GPIOIndexValue = YES;
    
    _waitingACK = [[NSMutableArray alloc] init];
    _managerState = ISAudioControlManagerStateNotConnected;

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    //[self.tableView setBackgroundView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iSSC_background.png"]]];
    //[self.tableView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"iSSC_background.png"]]];
    
    UIImageView *connectImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Connect.png"]];
    UIImageView *disconnectImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Disconnect.png"]];
    CGRect frame = connectImageView.frame;
    frame.size.height = 20;
    frame.size.width = 20;
    connectImageView.frame = frame;
    frame = disconnectImageView.frame;
    frame.size.height = 20;
    frame.size.width = 20;
    disconnectImageView.frame = frame;
    UIButton *connectButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [connectButton setBackgroundImage:disconnectImageView.image forState:UIControlStateNormal];
    connectButton.frame = CGRectMake(0.0f, 0.0f, 20.0f, 20.0f);
    [connectButton addTarget: self action:@selector(accessoryPicker:) forControlEvents:UIControlEventTouchUpInside];
    
    //connectBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:connectImageView];
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_5_1) {
        disconnectBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:connectButton];
    }
    else {
        disconnectBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:disconnectImageView];
    }

    self.navigationItem.rightBarButtonItem = disconnectBarButtonItem;
    //self.navigationItem.leftBarButtonItem = connectBarButtonItem;
    
    dataSource = [[NSMutableArray alloc] init];
    myTableViewSection *section;
    NSMutableArray *cells;
    myTableViewCell *myCell;
    
    equalizerTableView = [[UITableViewController alloc] initWithStyle:UITableViewStylePlain];
    //[equalizerTableView.tableView setBackgroundView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iSSC_background.png"]]];
    equalizerTableView.tableView.dataSource = self;
    equalizerTableView.tableView.delegate = self;
    equalizerModeOptions = [[NSArray alloc] initWithObjects:zEQModeOff ,zEQModeSoft, zEQModeBass, zEQModeTreble,zEQModeClassic, zEQModeRock, zEQModeJazz, zEQModePop, zEQModeDance, zEQModeRnB, zEQModeCustom, nil];// zEQModeLiveMusic, nil];
    equalizerTableView.title = @"EQ";
    
    gpioControlTableView = [[UITableViewController alloc] initWithStyle:UITableViewStyleGrouped];
    //[gpioControlTableView.tableView setBackgroundView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iSSC_background.png"]]];
    gpioControlTableView.tableView.dataSource = self;
    gpioControlTableView.tableView.delegate = self;
    _applyGPIO = [[UIBarButtonItem alloc] initWithTitle:@"Apply" style:UIBarButtonItemStylePlain target:self action:@selector(applyGPIOChange)];
    gpioControlTableView.title = @"GPIO";
    
    recordViewController = nil;
    
    /*UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(200, 28, 57, 57)];
    [titleLabel setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Icon_old.png"]]];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    self.navigationItem.titleView = titleLabel;
    [titleLabel release];*/
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 100, 50)];
    UIImage *image = [UIImage imageNamed:@"microchiplogo.png"];
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 100, 50)];
    imgView.image = image;
    [view addSubview:imgView];
    self.navigationItem.titleView = view;
    [imgView release];
    [view release];
    

    NSString *deviceName = [[NSUserDefaults standardUserDefaults] stringForKey:zPreferenceKeyDeviceName];
    if (deviceName == nil)
        deviceName = zSettingItemDeviceName;
    
    //Modify device name
    section = [[myTableViewSection alloc] init];
    section.sectionTitle = @"Device Name";
    cells = [[NSMutableArray alloc] init];
    myCell = [[myTableViewCell alloc] init];
    [myCell setItemName:deviceName];
    [myCell setCellIdentifier:SETTING_ID_DEVICE_NAME];
    [cells addObject:myCell];
    [section setCells:cells];
    [dataSource addObject:section];
    
    //Noise Cancellation
    NRIsEnabled = [[NSUserDefaults standardUserDefaults] boolForKey:zPreferenceKeyTXNR];
    
    section = [[myTableViewSection alloc] init];
    section.sectionTitle = @"Noise Cancellation";
    cells = [[NSMutableArray alloc] init];
    myCell = [[myTableViewCell alloc] init];
    if(NRIsEnabled)
        [myCell setItemName:zSettingItemTXNR];
    else
        [myCell setItemName:@"NR Disable"];
    [myCell setEventSelector:@selector(noiseCancellationTxValueChange:)];
    [myCell setCellIdentifier:SETTING_ID_TXNR];
    [cells addObject:myCell];
    [section setCells:cells];
    [dataSource addObject:section];
    
    int eq = [[NSUserDefaults standardUserDefaults] integerForKey:zPreferenceKeyEqualizer];
    SyncDeviceEQ = eq;
   // if (eq != 0)
   //     eq -= 1;
    [self setSelectedEQMode:[NSIndexPath indexPathForRow:eq inSection:0]];//EQ_MODE_INVALID
    //Equalizer
    section = [[myTableViewSection alloc] init];
    section.sectionTitle = @"Equalizer";
    cells = [[NSMutableArray alloc] init];
    myCell = [[myTableViewCell alloc] init];
    [myCell setItemName:[equalizerModeOptions objectAtIndex:eq]];
    [myCell setCellIdentifier:SETTING_ID_EQUALIZER];
    [cells addObject:myCell];
    [section setCells:cells];
    [dataSource addObject:section];
    
    //GPIO Control
    section = [[myTableViewSection alloc] init];
    section.sectionTitle = @"IO Control";
    cells = [[NSMutableArray alloc] init];
    myCell = [[myTableViewCell alloc] init];
    [myCell setCellIdentifier:SETTING_ID_GPIO_CONTROL];
    [myCell setItemName:zSettingItemGpioControl];
    [cells addObject:myCell];
    //[section setCells:cells];
#ifdef ModifiedIO
    
    myCell = [[myTableViewCell alloc] init];
    [myCell setCellIdentifier:SETTING_ID_GPIO_CONTROL1];
    [myCell setItemName:@"IO Index(0~31)"];
    [cells addObject:myCell];
    //[section setCells:cells];
    
    myCell = [[myTableViewCell alloc] init];
    [myCell setCellIdentifier:SETTING_ID_GPIO_CONTROL2];
    [myCell setItemName:@"Output"];
    [myCell setEventSelector:@selector(IODirChange:)];
    [cells addObject:myCell];
    //[section setCells:cells];
    
    myCell = [[myTableViewCell alloc] init];
    [myCell setCellIdentifier:SETTING_ID_GPIO_CONTROL3];
    [myCell setItemName:@"High"];
    [myCell setEventSelector:@selector(IOValueChange:)];
    [cells addObject:myCell];
    [section setCells:cells];
#endif
    [dataSource addObject:section];
    
    findAccessoryTones = [[NSArray alloc] initWithObjects:@"Tone1", @"Tone2", @"Tone3", @"Tone4", nil];
    //Find Accessory
    section = [[myTableViewSection alloc] init];
    section.sectionTitle = zSettingItemFindAccessory;
    cells = [[NSMutableArray alloc] init];
    myCell = [[myTableViewCell alloc] init];
    [myCell setItemName:[findAccessoryTones objectAtIndex:0]];
    [myCell setCellIdentifier:SETTING_ID_FIND_ACCESSORY];
    [myCell setEventSelector:@selector(ringFindAccessoryTone:)];
    [cells addObject:myCell];
    myCell = [[myTableViewCell alloc] init];
    [myCell setItemName:@"Vol"];
    [myCell setCellIdentifier:SETTING_ID_FIND_ACCESSORY_VOLUME];
    [cells addObject:myCell];
    [section setCells:cells];
    [dataSource addObject:section];
    
    //Record voice
    section = [[myTableViewSection alloc] init];
    section.sectionTitle = @"Record/Playback";
    cells = [[NSMutableArray alloc] init];
    myCell = [[myTableViewCell alloc] init];
    [myCell setItemName:zSettingItemRecord];
    [myCell setCellIdentifier:SETTING_ID_RECORD_VOICE];
    [cells addObject:myCell];
    [section setCells:cells];
    [dataSource addObject:section];
    
    //Buzzer Alert
    section = [[myTableViewSection alloc] init];
    section.sectionTitle = @"Buzzer";
    cells = [[NSMutableArray alloc] init];
    myCell = [[myTableViewCell alloc] init];
    [myCell setItemName:@"Buzzer Alert"];
    [myCell setCellIdentifier:SETTING_ID_BUZZER];
    //[myCell setEventSelector:@selector(BuzzerAlertStateChange:)];
    [myCell setEventSelector:@selector(ForceBuzzerAlert:)];
    [cells addObject:myCell];
    [section setCells:cells];
    [dataSource addObject:section];
    
    //Firmware version
    section = [[myTableViewSection alloc] init];
    section.sectionTitle = [[NSString alloc] initWithString:@"Information"];
    cells = [[NSMutableArray alloc] init];
    myCell = [[myTableViewCell alloc] init];
    [myCell setItemName:zSettingItemFwVersion];
    [myCell setCellIdentifier:SETTING_ID_FW_VERSION];
    [cells addObject:myCell];

    myCell = [[myTableViewCell alloc] init];
    [myCell setItemName:@"APP Version"];
    [myCell setCellIdentifier:SETTING_ID_APP_VERSION];
    [cells addObject:myCell];
    [section setCells:cells];
    
    myCell = [[myTableViewCell alloc] init];
    [myCell setItemName:@"Serial Number"];
    [myCell setCellIdentifier:SETTING_ID_SERIAL_NUMBER];
    [cells addObject:myCell];
    [section setCells:cells];
    [dataSource addObject:section];
    
#if 0
    section = [[myTableViewSection alloc] init];
    section.sectionTitle = @"SPP-TTS";
    cells = [[NSMutableArray alloc] init];
    myCell = [[myTableViewCell alloc] init];
    [myCell setItemName:@"TTS"];
    [myCell setCellIdentifier:SETTING_ID_TTS];
    [cells addObject:myCell];
    [section setCells:cells];
    [dataSource addObject:section];
#endif
    
    [cells release];
    [myCell release];
    [section release];
    
    gpioList = [[NSMutableArray alloc] init];
    //GPIO List
    NSString *str[] = {zGPIOLabel1, zGPIOLable2, zGPIOLable3, zGPIOLable4};
    for (int i=0; i<4; i++) {
        section = [[myTableViewSection alloc] init];
        section.sectionTitle = str[i];
        
        cells = [[NSMutableArray alloc] init];
        myCell = [[myTableViewCell alloc] init];
        [myCell setItemName:@"Direction"];
        [myCell setEventSelector:@selector(gpioDirectionChange:)];
        myCell.cellBody = (ToggleSwitchTableViewCell*) [[[NSBundle mainBundle] loadNibNamed:@"ToggleSwitchTableViewCell" owner:self options:nil] objectAtIndex:0];
        [[(ToggleSwitchTableViewCell*)myCell.cellBody itemLabel] setText:myCell.itemName];
        [[(ToggleSwitchTableViewCell*)myCell.cellBody stateSwitch] addTarget:self action:myCell.eventSelector forControlEvents:UIControlEventValueChanged];
        [(ToggleSwitchTableViewCell*)myCell.cellBody stateSwitch].enabled = NO;
        if(i == 0 || i == 2)
            [(ToggleSwitchTableViewCell*)myCell.cellBody stateSwitch].on = YES;//Set switch off(Output)
        else
            [(ToggleSwitchTableViewCell*)myCell.cellBody stateSwitch].on = NO;//Set switch off(Intput)
        [cells addObject:myCell];
        
        myCell = [[myTableViewCell alloc] init];
        [myCell setItemName:@"Value"];
        [myCell setEventSelector:@selector(gpioValueChange:)];
        myCell.cellBody = (ToggleSwitchTableViewCell*) [[[NSBundle mainBundle] loadNibNamed:@"ToggleSwitchTableViewCell" owner:self options:nil] objectAtIndex:0];
        [[(ToggleSwitchTableViewCell*)myCell.cellBody itemLabel] setText:myCell.itemName];
        [[(ToggleSwitchTableViewCell*)myCell.cellBody stateSwitch] addTarget:self action:myCell.eventSelector forControlEvents:UIControlEventValueChanged];
        [(ToggleSwitchTableViewCell*)myCell.cellBody stateSwitch].on = YES;
        if(i == 1 || i == 3)
            [(ToggleSwitchTableViewCell*)myCell.cellBody stateSwitch].enabled = NO;
        [cells addObject:myCell];
        
        [section setCells:cells];
        [gpioList addObject:section];
    }
    
    fwVersion = [[NSString alloc] initWithString:@"N/A"];
    
#ifdef APP_DEMO_VERSION
    gpioViewController = [[GPIODemoViewController alloc] initWithNibName:@"GPIODemoViewController" bundle:nil];
    //   gpioViewController.navigationItem.titleView = titleLabel;
#endif
   
    //audioControl = [[ISAudioControlManager alloc] init];
    //[audioControl setDelegate:self];
    
    findAccessoryPlayer = nil;
    
    /*UIBarButtonItem *left = [[UIBarButtonItem alloc] initWithTitle:@"AccessoryPicker" style:UIBarButtonItemStyleBordered target:self action:@selector(accessoryPicker:)];
    self.navigationItem.leftBarButtonItem = left;
    [left release];*/
    
    //self.title = @"AudioWidget";
    
    /*UIView *customView = [[UIView alloc]initWithFrame:CGRectMake(0,0,70,24)];
    UIImageView *imgView1 = [[UIImageView alloc]initWithFrame:CGRectMake(-7,1,14,22)];
    imgView1.image = [UIImage imageNamed:@"back.png"];
    [customView addSubview:imgView1];
    UIButton *urButton = [UIButton buttonWithType:UIButtonTypeCustom];
    urButton.frame = CGRectMake(0,0,65,24);
    [urButton setTitle:@"Back" forState:UIControlStateNormal];
    [urButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [urButton addTarget:self action:@selector(backClicked:)
       forControlEvents:UIControlEventTouchUpInside];
    [customView addSubview:urButton];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]initWithCustomView:customView];
    self.navigationItem.leftBarButtonItem = backButton;
    [customView release];
    [backButton release];*/
    
}

- (void)viewDidAppear:(BOOL)animated {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [[appDelegate navigationController] setToolbarHidden:YES animated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    if (findAccessoryPlayer.isPlaying)
        [self ringFindAccessoryTone:nil];
    if (recordViewController)
        [recordViewController release];
    [findAccessoryTones release];
    [dataSource removeAllObjects];
    [dataSource release];
    [gpioList removeAllObjects];
    [gpioList release];
    [gpioControlTableView release];
    [equalizerModeOptions release];
    [equalizerTableView release];
    [connectBarButtonItem release];
    [disconnectBarButtonItem release];
    [_applyGPIO release];
#ifdef APP_DEMO_VERSION
    [gpioViewController release];
#endif
    [LE_DeviceList release];
    [LE_DeviceListTableView release];
    [_cbController release];
    [super dealloc];
}

- (void)PressConnectButton:(id)sender{
    
    [_cbController disconnectDevice:controlPeripheral];
}

- (void)ShowDeviceList{
    
    //if([_cbController.devicesList count] != 0)
    //{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (![self.navigationController.viewControllers containsObject:LE_DeviceListTableView] && self.navigationController.viewControllers.count > 0)
        [[appDelegate navigationController] pushViewController:LE_DeviceListTableView animated:YES];
    //}
}

- (void)accessoryPicker:(id)sender {
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(ShowDeviceList) object:nil];
    if(_cbController.isScanning == YES)
    {
        
        if(DeviceIsSelect == FALSE)
        {
            
            [_cbController stopScan];
            
            [_cbController startScanWithUUID:nil];
            
        }
     }
    else{
        //NSLog(@"Refresh DeviceList");
        //[_cbController stopScan];
        [_cbController startScanWithUUID:nil];
    }
    
    //if(([_cbController.devicesList count]) >= 1)
    //{
    //    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //    [[appDelegate navigationController] pushViewController:LE_DeviceListTableView animated:YES];
    //}
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if([[appDelegate navigationController].topViewController.title isEqualToString:@"Select Device"])
    {
        return;
    }
    else{
        [self performSelector:@selector(ShowDeviceList) withObject:nil afterDelay:2.5];
        
    }
}

- (UITableViewCell *)retrieveCellForIdentifier:(uint8_t)cellIdentifier {
    for (uint8_t i = 0; i < [dataSource count]; i++) {
        myTableViewSection *mySection = [dataSource objectAtIndex:i];
        for (uint8_t cellIdx = 0; cellIdx < [mySection.cells count]; cellIdx++) {
            myTableViewCell *cell = [mySection.cells objectAtIndex:cellIdx];
            if (cell.cellIdentifier == cellIdentifier) {
                return cell.cellBody;
            }
        }
    }
    return nil;
}

- (void)ForceBuzzerAlert:(id)sender{
    //NSLog(@"ForceBuzzerAlert");
    
    if(_managerState == ISAudioControlManagerStateNormal)
    {
        //Force buzzer alert
        [self MMICommand:0x00 Para2:0x79];
    }
    else{
        [self SimpleAlert:@"Disconnected" message:@"Device not found!"];
    }
    
}

- (void)BuzzerAlertStateChange:(id)sender {
    
    //UISwitch *tmp = (UISwitch *)sender;
    //NSLog(@"BuzzerAlaertState,%d",tmp.on);
    
    if(_managerState == ISAudioControlManagerStateNormal)
    {
        //Force buzzer alert
        [self MMICommand:0x00 Para2:0x79];
    }
    else{
        [self SimpleAlert:@"Disconnected" message:@"Device not found!"];
    }
}

- (void)noiseCancellationTxValueChange:(id)sender {
    UISwitch *tmp = (UISwitch *)sender;
    
    //NSLog(@"NoiseCancellationTxValueChange, %d", tmp.on);
    //[audioControl setNoiseCancellationTxValue:tmp.on];
    
    if (tmp.on)
    {
        [self DSPControlShortCommand:DSP_COMMAND_TXNR_ENABLE];
        NRIsEnabled = TRUE;
    }
    else
    {
        [self DSPControlShortCommand:DSP_COMMAND_TXNR_DISABLE];
        NRIsEnabled = FALSE;
        //NSLog(@"noiseCancellationTxValueChange off");
    }
    
    [[NSUserDefaults standardUserDefaults] setBool:tmp.on forKey:zPreferenceKeyTXNR];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self.tableView reloadData];
    //BOOL bEnable = tmp.on;
    //[self queueTask:@selector(setNoiseCancellationRxValue:) target:audioControl argument:&bEnable, NULL];
    
}

- (void)noiseCancellationRxValueChange:(id)sender {
    UISwitch *tmp = (UISwitch *)sender;
    //NSLog(@"NoiseCancellationRxValueChange, %d", tmp.on);
    //[audioControl setNoiseCancellationRxValue:tmp.on];
    //[[NSUserDefaults standardUserDefaults] setBool:tmp.on forKey:zPreferenceKeyRXNR];
    //[[NSUserDefaults standardUserDefaults] synchronize];
    
    //if(tmp.on)
    //    [self DSPControlShortCommand:DSP_COMMAND_RXNR_ENABLE];
    //else
    //    [self DSPControlShortCommand:DSP_COMMAND_RXNR_DISABLE];
}

- (void)audioEnhancementValueChange:(id)sender {
    UISwitch *tmp = (UISwitch *)sender;
    //NSLog(@"audioEnhancementValueChange, %d", tmp.on);
}

- (void)ringFindAccessoryTone:(id)sender {
    //NSLog(@"ringFindAccessoryTone trigger");

    if(findAccessoryPlayer && [findAccessoryPlayer isPlaying])
    {
        [findAccessoryPlayer pause];
        [findAccessoryButton setTitle:@"Start" forState:UIControlStateNormal];
    }
    //If the track is not player, play the track and change the play button to "Pause"
    else
    {
        if (findAccessoryPlayer)
            [findAccessoryPlayer release];
        
        AVAudioSession *session = [AVAudioSession sharedInstance];
        
        NSError *sessionError;
        // [session setCategory:AVAudioSessionCategoryPlayAndRecord error:&sessionError];
        [session setCategory:AVAudioSessionCategoryPlayback error:&sessionError];
        if(session == nil)
            NSLog(@"Error creating session: %@", [sessionError description]);
        else
            [session setActive:YES error:nil];
        NSError *playerError;
        //  [self allowBluetoothAudioInput:0];
        //player = [[AVAudioPlayer alloc] initWithContentsOfURL:recordedFile error:&playerError];
        ButtonTableViewCell *cell = (ButtonTableViewCell *)[self retrieveCellForIdentifier:SETTING_ID_FIND_ACCESSORY];
        
        //NSURL *url = [NSURL fileURLWithPath:[[NSString alloc] initWithFormat:@"%@/%@.app/%@.mp3",NSHomeDirectory(), [[[NSBundle mainBundle] infoDictionary]   objectForKey:@"CFBundleName"], [[cell itemLabel] text]]];
        //NSLog(@"url = %@", url);
        //NSLog(@"%@",[[cell itemLabel] text]);
        //findAccessoryPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&playerError];
        
        NSString *path;
        NSURL *soundUrl;
        if([[[cell itemLabel] text] isEqualToString:@"Tone1"]){
            path = [NSString stringWithFormat:@"%@/Tone1.mp3", [[NSBundle mainBundle] resourcePath]];
        }
        if([[[cell itemLabel] text] isEqualToString:@"Tone2"]){
            path = [NSString stringWithFormat:@"%@/Tone2.mp3", [[NSBundle mainBundle] resourcePath]];
        }
        else if([[[cell itemLabel] text] isEqualToString:@"Tone3"]){
            path = [NSString stringWithFormat:@"%@/Tone3.mp3", [[NSBundle mainBundle] resourcePath]];
        }
        else if([[[cell itemLabel] text] isEqualToString:@"Tone4"]){
            path = [NSString stringWithFormat:@"%@/Tone4.mp3", [[NSBundle mainBundle] resourcePath]];
        }
        else{
            path = [NSString stringWithFormat:@"%@/Tone1.mp3", [[NSBundle mainBundle] resourcePath]];
        }                 
        
        soundUrl = [NSURL fileURLWithPath:path];
        findAccessoryPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:&playerError];
        
        if (findAccessoryPlayer == nil)
        {
            //NSLog(@"ERror creating player: %@", [playerError description]);
            return;
        }
        findAccessoryPlayer.delegate = self;
        [findAccessoryPlayer play];
        [findAccessoryButton setTitle:@"Stop" forState:UIControlStateNormal];
    }
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    [findAccessoryPlayer play];
}

- (void)playTone:(BOOL)b {
    //xx
}

-(void)IODirChange:(id)sender {
    UISwitch *tmp = (UISwitch *)sender;
    //NSLog(@"IODir,UISwitchState,%d",tmp.on);
    
    if(GPIOIndex == -1)
        return;
    
    GPIOIndexDir = tmp.on;
    
    NSMutableArray *propertyArray = [[NSMutableArray alloc] init];
    ISGPIOControlProperty *ioProperty = nil;
    ioProperty = [[ISGPIOControlProperty alloc] init];
    ioProperty.inOut = GPIOIndexDir;
    ioProperty.index = GPIOIndex;
    if(GPIOIndexDir)
        ioProperty.value = GPIOIndexValue;
    [propertyArray addObject:ioProperty];
    [ioProperty release];
    
    [self gpioControl:propertyArray];
    
    [self.tableView reloadData];
}

-(void)IOValueChange:(id)sender {
    UISwitch *tmp = (UISwitch *)sender;
    //NSLog(@"IOValue,UISwitchState,%d",tmp.on);
    
    if(GPIOIndex == -1)
        return;
    
    GPIOIndexValue = tmp.on;
    
    NSMutableArray *propertyArray = [[NSMutableArray alloc] init];
    ISGPIOControlProperty *ioProperty = nil;
    ioProperty = [[ISGPIOControlProperty alloc] init];
    ioProperty.inOut = YES;
    ioProperty.value = GPIOIndexValue;
    ioProperty.index = GPIOIndex;
    [propertyArray addObject:ioProperty];
    [ioProperty release];
    
    [self gpioControl:propertyArray];
    
    [self.tableView reloadData];
    
}

- (void)applyGPIOChange {
    //NSLog(@"applyGPIOChange");
    //return;
    
    uint8_t index[] = {indexGPIO1, indexGPIO2, indexGPIO3, indexGPIO4};
    
    NSMutableArray *propertyArray = [[NSMutableArray alloc] init];
    myTableViewSection *section = nil;
    myTableViewCell *cell = nil;
    
    ISGPIOControlProperty *ioProperty = nil;
    UISwitch *inOutSwitch = nil;
    for (uint8_t i = 0; i < [gpioList count]; i++) {
        ioProperty = [[ISGPIOControlProperty alloc] init];
        section = [gpioList objectAtIndex:i];
        cell = [section.cells objectAtIndex:0];
        inOutSwitch = [(ToggleSwitchTableViewCell *)cell.cellBody stateSwitch];
        ioProperty.inOut = inOutSwitch.on;
        ioProperty.index = index[i];
        if (ioProperty.inOut) {
            cell = [section.cells objectAtIndex:1];
            ioProperty.value = [(ToggleSwitchTableViewCell *)cell.cellBody stateSwitch].on;
            //NSLog(@"%d\n",ioProperty.value);
        }
       
        [propertyArray addObject:ioProperty];
        [ioProperty release];
    }
    
    [self gpioControl:propertyArray];
}

- (void)gpioDirectionChange:(id)sender {
    //NSLog(@"[gpioDirectionChange],%d",[gpioList count]);
    return;
    myTableViewSection *section = nil;
    myTableViewCell *cell = nil;
    UISwitch *stateSwitch = (UISwitch *)sender;
    ISGPIOControlProperty *ioProperty = [[[ISGPIOControlProperty alloc] init] autorelease];
    for (uint8_t i = 0; i < [gpioList count]; i++) {
        section = [gpioList objectAtIndex:i];
        cell = [section.cells objectAtIndex:0];
        if (stateSwitch == [(ToggleSwitchTableViewCell *)cell.cellBody stateSwitch]) {
            //NSLog(@"didChange,%d",stateSwitch.on);
            ioProperty.inOut = stateSwitch.on;
            ioProperty.index = i;
            
            if (stateSwitch.on) {
                //1/0 : Output/Input
                cell = [section.cells objectAtIndex:1];
                ioProperty.value = [(ToggleSwitchTableViewCell *)cell.cellBody stateSwitch].on;
            }
            
            //[audioControl gpioControl:[[NSArray alloc] initWithObjects:ioProperty, nil]];
            [self gpioControl:[[NSArray alloc] initWithObjects:ioProperty, nil]];
            break;
        }
    }
}

- (void)gpioValueChange:(id)sender {
    //NSLog(@"[gpioValueChange]");
}

-(BOOL)isPureInt:(NSString *)string {
    if([string length] < 3)
    {
        NSScanner *scan = [NSScanner scannerWithString:string];
        int val;
        return [scan scanInt:&val] && [scan isAtEnd];
    }
    else
    {
        //NSLog(@"Invalid data");
        //[self SimpleAlert:@"IO Control" message:@"Exceed range"];
        return NO;
    }
}

//#pragma mark - Text Field Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    //NSLog(@"textFieldShouldReturn,%@,%d",textField.text,textField.tag);
    
    BOOL IsString = YES;
    
    if ([textField.text length] == 0)
        return NO;
    else
    {
        if(textField.tag == 101)
        {
            if([self isPureInt:textField.text])
            {
                //NSLog(@"Parse textFileld data, IsPureInteger");
                IsString = NO;
                GPIOIndex = [textField.text intValue];
                if(GPIOIndex > 31)
                {
                    //NSLog(@"Exceed IO index range:0 ~ 31");
                    [self SimpleAlert:@"IO Control" message:@"Exceed range(0~31)"];
                    GPIOIndex = -1;
                }
                else{
                    [self.tableView reloadData];
                }
            }
            else
            {
                GPIOIndex = -1;
                [self SimpleAlert:@"Invalid data" message:@"It should be an integer"];
            }
        }
        
        NSMutableString *temp = [textField.text mutableCopy];
        NSData *temp_data = [temp dataUsingEncoding:NSUTF8StringEncoding];
        while ([temp_data length]>32) {
            [temp deleteCharactersInRange:NSMakeRange(temp.length -1, 1)];
            temp_data = [temp dataUsingEncoding:NSUTF8StringEncoding];
        }
        //[self.audioControl changeDeviceName:temp_data];
        if(_managerState == ISAudioControlManagerStateNormal)
        {
            if(textField.tag == 100)
            {
                //NSLog(@"didChangeDeviceName");
                if(IsString)
                {
                    [self changeDeviceName:temp_data];
                    [[NSUserDefaults standardUserDefaults] setObject:textField.text forKey:zPreferenceKeyDeviceName];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                }
            }
        }
        
        [textField resignFirstResponder];
        textField.text = temp;
        
        return YES;
    }
        
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    if (newLength > 32) {
        return NO;
    }
    else {
        NSMutableString *temp = [textField.text mutableCopy];
        [temp replaceCharactersInRange:range withString:string];
        NSData *temp_data = [temp dataUsingEncoding:NSUTF8StringEncoding];
        return [temp_data length]>32 ? NO : YES;
    }
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
 //   if (tableView == equalizerTableView.tableView)
  //      return 55;
 //   else
   //     return 65;
    
#if 1
    if(tableView == LE_DeviceListTableView.tableView)
        return 40;
    else
        return 55;
#else
    return 55;
#endif
}

- (NSString *)tableView:(UITableView*)tableView titleForHeaderInSection:(NSInteger)section {
    if (tableView == equalizerTableView.tableView) {
        return @"Equalizer Modes";
    }
    else if (tableView == gpioControlTableView.tableView) {
        myTableViewSection *mySection = [gpioList objectAtIndex:section];
        
        return mySection.sectionTitle;
    }
#if 1
    else if(tableView == LE_DeviceListTableView.tableView){
        return @"Select LE_Device";
    }
#endif
    else {
        myTableViewSection *mySection = [dataSource objectAtIndex:section];
        
        return mySection.sectionTitle;
    }
}

- (void)checkCell:(UITableViewCell *)cell {
	[cell setAccessoryType:UITableViewCellAccessoryCheckmark];
	[[cell textLabel] setTextColor:[UIColor colorWithRed:0.318 green:0.4 blue:0.569 alpha:1.0]];
}

- (void)uncheckCell:(UITableViewCell *)cell {
	[cell setAccessoryType:UITableViewCellAccessoryNone];
	[[cell textLabel] setTextColor:[UIColor darkTextColor]];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    if (tableView == equalizerTableView.tableView) {
        return 1;
    }
    else if (tableView == gpioControlTableView.tableView) {
        return [gpioList count];
    }
#if 1
    else if(tableView == LE_DeviceListTableView.tableView){
        return 1;
    }
#endif
    
   // NSLog(@"[numberOfSectionsInTableView] %d", [dataSource count]);
    return [dataSource count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (tableView == equalizerTableView.tableView) {
        return [equalizerModeOptions count];
    }
    else if (tableView == gpioControlTableView.tableView) {
        return 2;
    }
#if 1
    else if(tableView == LE_DeviceListTableView.tableView){
        //return 7;
        //return [LE_DeviceList count];
        return [_cbController.devicesList count];
    }
#endif
    
    myTableViewSection *mySection = [dataSource objectAtIndex:section];
    //NSLog(@"[numberOfRowsInSection] sec %d, Row %d", section,[mySection.cells count]);
    return [mySection.cells count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    static NSString *CellIdentifier = @"Cell";
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        
    myTableViewSection *mySection = nil;
    myTableViewCell *myCell = nil;
    if (tableView == equalizerTableView.tableView) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[equalizerModeOptions objectAtIndex:indexPath.row]];
        if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[equalizerModeOptions objectAtIndex:indexPath.row]] autorelease];
            [cell.textLabel setText:[equalizerModeOptions objectAtIndex:indexPath.row]];
        }
        
        if(indexPath.row == SyncDeviceEQ)
        {
            [self checkCell:cell];
            //NSLog(@"EQ : Check Cell");
        }
        else
        {
            [self uncheckCell:cell];
        }
        
        //if ([indexPath isEqual:selectedEQMode])
        //    [self checkCell:cell];
        //else
        //    [self uncheckCell:cell];
        return cell;
    }
    else if (tableView == gpioControlTableView.tableView) {
        mySection = [gpioList objectAtIndex:indexPath.section];
        myCell = [mySection.cells objectAtIndex:indexPath.row];
        if (myCell.cellBody == nil) {
            myCell.cellBody = (ToggleSwitchTableViewCell*) [[[NSBundle mainBundle] loadNibNamed:@"ToggleSwitchTableViewCell" owner:self options:nil] objectAtIndex:0];
            [[(ToggleSwitchTableViewCell*)myCell.cellBody itemLabel] setText:myCell.itemName];
            [[(ToggleSwitchTableViewCell*)myCell.cellBody stateSwitch] addTarget:self action:myCell.eventSelector forControlEvents:UIControlEventValueChanged];
        }
        
        return myCell.cellBody;
    }
#if 1
    else if(tableView == LE_DeviceListTableView.tableView){
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"devicesList"];
        if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"devicesList"] autorelease];
        }
        MyPeripheral *tmpPeripheral = [_cbController.devicesList objectAtIndex:indexPath.row];
        if(!(tmpPeripheral.advName))
        {
            cell.textLabel.text = @"null";
        }
        else
        {
            if([tmpPeripheral.advName length] != 0)
            {
                cell.textLabel.text = tmpPeripheral.advName;
            }
            else
            {
                cell.textLabel.text = @"null";
            }
        }
        
        //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[LE_DeviceList objectAtIndex:indexPath.row]];
        //if (cell == nil) {
            //cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[LE_DeviceList objectAtIndex:indexPath.row]] autorelease];
            //[cell.textLabel setText:[LE_DeviceList objectAtIndex:indexPath.row]];
            
            //MyPeripheral *tmpPeripheral = [_cbController.devicesList objectAtIndex:indexPath.row];
            //cell.textLabel.text = @"LE device";
        //}
        
        //if(LE_DeviceName != nil)
        //{
        //    if(SelectedLEDeviceListIndex != indexPath.row)
        //        [self uncheckCell:cell];
        //    else
        //        [self checkCell:cell];
        //}
        return cell;
    }
#endif
    
    
    mySection = [dataSource objectAtIndex:indexPath.section];
    myCell = [mySection.cells objectAtIndex:indexPath.row];
   // NSLog(@"[cellForRowAtIndexPath] id=%@", myCell.itemName);
    switch (myCell.cellIdentifier) {
        case SETTING_ID_TXNR:
            if (myCell.cellBody == nil) {
                myCell.cellBody = (ToggleSwitchTableViewCell*) [[[NSBundle mainBundle] loadNibNamed:@"ToggleSwitchTableViewCell" owner:self options:nil] objectAtIndex:0];
                
                //[[(ToggleSwitchTableViewCell*)myCell.cellBody itemLabel] setText:myCell.itemName];
                
                [[(ToggleSwitchTableViewCell*)myCell.cellBody stateSwitch] addTarget:self action:myCell.eventSelector forControlEvents:UIControlEventValueChanged];
                [[(ToggleSwitchTableViewCell *)myCell.cellBody customImageView] setImage:[UIImage imageNamed:@"Noise Cancellation.png"]];
                [[(ToggleSwitchTableViewCell*)myCell.cellBody stateSwitch] setOn:[[NSUserDefaults standardUserDefaults] boolForKey:zPreferenceKeyTXNR]];
 
            }
            
            if(_managerState == ISAudioControlManagerStateNormal)
                [(ToggleSwitchTableViewCell*)myCell.cellBody stateSwitch].enabled = YES;
            else
                [(ToggleSwitchTableViewCell*)myCell.cellBody stateSwitch].enabled = NO;
            
            if(NRIsEnabled)
            {
                [[(ToggleSwitchTableViewCell*)myCell.cellBody itemLabel] setText:zSettingItemTXNR];
                [(ToggleSwitchTableViewCell*)myCell.cellBody stateSwitch].on = YES;
            }
            else
            {
                [[(ToggleSwitchTableViewCell*)myCell.cellBody itemLabel] setText:@"NR Disable"];
                [(ToggleSwitchTableViewCell*)myCell.cellBody stateSwitch].on = NO;
            }
            
            break;
#ifdef ModifiedIO
        case SETTING_ID_GPIO_CONTROL1:
#endif
        case SETTING_ID_DEVICE_NAME:
            if (myCell.cellBody == nil) {
                myCell.cellBody = (TextFieldTableViewCell *) [[[NSBundle mainBundle] loadNibNamed:@"TextFieldTableViewCell" owner:self options:nil] objectAtIndex:0];
                [[(TextFieldTableViewCell *)myCell.cellBody textField] setText:myCell.itemName];
                [[(TextFieldTableViewCell *)myCell.cellBody textField] setDelegate:self];
                if(myCell.cellIdentifier == SETTING_ID_DEVICE_NAME)
                    [[(TextFieldTableViewCell *)myCell.cellBody textField] setTag:100];
                else
                    [[(TextFieldTableViewCell *)myCell.cellBody textField] setTag:101];
            }
            
            if(_managerState == ISAudioControlManagerStateNormal)
                [(TextFieldTableViewCell *)myCell.cellBody textField].enabled = YES;
            else
                [(TextFieldTableViewCell *)myCell.cellBody textField].enabled = NO;
            
#ifdef ModifiedIO
            if(myCell.cellIdentifier == SETTING_ID_GPIO_CONTROL1)
            {
                if(GPIOIndex == -1)
                {
                   [[(TextFieldTableViewCell *)myCell.cellBody textField] setText:@"IO index(0~31)"];
                }
            }
#endif
            break;
            
        case SETTING_ID_GPIO_CONTROL:
        case SETTING_ID_RECORD_VOICE:
            if (myCell.cellBody == nil) {
                myCell.cellBody = (ToggleSwitchTableViewCell *) [[[NSBundle mainBundle] loadNibNamed:@"ToggleSwitchTableViewCell" owner:self options:nil] objectAtIndex:0];
                [[(ToggleSwitchTableViewCell *)myCell.cellBody itemLabel] setText:myCell.itemName];
                [myCell.cellBody setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
                [[(ToggleSwitchTableViewCell*)myCell.cellBody stateSwitch] setHidden:TRUE];
                if (myCell.cellIdentifier == SETTING_ID_RECORD_VOICE) {
                    [[(ToggleSwitchTableViewCell *)myCell.cellBody customImageView] setImage:[UIImage imageNamed:@"Record voice.png"]];
                }
                else
                    [[(ToggleSwitchTableViewCell *)myCell.cellBody customImageView] setImage:[UIImage imageNamed:@"GPIO setting.png"]];
            }
            break;
#ifdef ModifiedIO
        case SETTING_ID_GPIO_CONTROL2:
        case SETTING_ID_GPIO_CONTROL3:
            if (myCell.cellBody == nil) {
                myCell.cellBody = (ToggleSwitchTableViewCell*) [[[NSBundle mainBundle] loadNibNamed:@"ToggleSwitchTableViewCell" owner:self options:nil] objectAtIndex:0];
                [[(ToggleSwitchTableViewCell*)myCell.cellBody itemLabel] setText:myCell.itemName];
                [[(ToggleSwitchTableViewCell*)myCell.cellBody stateSwitch] addTarget:self action:myCell.eventSelector forControlEvents:UIControlEventValueChanged];
                [[(ToggleSwitchTableViewCell *)myCell.cellBody customImageView] setImage:[UIImage imageNamed:@"GPIO setting.png"]];
            }
            
            if (myCell.cellIdentifier == SETTING_ID_GPIO_CONTROL2) {
                if(GPIOIndexDir){
                    [[(ToggleSwitchTableViewCell*)myCell.cellBody itemLabel] setText:@"Output"];
                    [(ToggleSwitchTableViewCell*)myCell.cellBody stateSwitch].on = YES;
                }
                else{
                    [[(ToggleSwitchTableViewCell*)myCell.cellBody itemLabel] setText:@"Input"];
                    [(ToggleSwitchTableViewCell*)myCell.cellBody stateSwitch].on = NO;
                }
            }
            else
            {
                if(GPIOIndexDir)
                {
                    if(GPIOIndexValue)
                        [[(ToggleSwitchTableViewCell*)myCell.cellBody itemLabel] setText:@"Set High"];
                    else
                        [[(ToggleSwitchTableViewCell*)myCell.cellBody itemLabel] setText:@"Set Low"];
                }
                else
                {
                    if(GPIOIndex != -1)
                    {
                        if(GPIOIndexValue)
                        {
                            [[(ToggleSwitchTableViewCell*)myCell.cellBody itemLabel] setText:@"Detect High"];
                            [(ToggleSwitchTableViewCell*)myCell.cellBody stateSwitch].on = YES;
                        }
                        else
                        {
                            [[(ToggleSwitchTableViewCell*)myCell.cellBody itemLabel] setText:@"Detect Low"];
                            [(ToggleSwitchTableViewCell*)myCell.cellBody stateSwitch].on = NO;
                        }
                    }
                }
            }
            
            if(GPIOIndex != -1)
            {
                if (myCell.cellIdentifier == SETTING_ID_GPIO_CONTROL2) {
                    [(ToggleSwitchTableViewCell*)myCell.cellBody stateSwitch].enabled = YES;
                }
                else
                {
                    if(GPIOIndexDir)
                        [(ToggleSwitchTableViewCell*)myCell.cellBody stateSwitch].enabled = YES;
                    else
                        [(ToggleSwitchTableViewCell*)myCell.cellBody stateSwitch].enabled = NO;
                }
            }
            else
            {
                [(ToggleSwitchTableViewCell*)myCell.cellBody stateSwitch].enabled = NO;
            }
            
            //if(_managerState == ISAudioControlManagerStateNormal)
            //    [(ToggleSwitchTableViewCell*)myCell.cellBody stateSwitch].enabled = YES;
            //else
            //    [(ToggleSwitchTableViewCell*)myCell.cellBody stateSwitch].enabled = NO;
            break;
#endif
            
        case SETTING_ID_EQUALIZER:
            if (myCell.cellBody == nil) {
                myCell.cellBody = (ToggleSwitchTableViewCell *) [[[NSBundle mainBundle] loadNibNamed:@"ToggleSwitchTableViewCell" owner:self options:nil] objectAtIndex:0];
                
                //[[(ToggleSwitchTableViewCell *)myCell.cellBody itemLabel] setText:myCell.itemName];
                
                [myCell.cellBody setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
                [[(ToggleSwitchTableViewCell*)myCell.cellBody stateSwitch] setHidden:TRUE];
                [[(ToggleSwitchTableViewCell *)myCell.cellBody customImageView] setImage:[UIImage imageNamed:@"Equalizer.png"]];
            }
            [[(ToggleSwitchTableViewCell *)myCell.cellBody itemLabel] setText:[equalizerModeOptions objectAtIndex:SyncDeviceEQ]];
            break;
            
        case SETTING_ID_BUZZER:
            /*
            if (myCell.cellBody == nil)
            {
                myCell.cellBody = (InformationTableViewCell *) [[[NSBundle mainBundle] loadNibNamed:@"InformationTableViewCell" owner:self options:nil] objectAtIndex:0];
                [[(InformationTableViewCell *)myCell.cellBody itemLabel] setText:myCell.itemName];
            }
            [[(InformationTableViewCell *)myCell.cellBody informationLabel] setText:@"Off"];
             */
            if (myCell.cellBody == nil) {
                //myCell.cellBody = (ToggleSwitchTableViewCell*) [[[NSBundle mainBundle] loadNibNamed:@"ToggleSwitchTableViewCell" owner:self options:nil] objectAtIndex:0];
                myCell.cellBody = (ButtonTableViewCell*) [[[NSBundle mainBundle] loadNibNamed:@"ButtonTableViewCell" owner:self options:nil] objectAtIndex:0];
                
                //[[(ToggleSwitchTableViewCell*)myCell.cellBody itemLabel] setText:myCell.itemName];
                //[[(ToggleSwitchTableViewCell*)myCell.cellBody stateSwitch] addTarget:self action:myCell.eventSelector forControlEvents:UIControlEventValueChanged];
                //[[(ToggleSwitchTableViewCell *)myCell.cellBody customImageView] setImage:[UIImage imageNamed:@"buzzer.jpg"]];
                //[[(ToggleSwitchTableViewCell*)myCell.cellBody stateSwitch] setOn:NO];
                [[(ButtonTableViewCell*)myCell.cellBody itemLabel] setText:myCell.itemName];
                [[(ButtonTableViewCell*)myCell.cellBody actionButton] addTarget:self action:myCell.eventSelector forControlEvents:UIControlEventTouchUpInside];
                [[(ButtonTableViewCell *)myCell.cellBody customImageView] setImage:[UIImage imageNamed:@"buzzer.jpg"]];
                forceAlertButton = [(ButtonTableViewCell*)myCell.cellBody actionButton];
            }
            
            //if(_managerState == ISAudioControlManagerStateNormal)
            //{
                //[(ToggleSwitchTableViewCell*)myCell.cellBody stateSwitch].enabled = YES;
            //    [(ButtonTableViewCell*)myCell.cellBody actionButton].enabled = YES;
            //}
            //else
            //{
                //[(ToggleSwitchTableViewCell*)myCell.cellBody stateSwitch].enabled = NO;
            //    [(ButtonTableViewCell*)myCell.cellBody actionButton].enabled = NO;
            //}
            
            break;
            
        case SETTING_ID_FW_VERSION:
            if (myCell.cellBody == nil) {
                myCell.cellBody = (InformationTableViewCell *) [[[NSBundle mainBundle] loadNibNamed:@"InformationTableViewCell" owner:self options:nil] objectAtIndex:0];
                [[(InformationTableViewCell *)myCell.cellBody itemLabel] setText:myCell.itemName];
            }
            [[(InformationTableViewCell *)myCell.cellBody informationLabel] setText:fwVersion];
            break;
        case SETTING_ID_APP_VERSION:
            if (myCell.cellBody == nil) {
                myCell.cellBody = (InformationTableViewCell *) [[[NSBundle mainBundle] loadNibNamed:@"InformationTableViewCell" owner:self options:nil] objectAtIndex:0];
                [[(InformationTableViewCell *)myCell.cellBody itemLabel] setText:myCell.itemName];
                [[(InformationTableViewCell *)myCell.cellBody informationLabel] setText:APP_VERSION];
            }
            
            
            break;
        case SETTING_ID_SERIAL_NUMBER:
            if (myCell.cellBody == nil) {
                myCell.cellBody = (InformationTableViewCell *) [[[NSBundle mainBundle] loadNibNamed:@"InformationTableViewCell" owner:self options:nil] objectAtIndex:0];
                [[(InformationTableViewCell *)myCell.cellBody itemLabel] setText:myCell.itemName];
                
            }
            [[(InformationTableViewCell *)myCell.cellBody informationLabel] setText:@"N/A"];
            //if (audioControl.managerState == ISAudioControlManagerStateNormal)
            //    [[(InformationTableViewCell *)myCell.cellBody informationLabel] setText:audioControl.dataPath.accessory.serialNumber];
            //else
            //    [[(InformationTableViewCell *)myCell.cellBody informationLabel] setText:@"N/A"];
            break;
        case SETTING_ID_FIND_ACCESSORY:
        case SETTING_ID_FIND_ACCESSORY_VOLUME:
            if (myCell.cellBody == nil) {
                myCell.cellBody = (ButtonTableViewCell*) [[[NSBundle mainBundle] loadNibNamed:@"ButtonTableViewCell" owner:self options:nil] objectAtIndex:0];
                [[(ButtonTableViewCell*)myCell.cellBody itemLabel] setText:myCell.itemName];
                [[(ButtonTableViewCell *)myCell.cellBody customImageView] setImage:[UIImage imageNamed:@"Send a tone.png"]];
                if (myCell.cellIdentifier == SETTING_ID_FIND_ACCESSORY_VOLUME) {
                    [[(ButtonTableViewCell*)myCell.cellBody actionButton] setHidden:YES];
                    CGRect frame = [[(ButtonTableViewCell*)myCell.cellBody itemLabel] frame];
                    MPVolumeView *myVolumeView = [[MPVolumeView alloc] initWithFrame: CGRectMake(frame.origin.x + 50, frame.origin.y + 5, frame.size.width + 35, frame.size.height)];
                    [myVolumeView setShowsRouteButton:NO];
                    [myCell.cellBody addSubview: myVolumeView];
                    [myVolumeView release];
                }
                else {
                    [[(ButtonTableViewCell*)myCell.cellBody actionButton] addTarget:self action:myCell.eventSelector forControlEvents:UIControlEventTouchUpInside];
                    findAccessoryButton = [(ButtonTableViewCell*)myCell.cellBody actionButton];
                }
            }
            
            break;
        case SETTING_ID_TTS: {
            if (myCell.cellBody == nil) {
                myCell.cellBody = (InformationTableViewCell *) [[[NSBundle mainBundle] loadNibNamed:@"InformationTableViewCell" owner:self options:nil] objectAtIndex:0];
#ifdef APP_DEMO_VERSION
                [[(InformationTableViewCell *)myCell.cellBody itemLabel] setText:@"TTS"];
#else
                [[(InformationTableViewCell *)myCell.cellBody itemLabel] setText:@"Convert Address Book"];
#endif
                [[(InformationTableViewCell *)myCell.cellBody informationLabel] setText:@""];
                myCell.cellBody.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                myCell.cellBody.userInteractionEnabled = YES;
            }
        }
            break;
        default:
            break;

    }
    
    // Configure the cell...
    
    return myCell.cellBody;
}


#pragma mark - Table view delegate
/*
- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	//create a set of specifier types that can't be selected
    NSLog(@"willSelectRowAtIndexPath");
	return nil;
}
*/
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"didSelectRowAtIndexPath");
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableView == equalizerTableView.tableView) {
        
        //if (selectedEQMode.row == indexPath.row) //ignore the same click
        //    return;
        if(SyncDeviceEQ == indexPath.row)
            return;
        
        /*
        if (audioControl.didChangeEqMode == FALSE) {
            [self uncheckCell:[tableView cellForRowAtIndexPath:indexPath]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Wait a moment" message:[NSString stringWithFormat:@"Last command isn't done yet"] delegate:self cancelButtonTitle:@"Close" otherButtonTitles: nil];
            [alert show];
            [alert release];
            return;
        }
        */
        //if (selectedEQMode.row != EQ_MODE_INVALID)
        [self uncheckCell:[tableView cellForRowAtIndexPath:selectedEQMode]];
        [self setSelectedEQMode:indexPath];
        [self checkCell:[tableView cellForRowAtIndexPath:selectedEQMode]];
        ToggleSwitchTableViewCell *cell = (ToggleSwitchTableViewCell *)[self retrieveCellForIdentifier:SETTING_ID_EQUALIZER];
        [[cell itemLabel] setText:[equalizerModeOptions objectAtIndex:indexPath.row]];
        //audio control command here

        //[audioControl setEqualizerMode:indexPath.row];
        [self EQModeControlCommand:indexPath.row];
        
        SyncDeviceEQ = indexPath.row;
        
        [[NSUserDefaults standardUserDefaults] setInteger:indexPath.row forKey:zPreferenceKeyEqualizer];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self.tableView reloadData];
        return;
    }
#if 1
    else if(tableView == LE_DeviceListTableView.tableView)
    {
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        //[self checkCell:cell];
        
        int count = (int)[_cbController.devicesList count];
        if ((count != 0) && count > indexPath.row) {
            MyPeripheral *tmpPeripheral = [_cbController.devicesList objectAtIndex:indexPath.row];
            if (tmpPeripheral.connectStatus != MYPERIPHERAL_CONNECT_STATUS_IDLE) {
                //NSLog(@"Device is not idle - break");
                //break;
            }
        
            [_cbController stopScan];
            
            [_cbController connectDevice:tmpPeripheral];
            controlPeripheral = tmpPeripheral;
            //NSLog(@"didSelect,LE_DeviceTableview,%@",tmpPeripheral.advName);
        }
        //SelectedLEDeviceListIndex = indexPath.row;
        //LE_DeviceName = [LE_DeviceList objectAtIndex:SelectedLEDeviceListIndex];
        
        DeviceIsSelect = TRUE;
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [[appDelegate navigationController] popViewControllerAnimated:YES];
        //NSLog(@"didSelect,LE_DeviceTableview,%@",LE_DeviceName);
        
        return;
    }
#endif
    else if (tableView == gpioControlTableView.tableView) {
        return;
    }
    
    
  //  UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
  //  [self setSelectedCell:cell];
    myTableViewSection *section = [dataSource objectAtIndex:indexPath.section];
    myTableViewCell *myCell = [section.cells objectAtIndex:indexPath.row];
    if (myCell.cellIdentifier == SETTING_ID_EQUALIZER) {
        if(_managerState == ISAudioControlManagerStateNormal)
        {
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            [[appDelegate navigationController] pushViewController:equalizerTableView animated:YES];
        }
        else
        {
            [self SimpleAlert:@"Disconnected" message:@"Device not found!"];
        }
    }
    else if (myCell.cellIdentifier == SETTING_ID_FIND_ACCESSORY) {
        if (findAccessoryPlayer.isPlaying)
            [self ringFindAccessoryTone:nil];
        ISSCTableAlertView *alertView = [[ISSCTableAlertView alloc] initWithCaller:self data:findAccessoryTones title:@"Find Accessory Tones:" buttonTitle:@"Cancel" andContext:nil];
        [alertView.myTableView setBackgroundView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iSSC_background.png"]]];
        [alertView show];
        [alertView release];
    }
    else if (myCell.cellIdentifier == SETTING_ID_RECORD_VOICE) {
        if (recordViewController == nil)
            recordViewController = [[RecordViewController alloc] initWithNibName:@"RecordViewController" bundle:nil];
        /*AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [[appDelegate navigationController] pushViewController:recordViewController animated:YES];*/
        [self.navigationController pushViewController:recordViewController animated:YES];
    }
    else if (myCell.cellIdentifier == SETTING_ID_GPIO_CONTROL) {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
#ifdef APP_DEMO_VERSION
        //gpioViewController.audioControl = audioControl;
        [[appDelegate navigationController] pushViewController:gpioViewController animated:YES];
#else
        if(_managerState == ISAudioControlManagerStateNormal)
        {
            [[appDelegate navigationController] pushViewController:gpioControlTableView animated:YES];
            //if (audioControl.managerState == ISAudioControlManagerStateNormal) {
            if(_managerState == ISAudioControlManagerStateNormal) {
                gpioControlTableView.navigationItem.rightBarButtonItem = _applyGPIO;
                
                [self applyGPIOChange];
            }
            else {
                gpioControlTableView.navigationItem.rightBarButtonItem = disconnectBarButtonItem;
            }
        }
        else{
            [self SimpleAlert:@"Disconnected" message:@"Device not found!"];
        }
#endif
        
    }
    else if(myCell.cellIdentifier == SETTING_ID_GPIO_CONTROL2) {
       
    }
    else if (myCell.cellIdentifier == SETTING_ID_TTS) {
        /*
        TTRootViewController *tts = [[TTRootViewController alloc] init];
        tts.setting = self;
#ifdef APP_DEMO_VERSION
        [self.navigationController pushViewController:tts animated:YES];
        [tts release];
#else
        [tts speak:nil];
        [tts release];
#endif
         */
    }
}

- (void)SimpleAlert:(NSString *)title message:(NSString *)msg
{
    if(floor(NSFoundationVersionNumber) < NSFoundationVersionNumber_iOS_7_0)
    {
        //UIAlertView
        UIAlertView *errorAlert = [[UIAlertView alloc]
                                   initWithTitle:title message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [errorAlert show];
    }
    else
    {
        if(floor(NSFoundationVersionNumber) == NSFoundationVersionNumber_iOS_7_0)
        {
            UIAlertView *errorAlert = [[UIAlertView alloc]
                                       initWithTitle:title message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [errorAlert show];

        }
        else if(floor(NSFoundationVersionNumber) == NSFoundationVersionNumber_iOS_7_1)
        {
            UIAlertView *errorAlert = [[UIAlertView alloc]
                                       initWithTitle:title message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [errorAlert show];

        }
        else
        {
            UIAlertController *myAlert = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){[self dismissViewControllerAnimated:YES completion:nil];}];
            [myAlert addAction:okAction];
            [self presentViewController:myAlert animated:YES completion:nil];
        }
    }
}

#pragma mark - Table Alert view delegate
- (void)didSelectRowAtIndex:(NSInteger)row withContext:(id)context {
    if (row >= 0) {
        ButtonTableViewCell *cell = (ButtonTableViewCell *)[self retrieveCellForIdentifier:SETTING_ID_FIND_ACCESSORY];
        [[cell itemLabel] setText:[findAccessoryTones objectAtIndex:row]];
    }
}


- (void)queueTask:(SEL)selector target:(id)target argument:(void *)arg1,... {
    //NSLog(@"[SettingTableViewController] queueTask");
    
    NSMethodSignature *sgn = [target methodSignatureForSelector:selector];
    NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:sgn];
    [invocation setTarget:target];
    [invocation setSelector:selector];
    
    uint8_t idx = 0;
    if (arg1 != NULL) {
        va_list args;
        va_start(args, arg1);
        for (void *arg = arg1; arg != NULL; arg = va_arg(args, void*))
        {
            NSLog(@"idx = %d",idx);
            [invocation setArgument:arg atIndex:2+idx];
            idx++;
        }
        va_end(args);
    }
    [invocation retainArguments];
    if (_queuedTasks == nil)
        _queuedTasks = [[NSMutableArray alloc] init];
    [_queuedTasks addObject:invocation];
}

- (void)checkQueuedTask {
  //  NSLog(@"[SettingTableViewController] checkQueuedTask1");
    if (_queuedTasks == nil)
        return;
  //  NSLog(@"[SettingTableViewController] checkQueuedTask2");
    if ([_queuedTasks count] == 0)
        return;
  //  NSLog(@"[SettingTableViewController] checkQueuedTask3");
    NSInvocation *invocation = [_queuedTasks objectAtIndex:0];
    [invocation invoke];
    [_queuedTasks removeObjectAtIndex:0];
}

- (void)removeQueuedTask {
    if (_queuedTasks)
        [_queuedTasks removeAllObjects];
}

// <-- CBController delegate
- (void)CBController:(CBController *)cbController didUpdateDiscoveredPeripherals:(NSArray *)peripherals {
    //NSLog(@"didUpdateDiscoveredPeripherals");
    //[devicesTableView reloadData];
    if(_cbController.isScanning == YES)
    {
        [LE_DeviceListTableView.tableView reloadData];
    }
}
/*
 - (void)updateDiscoverPeripherals {
 [super updateDiscoverPeripherals];
 [devicesTableView reloadData];
 }*/

- (void)CBController:(CBController *)cbController didDisconnectedPeripheral:(MyPeripheral *)myPeripheral {
    //NSLog(@"updateMyPeripheralForDisconnect");//, %@", myPeripheral.advName);
    _managerState = ISAudioControlManagerStateNotConnected;
    self.navigationItem.rightBarButtonItem = disconnectBarButtonItem;
    DeviceIsSelect = FALSE;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if([[appDelegate navigationController].topViewController.title isEqualToString:@"EQ"])
    {
        
        [[appDelegate navigationController] popViewControllerAnimated:YES];
    }
    else if([[appDelegate navigationController].topViewController.title isEqualToString:@"GPIO"])
    {
        
        [[appDelegate navigationController] popViewControllerAnimated:YES];
    }
    
    //else
    {
        GPIOIndex = -1;
        GPIOIndexDir = YES;
        GPIOIndexValue = YES;
        fwVersion = @"N/A";
        [self.tableView reloadData];
    }
    /*
    if (myPeripheral == controlPeripheral) {
        [NSTimer scheduledTimerWithTimeInterval:0.03 target:self selector:@selector(popToRootPage) userInfo:nil repeats:NO];
    }
    
    for (int idx =0; idx< [connectedDeviceInfo count]; idx++) {
        DeviceInfo *tmpDeviceInfo = [connectedDeviceInfo objectAtIndex:idx];
        if (tmpDeviceInfo.myPeripheral == myPeripheral) {
            [connectedDeviceInfo removeObjectAtIndex:idx];
            //NSLog(@"updateMyPeripheralForDisconnect1");
            break;
        }
    }
    
    [self displayDevicesList];
    [self updateButtonType];
    
    if(cbController.isScanning == YES){
        [self stopScan];
        [self startScan];
        [devicesTableView reloadData];
    }
     */
}

- (void)CBController:(CBController *)cbController didConnectedPeripheral:(MyPeripheral *)myPeripheral {
    //NSLog(@"[ConnectViewController] updateMyPeripheralForNewConnected");
    UIImageView *connectImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Connect.png"]];
    UIButton *connectButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [connectButton setBackgroundImage:connectImageView.image forState:UIControlStateNormal];
    connectButton.frame = CGRectMake(0.0f, 0.0f, 20.0f, 20.0f);
    [connectButton addTarget: self action:@selector(PressConnectButton:) forControlEvents:UIControlEventTouchUpInside];
    connectBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:connectButton];
    
    self.navigationItem.rightBarButtonItem = connectBarButtonItem;
    _managerState = ISAudioControlManagerStateNormal;
    DeviceIsSelect = TRUE;
    
    [self.tableView reloadData];
    
    //[audioControl getFirmwareVersion];
    [self LE_GetFirmwareVersion];
    
    //[self MMICommand:0x00 Para2:0x3F];    //Query current EQ mode
    //[self MMICommand:0x00 Para2:0x79];      //Force buzzer alert
    //[self MMICommand:0x00 Para2:0x78];      //Query NR(TX) status
    //[self MMICommand:0x00 Para2:0x77];      //Query NR(RX) status
    
    /*
    DeviceInfo *tmpDeviceInfo = [[DeviceInfo alloc]init];
    tmpDeviceInfo.mainViewController = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
    tmpDeviceInfo.mainViewController.connectedPeripheral = myPeripheral;
    tmpDeviceInfo.myPeripheral = myPeripheral;
    tmpDeviceInfo.myPeripheral.connectStatus = myPeripheral.connectStatus;
    //  deviceRecord setObject:myPeripheral.advName forKey:myPeripheral.
    
    [deviceRecord setObject:[myPeripheral advName] forKey:[myPeripheral uuidString]];
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    [def setObject:deviceRecord forKey:@"deviceRecord"];
    [def synchronize];
    
    bool b = FALSE;
    for (int idx =0; idx< [connectedDeviceInfo count]; idx++) {
        DeviceInfo *tmpDeviceInfo = [connectedDeviceInfo objectAtIndex:idx];
        if (tmpDeviceInfo.myPeripheral == myPeripheral) {
            b = TRUE;
            break;
        }
    }
    if (!b) {
        [connectedDeviceInfo addObject:tmpDeviceInfo];
    }
    else{
        NSLog(@"Connected List Filter!");
    }
    
    [self displayDevicesList];
    [self updateButtonType];
     */
}

- (void)startScan
{
    [_cbController startScanWithUUID:nil];
    //NSLog(@"startScan");
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (![self.navigationController.viewControllers containsObject:LE_DeviceListTableView] && self.navigationController.viewControllers.count > 0)
        [[appDelegate navigationController] pushViewController:LE_DeviceListTableView animated:YES];
}

#if 1
#define START_BYTE 0xAA

- (void)CBDataRxReceived:(CBController *)cbController Mydata:(NSData *)data
{
    if(data && ([data length]))
    {
        if(([data length]) <= 10)
        {
            //NSLog(@"CBReceivedData");
            unsigned char buf[10];
            int len = [data length];
            memset(buf, 0x00, sizeof(buf));
            [data getBytes:buf length:len];
            
            //NSLog(@"YA,len =%d,Rec= %x,%x,%x,%x,%x,%x,%x,%x,%x,%x",len,buf[0],buf[1],buf[2],buf[3],buf[4],buf[5],buf[6],buf[7],buf[8],buf[9]);
        }
        
        
        [_events appendData:data];
        
        [self parseEvents];
    }
}

- (void)ACKTimeoutOfCommand:(NSNumber *)cmdID {
    //NSLog(@"[ISAudioControlManager] ACKTimeoutOfCommand = %02x", [cmdID unsignedCharValue]);
    /* UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"ACK Timeout" message:[NSString stringWithFormat:@"Command id = %02x no ACK",[cmdID unsignedCharValue]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
     [self removeCheckResponseTimerForCommand:[cmdID unsignedCharValue]];
     [alert show];
     [alert release];
     */
    [self removeCheckResponseTimerForCommand:[cmdID unsignedCharValue]];
    NSError *error = [[[NSError alloc] initWithDomain:@"ACK Timeout" code:-1 userInfo:nil] autorelease];
    if ([cmdID unsignedCharValue] == AUDIO_COMMAND_ID_GPIO_CONTROL) {
        //if (gpioControlDelegate && [(NSObject *)gpioControlDelegate respondsToSelector:@selector(ISAudioControlManager:reportErrorForcmdID:error:)]) {
         //   [gpioControlDelegate ISAudioControlManager:self reportErrorForcmdID:[cmdID unsignedCharValue] error:error];
        //}
    }
    else if ([cmdID unsignedCharValue] == AUDIO_COMMAND_ID_EQ_MODE_SETTING) {
        didChangeEqMode = TRUE;
        
    }
    else if ([cmdID unsignedCharValue] == AUDIO_COMMAND_ID_TTS) {
        [_tts_data release];
        _tts_data = nil;
        _tts_data_index = 0;
    }
    else
        NSLog(@"Command Error!");
    //else if (delegate && [(NSObject *)delegate respondsToSelector:@selector(ISAudioControlManager:reportErrorForcmdID:error:)]) {
    //    [delegate ISAudioControlManager:self reportErrorForcmdID:[cmdID unsignedCharValue] error:error];
    //}
}

- (void)returnEventAck: (uint8_t)cmdID {
    unsigned char buf[25];
    memset(buf, 0x00, sizeof(buf));
    AUDIO_COMMAND_HEADER *header = (AUDIO_COMMAND_HEADER *)buf;
    header->start_byte = START_BYTE;
    header->commandID = AUDIO_COMMAND_ID_EVENT_ACK;
    unsigned char idx = COMMAND_HEADER_SIZE;
    
    buf[idx++] = cmdID;
    
    header->length_high = 0x00;
    header->length_low = idx - COMMAND_HEADER_SIZE + 1;
    buf[idx] = [self calculateChecksum:&buf[1] dataLength:idx-1];
    //[self sendCommand:buf length:idx+1];
}

- (void)checkResponseForCommand:(uint8_t)cmdID timeout:(NSTimeInterval)timeout {
    //   static uint32_t identifier = 0;
    //   identifier++;
    //NSLog(@"[ISAudioControlManager] checkResponseForCommand %02x",cmdID);
    NSMethodSignature *sgn = [self methodSignatureForSelector:@selector(ACKTimeoutOfCommand:)];
    NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:sgn];
    [invocation setTarget:self];
    [invocation setSelector:@selector(ACKTimeoutOfCommand:)];
    NSNumber *arg = [[[NSNumber alloc] initWithUnsignedChar:cmdID] autorelease];
    [invocation setArgument:&arg atIndex:2];
    // [invocation setArgument:[[NSNumber alloc] initWithUnsignedChar:cmdID] atIndex:2];
    [invocation retainArguments];
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:timeout invocation:invocation repeats:NO];
    ACKTimer1 *obj = [[ACKTimer1 alloc] initWithCommandID:cmdID timer:timer];
    [_waitingACK addObject:obj];
}


- (BOOL)removeCheckResponseTimerForCommand:(uint8_t)cmdID {
    ACKTimer1 *obj = nil;
    //NSLog(@"[ISAudiocontrolManager] removeCheckResponseTimerForCommand, cmdID = %02x, count = %d", cmdID, [_waitingACK count]);
    for (int i=0; i<[_waitingACK count]; i++) {
        obj = [_waitingACK objectAtIndex:i];
        //NSLog(@"[ISAudiocontrolManager] removeCheckResponseTimerForCommand, waitingACK = %02x", obj.commandID);
        if (obj.commandID == cmdID) {
            //NSLog(@"[ISAudiocontrolManager] removeCheckResponseTimerForCommand,found");
            [obj.timer invalidate];
            [_waitingACK removeObject:obj];
            return TRUE;
        }
    }
    return FALSE;
}

- (void)parseEvents {
    unsigned char start = 0x00;
    
    NSRange range = NSMakeRange(0, 1);
    [_events getBytes:&start length:1];
    
    while (start != START_BYTE && [_events length]) {
        [_events replaceBytesInRange:range withBytes:NULL length:0];
        [_events getBytes:&start length:1];
    }
    if ([_events length] < COMMAND_HEADER_SIZE)
    {
        //NSLog(@"[parseEvents] %d return1", [_events length]);
        return;
    }
    char buf[10];
    [_events getBytes:buf length:COMMAND_HEADER_SIZE];
    AUDIO_COMMAND_HEADER *header = (AUDIO_COMMAND_HEADER *)buf;
    short payloadLen = (header->length_high << 8 | header->length_low);
    short eventLen = payloadLen + COMMAND_HEADER_SIZE;
    //check if event is complete
    if ([_events length] < payloadLen +2) //+ start byte AA and checksum
    {
        //NSLog(@"[parseEvents] return2");
        return;
    }
    
    unsigned char *event = malloc(eventLen);
    [_events getBytes:event length:eventLen];
    unsigned char checksum = [self calculateChecksum:event+1 dataLength:eventLen-2]; //minus start_byte and checksum
    range = NSMakeRange(0, eventLen);
    [_events replaceBytesInRange:range withBytes:NULL length:0];
    if (checksum != event[eventLen-1]) {
        
        //NSLog(@"checksum error");
        return;
    }
    NSError *error = nil;
    if (header->commandID != AUDIO_EVENT_ID_ACK) {
        [self returnEventAck:header->commandID];
    }
    switch (header->commandID) {
        case AUDIO_EVENT_ID_ACK:
        {
            uint8_t cmdID = event[COMMAND_HEADER_SIZE];
            uint8_t status = event[COMMAND_HEADER_SIZE+1];
            //NSLog(@"received ACK, cmdID = 0x%02x, status = 0x%02x",cmdID, status);
            BOOL ack = [self removeCheckResponseTimerForCommand:cmdID];
            if (status)
                error = [[[NSError alloc] initWithDomain:@"execute command fail" code:status userInfo:nil] autorelease];
            switch (cmdID) {
                case AUDIO_COMMAND_ID_EVENT_MSASK_SETTING:
                    //if (ack && delegate && [(NSObject *)delegate respondsToSelector:@selector(ISAudioControlManager:didSetEventMaskSetting:)])
                    //    [delegate ISAudioControlManager:self didSetEventMaskSetting:error];
                    [self checkQueuedTask];
                    break;
                case AUDIO_COMMAND_ID_DSP_CONTROL:
                    switch (_dspParamRecord) {
                        case DSP_COMMAND_RXNR_DISABLE:
                        case DSP_COMMAND_RXNR_ENABLE:
                            //if (ack && delegate && [(NSObject *)delegate respondsToSelector:@selector(ISAudioControlManager:didSetNoiseCancellationRxValue:)])
                            //    [delegate ISAudioControlManager:self didSetNoiseCancellationRxValue:error];
                            //NSLog(@"[SettingTableViewController] didSetNoiseCancellationRxValue");
                            if (error) {
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"set noise cancellation rx fail" message:[NSString stringWithFormat:@"error code = %d", error.code] delegate:self cancelButtonTitle:@"Close" otherButtonTitles: nil];
                                [alert show];
                                [alert release];
                                return;
                            }
                            [self checkQueuedTask];
                            break;
                        case DSP_COMMAND_TXNR_DISABLE:
                        case DSP_COMMAND_TXNR_ENABLE:
                            //if (ack && delegate && [(NSObject *)delegate respondsToSelector:@selector(ISAudioControlManager:didSetNoiseCancellationTxValue:)])
                              //  [delegate ISAudioControlManager:self didSetNoiseCancellationTxValue:error];
                            //NSLog(@"[SettingTableViewController] didSetNoiseCancellationTxValue");
                            if (error) {
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"set noise cancellation tx fail" message:[NSString stringWithFormat:@"error code = %d", error.code] delegate:self cancelButtonTitle:@"Close" otherButtonTitles: nil];
                                [alert show];
                                [alert release];
                                return;
                            }
                            [self checkQueuedTask];
                            
                            if(NRIsEnabled == TRUE)
                            {
                                [self DSPControlShortCommand:DSP_COMMAND_RXNR_ENABLE];
                                //NSLog(@"NoiseCancellationRxValue,Enable");
                            }
                            else
                            {
                                [self DSPControlShortCommand:DSP_COMMAND_RXNR_DISABLE];
                                //NSLog(@"NoiseCancellationRxValue,Disable");
                            }

                            break;
                        default:
                            break;
                    }
                    
                    break;
                case AUDIO_COMMAND_ID_EQ_MODE_SETTING:
                    didChangeEqMode = TRUE;
                    
                    break;
                case AUDIO_COMMAND_ID_CHANGE_DEVICE_NAME:
                {
                    //if (ack && delegate && [(NSObject *)delegate respondsToSelector:@selector(ISAudioControlManager:didChangeDeviceName:)]) {
                    //    [delegate ISAudioControlManager:self didChangeDeviceName:error];
                    //}
                    //NSLog(@"[SettingTableViewController] didChangeDeviceName");
                    if (error) {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"change name fail" message:[NSString stringWithFormat:@"error code = %d", error.code] delegate:self cancelButtonTitle:@"Close" otherButtonTitles: nil];
                        [alert show];
                        [alert release];
                        return;
                    }
                    [self checkQueuedTask];
                }
                    break;
                case AUDIO_COMMAND_ID_TTS:
                {
                    if (_tts_type == TTS_COMMAND_INSTRUCTION) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"ttsState" object:[NSString stringWithFormat:@"event TTS_COMMAND_INSTRUCTION p=%d",_tts_ins_type]];
                        if (_tts_ins_type == TTS_INSTRUCTION_PARAMETER_STOP) {
                            //Clear_TTS_Buffer = NO;
                            //TTS_cancel = NO;
                            [_tts_data release];
                            _tts_data = nil;
                            _tts_data_index = 0;
                        }
                        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(clear) object:nil];
                        return;
                    }
                    else {         //TTS_COMMAND_VOICE_DATA
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"ttsState" object:[NSString stringWithFormat:@"event TTS_VOICE_DATA,status = %d",status]];
                        if (status != 0) {
                            //[self performSelector:@selector(clearTTSBuffer) withObject:nil afterDelay:3.0];
                            if (status == 1 /*&& TTS_cancel*/)
                            {
                                /*
                                @synchronized (self) {
                                    if (!Clear_TTS_Buffer) {
                                        //[self clearTTSBuffer];
                                    }
                                    else {
                                        Clear_TTS_Buffer = NO;
                                    }
                                    if ([tts_buffer count]>0) {
                                        [tts_buffer removeAllObjects];
                                    }
                                    TTS_cancel = NO;
                                    [_tts_data release];
                                    _tts_data = nil;
                                    _tts_data_index = 0;
                                }
                                */
                            }
                            else if  (status == 4) {
                                [self performSelector:@selector(clear) withObject:nil afterDelay:3.0];
                            }
                            return;
                        }
                        else {
                            [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(clear) object:nil];
                            switch (_tts_data_type) {
                                case TTS_VOICE_DATA_PARAMETER_CANCEL: {
                                    //@synchronized (self)
                                    //{
                                    //    TTS_cancel = NO;
                                    //}
                                    //Clear_TTS_Buffer = NO;
                                }
                                    break;
                                case TTS_VOICE_DATA_PARAMETER_SINGLE:
                                case TTS_VOICE_DATA_PARAMETER_END: {
                                    [_tts_data release];
                                    _tts_data = nil;
                                    _tts_data_index = 0;
                                }
                                    break;
                                case TTS_VOICE_DATA_PARAMETER_START:
                                case TTS_VOICE_DATA_PARAMETER_CONT: {
                                    int start_index = _tts_data_index;
                                    
                                    _tts_type = TTS_COMMAND_VOICE_DATA;
                                    unsigned char buf[480+COMMAND_HEADER_SIZE+1];
                                    memset(buf, 0x00, sizeof(buf));
                                    AUDIO_COMMAND_HEADER *header = (AUDIO_COMMAND_HEADER *)buf;
                                    header->start_byte = START_BYTE;
                                    header->commandID = AUDIO_COMMAND_ID_TTS;
                                    
                                    unsigned int idx = COMMAND_HEADER_SIZE;
                                    buf[idx++] = _tts_type;
                                    buf[idx++] = _tts_data_type;
                                    [_tts_data getBytes:&buf[idx] range:NSMakeRange(start_index, _tts_data_index-start_index)];
                                    idx = idx + (_tts_data_index-start_index);
                                    header->length_high = (unsigned char)((idx - COMMAND_HEADER_SIZE + 1)>>8);
                                    header->length_low = (unsigned char)((idx - COMMAND_HEADER_SIZE + 1)&0x00ff);
                                    buf[idx] = [self calculateChecksum:&buf[1] dataLength:idx-1];
                                    //NSLog(@"start_index= %d _tts_data_index = %d idx = %02x",start_index,_tts_data_index,idx);
                                    
                                }
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
                    break;
                default:
                    break;
            }
            
            break;
        }
        case AUDIO_EVENT_ID_VERSION_REPLY:
        {
            NSString *version = [NSString stringWithFormat:@"%d.%d%d", event[COMMAND_HEADER_SIZE+1], event[COMMAND_HEADER_SIZE+2]>>4, event[COMMAND_HEADER_SIZE+2]&0x0f];
            //NSLog(@"received version, type=%02x, version=%@",event[COMMAND_HEADER_SIZE], version);
            
            //if (delegate && [(NSObject *)delegate respondsToSelector:@selector(ISAudioControlManager:didGetFirmwareVersion:error:)])
            //    [delegate ISAudioControlManager:self didGetFirmwareVersion:version error:nil];
            
            //NSLog(@"[SettingTableViewController] didGetFirmwareVersion");
            if (error) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"get firmware fail" message:[NSString stringWithFormat:@"error code = %d", error.code] delegate:self cancelButtonTitle:@"Close" otherButtonTitles: nil];
                [alert show];
                [alert release];
                return;
            }
            
            [fwVersion release];
            fwVersion = [[NSString alloc] initWithString:version];
            [self.tableView reloadData];
            
            [self MMICommand:0x00 Para2:0x3F];    //Query current EQ mode
            //[self checkQueuedTask];
            
            /*   UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"received Version event" message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
             [alert show];
             [alert release];*/
            break;
        }
        case AUDIO_EVENT_ID_CALLER_ID:
        {
            break;
        }
        case AUDIO_EVENT_ID_SoundEffect_Status:
        {
            NSString *str1 = [NSString stringWithFormat:@"%d,%d", event[COMMAND_HEADER_SIZE], event[COMMAND_HEADER_SIZE+1]];
            //NSLog(@"Sound Effect status : %@",str1);
            
            int NRState = (int)[[NSUserDefaults standardUserDefaults] boolForKey:zPreferenceKeyTXNR];
            int k = event[COMMAND_HEADER_SIZE+1];

            if(NRState != k)
            {
                [[NSUserDefaults standardUserDefaults] setBool:(BOOL)k forKey:zPreferenceKeyTXNR];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                if(k == 0)
                    NRIsEnabled = FALSE;
                else
                    NRIsEnabled = TRUE;
                
                [self.tableView reloadData];
            }
            
            
            break;
        }
        case AUDIO_EVENT_ID_EQ_MDOE:
            
            //if (delegate && [(NSObject *)delegate respondsToSelector:@selector(ISAudioControlManager:didSetEqualizerMode:)])
            //    [delegate ISAudioControlManager:self didSetEqualizerMode:nil];
            
            NSLog(@"[SettingTableViewController] didSetEqualizerMode");
            NSString *dat = [NSString stringWithFormat:@"%d", event[COMMAND_HEADER_SIZE]];
            //NSLog(@"EQ type = %@,%d",dat,[dat intValue]);
            //int eq = [[NSUserDefaults standardUserDefaults] integerForKey:zPreferenceKeyEqualizer];
            int new_eq = [dat intValue];
            if(SyncDeviceEQ != new_eq)
            {
                [[NSUserDefaults standardUserDefaults] setInteger:new_eq forKey:zPreferenceKeyEqualizer];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                SyncDeviceEQ = new_eq;
                [self.tableView reloadData];
            }
            
            if (error) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"set EQ mode fail" message:[NSString stringWithFormat:@"error code = %d", error.code] delegate:self cancelButtonTitle:@"Close" otherButtonTitles: nil];
                [alert show];
                [alert release];
                return;
            }
            
            [self MMICommand:0x00 Para2:0x78];      //Query NR(TX) status
            
            [self checkQueuedTask];
            break;
        case AUDIO_EVENT_ID_GPIO_STATUS:
            NSLog(@"[ISAudioControlManager] receive GPIO status event");
            ISGPIOControlProperty *ioProperty;
            //if (delegate && [(NSObject *)delegate respondsToSelector:@selector(ISAudioControlManager:didUpdateGPIOStatus:)]) {
                //NSMutableArray *ioStatus = [[[NSMutableArray alloc] init] autorelease];
                GPIO_EVENT_BODY *gpio = (GPIO_EVENT_BODY *)event;
                myTableViewSection *section = nil;
                myTableViewCell *cell = nil;
                for (uint8_t byte_idx = 0; byte_idx < 4; byte_idx++) {
                    for (uint8_t bit_idx = 0; bit_idx < 8; bit_idx++) {
                        if ((gpio->mask[byte_idx] & (0x01 << bit_idx)) == 0x00) {
                            ioProperty = [[[ISGPIOControlProperty alloc] init] autorelease];
                            ioProperty.index = 8*byte_idx + bit_idx;
                            if (gpio->value[byte_idx] & (0x01 << bit_idx))
                                ioProperty.value = TRUE;
                            else
                                ioProperty.value = FALSE;
                            //[ioStatus addObject:ioProperty];
                            //NSLog(@"gpio index = %d,value = %d",ioProperty.index,ioProperty.value);
                            
                            if(ioProperty.index  == indexGPIO2 || ioProperty.index == indexGPIO4)
                            {
                                if(ioProperty.index == indexGPIO2)
                                {
                                    section = [gpioList objectAtIndex:1];
                                    cell = [section.cells objectAtIndex:1];
                                }
                                else
                                {
                                    section = [gpioList objectAtIndex:3];
                                    cell = [section.cells objectAtIndex:1];
                                }
                                [[(ToggleSwitchTableViewCell *)cell.cellBody stateSwitch] setOn:ioProperty.value animated:YES];
                            }
                            
                            #ifdef ModifiedIO
                            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                            if(!([[appDelegate navigationController].topViewController.title isEqualToString:@"GPIO"]))
                            {
                                if((GPIOIndex != -1) && (GPIOIndex == ioProperty.index))
                                {
                                    if(!(GPIOIndexDir))//Input
                                    {
                                        GPIOIndexValue = ioProperty.value;
                                        [self.tableView reloadData];
                                    }
                                }
                                
                            }
                            #endif
                        }
                    }
                }
                //[delegate ISAudioControlManager:self didUpdateGPIOStatus:ioStatus];
            //}
            break;
        case AUDIO_EVENT_ID_TTS: {
            uint8_t status = event[COMMAND_HEADER_SIZE];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ttsState" object:[NSString stringWithFormat:@"event AUDIO_EVENT_ID_TTS,status = %d",status]];
            if (status == 0x01) {
                _tts_data_index = 0;
                if ([_tts_data length]< 480) {
                    _tts_data_type = TTS_VOICE_DATA_PARAMETER_SINGLE;
                    _tts_data_index = [_tts_data length];
                }
                else {
                    _tts_data_type = TTS_VOICE_DATA_PARAMETER_START;
                    _tts_data_index = 480;
                }
                _tts_type = TTS_COMMAND_VOICE_DATA;
                unsigned char buf[480+COMMAND_HEADER_SIZE+1];
                memset(buf, 0x00, sizeof(buf));
                AUDIO_COMMAND_HEADER *header = (AUDIO_COMMAND_HEADER *)buf;
                header->start_byte = START_BYTE;
                header->commandID = AUDIO_COMMAND_ID_TTS;
                
                unsigned int idx = COMMAND_HEADER_SIZE;
                buf[idx++] = _tts_type;
                buf[idx++] = _tts_data_type;
                [_tts_data getBytes:&buf[idx] length:_tts_data_index];
                idx+=_tts_data_index;
                header->length_high = (unsigned char)((idx - COMMAND_HEADER_SIZE + 1)>>8);
                header->length_low = (unsigned char)((idx - COMMAND_HEADER_SIZE + 1)&0x00ff);
                buf[idx] = [self calculateChecksum:&buf[1] dataLength:idx-1];
                //NSLog(@"_tts_data_index = %d idx = %02x",_tts_data_index,idx);
                //[self sendCommand:buf length:idx+1];
                
            }
        }
            break;
        default:
            //NSLog(@"command id = 0x%02x", header->commandID);
            break;
    }
    if ([_events length] >= COMMAND_HEADER_SIZE)
        [self parseEvents];
}

- (unsigned char)calculateChecksum:(unsigned char *)data dataLength:(unsigned short)length {
    unsigned char checksum = 0x00;
    while (length--) {
        checksum += *data++;
    }
    return (0x100 - checksum);
}

- (void)sendCommand:(unsigned char*)command length:(int)length {
    NSMutableData *data = [[NSMutableData alloc] initWithBytes:command length:length];
    uint8_t cmdID = command[COMMAND_HEADER_SIZE-1];
    if (_managerState == ISAudioControlManagerStateNormal)
    {
        NSData *data1 = [[NSData alloc] initWithBytes:command length:length];
        [_cbController LE_SendCommand:controlPeripheral MyData:data1];
        [data1 release];

        if (cmdID != AUDIO_COMMAND_ID_EVENT_ACK)
            [self checkResponseForCommand:command[COMMAND_HEADER_SIZE-1] timeout:3.0];
    }
    else
    {
        /*
        NSError *error = [[[NSError alloc] initWithDomain:@"No Connect" code:-2 userInfo:nil] autorelease];
        if (cmdID == AUDIO_COMMAND_ID_TTS) {
            [_tts_data release];
            _tts_data = nil;
            _tts_data_index = 0;
            if ([tts_buffer count]>0) {
                [tts_buffer removeAllObjects];
            }
        }
        if (cmdID == AUDIO_COMMAND_ID_GPIO_CONTROL) {
            if (gpioControlDelegate && [(NSObject *)gpioControlDelegate respondsToSelector:@selector(ISAudioControlManager:reportErrorForcmdID:error:)]) {
                [gpioControlDelegate ISAudioControlManager:self reportErrorForcmdID:cmdID error:error];
            }
        }
        else if (delegate && [(NSObject *)delegate respondsToSelector:@selector(ISAudioControlManager:reportErrorForcmdID:error:)]) {
            [delegate ISAudioControlManager:self reportErrorForcmdID:cmdID error:error];
        }
        */
    }
    
    [data release];
}

- (void)LE_GetFirmwareVersion
{
    //NSLog(@"LE_GetFirmwareVersion");
    
    unsigned char buf[25];
    memset(buf, 0x00, sizeof(buf));
    AUDIO_COMMAND_HEADER *header = (AUDIO_COMMAND_HEADER *)buf;
    header->start_byte = START_BYTE;
    header->commandID = AUDIO_COMMAND_ID_GET_VERSION;
    
    unsigned char idx = COMMAND_HEADER_SIZE;
    buf[idx++] = 0x01; //read f/w version type
    header->length_high = 0x00;
    header->length_low = idx - COMMAND_HEADER_SIZE + 1;
    buf[idx] = [self calculateChecksum:&buf[1] dataLength:idx-1];
    
    //Command   : 0xAA 0x00 0x02 0x18 0x01 0xF5
    //Response  : 0xAA 0x00 0x04 0x18 0x01 0xXX 0xXX
    
    [self sendCommand:buf length:idx+1];
}

- (void)DSPControlShortCommand:(unsigned char)cmd {
    unsigned char buf[25];
    memset(buf, 0x00, sizeof(buf));
    AUDIO_COMMAND_HEADER *header = (AUDIO_COMMAND_HEADER *)buf;
    header->start_byte = START_BYTE;
    header->commandID = AUDIO_COMMAND_ID_DSP_CONTROL;
    unsigned char idx = COMMAND_HEADER_SIZE;
    
    //    buf[idx++] = 0x00; //DSP short command
    buf[idx++] = cmd;
    
    header->length_high = 0x00;
    header->length_low = idx - COMMAND_HEADER_SIZE + 1;
    buf[idx] = [self calculateChecksum:&buf[1] dataLength:idx-1];
    _dspParamRecord = cmd;
    [self sendCommand:buf length:idx+1];
}

- (void)changeDeviceName: (NSData *)deviceName {
    //  NSMutableData *data = [[NSMutableData alloc] init];
    unsigned char buf[50];
    memset(buf, 0x00, sizeof(buf));
    AUDIO_COMMAND_HEADER *header = (AUDIO_COMMAND_HEADER *)buf;
    header->start_byte = START_BYTE;
    header->commandID = AUDIO_COMMAND_ID_CHANGE_DEVICE_NAME;
    
    unsigned char idx = COMMAND_HEADER_SIZE;
    if ([deviceName length] > 32)
        [deviceName getBytes:&buf[idx] length:32];
    else
        [deviceName getBytes:&buf[idx]];
    idx += 32;
    header->length_high = 0x00;
    header->length_low = idx - COMMAND_HEADER_SIZE + 1;
    buf[idx] = [self calculateChecksum:&buf[1] dataLength:idx-1];
    
    [self sendCommand:buf length:idx+1];
    //    NSMutableData *data = [[NSMutableData alloc] initWithBytes:buf length:idx+1];
    //    [dataPath writeData:data];
    //    [data release];
}

- (void)EQModeControlCommand:(uint8_t) mode {
    unsigned char buf[25];
    memset(buf, 0x00, sizeof(buf));
    AUDIO_COMMAND_HEADER *header = (AUDIO_COMMAND_HEADER *)buf;
    header->start_byte = START_BYTE;
    header->commandID = AUDIO_COMMAND_ID_EQ_MODE_SETTING;
    unsigned char idx = COMMAND_HEADER_SIZE;
    
    buf[idx++] = mode;
    //   buf[idx++] = 0x00;
    //   buf[idx++] = 0x00;
    
    //Command : 0xAA 0x00 0x02 0x1C mode 0xE0
    
    header->length_high = 0x00;
    header->length_low = idx - COMMAND_HEADER_SIZE + 1;
    buf[idx] = [self calculateChecksum:&buf[1] dataLength:idx-1];
    didChangeEqMode = FALSE;
    //NSLog(@"Set EQ: %x,%x,%x,%x,%x,%x,%x",buf[0],buf[1],buf[2],buf[3],buf[4],buf[5],buf[6]);
    [self sendCommand:buf length:idx+1];
}

- (void)gpioControl:(NSArray *)control {
    uint8_t buf[25];
    memset(buf, 0x00, sizeof(buf));
    GPIO_COMMAND_BODY *body = (GPIO_COMMAND_BODY *)buf;
    body->header.start_byte = START_BYTE;
    body->header.commandID = AUDIO_COMMAND_ID_GPIO_CONTROL;
    body->mask[0] = body->mask[1] = body->mask[2] = body->mask[3] = 0xff;
    uint8_t byte_idx = 0;
    uint8_t bit_idx = 0;
    ISGPIOControlProperty *ioProperty = nil;
    for (uint8_t i = 0; i < [control count]; i++) {
        ioProperty = [control objectAtIndex:i];
        //NSLog(@"[ISAudioControlManager] gpioControl inOut=%d, value=%d, index=%d", ioProperty.inOut, ioProperty.value, ioProperty.index);
        byte_idx = ioProperty.index/8;
        bit_idx = ioProperty.index%8;
        body->mask[byte_idx] &= ~(0x01 << bit_idx);
        if (ioProperty.inOut == TRUE) {
            body->inOut[byte_idx] |= 0x01 << bit_idx;
            if (ioProperty.value)
                body->value[byte_idx] |= 0x01 << bit_idx;
        }
    }
    
    uint8_t idx = sizeof(GPIO_COMMAND_BODY);
    body->header.length_high = 0x00;
    body->header.length_low = idx - COMMAND_HEADER_SIZE + 1;
    buf[idx] = [self calculateChecksum:&buf[1] dataLength:idx-1];
    
    [self sendCommand:buf length:idx+1];
}

- (void)MMICommand:(uint8_t)para1 Para2:(uint8_t)para2
{
    unsigned char buf[25];
    memset(buf, 0x00, sizeof(buf));
    AUDIO_COMMAND_HEADER *header = (AUDIO_COMMAND_HEADER *)buf;
    header->start_byte = START_BYTE;
    header->commandID = AUDIO_COMMAND_ID_MMI;
    unsigned char idx = COMMAND_HEADER_SIZE;
    
    buf[idx++] = para1;
    buf[idx++] = para2;
    
    header->length_high = 0x00;
    header->length_low = idx - COMMAND_HEADER_SIZE + 1;
    buf[idx] = [self calculateChecksum:&buf[1] dataLength:idx-1];
    
    //buf = 0xAA 0x00 0x03 0x02 0x00 0x39 0xC2
    if(para2 == 0x3f)
        NSLog(@"Query EQ mode");
    else if(para2 == 0x79)
        NSLog(@"Force buzzer alert");
    else if(para2 == 0x78)
        NSLog(@"Query NR_Tx status");
    else if(para2 == 0x77)
        NSLog(@"Query NR_Rx status");
    else
        NSLog(@"Unknown!");
    
    //NSLog(@"Send MMICommand: %x,%x,%x,%x,%x,%x,%x",buf[0],buf[1],buf[2],buf[3],buf[4],buf[5],buf[6]);
    
    [self sendCommand:buf length:idx+1];
    
}

#endif
@end
