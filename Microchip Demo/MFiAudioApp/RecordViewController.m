//
//  RecordViewController.m
//  MFiAudioAPP
//
//  Created by D500 user on 13/6/20.
//  Copyright (c) 2013年 ISSC. All rights reserved.
//

#import "RecordViewController.h"


@interface RecordViewController ()

@end

@implementation RecordViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _recorder = nil;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    // Do any additional setup after loading the view from its nib.
   // [self.recordFileTableView setBackgroundView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iSSC_background.png"]]];
    /*CGRect screenRect = [[UIScreen mainScreen] applicationFrame];
    CGRect frame = self.recordFileTableView.frame;
    frame.size.height = screenRect.size.height - 40;
  //   NSLog(@"y = %f", frame.size.height);
    self.recordFileTableView.frame = frame;
    frame = self.recordButton.frame;
   // NSLog(@"y = %f", frame.origin.y);
    frame.origin.y = self.recordFileTableView.frame.size.height;*/
   // NSLog(@"revise y = %f", self.view.superview.frame.size.height);
   // self.recordButton.frame = frame;
    _recordedFiles = [[NSMutableArray alloc] init];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_recordedFiles release];
    [_recordFileTableView release];
    [_recordButton release];
    [_audioStatusLabel release];
    [_volumeLabel1 release];
    [_volumeLabel2 release];
    [_volumeLabel3 release];
    [_volumeLabel4 release];
    [_volumeLabel5 release];
    [_volumeLabel6 release];
    [_volumeLabel7 release];
    [_volumeLabel8 release];
    [_volumeLabel9 release];
    [_volumeLabel10 release];
    [_volumeLabel11 release];
    [_volumeLabel12 release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setRecordFileTableView:nil];
    [self setRecordButton:nil];
    [self setAudioStatusLabel:nil];
    [self setVolumeLabel1:nil];
    [self setVolumeLabel2:nil];
    [self setVolumeLabel3:nil];
    [self setVolumeLabel4:nil];
    [self setVolumeLabel5:nil];
    [self setVolumeLabel6:nil];
    [self setVolumeLabel7:nil];
    [self setVolumeLabel8:nil];
    [self setVolumeLabel9:nil];
    [self setVolumeLabel10:nil];
    [self setVolumeLabel11:nil];
    [self setVolumeLabel12:nil];
    [super viewDidUnload];
}

- (void)enumerateRecordedFiles {
    
    NSString *path = [NSString stringWithFormat:@"%@/Documents",NSHomeDirectory()];
    NSDirectoryEnumerator *dirEnumerator = [[NSFileManager defaultManager] enumeratorAtPath: path];
    
    [_recordedFiles removeAllObjects];
    
    NSString *currentFile = nil;
    while (currentFile = [dirEnumerator nextObject]) {
        [_recordedFiles addObject:currentFile];
      /*  NSRange range = [currentFile rangeOfString:@".txt"];
        if (range.location != NSNotFound) {
            [dirArray addObject:currentFile];
        }*/
    }
   // NSLog(@"recorded files count = %d", [_recordedFiles count]);
}

- (void)moniterSoundLevel {
    //NSLog(@"monoterSoundLevel");
    float peakPower = -160;
    [self.volumeLabel1 setBackgroundColor:[UIColor clearColor]];
    [self.volumeLabel2 setBackgroundColor:[UIColor clearColor]];
    [self.volumeLabel3 setBackgroundColor:[UIColor clearColor]];
    [self.volumeLabel4 setBackgroundColor:[UIColor clearColor]];
    [self.volumeLabel5 setBackgroundColor:[UIColor clearColor]];
    [self.volumeLabel6 setBackgroundColor:[UIColor clearColor]];
    [self.volumeLabel7 setBackgroundColor:[UIColor clearColor]];
    [self.volumeLabel8 setBackgroundColor:[UIColor clearColor]];
    [self.volumeLabel9 setBackgroundColor:[UIColor clearColor]];
    [self.volumeLabel10 setBackgroundColor:[UIColor clearColor]];
    [self.volumeLabel11 setBackgroundColor:[UIColor clearColor]];
    [self.volumeLabel12 setBackgroundColor:[UIColor clearColor]];
    if ([_recorder isRecording]) {
        [_recorder updateMeters];
        peakPower = [_recorder peakPowerForChannel:0];
    }
    else if ([self.recordFilePlayer isPlaying]) {
        [self.recordFilePlayer updateMeters];
        peakPower = [self.recordFilePlayer peakPowerForChannel:0];
    }
    else {
        if (_meterLevelTimer) {
            _meterLevelTimer = nil;
        }
        return;
    }
   // UIColor *color = self.volumeLabel1.backgroundColor;

    if (peakPower > -100) {
        [self.volumeLabel1 setBackgroundColor:[UIColor colorWithRed:0.145 green:0.813 blue:0.969 alpha:1.0]];
    }
    if (peakPower > -80) {
        [self.volumeLabel2 setBackgroundColor:[UIColor colorWithRed:0.145 green:0.813 blue:0.969 alpha:1.0]];
    }
    if (peakPower > -60) {
        [self.volumeLabel3 setBackgroundColor:[UIColor colorWithRed:0.145 green:0.813 blue:0.969 alpha:1.0]];
    }
    if (peakPower > -50) {
        [self.volumeLabel4 setBackgroundColor:[UIColor colorWithRed:0.145 green:0.813 blue:0.969 alpha:1.0]];
    }
    if (peakPower > -40) {
        [self.volumeLabel5 setBackgroundColor:[UIColor colorWithRed:0.145 green:0.813 blue:0.969 alpha:1.0]];
    }
    if (peakPower > -30) {
        [self.volumeLabel6 setBackgroundColor:[UIColor colorWithRed:0.145 green:0.813 blue:0.969 alpha:1.0]];
    }
    if (peakPower > -20) {
        [self.volumeLabel7 setBackgroundColor:[UIColor colorWithRed:0.145 green:0.813 blue:0.969 alpha:1.0]];
    }
    if (peakPower > -10) {
        [self.volumeLabel8 setBackgroundColor:[UIColor colorWithRed:0.145 green:0.813 blue:0.969 alpha:1.0]];
    }
    if (peakPower > -5) {
        [self.volumeLabel9 setBackgroundColor:[UIColor colorWithRed:0.145 green:0.813 blue:0.969 alpha:1.0]];
    }
    if (peakPower > -3) {
        [self.volumeLabel10 setBackgroundColor:[UIColor colorWithRed:1 green:0.827 blue:0.174 alpha:1.0]];
    }
    if (peakPower > -1) {
        [self.volumeLabel11 setBackgroundColor:[UIColor colorWithRed:1 green:0.255 blue:0.215 alpha:1.0]];
    }
    if (peakPower > -0.2) {
        [self.volumeLabel12 setBackgroundColor:[UIColor colorWithRed:1 green:0. blue:0 alpha:1.0]];
    }
    _meterLevelTimer = [NSTimer scheduledTimerWithTimeInterval:0.03 target:self selector:@selector(moniterSoundLevel) userInfo:nil repeats:NO];
   // NSLog(@"sound level = %f", peakPower);
}
/*
- (void)stopSoundLevelMeter {
    [_meterLevelTimer invalidate];
    _meterLevelTimer = nil;
    [self.volumeLabel1 setBackgroundColor:[UIColor clearColor]];
    [self.volumeLabel2 setBackgroundColor:[UIColor clearColor]];
    [self.volumeLabel3 setBackgroundColor:[UIColor clearColor]];
    [self.volumeLabel4 setBackgroundColor:[UIColor clearColor]];
    [self.volumeLabel5 setBackgroundColor:[UIColor clearColor]];
    [self.volumeLabel6 setBackgroundColor:[UIColor clearColor]];
    [self.volumeLabel7 setBackgroundColor:[UIColor clearColor]];
    [self.volumeLabel8 setBackgroundColor:[UIColor clearColor]];
    [self.volumeLabel9 setBackgroundColor:[UIColor clearColor]];
    [self.volumeLabel10 setBackgroundColor:[UIColor clearColor]];
    [self.volumeLabel11 setBackgroundColor:[UIColor clearColor]];
    [self.volumeLabel12 setBackgroundColor:[UIColor clearColor]];
}*/

- (void)allowBluetoothAudioInput:(uint8_t)value {
    UInt32 allowBluetoothInput = value;
    
    AudioSessionSetProperty(kAudioSessionProperty_OverrideCategoryEnableBluetoothInput, sizeof(allowBluetoothInput), &allowBluetoothInput);
}

- (void)startRecord:(NSString *)fileName {
    AVAudioSession *session = [AVAudioSession sharedInstance];
    
    NSError *sessionError;
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:&sessionError];
    
    if(session == nil)
        NSLog(@"Error creating session: %@", [sessionError description]);
    else
        [session setActive:YES error:nil];
    [self allowBluetoothAudioInput:1];
    
    //[self.recordButton setTitle:@"STOP" forState:UIControlStateNormal];
    NSString *path = [NSString stringWithFormat:@"%@/Documents/%@",NSHomeDirectory(), fileName];
    NSURL *recordedFile = [NSURL fileURLWithPath:path];
    NSDictionary *recordSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [NSNumber numberWithInt: kAudioFormatAppleLossless], AVFormatIDKey,
                                    [NSNumber numberWithFloat:8000.0], AVSampleRateKey,
                                    [NSNumber numberWithInt: 1], AVNumberOfChannelsKey,
                                    nil];
    NSError *error = nil;
    _recorder = [[AVAudioRecorder alloc] initWithURL:recordedFile settings:recordSettings error:&error];
    if (error) {
        NSLog(@"record error = %@", [error localizedDescription]);
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[error localizedDescription] delegate:self cancelButtonTitle:@"Close" otherButtonTitles: nil];
        [alert show];
        [alert release];
        return;
    }
    [_recorder prepareToRecord];
    [_recorder setMeteringEnabled:YES];
    [_recorder record];
    _meterLevelTimer = [NSTimer scheduledTimerWithTimeInterval:0.03 target:self selector:@selector(moniterSoundLevel) userInfo:nil repeats:NO];
    [self changeRecordButtonTitleState:NO];
    [self.audioStatusLabel setText:[NSString stringWithFormat:@"Recording %@", fileName]];
/*    CGRect frame = self.recordButton.frame;
    frame.origin.y += 1;
    frame.size.height -= 3;
    frame.size.width -= 3;
    self.recordButton.frame = frame;*/
}

- (void)stopRecording {
    [_recorder setMeteringEnabled:NO];
    //[self stopSoundLevelMeter];
    [self changeRecordButtonTitleState:YES];
    [self.audioStatusLabel setText:@""];
  /*  CGRect frame = self.recordButton.frame;
    frame.origin.y -= 1;
    frame.size.height += 3;
    frame.size.width += 3;
    self.recordButton.frame = frame;*/
    [_recorder stop];
    [_recorder release];
    _recorder = nil;
}

- (void)startPlay:(NSString *) fileName {
    if (_meterLevelTimer) {
        [_meterLevelTimer invalidate];
        _meterLevelTimer = nil;
    }
    if (self.recordFilePlayer)
        [self.recordFilePlayer release];
    AVAudioSession *session = [AVAudioSession sharedInstance];
    
    NSError *sessionError;

    [session setCategory:AVAudioSessionCategoryPlayback error:&sessionError];
    if(session == nil)
        NSLog(@"Error creating session: %@", [sessionError description]);
    else
        [session setActive:YES error:nil];
    NSError *playerError;
    //  [self allowBluetoothAudioInput:0];
    //player = [[AVAudioPlayer alloc] initWithContentsOfURL:recordedFile error:&playerError];
    
    NSURL *url = [NSURL fileURLWithPath:[[NSString alloc] initWithFormat:@"%@/Documents/%@",NSHomeDirectory(), fileName]];
    self.recordFilePlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&playerError];
    if (self.recordFilePlayer == nil)
    {
        NSLog(@"ERror creating player: %@", [playerError description]);
    }
    self.recordFilePlayer.delegate = self;
    [self.recordFilePlayer setMeteringEnabled:YES];
    [self.recordFilePlayer play];
    [self changeRecordButtonTitleState:NO];
    _meterLevelTimer = [NSTimer scheduledTimerWithTimeInterval:0.03 target:self selector:@selector(moniterSoundLevel) userInfo:nil repeats:NO];
    [self.audioStatusLabel setText:[NSString stringWithFormat:@"Playing %@", fileName]];
}

- (void)stopPlaying {
    [self.recordFilePlayer setMeteringEnabled:NO];
    [self.recordFilePlayer stop];
    [self changeRecordButtonTitleState:YES];
    //[self stopSoundLevelMeter];
    [self.audioStatusLabel setText:@""];
}

- (void)changeRecordButtonTitleState:(BOOL)state {
    if (state) {
        [self.recordButton setImage:[UIImage imageNamed:@"record.png"] forState:UIControlStateNormal];
        [self.recordButton setTitle:@"Record" forState:UIControlStateNormal];
    }
    else {
        [self.recordButton setImage:[UIImage imageNamed:@"stop.png"] forState:UIControlStateNormal];
        [self.recordButton setTitle:@" Stop " forState:UIControlStateNormal];
    }
}

- (IBAction)recordVoice:(id)sender {
    
    if (_recordFilePlayer.isPlaying) {
        [self stopPlaying];
    }
    else if ((_recorder == nil) || (_recorder.isRecording == NO))
    {
        NSString *currentVersion = [[UIDevice currentDevice] systemVersion];
        NSLog(@"version = %f", [currentVersion floatValue]);
        if ([currentVersion floatValue] >= 5.0) {
            _recordAlertView = [[UIAlertView alloc] initWithTitle:@"Record" message:@"Input the file name (.m4a)" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Record", nil];
            [_recordAlertView setAlertViewStyle:UIAlertViewStylePlainTextInput];
            [[_recordAlertView textFieldAtIndex:0] setText:[NSString stringWithFormat:@"%d.m4a",(uint32_t)[[NSDate date] timeIntervalSince1970]]];
            // [[_recordAlertView textFieldAtIndex:0] setText:[NSString stringWithFormat:@"%@.wav",[[NSDate date] description]]];
            [_recordAlertView show];
        }
        else {
            _recordAlertView = [[UIAlertView alloc] initWithTitle:@"Record" message:@"Start to recording?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Record", nil];
            [_recordAlertView show];
        }
    //    NSLog(@"height = %f, %f", self.recordFileTableView.frame.size.height, self.recordButton.frame.origin.y);
    }
    else
    {
        [self stopRecording];
        
        [self.recordFileTableView reloadData];
    }
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    [self.recordFilePlayer setMeteringEnabled:NO];
    //[self stopSoundLevelMeter];
    [self.audioStatusLabel setText:@""];
    [self changeRecordButtonTitleState:YES];
}

- (void)deleteRecordedFile:(id)sender {
    NSLog(@"deleteRecordedFile");
    UIButton *button = (UIButton *)sender;
    self.willDeleteRecordedFile = [_recordedFiles objectAtIndex:button.tag];
    NSString *str = [NSString stringWithFormat:@"Delete %@?", [_recordedFiles objectAtIndex:button.tag]];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Confirmation" message:str delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes", nil];
    [alertView show];
    [alertView release];
}

#pragma UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (alertView == _recordAlertView) {
        if (buttonIndex == 1) { //click record button
            NSString *currentVersion = [[UIDevice currentDevice] systemVersion];
            if ([currentVersion floatValue] >= 5.0) {
                [self startRecord:[[_recordAlertView textFieldAtIndex:0] text]];
            }
            else {
                [self startRecord:[NSString stringWithFormat:@"%d.m4a",(uint32_t)[[NSDate date] timeIntervalSince1970]]];
            }
        }
        [_recordAlertView release];
        _recordAlertView = nil;
    }
    else {
       // delete certain file;
        if (buttonIndex == 1) {
            [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@/Documents/%@",NSHomeDirectory(), self.willDeleteRecordedFile] error:NULL];
            [self.recordFileTableView reloadData];
        }
        
    }
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    //   if (tableView == equalizerTableView.tableView)
    return 55;
    //   else
    //     return 65;
}

- (NSString *)tableView:(UITableView*)tableView titleForHeaderInSection:(NSInteger)section {
    return @"Recorded Files:";
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    [self enumerateRecordedFiles];
    return [_recordedFiles count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell =
    (UITableViewCell*) [tableView dequeueReusableCellWithIdentifier:[_recordedFiles objectAtIndex:indexPath.row]];
	if (cell == nil) {
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:[_recordedFiles objectAtIndex:indexPath.row]] autorelease];
    }
	//NSLog(@"index = %d", indexPath.row);
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    CGRect frame = CGRectMake(0.0, 0.0, 32, 32);
    button.frame = frame;
    [button setBackgroundImage:[UIImage imageNamed:@"delete.png"] forState:UIControlStateNormal];
    button.tag = indexPath.row;
    [button addTarget:self action:@selector(deleteRecordedFile:)  forControlEvents:UIControlEventTouchUpInside];
    button.backgroundColor = [UIColor clearColor];
    cell.accessoryView = button;
    
    cell.textLabel.text = [_recordedFiles objectAtIndex:indexPath.row];
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"didSelectRowAtIndexPath");
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if ((_recorder == nil) || (_recorder.isRecording == NO))
        [self startPlay:[_recordedFiles objectAtIndex:indexPath.row]];
}
@end
