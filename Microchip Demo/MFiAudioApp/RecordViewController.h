//
//  RecordViewController.h
//  MFiAudioAPP
//
//  Created by D500 user on 13/6/20.
//  Copyright (c) 2013年 ISSC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface RecordViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, AVAudioPlayerDelegate, UIAlertViewDelegate>
{
    AVAudioRecorder *_recorder;
    NSTimer *_meterLevelTimer;
    //NSFileManager *_fileManager;
    NSMutableArray *_recordedFiles;
    UIAlertView *_recordAlertView;
    UIAlertView *_deleteAlertView;
}
@property (retain, nonatomic) IBOutlet UILabel *volumeLabel1;
@property (retain, nonatomic) IBOutlet UILabel *volumeLabel2;
@property (retain, nonatomic) IBOutlet UILabel *volumeLabel3;
@property (retain, nonatomic) IBOutlet UILabel *volumeLabel4;
@property (retain, nonatomic) IBOutlet UILabel *volumeLabel5;
@property (retain, nonatomic) IBOutlet UILabel *volumeLabel6;
@property (retain, nonatomic) IBOutlet UILabel *volumeLabel7;
@property (retain, nonatomic) IBOutlet UILabel *volumeLabel8;
@property (retain, nonatomic) IBOutlet UILabel *volumeLabel9;
@property (retain, nonatomic) IBOutlet UILabel *volumeLabel10;
@property (retain, nonatomic) IBOutlet UILabel *volumeLabel11;
@property (retain, nonatomic) IBOutlet UILabel *volumeLabel12;
@property (retain, nonatomic) IBOutlet UITableView *recordFileTableView;
@property (retain, nonatomic) IBOutlet UIButton *recordButton;
@property (retain, nonatomic) AVAudioPlayer *recordFilePlayer;
@property (retain, nonatomic) IBOutlet UILabel *audioStatusLabel;
@property (retain, nonatomic) NSString *willDeleteRecordedFile;
//@property (retain, nonatomic) NSString *recordPath;
- (IBAction)recordVoice:(id)sender;
- (void)moniterSoundLevel;
- (void)allowBluetoothAudioInput:(uint8_t)value;
- (void)enumerateRecordedFiles;
- (void)startPlay: fileName;
- (void)changeRecordButtonTitleState:(BOOL)state;
- (void)deleteRecordedFile:(id)sender;
@end
