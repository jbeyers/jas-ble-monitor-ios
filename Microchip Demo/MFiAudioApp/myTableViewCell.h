//
//  myTableViewCell.h
//  MFiAudioAPP
//
//  Created by D500 user on 13/4/25.
//  Copyright (c) 2013年 ISSC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface myTableViewCell : NSObject

@property (retain) NSString *itemName;
@property (nonatomic) UITableViewCellAccessoryType accessoryType;
@property (assign) SEL eventSelector;
@property (retain) UITableViewCell *cellBody;
@property (assign) int cellIdentifier;
@end
