//
//  GPIODemoViewController.h
//  MFiAudioAPP
//
//  Created by D500 user on 13/6/6.
//  Copyright (c) 2013年 ISSC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QBFlatButton.h"
#import "ISAudioControlManager.h"

@interface GPIODemoViewController : UIViewController <ISAudioControlManagerDelegate>{
    BOOL _inputButton1Status;
    BOOL _inputButton2Status;
    BOOL _LEDSwitch1Status;
    BOOL _LEDSwitch2Status;
    NSTimer *LEDPatternTimer;
}
@property (retain, nonatomic) IBOutlet UIImageView *LED1Image;
@property (retain, nonatomic) IBOutlet UIImageView *LED2Image;
@property (retain, nonatomic) IBOutlet UISwitch *LEDSwitch1;
@property (retain, nonatomic) IBOutlet UISwitch *LEDSwitch2;
@property (retain, nonatomic) IBOutlet QBFlatButton *inputButton1;
@property (retain, nonatomic) IBOutlet QBFlatButton *inputButton2;
@property (retain, nonatomic) ISAudioControlManager *audioControl;
- (IBAction)LEDSwitchValueChange1:(id)sender;
- (IBAction)LEDSwitchValueChange2:(id)sender;
- (void)LEDcontrollerPattern1;
- (void)LEDcontrollerPattern2;
- (void)LEDcontrollerPatternStop;
- (void)synchronizeIOState;
- (void)updateButtonDetection:(NSArray *)ioState;
- (void)setLEDImage:(UIImageView *)imageView status:(BOOL)status;
@end
