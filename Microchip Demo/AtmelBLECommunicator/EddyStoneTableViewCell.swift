/***************************************************************************
 
 EddystoneTabelViewCell.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import UIKit

protocol EddystoneTableCellDelegate{
    func connectButtonClickedAtCellWithIndex(_ index : Int)
    func disconnectButtonClickedAtCellWithIndex(_ index : Int)
}

class EddyStoneTableViewCell: UITableViewCell {
    
    @IBOutlet weak var beaconNameLabel: UILabel!
    @IBOutlet weak var connectButton: UIButton!
    
    var delegate : EddystoneTableCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        connectButton.setTitleColor(UIColor.AtmelDarkGrayColor(), for: .selected)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
    
    @IBAction func connectButtonClicked(_ sender: UIButton) {
        
        if  sender.isSelected == false {
            
            if delegate != nil{
                delegate?.connectButtonClickedAtCellWithIndex(sender.tag)
            }
            sender.isSelected = true
        }
        else{
            
            if delegate != nil {
                delegate?.disconnectButtonClickedAtCellWithIndex(sender.tag)
            }
            sender.isSelected = false
        }
        
        
    }
    
    
    
    
    

}
