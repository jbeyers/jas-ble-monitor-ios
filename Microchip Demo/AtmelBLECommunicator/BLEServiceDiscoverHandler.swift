/***************************************************************************
 
 BLEServiceDiscoverHandler.swift
  
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import UIKit
import CoreBluetooth


protocol BLEServicesDiscoverHandlerDelegate{
    func discoveredDeviceServicesWithStatus(_ status : Bool)
}

class BLEServiceDiscoverHandler: NSObject {
    
    var myDevice : CBPeripheral?
    var delegate : BLEServicesDiscoverHandlerDelegate?
    
    init(connectedDevice : CBPeripheral) {
        
        super.init()
        myDevice = connectedDevice
        getServiceList()
    }
    
    
    func getServiceList()
    {
        NSLog("Getting service List");
        if myDevice?.state == CBPeripheralState.connected
        {
            if let _ = myDevice
            {
                //NSLog("Getting all services and Characteristics from = \(myDevice)");
                
                GlobalCommunicator.shared.sourceDevice = BLESourceDevice( peripheral: myDevice!, withDelegate: self)
                GlobalCommunicator.shared.sourceDevice.getAllServicesAndCharactesristics()
            }
        }
    }
}


extension BLEServiceDiscoverHandler: BLESourceDeviceDelegate
{
    func bleSourceDevice(_ bleSourceDevice: BLESourceDevice, didDiscoverServicesWithError error: NSError!) {
        
        if error == nil
        {
        }
        
    }
    
    func bleSourceDevice(_ bleSourceDevice: BLESourceDevice, didDiscoverCharacteristicsForService service: CBService, withError error: NSError!)
    {
        if error == nil
        {
            NSLog("didDiscoverCharacteristicsForService")
        }
    }
    
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice, didDiscoverAllServicesAndCharacteristicsWithError error:NSError!){
        
        if error == nil
        {
            if delegate != nil{
                delegate?.discoveredDeviceServicesWithStatus(true)
            }
        }
        else{
            if delegate != nil{
                delegate?.discoveredDeviceServicesWithStatus(false)
            }
        }
        
    }
    
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice, DidInitializedOTAService isOTAPresent: Bool){
        ///Igonere
    }

    
  
}
