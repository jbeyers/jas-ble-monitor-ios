
/***************************************************************************
 
 PowerDetailTableViewCell.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/



import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


enum CellCategory : Int{
    case categoryPowerLevelCell
    case categoryPowerModeCell
}


enum PowerModeValues: Int{
    
    case lowest
    case lower
    case medium
    case high
    
    var mappedValues :[String]{
        
        switch self{
        case .lowest :
            return ["10", "20", "30"]
        case .lower :
            return ["40", "50", "60"]
        case .medium :
            return ["70", "80", "90"]
        case .high :
            return ["100", "120", "127"]
        }
    }
}




protocol PowerDetailTableCellDelegate{
    
    func writeToBeaconWithPowerLevel(_ powerLevelsArray : [Int8], field : BeaconConfigurationFields)
    func writeToBeaconWithPowerMode(_ powerMode : TxPowerModes, field : BeaconConfigurationFields)
}


class PowerDetailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var headerLabel          : UILabel!
    @IBOutlet weak var writeButton          : UIButton!
    
    @IBOutlet weak var lowestPowerLevelTextField: UITextField!
    @IBOutlet weak var lowPowerLevelTextField: UITextField!
    @IBOutlet weak var mediumPowerLevelTextField: UITextField!
    @IBOutlet weak var highPowerLevelTextField: UITextField!

    @IBOutlet weak var powerModeLabel: UILabel!
    @IBOutlet weak var powerLevelView: UIView!
    @IBOutlet weak var powerModeView: UIView!
    
    @IBOutlet weak var alertLabel: UILabel!
    
    fileprivate var dropDownView : DropDown?
    let powerModeOptions = NSArray(array: ["Lowest", "Lower", "Medium", "High"])
    fileprivate var selectedModeValue : PowerModeValues?
    fileprivate var selectedPowerLevelArray : [Int8]? = [0,0,0,0]

    
    var cellMode : CellCategory?{
        didSet{
            
            if cellMode == .categoryPowerLevelCell{
                powerLevelView.isHidden = false
                powerModeView.isHidden = true
                addObservers()
            }
            else{
                powerLevelView.isHidden = true
                powerModeView.isHidden = false
            }
        }
    }
    
    var parentTableView : UITableView?
    var delegate : PowerDetailTableCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        lowestPowerLevelTextField.delegate  = self
        lowPowerLevelTextField.delegate     = self
        mediumPowerLevelTextField.delegate  = self
        highPowerLevelTextField.delegate    = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
        
    @IBAction func writeButtonClicked(_ sender: UIButton) {
        
        if delegate != nil{
            
            if cellMode! == .categoryPowerLevelCell{
                
                
                let textFieldArray = [lowestPowerLevelTextField, lowPowerLevelTextField, mediumPowerLevelTextField, highPowerLevelTextField]
                var isTextFieldEmpty = false
                
                // Check whether any of the textfields are empty
                for textField in textFieldArray{
                    if textField?.text == ""{
                        textField?.backgroundColor = INVALID_RED
                        isTextFieldEmpty = true
                    }
                }
                
                if !isTextFieldEmpty{
                    
                    let lowestPowervalue    = Int(lowestPowerLevelTextField.text!)
                    let lowPowerValue       = Int(lowPowerLevelTextField.text!)
                    let mediumPowervalue    = Int(mediumPowerLevelTextField.text!)
                    let highPowerValue      = Int(highPowerLevelTextField.text!)
                    
                    
                    let powerLevelArray = [lowestPowervalue!, lowPowerValue!, mediumPowervalue!, highPowerValue!]
                    
                    // Check whether all the values fall in the valid range
                    if isPowerLevelValid(powerLevelArray){
                        
                        selectedPowerLevelArray = [Int8(lowestPowervalue!), Int8(lowPowerValue!), Int8(mediumPowervalue!), Int8(highPowerValue!)]
                        
                        // Check whether the power values entered are in the increaing order. Otherwise show error
                        if lowestPowervalue < lowPowerValue && lowPowerValue < mediumPowervalue && mediumPowervalue < highPowerValue{
                            
                            if selectedPowerLevelArray != nil{
                                delegate?.writeToBeaconWithPowerLevel(selectedPowerLevelArray!, field: .txPowerLevel)
                            }
                        }
                        else{
                            
                            let alertVC = UIAlertController(title: "ERROR!", message: "Please enter the power level values in the increasing order", preferredStyle: .alert)
                            let alertAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                            alertVC.addAction(alertAction)
                            
                            if self.parentViewController != nil{
                                self.parentViewController?.present(alertVC, animated: true, completion: nil)
                            }                        }
                    }
                    else{
                        
                        
                        let alertVC = UIAlertController(title: "ERROR!", message: "Power level values must be in between 20dB and -100dB", preferredStyle: .alert)
                        let alertAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                        alertVC.addAction(alertAction)
                        
                        if self.parentViewController != nil{
                            self.parentViewController?.present(alertVC, animated: true, completion: nil)
                        }                    }
                }
                
            }
            else{
                if selectedModeValue != nil{
                    
                    let selectedMode = TxPowerModes(rawValue: UInt8(selectedModeValue!.rawValue))
                    delegate?.writeToBeaconWithPowerMode(selectedMode!, field: .txPowerMode)
                }
            }
        }
    }
    
    
    func isPowerLevelValid( _ levelArray :[Int])-> Bool{
        
        var isValid : Bool = true
        
        for value in levelArray{
            
            if value > 20 || value < -100{
                isValid = false
            }
        }
        return isValid
    }
    
    
    @IBAction func powerModeSelectionButtonClicked(_ sender: UIButton) {
        
        if !sender.isSelected{
            let rect = powerModeLabel.frame
            showDropDownWithElements(powerModeOptions, fromRect: CGRect(x: powerModeLabel.superview!.frame.origin.x + rect.origin.x, y: powerModeLabel.superview!.superview!.frame.origin.y + powerModeLabel.superview!.frame.origin.y + rect.origin.y , width: rect.width, height: rect.height), parentButton: sender)
        }
        else{
            dropDownView?.removeDropDown()
        }
    }
    
    
    
    func setPowerLevelWith(_ powerLevelArray:[Int8]){
        
        assignPowerLevelValuesToLabelsWith(powerLevelArray)
        selectedPowerLevelArray = powerLevelArray
    }
    
    func setTransmissionPowermode(_ mode : String){
     
        powerModeLabel.text = mode
        selectedModeValue =  PowerModeValues(rawValue: powerModeOptions.index(of: mode))
    }
    
    func addObservers() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(PowerDetailTableViewCell.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(PowerDetailTableViewCell.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func removeObservers(){
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWillShow(_ notification:Notification)
    {
        if let userInfo = notification.userInfo
        {
            if let keyboardRect = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            {
                
                let y = mediumPowerLevelTextField.frame.origin.y + mediumPowerLevelTextField.frame.height + self.frame.origin.y +  mediumPowerLevelTextField.superview!.frame.origin.y + 150
                
                let keyboardY = UIApplication.shared.keyWindow!.frame.size.height - keyboardRect.size.height
                
                if  y > keyboardY {
                    
                    if UIDevice.is_iPad() && UIDevice.is_Landscape() {
                        parentTableView?.contentOffset = CGPoint(x: 0, y: y - keyboardY + 60)
                    }
                    else{
                        parentTableView?.contentOffset = CGPoint(x: 0, y: y - keyboardY + 20)
                    }

                }
                
            }
        }
    }
    
    func keyboardWillHide(_ notification:Notification)
    {
        parentTableView?.contentOffset = CGPoint.zero
    }

    
    
}



extension PowerDetailTableViewCell {
    
    fileprivate func showDropDownWithElements(_ contentArray: NSArray, fromRect rect:CGRect, parentButton button: UIButton){
        
        if (dropDownView != nil)
        {
            dropDownView?.removeDropDown()
        }
                
        dropDownView = DropDown().initDropDownWith(CGRect(x: 0, y: 0, width: rect.size.width, height: rect.height * 4), elements: contentArray, button: button)
        dropDownView?.delgate = self
        dropDownView?.showDropDownFrom(CGRect(x: self.frame.origin.x + rect.origin.x, y: self.frame.origin.y + rect.origin.y, width: rect.width, height: rect.height), view: self.superview!)
    }
    
    
    fileprivate func assignPowerLevelValuesToLabelsWith(_ values :[Int8]?){
        
        let textFieldArray = [lowestPowerLevelTextField, lowPowerLevelTextField, mediumPowerLevelTextField, highPowerLevelTextField]
        for i in 0 ..< values!.count  {
            
            let textField = textFieldArray[i]
            textField?.text = "\(values![i])"
        }
    }
}

extension PowerDetailTableViewCell : DropDownDelegate{
    
    
    func didselectRowIndropDownWith(_ element: String){
        
        if cellMode! == .categoryPowerLevelCell{

        }
        else{
            selectedModeValue =  PowerModeValues(rawValue: powerModeOptions.index(of: element))
            powerModeLabel.text = powerModeOptions[selectedModeValue!.rawValue] as? String
        }
    }
}


extension PowerDetailTableViewCell : UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        textField.backgroundColor = UIColor.clear
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        // return true if the replacementString only contains numeric characters
        let aSet = CharacterSet(charactersIn:"0123456789-").inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        let length = textField.text!.utf16.count + string.utf16.count - range.length


        if string == "-" && length != 1{
            return false
        }
        
        
        if length <= 4 && string == numberFiltered {
            return true
        }
        else{
            return false
        }
    }
}


