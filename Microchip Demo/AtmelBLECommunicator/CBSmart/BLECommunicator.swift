/* ***************************************************************************

BLECommunicator.swift
//
Interface class for implementing actions and handling responses of Bluetooth Central
//

Copyright (c) 2015, Atmel Corporation. All rights reserved.
Released under NDA
Licensed under Atmel's Limited License Agreement.


THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Atmel Corporation: http://www.atmel.com

******************************************************************************/

import Foundation
import CoreBluetooth

enum ScanMode:Int{
    case active=0
    case passive=1
}
enum BluetoothState:String {
    
    case Unsupported="Unsupported"
    case Unauthorized="Unauthorized"
    case PoweredOff="PoweredOff"
    case PoweredOn="PoweredOn"
    case Unknown="Unknown"
    case Resetting="Resetting"
}

protocol BLECommunicatorDelegate:NSObjectProtocol{
    
    func bleCommunicator(_ blecommunicator:BLECommunicator ,communicatorInitializationSuccessWithBluetoothState state:BluetoothState)
    func bleCommunicator(_ blecommunicator:BLECommunicator ,communicatorInitializationFailWithBluetoothState state:BluetoothState)
    
    
    func bleCommunicator(_ bleCommunicator: BLECommunicator!, didDiscoverPeripheral peripheral: CBPeripheral!, advertisementData: [AnyHashable: Any]!, RSSI: NSNumber!)
    func bleCommunicator(_ bleCommunicator: BLECommunicator!,didRetrievePeripherals peripherals: [AnyObject]!)
    func bleCommunicator(_ bleCommunicator:BLECommunicator!,didDiscoverDevice deviceInfo: BLEDeviceInfo! )
    
    
    func bleCommunicator(_ bleCommunicator:BLECommunicator!,didConnectDevice device: CBPeripheral!)
    func bleCommunicator(_ bleCommunicator:BLECommunicator!,didFailToConnectDevice device: CBPeripheral!)
    func bleCommunicator(_ bleCommunicator:BLECommunicator!,didDisconnectDevice device: CBPeripheral! , error: NSError!)
   
}


class BLECommunicator: NSObject  {
    
    var CBSManager: CBCentralManager!
    var delegate:BLECommunicatorDelegate?
    var scanMode:ScanMode?
    var selectedProfiles:Array<BLEProfile>?



    let TIME_OUT_DELAY = Constants.TIME_OUT_DELAY
    
    
    
    override init()
    {
        super.init()
        self.scanMode = .active
        CBSManager = CBCentralManager(delegate: self, queue: nil)
        print("CentralManager is initialized")
        
    }
    
    convenience init(scanmode:ScanMode, forBLEProfiles bleProfiles:[BLEProfile]? , withDelegate delegate:BLECommunicatorDelegate?)
    {
        
        self.init()
        self.selectedProfiles = bleProfiles
        self.delegate = delegate
        self.scanMode = scanmode
       
    }

    /*----------------------------------------------------------------------------------------------------------*/
    // MARK:
    // MARK: Scanning
    /*----------------------------------------------------------------------------------------------------------*/
    
    /*!
    *  @method startScan:
    *  @param serviceEnum      A enum value indicates the scanning method.
    *  @discussion   Scans for peripherals that are advertising services. serviceEnum can be allService enum or specified enum. Depends on the serviceEnum the central starts scan.
    */
    func startScan(_ bleprofiles:[BLEProfile]?)
    {
        
        //if CBSManager.state == CBCentralManagerState.poweredOn
        if #available(iOS 10.0, *) {
            if CBSManager.state == CBManagerState.poweredOn
            {
                stopScan()
                
                //CBSManager.scanForPeripheralsForProfiles(bleprofiles, options: [CBCentralManagerScanOptionAllowDuplicatesKey:true])
                CBSManager.scanForPeripheralsForProfiles(bleprofiles, options: [CBCentralManagerScanOptionAllowDuplicatesKey:true as AnyObject])
                /*
                 
                 let connectedBLEPeripherals = CBSManager.retrieveConnectedPeripheralsWithServices([Util.getServiceIDFromserviceEnum(serviceEnum)])
                 if connectedBLEPeripherals.count > 0 
                 {
                 println("Already Connected !")
                 }
                 
                 */
                
            }
        } else {
            // Fallback on earlier versions
            if CBSManager.state.rawValue == 5
            {
                stopScan()
                
                //CBSManager.scanForPeripheralsForProfiles(bleprofiles, options: [CBCentralManagerScanOptionAllowDuplicatesKey:true])
                CBSManager.scanForPeripheralsForProfiles(bleprofiles, options: [CBCentralManagerScanOptionAllowDuplicatesKey:true as AnyObject])
                /*
                 
                 let connectedBLEPeripherals = CBSManager.retrieveConnectedPeripheralsWithServices([Util.getServiceIDFromserviceEnum(serviceEnum)])
                 if connectedBLEPeripherals.count > 0
                 {
                 println("Already Connected !")
                 }
                 
                 */
                
            }

        }
    }
    
    
    //Asks the central manager to stop scanning for peripherals.
    func stopScan()
    {
        CBSManager.stopScan()
        print("Stop scan")
    }
    
    /*----------------------------------------------------------------------------------------------------------*/
    // MARK:
    // MARK: Connection
    /*----------------------------------------------------------------------------------------------------------*/
    
    func connect(_ bleDevice : CBPeripheral, timeOut : Bool = true )
    {
        CBSManager.cancelPeripheralConnection(bleDevice)
        CBSManager.connect(bleDevice, options: [CBConnectPeripheralOptionNotifyOnDisconnectionKey:false])
        NSLog("connecting to peripheral \(bleDevice)")
        //To handle connection timeout
        if timeOut == true
        {
            triggerTimeOutMethod(bleDevice)
        }
    }
    
    func disconnect(_ bleDevice : CBPeripheral)
    {
        
        print("Forcefully disconnecting peripheral = \(bleDevice)")
        CBSManager.cancelPeripheralConnection(bleDevice)
    }
    

    
    /*----------------------------------------------------------------------------------------------------------*/
    // MARK:
    // MARK: Timed Out
    /*----------------------------------------------------------------------------------------------------------*/
    
    // To cancel the connection request after desired time delay /"connectPeripheral()" method never timed out .
    func triggerTimeOutMethod(_ bleDevice : CBPeripheral)
    {
        Util.delay(TIME_OUT_DELAY)
        {
            if bleDevice.state != CBPeripheralState.connected
            {
                print("Delay after \(self.TIME_OUT_DELAY) seconds")
                
                DispatchQueue.main.async {
                    self.CBSManager.cancelPeripheralConnection(bleDevice)
                }
            }
            
        }
    }
    
    /*----------------------------------------------------------------------------------------------------------*/
    // MARK:
    // MARK:
    /*----------------------------------------------------------------------------------------------------------*/

    
    

    
}




extension BLECommunicator : CBCentralManagerDelegate
{
    /*----------------------------------------------------------------------------------------------------------*/
    // MARK: Peripheral Discovery
    /*----------------------------------------------------------------------------------------------------------*/
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        
        self.delegate?.bleCommunicator(self, didDiscoverPeripheral: peripheral, advertisementData: advertisementData, RSSI: RSSI)

        let deviceInfo = BLEDeviceInfo(peripheral: peripheral, advertisementData: advertisementData as [NSObject : AnyObject], RSSI: RSSI)
        self.delegate?.bleCommunicator(self, didDiscoverDevice: deviceInfo)

       
        
        
    }
    
    func centralManager(_ central: CBCentralManager!, didRetrievePeripherals peripherals: [AnyObject]!){
        
        self.delegate?.bleCommunicator(self, didRetrievePeripherals: peripherals)
    }

    /*----------------------------------------------------------------------------------------------------------*/
    // MARK: Peripheral Connection
    /*----------------------------------------------------------------------------------------------------------*/

    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral)
    {
        NSLog("Connected to peripheral = \(peripheral)")
        
        self.delegate?.bleCommunicator(self, didConnectDevice: peripheral)
    }
    
    
    
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        
        NSLog("didFailToConnectPeripheral with error \(String(describing: error?.localizedDescription))")
        self.delegate?.bleCommunicator(self, didFailToConnectDevice: peripheral)
        
    }     
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        //NSLog("didDisconnectPeripheral for error = \(error?.localizedDescription) and code \(error?.code) for peripheral \(peripheral) calling delegate \(self.delegate)")
//        if peripheral != nil
        self.delegate?.bleCommunicator(self, didDisconnectDevice: peripheral, error: error as NSError?)
        
    }
    
    
    /*----------------------------------------------------------------------------------------------------------*/
    // MARK: Central Manager State
    /*----------------------------------------------------------------------------------------------------------*/
    
    func centralManagerDidUpdateState(_ central: CBCentralManager)
    {
        NSLog("centralManagerDidUpdateState")
        if #available(iOS 10.0, *) {
            switch central.state{
                
            case CBManagerState.poweredOn:
                
                if self.scanMode != .active{
                    self.delegate?.bleCommunicator(self, communicatorInitializationSuccessWithBluetoothState:.PoweredOn)
                }
                else{
                    startScan(selectedProfiles)
                }
                break
            case CBManagerState.unauthorized:
                self.delegate?.bleCommunicator(self, communicatorInitializationFailWithBluetoothState:.Unauthorized)
                break
            case CBManagerState.poweredOff:
                self.delegate?.bleCommunicator(self, communicatorInitializationFailWithBluetoothState:.PoweredOff)
                break
            case CBManagerState.unsupported:
                self.delegate?.bleCommunicator(self, communicatorInitializationFailWithBluetoothState:.Unsupported)
                break
            default:
                self.delegate?.bleCommunicator(self, communicatorInitializationFailWithBluetoothState:.Unknown)
                
                break
            }
        } else {
            // Fallback on earlier versions
            switch central.state.rawValue{
                
            case 5:
                if self.scanMode != .active{
                    self.delegate?.bleCommunicator(self, communicatorInitializationSuccessWithBluetoothState:.PoweredOn)
                }
                else{
                    startScan(selectedProfiles)
                }
                break
            case 3:
                self.delegate?.bleCommunicator(self, communicatorInitializationFailWithBluetoothState:.Unauthorized)
                break
            case 4:
                self.delegate?.bleCommunicator(self, communicatorInitializationFailWithBluetoothState:.PoweredOff)
                break
            case 2:
                self.delegate?.bleCommunicator(self, communicatorInitializationFailWithBluetoothState:.Unsupported)
                break
            default:
                self.delegate?.bleCommunicator(self, communicatorInitializationFailWithBluetoothState:.Unknown)
                
                break
            }
            
        }
    }

    /*
     func centralManagerDidUpdateState(_ central: CBCentralManager)
     {
     
     NSLog("centralManagerDidUpdateState")
     switch central.state{
     
     case CBCentralManagerState.poweredOn:
     
     if self.scanMode != .active{
     
     self.delegate?.bleCommunicator(self, communicatorInitializationSuccessWithBluetoothState:.PoweredOn)
     }
     
     else{
     startScan(selectedProfiles)
     }
     break
     case CBCentralManagerState.unauthorized:
     self.delegate?.bleCommunicator(self, communicatorInitializationFailWithBluetoothState:.Unauthorized)
     break
     case CBCentralManagerState.poweredOff:
     self.delegate?.bleCommunicator(self, communicatorInitializationFailWithBluetoothState:.PoweredOff)
     break
     case CBCentralManagerState.unsupported:
     self.delegate?.bleCommunicator(self, communicatorInitializationFailWithBluetoothState:.Unsupported)
     break
     default:
     self.delegate?.bleCommunicator(self, communicatorInitializationFailWithBluetoothState:.Unknown)
     
     break
     }
     }
     */
}















