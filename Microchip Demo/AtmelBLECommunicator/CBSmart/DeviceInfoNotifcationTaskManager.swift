/***************************************************************************
 
 DeviceInfoNotifcationTaskManager.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import Foundation

class DeviceInfoNotificationResponseFrame:OTAResponseFrame{
    
    
    var productID:UInt16!
    var vendorID:UInt16!
    var totalSections:UInt8!

    var firmwareVersion:FirmWareVersion!
    var hardwareVersion:HardWareVersion!

    var hardwareRevision:UInt8!
    var OTAUFirmwareVersion:FirmWareVersion?
    
    override init(withData:Data){
        
        super.init(withData:withData)
        
        
        if command != nil {
            switch(command!){
                
            case OTAUCommand.failure:
                responseType = OTAUResponseType.failure
                
                
                if withData.count >= 4
                //{errorCode = ResponseErrorCode(rawValue:(withData.subdata(in: NSMakeRange(3, 1)) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(with: NSMakeRange(3, 1)).count).pointee)}
                {errorCode = ResponseErrorCode(rawValue:(withData.subdata(in: 3..<4) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(in: 3..<4).count).pointee)}
                
                break
                
            case OTAUCommand.get_DEVICE_INFO_RESP:
                
                responseType = OTAUResponseType.success
                
                if withData.count >= 5
                    //{productID = (withData.subdata(in: NSMakeRange(3, 2)) as NSData).bytes.bindMemory(to: UInt16.self, capacity: withData.subdata(with: NSMakeRange(3, 2)).count).pointee}
                    {productID = (withData.subdata(in: 3..<5) as NSData).bytes.bindMemory(to: UInt16.self, capacity: withData.subdata(in: 3..<5).count).pointee}
                if withData.count >= 7
                    //{vendorID = (withData.subdata(in: NSMakeRange(5, 2)) as NSData).bytes.bindMemory(to: UInt16.self, capacity: withData.subdata(with: NSMakeRange(5, 2)).count).pointee}
                    {vendorID = (withData.subdata(in: 5..<7) as NSData).bytes.bindMemory(to: UInt16.self, capacity: withData.subdata(in: 5..<7).count).pointee}
                if withData.count >= 8
                    //{totalSections = (withData.subdata(in: NSMakeRange(7, 1)) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(with: NSMakeRange(7, 1)).count).pointee}
                    {totalSections = (withData.subdata(in: 7..<8) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(in: 7..<8).count).pointee}
                if withData.count >= 12
                    //{firmwareVersion = FirmWareVersion(withData: withData.subdata(in: NSMakeRange(8, 4))) }
                    {firmwareVersion = FirmWareVersion(withData: withData.subdata(in: 8..<12)) }
                if withData.count >= 14
                    //{hardwareVersion = HardWareVersion(withData: withData.subdata(in: NSMakeRange(12, 2))) }
                    {hardwareVersion = HardWareVersion(withData: withData.subdata(in: 12..<14)) }
                if withData.count >= 15
                    //{hardwareRevision = (withData.subdata(in: NSMakeRange(14, 1)) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(with: NSMakeRange(14, 1)).count).pointee }
                    {hardwareRevision = (withData.subdata(in: 14..<15) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(in: 14..<15).count).pointee }
                if withData.count >= 19
                {
                    var OTAUFirmwareVersionValue : Int64 = 0
                    (withData as NSData).getBytes(&OTAUFirmwareVersionValue, range: NSMakeRange(15, 19))
                    
                    if OTAUFirmwareVersionValue == INVALID_OTAU_FIRMWARE_VERSION
                    { OTAUFirmwareVersion = nil}
                    else
                    //{ OTAUFirmwareVersion = FirmWareVersion(withData: withData.subdata(in: NSMakeRange(15, 4))) }
                    { OTAUFirmwareVersion = FirmWareVersion(withData: withData.subdata(in: 15..<19)) }
                }
                
                break
                
            default:
                responseType = OTAUResponseType.invalid
                break
            }
            
        }
        
    }
    
}

class  DeviceInfoNotifcationRequestFrame:OTARequestFrame {
    
    override init() {
        
        super.init()
        length  = OTAFrameLength.deviceInfoRequest.rawValue
        command = OTAUCommand.get_DEVICE_INFO_REQUEST
        
        
    }
    
    func dataFromFrame()->Data?{
        
        
        //16 bitlength data
        
        var dataLength:UInt16 = length!
        let returnFrameData = NSMutableData(bytes: &dataLength, length: MemoryLayout.size(ofValue: dataLength))
        
        //8 bit command data
        
        var commandData:UInt8 = command!.rawValue
        returnFrameData.append(&commandData, length: MemoryLayout.size(ofValue: commandData))
     
        
        return returnFrameData as Data
    }
    
}

class DeviceInfoNotifcationTaskManager: OTATask {
    
    
    var sucessBlock:((_ withDeviceInfoResponse:DeviceInfoNotificationResponseFrame)->())? = nil
    var failBlock:((_ withError:OTAErrorType,_ errorString:String)->())? = nil
    
    init(interface:AtmelOTAInterface){
        
        super.init()
        otaInterface = interface
        otaInterface.delegate = self
    }
    
    func start(success sucess:@escaping (_ withDeviceInfoResponse:DeviceInfoNotificationResponseFrame)->(),fail:@escaping (_ withError:OTAErrorType,_ errorString:String)->()){
        
        let deviceInfoNotifcationRequestFrame = DeviceInfoNotifcationRequestFrame()
        
        if let requestData = deviceInfoNotifcationRequestFrame.dataFromFrame(){
            
            sucessBlock = sucess
            failBlock   = fail
            otaInterface.sendFrameData(requestData)
        }
    }
    
    
}

extension DeviceInfoNotifcationTaskManager:AtmelOTAInterfaceDelegate{
    
    func receivedResponseFrameData(_ responseData:Data){
      
        if otaShouldAbort()
        {return}
        
        let deviceInfoNotificationResponseFrame = DeviceInfoNotificationResponseFrame(withData: responseData)
        
        if deviceInfoNotificationResponseFrame.responseType == OTAUResponseType.success{
            sucessBlock!(deviceInfoNotificationResponseFrame)
        }
        else if deviceInfoNotificationResponseFrame.responseType == OTAUResponseType.failure{
            failBlock!(OTAErrorType.communicationError,"device information retrieval failed :(\(deviceInfoNotificationResponseFrame.errorCode))")
        }
        
    }
    
}

