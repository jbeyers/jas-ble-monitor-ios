
/* ***************************************************************************

BLEHealthThermometerInterface.swift


Copyright (c) 2015, Atmel Corporation. All rights reserved.
Released under NDA
Licensed under Atmel's Limited License Agreement.


THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Atmel Corporation: http://www.atmel.com

******************************************************************************/


import Foundation
import Foundation
import CoreBluetooth

protocol BLEHealthThermometerInterfaceDelegate:NSObjectProtocol{
    
    func bleHealthThermometerInterfaceDidReadTemperatureMeasurements(_ valueMeasurement:Float,currentUnit unit:TemperatureUnit,TemperatureType type:String?)
    func bleHealthThermometerInterfaceDidReadTemperatureType(_ type:String)
    func bleHealthThermometerInterfaceDidReadDate(_ date:BLETime?)

    
}
enum TemperatureUnit:Int{
    
    case celsius                = 0
    case farenheit              = 1
}
struct BLETime {
    
    var date    :Date?
    var year    :UInt16 = 0
    var month   :UInt8  = 0
    var day     :UInt8  = 0
    var hour    :UInt8  = 0
    var min     :UInt8  = 0
    var sec     :UInt8  = 0
    
    init(data:Data){
        
        var byteArray8 = [UInt8](repeating: 0, count: data.count)
        (data as NSData).getBytes(&byteArray8, length: data.count)
        
        if data.count >= 0+2
        {(data as NSData).getBytes(&year, range: NSMakeRange(0, 2))}
        
        if byteArray8.count >= 2
        {month = byteArray8[2]}
        
        if byteArray8.count >= 3
        {day = byteArray8[3]}
        
        if byteArray8.count >= 4
        {hour = byteArray8[4]}
        
        if byteArray8.count >= 5
        {min = byteArray8[5]}
        
        if byteArray8.count >= 6
        {sec = byteArray8[6]}

        let dateString:NSString = NSString(format:"%d %d %d %d %d %d", year, month, day, hour, min, sec)
        
        let dateFormat:DateFormatter  = DateFormatter()
        dateFormat.dateFormat = "yyyy MM dd HH mm ss"
        self.date = dateFormat.date(from: dateString as String)
        }
}



class BLEHealthThermometerInterface:NSObject{
    
    var delegate                                :BLEHealthThermometerInterfaceDelegate?
    var sourcePeripheral                        :BLESourceDevice?
    var healthThermometerService                :CBService?
    var temperatureMeasurementCharacteristic    :CBCharacteristic?
    var temperatureTypeCharacteristic           :CBCharacteristic?
    var intermediateTemperatureCharacteristic   :CBCharacteristic?
    var measurementIntervalCharacteristic       :CBCharacteristic?
    var timeValue                               :BLETime?

    
    
    init(sourceDevice:BLESourceDevice){
        
        super.init()
        self.sourcePeripheral=sourceDevice
        self.sourcePeripheral!.operationsDelegate = self;
        let serviceArray = sourceDevice.CBSPeripheral.services
        
      


        healthThermometerService=serviceArray?.filter(){$0.uuid.isEqual(CBUUID(bleService: .HealthThermometerService))}.first

        temperatureMeasurementCharacteristic=(healthThermometerService?.characteristics )?.filter(){$0.uuid.isEqual(CBUUID(blecharacteristic:BLECharacteristic.TemperatureMeasurementCharacteristic))}.first
        
        temperatureTypeCharacteristic=(healthThermometerService?.characteristics )?.filter(){$0.uuid.isEqual(CBUUID(blecharacteristic: .TemperatureTypeCharacteristic))}.first
        
        intermediateTemperatureCharacteristic=(healthThermometerService?.characteristics )?.filter(){$0.uuid.isEqual(CBUUID(blecharacteristic: .IntermediateTemperatureCharacteristic))}.first
        measurementIntervalCharacteristic=(healthThermometerService?.characteristics )?.filter(){$0.uuid.isEqual(CBUUID(blecharacteristic: .MeasurementIntervalCharacteristic))}.first
 
    }
    
}


extension BLEHealthThermometerInterface{
    
    func startTemperatureMeasurementNotifications()
        {self.sourcePeripheral?.setNotifyValue(true, forCharacteristic: temperatureMeasurementCharacteristic)}
    
    func stopTemperatureMeasurementNotifications()
        {self.sourcePeripheral?.setNotifyValue(false, forCharacteristic: temperatureMeasurementCharacteristic)}
    
    func readTemperatureType()
        {self.sourcePeripheral?.readValueForCharacteristic(temperatureTypeCharacteristic)}
}


extension BLEHealthThermometerInterface:BLESourceDeviceOperationsDelegate{
    
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice, didUpdateValueForCharacteristic characteristic: CBCharacteristic!, error: NSError!){
        
        if characteristic.isEqual(temperatureMeasurementCharacteristic)
            {calculateTemperatureMeasurmentValueFrom(characteristic.value!)}

        else if characteristic.isEqual( temperatureTypeCharacteristic)
            {calculateTemperatureTypeValueFrom(characteristic.value!)}
    }
    
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice,didWriteValueForCharacteristic characteristic: CBCharacteristic!, error: NSError!){
        
        
    }
   
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice,didUpdateNotificationStateForCharacteristic characteristic: CBCharacteristic!, error: NSError!){
        
        
    }
}

extension BLEHealthThermometerInterface{
    
    
    fileprivate func getTemperatureTypeStringFrom(_ type:UInt8)->String{
        
        switch(type){
            
        case 1:return "Armpit"
        case 2:return "Body"
        case 3:return "Ear"
        case 4:return "Finger"
        case 5:return "Gastro-intestinal Tract"
        case 6:return "Mouth"
        case 7:return "Rectum"
        case 8:return "Toe"
        case 9:return "Tympanum"
        default: return "Tympanum(ear drum)"
        //default: return "Unknown"//Tympanum(ear drum) as per client requirment the unknown value changed to Tympanum(ear drum)
        }
    }
    
    fileprivate func calculateTemperatureMeasurmentValueFrom(_ data:Data){
        
        var byteArray8 = [UInt8](repeating: 0, count: data.count)

        (data as NSData).getBytes(&byteArray8, length: data.count)
        
        if byteArray8.count<1
            {return}
        //Check flag
        let flagTemp:UInt8 = byteArray8[0]
    
        let isTimeParameter:Bool = (flagTemp & 0x02).toBool()
        let isTempType:Bool =  (flagTemp & 0x04).toBool()
//        println("temp type==\(flagTemp & 0x04)")
        let currentUnit:TemperatureUnit = TemperatureUnit(rawValue:Int(flagTemp & 0x01))!
       

        
        var tempValue:Int64 = 0
        
        if data.count >= (1+4)
        {(data as NSData).getBytes(&tempValue, range: NSMakeRange(1, 4))}
        
        tempValue = Int64(CFSwapInt32LittleToHost(UInt32(tempValue)))
//        println("\(tempValue)")
        
        var exponent :Int64 = (tempValue & 0xff000000) >> 24
        var mantissa :Int64 = tempValue & 0x00ffffff
        
        if mantissa >= 0x800000
        {
            mantissa = -(0x01000000 - mantissa)
        }
        
        if (exponent >= 0x80) {
            exponent = -(0x0100 - exponent);
        }
        
        let temperatureValue:Float = Float(mantissa) * pow(Float(10),Float(exponent))
        

//        println(byteArray8)

        //56 bit for time
        
        if isTempType{
            
            if isTimeParameter{
                
                //timeValue = BLETime(data:data.subdata(in: NSMakeRange(5,7)))
                timeValue = BLETime(data:data.subdata(in: 5..<12))
                self.delegate?.bleHealthThermometerInterfaceDidReadDate(timeValue)

                self.delegate?.bleHealthThermometerInterfaceDidReadTemperatureMeasurements(temperatureValue, currentUnit: currentUnit, TemperatureType: getTemperatureTypeStringFrom(byteArray8[12]))
            }
            else{

                self.delegate?.bleHealthThermometerInterfaceDidReadTemperatureMeasurements(temperatureValue, currentUnit: currentUnit, TemperatureType: getTemperatureTypeStringFrom(byteArray8[5]))

            }
        }
        
        else{
            self.delegate?.bleHealthThermometerInterfaceDidReadTemperatureMeasurements(temperatureValue, currentUnit: currentUnit, TemperatureType: nil)
        }
        
        
        
    }
    fileprivate func calculateTemperatureTypeValueFrom(_ data:Data){
        
        var tempValue:UInt8 = 0
        (data as NSData).getBytes(&tempValue, length: MemoryLayout.size(ofValue: tempValue))
        self.delegate?.bleHealthThermometerInterfaceDidReadTemperatureType(getTemperatureTypeStringFrom(tempValue))
        
        
        
    }
}
