/* ***************************************************************************

BLEenum.swift


Copyright (c) 2015, Atmel Corporation. All rights reserved.
Released under NDA
Licensed under Atmel's Limited License Agreement.


THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Atmel Corporation: http://www.atmel.com

******************************************************************************/


import Foundation
import CoreBluetooth

enum BeaconType{
    case eddystone
    case altBeacon
    case iBeacon
}
enum AlertLevel:UInt8{
    
    case noAlert    = 0
    case mildAlert  = 1
    case highAlert  = 2
}

enum BLECharacteristic:String{
    
    case AlertLevelCharacteristic                    =   "0x2A06"
    case TransmissionPowerLevelCharacteristic        =   "0x2A07"
    
    static let LinkLossService          =   [AlertLevelCharacteristic]
    static let ImmediateAlertService    =   [AlertLevelCharacteristic]
    static let TransmissionPowerService =   [TransmissionPowerLevelCharacteristic]
    
    case TemperatureMeasurementCharacteristic       =   "0x2A1C"
    case TemperatureTypeCharacteristic              =   "0x2A1D"
    case IntermediateTemperatureCharacteristic      =   "0x2A1E"
    case MeasurementIntervalCharacteristic          =   "0x2A21"
    
    
    case ManufacturerNameStringCharacteristics      =   "0x2A29"
    case ModelNumberStringCharacteristics           =   "0x2A24"
    case SerialNumberStringCharacteristics          =   "0x2A25"
    case HardwareRevisionStringCharacteristics      =   "0x2A27"
    case FirmwareRevisionStringCharacteristics      =   "0x2A26"
    case SoftwareRevisionStringCharacteristics      =   "0x2A28"
    case SystemIDCharacteristics                    =   "0x2A23"
    case IEEERegulatoryCertificationCharacteristics =   "0x2A2A"
    case PnPIDCharacteristics                       =   "0x2A50"

    case BatteryLevelCharacteristic                 =   "0x2A19"
    
    
    case HeartRateMeasurementCharacteristics        =   "0x2A37"
    case BodySensorLocationCharacteristics          =   "0x2A38"
    case HeartRateControlPointCharacteristics       =   "0x2A39"
    
    case BloodPressureMeasurementCharacteristics    =   "0x2A35"
    case IntermediateCuffPressureCharacteristics    =   "0x2A36"
    case BloodPressureFeatureCharacteristics        =   "0x2A49"
    
    
    case ScanRefreshCharacteristics                 =   "0x2A31"
    case ScanIntervalWindowCharacteristics          =   "0x2A4F"
    
    case EndPointCharacteristics                    =   "0xfd5abba1-3935-11e5-85a6-0002a5d5c51b"
    
    
    case AtmelOTANotificationCharacteristics        =   "8b698d5b-04e1-48b1-9617-ac80aa64e0e0"
    case AtmelOTAReadCharacteristics                =   "8b698d5b-04e1-48b1-9617-ac80aa64e0ea"
    case AtmelOTAWriteCharacteristics               =   "8b698d5b-04e1-48b1-9617-ac80aa64e0e5"

    
    case EddyStoneLockStateCharacteristics          =   "ee0c2081-8786-40ba-ab96-99b91ac981d8"
    case EddyStoneLockCharacteristics               =   "ee0c2082-8786-40ba-ab96-99b91ac981d8"
    case EddyStoneUnLockCharacteristics             =   "ee0c2083-8786-40ba-ab96-99b91ac981d8"
    case EddyStoneURICharacteristics                =   "ee0c2084-8786-40ba-ab96-99b91ac981d8"
    case EddyStoneFlagsCharacteristics              =   "ee0c2085-8786-40ba-ab96-99b91ac981d8"
    case EddyStoneTxPowerLevelCharacteristics       =   "ee0c2086-8786-40ba-ab96-99b91ac981d8"
    case EddyStoneTxModeCharacteristics             =   "ee0c2087-8786-40ba-ab96-99b91ac981d8"
    case EddyStoneBeaconPeriodCharacteristics       =   "ee0c2088-8786-40ba-ab96-99b91ac981d8"
    case EddyStoneResetCharacteristics              =   "ee0c2089-8786-40ba-ab96-99b91ac981d8"
    case EddyStoneReservedCharacteristcs            =   "ee0c2090-8786-40ba-ab96-99b91ac981d8"

    
    
    
//    case EddyStoneLockStateCharacteristics          =   "00000081-0000-0000-0000-0000000081D8"
//    case EddyStoneURICharacteristics                =   "00000084-0000-0000-0000-0000000081D8"
//    case EddyStoneFlagsCharacteristics              =   "00000085-0000-0000-0000-0000000081D8"
//    case EddyStoneTxPowerLevelCharacteristics       =   "00000086-0000-0000-0000-0000000081D8"
//    case EddyStoneTxModeCharacteristics             =   "00000087-0000-0000-0000-0000000081D8"
//    case EddyStoneBeaconPeriodCharacteristics       =   "00000088-0000-0000-0000-0000000081D8"
//    case EddyStoneResetCharacteristics              =   "00000089-0000-0000-0000-0000000081D8"
    
    
    
    static let HealthThermometerService             =
    [TemperatureMeasurementCharacteristic,TemperatureTypeCharacteristic,IntermediateTemperatureCharacteristic,MeasurementIntervalCharacteristic]
    
    static let BatteryService                       =
    [BatteryLevelCharacteristic]
    
    static let DeviceInformationService             =
    [ManufacturerNameStringCharacteristics,ModelNumberStringCharacteristics,SerialNumberStringCharacteristics,HardwareRevisionStringCharacteristics,FirmwareRevisionStringCharacteristics,SoftwareRevisionStringCharacteristics,SystemIDCharacteristics,IEEERegulatoryCertificationCharacteristics,PnPIDCharacteristics]
    
    static let HeartRateService                     =
        [HeartRateMeasurementCharacteristics,BodySensorLocationCharacteristics,HeartRateControlPointCharacteristics]
    
    static let BloodPressureService                 =
    [BloodPressureMeasurementCharacteristics,IntermediateCuffPressureCharacteristics,BloodPressureFeatureCharacteristics]
    
    static let ScanParameterService                 =
    [ScanRefreshCharacteristics,ScanIntervalWindowCharacteristics]
    
    static let SerialPortService                    =
    [EndPointCharacteristics]
    
    static let AtmelOTAService                      =
    [AtmelOTANotificationCharacteristics,AtmelOTAReadCharacteristics,AtmelOTAWriteCharacteristics]
    
    static let AtmelEddyStoneService                =
    [EddyStoneLockStateCharacteristics, EddyStoneLockCharacteristics, EddyStoneUnLockCharacteristics, EddyStoneURICharacteristics, EddyStoneFlagsCharacteristics, EddyStoneTxPowerLevelCharacteristics, EddyStoneTxModeCharacteristics, EddyStoneBeaconPeriodCharacteristics, EddyStoneResetCharacteristics, EddyStoneReservedCharacteristcs]
    
    var description: String {
        return self.rawValue
    }
}

enum BLEService:String{
    
    case  LinkLossService                           =   "0x1803"
    case  ImmediateAlertService                     =   "0x1802"
    case  TransmissionPowerService                  =   "0x1804"
    
    static let ProximityMandatory                   =
        [LinkLossService]
    static let ProximityAll                         =
        [LinkLossService,ImmediateAlertService,TransmissionPowerService]
    
    case HealthThermometerService                   =   "0x1809"
    case DeviceInformationService                   =   "0x180A"
    
    case BatteryService                             =   "0x180F"
    
    case HeartRateService                           =   "0x180D"
    
    case BloodPressureService                       =   "0x1810"
    
    case ScanParameterService                       =   "0x1813"
    
    case SerialPortService                          =   "0xfd5abba0-3935-11e5-85a6-0002a5d5c51b"
    
    case GenericAccessService                       =   "0x1800"
    
    case GenericAttributeService                    =   "0x1801"
    
    case AtmelOTAService                            =   "8b698d5b-04e1-48b1-9617-ac80aa64e0d0"
    
    case AtmelEddyStoneService                      =   "ee0c2080-8786-40ba-ab96-99b91ac981d8"
    
    static let HealthThermometerMandatory           =
        [HealthThermometerService]
    static let HealthThermometerAll                 =
        [HealthThermometerService,DeviceInformationService]
    
    static let FindMeAll                            =
        [ImmediateAlertService]
    static let ScanParameterAll                     =
    [ScanParameterService]

    
    static let HeartRateMandatory                   =
    [HeartRateService]
    static let HeartRateAll                         =
    [HeartRateService,DeviceInformationService]
    
    static let BloodPressureMandatory               =
    [BloodPressureService]
    static let BloodPressureAll                     =
    [BloodPressureService,DeviceInformationService]
    
    static let SerialPortAll                        =
    [SerialPortService]
    
    static let BatteryServiceMandatory              =
    [BatteryService]
    
    static let BatteryServiceAll                    =
    [BatteryService,AtmelOTAService]

    static let AtmelOTAAll                          =
    [AtmelOTAService]
    
    static let EddyStoneAll                         =
    [AtmelEddyStoneService]
    
    var description: String {
        return self.rawValue
    }
}


enum BLEDescriptor : String{
    
    case CharacteristicExtendedProperties       = "0x2900"
    case CharacteristicUserDescription          = "0x2901"
    case ClientCharacteristicConfiguration      = "0x2902"
    case ServerCharacteristicConfiguration      = "0x2903"
    case CharacteristicPresentationFormat       = "0x2904"
    case CharacteristicAggregateFormat          = "0x2905"
    case ValidRange                             = "0x2906"
    case ExternalReportReference                = "0x2907"
    case ReportReference                        = "0x2908"
    case NumberOfDigitals                       = "0x2909"
    case ValueTriggerSetting                    = "0x290A"
    case EnvironmentalSensingConfiguration      = "0x290B"
    case EnvironmentalSensingMeasurement        = "0x290C"
    case EnvironmentalSensingTriggerSetting     = "0x290D"
    case TimeTriggerSetting                     = "0x290E"
    
    var description : String{
        return self.rawValue
    }

}



enum BLEProfile : String , Equatable{
    
    case AlertNotification                          =   "Alert Notification"
    case BloodPressure                              =   "Blood Pressure"
    case CyclingPower                               =   "Cycling Power"
    case CyclingSpeedandCadence                     =   "Cycling Speed and Cadence"
    case FindMe                                     =   "Find Me"
    case Glucose                                    =   "Glucose"
    case Thermometer                                =   "Health Thermometer"
    case BatteryService                             =   "BatteryService"
    case HeartRate                                  =   "Heart Rate"
    case HIDOVERGATT                                =   "HID OVER GATT"
    case LocationandNavigation                      =   "Location and Navigation"
    case PhoneAlertStatus                           =   "Phone Alert Status"
    case Proximity                                  =   "Proximity"
    case RunningSpeedandCadence                     =   "Running Speed and Cadence"
    case ScanParameters                             =   "Scan Parameters"
    case SerialPort                                 =   "CustomSerialChat"
    case Time                                       =   "Time"
    case NoFilter                                   =   "No Filter"
    case OTA                                        =   "OTA"
    case EddyStone                                  =   "EddyStone"
    
    static let allValues = [BloodPressure,HeartRate,BatteryService,FindMe,Thermometer,Proximity,ScanParameters,SerialPort,OTA]
}



struct ProfileDetail {
 
    var profileName                 :BLEProfile
    var profileMandatoryServices    :Array<BLEService>
    var profileServices             :Array<BLEService>
}

 let ProfileServiceDetails                          =
    [ProfileDetail(profileName: BLEProfile.Proximity, profileMandatoryServices: BLEService.ProximityMandatory, profileServices:BLEService.ProximityAll),
    ProfileDetail(profileName: BLEProfile.Thermometer, profileMandatoryServices: BLEService.HealthThermometerMandatory, profileServices:BLEService.HealthThermometerAll),
    ProfileDetail(profileName: BLEProfile.FindMe, profileMandatoryServices: BLEService.FindMeAll, profileServices:BLEService.FindMeAll),
    ProfileDetail(profileName: BLEProfile.HeartRate, profileMandatoryServices: BLEService.HeartRateMandatory, profileServices:BLEService.HeartRateAll),
    ProfileDetail(profileName: BLEProfile.BatteryService, profileMandatoryServices: BLEService.BatteryServiceMandatory, profileServices:BLEService.BatteryServiceAll),
    ProfileDetail(profileName: BLEProfile.SerialPort, profileMandatoryServices: BLEService.SerialPortAll, profileServices:BLEService.SerialPortAll),
    ProfileDetail(profileName: BLEProfile.BloodPressure, profileMandatoryServices: BLEService.BloodPressureMandatory, profileServices:BLEService.BloodPressureAll),
    ProfileDetail(profileName: BLEProfile.ScanParameters, profileMandatoryServices: BLEService.ScanParameterAll, profileServices:BLEService.ScanParameterAll),
    ProfileDetail(profileName: BLEProfile.SerialPort, profileMandatoryServices: BLEService.SerialPortAll, profileServices:BLEService.SerialPortAll),
    ProfileDetail(profileName: BLEProfile.OTA, profileMandatoryServices: BLEService.AtmelOTAAll, profileServices:BLEService.AtmelOTAAll),
    ProfileDetail(profileName: BLEProfile.EddyStone, profileMandatoryServices: BLEService.EddyStoneAll, profileServices: BLEService.EddyStoneAll)]



struct BLEConstants
{
    
    struct BLENotificationNames {
        
        static let  BLE_PERIPHERAL_DISCONNECTION = "DeviceDisconnection"
        
    }
}
extension Array{
    
    mutating func removeDependentProfiles(){
        
        if self.contains(BLEProfile.Proximity)
            {self = self.filter(){($0 as? BLEProfile) != BLEProfile.FindMe}}
        if self.contains(BLEProfile.BatteryService)
        {self = self.filter(){($0 as? BLEProfile) != BLEProfile.OTA}}
        
    }
    
    func bleServicetoCBUUIDArray()->Array<CBUUID> {
        
        var array:Array<CBUUID> = [CBUUID]()
        
        for obj1 in self {
            if let service:BLEService? = obj1 as? BLEService{
                
                if(service != nil)
                    {array.append(CBUUID(bleService: service!))}
                }
            }
        return array
    }
    
    func findAssociatedBLEProfiles() -> [BLEProfile]{
        
        var returnArray:[BLEProfile] = [BLEProfile]()
        
        for profileDetail:ProfileDetail in ProfileServiceDetails{
        
            if self.selectProfiles(profileDetail)
                {returnArray.append(profileDetail.profileName)}
        }
        return returnArray
    }
    
    fileprivate func selectProfiles(_ profileDetail:ProfileDetail) -> Bool {
        
        let cbuuidArray     :[CBUUID]  = profileDetail.profileMandatoryServices.bleServicetoCBUUIDArray()
        let sercbuuidArray  :[CBUUID]  = self.map({($0 as? CBService)!.uuid})
        
        for obj in cbuuidArray{
            if !sercbuuidArray.contains(obj)
                {return false}
        }
       
        return true
    }
    
    func contains<T>(_ obj: T) -> Bool where T : Equatable
        {return self.filter({$0 as? T == obj}).count > 0}
    
}

