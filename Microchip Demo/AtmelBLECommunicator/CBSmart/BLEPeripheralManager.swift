/***************************************************************************

BLEPeripheralManager.swift
//
Interface class for implementing actions and handling responses of Bluetooth Central
//

Copyright (c) 2015, Atmel Corporation. All rights reserved.
Released under NDA
Licensed under Atmel's Limited License Agreement.


THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Atmel Corporation: http://www.atmel.com

******************************************************************************/

import Foundation
import CoreBluetooth



protocol BLEPeripheralManagerDelegate:NSObjectProtocol{
    
    func blePeripheralManager(_ blePeripheralManager:BLEPeripheralManager ,communicatorInitializationSuccessWithBluetoothState state:BluetoothState)
    func blePeripheralManager(_ blePeripheralManager:BLEPeripheralManager ,communicatorInitializationFailWithBluetoothState state:BluetoothState)
    func blePeripheralManager(_ blePeripheralManager:BLEPeripheralManager , didReceiveReadRequest request: CBATTRequest!)
    func blePeripheralManager(_ blePeripheralManager:BLEPeripheralManager ,didReceiveWriteRequests requests: [AnyObject]!)
    func blePeripheralManager(_ blePeripheralManager:BLEPeripheralManager  ,central: CBCentral!, didSubscribeToCharacteristic characteristic: CBCharacteristic!)
     func blePeripheralManager(_ blePeripheralManager:BLEPeripheralManager ,central: CBCentral!, didUnsubscribeFromCharacteristic characteristic: CBCharacteristic!)
    func blePeripheralManagerIsReadyToUpdateSubscribers(_ blePeripheralManager:BLEPeripheralManager)
    func blePeripheralManager(_ blePeripheralManager:BLEPeripheralManager ,didAddService service: CBService!, error: NSError?)
 }


class BLEPeripheralManager: NSObject {
    
    var CBPManager: CBPeripheralManager!
    var delegate:BLEPeripheralManagerDelegate?
 
    override init()
    {
        super.init()
        CBPManager = CBPeripheralManager(delegate: self, queue: nil)
        print(" CBPeripheralManager is initialized")
        
    }
    
    /*var CBPManager: CBCentralManager!
    var delegate:BLEPeripheralManagerDelegate?
    
    override init()
    {
        super.init()
        CBPManager = CBCentralManager(delegate: self, queue: nil)
        print(" CBPeripheralManager is initialized")
        
    }*/
    
}

extension BLEPeripheralManager:CBPeripheralManagerDelegate{
    
    func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager) {
        
        print("state updated\(peripheral.state)")
        
        if #available(iOS 10.0, *) {
            switch(peripheral.state){
                
            case CBManagerState.poweredOn:
                self.delegate?.blePeripheralManager(self, communicatorInitializationSuccessWithBluetoothState:BluetoothState.PoweredOn)
                break
                
            case CBManagerState.poweredOff:
                self.delegate?.blePeripheralManager(self, communicatorInitializationFailWithBluetoothState: BluetoothState.PoweredOff)
                break
                
            case CBManagerState.resetting:
                self.delegate?.blePeripheralManager(self, communicatorInitializationFailWithBluetoothState: BluetoothState.Resetting)
                break
                
            case CBManagerState.unauthorized:
                self.delegate?.blePeripheralManager(self, communicatorInitializationFailWithBluetoothState: BluetoothState.Unauthorized)
                break
                
            case CBManagerState.unknown:
                self.delegate?.blePeripheralManager(self, communicatorInitializationFailWithBluetoothState: BluetoothState.Unknown)
                break
                
            case CBManagerState.unsupported:
                self.delegate?.blePeripheralManager(self, communicatorInitializationFailWithBluetoothState: BluetoothState.Unsupported)
                break
                
            }
        } else {
            // Fallback on earlier versions
            switch(peripheral.state.rawValue){
                
            case 5:
                self.delegate?.blePeripheralManager(self, communicatorInitializationSuccessWithBluetoothState:BluetoothState.PoweredOn)
                break
                
            case 4:
                self.delegate?.blePeripheralManager(self, communicatorInitializationFailWithBluetoothState: BluetoothState.PoweredOff)
                break
                
            case 1:
                self.delegate?.blePeripheralManager(self, communicatorInitializationFailWithBluetoothState: BluetoothState.Resetting)
                break
                
            case 3:
                self.delegate?.blePeripheralManager(self, communicatorInitializationFailWithBluetoothState: BluetoothState.Unauthorized)
                break
                
            case 0:
                self.delegate?.blePeripheralManager(self, communicatorInitializationFailWithBluetoothState: BluetoothState.Unknown)
                break
                
            case 2:
                self.delegate?.blePeripheralManager(self, communicatorInitializationFailWithBluetoothState: BluetoothState.Unsupported)
                break
                
            default: break
                
            }
        }
    }
    func peripheralManager(_ peripheral: CBPeripheralManager, didAdd service: CBService, error: Error?){
        
        print(" successfully added service with UUID =\(service.uuid.uuidString)")
        self.delegate?.blePeripheralManager(self, didAddService: service, error: error as? NSError)
    }
    
    func peripheralManager(_ peripheral: CBPeripheralManager, didReceiveRead request: CBATTRequest){

        self.delegate?.blePeripheralManager(self, didReceiveReadRequest: request)
    }
    
    func peripheralManager(_ peripheral: CBPeripheralManager, didReceiveWrite requests: [CBATTRequest]){

        self.delegate?.blePeripheralManager(self, didReceiveWriteRequests: requests)
    }
    
    func peripheralManager(_ peripheral: CBPeripheralManager, central: CBCentral, didSubscribeTo characteristic: CBCharacteristic){

        print(" subscribed to characteristics by central =\(central.identifier)")

        self.delegate?.blePeripheralManager(self, central: central, didSubscribeToCharacteristic: characteristic)

        
    }
    func peripheralManager(_ peripheral: CBPeripheralManager, central: CBCentral, didUnsubscribeFrom characteristic: CBCharacteristic){
        
        print(" UNsubscribed to characteristics by central =\(central.identifier)")

        self.delegate?.blePeripheralManager(self, central: central, didUnsubscribeFromCharacteristic: characteristic)
        
    }
    func peripheralManagerIsReady(toUpdateSubscribers peripheral: CBCentralManager){
        self.delegate?.blePeripheralManagerIsReadyToUpdateSubscribers(self)
    }

}

//extension BLEPeripheralManager:CBPeripheralManagerDelegate{

/*func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager){
 
 print("state updated\(peripheral.state)")
 
 switch(peripheral.state){
 
 case CBPeripheralManagerState.poweredOn:
 self.delegate?.blePeripheralManager(self, communicatorInitializationSuccessWithBluetoothState:BluetoothState.PoweredOn)
 break
 
 case CBPeripheralManagerState.poweredOff:
 self.delegate?.blePeripheralManager(self, communicatorInitializationFailWithBluetoothState: BluetoothState.PoweredOff)
 break
 
 case CBPeripheralManagerState.resetting:
 self.delegate?.blePeripheralManager(self, communicatorInitializationFailWithBluetoothState: BluetoothState.Resetting)
 break
 
 case CBPeripheralManagerState.unauthorized:
 self.delegate?.blePeripheralManager(self, communicatorInitializationFailWithBluetoothState: BluetoothState.Unauthorized)
 break
 
 case CBPeripheralManagerState.unknown:
 self.delegate?.blePeripheralManager(self, communicatorInitializationFailWithBluetoothState: BluetoothState.Unknown)
 break
 
 case CBPeripheralManagerState.unsupported:
 self.delegate?.blePeripheralManager(self, communicatorInitializationFailWithBluetoothState: BluetoothState.Unsupported)
 break
 
 }
 }
 
 func peripheralManager(_ peripheral: CBPeripheralManager, didAdd service: CBService, error: Error?){
 
 print(" successfully added service with UUID =\(service.uuid.uuidString)")
 self.delegate?.blePeripheralManager(self, didAddService: service, error: error as! NSError)
 }
 
 func peripheralManager(_ peripheral: CBPeripheralManager, didReceiveRead request: CBATTRequest){
 
 self.delegate?.blePeripheralManager(self, didReceiveReadRequest: request)
 }
 
 func peripheralManager(_ peripheral: CBPeripheralManager, didReceiveWrite requests: [CBATTRequest]){
 
 self.delegate?.blePeripheralManager(self, didReceiveWriteRequests: requests)
 }
 
 func peripheralManager(_ peripheral: CBPeripheralManager, central: CBCentral, didSubscribeTo characteristic: CBCharacteristic){
 
 print(" subscribed to characteristics by central =\(central.identifier)")
 
 self.delegate?.blePeripheralManager(self, central: central, didSubscribeToCharacteristic: characteristic)
 
 
 }
 func peripheralManager(_ peripheral: CBPeripheralManager, central: CBCentral, didUnsubscribeFrom characteristic: CBCharacteristic){
 
 print(" UNsubscribed to characteristics by central =\(central.identifier)")
 
 self.delegate?.blePeripheralManager(self, central: central, didUnsubscribeFromCharacteristic: characteristic)
 
 }
 func peripheralManagerIsReady(toUpdateSubscribers peripheral: CBPeripheralManager){
 self.delegate?.blePeripheralManagerIsReadyToUpdateSubscribers(self)
 }
 
 }
 */



















