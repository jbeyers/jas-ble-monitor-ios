/***************************************************************************
 
 ImageSwitchTaskManager.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import Foundation

class ImageSwitchResponseFrame:OTAResponseFrame{
    
    
    var totalSections:UInt8!
    
    
    override init(withData:Data){
        
        super.init(withData:withData)
       
        
        if command != nil {
            switch(command!){
                
            case OTAUCommand.failure:
                responseType = OTAUResponseType.failure
                
                
                if withData.count >= 4
                //{errorCode = ResponseErrorCode(rawValue:(withData.subdata(in: NSMakeRange(3, 1)) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(with: NSMakeRange(3, 1)).count).pointee)}
                {errorCode = ResponseErrorCode(rawValue:(withData.subdata(in: 3..<4) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(in: 3..<4).count).pointee)}
                
                break
                
            case OTAUCommand.image_SWITCH_RESP:
                
                responseType = OTAUResponseType.success
                
                if withData.count >= 4
                //{totalSections = (withData.subdata(in: NSMakeRange(3, 1)) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(with: NSMakeRange(3, 1)).count).pointee}
                {totalSections = (withData.subdata(in: 3..<4) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(in: 3..<4).count).pointee}
                
                
                break
                
            default:
                responseType = OTAUResponseType.invalid
                break
            }
            
        }
        
    }
    
}
class ImageSwitchRequestFrame:OTARequestFrame {
    
    var totalSections:UInt8!
    var firmwareVersion:FirmWareVersion!
    
    
    init(image:Image) {
        
        super.init()
        
        
        length  = OTAFrameLength.ImageSwitchRequest.rawValue
        command = OTAUCommand.image_SWITCH_REQUEST
        
        //Frame init
        
        totalSections = image.totalSections
        firmwareVersion = image.firmwareVersion

    }
    
    func dataFromFrame()->Data?{
        
        if  totalSections == nil || firmwareVersion == nil
        {return nil}
        
        
        //16 bitlength data
        
        var dataLength:UInt16 = length!
        let returnFrameData = NSMutableData(bytes: &dataLength, length: MemoryLayout.size(ofValue: dataLength))
        
        //8 bit command data
        
        var commandData:UInt8 = command!.rawValue
        returnFrameData.append(&commandData, length: MemoryLayout.size(ofValue: commandData))
        
        
        //8 bit total number of sections  data
        
        var sectID:UInt8 = totalSections
        returnFrameData.append(&sectID, length: MemoryLayout.size(ofValue: sectID))
        
        //32 bit total firmwareVersion
        
        returnFrameData.append(firmwareVersion.getData() as Data)
        
        return returnFrameData as Data
    }
    
}

class ImageSwitchTaskManager: OTATask {
    
    
    var sucessBlock:(()->())? = nil
    var failBlock:((_ withError:OTAErrorType,_ errorString:String)->())? = nil
    
    
    var offSet:Int = 0
    
    init(interface:AtmelOTAInterface){

        super.init()
        otaInterface = interface
        otaInterface.delegate = self
        
    }
    
    func start(success sucess:@escaping ()->(),fail:@escaping (_ withError:OTAErrorType,_ errorString:String)->()){
        
        let imageSwitchRequestFrame = ImageSwitchRequestFrame(image: otaInterface.image!)
        
        if let requestData = imageSwitchRequestFrame.dataFromFrame(){
            otaInterface.updatedProgressInfo("Switching Image", progress:-2)

            otaInterface.sendFrameData(requestData)
            
            sucessBlock = sucess
            failBlock = fail
        }
            
        else{
            fail(OTAErrorType.localError, "Image switch data  from image file Could not be obtained")
        }
    }
    

   

    
    
    
    
}
extension ImageSwitchTaskManager:AtmelOTAInterfaceDelegate{
    
    func receivedResponseFrameData(_ responseData:Data){
        
        if otaShouldAbort()
        {return}
        
        let imageSwitchResponseFrame = ImageSwitchResponseFrame(withData: responseData)
        
        if imageSwitchResponseFrame.responseType == OTAUResponseType.success{
            sucessBlock!()
        }
        else if imageSwitchResponseFrame.responseType == OTAUResponseType.failure{
            failBlock!(OTAErrorType.communicationError,"\(imageSwitchResponseFrame.errorCode)")
        }
    }

  
}
