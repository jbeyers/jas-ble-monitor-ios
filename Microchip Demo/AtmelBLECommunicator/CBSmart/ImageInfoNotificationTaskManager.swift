/***************************************************************************
 
 ImageInfoNotificationTaskManager.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import UIKit

enum OTAUImageOption:UInt8{
    
    case start     =  0x05
    case resume    =  0x0D
    
}

enum OTAUImageSectionID:UInt8{
    
    case patch          = 0x01
    case appHeaderPatch = 0x02
    case application    = 0x03
    
    
    var description:String{
        switch(self){
        case .patch :
            return "Patch"
        case .appHeaderPatch :
            return "AppHeaderPatch"
        case .application   :
            return "Application"
            
        }
    }

    static let allValues = [patch,appHeaderPatch,application]
    
}


class ImageInfoNotificationResponseFrame:OTAResponseFrame{
    
    var imageOption:OTAUImageOption?
    var currentOTAMode:OTAMode?

    
    var offsetBlockNo:UInt8?
    var offsetPageNo:UInt16?
    var offsetSectionID:OTAUImageSectionID?
    
    var pageSize:UInt16?
    var blockSize:UInt16?
    
    override init(withData:Data){
        
        super.init(withData: withData)
        
            if command != nil {
            
            switch(command!){
                
            case OTAUCommand.failure:
                responseType = OTAUResponseType.failure
                
                if withData.count >= 4
                //{errorCode = ResponseErrorCode(rawValue:(withData.subdata(in: NSMakeRange(3, 1)) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(with: NSMakeRange(3, 1)).count).pointee)}
                {errorCode = ResponseErrorCode(rawValue:(withData.subdata(in: 3..<4) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(in: 3..<4).count).pointee)}
                break
                
            case OTAUCommand.image_INFO_NOTIFY_RESP:
                
                responseType = OTAUResponseType.success
                
                if withData.count >= 4
                //{imageOption = OTAUImageOption(rawValue: (withData.subdata(in: NSMakeRange(3, 1)) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(with: NSMakeRange(3, 1)).count).pointee)}
                {imageOption = OTAUImageOption(rawValue: (withData.subdata(in: 3..<4) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(in: 3..<4).count).pointee)}
                
                if withData.count >= 5
                //{offsetSectionID = OTAUImageSectionID(rawValue:(withData.subdata(in: NSMakeRange(4, 1)) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(with: NSMakeRange(4, 1)).count).pointee)}
                {offsetSectionID = OTAUImageSectionID(rawValue:(withData.subdata(in: 4..<5) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(in: 4..<5).count).pointee)}
                
                if withData.count >= 6
                //{offsetPageNo =  (withData.subdata(in: NSMakeRange(5, 2)) as NSData).bytes.bindMemory(to: UInt16.self, capacity: withData.subdata(with: NSMakeRange(5, 2)).count).pointee}
                {offsetPageNo =  (withData.subdata(in: 5..<7) as NSData).bytes.bindMemory(to: UInt16.self, capacity: withData.subdata(in: 5..<7).count).pointee}
                
                if withData.count >= 8
                //{offsetBlockNo =  (withData.subdata(in: NSMakeRange(7, 1)) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(with: NSMakeRange(7, 1)).count).pointee}
                {offsetBlockNo =  (withData.subdata(in: 7..<8) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(in: 7..<8).count).pointee}
                
                if withData.count >= 10
                //{pageSize =  (withData.subdata(in: NSMakeRange(8, 2)) as NSData).bytes.bindMemory(to: UInt16.self, capacity: withData.subdata(with: NSMakeRange(8, 2)).count).pointee}
                {pageSize =  (withData.subdata(in: 8..<10) as NSData).bytes.bindMemory(to: UInt16.self, capacity: withData.subdata(in: 8..<10).count).pointee}
                
                if withData.count >= 12
                //{blockSize =  (withData.subdata(in: NSMakeRange(10, 2)) as NSData).bytes.bindMemory(to: UInt16.self, capacity: withData.subdata(with: NSMakeRange(10, 2)).count).pointee}
                {blockSize =  (withData.subdata(in: 10..<12) as NSData).bytes.bindMemory(to: UInt16.self, capacity: withData.subdata(in: 10..<12).count).pointee}
                
                
                if blockSize == pageSize
                    {currentOTAMode = OTAMode.pageMode}
                else
                    {currentOTAMode = OTAMode.blockMode}
                break
                
                
                
            default:
                responseType = OTAUResponseType.invalid
                break
            }
            
        }
        
    }
    
}
class ImageInfoNotificationRequestFrame:OTARequestFrame {
    
    var totalSections:UInt8!
    var totalImageSize:UInt32!
    
    var patchSectionID:UInt8!
    var patchSectionSize:Data!
    var patchStartAddress:Data!
    
    var appHeaderPatchSectionID:UInt8!
    var appHeaderPatchSectionSize:Data!
    var appHeaderPatchStartAddress:Data!
    
    var appSectionID:UInt8!
    var appSectionSize:Data!
    var appStartAddress:Data!
    
    var appCRC:Data!
    var patchCRC:Data!
    var appHeaderPatchCRC:Data!
    
    var totalImageCRC:Data!
    
    
    
    
    
    init(image:Image) {
        
        super.init()
        
        length  = OTAFrameLength.imageInfoRequest.rawValue
        command = OTAUCommand.image_INFO_NOTIFY_REQUEST
        
        
        //Frame Values
        
        totalSections = image.totalSections
        totalImageSize = image.totalImageSize
        
        patchSectionID  = image.patchSectionID
        patchSectionSize  = image.patchSectionSize as Data!
        patchStartAddress  = image.patchStartAddress as Data!
        
        appHeaderPatchSectionID  = image.appHeaderPatchSectionID
        appHeaderPatchSectionSize  = image.appHeaderPatchSectionSize as Data!
        appHeaderPatchStartAddress  = image.appHeaderPatchStartAddress as Data!
        
        appSectionID  = image.appSectionID
        appSectionSize  = image.appSectionSize as Data!
        appStartAddress  = image.appStartAddress as Data!
        
        appCRC  = image.appCRC as Data!
        patchCRC  = image.patchCRC as Data!
        appHeaderPatchCRC  = image.appHeaderPatchCRC as Data!
        
        totalImageCRC  = image.totalImageCRC as Data!

    }
    
    func dataFromFrame()->Data?{
        
        if  totalSections == nil || totalImageSize == nil ||
            patchSectionID == nil || patchSectionSize == nil || patchStartAddress == nil ||
            appHeaderPatchSectionID == nil || appHeaderPatchSectionSize == nil || appHeaderPatchStartAddress == nil ||
            appSectionID == nil || appSectionSize == nil || appStartAddress == nil ||
            appCRC == nil || patchCRC == nil || appHeaderPatchCRC == nil || totalImageCRC == nil
        {return nil}
        
        
        //16 bitlength data
        
        var dataLength:UInt16 = length!
        let returnFrameData = NSMutableData(bytes: &dataLength, length: MemoryLayout.size(ofValue: dataLength))
        
        //8 bit command data
        
        var commandData:UInt8 = command!.rawValue
        returnFrameData.append(&commandData, length: MemoryLayout.size(ofValue: commandData))
        
        // total sections
        
        var totalSect:UInt8 = totalSections!
        returnFrameData.append(&totalSect, length: MemoryLayout.size(ofValue: totalSect))
        
        // total image size
        var totalImageSiz:UInt32 = totalImageSize!
        returnFrameData.append(&totalImageSiz, length: MemoryLayout.size(ofValue: totalImageSiz))
        
        
        // patch section  data
        var patchSectionid:UInt8 = patchSectionID!
        
        returnFrameData.append(&patchSectionid, length: MemoryLayout.size(ofValue: patchSectionid))
        returnFrameData.append(patchSectionSize)
        returnFrameData.append(patchStartAddress)
        
        // AppHeaderPatch section  data
        
        var appHeaderPatchSectionid:UInt8 = appHeaderPatchSectionID!
        
        returnFrameData.append(&appHeaderPatchSectionid, length: MemoryLayout.size(ofValue: appHeaderPatchSectionid))
        returnFrameData.append(appHeaderPatchSectionSize)
        returnFrameData.append(appHeaderPatchStartAddress)
        
        // patch section  data
        
        var appSectionid:UInt8 = appSectionID!
        
        returnFrameData.append(&appSectionid, length: MemoryLayout.size(ofValue: appSectionid))
        returnFrameData.append(appSectionSize)
        returnFrameData.append(appStartAddress)
        
        // CRC data
        
        returnFrameData.append(totalImageCRC)
        returnFrameData.append(patchCRC)
        returnFrameData.append(appHeaderPatchCRC)
        returnFrameData.append(appCRC)
        
        
        return returnFrameData as Data
    }
    
}


class ImageInfoNotificationTaskManager: OTATask {
    
    
    var sucessBlock:((_ withImageInfoResponse:ImageInfoNotificationResponseFrame)->())? = nil
    var failBlock:((_ withError:OTAErrorType,_ errorString:String)->())? = nil
    
    init(interface:AtmelOTAInterface){
        
        super.init()
        otaInterface = interface
        otaInterface.delegate = self
    }
    
    func start(success sucess:@escaping (_ withImageInfoResponse:ImageInfoNotificationResponseFrame)->(),fail:@escaping (_ withError:OTAErrorType,_ errorString:String)->()){
        
        let imageInfoNotificationRequestFrame = ImageInfoNotificationRequestFrame(image: otaInterface.image!)
        
        if let requestData = imageInfoNotificationRequestFrame.dataFromFrame(){
            otaInterface.updatedProgressInfo("Validating image for OTA update", progress:nil)

            sucessBlock = sucess
            failBlock   = fail
            otaInterface.sendFrameData(requestData)
        }
        else{
            
            fail(OTAErrorType.localError, "Image Information Notify data  from image file could not be obtained")
        }
    }
    
    
    
}

extension ImageInfoNotificationTaskManager:AtmelOTAInterfaceDelegate{
    
    func receivedResponseFrameData(_ responseData:Data){
        
        if otaShouldAbort()
            {return}
        
        let imageResponseFrame = ImageInfoNotificationResponseFrame(withData: responseData)
        
        if imageResponseFrame.responseType == OTAUResponseType.success{
            
            otaInterface.updatedProgressInfo("Image validation successful", progress:nil)

            sucessBlock!(imageResponseFrame)
        }
        else if imageResponseFrame.responseType == OTAUResponseType.failure{
            
            failBlock!(OTAErrorType.communicationError,"Image Validation failed: (\(imageResponseFrame.errorCode))")
        }
        
    }
    
}

