/* ***************************************************************************

//BLEDeviceInfo.swift
//
Parser/Modal class for parsing and storing device info details.
//

Copyright (c) 2015, Atmel Corporation. All rights reserved.
Released under NDA
Licensed under Atmel's Limited License Agreement.


THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Atmel Corporation: http://www.atmel.com

******************************************************************************/


import Foundation
import CoreBluetooth

class BLEDeviceInfo : NSObject,CBPeripheralDelegate
{
    var BLEPeripheral : CBPeripheral!
    var BLEPeripheralRSSI : NSNumber!
    var BLEPeripheralLocalName: String?
    var BLEPeripheralManufacturerData : String?
    var BLEPeripheralTxPowerLevel : String?
    var BLEPeripheralsConnectable : Bool
    var BLEPeripheralServices : Array<CBUUID>?
    
    
    init(peripheral:CBPeripheral!, advertisementData:[AnyHashable: Any]!, RSSI:NSNumber!) {
        
        self.BLEPeripheral = peripheral
        self.BLEPeripheralRSSI = RSSI
        
       if let localName = advertisementData[CBAdvertisementDataLocalNameKey] as? String
       {
            self.BLEPeripheralLocalName = localName
        }
        else if let deviceName = peripheral.name
       {
            self.BLEPeripheralLocalName = deviceName
        }
        else
        {
            self.BLEPeripheralLocalName = "No Name"
        }
        
//        print("Advertisement packet=\(advertisementData)")
        //CBAdvertisementDataServiceUUIDsKey
        
        let newData = advertisementData[CBAdvertisementDataManufacturerDataKey] as? Data
        if newData == nil {self.BLEPeripheralManufacturerData = nil}
        else {self.BLEPeripheralManufacturerData = newData?.hexRepresentation()}
        
        
        let txNum = advertisementData[CBAdvertisementDataTxPowerLevelKey] as? NSNumber
        if txNum == nil {self.BLEPeripheralTxPowerLevel = nil}
        else { self.BLEPeripheralTxPowerLevel = txNum!.stringValue}
        
        let num = advertisementData[CBAdvertisementDataIsConnectable] as? NSNumber
        if num == nil {  self.BLEPeripheralsConnectable = false   }
        else {self.BLEPeripheralsConnectable = num!.boolValue}
        
        let serviceArray = advertisementData[CBAdvertisementDataServiceUUIDsKey] as? Array<CBUUID>
        if serviceArray == nil {  self.BLEPeripheralServices = nil   }
        else {self.BLEPeripheralServices = serviceArray}
        
        super.init()

    }
    
    
}
