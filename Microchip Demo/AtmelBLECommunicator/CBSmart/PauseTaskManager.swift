/***************************************************************************
 
 PauseTaskManager.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import UIKit

class PauseResponseFrame:OTAResponseFrame{
    
     override init() {
        super.init()
        length  = OTAFrameLength.PauseResponse.rawValue
        command = OTAUCommand.pause_OTAU_RESP
        
        
    }
    override init(withData:Data){
        
        super.init(withData:withData)
        
        
        if command != nil {
            switch(command!){
                
            case OTAUCommand.failure:
                responseType = OTAUResponseType.failure
                
                
                if withData.count >= 4
                //{errorCode = ResponseErrorCode(rawValue:(withData.subdata(in: NSMakeRange(3, 1)) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(with: NSMakeRange(3, 1)).count).pointee)}
                {errorCode = ResponseErrorCode(rawValue:(withData.subdata(in: 3..<4) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(in: 3..<4).count).pointee)}
                
                break
                
            case OTAUCommand.pause_OTAU_RESP:
                
                responseType = OTAUResponseType.success
                
                break
                
            default:
                responseType = OTAUResponseType.invalid
                break
            }
            
        }
        
    }
    func dataFromFrame()->Data?{
        
        
        //16 bitlength data
        
        var dataLength:UInt16 = length!
        let returnFrameData = NSMutableData(bytes: &dataLength, length: MemoryLayout.size(ofValue: dataLength))
        
        //8 bit command data
        
        var commandData:UInt8 = command!.rawValue
        returnFrameData.append(&commandData, length: MemoryLayout.size(ofValue: commandData))
        
        
        return returnFrameData as Data
    }
    
    
}

class  PauseRequestFrame:OTARequestFrame {
    
    override init() {
        
        super.init()
        length  = OTAFrameLength.PauseRequest.rawValue
        command = OTAUCommand.pause_OTAU_REQUEST
        
        
    }
    class func isPauseRequest(_ data:Data)->Bool{
        
        //let command =  OTAUCommand(rawValue:(data.subdata(in: NSMakeRange(2, 1)) as NSData).bytes.bindMemory(to: UInt8.self, capacity: data.subdata(with: NSMakeRange(2, 1)).count).pointee)
        let command =  OTAUCommand(rawValue:(data.subdata(in: 2..<1) as NSData).bytes.bindMemory(to: UInt8.self, capacity: data.subdata(in: 2..<1).count).pointee)
        
        if command == OTAUCommand.pause_OTAU_REQUEST
            {return true}
        return false
    }
     init(withData:Data){
        
        super.init()

        if withData.count >= 2
        //{length = (withData.subdata(in: NSMakeRange(0, 2)) as NSData).bytes.bindMemory(to: UInt16.self, capacity: withData.subdata(with: NSMakeRange(0, 2)).count).pointee}
        {length = (withData.subdata(in: 0..<2) as NSData).bytes.bindMemory(to: UInt16.self, capacity: withData.subdata(in: 0..<2).count).pointee}
        
        if withData.count >= 3
        //{command = OTAUCommand(rawValue:(withData.subdata(in: NSMakeRange(2, 1)) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(with: NSMakeRange(2, 1)).count).pointee)}
        {command = OTAUCommand(rawValue:(withData.subdata(in: 2..<1) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(in: 2..<1).count).pointee)}
        
    }

    
    func dataFromFrame()->Data?{
        
        
        //16 bitlength data
        
        var dataLength:UInt16 = length!
        let returnFrameData = NSMutableData(bytes: &dataLength, length: MemoryLayout.size(ofValue: dataLength))
        
        //8 bit command data
        
        var commandData:UInt8 = command!.rawValue
        returnFrameData.append(&commandData, length: MemoryLayout.size(ofValue: commandData))
        
        
        return returnFrameData as Data
    }
    
}

class PauseTaskManager: OTATask {
    
    
    var sucessBlock:(()->())? = nil
    var failBlock:((_ withError:OTAErrorType,_ errorString:String)->())? = nil
    
    init(interface:AtmelOTAInterface){
        
        super.init()
        otaInterface = interface
        otaInterface.delegate = self
    }
    
    func start(success sucess:@escaping ()->(),fail:@escaping (_ withError:OTAErrorType,_ errorString:String)->()){
        
        let pauseRequestFrame = PauseRequestFrame()
        
        if let requestData = pauseRequestFrame.dataFromFrame(){
            
            sucessBlock = sucess
            failBlock   = fail
            otaInterface.sendFrameData(requestData)
        }
    }
    func sendPauseAck(){
        
        let pauseResponseFrame = PauseResponseFrame()
        
        if let responseData = pauseResponseFrame.dataFromFrame(){
            
            otaInterface.sendFrameData(responseData)
        }
    }
}

extension PauseTaskManager:AtmelOTAInterfaceDelegate{
    
    func receivedResponseFrameData(_ responseData:Data){
        
        if otaShouldAbort()
        {return}
        
        let pauseResponseFrame = PauseResponseFrame(withData: responseData)
        
        if pauseResponseFrame.responseType == OTAUResponseType.success{
            sucessBlock!()
        }
        else if pauseResponseFrame.responseType == OTAUResponseType.failure{
            failBlock!(OTAErrorType.communicationError,"Pause request failed : (\(pauseResponseFrame.errorCode))")
        }
        
    }
    
}

