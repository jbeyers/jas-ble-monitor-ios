/***************************************************************************
 
 OTAUnits.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import Foundation

let IMAGE_FILE_EXTENTION = ".bin"

enum OTAMode{
    
    case pageMode
    case blockMode
}
enum OTAErrorType{
    
    case localError
    case communicationError
}


struct ImageHeader{
    
    var productID:UInt16!
    var vendorID:UInt16!
    var firmwareVersion:FirmWareVersion!
    var hardwareVersion:HardWareVersion!
    
    var hardwareRevision:UInt8!
    
    init(withDeviceInfo:DeviceInfoNotificationResponseFrame){
        productID = withDeviceInfo.productID
        vendorID = withDeviceInfo.vendorID
        firmwareVersion = withDeviceInfo.firmwareVersion
        hardwareVersion = withDeviceInfo.hardwareVersion
        hardwareRevision = withDeviceInfo.hardwareRevision
    }
    
    init(fileString:String?){
        
        var  file: FileHandle?
        do {
            try file = FileHandle(forReadingFrom: URL(string: fileString!)!)
            
        } catch let error as NSError {
            print(error.localizedDescription);
        }

        
        if file != nil {
            
            //get product id of file
            
            file!.seek(toFileOffset: 2)
            if let productIDData = file?.readData(ofLength: 2){
                productID = (productIDData as NSData).bytes.bindMemory(to: UInt16.self, capacity: productIDData.count).pointee
                
            }
            //get vendor id of file
            
            file!.seek(toFileOffset: 4)
            
            if let vendorIDData = file?.readData(ofLength: 2){
                vendorID = (vendorIDData as NSData).bytes.bindMemory(to: UInt16.self, capacity: vendorIDData.count).pointee
                
            }
            //get firmware version  of file
            
            file!.seek(toFileOffset: 7)
            
            if let firmwareversionData = file?.readData(ofLength: 4){
                firmwareVersion = FirmWareVersion(withData:firmwareversionData)
           
            }
           
            //get hardware version  of file
            
            file!.seek(toFileOffset: 11)
            
            if let hardwareversionData = file?.readData(ofLength: 2){
                hardwareVersion = HardWareVersion(withData:hardwareversionData)
          
            }
            //get hardwarerevision  of file
            
            file!.seek(toFileOffset: 13)
            
            if let hardwareRevisionData = file?.readData(ofLength: 1){
                hardwareRevision = (hardwareRevisionData as NSData).bytes.bindMemory(to: UInt8.self, capacity: hardwareRevisionData.count).pointee
                
                file?.closeFile()
            }
   
        }
    }
    init(withData:Data){
        
        if withData.count >= 4
        //{productID = (withData.subdata(in: NSMakeRange(2, 2)) as NSData).bytes.bindMemory(to: UInt16.self, capacity: withData.subdata(with: NSMakeRange(2, 2)).count).pointee}
        {productID = (withData.subdata(in: 2..<4) as NSData).bytes.bindMemory(to: UInt16.self, capacity: withData.subdata(in: 2..<4).count).pointee}
        
        if withData.count >= 6
        //{vendorID = (withData.subdata(in: NSMakeRange(4, 2)) as NSData).bytes.bindMemory(to: UInt16.self, capacity: withData.subdata(with: NSMakeRange(4, 2)).count).pointee}
        {vendorID = (withData.subdata(in: 4..<6) as NSData).bytes.bindMemory(to: UInt16.self, capacity: withData.subdata(in: 4..<6).count).pointee}
        
        
        if withData.count >= 11
        //{firmwareVersion = FirmWareVersion(withData: withData.subdata(in: NSMakeRange(7, 4))) }
        {firmwareVersion = FirmWareVersion(withData: withData.subdata(in: 7..<11)) }
        
        if withData.count >= 13
        //{hardwareVersion = HardWareVersion(withData: withData.subdata(in: NSMakeRange(11, 2))) }
        {hardwareVersion = HardWareVersion(withData: withData.subdata(in: 11..<13)) }
        
        if withData.count >= 14
        //{hardwareRevision = (withData.subdata(in: NSMakeRange(13, 1)) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(with: NSMakeRange(13, 1)).count).pointee }
        {hardwareRevision = (withData.subdata(in: 13..<14) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(in: 13..<14).count).pointee }
        
    }
    
    
}


class Image{
    
    var headerLength:UInt16!
    var productID:UInt16!
    var vendorID:UInt16!
    var flagbites:UInt8!

    var IMGTFlag:BoolFlag!
    var PATFlag:BoolFlag!
    var LIBflag:BoolFlag!
    var APPFlag:BoolFlag!
    var AHPlag:BoolFlag!

    var firmwareVersion:FirmWareVersion!
    var hardwareVersion:HardWareVersion!
    
    var hardwareRevison:UInt8!
    var securityLevel:OTAUSecurityLevel!
    var totalSections:UInt8!

    var totalImageSize:UInt32!
    var totalSectionSize:UInt32!

    var patchSectionID:UInt8!
    var patchSectionSize:Data!
    var patchStartAddress:Data!
    
    var appHeaderPatchSectionID:UInt8!
    var appHeaderPatchSectionSize:Data!
    var appHeaderPatchStartAddress:Data!
    
    var appSectionID:UInt8!
    var appSectionSize:Data!
    var appStartAddress:Data!
    
    var appCRC:Data!
    var patchCRC:Data!
    var appHeaderPatchCRC:Data!
    var totalImageCRC:Data!
    
    var patchSectionData:Data!
    {
        
        if fileURL != nil {
            
            var  file: FileHandle?
            do {
                try file = FileHandle(forReadingFrom:self.fileURL!)
                
            } catch _ as NSError {
                return nil
            }
            
            if file != nil {
                file!.seek(toFileOffset: UInt64(patchIndex))
                return file!.readData(ofLength: patchSectionSize.getValueFromUInt24())
            }
            file?.closeFile()
        }
        return nil
    }
    var appHeaderSectionData:Data!
    {
        
        if fileURL != nil {
            
            var  file: FileHandle?
            do {
                try file = FileHandle(forReadingFrom:self.fileURL!)
                
            } catch _ as NSError {
                return nil
            }
            
            if file != nil {
                file!.seek(toFileOffset: UInt64(appHeaderPatchIndex))
                return file!.readData(ofLength: appHeaderPatchSectionSize.getValueFromUInt24())
            }
            file?.closeFile()
        }
        return nil
    }
    var appSectionData:Data!
    {
        
        if fileURL != nil {
            
            var  file: FileHandle?
            do {
                try file = FileHandle(forReadingFrom:self.fileURL!)
                
            } catch _ as NSError {
                return nil
            }
            
            if file != nil {
                file!.seek(toFileOffset: UInt64(appIndex))
                return file!.readData(ofLength: appSectionSize.getValueFromUInt24())
            }
            file?.closeFile()
        }
        return nil
    }
    
    var patchIndex:Int = 0
    var appHeaderPatchIndex:Int = 0
    var appIndex:Int = 0

    
    var name:String?
    var fileURL:URL?
    
    init(withFile:String){
        
        
        var  file: FileHandle?
        do {
            try file = FileHandle(forReadingFrom: URL(string: withFile)!)
            
        } catch let error as NSError {
            print(error.localizedDescription);
        }

        if  file != nil{
            
            fileURL = URL(string: withFile)!
            name = fileURL?.lastPathComponent
            
//            HeaderData Range(0, 2)
            
            file!.seek(toFileOffset: 0)
            if let headerLengthData = file?.readData(ofLength: 2){
                headerLength = (headerLengthData as NSData).bytes.bindMemory(to: UInt16.self, capacity: headerLengthData.count).pointee
                
            }
//            productID Range(2, 2)
            
            file!.seek(toFileOffset: 2)
            if let productIDData = file?.readData(ofLength: 2){
                productID = (productIDData as NSData).bytes.bindMemory(to: UInt16.self, capacity: productIDData.count).pointee
                
            }
            
//            vendorID Range(4, 2)
            
            file!.seek(toFileOffset: 4)
            if let vendorIDData = file?.readData(ofLength: 2){
                vendorID = (vendorIDData as NSData).bytes.bindMemory(to: UInt16.self, capacity: vendorIDData.count).pointee
                
            }
//               flagbites Range(6, 1)

             file!.seek(toFileOffset: 6)
            if let flagbitesData = file?.readData(ofLength: 1){
                
                flagbites = (flagbitesData as NSData).bytes.bindMemory(to: UInt8.self, capacity: flagbitesData.count).pointee
                IMGTFlag = (flagbites & 0x01).toBoolFlag()
                PATFlag  = (flagbites & 0x02).toBoolFlag()
                LIBflag  = (flagbites & 0x04).toBoolFlag()
                APPFlag  = (flagbites & 0x08).toBoolFlag()
                AHPlag   = (flagbites & 0x10).toBoolFlag()
                
            }
 
//            firmwareVersionRange(7, 4)
            
            file!.seek(toFileOffset: 7)
            if let firmwareVersionData = file?.readData(ofLength: 4){
                
                firmwareVersion = FirmWareVersion(withData: firmwareVersionData)
                
            }
            

//            hardwareVersion Range(11, 2)
            
            file!.seek(toFileOffset: 11)
            if let hardwareVersionData = file?.readData(ofLength: 2){
                
                hardwareVersion = HardWareVersion(withData: hardwareVersionData)
                
            }
            
//          hardwareRevisonRange(13, 1)
            
            file!.seek(toFileOffset: 13)
            if let hardwareRevisonData = file?.readData(ofLength: 1){
                
                hardwareRevison = (hardwareRevisonData as NSData).bytes.bindMemory(to: UInt8.self, capacity: hardwareRevisonData.count).pointee
                
            }
            
//          securityLevel Range(14, 1)
            
            file!.seek(toFileOffset: 14)
            if let securityLevelData = file?.readData(ofLength: 1){
                
                securityLevel = OTAUSecurityLevel(rawValue:(securityLevelData as NSData).bytes.bindMemory(to: UInt8.self, capacity: securityLevelData.count).pointee)
            }
            
//          totalSections Range(15, 1)
            
            file!.seek(toFileOffset: 15)
            if let totalSectionsData = file?.readData(ofLength: 1){
                
                totalSections = (totalSectionsData as NSData).bytes.bindMemory(to: UInt8.self, capacity: totalSectionsData.count).pointee
            }

            
//          totalImageSize Range(16, 4)
            file!.seek(toFileOffset: 16)
            if let totalImageSizeData = file?.readData(ofLength: 4){
                
                totalImageSize = (totalImageSizeData as NSData).bytes.bindMemory(to: UInt32.self, capacity: totalImageSizeData.count).pointee
            }
            
//          patchSectionID Range(20,1)
            
            file!.seek(toFileOffset: 20)
            if let patchSectionIDData = file?.readData(ofLength: 1){
                
                patchSectionID = (patchSectionIDData as NSData).bytes.bindMemory(to: UInt8.self, capacity: patchSectionIDData.count).pointee
            }
            
//          patchSectionSizeRange(21,3)
            file!.seek(toFileOffset: 21)
            if let patchSectionSizeData = file?.readData(ofLength: 3){
                
                patchSectionSize = patchSectionSizeData
            }
            
//          patchStartAddress Range(24,4)
            
            file!.seek(toFileOffset: 24)
            if let patchStartAddressData = file?.readData(ofLength: 4){
                
                patchStartAddress = patchStartAddressData
            }
            
//          appHeaderPatchSectionID Range(28,1)
            
            file!.seek(toFileOffset: 28)
            
            if let appHeaderPatchSectionIDData = file?.readData(ofLength: 1){
                
                appHeaderPatchSectionID = (appHeaderPatchSectionIDData as NSData).bytes.bindMemory(to: UInt8.self, capacity: appHeaderPatchSectionIDData.count).pointee
            }
            
//          appHeaderPatchSectionSize Range(29,3)
            
            file!.seek(toFileOffset: 29)
            if let appHeaderPatchSectionSizeData = file?.readData(ofLength: 3){
                
                appHeaderPatchSectionSize = appHeaderPatchSectionSizeData
            }
            
            
//          appHeaderPatchStartAddress Range(32,4)
            
            file!.seek(toFileOffset: 32)
            if let appHeaderPatchStartAddressData = file?.readData(ofLength: 4){
                
                appHeaderPatchStartAddress = appHeaderPatchStartAddressData
            }

            
//          appSectionID Range(NSMakeRange(36,1))
            
            file!.seek(toFileOffset: 36)
            
            if let appSectionIDDData = file?.readData(ofLength: 1){
                
                appSectionID = (appSectionIDDData as NSData).bytes.bindMemory(to: UInt8.self, capacity: appSectionIDDData.count).pointee
            }
            
//          appSectionSize Range(37,3)
            
            file!.seek(toFileOffset: 37)
            if let appSectionSizeData = file?.readData(ofLength: 3){
                
                appSectionSize = appSectionSizeData
            }
            
//          appStartAddress Range(40,4)
            
            file!.seek(toFileOffset: 40)
            if let appStartAddressData = file?.readData(ofLength: 4){
                
                appStartAddress = appStartAddressData
            }
            
//          totalImageCRC Range(44,4)
            
            file!.seek(toFileOffset: 44)
            if let totalImageCRCData = file?.readData(ofLength: 4){
                
                totalImageCRC = totalImageCRCData
            }
//          patchCRC Range(48,4)
            
            file!.seek(toFileOffset: 48)
            if let patchCRCData = file?.readData(ofLength: 4){
                
                patchCRC = patchCRCData
            }
            
//          appHeaderPatchCRC Range(52,4)
            
            file!.seek(toFileOffset: 52)
            if let appHeaderPatchCRCData = file?.readData(ofLength: 4){
                
                appHeaderPatchCRC = appHeaderPatchCRCData
            }
            
//          appCRC Range(56,4)
            
            file!.seek(toFileOffset: 56)
            if let appCRCData = file?.readData(ofLength: 4){
                
                appCRC = appCRCData
            }
            
            
            let patchLength = patchSectionSize.getValueFromUInt24()
            let appHeaderPatchLength = appHeaderPatchSectionSize.getValueFromUInt24()
            let appSectLength = appSectionSize.getValueFromUInt24()
            
            patchIndex = 60
            
            var currentIndex = patchIndex
            
            currentIndex += patchLength
            appHeaderPatchIndex = currentIndex
            

            
            
            currentIndex += appHeaderPatchLength
            
            appIndex = appHeaderPatchIndex+appHeaderPatchLength
            

            
            totalSectionSize = UInt32(patchLength+appHeaderPatchLength+appSectLength)
        
        }
        file?.closeFile()
    }

//    init(withFile:String){
//
//        name = withFile.getLastPathComponent()
//
//        if let fileData = NSData(contentsOfURL: NSURL(string: withFile)!){
//            
//            if fileData.length >= 2
//                {headerLength = UnsafePointer<UInt16>(fileData.subdataWithRange(NSMakeRange(0, 2)).bytes).memory}
//            
//            if fileData.length >= 4
//                {productID = UnsafePointer<UInt16>(fileData.subdataWithRange(NSMakeRange(2, 2)).bytes).memory}
//            
//            if fileData.length >= 6
//                {vendorID = UnsafePointer<UInt16>(fileData.subdataWithRange(NSMakeRange(4, 2)).bytes).memory}
//            
//            if fileData.length >= 7{
//            
//                flagbites = UnsafePointer<UInt8>(fileData.subdataWithRange(NSMakeRange(6, 1)).bytes).memory
//                
//                IMGTFlag = (flagbites & 0x01).toBoolFlag()
//                PATFlag  = (flagbites & 0x02).toBoolFlag()
//                LIBflag  = (flagbites & 0x04).toBoolFlag()
//                APPFlag  = (flagbites & 0x08).toBoolFlag()
//                AHPlag   = (flagbites & 0x10).toBoolFlag()
//            }
//            
//            if fileData.length >= 11
//            {firmwareVersion = FirmWareVersion(withData: fileData.subdataWithRange(NSMakeRange(7, 4))) }
//            
//            if fileData.length >= 13
//            {hardwareVersion = HardWareVersion(withData: fileData.subdataWithRange(NSMakeRange(11, 2))) }
//            
//            if fileData.length >= 14
//            {hardwareRevison = UnsafePointer<UInt8>(fileData.subdataWithRange(NSMakeRange(13, 1)).bytes).memory }
//            
//            if fileData.length >= 15
//            {securityLevel = OTAUSecurityLevel(rawValue:UnsafePointer<UInt8>(fileData.subdataWithRange(NSMakeRange(14, 1)).bytes).memory) }
//            
//            if fileData.length >= 16
//            {totalSections = UnsafePointer<UInt8>(fileData.subdataWithRange(NSMakeRange(15, 1)).bytes).memory }
//            
//            if fileData.length >= 20
//            {totalImageSize = UnsafePointer<UInt32>(fileData.subdataWithRange(NSMakeRange(16, 4)).bytes).memory }
//            
//            if fileData.length >= 21
//            {patchSectionID =  UnsafePointer<UInt8>(fileData.subdataWithRange(NSMakeRange(20,1)).bytes).memory }
//            if fileData.length >= 24
//            {patchSectionSize =  fileData.subdataWithRange(NSMakeRange(21,3))}
//            if fileData.length >= 28
//            {patchStartAddress =  fileData.subdataWithRange(NSMakeRange(24,4))}
//            
//            if fileData.length >= 29
//            {appHeaderPatchSectionID =  UnsafePointer<UInt8>(fileData.subdataWithRange(NSMakeRange(28,1)).bytes).memory }
//            if fileData.length >= 32
//            {appHeaderPatchSectionSize =  fileData.subdataWithRange(NSMakeRange(29,3))}
//            if fileData.length >= 36
//            {appHeaderPatchStartAddress =  fileData.subdataWithRange(NSMakeRange(32,4))}
//            
//            if fileData.length >= 37
//            {appSectionID =  UnsafePointer<UInt8>(fileData.subdataWithRange(NSMakeRange(36,1)).bytes).memory }
//            if fileData.length >= 40
//            {appSectionSize =  fileData.subdataWithRange(NSMakeRange(37,3))}
//            if fileData.length >= 44
//            {appStartAddress =  fileData.subdataWithRange(NSMakeRange(40,4))}
//            
//            if fileData.length >= 48
//            {totalImageCRC =  fileData.subdataWithRange(NSMakeRange(44,4))}
//            if fileData.length >= 52
//            {patchCRC =  fileData.subdataWithRange(NSMakeRange(48,4))}
//            if fileData.length >= 56
//            {appHeaderPatchCRC =  fileData.subdataWithRange(NSMakeRange(52,4))}
//            if fileData.length >= 60
//            {appCRC =  fileData.subdataWithRange(NSMakeRange(56,4))}
//            
//            
//            let patchLength = patchSectionSize.getValueFromUInt24()
//            let appHeaderPatchLength = appHeaderPatchSectionSize.getValueFromUInt24()
//            let appSectLength = appSectionSize.getValueFromUInt24()
//            
//            var currentIndex = 60
//            
//            if fileData.length >= (currentIndex+patchLength)
//                {patchSectionData =  fileData.subdataWithRange(NSMakeRange(currentIndex,patchLength))}
//            currentIndex += patchLength
//            appHeaderPatchIndex = patchLength
//            
//            if fileData.length >= (currentIndex+appHeaderPatchLength)
//            {appHeaderSectionData =  fileData.subdataWithRange(NSMakeRange(currentIndex,appHeaderPatchLength))}
//            
//            currentIndex += appHeaderPatchLength
//            
//            appIndex = appHeaderPatchIndex+appHeaderPatchLength
//            
//            if fileData.length >= (currentIndex+appSectLength)
//            {appSectionData =  fileData.subdataWithRange(NSMakeRange(currentIndex,appSectLength))}
//            
//            totalSectionSize = UInt32(patchSectionData.length+appHeaderSectionData.length+appSectionData.length)
//        }
//    }
    
    
    
    ///Image File handling 
    
    class func validateFile(_ withURL:URL)->Bool{
        
        //YET TO IMPLEMENT
        return true
    }
    class func validateFileData(_ withData:Data)->Bool{
        
        //YET TO IMPLEMENT
        return true
    }
    
    class func filterImageFilesWith(_ currentDeviceInformation:DeviceInfoNotificationResponseFrame)->[String]?{
        
       return Image.getAllImageFileUrlArray()?.filter(){ Image.isImage($0, isValidForDevice: currentDeviceInformation)}        
    }
    class func findImageFilesWith(_ resumeInfo:ResumeRequestFrame)->String?{
        
        return Image.getAllImageFileUrlArray()?.filter(){ Image.isImage($0, isForResume:resumeInfo)}.first
        
    }
    class func findImageFilesWith(_ resumeInfo:ResumeResponseFrame)->String?{
        
        return Image.getAllImageFileUrlArray()?.filter(){ Image.isImage($0, isForResume:resumeInfo)}.first
        
    }

    class func filterImageFilesWith(_ imageHeader:ImageHeader)->[String]?{
        
        return Image.getAllImageFileUrlArray()?.filter(){ Image.isImage($0, isValidImage: imageHeader)}
        
        
        
    }
    
    class func saveFile(_ withData:Data,name:String){
        
        if Image.validateFileData(withData){
            
            let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
            //let documentsDirectory: AnyObject = paths.first! as AnyObject
            let documentsDirectory = paths.first! as NSString
            let dataPath = documentsDirectory.appendingPathComponent("OTAUImages")
            
            do {
                try FileManager.default.createDirectory(atPath: dataPath, withIntermediateDirectories: false, attributes: nil)
               try? withData.write(to: URL(fileURLWithPath: dataPath + name), options: [])
                
            } catch let error as NSError {
                print(error.localizedDescription);
                return
            }
        }
    }
    
    class func deleteImage(_ withPath:String)->Bool{
        do{
        try FileManager.default.removeItem(atPath: withPath)
        }
        catch _ as NSError {
            return false
        }
        return true
    }
    
    class func saveFile(_ withURL:URL)->(Bool,ImageHeader?){
        
        if Image.validateFile(withURL){
        
            let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
            //let documentsDirectory: AnyObject = paths.first! as AnyObject
            let documentsDirectory = paths.first! as NSString
            let dataPath = documentsDirectory.appendingPathComponent("OTAUImages")
            
            var isDirectory:ObjCBool = false
            let isPath =  FileManager.default.fileExists(atPath: dataPath, isDirectory: &isDirectory)
            //if isPath && isDirectory{
            if isPath && isDirectory.boolValue{
               //let nwPathURL = URL(string:dataPath)!.appendingPathComponent(withURL.lastPathComponent!)
                let nwPathURL = URL(string:dataPath)!.appendingPathComponent(withURL.lastPathComponent)
                
                if !self.isFilewithUrl(withURL, existsAtDirectory: dataPath) {
                
                    if let fileData = try? Data(contentsOf: withURL){
                        return ((try? fileData.write(to: URL(fileURLWithPath: nwPathURL.absoluteString),options: [])) != nil,ImageHeader(withData: fileData))
                    }
                }
            }
            else{
                do {
                    try FileManager.default.createDirectory(atPath: dataPath, withIntermediateDirectories: false, attributes: nil)
                    //let nwPathURL = URL(string:dataPath)!.appendingPathComponent(withURL.lastPathComponent!)
                    let nwPathURL = URL(string:dataPath)!.appendingPathComponent(withURL.lastPathComponent)
                    
                    if let fileData = try? Data(contentsOf: withURL){
                        
                        return ((try? fileData.write(to: URL(fileURLWithPath: nwPathURL.absoluteString),options: [])) != nil,ImageHeader(withData: fileData))
                        
                    }
                    
                } catch let error as NSError {
                    print(error.localizedDescription);
                    return (false,nil)
                }
            }
        }
            return (false,nil)
            
        }
    
    class func isFilewithUrl(_ pathUrl : URL, existsAtDirectory directoryPath:String) -> Bool {
        
        var fileNames = [String]()
        
        if let fileData = try? Data(contentsOf: pathUrl){
            
            let latestFileImageHeader = ImageHeader(withData: fileData)
            
            do{
                try fileNames = FileManager.default.subpathsOfDirectory(atPath: directoryPath)
                
                for fileName in fileNames {
                    
                    let filePathUrl = URL(string: directoryPath)?.appendingPathComponent(fileName)
                    let fileImageHeader = ImageHeader(fileString: filePathUrl?.absoluteString)
                    
                    if latestFileImageHeader.firmwareVersion.majorNumber == fileImageHeader.firmwareVersion.majorNumber && latestFileImageHeader.firmwareVersion.minorNumber == fileImageHeader.firmwareVersion.minorNumber && latestFileImageHeader.firmwareVersion.buildNumber == fileImageHeader.firmwareVersion.buildNumber {
                        
                        return true
                    }
                }
            }
            catch let error as NSError{
                print("copy error \(error)")
            }
        }
        return false
    }

    class func getAllImageFileUrlArray()->[String]?{
        
        if let documentsUrl = URL(string:Image.getImageFileFolderPath()){
        
        do {
            let directoryUrls = try  FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil, options: FileManager.DirectoryEnumerationOptions())
            return directoryUrls.filter{ $0.pathExtension == "bin" }.map{ $0.absoluteString}
        }
        catch _ as NSError {
            return nil
        }
    }
    return nil
}

    class func getImageFileFolderPath()->String{
        
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        //let documentsDirectory: AnyObject = paths.first! as AnyObject
        let documentsDirectory = paths.first! as NSString
        let dataPath = documentsDirectory.appendingPathComponent("OTAUImages")
        return dataPath
    }
    
    class func isImage(_ fileString:String?, isValidImage imageHeader:ImageHeader)->Bool{
        
        var  file: FileHandle?
        do {
            try file = FileHandle(forReadingFrom: URL(string: fileString!)!)
            
        } catch let error as NSError {
            print(error.localizedDescription);
            return false
        }
        
        if file != nil {
            
            
            //Check product id of file
            
            file!.seek(toFileOffset: 2)
            
            if let productIDData = file?.readData(ofLength: 2){
                let productID = (productIDData as NSData).bytes.bindMemory(to: UInt16.self, capacity: productIDData.count).pointee
                
                if productID != imageHeader.productID {
                    file?.closeFile()
                    return false
                }
            }
            else{
                file?.closeFile()
                return false
            }
            
            //Check vendor id of file
            
            file!.seek(toFileOffset: 4)
            
            if let vendorIDData = file?.readData(ofLength: 2){
                let vendorID = (vendorIDData as NSData).bytes.bindMemory(to: UInt16.self, capacity: vendorIDData.count).pointee
                
                if vendorID != imageHeader.vendorID{
                    file?.closeFile()
                    return false
                }
            }
            else{
                file?.closeFile()
                return false
            }
            
            //Check higher firmware version  of file
            
            //            file!.seekToFileOffset(7)
            //
            //            if let firmwareversionData = file?.readDataOfLength(4){
            //                let firmwareversion = FirmWareVersion(withData:firmwareversionData)
            //
            //                if !firmwareversion.isHigherThan(deviceInfo.firmwareVersion){
            //                    file?.closeFile()
            //                    return false
            //                }
            //            }
            //            else{
            //                file?.closeFile()
            //                return false
            //            }
            
            //Check higher hardware version  of file
            
            file!.seek(toFileOffset: 11)
            
            if let hardwareVersionData = file?.readData(ofLength: 2){
                let hardwareVersion = HardWareVersion(withData:hardwareVersionData)
                
                if !hardwareVersion.isEqual(imageHeader.hardwareVersion){
                    file?.closeFile()
                    return false
                }
            }
            else{
                file?.closeFile()
                return false
            }
            
            //Check hardwarerevision  of file
            
            file!.seek(toFileOffset: 13)
            
            if let hardwareRevisionData = file?.readData(ofLength: 1){
                let hardwareRevision = (hardwareRevisionData as NSData).bytes.bindMemory(to: UInt8.self, capacity: hardwareRevisionData.count).pointee
                
                if hardwareRevision != imageHeader.hardwareRevision{
                    file?.closeFile()
                    return false
                }
            }
            else{
                file?.closeFile()
                return false
            }
            
            file?.closeFile()
            
            return true
            
        }
        return false
    }
    

    class func isImage(_ fileString:String?, isValidForDevice deviceInfo:DeviceInfoNotificationResponseFrame)->Bool{
        
//        let file = NSFileHandle(forReadingAtPath:fileString!)
        
        var  file: FileHandle?
        do {
            try file = FileHandle(forReadingFrom: URL(string: fileString!)!)
            
        } catch let error as NSError {
            print(error.localizedDescription);
            return false
        }

        if file != nil {
            
            
            //Check product id of file
            
            file!.seek(toFileOffset: 2)
            
            if let productIDData = file?.readData(ofLength: 2){
                let productID = (productIDData as NSData).bytes.bindMemory(to: UInt16.self, capacity: productIDData.count).pointee
                
                if productID != deviceInfo.productID {
                        file?.closeFile()
                        return false
                }
            }
            else{
                file?.closeFile()
                return false
            }
            
            //Check vendor id of file
            
            file!.seek(toFileOffset: 4)
            
            if let vendorIDData = file?.readData(ofLength: 2){
                let vendorID = (vendorIDData as NSData).bytes.bindMemory(to: UInt16.self, capacity: vendorIDData.count).pointee
                
                if vendorID != deviceInfo.vendorID{
                    file?.closeFile()
                    return false
                }
            }
            else{
                file?.closeFile()
                return false
            }
            
            //Check higher firmware version  of file
            
//            file!.seekToFileOffset(7)
//            
//            if let firmwareversionData = file?.readDataOfLength(4){
//                let firmwareversion = FirmWareVersion(withData:firmwareversionData)
//                
//                if !firmwareversion.isHigherThan(deviceInfo.firmwareVersion){
//                    file?.closeFile()
//                    return false
//                }
//            }
//            else{
//                file?.closeFile()
//                return false
//            }
            
            //Check higher hardware version  of file
            
            file!.seek(toFileOffset: 11)
            
            if let hardwareVersionData = file?.readData(ofLength: 2){
                let hardwareVersion = HardWareVersion(withData:hardwareVersionData)
                
                if !hardwareVersion.isEqual(deviceInfo.hardwareVersion){
                    file?.closeFile()
                    return false
                }
            }
            else{
                file?.closeFile()
                return false
            }
            
            //Check hardwarerevision  of file
            
            file!.seek(toFileOffset: 13)
            
            if let hardwareRevisionData = file?.readData(ofLength: 1){
                let hardwareRevision = (hardwareRevisionData as NSData).bytes.bindMemory(to: UInt8.self, capacity: hardwareRevisionData.count).pointee
                
                if hardwareRevision != deviceInfo.hardwareRevision{
                    file?.closeFile()
                    return false
                }
            }
            else{
                file?.closeFile()
                return false
            }
            
        file?.closeFile()
  
        return true

        }
        return false
    }
    class func isImage(_ fileString:String?, isForResume resumeInfo:ResumeRequestFrame)->Bool{
        
        //        let file = NSFileHandle(forReadingAtPath:fileString!)
        
        var  file: FileHandle?
        do {
            try file = FileHandle(forReadingFrom: URL(string: fileString!)!)
            
        } catch let error as NSError {
            print(error.localizedDescription);
            return false
        }
        
        if file != nil {
            
            
            //Check product id of file
            
            file!.seek(toFileOffset: 2)
            
            if let productIDData = file?.readData(ofLength: 2){
                let productID = (productIDData as NSData).bytes.bindMemory(to: UInt16.self, capacity: productIDData.count).pointee
                
                if productID != resumeInfo.productID {
                    file?.closeFile()
                    return false
                }
            }
            else{
                file?.closeFile()
                return false
            }
            
            //Check vendor id of file
            
            file!.seek(toFileOffset: 4)
            
            if let vendorIDData = file?.readData(ofLength: 2){
                let vendorID = (vendorIDData as NSData).bytes.bindMemory(to: UInt16.self, capacity: vendorIDData.count).pointee
                
                if vendorID != resumeInfo.vendorID{
                    file?.closeFile()
                    return false
                }
            }
            else{
                file?.closeFile()
                return false
            }
            
            //Check  firmware version  of file
            
            file!.seek(toFileOffset: 7)
            
            if let firmwareversionData = file?.readData(ofLength: 4){
                let firmwareversion = FirmWareVersion(withData:firmwareversionData)
                
                if !firmwareversion.isEqualTo(resumeInfo.firmwareVersion!){
                    file?.closeFile()
                    return false
                }
            }
            else{
                file?.closeFile()
                return false
            }
            
            //Check higher hardware version  of file
            
            file!.seek(toFileOffset: 11)
            
            if let hardwareVersionData = file?.readData(ofLength: 2){
                let hardwareVersion = HardWareVersion(withData:hardwareVersionData)
                
                if !hardwareVersion.isEqual(resumeInfo.hardwareVersion!){
                    file?.closeFile()
                    return false
                }
            }
            else{
                file?.closeFile()
                return false
            }
            
            //Check hardwarerevision  of file
            
            file!.seek(toFileOffset: 13)
            
            if let hardwareRevisionData = file?.readData(ofLength: 1){
                let hardwareRevision = (hardwareRevisionData as NSData).bytes.bindMemory(to: UInt8.self, capacity: hardwareRevisionData.count).pointee
                
                if hardwareRevision != resumeInfo.hardwareRevision{
                    file?.closeFile()
                    return false
                }
            }
            else{
                file?.closeFile()
                return false
            }
            
            file?.closeFile()
            
            return true
            
        }
        return false
    }
    
    class func isImage(_ fileString:String?, isForResume resumeInfo:ResumeResponseFrame)->Bool{
        
        //        let file = NSFileHandle(forReadingAtPath:fileString!)
        
        var  file: FileHandle?
        do {
            try file = FileHandle(forReadingFrom: URL(string: fileString!)!)
            
        } catch let error as NSError {
            print(error.localizedDescription);
            return false
        }
        
        if file != nil {
            
            
            //Check product id of file
            
            file!.seek(toFileOffset: 2)
            
            if let productIDData = file?.readData(ofLength: 2){
                let productID = (productIDData as NSData).bytes.bindMemory(to: UInt16.self, capacity: productIDData.count).pointee
                
                if productID != resumeInfo.productID {
                    file?.closeFile()
                    return false
                }
            }
            else{
                file?.closeFile()
                return false
            }
            
            //Check vendor id of file
            
            file!.seek(toFileOffset: 4)
            
            if let vendorIDData = file?.readData(ofLength: 2){
                let vendorID = (vendorIDData as NSData).bytes.bindMemory(to: UInt16.self, capacity: vendorIDData.count).pointee
                
                if vendorID != resumeInfo.vendorID{
                    file?.closeFile()
                    return false
                }
            }
            else{
                file?.closeFile()
                return false
            }
            
            //Check  firmware version  of file
            
            file!.seek(toFileOffset: 7)
            
            if let firmwareversionData = file?.readData(ofLength: 4){
                let firmwareversion = FirmWareVersion(withData:firmwareversionData)
                
                if !firmwareversion.isEqualTo(resumeInfo.firmwareVersion!){
                    file?.closeFile()
                    return false
                }
            }
            else{
                file?.closeFile()
                return false
            }
            
            //Check higher hardware version  of file
            
            file!.seek(toFileOffset: 11)
            
            if let hardwareVersionData = file?.readData(ofLength: 2){
                let hardwareVersion = HardWareVersion(withData:hardwareVersionData)
                
                if !hardwareVersion.isEqual(resumeInfo.hardwareVersion!){
                    file?.closeFile()
                    return false
                }
            }
            else{
                file?.closeFile()
                return false
            }
            
            //Check hardwarerevision  of file
            
            file!.seek(toFileOffset: 13)
            
            if let hardwareRevisionData = file?.readData(ofLength: 1){
                let hardwareRevision = (hardwareRevisionData as NSData).bytes.bindMemory(to: UInt8.self, capacity: hardwareRevisionData.count).pointee
                
                if hardwareRevision != resumeInfo.hardwareRevision{
                    file?.closeFile()
                    return false
                }
            }
            else{
                file?.closeFile()
                return false
            }
            
            file?.closeFile()
            
            return true
            
        }
        return false
    }

}



class Section{
    
    var sectionData:Data!
    var sectionID:OTAUImageSectionID!
    var numberOFPages:UInt16!
    var image:Image!
    var index:Int = 0
    var pageSize:UInt16!{
        didSet
        {numberOFPages = UInt16(ceilf(Float(sectionData.count)/Float(pageSize)))}
    }
    
    init(fromImage:Image,sectionType:OTAUImageSectionID,pagesize:UInt16){
        
        image = fromImage
        
        switch(sectionType){
            
        case OTAUImageSectionID.patch:
            sectionData = fromImage.patchSectionData
            index = fromImage.patchIndex
            break
        case OTAUImageSectionID.appHeaderPatch:
            sectionData = fromImage.appHeaderSectionData
            index = fromImage.appHeaderPatchIndex

            
            break
        case OTAUImageSectionID.application:
            sectionData = fromImage.appSectionData
            index = fromImage.appIndex

            break
        }
        sectionID = sectionType
        pageSize = pagesize
        numberOFPages = UInt16(ceilf(Float(sectionData.count)/Float(pageSize)))
    }
    
    func nextSection()->Section?{
        if let index = OTAUImageSectionID.allValues.index(of: sectionID){
            
            if  (index+1) < OTAUImageSectionID.allValues.count
            {return Section(fromImage:image, sectionType: OTAUImageSectionID.allValues[index+1], pagesize:pageSize)}}
        
        return nil
    }
    
}

class Page{
    
    var pageNo:UInt16!
    var pageData:Data!
    
    var numberOFBlocks:UInt8!
    var section:Section!
    
    var blockSize:UInt16!{
        didSet
            {numberOFBlocks = UInt8(ceilf(Float(pageData.count)/Float(blockSize)))}
        }

    var segmentSize:Int!{
        didSet{
            maxNumberOfSegments = Int(ceilf(Float(pageData.count)/Float(segmentSize)))
        }
        
    }
    var maxNumberOfSegments:Int!
    
    init(fromSection:Section,pageNumber:UInt16,blocksize:UInt16,segmentsWithSize:Int){
        
        section = fromSection
        let remainingDatalength = section.sectionData.count - (Int(pageNumber*section.pageSize))
        
        if remainingDatalength > Int(section.pageSize)
            //{pageData = section.sectionData.subdata(in: NSMakeRange(Int(pageNumber*section.pageSize), Int(section.pageSize)))}
            {
                let end = Int(pageNumber*section.pageSize) + Int(section.pageSize)
                pageData = section.sectionData.subdata(in: Int(pageNumber*section.pageSize)..<end)}
        else
            //{pageData = section.sectionData.subdata(in: NSMakeRange(Int(pageNumber*section.pageSize), remainingDatalength))}
            {   let end = Int(pageNumber*section.pageSize) + remainingDatalength
                pageData = section.sectionData.subdata(in: Int(pageNumber*section.pageSize)..<end)}
        
        pageNo = pageNumber
        self.segmentSize = segmentsWithSize
        maxNumberOfSegments = pageData.count/segmentSize
        blockSize = blocksize
        numberOFBlocks = UInt8(UInt16(pageData.count)/blockSize)
    }
    
    func nextPage()->Page?{
        
        if pageNo+1 < section.numberOFPages
        {return Page(fromSection: section, pageNumber: pageNo+1 ,blocksize:blockSize,segmentsWithSize:self.segmentSize)}
        else
            {return nil}

    }
    
    func getPageSegmentData(_ withOffset:Int)->Data?{
        
        if withOffset < maxNumberOfSegments
            {
                //return pageData.subdata(in: NSMakeRange(Int(withOffset), Int(segmentSize)))
                let end = Int(withOffset) + Int(segmentSize)
                return pageData.subdata(in: Int(withOffset)..<end)
        }
            
        else if withOffset == maxNumberOfSegments
            {
                
                //return pageData.subdata(in: NSMakeRange(Int(withOffset),pageData.count - withOffset*segmentSize))
                let end = Int(withOffset) + (pageData.count - withOffset*segmentSize)
                return pageData.subdata(in: Int(withOffset)..<end)
            }
            
        else
            {return nil}
    }
    
    func progressFromSegment(_ withOffset:Int,totalImageSize:Float)->Float{
        
        var pageProgress:Float = 0
        
        var sectionProgress:Float = 0
        var imageProgress:Float = 0
        
        
        
        pageProgress = Float((withOffset)*self.segmentSize)
        
        
        sectionProgress = Float((Float(self.pageNo)+pageProgress)*Float(self.section.pageSize))
        
        if totalImageSize > 0
        {imageProgress = (Float(self.section.index)+sectionProgress)/Float(totalImageSize)}
        
        
        return (imageProgress)
        
    }
}

class Block{
    
    
    var page:Page!
    var blockNo:UInt8!
    var blockData:Data!
    var segmentSize:Int!{
        didSet{
            maxNumberOfSegments = blockData.count/segmentSize
        }
        
    }
    var maxNumberOfSegments:Int!
    
    init(fromPage:Page,blockNumber:UInt8,segmentsWithSize:Int){
        
        page = fromPage
        //blockData = fromPage.pageData.subdata(in: NSMakeRange(Int(UInt16(blockNo)*page.blockSize), Int(page.blockSize)))
        let end = Int(UInt16(blockNo)*page.blockSize) + Int(page.blockSize)
        blockData = fromPage.pageData.subdata(in: Int(UInt16(blockNo)*page.blockSize)..<end)
        
        
        let remainingDatalength = fromPage.pageData.count - (Int(UInt16(blockNo)*page.blockSize))
        
        if remainingDatalength > Int(page.blockSize)
        //{blockData = fromPage.pageData.subdata(in: NSMakeRange(Int(UInt16(blockNo)*page.blockSize), Int(page.blockSize)))}
        {   let end = Int(UInt16(blockNo)*page.blockSize) + Int(page.blockSize)
            blockData = fromPage.pageData.subdata(in: Int(UInt16(blockNo)*page.blockSize)..<end)}
        else
        //{blockData = fromPage.pageData.subdata(in: NSMakeRange(Int(UInt16(blockNo)*page.blockSize),remainingDatalength))}
        {   let end = Int(UInt16(blockNo)*page.blockSize) + remainingDatalength
            blockData = fromPage.pageData.subdata(in: Int(UInt16(blockNo)*page.blockSize)..<end)}
        
        blockNo = blockNumber
        segmentSize = segmentsWithSize
        maxNumberOfSegments = blockData.count/segmentSize
        
    }
    func getBlockSegmentData(_ withOffset:Int)->Data?{
        
        if withOffset <= maxNumberOfSegments
        //{ return blockData.subdata(in: NSMakeRange(Int(withOffset), Int(segmentSize)))}
        {   let end = Int(withOffset) + Int(segmentSize)
            return blockData.subdata(in: Int(withOffset)..<end)}
        else if withOffset == maxNumberOfSegments
        //{return blockData.subdata(in: NSMakeRange(Int(withOffset),blockData.count - withOffset*segmentSize))}
        {   let end = Int(withOffset) + (blockData.count - withOffset*segmentSize)
            return blockData.subdata(in: Int(withOffset)..<end)}
        else
        {return nil}
    }
    
    func progressFromSegment(_ withOffset:Int,totalImageSize:Float)->Float{
        
        var blockProgress:Float = 0
        var pageProgress:Float = 0

        var sectionProgress:Float = 0
        var imageProgress:Float = 0
        
        
    
        blockProgress = Float((withOffset)*self.segmentSize)
        
        pageProgress = Float((Float(blockNo)+blockProgress)*Float(page.blockSize))
        
        sectionProgress = Float((Float(page.pageNo)+pageProgress)*Float(page.section.pageSize))
        
        if totalImageSize > 0
            {imageProgress = (Float(page.section.index)+sectionProgress)/Float(totalImageSize)}
      
        
        return (imageProgress)
        
        
    }
    func nextBlock()->Block?{
        
        if blockNo+1 < page.numberOFBlocks
            {return Block(fromPage: page, blockNumber: blockNo+1,segmentsWithSize:self.segmentSize)}
        else
            {return nil}

    }
}

