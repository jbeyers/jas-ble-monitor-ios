
/* ***************************************************************************

FindMeInterface.swift



Copyright (c) 2015, Atmel Corporation. All rights reserved.
Released under NDA
Licensed under Atmel's Limited License Agreement.


THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Atmel Corporation: http://www.atmel.com

******************************************************************************/


protocol BLEFindMeInterfaceDelegate:NSObjectProtocol{
    
    func bleFindMeInterfaceDidReadRSSIValue(_ RSSI:NSNumber!)
    
}

import Foundation
import CoreBluetooth

class BLEFindMeInterface:NSObject{
    
    var delegate:BLEFindMeInterfaceDelegate?
    var sourcePeripheral:BLESourceDevice?
    
    var immediateAlertService:ImmediateAlertService?
    
    
    init(sourceDevice:BLESourceDevice ,delgateObject:BLEFindMeInterfaceDelegate){
        
        super.init()
        self.delegate = delgateObject
        self.sourcePeripheral=sourceDevice
        self.sourcePeripheral?.rssiDelegate=self
        let serviceArray = sourceDevice.CBSPeripheral.services
        
        immediateAlertService=ImmediateAlertService(deviceInfoService: serviceArray?.filter(){$0.uuid.isEqual(CBUUID(bleService: .ImmediateAlertService))}.first, andSourceDevice:sourcePeripheral)
        
        
    }
    
}

extension BLEFindMeInterface{
    
    func findMeWithAlertLevel(_ alertLevel:AlertLevel){
        
        immediateAlertService?.writeImmediateAlertLevel(alertLevel)
        
    }
    
    func readRSSIValue()
    {
        self.sourcePeripheral?.CBSPeripheral.readRSSI()
    }
}
//MARK: RSSI Reading

extension BLEFindMeInterface:BLESourceDeviceRSSIDelegate{
    
    func bleSourceDevice(_ bleSourceDevice: BLESourceDevice, didReadRSSI RSSI: NSNumber!, error: NSError!){
        
        if error == nil
        {self.delegate?.bleFindMeInterfaceDidReadRSSIValue(RSSI)}
    }
}
