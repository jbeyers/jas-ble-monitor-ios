/***************************************************************************
 
 OTAFrame.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import UIKit


enum OTAFrameLength:UInt16{
    
    case deviceInfoRequest = 1
    case imageRequest = 14
    case imageInfoRequest = 46
    case pageRequest = 4
    case blockRequest = 6
    static let PageEndRequest = pageRequest
    case sectionEndRequest = 2
    case imageEndRequest = 10
    static let  ImageSwitchRequest = blockRequest
    
    static let  PauseRequest = deviceInfoRequest // Length 1
    static let  PauseResponse = PauseRequest     // Length 1
    static let  ResumeRequest = PauseRequest     // Length 1

}
//Should add req commands

enum OTAUCommand:UInt8{
    
    //Device Info Commands
    
     case get_DEVICE_INFO_REQUEST   = 0x1B
     case get_DEVICE_INFO_RESP      = 0x1C

    //Image Notify Commands
    
    case image_NOTIFY_UPGRADE_REQUEST         = 0x01
    case image_NOTIFY_FORCED_UPDATE_REQUEST   = 0x0F

    case image_UPGRADE_NOTIFY_RESP            = 0x02
    case image_FORCED_UPDATE_NOTIFY_RESP      = 0x10

    
    //Image Info Notify Commands
    
    case image_INFO_NOTIFY_REQUEST = 0x03
    case image_INFO_NOTIFY_RESP = 0x04
    
    //Page Data Notify and   //Page end
    
    case page_DATA_NOTIFY_REQUEST  = 0x06
    case page_DATA_END_NOTIFY_REQUEST  = 0x18

    case page_DATA_NOTIFY_RESP  = 0x07
    case image_PAGE_INFO_ERROR  = 0x91
    
    //Block Data Notify 
    case block_DATA_NOTIFY_REQUEST = 0x08

    case block_DATA_NOTIFY_RESP = 0x09
    case image_BLOCK_INFO_ERROR = 0x92
    
  
    
    //section end
    
    case image_SECTION_END_NOTIFY_RESP = 0x1A
    
    case image_SECTION_END_NOTIFY_REQUEST = 0x19

    case image_SECTION_END_ERROR       = 0x93

    //Image end
    
    case image_END_NOTIFY_REQUEST = 0x13
    case image_END_NOTIFY_RESP = 0x14
    
    //  Pause
    case pause_OTAU_REQUEST = 0x0B
    case pause_OTAU_RESP = 0x0C
    
    //Resume

    case resume_OTAU_REQUEST = 0x0D
    case resume_OTAU_RESP = 0x0E


    //Image Switch
    case image_SWITCH_REQUEST = 0x11
    case image_SWITCH_RESP = 0x12
    
    case failure = 0x30
    
    
}

enum ResponseErrorCode:UInt8{
    
    
    case    invalid_COMMAND             = 0x95
    //Image Notify Error codes
    
    case   image_OLDER_VERSION          = 0x85
    case   invalid_VENDOR_ID            = 0x88
    case   invalid_PRODCUT_ID           = 0x89
    case   invalid_HARDWARE_VERSION     = 0x8A
    case   invalid_HARDWARE_REVISION    = 0x8B
    case   security_LEVEL_NOT_SUPPORTED = 0x8D
    
    
    //Image Notify info Error codes

    case   invalid_SECTION              = 0x8E
    case   invalid_IMAGE_SIZE           = 0x8F
    case   invalid_IMAGE                = 0x81

    case   unknown_ERROR                = 0x90
    
    //Image End
    
    case image_VERIFICATION_FAILED      = 0x86
    
    //Image Switch
    
    case image_SWITCH_ERROR             = 0x82

}
enum OTAUResponseType{

    case failure
    case success
    case invalid
}

enum BoolFlag:UInt8{
    
    case yes = 0x01
    case no  = 0x00
}
enum OTAUpdateType{
    
    case upgrade
    case downgrade
    case forcedUpdate
    
    var requestCommand:OTAUCommand{
        
        switch self {
            
        case .upgrade:
           return OTAUCommand.image_NOTIFY_UPGRADE_REQUEST
            
        default:
            return OTAUCommand.image_NOTIFY_FORCED_UPDATE_REQUEST
        }
    }
    var resposeCommand:OTAUCommand{
        
        switch self {
            
        case .upgrade:
            return OTAUCommand.image_UPGRADE_NOTIFY_RESP
            
        default:
            return OTAUCommand.image_FORCED_UPDATE_NOTIFY_RESP
        }
    }
    
    var updateType : String{
        switch self {
        case .downgrade:
            return "Downgrading..."
        default:
            return "Upgrading..."
        }
    }
    
}

let INVALID_OTAU_FIRMWARE_VERSION : Int64 = 0xFFFFFFFF


struct FirmWareVersion{
    
    var majorNumber:UInt8!
    var minorNumber:UInt8!
    var buildNumber:UInt16!
    
    
    var description: String
        {return "\(majorNumber).\(minorNumber)(\(buildNumber))"}
    
    init(withData:Data){
        
        if withData.count >= 2
            //{majorNumber = (withData.subdata(in: NSMakeRange(0, 1)) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(with: NSMakeRange(0, 1)).count).pointee}
            {majorNumber = (withData.subdata(in: 0..<1) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(in: 0..<1).count).pointee}
        
        if withData.count >= 2
            //{minorNumber = (withData.subdata(in: NSMakeRange(1, 1)) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(with: NSMakeRange(1, 1)).count).pointee}
            {minorNumber = (withData.subdata(in: 1..<1) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(in: 1..<1).count).pointee}
        
        if withData.count >= 4
            //{buildNumber = (withData.subdata(in: NSMakeRange(2, 2)) as NSData).bytes.bindMemory(to: UInt16.self, capacity: withData.subdata(with: NSMakeRange(2, 2)).count).pointee}
            {buildNumber = (withData.subdata(in: 2..<2) as NSData).bytes.bindMemory(to: UInt16.self, capacity: withData.subdata(in: 2..<2).count).pointee}
    }
    
    func getData()->Data{
        
        var major:UInt8 = majorNumber
        var minor:UInt8 = minorNumber
        var build:UInt16 = buildNumber
        
        let returnData = NSMutableData(bytes: &major, length: MemoryLayout.size(ofValue: major))
        
        returnData.append(&minor, length: MemoryLayout.size(ofValue: minor))
        returnData.append(&build, length: MemoryLayout.size(ofValue: build))
        
        return returnData as Data
    }
    func isHigherThan(_ firmwareversion:FirmWareVersion)->Bool{
        
        if majorNumber > firmwareversion.majorNumber
        {return true}
            
        else  if majorNumber == firmwareversion.majorNumber{
            
            if minorNumber > firmwareversion.minorNumber
            {return true}
                
            else  if minorNumber == firmwareversion.minorNumber
            {
                if self.buildNumber > firmwareversion.buildNumber
                {return true}
                
            }
            
        }
        return false
    }
    func isEqualTo(_ firmwareversion:FirmWareVersion)->Bool{
        
          if majorNumber == firmwareversion.majorNumber{
            
            if minorNumber == firmwareversion.minorNumber{
              
                if self.buildNumber == firmwareversion.buildNumber
                {return true}
                
            }
        }
        return false
    }

    
    
}
struct HardWareVersion{
    
    var majorNumber:UInt8!
    var minorNumber:UInt8!
    
    var description: String
        {return "\(majorNumber).\(minorNumber)"}
    init(withData:Data){
        
        if withData.count >= 2
        //{majorNumber = (withData.subdata(in: NSMakeRange(0, 1)) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(with: NSMakeRange(0, 1)).count).pointee}
        {majorNumber = (withData.subdata(in: 0..<1) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(in: 0..<1).count).pointee}
        
        if withData.count >= 2
        //{minorNumber = (withData.subdata(in: NSMakeRange(1, 1)) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(with: NSMakeRange(1, 1)).count).pointee}
        {minorNumber = (withData.subdata(in: 1..<1) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(in: 1..<1).count).pointee}
        
    }
    func isHigherThan(_ hardWareVersion:HardWareVersion)->Bool{
        
        if majorNumber > hardWareVersion.majorNumber
        {return true}
            
        else  if majorNumber == hardWareVersion.majorNumber{
            
            if minorNumber > hardWareVersion.minorNumber
            {return true}
        }
        return false
    }
    
    func isEqual(_ hardWareVersion:HardWareVersion)->Bool{
        
       
            if minorNumber == hardWareVersion.minorNumber && majorNumber == hardWareVersion.majorNumber
            {return true}
        
        return false
    }


    func getData()->Data{
        
        var major:UInt8 = majorNumber
        var minor:UInt8 = minorNumber
        
        let returnData = NSMutableData(bytes: &major, length: MemoryLayout.size(ofValue: major))
        returnData.append(&minor, length: MemoryLayout.size(ofValue: minor))
        
        return returnData as Data
    }
    
}


class OTARequestFrame: NSObject {
    
    var length:UInt16!
    var command:OTAUCommand!
}
class OTAResponseFrame: NSObject {
    
    var length:UInt16!
    var command:OTAUCommand!
    
    var responseType:OTAUResponseType = OTAUResponseType.invalid
    var errorCode:ResponseErrorCode?
    
    override init() {
        super.init()
    }
    init(withData:Data){
        
        if withData.count >= 2
        //{length = (withData.subdata(in: NSMakeRange(0, 2)) as NSData).bytes.bindMemory(to: UInt16.self, capacity: withData.subdata(with: NSMakeRange(0, 2)).count).pointee}
        {length = (withData.subdata(in: 0..<2) as NSData).bytes.bindMemory(to: UInt16.self, capacity: withData.subdata(in: 0..<2).count).pointee}
        
        if withData.count >= 3
        //{command = OTAUCommand(rawValue:(withData.subdata(in: NSMakeRange(2, 1)) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(with: NSMakeRange(2, 1)).count).pointee)}
        {command = OTAUCommand(rawValue:(withData.subdata(in: 2..<1) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(in: 2..<1).count).pointee)}
    }
}
