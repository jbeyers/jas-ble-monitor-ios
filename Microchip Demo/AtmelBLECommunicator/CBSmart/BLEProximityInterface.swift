
/* ***************************************************************************

BLEProximityInterface.swift


Copyright (c) 2015, Atmel Corporation. All rights reserved.
Released under NDA
Licensed under Atmel's Limited License Agreement.


THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Atmel Corporation: http://www.atmel.com

******************************************************************************/

import Foundation
import CoreBluetooth

protocol BLEProximityInterfaceDelegate:NSObjectProtocol{

    func bleProximityInterfaceDidReadLinkLossLevel(_ alertLevel:AlertLevel)
    func bleProximityInterfaceDidWriteValueForLinkLossCharacteristic(_ error: NSError!)
    func bleProximityInterfaceDidReadTransmissionPowerLevel(_ txPowerLevel:Int)
    func bleProximityInterfaceDidReadRSSIValue(_ RSSI:NSNumber!)

}

class BLEProximityInterface:NSObject{
    
    var delegate                                :BLEProximityInterfaceDelegate?
    var sourcePeripheral                        :BLESourceDevice?
    var linkLossService                         :CBService?
    var immediateAlertService                   :ImmediateAlertService?
    var transmissionLevelService                :CBService?
    
    var linkLossAlertLevelCharacteristic        :CBCharacteristic?
    var trasnsmissionPowerLevelCharacteristic   :CBCharacteristic?
    
     init(sourceDevice:BLESourceDevice){
        
        super.init()
        self.sourcePeripheral=sourceDevice
        self.sourcePeripheral!.operationsDelegate=self
        self.sourcePeripheral?.rssiDelegate=self
        
        let serviceArray = sourceDevice.CBSPeripheral.services
        
        linkLossService=serviceArray?.filter(){$0.uuid.isEqual(CBUUID(bleService: .LinkLossService))}.first
        
        immediateAlertService=ImmediateAlertService(deviceInfoService: serviceArray?.filter(){$0.uuid.isEqual(CBUUID(bleService: .ImmediateAlertService))}.first, andSourceDevice:sourcePeripheral)
        
        transmissionLevelService=serviceArray?.filter(){$0.uuid.isEqual(CBUUID(bleService: .TransmissionPowerService))}.first
        
        linkLossAlertLevelCharacteristic=(linkLossService?.characteristics)?.filter(){$0.uuid.isEqual(CBUUID(blecharacteristic: .AlertLevelCharacteristic))}.first
        
        
        trasnsmissionPowerLevelCharacteristic=(transmissionLevelService?.characteristics)?.filter(){$0.uuid.isEqual(CBUUID(blecharacteristic: .TransmissionPowerLevelCharacteristic))}.first
        
    }

}

extension BLEProximityInterface:BLESourceDeviceOperationsDelegate{
    
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice, didUpdateValueForCharacteristic characteristic: CBCharacteristic!, error: NSError!){
        
        print("received updated Value for characteristic with uuid \(characteristic.uuid.uuidString) with value \(characteristic.value) in BLEProximity")
        
        if characteristic.isEqual(linkLossAlertLevelCharacteristic){
            
            print("received updated Value for linkloss in BLEProximity")
            self.updatedLinkLossValueWithData(linkLossAlertLevelCharacteristic!.value!)
            
        }
        
        else if characteristic.isEqual(trasnsmissionPowerLevelCharacteristic)
            {self.updatedTransmissionPowerLevelWithData(trasnsmissionPowerLevelCharacteristic!.value!)}
        
    }
    
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice,didWriteValueForCharacteristic characteristic: CBCharacteristic!, error: NSError!){
        
        print(" written value for characteristics \(characteristic.uuid.uuidString) in ble proximity class")
        if characteristic .isEqual(linkLossAlertLevelCharacteristic)
            {self.delegate?.bleProximityInterfaceDidWriteValueForLinkLossCharacteristic(error)}
        }
    
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice,didUpdateNotificationStateForCharacteristic characteristic: CBCharacteristic!, error: NSError!){
        
        }
}


//MARK: RSSI Reading

extension BLEProximityInterface:BLESourceDeviceRSSIDelegate{
    
    func bleSourceDevice(_ bleSourceDevice: BLESourceDevice, didReadRSSI RSSI: NSNumber!, error: NSError!){
        
        if error == nil
            {self.delegate?.bleProximityInterfaceDidReadRSSIValue(RSSI)}
    }
}
//MARK: Characteristic Operations

extension BLEProximityInterface{
    
    
    //MARK: Link Loss Operations
    
    func readLinkLinkLossLevel()
        {self.sourcePeripheral!.readValueForCharacteristic(linkLossAlertLevelCharacteristic)}
    
    fileprivate func updatedLinkLossValueWithData(_ data:Data){
        
        if data.count > 0 {
             print(" \n received updated Value for linkloss in BLEProximity , data present updating the Value")
            let currentAlertLevel=linkLossAlertLevelCharacteristic?.value
            var rawValue:UInt8 = 0
            (currentAlertLevel as NSData?)?.getBytes(&rawValue, length: currentAlertLevel!.count)

            let readAlertLevel:AlertLevel? = AlertLevel(rawValue: rawValue)
            
             print(" \n received updated Value for linkloss in BLEProximity , data present updatting the value as = \(rawValue)")
            
            self.delegate?.bleProximityInterfaceDidReadLinkLossLevel(readAlertLevel!)
        }
    }
    
    func writeLinkLossAlertLevel(_ alertLevel:AlertLevel, withAcknowledgement isWithAck:Bool){
        
        var alertLevels=alertLevel.rawValue
        //let alertData:Data = Data(bytes: UnsafePointer<UInt8>(&alertLevels), count: sizeofValue(alertLevels))
        let alertData:Data = Data(bytes: &alertLevels, count: MemoryLayout.size(ofValue: alertLevels))
        self.sourcePeripheral?.writeValue(alertData, forCharacteristic: linkLossAlertLevelCharacteristic, withAcknowledgement: isWithAck)
    }
    
    //MARK: Immediate Alert Operations

    
    func writeImmediateAlertLevel(_ alertLevel:AlertLevel)
        {immediateAlertService?.writeImmediateAlertLevel(alertLevel)}
    
    //MARK: Transmission Power Level Operations
    
    func readTransmissionPowerLevelValue()
        {self.sourcePeripheral!.readValueForCharacteristic(trasnsmissionPowerLevelCharacteristic)}
    
   fileprivate func updatedTransmissionPowerLevelWithData(_ data:Data){

        let currentTXPowerLevel=trasnsmissionPowerLevelCharacteristic?.value
        var txPwrValue:Int8=0

        (currentTXPowerLevel as NSData?)?.getBytes(&txPwrValue, length: currentTXPowerLevel!.count)
    
        print("vvvvv"+"\(txPwrValue)")
    
        let txPower:Int=Int(txPwrValue)
        self.delegate?.bleProximityInterfaceDidReadTransmissionPowerLevel(txPower)
    }

    
    // MARK: RSSI Value Reading
    
    func readRSSIValue()
        {self.sourcePeripheral?.CBSPeripheral.readRSSI()}
}


        
