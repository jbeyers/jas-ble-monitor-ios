/***************************************************************************
 
 AtmelOTAInterface.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/
import UIKit
import CoreBluetooth
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}




protocol AtmelOTAInterfaceDelegate:NSObjectProtocol{
    
    func receivedResponseFrameData(_ responseData:Data)
    
}

protocol AtmelOTAInterfaceInfoDelegate:NSObjectProtocol{
    func atmelOtaInterfaceUpdateValueChanged()
    func updatedProgressDataInfo(_ withString:String,progress:Float?)
    func otaFailed(_ error:NSError)
}

enum OTAUpdateStatus{
    
    case otaAvailable
    case updateAvailable
    case updating
    case updated
    case paused
}

class AtmelOTAInterface: NSObject {
    
    
    var shouldPause:Bool = false
    
    var isIndicated:Bool = false
    var isWriteAck:Bool = false
    var responseData:Data!
    
    var sourcePeripheral:BLESourceDevice?
    var delegate:AtmelOTAInterfaceDelegate?
    var infoDelegate:AtmelOTAInterfaceInfoDelegate?
    var updateType:OTAUpdateType = OTAUpdateType.upgrade
    var currentDeviceInformation:DeviceInfoNotificationResponseFrame?
    
    var atmelOTAService:CBService?
    var otaNotificationCharacteristics:CBCharacteristic?
    var otaReadCharacteristics:CBCharacteristic?
    var otaWriteCharacteristics:CBCharacteristic?
    
    var imageFileUrlArray:[String]?
    var updateStatus:OTAUpdateStatus = OTAUpdateStatus.updated{
        
        didSet{
           infoDelegate?.atmelOtaInterfaceUpdateValueChanged()
        }
    }
    
    
    var validImageFileUrlArray:[String]?
    var otaAlert:UIAlertController?
    var updationProgress : Float?

    
    //OTA Task Objects
    
    var deviceInfoNotifcationTaskManager:DeviceInfoNotifcationTaskManager?
    var imageNotificationTaskManager:ImageNotificationTaskManager?
    var imageInfoNotificationTaskManager:ImageInfoNotificationTaskManager?
    var sectionUpdateManager:SectionUpdateManager?
    var imageUpdateEndTaskManager:ImageUpdateEndTaskManager?
    var imageSwitchTaskManager:ImageSwitchTaskManager?
    
    var pauseTaskManager:PauseTaskManager?
    var resumeTaskManager:ResumeTaskManager?
    

    var latestImageFileUrl:String?
    var currentOTAMode:OTAMode?

    var image:Image?
    
    var currentBlock:Block?
    var currentPage:Page?
    var currentSection:Section?
    
    init(sourceDevice:BLESourceDevice? ,delgateObject:AtmelOTAInterfaceInfoDelegate?){
        
        super.init()
        self.infoDelegate = delgateObject
        self.sourcePeripheral=sourceDevice
        let serviceArray = sourceDevice?.CBSPeripheral.services
        
        atmelOTAService=serviceArray?.filter(){$0.uuid.isEqual(CBUUID(bleService:BLEService.AtmelOTAService))}.first
        
        otaNotificationCharacteristics=(atmelOTAService?.characteristics )?.filter(){$0.uuid.isEqual(CBUUID(blecharacteristic: BLECharacteristic.AtmelOTANotificationCharacteristics))}.first
        otaReadCharacteristics=(atmelOTAService?.characteristics )?.filter(){$0.uuid.isEqual(CBUUID(blecharacteristic: BLECharacteristic.AtmelOTAReadCharacteristics))}.first
        otaWriteCharacteristics=(atmelOTAService?.characteristics )?.filter(){$0.uuid.isEqual(CBUUID(blecharacteristic: BLECharacteristic.AtmelOTAWriteCharacteristics))}.first
        addListner()
        imageFileUrlArray = Image.getAllImageFileUrlArray()
        
//        if imageFileUrlArray?.count > 0{
            startInterface()
//        }
    }
    deinit{
        removeListner()
        
    }
    
    class func hasOTA(_ sourceDevice:CBPeripheral?)->Bool{
        
        if let _ = (sourceDevice?.services?.filter(){$0.uuid.isEqual(CBUUID(bleService:BLEService.AtmelOTAService))}.first)
        {return true}
        return false
    }
    
    
    func startInterface(){
        sourcePeripheral?.CBSPeripheral.setNotifyValue(true, for: otaNotificationCharacteristics!)
    }
    
    func isReadyWithNoitifyCharacteristics(){
        
        getDeviceInfo()
    }
    func isNowIdle(){
        if shouldPause{
            initiatePauseRequest()
        }
    }
    
}


//MARK: OTA File Processing

extension AtmelOTAInterface{
    
    func getOTAUpdateType(){
        if image != nil && currentDeviceInformation != nil {
            if image!.firmwareVersion.isHigherThan(currentDeviceInformation!.firmwareVersion)
            {updateType = OTAUpdateType.upgrade}
            else if  image!.firmwareVersion.isEqualTo(currentDeviceInformation!.firmwareVersion)
            {updateType = OTAUpdateType.forcedUpdate}
            else
            {updateType = OTAUpdateType.downgrade}
        }
    }
    
    func getAllImageFileUrls()->[String]?{
        
        let documentsUrl =  Bundle.main.bundleURL
        
        do {
            let directoryUrls = try  FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil, options: FileManager.DirectoryEnumerationOptions())
            return directoryUrls.filter{ $0.pathExtension == "bin" }.map{ $0.absoluteString}
        }
        catch _ as NSError {
            return nil
        }
        
    }
    
    func filterImageFilesWith(){
        
        validImageFileUrlArray = imageFileUrlArray?.filter(){ Image.isImage($0, isValidForDevice: currentDeviceInformation!)}
        
       
        
    }
    
    func findLatestImageFile(){
        
        if validImageFileUrlArray != nil
            {latestImageFileUrl = validImageFileUrlArray?.getImageWithLatestFirmwareVersion()}
    }
    
    
    func getImageFileWith(_ firmwareVersion:FirmWareVersion) -> String? {
        
        if validImageFileUrlArray != nil {
            
            return validImageFileUrlArray?.getImageWithFirmwareVersion(firmwareVersion)
        }
        return nil
    }
    
    func updatedProgressInfo(_ withString:String,progress:Float?)
    {
        self.updationProgress = progress
        self.infoDelegate?.updatedProgressDataInfo(withString, progress: progress)
    }
    
}

    
    //MARK: OTA TASK FUNCTIONS
    
    
 extension AtmelOTAInterface{
    
    func getDeviceInfo(){
        
        print("OTA TASK: Get device Info")
        deviceInfoNotifcationTaskManager = DeviceInfoNotifcationTaskManager(interface: self)
        deviceInfoNotifcationTaskManager?.start(
            
            success: {
                (withDeviceInfoResponse:DeviceInfoNotificationResponseFrame) -> () in
                
                print("OTA SUCCESS: Get device Info")
            
                self.currentDeviceInformation = withDeviceInfoResponse
                self.filterImageFilesWith()
                self.updateStatus = OTAUpdateStatus.updated
                
                if self.validImageFileUrlArray?.count > 0{
                    self.updateStatus = OTAUpdateStatus.otaAvailable
                }
                
                //   Filtered Image array Done
                
                self.findLatestImageFile()
                
                
                if withDeviceInfoResponse.OTAUFirmwareVersion == nil  || self.getImageFileWith(withDeviceInfoResponse.OTAUFirmwareVersion!) == nil {
                    
                    print(" OTAUFirmwareVersion is nil")
                    
                    if self.latestImageFileUrl != nil && ImageHeader(fileString: self.latestImageFileUrl).firmwareVersion.isHigherThan(withDeviceInfoResponse.firmwareVersion){
                        
                        self.showUpdateAvailableAlert()
                    }
                }
                else if withDeviceInfoResponse.OTAUFirmwareVersion != nil && self.getImageFileWith(withDeviceInfoResponse.OTAUFirmwareVersion!) != nil{
                    
                    print("OTAUFirmwareVersion is \(String(describing: withDeviceInfoResponse.OTAUFirmwareVersion))")
                    
                    self.image = Image(withFile:self.getImageFileWith(withDeviceInfoResponse.OTAUFirmwareVersion!)!)

                    self.showResumeAvailableAlert()
                }
                
                
            }
            ,fail: { (withError:OTAErrorType,errorString:String) -> () in

                print("OTA FAIL: Get device Info")

                self.infoDelegate?.otaFailed(NSError(domain: "OTA FAIL", code: 200, userInfo: [NSLocalizedDescriptionKey:errorString]))
        })
    
    }
    
    
    
    //MARK:Alert Methods
    func showResumeFailureAlert(){
        
        
        if let topController = Util.getTopViewcontroller() {
            
            otaAlert = UIAlertController(title:"Resume Failed",message: "Resuming firmware update failed", preferredStyle: UIAlertControllerStyle.alert)
            
            let updateAction = UIAlertAction(title: "Retry", style: UIAlertActionStyle.default) {
                UIAlertAction in
                self.forcedImageNotification()
                self.updateStatus = OTAUpdateStatus.updating
                
            }
            let IgnoreAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default) {
                UIAlertAction in
                
                self.image = nil
                self.updateStatus = OTAUpdateStatus.updateAvailable

                //IGNORE OTA
                
            }
            otaAlert!.addAction(updateAction)
            otaAlert!.addAction(IgnoreAction)
            topController.present(otaAlert!, animated: true, completion: nil)
        }
    }
    
    
    func showResumeAvailableAlert(){
        
       
        if let topController = Util.getTopViewcontroller() {
            
            let resumeAction = UIAlertAction(title: "Resume", style: UIAlertActionStyle.default) {
                UIAlertAction in
//                self.imageInfoNotification()
//                self.updateStatus = OTAUpdateStatus.Updating
//                if topController .isKindOfClass(BLEServiceList) || topController.isKindOfClass(BatteryServiceVC)
//                {topController.performSegueWithIdentifier(OTA_INFO_VC_SEGUE, sender: topController)}
                
                self.imageNotification() 
            }
            
            let updateAction = UIAlertAction(title: "Upgrade to \(ImageHeader(fileString: self.latestImageFileUrl!).firmwareVersion.description)", style: .default, handler: { (action) in
                
                self.image = Image(withFile: self.latestImageFileUrl!)
                self.imageNotification()
            })
            
            
            let IgnoreAction = UIAlertAction(title: "Ignore", style: UIAlertActionStyle.default) {
                UIAlertAction in
                
                self.image = nil
                
                //IGNORE OTA

            }
            
            
             if self.latestImageFileUrl != nil && ImageHeader(fileString: self.latestImageFileUrl).firmwareVersion.isHigherThan(self.image!.firmwareVersion){
                otaAlert = UIAlertController(title:"Pending Update",message: "An update with firmware version \(self.image!.firmwareVersion.description) is pending. Please choose an action.", preferredStyle: UIAlertControllerStyle.alert)
                otaAlert!.addAction(updateAction)
            }
             else{
                otaAlert = UIAlertController(title:"Resume Availbable",message: "Resuming firmware upgrade", preferredStyle: UIAlertControllerStyle.alert)
            }
            
            otaAlert!.addAction(resumeAction)
            otaAlert!.addAction(IgnoreAction)
            
            
            topController.present(otaAlert!, animated: true, completion: nil)
        }
    }

    func showUpdateAvailableAlert(){
        
        if let topController = Util.getTopViewcontroller() {
            
            otaAlert = UIAlertController(title:"Update Availbable",message: "Firmware can be upgraded from\(currentDeviceInformation!.firmwareVersion.description) to \(ImageHeader(fileString: self.latestImageFileUrl).firmwareVersion.description)", preferredStyle: UIAlertControllerStyle.alert)
            
            let updateAction = UIAlertAction(title: "Update", style: UIAlertActionStyle.default) {
                UIAlertAction in
//                self.imageInfoNotification()
//                self.updateStatus = OTAUpdateStatus.Updating
//                if topController .isKindOfClass(BLEServiceList) || topController.isKindOfClass(BatteryServiceVC)
//                    {topController.performSegueWithIdentifier(OTA_INFO_VC_SEGUE, sender: topController)}
                
                self.image = Image(withFile: self.latestImageFileUrl!)
                self.imageNotification()

            }
            let IgnoreAction = UIAlertAction(title: "Ignore", style: UIAlertActionStyle.default) {
                UIAlertAction in
                self.image = nil

                //IGNORE OTA
            }
            
            otaAlert!.addAction(updateAction)
            otaAlert!.addAction(IgnoreAction)
            topController.present(otaAlert!, animated: true, completion: nil)
        }
    }
    
    func showUpdateSuccessAlert(){
        
        if let topController = Util.getTopViewcontroller() {
            
            otaAlert = UIAlertController(title:"Update Successful",message: "Firmware successfully upgraded to \(image!.firmwareVersion.description)", preferredStyle: UIAlertControllerStyle.alert)
           
            let IgnoreAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                UIAlertAction in
                topController.navigationController?.popToRootViewController(animated: true)
            }
            otaAlert!.addAction(IgnoreAction)
            topController.present(otaAlert!, animated: true, completion: nil)
        }
    }

    
    func imageNotification(){
        
        print("OTA TASK: Image Notification")

        getOTAUpdateType()
        imageNotificationTaskManager = ImageNotificationTaskManager(interface: self)
        
        imageNotificationTaskManager?.start(
            
            success: { (isResume:Bool) -> () in
                
                print("OTA SUCCESS: Image Notification")

//                self.updateStatus = OTAUpdateStatus.UpdateAvailable
//                if isResume
//                {
//                    self.showResumeAvailableAlert()
                
                self.imageInfoNotification()
                self.updateStatus = OTAUpdateStatus.updating
                
                let topViewController = Util.getTopViewcontroller()
                
                if topViewController != nil{
                    
                    if topViewController!.isKind(of: BLEServiceList.self) || topViewController!.isKind(of: BatteryServiceVC.self)
                    {
                        topViewController!.performSegue(withIdentifier: OTA_INFO_VC_SEGUE, sender: topViewController)
                    }
                }
                
                
//                }
//                else
//                {self.showUpdateAvailableAlert()}

            },
            
            fail: { (withError:OTAErrorType,errorString:String) -> () in
                
                print("OTA FAIL: Image Notification")

                 self.infoDelegate?.otaFailed(NSError(domain: "OTA FAIL", code: 200, userInfo: [NSLocalizedDescriptionKey:errorString]))
            }
        )
    }
    
    
    //initiated  by user
    
    func forcedImageNotification(){
        
        print("OTA TASK: Image Notifcatiom")

        getOTAUpdateType()

        imageNotificationTaskManager = ImageNotificationTaskManager(interface: self)
        
        imageNotificationTaskManager?.start(
            
            success: { (isResume:Bool) -> () in
                
                print("OTA SUCCESS: Image Notifcatiom")
                self.updateStatus = OTAUpdateStatus.updating
                self.imageInfoNotification()
                
            },
            
            fail: { (withError:OTAErrorType,errorString:String) -> () in
                
                print("OTA FAIL: Image Notifcatiom")

                 self.infoDelegate?.otaFailed(NSError(domain: "OTA FAIL", code: 200, userInfo: [NSLocalizedDescriptionKey:errorString]))
            }
        )
    }
    
    func imageInfoNotification(){
        
        print("OTA TASK: Image Info Notifcation")

        imageInfoNotificationTaskManager = ImageInfoNotificationTaskManager(interface: self)
        
        imageInfoNotificationTaskManager?.start(
            
            success: { (withImageInfoResponse:ImageInfoNotificationResponseFrame) -> () in
                
                print("OTA SUCCESS: Image Info Notifcation")

                self.currentOTAMode = withImageInfoResponse.currentOTAMode
                self.startImageDataUpdate(withImageInfoResponse)
                
            },
            
            fail: { (withError:OTAErrorType,errorString:String) -> () in
                print("OTA FAIL: Image Info Notifcation")

                self.infoDelegate?.otaFailed(NSError(domain: "OTA FAIL", code: 200, userInfo: [NSLocalizedDescriptionKey:errorString]))
            }
        )
    }
    
    func startImageDataUpdate(_ withImageInfoResponse:ImageInfoNotificationResponseFrame){
        
        sectionUpdateManager = SectionUpdateManager(interface: self, withImageInfoResponse: withImageInfoResponse)
        
        sectionUpdateManager?.start(
            
            success: { () -> () in
                
                self.sendImageEndNotification()
                print("All Sections Updated")
            },
            
            fail: { (withError:OTAErrorType,errorString:String) -> () in
                self.infoDelegate?.otaFailed(NSError(domain: "OTA FAIL", code: 200, userInfo: [NSLocalizedDescriptionKey:errorString]))
        })
        
        
    }
    
    func sendImageEndNotification(){
        
        print("OTA TASK: Image End")

        imageUpdateEndTaskManager = ImageUpdateEndTaskManager(interface: self)
        
        imageUpdateEndTaskManager?.start(
            
            success: { () -> () in
                self.sendImageSwitchNotification()
                print("OTA SUCCESS: Image End")

            },
            
            fail: { (withError:OTAErrorType,errorString:String) -> () in
                
                print("OTA FAIL: Image End")

                self.infoDelegate?.otaFailed(NSError(domain: "OTA FAIL", code: 200, userInfo: [NSLocalizedDescriptionKey:errorString]))
        })
    }
    func abortAndPause(){
        shouldPause = true
        OTATask.abort()
        
    }
    func initiatePauseRequest(){
        
        shouldPause = false

        print("OTA TASK: Pause Request")

        pauseTaskManager = PauseTaskManager(interface: self)
        pauseTaskManager?.start(success: {
            print("OTA SUCCESS: Pause Request")

            self.updateStatus = .paused
            }, fail: { (withError, errorString) in
                
                print("OTA FAIL: Pause Request")

               self.infoDelegate?.otaFailed(NSError(domain: "OTA FAIL", code: 200, userInfo: [NSLocalizedDescriptionKey:"Pause Request failed"]))
        })
        
    }
    func sendPauseRequestAcknoledgement(){
        
        let pauseTaskManager = PauseTaskManager(interface: self)
        pauseTaskManager.sendPauseAck()
    }
    
    func initiateResumeRequest() {
        print("OTA TASK: Resume Request")

        resumeTaskManager = ResumeTaskManager(interface: self)
        resumeTaskManager?.start(success: { (resumeResponse:ResumeResponseFrame) in
            
            print("OTA SUCCESS: Resume Request")
            self.proceedForResumeResponseFromTarget(resumeResponse)
            },
        fail: { (withError, errorString) in
            
            print("OTA FAIL: Resume Request")

                self.showResumeFailureAlert()
        })
    }
    func proceedForResumeResponseFromTarget(_ resumeResFrame:ResumeResponseFrame){
        
        self.image = Image(withFile: Image.findImageFilesWith(resumeResFrame)!)
        
        if self.image != nil{
            forcedImageNotification()
        }
        else{
            showResumeFailureAlert()
        }
        
    }
    func proceedForResumeRequestFromTarget(_ resumeReqFrame:ResumeRequestFrame){
        
        self.image = Image(withFile: Image.findImageFilesWith(resumeReqFrame)!)
        
        if self.image != nil{
            forcedImageNotification()
        }
        else{
            showResumeFailureAlert()
        }

    }
    
    func sendImageSwitchNotification(){
        
        print("OTA TASK: Image Switch Request")

        imageSwitchTaskManager = ImageSwitchTaskManager(interface: self)
        
        imageSwitchTaskManager?.start(
            
            success: { () -> () in
                
                print("OTA SUCCESS: Image Switch Request")

                self.updateStatus = OTAUpdateStatus.updated
                self.showUpdateSuccessAlert()
            },
            
            fail: { (withError:OTAErrorType,errorString:String) -> () in
                
                print("OTA FAIL: Image Switch Request")

                self.infoDelegate?.otaFailed(NSError(domain: "IMAGE SWITCH FAIL", code: 200, userInfo: [NSLocalizedDescriptionKey:"switching fimrware image failed"]))
        })
    }
    

}

extension AtmelOTAInterface{
    
    func sendFrameData(_ frameData:Data){
        sourcePeripheral?.writeValue(frameData, forCharacteristic: otaWriteCharacteristics, withAcknowledgement: true)
//        
        isIndicated = false
        isWriteAck  = false
    }
    
    
}
extension AtmelOTAInterface:BLESourceDeviceOTAOperationsDelegate{
    
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice, didUpdateValueForCharacteristic characteristic: CBCharacteristic!, error: NSError!){
        
        if characteristic.isEqual(otaNotificationCharacteristics){
            
            print("OTA Response")

            
            if updateStatus == OTAUpdateStatus.paused{
                
                //If Paused check for resume requests from firmware
                
                if ResumeRequestFrame.isResumeRequest(otaNotificationCharacteristics!.value!){
                    
                    proceedForResumeRequestFromTarget(ResumeRequestFrame(withData: otaNotificationCharacteristics!.value!))
                    return
                }
            }
            
                
                //Check for Pause requests from firmware
                
                if PauseRequestFrame.isPauseRequest(otaNotificationCharacteristics!.value!){
                    self.updateStatus = .paused
                    sendPauseRequestAcknoledgement()
                    return
                }
//                delegate?.receivedResponseFrameData(otaNotificationCharacteristics!.value!)
                
                
                            isIndicated = true
                
                            if isWriteAck{
                                    delegate?.receivedResponseFrameData(otaNotificationCharacteristics!.value!)
                                isIndicated = false
                                isWriteAck  = false
                            }
                            else
                            {
                                responseData = characteristic.value
                            }
                
            
        }
    }
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice,didWriteValueForCharacteristic characteristic: CBCharacteristic!, error: NSError!){
        
        print("Write ACK")
        
        if characteristic.isEqual(otaWriteCharacteristics){
            
            isWriteAck = true
            
            if isIndicated && responseData != nil{
                
            delegate?.receivedResponseFrameData(responseData)
            responseData = nil
            isIndicated = false
            isWriteAck  = false
            }
        }
    }
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice,didUpdateNotificationStateForCharacteristic characteristic: CBCharacteristic!, error: NSError!){
        
        if characteristic.isEqual(otaNotificationCharacteristics)
            {isReadyWithNoitifyCharacteristics()}
        
    }
    
}
extension AtmelOTAInterface
{
    func addListner()
    {
        
        NotificationCenter.listenNotification(self, notificationName: Constants.NotificationName.onEnterBackground)
        NotificationCenter.listenNotification(self, notificationName: Constants.NotificationName.onEnterForeground)
    }
    
    func removeListner()
    {
        NotificationCenter.removelistener(self, notificationName: Constants.NotificationName.onEnterBackground)
        NotificationCenter.removelistener(self, notificationName: Constants.NotificationName.onEnterForeground)
    }
    

    
    func onEnterBackground()
    {
    }
    
    func onEnterForeground()
    {
        
    }
    
    
    
    
    
}
