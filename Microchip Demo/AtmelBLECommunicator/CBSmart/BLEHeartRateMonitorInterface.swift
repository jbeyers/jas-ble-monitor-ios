
/* ***************************************************************************

HRM.swift



Copyright (c) 2015, Atmel Corporation. All rights reserved.
Released under NDA
Licensed under Atmel's Limited License Agreement.


THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Atmel Corporation: http://www.atmel.com

******************************************************************************/


protocol BLEHeartRateMonitorInterfaceDelegate:NSObjectProtocol{
    
    func bleHeartRateMonitorInterfaceDidReadMeasurementData(_ heartRateData:HeartRateData?)
    func bleHeartRateMonitorInterfaceDidReadBodySensorLocation(_ heartSensorLocation:HRMSensorLocation?)
    func bleHeartRateMonitorInterfaceUpdateNotificationStateForCharacteristic(_ characteristic: CBCharacteristic!, error: NSError!)
}

import Foundation
import CoreBluetooth
//0	Other
//1	Chest
//2	Wrist
//3	Finger
//4	Hand
//5	Ear Lobe
//6	Foot

enum HRMSensorLocation:String{
    
    case	Other   =   "Other"
    case	Chest   =   "Chest"
    case    Wrist   =   "Wrist"
    case    Finger  =   "Finger"
    case    Hand    =   "Hand"
    case    EarLobe =   "EarLobe"
    case    Foot    =   "Foot"
    
    static let position = [Other,Chest,Wrist,Finger,Hand,EarLobe,Foot]
}
enum HRMSensorLocationSupport{
    case supported
    case notSupported
}

enum HRMSensorLocationDetection{
    case detected
    case notDetected
}

struct HeartRateData{
    
    var heartRateMeasurementValue:Int?
    var enegyExpended:Int?
    var rrInterval:Int?
    var sensorLocationSupport:HRMSensorLocationSupport?
    var sensorLocationDetection:HRMSensorLocationDetection?

    init(data:Data){
        
        var byteArray8 = [UInt8](repeating: 0, count: data.count)
        
        (data as NSData).getBytes(&byteArray8, length: data.count)
        
        if byteArray8.count<1
        {return}
        
        //Check flag
        
        let flagTemp:UInt8 = byteArray8[0]
        
        let isHeartRateValue8bit:Bool = !(flagTemp & 0x01).toBool()
        
        if (flagTemp & 0x04).toBool() {
            
            sensorLocationSupport = HRMSensorLocationSupport.supported
            
            if (flagTemp & 0x02).toBool()
                {sensorLocationDetection = HRMSensorLocationDetection.detected}
            else
                {sensorLocationDetection = HRMSensorLocationDetection.notDetected}
            
        }
        else{
            
            sensorLocationSupport = HRMSensorLocationSupport.notSupported
            sensorLocationDetection = HRMSensorLocationDetection.notDetected
        }
        
        
        let isEnergyExpendedPresent:Bool = (flagTemp & 0x8).toBool()
        let isRRIntervalPresent:Bool = (flagTemp & 0x10).toBool()
        
        var bitIncrementer = 0
        if isHeartRateValue8bit
            {heartRateMeasurementValue = Int(byteArray8[1])}
            
        else{
            
            var hmValue:UInt16 = 0
            
            if data.count >= (1+2)
            {(data as NSData).getBytes(&hmValue, range: NSMakeRange(1, 2))}
            
            heartRateMeasurementValue = Int(hmValue)
            bitIncrementer += 1
            }
        
        if isEnergyExpendedPresent{
            var eeValue:UInt16 = 0
            
            if data.count >= (2+bitIncrementer+2)
            {(data as NSData).getBytes(&eeValue, range: NSMakeRange(2+bitIncrementer, 2))}
            
            enegyExpended = Int(eeValue)
            bitIncrementer += 1
        }
        
        if isRRIntervalPresent{
            var rrValue:UInt16 = 0
            
            if data.count >= (2+bitIncrementer+2)
            {(data as NSData).getBytes(&rrValue, range: NSMakeRange(2+bitIncrementer, 2))}
            
            rrInterval = Int(rrValue)
        }

    }
}

class BLEHeartRateMonitorInterface:NSObject{
    
    var delegate:BLEHeartRateMonitorInterfaceDelegate?
    var sourcePeripheral:BLESourceDevice?
    var heartRateService                        :CBService?
    var heartRateMeasurementCharacteristic      :CBCharacteristic?
    var bodySensorLocationCharacteristic        :CBCharacteristic?
    var heartRateControlPointCharacteristic     :CBCharacteristic?

    
    
    init(sourceDevice:BLESourceDevice ,delgateObject:BLEHeartRateMonitorInterfaceDelegate){
        
        super.init()
        self.delegate = delgateObject
        self.sourcePeripheral=sourceDevice
        self.sourcePeripheral!.operationsDelegate = self;
        let serviceArray = sourceDevice.CBSPeripheral.services
        
        heartRateService=serviceArray?.filter(){$0.uuid.isEqual(CBUUID(bleService: .HeartRateService))}.first
        
        heartRateMeasurementCharacteristic=(heartRateService?.characteristics)?.filter(){$0.uuid.isEqual(CBUUID(blecharacteristic:BLECharacteristic.HeartRateMeasurementCharacteristics))}.first
        
        bodySensorLocationCharacteristic=(heartRateService?.characteristics)?.filter(){$0.uuid.isEqual(CBUUID(blecharacteristic: BLECharacteristic.BodySensorLocationCharacteristics))}.first
        
        heartRateControlPointCharacteristic=(heartRateService?.characteristics)?.filter(){$0.uuid.isEqual(CBUUID(blecharacteristic: BLECharacteristic.HeartRateControlPointCharacteristics))}.first
      
        
        
    }
    
}

extension BLEHeartRateMonitorInterface{
    
    func startHeartRateMeasurmentNotifications()
    {
        if let _ = self.sourcePeripheral
        {
            self.sourcePeripheral?.setNotifyValue(true, forCharacteristic: heartRateMeasurementCharacteristic)
        }
    }
    
    func stopHeartRateMeasurmentNotifications()
    {self.sourcePeripheral?.setNotifyValue(false, forCharacteristic: heartRateMeasurementCharacteristic)}
    
    func readHeartRateBodySensorLocation()
    {
        if let _ = self.sourcePeripheral
        {
            self.sourcePeripheral?.readValueForCharacteristic(bodySensorLocationCharacteristic)
        }
    }
    
    func resetEnergyExpended(){
        
        var writeValue:Int8 = 1
        //let writeData:Data = Data(bytes: UnsafePointer<UInt8>(&writeValue), count: 1)
        let writeData:Data = Data(bytes: &writeValue, count: 1)
        self.sourcePeripheral?.writeValue(writeData, forCharacteristic: heartRateControlPointCharacteristic, withAcknowledgement: true)
        
    }
    
   }


extension BLEHeartRateMonitorInterface:BLESourceDeviceOperationsDelegate{
    
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice, didUpdateValueForCharacteristic characteristic: CBCharacteristic!, error: NSError!){
        
        if characteristic.isEqual(heartRateMeasurementCharacteristic){
            
            if let cData = heartRateMeasurementCharacteristic?.value
            {self.delegate?.bleHeartRateMonitorInterfaceDidReadMeasurementData(HeartRateData(data:cData))}
        }
        
        if characteristic.isEqual(bodySensorLocationCharacteristic){
            
            var bLoc:UInt8 = UInt8()
            
            if let bodySensorLocationData = bodySensorLocationCharacteristic?.value
            {(bodySensorLocationData as NSData).getBytes(&bLoc, length: 1)}
            
            print("body sensor location value received as \(bLoc) from data \(String(describing: bodySensorLocationCharacteristic?.value))")
            self.delegate?.bleHeartRateMonitorInterfaceDidReadBodySensorLocation(HRMSensorLocation.position[Int(bLoc)])
        }

        
    }
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice,didWriteValueForCharacteristic characteristic: CBCharacteristic!, error: NSError!){
        
    }
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice,didUpdateNotificationStateForCharacteristic characteristic: CBCharacteristic!, error: NSError!){
        
        self.delegate?.bleHeartRateMonitorInterfaceUpdateNotificationStateForCharacteristic(characteristic, error: error)
    }
}
