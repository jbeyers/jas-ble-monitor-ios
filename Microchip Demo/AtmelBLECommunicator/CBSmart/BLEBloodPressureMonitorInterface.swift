
/* ***************************************************************************

FindMeInterface.swift



Copyright (c) 2015, Atmel Corporation. All rights reserved.
Released under NDA
Licensed under Atmel's Limited License Agreement.


THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Atmel Corporation: http://www.atmel.com

******************************************************************************/


protocol BLEBloodPressureMonitorInterfaceDelegate:NSObjectProtocol{
    
    func bleBloodPressureMonitorInterfaceDidReadBPMData(_ bloodPressureData:BloodPressureData?)
    func bleBloodPressureMonitorInterfaceDidReadICMData(_ bloodPressureData:IntermediateCuffPressureData?)
    func bleBloodPressureMonitorInterfaceDidReadBPMFeatureData(_ bpmFeatureStatus:BPMFeatureStatus?)

    func bleBloodPressureMonitorInterfaceUpdateNotificationStateForCharacteristic(_ characteristic: CBCharacteristic!, error: NSError!)
}

import Foundation
import CoreBluetooth


enum BPMSensorLocation:String{
    
    case	Other   =   "Other"
    case	Chest   =   "Chest"
    case    Wrist   =   "Wrist"
    case    Finger  =   "Finger"
    case    Hand    =   "Hand"
    case    EarLobe =   "EarLobe"
    case    Foot    =   "Foot"
    
    static let position = [Other,Chest,Wrist,Finger,Hand,EarLobe,Foot]
}
enum BPMSensorLocationSupport{
    case supported
    case notSupported
}

enum BPMSensorLocationDetection{
    case detected
    case notDetected
}


enum PulseRateInRange{
    case inRange
    case aboveUpperLimit
    case belowLowerLimit
}
enum BPMUnit:Int{
    case mmHg = 0
    case kPa = 1
}
struct BPMStatus{
    var isBodyMoved:Bool?
    var isCuffFitProper:Bool?
    var isIrregularPulse:Bool?
    var pulseRateInRange:PulseRateInRange?
    var isMeasurementPositionProper:Bool?

    init(data:Data){
        var flagStatus:UInt16 = 0


        (data as NSData).getBytes(&flagStatus, length: data.count)
        isBodyMoved  = (flagStatus & 0x0001).toBool()
        isCuffFitProper  = (flagStatus & 0x0002).toBool()
        isIrregularPulse  = (flagStatus & 0x0004).toBool()
        
        switch(flagStatus & 0x0018){
            
        case 0:pulseRateInRange = PulseRateInRange.inRange
            break
        case 1:pulseRateInRange = PulseRateInRange.aboveUpperLimit
            break
        case 2:pulseRateInRange = PulseRateInRange.belowLowerLimit
            break
        case 3:
            break
        default:
            break
        }
        
        isMeasurementPositionProper  = (flagStatus & 0x0020).toBool()

    }
}

enum BPMFeatureStatusEnum
{
    case isBodyMovementDetection
    case isCuffFitDetection
    case isIrregularPulseDetection
    case isPulseRateDetection
    case isMeasurementPositionDetection
    case isMultipleBondSupport
}
struct BPMFeatureStatus{
    var isBodyMovementDetection:Bool?
    var isCuffFitDetection:Bool?
    var isIrregularPulseDetection:Bool?
    var isPulseRateDetection:Bool?
    var isMeasurementPositionDetection:Bool?
    var isMultipleBondSupport:Bool?

    init(data:Data){
        var flagStatus:UInt16 = 0
        
        (data as NSData).getBytes(&flagStatus, length: data.count)
        
        isBodyMovementDetection  = (flagStatus & 0x0001).toBool()
        isCuffFitDetection  = (flagStatus & 0x0002).toBool()
        isIrregularPulseDetection  = (flagStatus & 0x0004).toBool()
        isPulseRateDetection  = (flagStatus & 0x0008).toBool()
        isMeasurementPositionDetection  = (flagStatus & 0x0010).toBool()
        isMultipleBondSupport  = (flagStatus & 0x0020).toBool()

    }
}

struct BloodPressureData{
    
    var systolicBPM:Float?
    var diastolicBPM:Float?
    var meanArterialBPM:Float?
    
    var bpmUnit:BPMUnit?
    
    var rrInterval:Int?
    var timeStamp:BLETime?
    var pulseRate:Float?
    var userId:Int?
    var measurementStatus:BPMStatus?
    
    init(data:Data){
        
        var byteArray8 = [UInt8](repeating: 0, count: data.count)
        
        (data as NSData).getBytes(&byteArray8, length: data.count)
        
        //Check flag
        
        let flagTemp:UInt8 = byteArray8[0]
        bpmUnit = BPMUnit(rawValue:Int(flagTemp & 0x01))!
        
        //systolicBPM = data.subdata(in: NSMakeRange(1, 2)).sFloatFromData()
        systolicBPM = data.subdata(in: 1..<2).sFloatFromData()
        //diastolicBPM = data.subdata(in: NSMakeRange(3, 2)).sFloatFromData()
        diastolicBPM = data.subdata(in: 3..<2).sFloatFromData()
//        meanArterialBPM = data.subdataWithRange(NSMakeRange(5, 2)).sFloatFromData()                           Not Me Already Commented
        
        let isTimeStamp:Bool = (flagTemp & 0x02).toBool()
        let isPulseRate:Bool =  (flagTemp & 0x04).toBool()
        let isUserId:Bool =  (flagTemp & 0x08).toBool()
        let isMeasurementStatus:Bool =  (flagTemp & 0x10).toBool()
        
        var bitIncrementar = 7
        
        if isTimeStamp{
            //timeStamp = BLETime(data:data.subdata(in: NSMakeRange(bitIncrementar, 7)))
            timeStamp = BLETime(data:data.subdata(in: bitIncrementar..<7))
            bitIncrementar+=7
        }
        if isPulseRate{
            //pulseRate = data.subdata(in: NSMakeRange(bitIncrementar, 2)).sFloatFromData()
            pulseRate = data.subdata(in: bitIncrementar..<2).sFloatFromData()
            bitIncrementar+=2
        }
        if isUserId{
            userId = Int(byteArray8[bitIncrementar])
            bitIncrementar += 1
        }
        if isMeasurementStatus{
            //measurementStatus = BPMStatus (data: data.subdata(in: NSMakeRange(bitIncrementar, 2)))
            measurementStatus = BPMStatus (data: data.subdata(in: bitIncrementar..<2))
        }
        
    }
}
struct IntermediateCuffPressureData{
    
    var systolicICM:Float?
    var diastolicICM:Float?
    var meanArterialICM:Float?
    
    var icmUnit:BPMUnit?
    
    var rrInterval:Int?
    var timeStamp:BLETime?
    var pulseRate:Float?
    var userId:Int?
    var measurementStatus:BPMStatus?
    
    init(data:Data){
        
        
        var byteArray8 = [UInt8](repeating: 0, count: data.count)
        
        (data as NSData).getBytes(&byteArray8, length: data.count)
        
        //Check flag
        
        let flagTemp:UInt8 = byteArray8[0]
        icmUnit = BPMUnit(rawValue:Int(flagTemp & 0x01))!
        //systolicICM = data.subdata(in: NSMakeRange(1, 2)).sFloatFromData()
        systolicICM = data.subdata(in: 1..<2).sFloatFromData()
        //diastolicICM = data.subdata(in: NSMakeRange(3, 2)).sFloatFromData()
        diastolicICM = data.subdata(in: 3..<2).sFloatFromData()
//        meanArterialICM = data.subdataWithRange(NSMakeRange(5, 2)).sFloatFromData()                           Not Me Already Commented

        let isTimeStamp:Bool = (flagTemp & 0x02).toBool()
        let isPulseRate:Bool =  (flagTemp & 0x04).toBool()
        let isUserId:Bool =  (flagTemp & 0x08).toBool()
        let isMeasurementStatus:Bool =  (flagTemp & 0x10).toBool()
        
        var bitIncrementar = 6
        
        if isTimeStamp{
            //timeStamp = BLETime(data:data.subdata(in: NSMakeRange(bitIncrementar, 7)))
            timeStamp = BLETime(data:data.subdata(in: bitIncrementar..<7))
            bitIncrementar+=7
        }
        if isPulseRate{
            //pulseRate = data.subdata(in: NSMakeRange(bitIncrementar, 2)).sFloatFromData()
            pulseRate = data.subdata(in: bitIncrementar..<2).sFloatFromData()
            bitIncrementar+=2
        }
        if isUserId{
            userId = Int(byteArray8[bitIncrementar])
            bitIncrementar += 1
        }
        if isMeasurementStatus{
            //measurementStatus = BPMStatus (data: data.subdata(in: NSMakeRange(bitIncrementar, 2)))
            measurementStatus = BPMStatus (data: data.subdata(in: bitIncrementar..<2))
        }

    }
}

class BLEBloodPressureMonitorInterface:NSObject{
    
    var delegate:BLEBloodPressureMonitorInterfaceDelegate?
    var sourcePeripheral:BLESourceDevice?
    var bloodPressureService                        :CBService?
    var bloodPressureMeasurementCharacteristic      :CBCharacteristic?
    var intermediateCuffPressureCharacteristics     :CBCharacteristic?
    var bloodPressureFeatureCharacteristics         :CBCharacteristic?
    
    
    
    init(sourceDevice:BLESourceDevice ,delgateObject:BLEBloodPressureMonitorInterfaceDelegate){
        
        super.init()
        self.delegate = delgateObject
        self.sourcePeripheral=sourceDevice
        self.sourcePeripheral!.operationsDelegate = self;
        let serviceArray = sourceDevice.CBSPeripheral.services
        
        bloodPressureService=serviceArray?.filter(){$0.uuid.isEqual(CBUUID(bleService: .BloodPressureService))}.first
        
        bloodPressureMeasurementCharacteristic=(bloodPressureService?.characteristics)?.filter(){$0.uuid.isEqual(CBUUID(blecharacteristic:BLECharacteristic.BloodPressureMeasurementCharacteristics))}.first
        
        intermediateCuffPressureCharacteristics=(bloodPressureService?.characteristics)?.filter(){$0.uuid.isEqual(CBUUID(blecharacteristic: BLECharacteristic.IntermediateCuffPressureCharacteristics))}.first
        
        bloodPressureFeatureCharacteristics=(bloodPressureService?.characteristics)?.filter(){$0.uuid.isEqual(CBUUID(blecharacteristic: BLECharacteristic.BloodPressureFeatureCharacteristics))}.first
        
        
        
    }
    
}

extension BLEBloodPressureMonitorInterface{
    
    func startBloodPressureMeasurmentNotifications()
    {self.sourcePeripheral?.setNotifyValue(true, forCharacteristic: bloodPressureMeasurementCharacteristic)}
    
    func stopBloodPressureMeasurmentNotifications()
    {self.sourcePeripheral?.setNotifyValue(false, forCharacteristic: bloodPressureMeasurementCharacteristic)}
    
    func startIntermediateCuffPressureNotifications()
    {self.sourcePeripheral?.setNotifyValue(true, forCharacteristic: intermediateCuffPressureCharacteristics)}
    
    func stopIntermediateCuffPressureNotifications()
    {self.sourcePeripheral?.setNotifyValue(false, forCharacteristic: intermediateCuffPressureCharacteristics)}
    func readBloodPressureFeatures()
    {self.sourcePeripheral?.readValueForCharacteristic(bloodPressureFeatureCharacteristics)}
    
   
    
}


extension BLEBloodPressureMonitorInterface:BLESourceDeviceOperationsDelegate{
    
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice, didUpdateValueForCharacteristic characteristic: CBCharacteristic!, error: NSError!){
        
        if characteristic.isEqual(bloodPressureMeasurementCharacteristic){
            
            if let cData = bloodPressureMeasurementCharacteristic?.value
            {
                    self.delegate?.bleBloodPressureMonitorInterfaceDidReadBPMData(BloodPressureData(data:cData))
            }
        }
        if characteristic.isEqual(intermediateCuffPressureCharacteristics){
            
            if let cData = intermediateCuffPressureCharacteristics?.value
            {
                self.delegate?.bleBloodPressureMonitorInterfaceDidReadICMData(IntermediateCuffPressureData(data:cData))
            }
            
        }
        if characteristic.isEqual(bloodPressureFeatureCharacteristics){
            
            if let cData = bloodPressureFeatureCharacteristics?.value
            {self.delegate?.bleBloodPressureMonitorInterfaceDidReadBPMFeatureData(BPMFeatureStatus(data:cData))}
        }
        
        
        
    }
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice,didWriteValueForCharacteristic characteristic: CBCharacteristic!, error: NSError!){
        
    }
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice,didUpdateNotificationStateForCharacteristic characteristic: CBCharacteristic!, error: NSError!){
        
        self.delegate?.bleBloodPressureMonitorInterfaceUpdateNotificationStateForCharacteristic(characteristic, error: error)
    }
}
