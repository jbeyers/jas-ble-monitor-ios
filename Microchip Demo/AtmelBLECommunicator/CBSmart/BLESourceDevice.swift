/* ***************************************************************************

BLESourceDevice.swift
//
Interface class for implementing actions and handling responses of Bluetooth Peripheral
//


Copyright (c) 2015, Atmel Corporation. All rights reserved.
Released under NDA
Licensed under Atmel's Limited License Agreement.


THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Atmel Corporation: http://www.atmel.com

******************************************************************************/


import Foundation
import CoreBluetooth


let prints = false

protocol BLESourceDeviceDelegate:NSObjectProtocol{
    
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice, didDiscoverServicesWithError error:NSError!)
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice, didDiscoverCharacteristicsForService service:CBService, withError error:NSError!)
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice, didDiscoverAllServicesAndCharacteristicsWithError error:NSError!)
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice, DidInitializedOTAService isOTAPresent: Bool)
}

protocol BLESourceDeviceOperationsDelegate{
    
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice, didUpdateValueForCharacteristic characteristic: CBCharacteristic!, error: NSError!)
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice,didWriteValueForCharacteristic characteristic: CBCharacteristic!, error: NSError!)
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice,didUpdateNotificationStateForCharacteristic characteristic: CBCharacteristic!, error: NSError!)
}

protocol BLESourceDeviceOTAOperationsDelegate{
    
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice, didUpdateValueForCharacteristic characteristic: CBCharacteristic!, error: NSError!)
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice,didWriteValueForCharacteristic characteristic: CBCharacteristic!, error: NSError!)
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice,didUpdateNotificationStateForCharacteristic characteristic: CBCharacteristic!, error: NSError!)
}


protocol BLESourceDeviceDescriptorOpertationsDelegate {
    
    func bleSourceDevice(_ bleSorceDevice:BLESourceDevice, didDiscoverDescriptorsForCharacteristic characteristic: CBCharacteristic, error: NSError?)
    func bleSourceDevice(_ bleSorceDevice:BLESourceDevice, didUpdateValueForDescriptor descriptor: CBDescriptor, error: NSError?)
    func bleSourceDevice(_ bleSorceDevice:BLESourceDevice, didWriteValueForDescriptor descriptor: CBDescriptor, error: NSError?)
}





protocol BLESourceDeviceRSSIDelegate{
    
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice, didReadRSSI RSSI: NSNumber!, error: NSError!)
    
}

class BLESourceDevice: NSObject
{
    var CBSPeripheral           : CBPeripheral!
    var activeProfiles          :Array<BLEProfile>!
    var delegate                :BLESourceDeviceDelegate?
    var operationsDelegate      :BLESourceDeviceOperationsDelegate?
    var otaDelegate             :BLESourceDeviceOTAOperationsDelegate?
    var descriptorDelegate      :BLESourceDeviceDescriptorOpertationsDelegate?
    var rssiDelegate            :BLESourceDeviceRSSIDelegate!
    var isServiceAndCharScan    :Bool = false
    var servicesArray           :Array <CBService>?
    
    
    init(peripheral: CBPeripheral)
    {
        super.init()
        
        self.activeProfiles = nil
        self.CBSPeripheral = peripheral
        self.CBSPeripheral.delegate = self
    }
    
    init(peripheral: CBPeripheral, withDelegate delegate:BLESourceDeviceDelegate!)
    {
        super.init()
        self.delegate=delegate
        self.CBSPeripheral = peripheral
        self.CBSPeripheral.delegate = self
    }

}


//MARK: BLESourcedeviceMethods

extension BLESourceDevice{
    
    func getServices(){
        
        self.isServiceAndCharScan=false
        
        if activeProfiles == nil
        {CBSPeripheral.discoverServices(nil)}
        else
        {CBSPeripheral.discoverServices(CBUUID.getCBUUIDArrayFrom(bleProfilesArray:activeProfiles))}
        
    }
    
    func getAllServicesAndCharactesristics(){
        
        servicesArray=Array()
        self.isServiceAndCharScan = true
        
        if prints
        {NSLog("BLE Sourcedevice = \(CBSPeripheral) Getting all services and Characteristics")}
        
        if activeProfiles == nil
        {CBSPeripheral?.discoverServices(nil)}
        else
        {CBSPeripheral?.discoverServices(CBUUID.getCBUUIDArrayFrom(bleProfilesArray:activeProfiles))}
    }
    
    func getCharacteristicsForService(_ service:CBService)
    {CBSPeripheral.discoverCharacteristics(nil, for: service)}
    
    func readValueForCharacteristic(_ characteristic:CBCharacteristic?){
        
        if characteristic != nil {
            if prints
                
            {print("reading value for Characteristics with uuid = \(characteristic!.uuid.uuidString) from BLESourceDevice")}
            CBSPeripheral.readValue(for: characteristic!)
        }
    }
    
    func writeValue(_ value:Data!,forCharacteristic characteristic:CBCharacteristic?, withAcknowledgement ack:Bool){
        
        if characteristic != nil{
            if prints
                
            {print(" going to write value for characteristics \(characteristic!.uuid.uuidString) with value \(value) in bleSourceDevice class and isaack= \(ack)")}
            
            if ack==true
            {CBSPeripheral.writeValue(value, for: characteristic!, type: .withResponse)}
            else
            {CBSPeripheral.writeValue(value, for: characteristic!, type: .withoutResponse)}
        }
    }
    
    func setNotifyValue(_ enabled: Bool, forCharacteristic characteristic: CBCharacteristic?){
        
        if characteristic != nil{
            if prints
                
            {print(" going to set Notfications value for characteristics \(characteristic!.uuid.uuidString) with value \(enabled) in bleSourceDevice class ")}
            CBSPeripheral.setNotifyValue(enabled, for: characteristic!)
        }
    }
    
    func getDescriptorsForCharacteristic(_ characteristic : CBCharacteristic?){
        
        if characteristic != nil {
            
            CBSPeripheral.discoverDescriptors(for: characteristic!)
        }
    }
    
    
    func readValueForDescriptor(_ descriptor : CBDescriptor?){
        
        if descriptor != nil {
            
            CBSPeripheral.readValue(for: descriptor!)
        }
    }
    
    
}


extension BLESourceDevice:CBPeripheralDelegate{
    
    func peripheralDidUpdateName(_ peripheral: CBPeripheral){
        
    }
    
    func peripheral(_ peripheral: CBPeripheral, didModifyServices invalidatedServices: [CBService]){
        
    }
    
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?){
        
        
        print("discovered services \(peripheral.services?.count) for peripheral = \(peripheral) and error \(String(describing: error)) if is service and Character = \(isServiceAndCharScan)")
        
        if peripheral.services != nil {
            
            if activeProfiles == nil {
                activeProfiles = peripheral.services?.findAssociatedBLEProfiles()
                activeProfiles.removeDependentProfiles()
            }
            
            if !isServiceAndCharScan
            {self.delegate?.bleSourceDevice(self, didDiscoverServicesWithError: error as! NSError)}
                
            else{
                
                print("Discovering characteristic for service")
                for service in peripheral.services!
                {peripheral.discoverCharacteristics(nil, for: service)}
                
                
                
            }
        }
        else{
            GlobalCommunicator.shared.communicator.centralManager(GlobalCommunicator.shared.communicator.CBSManager, didFailToConnect: peripheral, error: NSError(domain: "Fail", code: 10, userInfo: [NSLocalizedDescriptionKey:"Service discovery failure"]))
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverIncludedServicesFor service: CBService, error: Error?){
        
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?){
        
        print("discovered characteristic  \(service.characteristics) for service = \(service) for peipheral \(peripheral)")

        if !isServiceAndCharScan
            {self.delegate?.bleSourceDevice(self, didDiscoverCharacteristicsForService:service, withError: error as! NSError)}
        else{
            
            if error == nil{
                
                servicesArray?.append(service)
                if servicesArray?.count == peripheral.services?.count {
                    
                    print("All characteristics of \(servicesArray?.count) services discovered")
                    
                    if AtmelOTAInterface.hasOTA(self.CBSPeripheral){
                        
                        GlobalCommunicator.shared.otaInterface = AtmelOTAInterface(sourceDevice: self, delgateObject: nil)
                        self.otaDelegate = GlobalCommunicator.shared.otaInterface
//                        self.delegate?.bleSourceDeviceDidInitializedOTAService(self)
                        self.delegate?.bleSourceDevice(self, DidInitializedOTAService: true)
                    }
                    else{
                        self.delegate?.bleSourceDevice(self, DidInitializedOTAService: false)
                    }
                    
                    self.delegate?.bleSourceDevice(self, didDiscoverAllServicesAndCharacteristicsWithError: error as? NSError)
                    
                    //Trigger Notification of all service discovered
                    
                    NotificationCenter.trigger(Constants.NotificationName.onServiceDiscovered)
                    servicesArray = nil
                }
            }
                
            else
                {self.delegate?.bleSourceDevice(self, didDiscoverAllServicesAndCharacteristicsWithError: error as? NSError)}
        }

    }
    
   
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?){
        if prints
        {print("received updated Value for characteristic with uuid \(characteristic.uuid.uuidString) with value \(characteristic.value) in BLESourceDevice")}
        
        if characteristic.value != nil
        {
            self.operationsDelegate?.bleSourceDevice(self, didUpdateValueForCharacteristic: characteristic, error: error as? NSError)
            self.otaDelegate?.bleSourceDevice(self, didUpdateValueForCharacteristic: characteristic, error: error as? NSError )

        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?){
        if prints
        { print(" written value for characteristics \(characteristic.uuid.uuidString) with value \(characteristic.value) in bleSourceDevice class")}
        
        
        self.operationsDelegate?.bleSourceDevice(self, didWriteValueForCharacteristic: characteristic,error: error as? NSError)
        self.otaDelegate?.bleSourceDevice(self, didWriteValueForCharacteristic: characteristic,error: error as? NSError)
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateNotificationStateFor characteristic: CBCharacteristic, error: Error?){
       
        //printl- Check error code for encryption issue
        if prints
        {print(" didUpdateNotificationStateForCharacteristic\(characteristic.uuid.uuidString)  in bleSourceDevice class")}
       
            self.operationsDelegate?.bleSourceDevice(self, didUpdateNotificationStateForCharacteristic: characteristic,error: error as? NSError)
            self.otaDelegate?.bleSourceDevice(self, didUpdateNotificationStateForCharacteristic: characteristic,error: error as? NSError)
            
        
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverDescriptorsFor characteristic: CBCharacteristic, error: Error?){
        
        if descriptorDelegate != nil {
            descriptorDelegate?.bleSourceDevice(self, didDiscoverDescriptorsForCharacteristic: characteristic, error: error as? NSError)
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor descriptor: CBDescriptor, error: Error?){
     
        if descriptorDelegate != nil {
            descriptorDelegate?.bleSourceDevice(self, didUpdateValueForDescriptor: descriptor, error: error as? NSError)
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor descriptor: CBDescriptor, error: Error?){
        
        if descriptorDelegate != nil {
            descriptorDelegate?.bleSourceDevice(self, didWriteValueForDescriptor: descriptor, error: error as? NSError)
        }
    }
    
    
    func peripheral(_ peripheral: CBPeripheral, didReadRSSI RSSI: NSNumber, error: Error?)
        {self.rssiDelegate?.bleSourceDevice(self, didReadRSSI: RSSI, error: error as? NSError)}

}


extension BLESourceDevice
{
    func isDisconnectionDueToInsufficientEncryption(_ error: NSError?) -> Bool
    {
        if error?.code == CBATTError.Code.insufficientEncryption.rawValue
        {
            return true
        }
        return false

    }
}











































