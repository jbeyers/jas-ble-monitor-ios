/***************************************************************************
 
 AtmelEddystoneInterface.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import UIKit
import CoreBluetooth
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}



enum LockStatus{
    case locked
    case unLocked
}

enum BeaconErrorCode : Int{
    
    case beaconWriteSuccess                 = 0x00
    case beaconWriteNotPermitted            = 0x03
    case beaconInsufficientAuthorization    = 0x08
    case beaconInvalidAttributeLength       = 0x0d
}


enum TxPowerModes : UInt8{
    case txPowerModeLowest
    case txPowerModeLow
    case txPowerModeMedium
    case txPowerModeHigh
}


enum URLSchemePrefix : String{
    
    case URLScheme0     = "http://www."
    case URLScheme1     = "https://www."
    case URLScheme2     = "http://"
    case URLScheme3     = "https://"
    
    var prefixHexValue : UInt8{
        switch self{
        case .URLScheme0 :
            return 0x00
        case .URLScheme1 :
            return 0x01
        case .URLScheme2 :
            return 0x02
        case .URLScheme3 :
            return 0x03
        }
    }
}


enum URLSchemePostfix : String{
    
    case Scheme0Flag  = ".com/"
    case Scheme1Flag  = ".org/"
    case Scheme2Flag  = ".edu/"
    case Scheme3Flag  = ".net/"
    case Scheme4Flag  = ".info/"
    case Scheme5Flag  = ".biz/"
    case Scheme6Flag  = ".gov/"
    case Scheme7Flag  = ".com"
    case Scheme8Flag  = ".org"
    case Scheme9Flag  = ".edu"
    case Scheme10Flag = ".net"
    case Scheme11Flag = ".info"
    case Scheme12Flag = ".biz"
    case Scheme13Flag = ".gov"
    
    var postfixHexValue : UInt8{
        
        switch(self){
            
        case .Scheme0Flag :
            return 0x00
        case .Scheme1Flag :
            return 0x01
            
        case .Scheme2Flag :
            return 0x02
            
        case .Scheme3Flag :
            return 0x03
            
        case .Scheme4Flag :
            return 0x04
            
        case .Scheme5Flag :
            return  0x05
            
        case .Scheme6Flag :
            return 0x06
            
        case .Scheme7Flag :
            return 0x07
            
        case .Scheme8Flag :
            return 0x08
            
        case .Scheme9Flag :
            return 0x09
            
        case .Scheme10Flag :
            return 0x0a
            
        case .Scheme11Flag :
            return 0x0b
            
        case .Scheme12Flag :
            return 0x0c
            
        case .Scheme13Flag :
            return 0x0d
            
        }
    }
    
}


struct EddystoneDetails {
    
    var urlString               : String?
    var urlFlag                 : UInt8?
    var txPowerLevelsArray      : [Int8]?
    var txPowerMode             : String?
    var beaconPeriod            : UInt16?
    
    var beaconCharArray         : [CBCharacteristic?]?
    
    init(characteristics : [CBCharacteristic?]){
        
        if characteristics.count >= 1{
            beaconCharArray = characteristics
        }
    }
}

struct URIDataFrame {
    
    var urlString : String?
    init(data:Data)
    {
        
        var urlPrefix:String?
        var urlPostfix:String?
        var urlMiddleString:String?
        
        
        if  data.count >= 1
        {
            //let urlflag:EddystoneURLPrefixFlag? = EddystoneURLPrefixFlag(rawValue:((data.subdata(in: NSMakeRange(0, 1))) as NSData).bytes.bindMemory(to: UInt8.self, capacity: (data.subdata(with: NSMakeRange(0, 1))).count).pointee)
            let urlflag:EddystoneURLPrefixFlag? = EddystoneURLPrefixFlag(rawValue:((data.subdata(in: 0..<1)) as NSData).bytes.bindMemory(to: UInt8.self, capacity: (data.subdata(in: 0..<1)).count).pointee)
            if urlflag != nil
            {urlPrefix = urlflag?.schemeURL}
            
        }
        
        if data.count>=3{            
            
            //let urlflag:EddystoneURLPostfixFlag? = EddystoneURLPostfixFlag(rawValue:((data.subdata(in: NSMakeRange((data.count-1), 1))) as NSData).bytes.bindMemory(to: UInt8.self, capacity: (data.subdata(with: NSMakeRange((data.count-1), 1))).count).pointee)
            let urlflag:EddystoneURLPostfixFlag? = EddystoneURLPostfixFlag(rawValue:((data.subdata(in: (data.count-1)..<data.count)) as NSData).bytes.bindMemory(to: UInt8.self, capacity: (data.subdata(in: (data.count-1)..<data.count)).count).pointee)
            
            if urlflag != nil
            {urlPostfix = urlflag?.schemeURL}
            
            let urlEncodedData : Data?
            
            if urlflag != nil{
                //urlEncodedData = data.subdata(in: NSMakeRange(1, (data.count-2)))
                urlEncodedData = data.subdata(in: 1..<(data.count-2))
            }
            else{
                //urlEncodedData = data.subdata(in: NSMakeRange(1, (data.count-1)))
                urlEncodedData = data.subdata(in: 1..<(data.count-1))
            }
            
            if let urlString = urlEncodedData!.USASCIICoverstion()
            {urlMiddleString = urlString}
        }
        
        
        var URLString = ""
        
        if urlPrefix != nil{
            URLString += urlPrefix!
        }
        
        if urlMiddleString != nil{
            URLString += urlMiddleString!
        }
        
        if urlPostfix != nil{
            URLString += urlPostfix!
        }
        
        self.urlString = URLString
    }
}




protocol AtmelEddyStoneDelegate{
    
    func configurationServiceDidReadLockStatus(_ status : LockStatus)
    func configurationServiceDidReadValueForCharacteristic(_ characteristic : CBCharacteristic,beaconInfo : EddystoneDetails?)
    func configurationServiceDidWriteValueForCharacteristic(_ characteristic : CBCharacteristic, withError errorString: String?)
}


class AtmelEddystoneInterface: NSObject {
    
    var sourcePeripheral            : BLESourceDevice?
    var configurationService        : CBService?
    var delegate                    : AtmelEddyStoneDelegate?

    // Required characteristics
    
    var lockStateCharacteristic     : CBCharacteristic?
    var lockCharacteristic          : CBCharacteristic?
    var unlockCharacteristic        : CBCharacteristic?
    var URIDataCharacteristic       : CBCharacteristic?
    var URLFlagCharacteristic       : CBCharacteristic?
    var TXPowerLevelCharacteristic  : CBCharacteristic?
    var TXPowerModeCharacteristic   : CBCharacteristic?
    var beaconPeriodCharcteristic   : CBCharacteristic?
    var beaconResetCharacteristic   : CBCharacteristic?
    
    var beaconLockStatus            : LockStatus?
    var beaconDetails               : EddystoneDetails?
    
    init(sourceDevice:BLESourceDevice){
        
        super.init()
        self.sourcePeripheral   =   sourceDevice
        self.sourcePeripheral!.operationsDelegate = self;
        let serviceArray = sourceDevice.CBSPeripheral.services
        
        
        configurationService = serviceArray?.filter(){$0.uuid.isEqual(CBUUID(bleService: .AtmelEddyStoneService))}.first
        
        
        lockStateCharacteristic = (configurationService?.characteristics )?.filter(){$0.uuid.isEqual(CBUUID(blecharacteristic:BLECharacteristic.EddyStoneLockStateCharacteristics))}.first
        lockCharacteristic = (configurationService?.characteristics )?.filter(){$0.uuid.isEqual(CBUUID(blecharacteristic: .EddyStoneLockCharacteristics))}.first
        unlockCharacteristic = (configurationService?.characteristics )?.filter(){$0.uuid.isEqual(CBUUID(blecharacteristic: .EddyStoneUnLockCharacteristics))}.first
        URIDataCharacteristic = (configurationService?.characteristics )?.filter(){$0.uuid.isEqual(CBUUID(blecharacteristic: .EddyStoneURICharacteristics))}.first
        URLFlagCharacteristic = (configurationService?.characteristics)?.filter(){$0.uuid.isEqual(CBUUID(blecharacteristic: .EddyStoneFlagsCharacteristics))}.first
        TXPowerLevelCharacteristic = (configurationService?.characteristics)?.filter(){$0.uuid.isEqual(CBUUID(blecharacteristic: .EddyStoneTxPowerLevelCharacteristics))}.first
        TXPowerModeCharacteristic = (configurationService?.characteristics)?.filter(){$0.uuid.isEqual(CBUUID(blecharacteristic: .EddyStoneTxModeCharacteristics))}.first
        beaconPeriodCharcteristic = (configurationService?.characteristics)?.filter(){$0.uuid.isEqual(CBUUID(blecharacteristic: .EddyStoneBeaconPeriodCharacteristics))}.first
        beaconResetCharacteristic = (configurationService?.characteristics)?.filter(){$0.uuid.isEqual(CBUUID(blecharacteristic: .EddyStoneResetCharacteristics))}.first
        
        beaconDetails = EddystoneDetails(characteristics: [URIDataCharacteristic, URLFlagCharacteristic, TXPowerLevelCharacteristic, TXPowerModeCharacteristic, beaconPeriodCharcteristic])
    }
}


// MARK:- Eddystone Operations

extension AtmelEddystoneInterface{
    
    
    func readBeaconLockState(){
        self.sourcePeripheral?.readValueForCharacteristic(lockStateCharacteristic)
    }
    
    
    func readValueForAllCharacteristics(){
    
        if beaconDetails?.beaconCharArray?.count >= 1{
            
            for characteristic in beaconDetails!.beaconCharArray!{
                self.sourcePeripheral?.readValueForCharacteristic(characteristic)
            }
        }
    }
    
    
    func readValueForCharacteristic(_ characertistic : CBCharacteristic){
        self.sourcePeripheral?.readValueForCharacteristic(characertistic)
    }
    
    
    func lockBeaconWithPassword(_ password : UInt){

        var lockPassword = password
        if self.beaconLockStatus == .unLocked{
            
            var zeroValue  = 0
            //let lockData = NSData(data: Data(bytes: UnsafePointer<UInt8>(&zeroValue), count: 8)) as Data
            var lockData = NSData(data: Data(bytes: &zeroValue, count: 8)) as Data
            // Need 128 bit data for locking
            //lockData.append(Data(bytes: UnsafePointer<UInt8>(&lockPassword), count: 8))
            lockData.append(Data(bytes: &lockPassword, count: 8))
            self.sourcePeripheral?.writeValue(lockData, forCharacteristic: lockCharacteristic, withAcknowledgement: true)
        }
    }
    
    func unlockBeaconWithPassword(_ password : UInt){
        
        var unlockPassword  = password
        if self.beaconLockStatus == .locked {
            var zeroValue  = 0
            //let unlockData = NSData(data: Data(bytes: UnsafePointer<UInt8>(&zeroValue), count: 8)) as Data
            var unlockData = NSData(data: Data(bytes: &zeroValue, count: 8)) as Data
            // Need 128 bit data for unlocking
            //unlockData.append(Data(bytes: UnsafePointer<UInt8>(&unlockPassword), count: 8))
            unlockData.append(Data(bytes: &unlockPassword, count: 8))
            self.sourcePeripheral?.writeValue(unlockData, forCharacteristic: unlockCharacteristic, withAcknowledgement: true)
        }
    }
    
    
    func resetBeacon(){
        var resetValue : UInt8 = 10  // Any non-zero value is written for resetting Eddystone beacon
        //let resetData = Data(bytes: UnsafePointer<UInt8>(&resetValue), count: sizeof(UInt8))
        let resetData = Data(bytes: &resetValue, count: MemoryLayout<UInt8>.size)
        self.sourcePeripheral?.writeValue(resetData, forCharacteristic: beaconResetCharacteristic, withAcknowledgement: true)
    }
    
    
    func setBeaconPeriodToValue(_ value : UInt16){
        var period = value
        //let periodData = Data(bytes: UnsafePointer<UInt8>(&period), count: sizeof(UInt16))
        let periodData = Data(bytes: &period, count: MemoryLayout<UInt16>.size)
        self.sourcePeripheral?.writeValue(periodData, forCharacteristic: beaconPeriodCharcteristic, withAcknowledgement: true)
    }
    
    
    func setBeaconPowerModeToMode (_ powerMode : TxPowerModes){
        var value = powerMode.rawValue
        //let txModeData = Data(bytes: UnsafePointer<UInt8>(&value), count: sizeof(UInt8))
        let txModeData = Data(bytes: &value, count: MemoryLayout<UInt8>.size)
        self.sourcePeripheral?.writeValue(txModeData, forCharacteristic: TXPowerModeCharacteristic, withAcknowledgement: true)
    }
    
    func setAdvertisedPowerLevelValues(_ values :[Int8]){
        var powerLevels = values
        //let powerLevelData = Data(bytes: UnsafePointer<UInt8>(&powerLevels), count: powerLevels.count * sizeof(Int8))
        let powerLevelData = Data(bytes: &powerLevels, count: powerLevels.count * MemoryLayout<Int8>.size)
        self.sourcePeripheral?.writeValue(powerLevelData, forCharacteristic: TXPowerLevelCharacteristic, withAcknowledgement: true)
    }
    
    
    func setFlagsToValue(_ flag : UInt8){
        var flagValue = flag
        //let flagData = Data(bytes: UnsafePointer<UInt8>(&flagValue), count: sizeof(UInt8))
        let flagData = Data(bytes: &flagValue, count: MemoryLayout<UInt8>.size)
        self.sourcePeripheral?.writeValue(flagData, forCharacteristic: URLFlagCharacteristic, withAcknowledgement: true)
    }
    
    func writeURLToBeaconWith(_ url : String){
        
        var urlString = url
        let urlData = NSMutableData()
        
        if let urlPrefixString = urlString.getURLPrefixString(){
            
            let prefixFlag  = URLSchemePrefix(rawValue: urlPrefixString)
            if prefixFlag != nil{
                var prefixValue = prefixFlag!.prefixHexValue
                urlData.append(&prefixValue, length: MemoryLayout<UInt8>.size)
            }
            
            urlString = urlString.replacingOccurrences(of: urlPrefixString, with: "")
        }
        
        
        if let urlPostfixString = urlString.getURLPostfixString(){
            
            let postfixFlag = URLSchemePostfix(rawValue: urlPostfixString)
            
            if postfixFlag != nil{
                var postfixValue = postfixFlag!.postfixHexValue
                urlString = urlString.replacingOccurrences(of: urlPostfixString, with: "")

                urlData.append(urlString.getASCIIEncodedData() as Data)
                urlData.append(&postfixValue, length: MemoryLayout<UInt8>.size)
            }
        }else{
            urlData.append(urlString.getASCIIEncodedData() as Data)

        }
        
        print("URL data = \(urlData)")
        //self.sourcePeripheral?.writeValue(urlData, forCharacteristic: URIDataCharacteristic, withAcknowledgement: true)
        self.sourcePeripheral?.writeValue(urlData as Data!, forCharacteristic: URIDataCharacteristic, withAcknowledgement: true)
    }
}




// MARK:- BLESourceDeviceOperationsDelegate


extension AtmelEddystoneInterface : BLESourceDeviceOperationsDelegate

{
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice, didUpdateValueForCharacteristic characteristic: CBCharacteristic!, error: NSError!){
    
        if characteristic.isEqual(lockStateCharacteristic){
            getBeaconLockStateFrom(characteristic)
        }
        else if characteristic.isEqual(URIDataCharacteristic){
            print("read URIDataCharacteristic  value \(String(describing: characteristic.value))")
            getURLDataFrom(characteristic)
        }
        else if characteristic.isEqual(URLFlagCharacteristic){
            print("read URLFlagCharacteristic")

            getURLFlagFrom(characteristic)
        }
        else if characteristic.isEqual(TXPowerLevelCharacteristic){
            
            print("read TXPowerLevelCharacteristic")
            getPowerLevelValuesFrom(characteristic)
        }
        else if characteristic.isEqual(TXPowerModeCharacteristic){
            print("read TXPowerModeCharacteristic")

            getPowerModeFrom(characteristic)
        }
        else if characteristic.isEqual(beaconPeriodCharcteristic){
            print("read beaconPeriodCharcteristic")

            getBeaconPeriodFrom(characteristic)
        }
        
    }
    
    
    
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice,didWriteValueForCharacteristic characteristic: CBCharacteristic!, error: NSError!){
        
        
        if error == nil{
        
            if delegate != nil{
                delegate?.configurationServiceDidWriteValueForCharacteristic(characteristic, withError: nil)
            }
        }
            
        else{
            let writeStatus  = BeaconErrorCode(rawValue: error.code)
            
            if writeStatus != nil{
                
                var errorString = ""
                
                switch writeStatus!{
                    
                case .beaconWriteNotPermitted :
                    errorString = "Write not permitted"
                    
                case .beaconInvalidAttributeLength :
                    errorString = "invalid attribute length"
                    
                case .beaconInsufficientAuthorization :
                    errorString = " Insufficient authorization"
                    
                default : break
                }
                
                if delegate != nil{
                    delegate?.configurationServiceDidWriteValueForCharacteristic(characteristic, withError: errorString)
                }
            }
        }
    }
    
    
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice,didUpdateNotificationStateForCharacteristic characteristic: CBCharacteristic!, error: NSError!){
        
    }
}



// MARK:- Parsing Methods

extension AtmelEddystoneInterface{
    
    
    fileprivate func getBeaconLockStateFrom(_ characteristic : CBCharacteristic){
        
        let data : Data = characteristic.value!
        
        if data.count < 1{
            return
        }
        //let lockStateValue : UInt8 = (data.subdata(in: NSMakeRange(0, 1)) as NSData).bytes.bindMemory(to: UInt8.self, capacity: data.subdata(with: NSMakeRange(0, 1)).count).pointee
        let lockStateValue : UInt8 = (data.subdata(in: 0..<1) as NSData).bytes.bindMemory(to: UInt8.self, capacity: data.subdata(in: 0..<1).count).pointee
            
        if lockStateValue > 0{
            beaconLockStatus = .locked
        }
        else{
            beaconLockStatus = .unLocked
        }
        
        if delegate != nil{
            delegate?.configurationServiceDidReadLockStatus(beaconLockStatus!)
        }
    }
    
    
    
    fileprivate func getURLDataFrom(_ characteristic : CBCharacteristic){
        
        let data : Data = characteristic.value!
        let URLData =  URIDataFrame(data: data)
        print("Eddystone URL   : \(URLData.urlString!)")
        beaconDetails?.urlString = URLData.urlString
        updateBeaconDetailsFor(characteristic)
    }
    
    
    
    fileprivate func getPowerLevelValuesFrom(_ characteristic : CBCharacteristic){
        
        let data : Data = characteristic.value!

        if data.count >= 4{
            
            /*let lowestPowerLevel    = (data.subdata(in: NSMakeRange(0, 1)) as NSData).bytes.bindMemory(to: Int8.self, capacity: data.subdata(with: NSMakeRange(0, 1)).count).pointee
            let lowerPowerLevel     = (data.subdata(in: NSMakeRange(1, 1)) as NSData).bytes.bindMemory(to: Int8.self, capacity: data.subdata(with: NSMakeRange(1, 1)).count).pointee
            let mediumPowerLevel    = (data.subdata(in: NSMakeRange(2, 1)) as NSData).bytes.bindMemory(to: Int8.self, capacity: data.subdata(with: NSMakeRange(2, 1)).count).pointee
            let highPowerLevel      = (data.subdata(in: NSMakeRange(3, 1)) as NSData).bytes.bindMemory(to: Int8.self, capacity: data.subdata(with: NSMakeRange(3, 1)).count).pointee*/
            let lowestPowerLevel    = (data.subdata(in: 0..<1) as NSData).bytes.bindMemory(to: Int8.self, capacity: data.subdata(in: 0..<1).count).pointee
            let lowerPowerLevel     = (data.subdata(in: 1..<2) as NSData).bytes.bindMemory(to: Int8.self, capacity: data.subdata(in: 1..<2).count).pointee
            let mediumPowerLevel    = (data.subdata(in: 2..<3) as NSData).bytes.bindMemory(to: Int8.self, capacity: data.subdata(in: 2..<3).count).pointee
            let highPowerLevel      = (data.subdata(in: 3..<4) as NSData).bytes.bindMemory(to: Int8.self, capacity: data.subdata(in: 3..<4).count).pointee
            
            print("lowestPowerLevel = \(lowestPowerLevel)  lowerPowerLevel = \(lowerPowerLevel)  mediumPowerLevel = \(mediumPowerLevel)   highPowerLevel = \(highPowerLevel)")
            
            beaconDetails?.txPowerLevelsArray = [lowestPowerLevel, lowerPowerLevel, mediumPowerLevel, highPowerLevel]
            updateBeaconDetailsFor(characteristic)
        }
    }
    
    
    
    fileprivate func getPowerModeFrom(_ characteristic : CBCharacteristic){
       
        let data : Data = characteristic.value!
        var powerModeString = ""
        
        if data.count >= 1{
            
            //let powerModeValue = (data.subdata(in: NSMakeRange(0, 1)) as NSData).bytes.bindMemory(to: UInt8.self, capacity: data.subdata(with: NSMakeRange(0, 1)).count).pointee
            let powerModeValue = (data.subdata(in: 0..<1) as NSData).bytes.bindMemory(to: UInt8.self, capacity: data.subdata(in: 0..<1).count).pointee
            let powerMode = TxPowerModes(rawValue: powerModeValue)
            
            switch powerMode!{
            case .txPowerModeLowest :
                powerModeString = "Lowest"
            case .txPowerModeLow :
                powerModeString = "Lower"
            case .txPowerModeMedium :
                powerModeString = "Medium"
            case .txPowerModeHigh :
                powerModeString = "High"
            }
        }
        print("Eddystone Powermode = \(powerModeString)")
        beaconDetails?.txPowerMode = powerModeString
        updateBeaconDetailsFor(characteristic)
    }
    
    
    fileprivate func getBeaconPeriodFrom(_ characteristic : CBCharacteristic){
        
        let data : Data = characteristic.value!
        
        if data.count >= 2{
            
            //let beaconPeriod : UInt16 = (data.subdata(in: NSMakeRange(0, 2)) as NSData).bytes.bindMemory(to: UInt16.self, capacity: data.subdata(with: NSMakeRange(0, 2)).count).pointee
            let beaconPeriod : UInt16 = (data.subdata(in: 0..<2) as NSData).bytes.bindMemory(to: UInt16.self, capacity: data.subdata(in: 0..<2).count).pointee
            print("Eddystone Beacon period = \(beaconPeriod)")
            beaconDetails?.beaconPeriod = beaconPeriod
            updateBeaconDetailsFor(characteristic)
        }
    }
    
    fileprivate func getURLFlagFrom(_ characteristic : CBCharacteristic){
        
        let data : Data = characteristic.value!
    
        if data.count >= 1{
            
            //let flagValue = (data.subdata(in: NSMakeRange(0, 1)) as NSData).bytes.bindMemory(to: UInt8.self, capacity: data.subdata(with: NSMakeRange(0, 1)).count).pointee
            let flagValue = (data.subdata(in: 0..<1) as NSData).bytes.bindMemory(to: UInt8.self, capacity: data.subdata(in: 0..<1).count).pointee
            print("Eddystone URL flag value = \(flagValue)")
            beaconDetails?.urlFlag = flagValue
            updateBeaconDetailsFor(characteristic)
        }
        
    }
    
    
    fileprivate func updateBeaconDetailsFor(_ characteristic : CBCharacteristic){
        
        if delegate != nil{
            delegate?.configurationServiceDidReadValueForCharacteristic(characteristic, beaconInfo: beaconDetails)
        }
    }
    
    
    
}







