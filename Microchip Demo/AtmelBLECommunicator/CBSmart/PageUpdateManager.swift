/***************************************************************************
 
 PageUpdateManager.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}



let maxSegmentPageSize = 274

class PageDataNotificationResponseFrame:OTAResponseFrame{
    
    
    var sectionID:OTAUImageSectionID!
    var pageNo:UInt16!
    
    override init(withData:Data){
        
        super.init(withData:withData)
        
        if command != nil {
            switch(command!){
                
            case OTAUCommand.failure:
                
                responseType = OTAUResponseType.failure
            
            if withData.count >= 4
                //{errorCode = ResponseErrorCode(rawValue:(withData.subdata(in: NSMakeRange(3, 1)) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(with: NSMakeRange(3, 1)).count).pointee)}
                {errorCode = ResponseErrorCode(rawValue:(withData.subdata(in: 3..<4) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(in: 3..<4).count).pointee)}
                
                break
                
            case OTAUCommand.image_PAGE_INFO_ERROR:
                responseType = OTAUResponseType.failure
                
                
                if withData.count >= 4
                //{sectionID = OTAUImageSectionID(rawValue:(withData.subdata(in: NSMakeRange(3, 1)) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(with: NSMakeRange(3, 1)).count).pointee)}
                {sectionID = OTAUImageSectionID(rawValue:(withData.subdata(in: 3..<4) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(in: 3..<4).count).pointee)}
                
                if withData.count >= 6
                //{pageNo = (withData.subdata(in: NSMakeRange(4, 2)) as NSData).bytes.bindMemory(to: UInt16.self, capacity: withData.subdata(with: NSMakeRange(4, 2)).count).pointee}
                {pageNo = (withData.subdata(in: 4..<6) as NSData).bytes.bindMemory(to: UInt16.self, capacity: withData.subdata(in: 4..<6).count).pointee}

                break
                
            case OTAUCommand.page_DATA_NOTIFY_RESP:
                
                responseType = OTAUResponseType.success
                
                if withData.count >= 4
                    //{sectionID = OTAUImageSectionID(rawValue:(withData.subdata(in: NSMakeRange(3, 1)) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(with: NSMakeRange(3, 1)).count).pointee)}
                    {sectionID = OTAUImageSectionID(rawValue:(withData.subdata(in: 3..<4) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(in: 3..<4).count).pointee)}
                
                if withData.count >= 6
                    //{pageNo = (withData.subdata(in: NSMakeRange(4, 2)) as NSData).bytes.bindMemory(to: UInt16.self, capacity: withData.subdata(with: NSMakeRange(4, 2)).count).pointee}
                    {pageNo = (withData.subdata(in: 4..<6) as NSData).bytes.bindMemory(to: UInt16.self, capacity: withData.subdata(in: 4..<6).count).pointee}
                
                break
                
            default:
                responseType = OTAUResponseType.invalid
                break
            }
            
        }
        
    }
    
}
class PageDataNotificationRequestFrame:OTARequestFrame {
    
    var sectionID:OTAUImageSectionID!
    var pageNo:UInt16!
    var pageData:Data!
    
    init(withPageManager:PageUpdateManager,pageDatas:Data) {
        
        super.init()
        
        pageData = pageDatas

        length  = OTAFrameLength.pageRequest.rawValue + UInt16(pageData.count)
        command = OTAUCommand.page_DATA_NOTIFY_REQUEST
        
        sectionID = withPageManager.currentSection.sectionID
        pageNo = withPageManager.currentPageNo
    }
    
    func dataFromFrame()->Data?{
        
        if  sectionID == nil || pageNo == nil || pageData == nil
        {return nil}
        
        
        //16 bitlength data
        
        var dataLength:UInt16 = length!
        let returnFrameData = NSMutableData(bytes: &dataLength, length: MemoryLayout.size(ofValue: dataLength))
        
        //8 bit command data
        
        var commandData:UInt8 = command!.rawValue
        returnFrameData.append(&commandData, length: MemoryLayout.size(ofValue: commandData))
        
        
        //8 bit sectionID  data
        
        var sectID:UInt8 = sectionID.rawValue
        returnFrameData.append(&sectID, length: MemoryLayout.size(ofValue: sectID))
        
        // 16 bit pageNumber data
        
        var pageNumber:UInt16 = pageNo
        returnFrameData.append(&pageNumber, length: MemoryLayout.size(ofValue: pageNumber))
        
        // pageData
        
        returnFrameData.append(pageData)
        
        return returnFrameData as Data
    }
    
}
class PageUpdateManager: OTATask {
    
    var sucessBlock:(()->())? = nil
    var failBlock:((_ withError:OTAErrorType,_ errorString:String)->())? = nil
    
    var updateInfo:ImageInfoNotificationResponseFrame!
    
    var currentPageNo:UInt16!
    var currentSection:Section!
    var pageSize:UInt16!
    var currentPage:Page?
    var offSet:Int = 0
    
    init(interface:AtmelOTAInterface, currentPageNumber:UInt16,currentSec:Section,withPageSize:UInt16!){
        
        super.init()
        pageSize = withPageSize
        otaInterface = interface
        otaInterface.delegate = self
        currentSection = currentSec
        currentPageNo = currentPageNumber
    }
    
    func start(success sucess:@escaping ()->(),fail:@escaping (_ withError:OTAErrorType,_ errorString:String)->()){
        
        currentPage = Page(fromSection: currentSection, pageNumber: currentPageNo,blocksize:pageSize,segmentsWithSize:Int(maxSegmentPageSize))

        if  currentPage?.pageData != nil {
            
            sucessBlock = sucess
            failBlock = fail
             writePageData()
        }
            
        else{
            fail(OTAErrorType.localError, "Page data  from image file Could not be obtained")
        }
    }
    
    func writePageData(){
        
        if let data = currentPage?.getPageSegmentData(offSet){
//            otaInterface..updatedProgressInfo("Writing page \(currentPageNo!+1) of \(currentSection!.numberOFPages!) in  \(currentSection!.sectionID!.description) section", progress:currentPage?.progressFromSegment(offSet, totalImageSize: Float(otaInterface.image.totalSectionSize)))
            print("Writing page \(currentPageNo!+1) of \(currentSection!.numberOFPages!) in  \(currentSection!.sectionID!.description) section with size = \(data.count)")
            

            let requestFrame = PageDataNotificationRequestFrame(withPageManager: self,pageDatas: data)
            otaInterface.sendFrameData(requestFrame.dataFromFrame()!)
            
        }
        
    }
    
    
    
}

extension PageUpdateManager:AtmelOTAInterfaceDelegate{
    
    func receivedResponseFrameData(_ responseData:Data){
        
        let respFrame = PageDataNotificationResponseFrame(withData: responseData)
        
        if  respFrame.responseType == OTAUResponseType.success{
            
            
            let progressvalue = currentPage?.progressFromSegment(offSet, totalImageSize: Float(otaInterface.image!.totalSectionSize))
            
            //Write next segment of current page
            print("Page Written")
            otaInterface.updatedProgressInfo("Written  \(Int(progressvalue! * Float(otaInterface.image!.totalSectionSize))) bytes  of  \(otaInterface.image!.totalSectionSize) bytes", progress:progressvalue)
            
            if otaShouldAbort(){
                return
            }
            
            offSet+=1
            if offSet < currentPage?.maxNumberOfSegments{
                writePageData()
            }
            else
            {
                //Write next page in the segment
                
                currentPage = currentPage?.nextPage()
                
                if currentPage != nil && currentPage?.pageData != nil{
                    
                    offSet = 0
                    currentPageNo = currentPage?.pageNo
                    writePageData()
                }
                    
                    // call success if there are no pages remaining in section
                    
                else{
                    sucessBlock!()
                }
                
            }
        }
        else if  respFrame.responseType == OTAUResponseType.failure{
            
            print("Page Fail")
            failBlock!(OTAErrorType.communicationError,"Page Fail error (\(respFrame.errorCode))")
            
            if otaShouldAbort(){
                return
            }
        }
        
    }
}
