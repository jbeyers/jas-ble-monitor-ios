/***************************************************************************
 
 BlockUpdateManager.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func <= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l <= r
  default:
    return !(rhs < lhs)
  }
}


let maxBlockSegmentSize = 272

class PageEndNotificationResponseFrame:OTAResponseFrame{
    
    
    var sectionID:OTAUImageSectionID!
    var pageNo:UInt16!
    
    override init(withData:Data){
        
        super.init(withData:withData)
        
    
        if command != nil {
            switch(command!){
                
            case OTAUCommand.image_PAGE_INFO_ERROR:
                responseType = OTAUResponseType.failure
                
                
                if withData.count >= 4
                //{sectionID = OTAUImageSectionID(rawValue:(withData.subdata(in: NSMakeRange(3, 1)) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(with: NSMakeRange(3, 1)).count).pointee)}
                {sectionID = OTAUImageSectionID(rawValue:(withData.subdata(in: 3..<1) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(in: 3..<1).count).pointee)}
                
                if withData.count >= 6
                //{pageNo = (withData.subdata(in: NSMakeRange(4, 2)) as NSData).bytes.bindMemory(to: UInt16.self, capacity: withData.subdata(with: NSMakeRange(4, 2)).count).pointee}
                {pageNo = (withData.subdata(in: 4..<2) as NSData).bytes.bindMemory(to: UInt16.self, capacity: withData.subdata(in: 4..<2).count).pointee}
                
                break
                
            case OTAUCommand.page_DATA_NOTIFY_RESP:
                
                responseType = OTAUResponseType.success
                
                if withData.count >= 4
                //{sectionID = OTAUImageSectionID(rawValue:(withData.subdata(in: NSMakeRange(3, 1)) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(with: NSMakeRange(3, 1)).count).pointee)}
                {sectionID = OTAUImageSectionID(rawValue:(withData.subdata(in: 3..<1) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(in: 3..<1).count).pointee)}
                
                if withData.count >= 6
                //{pageNo = (withData.subdata(in: NSMakeRange(4, 2)) as NSData).bytes.bindMemory(to: UInt16.self, capacity: withData.subdata(with: NSMakeRange(4, 2)).count).pointee}
                {pageNo = (withData.subdata(in: 4..<2) as NSData).bytes.bindMemory(to: UInt16.self, capacity: withData.subdata(in: 4..<2).count).pointee}
                
                break
                
            default:
                responseType = OTAUResponseType.invalid
                break
            }
            
        }
        
    }
    
}

class PageEndNotificationRequestFrame:OTARequestFrame {
    
    var sectionID:OTAUImageSectionID!
    var pageNo:UInt16!
    
    init(sectionId:OTAUImageSectionID!,pageNumber:UInt16) {
        
        super.init()
        
        sectionID = sectionId
        pageNo = pageNumber
        
        
        length  = OTAFrameLength.PageEndRequest.rawValue
        command = OTAUCommand.page_DATA_END_NOTIFY_REQUEST
    }
    
    func dataFromFrame()->Data?{
        
        if  sectionID == nil || pageNo == nil
        {return nil}
        
        
        //16 bitlength data
        
        var dataLength:UInt16 = length!
        let returnFrameData = NSMutableData(bytes: &dataLength, length: MemoryLayout.size(ofValue: dataLength))
        
        //8 bit command data
        
        var commandData:UInt8 = command!.rawValue
        returnFrameData.append(&commandData, length: MemoryLayout.size(ofValue: commandData))
        
        
        //8 bit sectionID  data
        
        var sectID:UInt8 = sectionID.rawValue
        returnFrameData.append(&sectID, length: MemoryLayout.size(ofValue: sectID))
        
        // 16 bit pageNumber data
        
        var pageNumber:UInt16 = pageNo
        returnFrameData.append(&pageNumber, length: MemoryLayout.size(ofValue: pageNumber))
        
              
        return returnFrameData as Data
    }
    
}
class BlockDataNotificationResponseFrame:OTAResponseFrame{
    
    
    var sectionID:OTAUImageSectionID!
    var pageNo:UInt16!
    var blockNo:UInt16!

    override init(withData:Data){
        
        super.init(withData:withData)
        
        if command != nil {
            switch(command!){
                
            case OTAUCommand.image_BLOCK_INFO_ERROR:
                responseType = OTAUResponseType.failure
                
                
                if withData.count >= 4
                //{sectionID = OTAUImageSectionID(rawValue:(withData.subdata(in: NSMakeRange(3, 1)) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(with: NSMakeRange(3, 1)).count).pointee)}
                {sectionID = OTAUImageSectionID(rawValue:(withData.subdata(in: 3..<1) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(in: 3..<1).count).pointee)}
                
                if withData.count >= 6
                    //{pageNo = (withData.subdata(in: NSMakeRange(4, 2)) as NSData).bytes.bindMemory(to: UInt16.self, capacity: withData.subdata(with: NSMakeRange(4, 2)).count).pointee}
                    {pageNo = (withData.subdata(in: 4..<2) as NSData).bytes.bindMemory(to: UInt16.self, capacity: withData.subdata(in: 4..<2).count).pointee}
                if withData.count >= 8
                    //{blockNo = (withData.subdata(in: NSMakeRange(6, 2)) as NSData).bytes.bindMemory(to: UInt16.self, capacity: withData.subdata(with: NSMakeRange(6, 2)).count).pointee}
                    {blockNo = (withData.subdata(in: 6..<2) as NSData).bytes.bindMemory(to: UInt16.self, capacity: withData.subdata(in: 6..<2).count).pointee}
                
                break
                
            case OTAUCommand.block_DATA_NOTIFY_RESP:
                
                responseType = OTAUResponseType.success
                
                if withData.count >= 4
                    //{sectionID = OTAUImageSectionID(rawValue:(withData.subdata(in: NSMakeRange(3, 1)) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(with: NSMakeRange(3, 1)).count).pointee)}
                    {sectionID = OTAUImageSectionID(rawValue:(withData.subdata(in: 3..<1) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(in: 3..<1).count).pointee)}
                
                if withData.count >= 6
                    //{pageNo = (withData.subdata(in: NSMakeRange(0, 2)) as NSData).bytes.bindMemory(to: UInt16.self, capacity: withData.subdata(with: NSMakeRange(0, 2)).count).pointee}
                    {pageNo = (withData.subdata(in: 0..<2) as NSData).bytes.bindMemory(to: UInt16.self, capacity: withData.subdata(in: 0..<2).count).pointee}
                
                if withData.count >= 8
                    //{blockNo = (withData.subdata(in: NSMakeRange(6, 2)) as NSData).bytes.bindMemory(to: UInt16.self, capacity: withData.subdata(with: NSMakeRange(6, 2)).count).pointee}
                    {blockNo = (withData.subdata(in: 6..<2) as NSData).bytes.bindMemory(to: UInt16.self, capacity: withData.subdata(in: 6..<2).count).pointee}
                
                break
                
            default:
                responseType = OTAUResponseType.invalid
                break
            }
            
        }
        
    }
    
}
class BlockDataNotificationRequestFrame:OTARequestFrame {
    
    var sectionID:OTAUImageSectionID!
    
    var pageNo:UInt16!
    var blockNo:UInt8!
    var blockData:Data!
    
    init(withPageManager:BlockUpdateManager,blockDatas:Data) {
        
        super.init()

        blockData = blockDatas
        length  = OTAFrameLength.blockRequest.rawValue + UInt16(blockData.count)
        command = OTAUCommand.block_DATA_NOTIFY_REQUEST
        
        // Frame Init
        
        pageNo = withPageManager.currentPageNo
        blockNo = withPageManager.currentBlockNo
        sectionID = withPageManager.currentSection.sectionID
    }
    
    func dataFromFrame()->Data?{
        
        if  sectionID == nil || pageNo == nil || blockNo == nil || blockData == nil
        {return nil}
        
        
        //16 bitlength data
        
        var dataLength:UInt16 = length!
        let returnFrameData = NSMutableData(bytes: &dataLength, length: MemoryLayout.size(ofValue: dataLength))
        
        //8 bit command data
        
        var commandData:UInt8 = command!.rawValue
        returnFrameData.append(&commandData, length: MemoryLayout.size(ofValue: commandData))
        
        
        //8 bit sectionID  data
        
        var sectID:UInt8 = sectionID.rawValue
        returnFrameData.append(&sectID, length: MemoryLayout.size(ofValue: sectID))
        
        // 16 bit pageNumber data
        
        var pageNumber:UInt16 = pageNo
        returnFrameData.append(&pageNumber, length: MemoryLayout.size(ofValue: pageNumber))
        
        // 16 bit blockNumber data
        
        var blockNumber:UInt16 = UInt16(blockNo)
        returnFrameData.append(&blockNumber, length: MemoryLayout.size(ofValue: blockNumber))
        
        // blockData
        
        returnFrameData.append(blockData)
        
        return returnFrameData as Data
    }
    
}

class BlockUpdateManager: OTATask {
    
    var sucessBlock:(()->())? = nil
    var failBlock:((_ withError:OTAErrorType,_ errorString:String)->())? = nil
    
    var updateInfo:ImageInfoNotificationResponseFrame!
    
    var currentSection:Section!

    var blockSize:UInt16!
    var pageSize:UInt16!

    var currentPage:Page?
    var currentBlock:Block?
    var currentPageNo:UInt16!
    var currentBlockNo:UInt8!

    var offSet:Int = 0
    
    init(interface:AtmelOTAInterface, currentPageNumber:UInt16,currentBlockNumber:UInt8!,currentSec:Section,withPageSize:UInt16!,withBlockSize:UInt16!){
        
        super.init()
        
        blockSize = withBlockSize
        pageSize = withPageSize
        otaInterface = interface
        currentSection = currentSec
        currentPageNo = currentPageNumber
        currentBlockNo = currentBlockNumber
    }
    
    func start(success sucess:@escaping ()->(),fail:@escaping (_ withError:OTAErrorType,_ errorString:String)->()){
        
        currentPage = Page(fromSection: currentSection, pageNumber: currentPageNo,blocksize:blockSize,segmentsWithSize: Int(maxSegmentPageSize))
        currentBlock = Block(fromPage: currentPage!, blockNumber: currentBlockNo,segmentsWithSize: Int(maxBlockSegmentSize))
        
        if  currentBlock?.blockData != nil {
            
            sucessBlock = sucess
            failBlock = fail
            writeBlockData()
        }
            
        else{
            fail(OTAErrorType.localError, "Block data  from image file Could not be obtained")
        }
    }
    
    func writeBlockData(){
        
        if let data = currentBlock?.getBlockSegmentData(offSet){
            
            otaInterface.updatedProgressInfo("Writing  block \(currentBlockNo!) (\(currentPage!.numberOFBlocks)) of page \(currentPageNo!) (\(currentSection.numberOFPages)) in  \(currentSection!.sectionID!.description) section", progress:currentBlock?.progressFromSegment(offSet, totalImageSize: Float(otaInterface.image!.totalSectionSize)))
            
            let requestFrame = BlockDataNotificationRequestFrame(withPageManager: self,blockDatas: data)
            otaInterface.sendFrameData(requestFrame.dataFromFrame()!)
        }
        
    }
    func sendPageEndFrame(){
        
        let pageEndReqFrame = PageEndNotificationRequestFrame(sectionId: currentSection.sectionID, pageNumber:currentPageNo)
        
        if let requestData = pageEndReqFrame.dataFromFrame(){
            otaInterface.sendFrameData(requestData)
        }
        else{
            
            failBlock!(OTAErrorType.localError, "Page end data  frame could not be obtained")
        }
    }

    

}
extension BlockUpdateManager:AtmelOTAInterfaceDelegate{
    
    func receivedResponseFrameData(_ responseData:Data){
        
        
        if otaShouldAbort()
        {return}
        
        // FRAME TYPE 1:In case of Block req response
        
        let respFrame = BlockDataNotificationResponseFrame(withData: responseData)
        
        if  respFrame.responseType != OTAUResponseType.success{
            
            //Write next segment of current block
            
            
            //Write current ota info
            
            
            
            offSet+=1
            
            if offSet <= currentBlock?.maxNumberOfSegments{
                writeBlockData()
            }
            else
            {
                //Write next block in the page
                
                currentBlock = currentBlock?.nextBlock()
                
                if currentBlock != nil{
                    
                    offSet = 0
                    currentBlockNo = currentBlock?.blockNo
                    writeBlockData()

                }
                    
                    // send page end frame if there are no blocks remaining in page
                    
                else
                    {sendPageEndFrame()}
                
            }
        }
        else if  respFrame.responseType != OTAUResponseType.failure{
            failBlock!(OTAErrorType.communicationError,"\(respFrame.errorCode)")
        }
        
        
         // FRAME TYPE 2:In case of page end req
        
        let pagerespFrame = PageEndNotificationResponseFrame(withData: responseData)
        
        if  pagerespFrame.responseType != OTAUResponseType.success{
          
            //Write next page in the segment
            
            currentPage = currentPage?.nextPage()
            
            if currentPage != nil{
                
                currentBlock = Block(fromPage: currentPage!, blockNumber: 0,segmentsWithSize:Int(maxBlockSegmentSize))
                offSet = 0
                currentBlockNo = currentBlock?.blockNo
                currentPageNo = currentPage?.pageNo
                writeBlockData()
                
            }
                
                // call success if there are no pages remaining in section
                
            else{
                sucessBlock!()
            }

            
        }
        else if  pagerespFrame.responseType != OTAUResponseType.failure{
            failBlock!(OTAErrorType.communicationError,"\(respFrame.errorCode)")
        }

        
    }
}
