
/* ***************************************************************************

SerialPortInterface.swift


Copyright (c) 2015, Atmel Corporation. All rights reserved.
Released under NDA
Licensed under Atmel's Limited License Agreement.


THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Atmel Corporation: http://www.atmel.com

******************************************************************************/


protocol BLESerialPortInterfaceDelegate:NSObjectProtocol{
    
    func bleSerialPortDidReceiveMessageFromSender(_ message:String?)
    func blePeripheralReadyForCommunication()
    func blePeripheralNotReadyForComunication()
    func blePeripheralMessageSuccessfullySent()
    func blePeripheralMessageSendingFailed()
}
protocol BLESerialPortProfileReadyDelegate:NSObjectProtocol{
    func bleSerialPortReady()
}

import Foundation
import CoreBluetooth

class BLESerialPortInterface:NSObject{
    
    var isCommunicatingPeripheralReady:Bool = false{
        didSet{
            
            if isCommunicatingPeripheralReady
            {self.delegate?.blePeripheralReadyForCommunication()}
            else
            {self.delegate?.blePeripheralNotReadyForComunication()}
        }
    }
    
    var peripheralManager:BLEPeripheralManager

    var sourcePeripheral:BLESourceDevice?
    var delegate:BLESerialPortInterfaceDelegate?
    var profileReadyDelegate:BLESerialPortProfileReadyDelegate?

    var serialPortService:CBService?
   
    var endPointCharacteristics:CBCharacteristic?
    
    var peripheralSerialPortService:CBMutableService?

    var peripheralEndPointCharacteristics:CBMutableCharacteristic?

    var receiverCentral:CBCentral?
    var messageCharLimit:Int?

    init(sourceDevice:BLESourceDevice? ,delgateObject:BLESerialPortInterfaceDelegate?){
        
        peripheralManager = BLEPeripheralManager()
        super.init()
        self.delegate = delgateObject
        peripheralManager.delegate=self
        self.sourcePeripheral=sourceDevice
        self.sourcePeripheral?.operationsDelegate = self
        let serviceArray = sourceDevice?.CBSPeripheral.services
        
        serialPortService=serviceArray?.filter(){$0.uuid.isEqual(CBUUID(bleService:BLEService.SerialPortService))}.first
        endPointCharacteristics=(serialPortService?.characteristics )?.filter(){$0.uuid.isEqual(CBUUID(blecharacteristic: BLECharacteristic.EndPointCharacteristics))}.first


    }
    func set(_ sourceDevice:BLESourceDevice? ,delgateObject:BLESerialPortInterfaceDelegate){
        
        self.delegate = delgateObject

        
//        self.sourcePeripheral=sourceDevice
//        self.sourcePeripheral?.operationsDelegate = self
//        let serviceArray = sourceDevice?.CBSPeripheral.services
//        serialPortService=serviceArray?.filter(){$0.UUID.isEqual(CBUUID(bleService:BLEService.SerialPortService))}.first
//        endPointCharacteristics=(serialPortService?.characteristics )?.filter(){$0.UUID.isEqual(CBUUID(blecharacteristic: BLECharacteristic.EndPointCharacteristics))}.first
        

    }
    
    func initialSet(){
        
        self.sourcePeripheral=GlobalCommunicator.shared.sourceDevice
        self.delegate = nil
        self.sourcePeripheral?.operationsDelegate = self
        let serviceArray = GlobalCommunicator.shared.sourceDevice?.CBSPeripheral.services
        
        serialPortService=serviceArray?.filter(){$0.uuid.isEqual(CBUUID(bleService:BLEService.SerialPortService))}.first
        endPointCharacteristics=(serialPortService?.characteristics )?.filter(){$0.uuid.isEqual(CBUUID(blecharacteristic: BLECharacteristic.EndPointCharacteristics))}.first
        
        
    }
    
}
//MARK : Messanger functions

extension BLESerialPortInterface{
    
    
    func checkReceiverStatus()
    {
        if  receiverCentral != nil
        {
            isCommunicatingPeripheralReady = true
        }
        else
        {
            isCommunicatingPeripheralReady = false
        }
    }
    func stopServices(){
        if(peripheralManager.CBPManager != nil){
            print("peripheralmanager is Removing all services")
            peripheralManager.CBPManager.removeAllServices()
            peripheralManager.CBPManager.stopAdvertising()

            
            peripheralManager.CBPManager = nil
        }
    }
    func startMessageBoxWithCharLimit(_ limit:Int){
        startEndPointNotifications()
        messageCharLimit = limit
        }
    
    func sendMessage(_ message:String){
        
        if(isCommunicatingPeripheralReady){
            
            print("sending message to  central =\(message)")
            self.peripheralManager.CBPManager.updateValue(message.data(using: String.Encoding.utf8, allowLossyConversion: true)!, for: peripheralEndPointCharacteristics!, onSubscribedCentrals: [receiverCentral!])
                self.delegate?.blePeripheralMessageSuccessfullySent()
        }
        else
        {self.delegate?.blePeripheralMessageSendingFailed()}
    }
}

//MARK : Interface functions

extension BLESerialPortInterface{
    
   
    
    func startEndPointNotifications()
    {self.sourcePeripheral?.setNotifyValue(true, forCharacteristic: endPointCharacteristics)}
    
    func stopEndPointNotifications()
    {self.sourcePeripheral?.setNotifyValue(false, forCharacteristic: endPointCharacteristics)}
}

extension BLESerialPortInterface:BLESourceDeviceOperationsDelegate{
    
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice, didUpdateValueForCharacteristic characteristic: CBCharacteristic!, error: NSError!){
        
        
        print("SPP didUpdateValueForCharacteristic ")
        if characteristic.isEqual(endPointCharacteristics){
            let messageString = NSString(data:endPointCharacteristics!.value!, encoding: String.Encoding.ascii.rawValue) as String?
            receivedMessage(messageString)
        }
        
    }
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice,didWriteValueForCharacteristic characteristic: CBCharacteristic!, error: NSError!){
        
    }
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice,didUpdateNotificationStateForCharacteristic characteristic: CBCharacteristic!, error: NSError!){
        if characteristic.isEqual(endPointCharacteristics)
        {}
        
    }
   
}
extension BLESerialPortInterface{

    fileprivate func startAdvertising(){

        
        
        self.peripheralManager.CBPManager.startAdvertising([CBAdvertisementDataServiceUUIDsKey:[CBUUID(bleService: BLEService.SerialPortService)]])
        
    }
    fileprivate func createSerialPortPeripheralServices(){
        
        
        print("Creating peripheral Services and adding")
        peripheralSerialPortService=CBMutableService(type: CBUUID(bleService: BLEService.SerialPortService), primary: true)
        
        peripheralEndPointCharacteristics = CBMutableCharacteristic(type: CBUUID(blecharacteristic: BLECharacteristic.EndPointCharacteristics), properties: CBCharacteristicProperties.notify, value: nil, permissions:CBAttributePermissions.readable)
       
        let charArray = [peripheralEndPointCharacteristics!]
        peripheralSerialPortService?.characteristics = charArray
        self.peripheralManager.CBPManager.add(peripheralSerialPortService!)
        
        }
    fileprivate func receivedMessage(_ messageString:String?){
        
        print("Received message =\(String(describing: messageString))")

        print("Saving message to global=\(String(describing: messageString))")
        let deviceSender    = "Device"
        let message = Message(text: messageString, sender: deviceSender)
        GlobalCommunicator.shared.sppMessages.append(message)
        
        if let _ = self.delegate
        {
            self.delegate?.bleSerialPortDidReceiveMessageFromSender(messageString)
        }
//        else
//        {
        
//        }
       
    }
   
}


extension BLESerialPortInterface:BLEPeripheralManagerDelegate{
    
    func blePeripheralManager(_ blePeripheralManager:BLEPeripheralManager ,communicatorInitializationSuccessWithBluetoothState state:BluetoothState){
        
        createSerialPortPeripheralServices()
    }
    func blePeripheralManager(_ blePeripheralManager:BLEPeripheralManager ,communicatorInitializationFailWithBluetoothState state:BluetoothState){
        
    }
    func blePeripheralManager(_ blePeripheralManager:BLEPeripheralManager , didReceiveReadRequest request: CBATTRequest!){
        
        
        
    }
    func blePeripheralManager(_ blePeripheralManager:BLEPeripheralManager ,didAddService service: CBService!, error: NSError?){
        print("Added all peripheral services")

        self.profileReadyDelegate?.bleSerialPortReady()
    }
    func blePeripheralManager(_ blePeripheralManager:BLEPeripheralManager ,didReceiveWriteRequests requests: [AnyObject]!){
        
    }
    func blePeripheralManager(_ blePeripheralManager:BLEPeripheralManager  ,central: CBCentral!, didSubscribeToCharacteristic characteristic: CBCharacteristic!){
        
        
        receiverCentral = central
        
        if receiverCentral != nil
            {isCommunicatingPeripheralReady = true}
        else
            {isCommunicatingPeripheralReady = false}
        
        
    }
    func blePeripheralManager(_ blePeripheralManager:BLEPeripheralManager ,central: CBCentral!, didUnsubscribeFromCharacteristic characteristic: CBCharacteristic!){
        receiverCentral = nil
        isCommunicatingPeripheralReady = false

    }
    func blePeripheralManagerIsReadyToUpdateSubscribers(_ blePeripheralManager:BLEPeripheralManager){
        
    }
    
    
}
