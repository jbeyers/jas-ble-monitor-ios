
/* ***************************************************************************

BLEExtensions.swift

Extensions add new functionality to an existing class,

Copyright (c) 2015, Atmel Corporation. All rights reserved.
Released under NDA
Licensed under Atmel's Limited License Agreement.


THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Atmel Corporation: http://www.atmel.com

******************************************************************************/

import Foundation
import CoreBluetooth
import CoreLocation



/*----------------------------------------------------------------------------------------------------------*/
// MARK: extension NSData
/*----------------------------------------------------------------------------------------------------------*/

let INVALID_RED = UIColor(red: 255.0/255.0, green: 0, blue: 0, alpha: 0.1)

let ALT_BEACON_ID : UInt16 = 0xBEAC

extension UITextField{
    func toUUID(_ markInvalid:Bool)->UUID?{
        
        var returnVal:AnyObject? = nil
        
        if self.text != nil
        {
            
            let uuidString  = NSMutableString(string: self.text!.replacingOccurrences(of: "0x", with: ""))
            
            if uuidString.length > 8 && uuidString.character(at: 8) != "-".utf16.first {
                uuidString.insert("-", at: 8)
            }
            
            if uuidString.length > 13 && uuidString.character(at: 13) != "-".utf16.first {
                uuidString.insert("-", at: 13)
            }
            
            if uuidString.length > 18 && uuidString.character(at: 18) != "-".utf16.first {
                uuidString.insert("-", at: 18)
            }
            
            if uuidString.length > 23 && uuidString.character(at: 23) != "-".utf16.first {
                uuidString.insert("-", at: 23)
            }

            
            returnVal = UUID(uuidString: uuidString as String) as AnyObject
        }
        
        if returnVal == nil && markInvalid
            {self.backgroundColor = INVALID_RED}
        
        return returnVal as? UUID
    }
    func toString(_ markInvalid:Bool)->String?{
        
        var returnVal:AnyObject? = nil
        
        if self.text != nil && self.text != ""
            {returnVal = self.text! as AnyObject}
        
        if returnVal == nil && markInvalid
            {self.backgroundColor = INVALID_RED}
        
        return returnVal as? String
    }

    func toUInt16(_ markInvalid:Bool)->NSNumber?{
         var returnVal:AnyObject? = nil
        
        if self.text != nil{
            
            if let intval = UInt16(self.text!)
                {returnVal =  NSNumber(value: intval as UInt16)}
            
        }
        
        if returnVal == nil && markInvalid
            {self.backgroundColor = INVALID_RED}
        return returnVal as? NSNumber
    }
    
    func toData(_ markInvalid:Bool)->Data?{
    
        var returnVal:AnyObject? = nil
        
        if self.text != nil
            {returnVal = self.text!.dataFromHexadecimalString() as AnyObject}
        
        if returnVal == nil && markInvalid
            {self.backgroundColor = INVALID_RED}
        
        return returnVal as? Data
        
        }
    func toURLString(_ markInvalid:Bool)->String?{
    
        var returnVal:AnyObject? = nil
        
        if self.text != nil
        {
            if let checkedUrl : URL? = URL(string: self.text!) {
                
                if checkedUrl != nil && checkedUrl?.scheme != nil && checkedUrl?.host != nil {
                    returnVal =  checkedUrl!.absoluteString as AnyObject
                }
            }
        }
        
        if returnVal == nil && markInvalid
            {self.backgroundColor = INVALID_RED}
        
        return returnVal as? String
    }
    
    
}
extension CLBeacon{
    func isSame(_ rBeacon: CLBeacon!) -> Bool {
        
        if rBeacon == nil
            {return false}
    return ((self.proximityUUID.uuidString == rBeacon.proximityUUID.uuidString)
    && (self.major == rBeacon.major)
    && (self.minor == rBeacon.minor))
}

}
extension String{
    
    func getLastPathComponent()->String?{
        
        if let url = URL(string: self)
            {return url.lastPathComponent}
        return nil
    }
    func dataFromHexadecimalString() -> Data? {
        let trimmedString = self.trimmingCharacters(in: CharacterSet(charactersIn: "<> ")).replacingOccurrences(of: " ", with: "")
        
        // make sure the cleaned up string consists solely of hex digits, and that we have even number of them
        
        let regex = try! NSRegularExpression(pattern: "^[0-9a-f]*$", options: .caseInsensitive)
        
        let found = regex.firstMatch(in: trimmedString, options: [], range: NSMakeRange(0, trimmedString.characters.count))
        if found == nil || found?.range.location == NSNotFound || trimmedString.characters.count % 2 != 0 {
            return nil
        }
        
        // everything ok, so now let's build NSData
        
        let data = NSMutableData(capacity: trimmedString.characters.count / 2)
        
        let index = trimmedString.startIndex
        while  index < trimmedString.endIndex  {
            
            //let byteString = trimmedString.substring (index, Collection.index())
            let byteString = trimmedString.substring (to: index)
            
            //let byteString = trimmedString.substring(with: index ..< AtmelBLECommunicator.index(after: Collection.index(after: index)))
            let num = UInt8(byteString.withCString { strtoul($0, nil, 16) })
            data?.append([num] as [UInt8], length: 1)
            //index = <#T##Collection corresponding to your index##Collection#>.index(after: <#T##Collection corresponding to `index`##Collection#>.index(after: index))
        }
        
        //return data as! Data
        return data! as Data
    }
    static func getProximityString(_ prox:CLProximity)->String{
        switch(prox){
        case .unknown:
            return "Unknown"
        case .immediate:
            return "Immediate"
        case .far:
            return "Far"
        case .near:
            return "Near"
            
        }
    }
    
    func getURLPrefixString() -> String?{
        
        let validURLPrefixes = ["http://", "https://","http://www.","https://www."]
        let requiredString = validURLPrefixes.filter() {self.contains($0)}.first
        return requiredString
    }
    
    
    func getURLPostfixString() -> String?{
        
        let validURLPostfixes = [".com/",".org/",".edu/",".net/",".info/",".biz/",".gov/",".com",".org",".edu",".net",".info",".biz",".gov"]
        let requiredString = validURLPostfixes.filter() {self.contains($0)}.first
        return requiredString
    }
    
    
    func getASCIIEncodedData() -> Data!{
        var encodedData : Data = Data()
        encodedData = self.data(using: String.Encoding.ascii)!
        return encodedData
    }

    
    
    func removeInvalidHexCharacters() -> String{
        
        let value = self.replacingOccurrences(of: "0x", with: "")
        let validHexCharacters : Set <Character> = Set("0123456789ABCDEF".characters)
        return String(value.characters.filter(){validHexCharacters.contains($0)})
    }
    
    
     func validateHexString() -> String {
        
        if  self.characters.count % 2 == 0 {
            return self
        }
        var returnString  = self
        returnString.insert("0", at: returnString.characters.index(before: returnString.characters.endIndex))
        return returnString
    }
    
    func getHexStringFromAscii() -> String{
        
        guard let hexData = self.data(using: String.Encoding.utf8) else {
            return ""
        }
        
        let hexString = "\(hexData)".trimmingCharacters(in: CharacterSet(charactersIn: "<>"))
        return hexString
    }
    
    
}


extension UIView {
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if parentResponder is UIViewController {
                return parentResponder as! UIViewController!
            }
        }
        return nil
    }
}

extension Array{
    
    
    func getImageWithLatestFirmwareVersion()->String?{
        
        if self.count == 1
            {return self.first as? String}
        
        var max = FirmWareVersion(withData: Data())
        max.majorNumber = 0
        max.minorNumber = 0
        max.buildNumber = 0
        
        var retFileUrl:String? = nil
        
        for fileUrl in self{
            
            if let str = fileUrl as? String{
            let imgH = ImageHeader(fileString: str)
                if imgH.firmwareVersion.isHigherThan(max){
                    retFileUrl = str
                    max = imgH.firmwareVersion
                }
            }
        }
        if retFileUrl != nil
            {return retFileUrl}
        else
            {return self.first as? String}
        
    }
    
    
    func getImageWithFirmwareVersion(_ version: FirmWareVersion) -> String?{
        
        for fileUrl in self {
            
            if  let str = fileUrl as? String {
                
                let imageHeader = ImageHeader(fileString: str)
                if imageHeader.firmwareVersion.isEqualTo(version) {
                    return str
                }
            }
        }
        return nil
    }
    
    
    
    func getAtmelBeaconFromArrayOfPeripheral(_ beaconPeripheral:CBPeripheral)->AtmelBeacon?{
       
        for obj in self
        {
            if let beaconObject = obj as? AtmelBeacon{
                if beaconObject.peripheral.identifier == beaconPeripheral.identifier
                    {return beaconObject}
            }
            else
            {return nil}
        }
        return nil
    }
    
    func getImageHeaderArray()->[ImageHeader]?{
        
        var returnArray = [ImageHeader]()
        for obj in self
        {
            if let fileString = obj as? String{
                returnArray.append(ImageHeader(fileString: fileString))
            }
            else
                {return nil}
           
        }

        return returnArray
    }
}

extension Data {
    
    
    func isAltBeaconAdvertisementPacket() -> Bool{
        
        if self.count >= 4{
            
            
            //if ALT_BEACON_ID == CFSwapInt16BigToHost((self.subdata(in: NSMakeRange(2, 2)) as NSData).bytes.bindMemory(to: UInt16.self, capacity: self.subdata(with: NSMakeRange(2, 2)).count).pointee){
            if ALT_BEACON_ID == CFSwapInt16BigToHost((self.subdata(in: 1..<2) as NSData).bytes.bindMemory(to: UInt16.self, capacity: self.subdata(in: 1..<2).count).pointee){                   //**** Resolve ****//
                return true
            }
          }
        return false
    }
    
    func getValueFromUInt24()->Int{
        let returnValue:UInt32 = 0
        
        var commandData:UInt8 = 0
        var returnData = NSData(data:self) as Data
        //returnData.append(Data(bytes: UnsafePointer<UInt8>(&commandData), count: sizeofValue(commandData)))
        returnData.append(Data(bytes: &commandData, count: MemoryLayout.size(ofValue: commandData)))
        //returnValue = returnData.bytes.bindMemory(to: UInt32.self, capacity: returnData.count).pointee  ****uncomment it
        
        return Int(returnValue)
    }
    
    func fixedPointToFloat()->Float?{
        
        var tempValue:Int32 = 0
        //let tempValue: Int32
    
        if self.count >= 0+2
        {
//            self.getBytes(&tempValue, range: NSMakeRange(0, 2))
        
            //let swappedData = NSData(data: self.subdata(in: NSMakeRange(1, 1))) as Data
            var swappedData = NSData(data: self.subdata(in: 1..<2)) as Data
            //swappedData.append(self.subdata(with: NSMakeRange(0, 1)))
            //swappedData.getBytes(&tempValue, range: NSMakeRange(0, 2))
            swappedData.append(self.subdata(in: 0..<1))
            //swappedData.getBytes(&tempValue, range: NSMakeRange(0, 2)) //****uncomment it
        }
        
        var signFlag = false
        
        if((tempValue & 0x8000) == 0x8000)
        {
            signFlag = true
            tempValue -=  0x0001
            tempValue = (~tempValue)&0x0000FFFF
        }
        
        let integer :Int32 = (tempValue) >> 8
        let fraction :Int32 = tempValue & 0x00FF
        
        
        var returnFloat:Float = Float(integer) + Float(fraction)/256.0
        
        if signFlag{returnFloat *= -1}
        
        return returnFloat
    }
    
    
    
    func USASCIICoverstion()->String?{
        var urlString:NSString = NSString()
        for i in 0...self.count-1{
            //let urlEncodedData = self.subdata(in: NSMakeRange(i,1))
            let urlEncodedData = self.subdata(in: i..<i+1)
            //let urlChar = NSString(data: urlEncodedData, encoding: String.Encoding.ascii)
            //urlString = urlString.appending(urlChar as! String)
             let urlChar = NSString(data: urlEncodedData, encoding: String.Encoding.ascii.rawValue)
            urlString = urlString.appending(urlChar! as String) as NSString
        }

        return urlString as String
    }
    
    
    func asciiFormat() -> String{
        
        let asciiString = NSMutableString(string: "")
        
        if  self.count == 0 {
            return asciiString as String
        }
        
        for i in 0...self.count-1 {
            var byte : UInt8 = 0
            (self as NSData).getBytes(&byte, range: NSMakeRange(i,1))
            
            if byte >= 32 && byte < 127 {
                asciiString.append(String(format: "%c", byte))
            }
        }
        
        return asciiString as String
    }
    
    
    
    
    

    func hexRepresentationWithSpaces(_ spaces:Bool) ->NSString {
        
        var byteArray = [UInt8](repeating: 0x0, count: self.count)
        // The Test Data is moved into the 8bit Array.
        (self as NSData).getBytes(&byteArray, length:self.count)
        
        var hexBits = "" as String
        for value in byteArray {
            let newHex = NSString(format:"0x%2X", value) as String
            hexBits += newHex.replacingOccurrences(of: " ", with: "0", options: NSString.CompareOptions.caseInsensitive)
            if spaces {
                hexBits += " "
            }
        }
        return hexBits as NSString
    }
    
    func sFloatFromData()->Float?{
        
        
        //            int16_t exponent = (tempData & 0xF000) >> 12;
        //            int16_t mantissa = (int16_t)(tempData & 0x0FFF);
        //
        //            if (mantissa >= 0x0800)
        //            mantissa = -(0x1000 - mantissa);
        //            if (exponent >= 0x08)
        //            exponent = -(0x10 - exponent);
        //
        //            float tempValue = (float)(mantissa*pow(10, exponent));
        
        var tempValue:Int32 = 0
        if self.count >= 0+2
            {(self as NSData).getBytes(&tempValue, range: NSMakeRange(0, 2))}
        tempValue = Int32(CFSwapInt32LittleToHost(UInt32(tempValue)))
        
        var exponent :Int32 = (tempValue & 0xF000) >> 12
        var mantissa :Int32 = tempValue & 0x0FFF
        
        if mantissa >= 0x0800
        {
            mantissa = -(0x1000 - mantissa)
        }
        
        if (exponent >= 0x08) {
            exponent = -(0x10 - exponent)
        }
        
        let returnFloat:Float = Float(mantissa) * pow(Float(10),Float(exponent))
        return returnFloat;
        
    }

    /*func hexRepresentation()->String {
        
        let dataLength:Int = self.count
        let string = NSMutableString(capacity: dataLength*2)
        let dataBytes:UnsafeRawPointer = (self as NSData).bytes
        
        for idx in 0..<dataLength {
            //string.appendFormat("%02x", [UInt(dataBytes[idx])] )
            string.appendFormat("%02x", [UInt(dataBytes[idx])] as String )
        }
        
        return string as String
    }*/
    
    func hexRepresentation()->String {
        
        let dataLength:Int = self.count
        let string = NSMutableString(capacity: dataLength*2)
        let dataBytes:UnsafeRawPointer = (self as NSData).bytes
        
        for idx in 0..<dataLength {
            //dataBytes[idx].hexEncodedString()
            //string.appendFormat("%02x", UInt(dataBytes[idx]) )
            //string.appendFormat("%02x", dataBytes[idx])
            
            //string.appendFormat("%02x", [UInt(dataBytes?[idx])] as String )
            //string.appendFormat("%02x", UInt(dataBytes[idx]))
            
            //let hexString = String(format:"%02X", UInt(dataBytes[0]))
        }
        
        self.forEach { string.appendFormat("%02x", UInt($0)) }
        
        return string as String
    }
    
    func hex0xFormat()->String {
        
        let strings:NSMutableString = NSMutableString(string:self.description)
        strings.insert("0x", at: 1)
        
        return strings as String
    }

    
    func hexaDecimalConversion() -> String{
        
        let strings:NSMutableString = NSMutableString(string:self.description)
        strings.insert("0x", at: 1)
        let returnString = strings.replacingOccurrences(of: "<", with: "").replacingOccurrences(of: ">", with: "")
        return returnString
    }
    
    
    
    func getDescriptorFormat() -> String{
        
        let value = (self as NSData).bytes.bindMemory(to: UInt8.self, capacity: self.count).pointee
        
        switch value {
        
        case   0: return "Reserved For Future Use"
        case   1: return "Boolean"
        case   2: return "unsigned 2-bit integer"
        case   3: return "unsigned 4-bit integer"
        case   4: return "unsigned 8-bit integer"
        case   5: return "unsigned 12-bit integer"
        case   6: return "unsigned 16-bit integer"
        case   7: return "unsigned 24-bit integer"
        case   8: return "unsigned 32-bit integer"
        case   9: return "unsigned 48-bit integer"
        case   10: return "unsigned 64-bit integer"
        case   11: return "unsigned 128-bit integer"
        case   12: return "signed 8-bit integer"
        case   13: return "signed 12-bit integer"
        case   14: return "signed 16-bit integer"
        case   15: return "signed 24-bit integer"
        case   16: return "signed 32-bit integer"
        case   17: return "signed 48-bit integer"
        case   18: return "signed 64-bit integer"
        case   19: return "signed 128-bit integer"
        case   20: return "IEEE-754 32-bit floating point"
        case   21: return "IEEE-754 64-bit floating point"
        case   22: return "IEEE-11073 16-bit SFLOAT"
        case   23: return "IEEE-11073 32-bit FLOAT"
        case   24: return "IEEE-20601 format"
        case   25: return "UTF-8 string"
        case   26: return "UTF-16 string"
        case   27: return "Opaque Structure"
        case   28...255: return "Reserved For Future Use"
       
        default:
            return ""
        }
    }
    
    
    func getDescriptorNameSpace() -> String{
        
        let value = (self as NSData).bytes.bindMemory(to: UInt8.self, capacity: self.count).pointee
        
        switch value {
        case   1: return "Bluetooth SIG Assigned Numbers"
        case   2...255 : return "Reserved for future use"
        default:
            return ""
        }
    }
    
    
    func getDescriptorReportType() -> String{
        
        let value = (self as NSData).bytes.bindMemory(to: UInt8.self, capacity: self.count).pointee
        
        switch value {
        case    0: return "Reserved for future use"
        case    1: return "Input Report"
        case    2: return "Output report"
        case    3: return "Feature Report"
        case    4...255: return "Reserved for future use"
        default:
            return ""
        }
    }
    
    
    func getDescriptorBitMaskValue() -> String{
        
        let value = (self as NSData).bytes.bindMemory(to: UInt8.self, capacity: self.count).pointee
        
        switch value {
            
        case    0: return "Inactive"
        case    1: return "Active"
        case    2: return "Tri-state"
        case    3: return "Output-state"
        default:
            return ""
        }
    }
    
    
    
    
}

/*----------------------------------------------------------------------------------------------------------*/
// MARK: extension CBCharacteristic
/*----------------------------------------------------------------------------------------------------------*/


extension CBCharacteristic{
    
    
    func propertyStrings() -> [String]? {
        
        var characteristicProperties = [String]()
        
        if  (self.properties.rawValue & CBCharacteristicProperties.read.rawValue) != 0 {
            characteristicProperties.append("Read")
        }
        
        if (self.properties.rawValue & CBCharacteristicProperties.write.rawValue) != 0 || (self.properties.rawValue & CBCharacteristicProperties.writeWithoutResponse.rawValue) != 0 {
            characteristicProperties.append("Write")
        }
        
        if (self.properties.rawValue & CBCharacteristicProperties.notify.rawValue) != 0 {
            characteristicProperties.append("Notify")
        }
        
//        if (self.properties.rawValue & CBCharacteristicProperties.Broadcast.rawValue) != 0 {
//            characteristicProperties.append("Broadcast")
//        }
        
        if (self.properties.rawValue & CBCharacteristicProperties.indicate.rawValue) != 0 {
            characteristicProperties.append("Indicate")
        }
    
        return characteristicProperties
    }
    
    
    func hasNotifyProperty() -> Bool{
        
        if (self.properties.rawValue & CBCharacteristicProperties.notify.rawValue) != 0 {
            return true
        }
        return false
    }
    
    
    func hasIndicateProperty() -> Bool{
        
        if (self.properties.rawValue & CBCharacteristicProperties.indicate.rawValue) != 0 {
            return true
        }
        return false
    }
    
    
    
    
    
    func propertyDescription() -> String{
        
        
        var properties = self.propertyStrings()
        
        var description = ""
        
        if properties?.count != nil {
            
            description += properties!.first!
            properties!.removeFirst()
            
            
            if properties?.count == 0 {
                return description.uppercased()
            }
            
            for i in 0..<(properties!.count - 1) {
                
                description += ", " + properties![i]
            }
            
            description += " & " + properties!.last!
        }
        
        
        return description.uppercased()
        
    }
    
    
}


/*----------------------------------------------------------------------------------------------------------*/
// MARK: extension CBService
/*----------------------------------------------------------------------------------------------------------*/




extension CBService
{
    func isLinkLoss() -> Bool
    {
        if self.uuid.isEqual(CBUUID(bleService: BLEService.LinkLossService))
        {return true}
        
        return false
        
    }
    
    func isImmediateAlert() -> Bool
    {
        if self.uuid.isEqual(CBUUID(bleService: BLEService.ImmediateAlertService))
        {return true}
        
        return false
        
    }
    
    func isTxPower() -> Bool
    {
        if self.uuid.isEqual(CBUUID(bleService: BLEService.TransmissionPowerService))
        {return true}
        
        return false
        
    }
    
    func isThermoMeter() -> Bool
    {
        if self.uuid.isEqual(CBUUID(bleService: BLEService.HealthThermometerService))
        {return true}
        
        return false
        
    }
    
    
    func isDeviceInfo() -> Bool
    {
        if self.uuid.isEqual(CBUUID(bleService: BLEService.DeviceInformationService))
        {return true}
        
        return false
        
    }
    
    func isHeartRate() -> Bool
    {
        if self.uuid.isEqual(CBUUID(bleService: BLEService.HeartRateService))
        {return true}
        
        return false
        
    }
    
    func isBloodPressure() -> Bool
    {
        if self.uuid.isEqual(CBUUID(bleService: BLEService.BloodPressureService))
        {return true}
        
        return false
        
    }
    
    func isBattery() -> Bool
    {
        if self.uuid.isEqual(CBUUID(bleService: BLEService.BatteryService))
        {return true}
        
        return false
        
    }
    func isSerialPort()->Bool{
        
        if self.uuid.isEqual(CBUUID(bleService: BLEService.SerialPortService))
        {return true}
        
        return false
    }
    func isScanParam()->Bool{
        
        if self.uuid.isEqual(CBUUID(bleService: BLEService.ScanParameterService))
        {return true}
        
        return false
    }
    
    
    func isGattService() -> Bool{
        
        if self.uuid.isEqual(CBUUID(bleService: BLEService.GenericAccessService)){
            return true
        }
        return false
    }
}



/*----------------------------------------------------------------------------------------------------------*/
// MARK: extension CBPeripheral
/*----------------------------------------------------------------------------------------------------------*/


extension CBPeripheral{
    
    func servicesfilterOTAService()->[CBService]?{
        return self.services?.filter(){!($0.uuid.isEqual(CBUUID(bleService:BLEService.AtmelOTAService)))}
    }

    /*
    *
    * This method filter OTA service and add gattDB in the peripheral services
    */
    
    func servicesHandlingOTAAndGattDB() -> [CBService]{
        
        var services = [CBService]()
        
        if self.servicesfilterOTAService() != nil {
            services = self.servicesfilterOTAService()!
        }
        
        // GattDB is added in this way beacuse iOS framework filter Generic Access & Generic Attribute services from the CBPeripheral services
        
        let gattDBService = CBMutableService(type: CBUUID(bleService: BLEService.GenericAccessService), primary: true)
        services.append(gattDBService)
        
        return services
    }
    
    
    
    
   
    func isItHasLinlLoss() -> Bool
    {
        for aService in self.services!
        {
            if aService.isLinkLoss() == true
            {
                return true
            }
        }
        return false
    }
}

/*----------------------------------------------------------------------------------------------------------*/
// MARK: extension UInt8
/*----------------------------------------------------------------------------------------------------------*/

extension UInt8{
    func toBool()->Bool{
        if self == 0
        {return false}
        else
        {return true}
    }
    func toBoolFlag()->BoolFlag{
        if self == 0
        {return BoolFlag.no}
        else
        {return BoolFlag.yes}
    }
}
extension UInt16{
    func toBool()->Bool{
        if self == 0
        {return false}
        else
        {return true}
    }
}
/*----------------------------------------------------------------------------------------------------------*/
// MARK: extension CBUUID
/*----------------------------------------------------------------------------------------------------------*/

extension CBUUID{
    convenience init(bleService:BLEService) {
        self.init(string: bleService.description)
    }
    convenience init(blecharacteristic:BLECharacteristic) {
        self.init(string: blecharacteristic.description)
    }
    convenience init(bleDescriptor:BLEDescriptor) {
        self.init(string: bleDescriptor.rawValue)
    }
    class func getCBUUIDArrayFrom(bleProfilesArray:[BLEProfile]!)->Array<CBUUID>{
        
        var CBUUIDs:[CBUUID]=[CBUUID]()
        let profilesArray = ProfileServiceDetails.filter{bleProfilesArray.contains($0.profileName)}
        
        for profiles:ProfileDetail in profilesArray{
            
            CBUUIDs += profiles.profileServices.bleServicetoCBUUIDArray()
        }
        return CBUUIDs
    }
    
    class func getMandatoryCBUUIDArrayFrom(bleProfilesArray:[BLEProfile]!)->Array<CBUUID>{
        
        var CBUUIDs:[CBUUID]=[CBUUID]()
        let profilesArray = ProfileServiceDetails.filter{bleProfilesArray.contains($0.profileName)}
        
        for profiles:ProfileDetail in profilesArray{
            
            CBUUIDs += profiles.profileMandatoryServices.bleServicetoCBUUIDArray()
        }
        return CBUUIDs
    }

    func getServiceName() -> String
    {
        switch(self.uuidString)
        {
        case   "1800":  return  "Generic Access";
        case   "1801":  return  "Generic Attribute";
        case   "1802":  return  "Immediate Alert Service";
        case   "1803":  return  "Link Loss Service";
        case   "1804":  return  "Tx Power Service";
        case   "1805":  return  "Current Time Service";
        case   "1806":  return  "Reference Time Update Service";
        case   "1807":  return  "Next DST Change Service";
        case   "1808":  return  "Glucose";
        case   "1809":  return  "Health Thermometer Service";
        case   "180A":  return  "Device Information Service";
        case   "180B":  return  "Network Availability Service";
        case   "180C":  return  "Watchdog";
        case   "180D":  return  "Heart Rate Service";
        case   "180E":  return  "Phone Alert Status Service";
        case   "180F":  return  "Battery Service";
        case   "1810":  return  "Blood Pressure";
        case   "1811":  return  "Alert Notification Service";
        case   "1812":  return  "Human Interface Device";
        case   "1813":  return  "Scan Parameters";
        case   "1814":  return  "RUNNING SPEED AND CADENCE";
        case   "1815":  return  "Automation IO";
        case   "1816":  return  "Cycling Speed and Cadence";
        case   "1817":  return  "Pulse Oximeter";
        case   "1818":  return  "Cycling Power Service";
        case   "1819":  return  "Location and Navigation Service";
        case   "181A":  return  "Continous Glucose Measurement Service";
        case   "2A00":  return  "Device Name";
        case   "2A01":  return  "Appearance";
        case   "2A02":  return  "Peripheral Privacy Flag";
        case   "2A03":  return  "Reconnection Address";
        case   "2A04":  return  "Peripheral Preferred Connection Parameters";
        case   "2A05":  return  "Service Changed";
        case   "2A06":  return  "Alert Level";
        case   "2A07":  return  "Tx Power Level";
        case   "2A08":  return  "Date Time";
        case   "2A09":  return  "Day of Week";
        case   "2A0A":  return  "Day Date Time";
        case   "2A0B":  return  "Exact Time 100";
        case   "2A0C":  return  "Exact Time 256";
        case   "2A0D":  return  "DST Offset";
        case   "2A0E":  return  "Time Zone";
        case   "2A0F":  return  "Local Time Information";
        case   "2A10":  return  "Secondary Time Zone";
        case   "2A11":  return  "Time with DST";
        case   "2A12":  return  "Time Accuracy";
        case   "2A13":  return  "Time Source";
        case   "2A14":  return  "Reference Time Information";
        case   "2A15":  return  "Time Broadcast";
        case   "2A16":  return  "Time Update Control Point";
        case   "2A17":  return  "Time Update State";
        case   "2A18":  return  "Glucose Measurement";
        case   "2A19":  return  "Battery Level";
        case   "2A1A":  return  "Battery Power State";
        case   "2A1B":  return  "Battery Level State";
        case   "2A1C":  return  "Temperature Measurement";
        case   "2A1D":  return  "Temperature Type";
        case   "2A1E":  return  "Intermediate Temperature";
        case   "2A1F":  return  "Temperature in Celsius";
        case   "2A20":  return  "Temperature in Fahrenheit";
        case   "2A21":  return  "Measurement Interval";
        case   "2A22":  return  "Boot Keyboard Input Report";
        case   "2A23":  return  "System ID";
        case   "2A24":  return  "Model Number String";
        case   "2A25":  return  "Serial Number String";
        case   "2A26":  return  "Firmware Revision String";
        case   "2A27":  return  "Hardware Revision String";
        case   "2A28":  return  "Software Revision String";
        case   "2A29":  return  "Manufacturer Name String";
        case   "2A2A":  return  "IEEE 11073-20601 Regulatory Certification Data List";
        case   "2A2B":  return  "Current Time";
        case   "2A2C":  return  "Elevation";
        case   "2A2D":  return  "Latitude";
        case   "2A2E":  return  "Longitude";
        case   "2A2F":  return  "Position 2D";
        case   "2A30":  return  "Position 3D";
        case   "2A31":  return  "Scan Refresh";
        case   "2A32":  return  "Boot Keyboard Output Report";
        case   "2A33":  return  "Boot Mouse Input Report";
        case   "2A34":  return  "Glucose Measurement Context";
        case   "2A35":  return  "Blood Pressure Measurement";
        case   "2A36":  return  "Intermediate Cuff Pressure";
        case   "2A37":  return  "Heart Rate Measurement";
        case   "2A38":  return  "Body Sensor Location";
        case   "2A39":  return  "Heart Rate Control Point";
        case   "2A3A":  return  "Removable";
        case   "2A3B":  return  "Service Required";
        case   "2A3C":  return  "Scientific Temperature in Celsius";
        case   "2A3D":  return  "String";
        case   "2A3E":  return  "Network Availability";
        case   "2A3F":  return  "Alert Status";
        case   "2A40":  return  "Ringer Control Point";
        case   "2A41":  return  "Ringer Setting";
        case   "2A42":  return  "Alert Category ID Bit Mask";
        case   "2A43":  return  "Alert Category ID";
        case   "2A44":  return  "Alert Notification Control Point";
        case   "2A45":  return  "Unread Alert Status";
        case   "2A46":  return  "New Alert";
        case   "2A47":  return  "Supported New Alert Category";
        case   "2A48":  return  "Supported Unread Alert Category";
        case   "2A49":  return  "Blood Pressure Feature";
        case   "2A4A":  return  "HID Information";
        case   "2A4B":  return  "Report Map";
        case   "2A4C":  return  "HID Control Point";
        case   "2A4D":  return  "Report";
        case   "2A4E":  return  "Protocol Mode";
        case   "2A4F":  return  "Scan Interval Window";
        case   "2A50":  return  "PnP ID";
        case   "2A51":  return  "Glucose Features";
        case   "2A52":  return  "Record Access Control Point";
        case   "2A53":  return  "RSC Measurement";
        case   "2A54":  return  "RSC Feature";
        case   "2A55":  return  "SC Control Point";
        case   "2A56":  return  "Digital Input";
        case   "2A57":  return  "Digital Output";
        case   "2A58":  return  "Analog Input";
        case   "2A59":  return  "Analog Output";
        case   "2A5A":  return  "Aggregate Input";
        case   "2A5B":  return  "CSC Measurement";
        case   "2A5C":  return  "CSC Feature";
        case   "2A5D":  return  "Sensor Location";
        case   "2A5E":  return  "Pulse Oximetry Spot-check Measurement";
        case   "2A5F":  return  "Pulse Oximetry Continuous Measurement";
        case   "2A60":  return  "Pulse Oximetry Pulsatile Event";
        case   "2A61":  return  "Pulse Oximetry Features";
        case   "2A62":  return  "Pulse Oximetry Control Point";
        case   "2A63":  return  "Cycling Power Measurement Characteristic";
        case   "2A64":  return  "Cycling Power Vector Characteristic";
        case   "2A65":  return  "Cycling Power Feature Characteristic";
        case   "2A66":  return  "Cycling Power Control Point Characteristic";
        case   "2A67":  return  "Location and Speed Characteristic";
        case   "2A68":  return  "Navigation Characteristic";
        case   "2A69":  return  "Position Quality Characteristic";
        case   "2A6A":  return  "LN Feature Characteristic";
        case   "2A6B":  return  "LN Control Point Characteristic";
        case   "2A6C":  return  "CGM Measurement Characteristic";
        case   "2A6D":  return  "CGM Features Characteristic";
        case   "2A6E":  return  "CGM Status Characteristic";
        case   "2A6F":  return  "CGM Session Start Time Characteristic";
        case   "2A70":  return  "Application Security Point Characteristic";
        case   "2A71":  return  "CGM Specific Ops Control Point Characteristic";
        case "FD5ABBA0-3935-11E5-85A6-0002A5D5C51B": return "Custom Serial Chat"
        default:
            return  "Custom Service";
            
        }
    }
    
    
    func getCharacteristicName() -> String {
        
        switch self.uuidString {
    
            
        case    "2A7E":  return "Aerobic Heart Rate Lower Limit";
        case    "2A84":  return "Aerobic Heart Rate Upper Limit";
        case    "2A7F":  return "Aerobic Threshold";
        case    "2A80":  return "Age";
        case    "2A5A":  return "Aggregate";
        case    "2A43":  return "Alert Category ID";
        case    "2A42":  return "Alert Category ID Bit Mask";
        case    "2A06":  return "Alert Level";
        case    "2A3F":  return "Alert Status";
        case    "2AB3":  return "Altitude";
        case    "2A81":  return "Anaerobic Heart Rate Lower Limit";
        case    "2A82":  return "Anaerobic Heart Rate Upper Limit";
        case    "2A83":  return "Anaerobic Threshold";
        case    "2A58":  return "Analog";
        case    "2A73":  return "Apparent Wind Direction";
        case    "2A72":  return "Apparent Wind Speed";
        case    "2A01":  return "Appearance";
        case    "2AA3":  return "Barometric Pressure Trend";
        case    "2A19":  return "Battery Level";
        case    "2A49":  return "Blood Pressure Feature";
        case    "2A35":  return "Blood Pressure Measurement";
        case    "2A9B":  return "Body Composition Feature";
        case    "2A9C":  return "Body Composition Measurement";
        case    "2A38":  return "Body Sensor Location";
        case    "2AA4":  return "Bond Management Control Point";
        case    "2AA5":  return "Bond Management Feature";
        case    "2A22":  return "Boot Keyboard Input Report";
        case    "2A32":  return "Boot Keyboard Output Report";
        case    "2A33":  return "Boot Mouse Input Report";
        case    "2AA6":  return "Central Address Resolution";
        case    "2AA8":  return "CGM Feature";
        case    "2AA7":  return "CGM Measurement";
        case    "2AAB":  return "CGM Session Run Time";
        case    "2AAA":  return "CGM Session Start Time";
        case    "2AAC":  return "CGM Specific Ops Control Point";
        case    "2AA9":  return "CGM Status";
        case    "2A5C":  return "CSC Feature";
        case    "2A5B":  return "CSC Measurement";
        case    "2A2B":  return "Current Time";
        case    "2A66":  return "Cycling Power Control Point";
        case    "2A65":  return "Cycling Power Feature";
        case    "2A63":  return "Cycling Power Measurement";
        case    "2A64":  return "Cycling Power Vector";
        case    "2A99":  return "Database Change Increment";
        case    "2A85":  return "Date of Birth";
        case    "2A86":  return "Date of Threshold Assessment";
        case    "2A08":  return "Date Time";
        case    "2A0A":  return "Day Date Time";
        case    "2A09":  return "Day of Week";
        case    "2A7D":  return "Descriptor Value Changed";
        case    "2A00":  return "Device Name";
        case    "2A7B":  return "Dew Point";
        case    "2A56":  return "Digital";
        case    "2A0D":  return "DST Offset";
        case    "2A6C":  return "Elevation";
        case    "2A87":  return "Email Address";
        case    "2A7E":  return "Exact Time 256";
        case    "2A0C":  return "Fat Burn Heart Rate Lower Limit";
        case    "2A89":  return "Fat Burn Heart Rate Upper Limit";
        case    "2A26":  return "Firmware Revision String";
        case    "2A8A":  return "First Name";
        case    "2A8B":  return "Five Zone Heart Rate Limits";
        case    "2AB2":  return "Floor Number";
        case    "2A8C":  return "Gender";
        case    "2A51":  return "Glucose Feature";
        case    "2A18":  return "Glucose Measurement";
        case    "2A34":  return "Glucose Measurement Context";
        case    "2A74":  return "Gust Factor";
        case    "2A27":  return "Hardware Revision String";
        case    "2A39":  return "Heart Rate Control Point";
        case    "2A8D":  return "Heart Rate Max";
        case    "2A37":  return "Heart Rate Measurement";
        case    "2A7A":  return "Heat Index";
        case    "2A8E":  return "Height";
        case    "2A4C":  return "HID Control Point";
        case    "2A4A":  return "HID Information";
        case    "2A8F":  return "Hip Circumference";
        case    "2ABA":  return "HTTP Control Point";
        case    "2AB9":  return "HTTP Entity Body";
        case    "2AB7":  return "HTTP Headers";
        case    "2AB8":  return "HTTP Status Code";
        case    "2ABB":  return "HTTPS Security";
        case    "2A6F":  return "Humidity";
        case    "2A2A":  return "IEEE 11073-20601 Regulatory Certification Data List";
        case    "2AAD":  return "Indoor Positioning Configuration";
        case    "2A36":  return "Intermediate Cuff Pressure";
        case    "2A1E":  return "Intermediate Temperature";
        case    "2A77":  return "Irradiance";
        case    "2AA2":  return "Language";
        case    "2A90":  return "Last Name";
        case    "2AAE":  return "Latitude";
        case    "2A6B":  return "LN Control Point";
        case    "2A6A":  return "LN Feature";
        case    "2AB1":  return "Local East Coordinate";
        case    "2AB0":  return "Local North Coordinate";
        case    "2A0F":  return "Local Time Information";
        case    "2A67":  return "Location and Speed";
        case    "2AB5":  return "Location Name";
        case    "2AAF":  return "Longitude";
        case    "2A2C":  return "Magnetic Declination";
        case    "2AA0":  return "Magnetic Flux Density - 2D";
        case    "2AA1":  return "Magnetic Flux Density - 3D";
        case    "2A29":  return "Manufacturer Name String";
        case    "2A91":  return "Maximum Recommended Heart Rate";
        case    "2A21":  return "Measurement Interval";
        case    "2A24":  return "Model Number String";
        case    "2A68":  return "Navigation";
        case    "2A46":  return "New Alert";
        case    "2AC5":  return "Object Action Control Point";
        case    "2AC8":  return "Object Changed";
        case    "2AC1":  return "Object First-Created";
        case    "2AC3":  return "Object ID";
        case    "2AC2":  return "Object Last-Modified";
        case    "2AC6":  return "Object List Control Point";
        case    "2AC7":  return "Object List Filter";
        case    "2ABE":  return "Object Name";
        case    "2AC4":  return "Object Properties";
        case    "2AC0":  return "Object Size";
        case    "2ABF":  return "Object Type";
        case    "2ABD":  return "OTS Feature";
        case    "2A04":  return "Peripheral Preferred Connection Parameters";
        case    "2A02":  return "Peripheral Privacy Flag";
        case    "2A5F":  return "PLX Continuous Measurement";
        case    "2A60":  return "PLX Features";
        case    "2A5E":  return "PLX Spot-Check Measurement";
        case    "2A50":  return "PnP ID";
        case    "2A75":  return "Pollen Concentration";
        case    "2A69":  return "Position Quality";
        case    "2A6D":  return "Pressure";
        case    "2A4E":  return "Protocol Mode";
        case    "2A78":  return "Rainfall";
        case    "2A03":  return "Reconnection Address";
        case    "2A52":  return "Record Access Control Point";
        case    "2A14":  return "Reference Time Information";
        case    "2A4D":  return "Report";
        case    "2A4B":  return "Report Map";
        case    "2A92":  return "Resting Heart Rate";
        case    "2A40":  return "Ringer Control Point";
        case    "2A41":  return "Ringer Setting";
        case    "2A54":  return "RSC Feature";
        case    "2A53":  return "RSC Measurement";
        case    "2A55":  return "SC Control Point";
        case    "2A4F":  return "Scan Interval Window";
        case    "2A31":  return "Scan Refresh";
        case    "2A5D":  return "Sensor Location";
        case    "2A25":  return "Serial Number String";
        case    "2A05":  return "Service Changed";
        case    "2A28":  return "Software Revision String";
        case    "2A93":  return "Sport Type for Aerobic and Anaerobic Thresholds";
        case    "2A47":  return "Supported New Alert Category";
        case    "2A48":  return "Supported Unread Alert Category";
        case    "2A23":  return "System ID";
        case    "2ABC":  return "TDS Control Point";
        case    "2A6E":  return "Temperature";
        case    "2A1C":  return "Temperature Measurement";
        case    "2A1D":  return "Temperature Type";
        case    "2A94":  return "Three Zone Heart Rate Limits";
        case    "2A12":  return "Time Accuracy";
        case    "2A13":  return "Time Source";
        case    "2A16":  return "Time Update Control Point";
        case    "2A17":  return "Time Update State";
        case    "2A11":  return "Time with DST";
        case    "2A0E":  return "Time Zone";
        case    "2A71":  return "True Wind Direction";
        case    "2A70":  return "True Wind Speed";
        case    "2A95":  return "Two Zone Heart Rate Limit";
        case    "2A07":  return "Tx Power Level";
        case    "2AB4":  return "Uncertainty";
        case    "2A45":  return "Unread Alert Status";
        case    "2AB6":  return "URI";
        case    "2A9F":  return "User Control Point";
        case    "2A9A":  return "User Index";
        case    "2A76":  return "UV Index";
        case    "2A96":  return "VO2 Max";
        case    "2A97":  return "Waist Circumference";
        case    "2A98":  return "Weight";
        case    "2A9D":  return "Weight Measurement";
        case    "2A9E":  return "Weight Scale Feature";
        case    "2A79":  return "Wind Chill";
            
        default:
            return "Custom Characteristic"
        }
    }
    
    
    func getDescriptorName() -> String{
        
        switch self.uuidString {
        case    "2900":  return "Characteristic Extended Properties";
        case    "2901":  return "Characteristic User Description";
        case    "2902":  return "Client Characteristic Configuration";
        case    "2903":  return "Server Characteristic Configuration";
        case    "2904":  return "Characteristic Presentation Format";
        case    "2905":  return "Characteristic Aggregate Format";
        case    "2906":  return "Valid Range";
        case    "2907":  return "External Report Reference";
        case    "2908":  return "Report Reference";
        case    "2909":  return "Number of Digitals";
        case    "290A":  return "Value Trigger Setting";
        case    "290B":  return "Environmental Sensing Configuration";
        case    "290C":  return "Environmental Sensing Measurement";
        case    "290D":  return "Environmental Sensing Trigger Setting";
        case    "290E":  return "Time Trigger Setting";
        
        default:
            return "Custom Descriptor"
        }
    }
    
    
    
    
    func getServiceDescription() -> String
    {
        switch(self.uuidString)
        {
        case   "1800":  return  "the Generic access servicecontains generic information about the device. All available characteristics are read only.";
        case   "1801":  return  "Generic Attribute";
        case   "1802":  return  "This service exposes a control point to allow a peer device to cause the device to immediately alert.";
        case   "1803":  return  "This service defines behavior when a link is lost between two devices.";
        case   "1804":  return  "This service exposes a device’s current transmit power level when in a connection.";
        case   "1805":  return  "Current Time Service";
        case   "1806":  return  "Reference Time Update Service";
        case   "1807":  return  "Next DST Change Service";
        case   "1808":  return  "Glucose";
        case   "1809":  return  "This service exposes temperature and other data from a thermometer intended for healthcare and fitness applications.";
        case   "180A":  return  "The Device Information Service exposes manufacturer information about a device.";
        case   "180B":  return  "Network Availability Service";
        case   "180C":  return  "Watchdog";
        case   "180D":  return  "This service exposes heart rate and other data from a Heart Rate Sensor intended for fitness applications.";
        case   "180E":  return  "Phone Alert Status Service";
        case   "180F":  return  "The Battery Service exposes the state of a battery within a device.";
        case   "1810":  return  "This service exposes blood pressure and other data from a blood pressure monitor intended for healthcare applications.";
        case   "1811":  return  "Alert Notification Service";
        case   "1812":  return  "Human Interface Device";
        case   "1813":  return  "This service enables a GATT Client to store the LE scan parameters it is using on a GATT Server device ";
        case   "1814":  return  "RUNNING SPEED AND CADENCE";
        case   "1815":  return  "Automation IO";
        case   "1816":  return  "Cycling Speed and Cadence";
        case   "1817":  return  "Pulse Oximeter";
        case   "1818":  return  "Cycling Power Service";
        case   "1819":  return  "Location and Navigation Service";
        case   "181A":  return  "Continous Glucose Measurement Service";
        case   "2A00":  return  "Device Name";
        case   "2A01":  return  "Appearance";
        case   "2A02":  return  "Peripheral Privacy Flag";
        case   "2A03":  return  "Reconnection Address";
        case   "2A04":  return  "Peripheral Preferred Connection Parameters";
        case   "2A05":  return  "Service Changed";
        case   "2A06":  return  "Alert Level";
        case   "2A07":  return  "Tx Power Level";
        case   "2A08":  return  "Date Time";
        case   "2A09":  return  "Day of Week";
        case   "2A0A":  return  "Day Date Time";
        case   "2A0B":  return  "Exact Time 100";
        case   "2A0C":  return  "Exact Time 256";
        case   "2A0D":  return  "DST Offset";
        case   "2A0E":  return  "Time Zone";
        case   "2A0F":  return  "Local Time Information";
        case   "2A10":  return  "Secondary Time Zone";
        case   "2A11":  return  "Time with DST";
        case   "2A12":  return  "Time Accuracy";
        case   "2A13":  return  "Time Source";
        case   "2A14":  return  "Reference Time Information";
        case   "2A15":  return  "Time Broadcast";
        case   "2A16":  return  "Time Update Control Point";
        case   "2A17":  return  "Time Update State";
        case   "2A18":  return  "Glucose Measurement";
        case   "2A19":  return  "Battery Level";
        case   "2A1A":  return  "Battery Power State";
        case   "2A1B":  return  "Battery Level State";
        case   "2A1C":  return  "Temperature Measurement";
        case   "2A1D":  return  "Temperature Type";
        case   "2A1E":  return  "Intermediate Temperature";
        case   "2A1F":  return  "Temperature in Celsius";
        case   "2A20":  return  "Temperature in Fahrenheit";
        case   "2A21":  return  "Measurement Interval";
        case   "2A22":  return  "Boot Keyboard Input Report";
        case   "2A23":  return  "System ID";
        case   "2A24":  return  "Model Number String";
        case   "2A25":  return  "Serial Number String";
        case   "2A26":  return  "Firmware Revision String";
        case   "2A27":  return  "Hardware Revision String";
        case   "2A28":  return  "Software Revision String";
        case   "2A29":  return  "Manufacturer Name String";
        case   "2A2A":  return  "IEEE 11073-20601 Regulatory Certification Data List";
        case   "2A2B":  return  "Current Time";
        case   "2A2C":  return  "Elevation";
        case   "2A2D":  return  "Latitude";
        case   "2A2E":  return  "Longitude";
        case   "2A2F":  return  "Position 2D";
        case   "2A30":  return  "Position 3D";
        case   "2A31":  return  "Scan Refresh";
        case   "2A32":  return  "Boot Keyboard Output Report";
        case   "2A33":  return  "Boot Mouse Input Report";
        case   "2A34":  return  "Glucose Measurement Context";
        case   "2A35":  return  "Blood Pressure Measurement";
        case   "2A36":  return  "Intermediate Cuff Pressure";
        case   "2A37":  return  "Heart Rate Measurement";
        case   "2A38":  return  "Body Sensor Location";
        case   "2A39":  return  "Heart Rate Control Point";
        case   "2A3A":  return  "Removable";
        case   "2A3B":  return  "Service Required";
        case   "2A3C":  return  "Scientific Temperature in Celsius";
        case   "2A3D":  return  "String";
        case   "2A3E":  return  "Network Availability";
        case   "2A3F":  return  "Alert Status";
        case   "2A40":  return  "Ringer Control Point";
        case   "2A41":  return  "Ringer Setting";
        case   "2A42":  return  "Alert Category ID Bit Mask";
        case   "2A43":  return  "Alert Category ID";
        case   "2A44":  return  "Alert Notification Control Point";
        case   "2A45":  return  "Unread Alert Status";
        case   "2A46":  return  "New Alert";
        case   "2A47":  return  "Supported New Alert Category";
        case   "2A48":  return  "Supported Unread Alert Category";
        case   "2A49":  return  "Blood Pressure Feature";
        case   "2A4A":  return  "HID Information";
        case   "2A4B":  return  "Report Map";
        case   "2A4C":  return  "HID Control Point";
        case   "2A4D":  return  "Report";
        case   "2A4E":  return  "Protocol Mode";
        case   "2A4F":  return  "Scan Interval Window";
        case   "2A50":  return  "PnP ID";
        case   "2A51":  return  "Glucose Features";
        case   "2A52":  return  "Record Access Control Point";
        case   "2A53":  return  "RSC Measurement";
        case   "2A54":  return  "RSC Feature";
        case   "2A55":  return  "SC Control Point";
        case   "2A56":  return  "Digital Input";
        case   "2A57":  return  "Digital Output";
        case   "2A58":  return  "Analog Input";
        case   "2A59":  return  "Analog Output";
        case   "2A5A":  return  "Aggregate Input";
        case   "2A5B":  return  "CSC Measurement";
        case   "2A5C":  return  "CSC Feature";
        case   "2A5D":  return  "Sensor Location";
        case   "2A5E":  return  "Pulse Oximetry Spot-check Measurement";
        case   "2A5F":  return  "Pulse Oximetry Continuous Measurement";
        case   "2A60":  return  "Pulse Oximetry Pulsatile Event";
        case   "2A61":  return  "Pulse Oximetry Features";
        case   "2A62":  return  "Pulse Oximetry Control Point";
        case   "2A63":  return  "Cycling Power Measurement Characteristic";
        case   "2A64":  return  "Cycling Power Vector Characteristic";
        case   "2A65":  return  "Cycling Power Feature Characteristic";
        case   "2A66":  return  "Cycling Power Control Point Characteristic";
        case   "2A67":  return  "Location and Speed Characteristic";
        case   "2A68":  return  "Navigation Characteristic";
        case   "2A69":  return  "Position Quality Characteristic";
        case   "2A6A":  return  "LN Feature Characteristic";
        case   "2A6B":  return  "LN Control Point Characteristic";
        case   "2A6C":  return  "CGM Measurement Characteristic";
        case   "2A6D":  return  "CGM Features Characteristic";
        case   "2A6E":  return  "CGM Status Characteristic";
        case   "2A6F":  return  "CGM Session Start Time Characteristic";
        case   "2A70":  return  "Application Security Point Characteristic";
        case   "2A71":  return  "CGM Specific Ops Control Point Characteristic";
        case "FD5ABBA0-3935-11E5-85A6-0002A5D5C51B": return "This service exposes the characteristics to transfer serial data "
            
        default:
            return  "Custom Profile";
            
        }
    }

    
}

/*----------------------------------------------------------------------------------------------------------*/
// MARK: extension CBDescriptor
/*----------------------------------------------------------------------------------------------------------*/


extension CBDescriptor{
    
    
    func isCharacteristicExtendedProperties() -> Bool {
        
        if self.uuid.isEqual(CBUUID(bleDescriptor: BLEDescriptor.CharacteristicExtendedProperties)) {
            return true
        }
        return false
    }
    
    
    func isCharactristicUserDescription() -> Bool {
        
        if self.uuid.isEqual(CBUUID(bleDescriptor: BLEDescriptor.CharacteristicUserDescription)) {
            return true
        }
        return false
    }
    
    func isClientCharacteristicConfiguration() -> Bool{
        
        if self.uuid.isEqual(CBUUID(bleDescriptor: BLEDescriptor.ClientCharacteristicConfiguration)) {
            return true
        }
        return false
    }
    
    func isServerCharacteristicConfiguration() -> Bool{
        
        if self.uuid.isEqual(CBUUID(bleDescriptor: BLEDescriptor.ServerCharacteristicConfiguration)) {
            return true
        }
        return false
    }
    
    
    func isCharacteristicPresentationFormat() -> Bool{
        
        if  self.uuid.isEqual(CBUUID(bleDescriptor: BLEDescriptor.CharacteristicPresentationFormat)) {
            return true
        }
        return false
    }
    
    
    func isCharacteristicAggregateFormat() -> Bool{
        
        if self.uuid.isEqual(CBUUID(bleDescriptor: BLEDescriptor.CharacteristicAggregateFormat)) {
            return true
        }
        return false
    }
    
    
    
    func isReportReference() -> Bool{
        
        if  self.uuid.isEqual(CBUUID(bleDescriptor: BLEDescriptor.ExternalReportReference)) {
            return true
        }
        return false
    }
    
    
    func isNumberOfDigitals() -> Bool{
        
        if self.uuid.isEqual(CBUUID(bleDescriptor: BLEDescriptor.NumberOfDigitals)) {
            return true
        }
        return false
    }
    
    func isValueTriggerSetting() -> Bool{
        
        if self.uuid.isEqual(CBUUID(bleDescriptor: BLEDescriptor.ValueTriggerSetting)) {
            return true
        }
        return false
    }
    
    func isTimeTriggerSetting() -> Bool{
        
        if self.uuid.isEqual(CBUUID(bleDescriptor: BLEDescriptor.TimeTriggerSetting)) {
            return true
        }
        return false
    }
    
}




/*----------------------------------------------------------------------------------------------------------*/
// MARK: extension CBCentralManager
/*----------------------------------------------------------------------------------------------------------*/

extension CBCentralManager{
    func scanForPeripheralsForProfiles(_ profiles:[BLEProfile]?, options: [String : AnyObject]!){
        
        if let _ = profiles
        {
            
            self.scanForPeripherals(withServices: CBUUID.getMandatoryCBUUIDArrayFrom(bleProfilesArray:profiles), options: options)
        }
        else
        {
            self.scanForPeripherals(withServices: nil, options: options)
        }
        
    }
}


/*----------------------------------------------------------------------------------------------------------*/
// MARK: extension Int
/*----------------------------------------------------------------------------------------------------------*/


extension Int {
    func format(_ f: String) -> String {
        return String(format: "%\(f)d", self)
    }
}



/*----------------------------------------------------------------------------------------------------------*/
// MARK: extension Int
/*----------------------------------------------------------------------------------------------------------*/



extension Date{
    
    static func currentDate() -> String{
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        return dateFormatter.string(from: Date())
    }
    
    static func currentTime() -> String{
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .medium
        return dateFormatter.string(from: Date())
    }
}





















