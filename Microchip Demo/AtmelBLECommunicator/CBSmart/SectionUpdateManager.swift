/***************************************************************************
 
 SectionUpdateManager.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import UIKit

class SectionEndResponseFrame:OTAResponseFrame{
    
    
    var sectionID:OTAUImageSectionID!
    
    override init(withData:Data){
        
        super.init(withData:withData)
        
        
        if command != nil {
            switch(command!){
                
            case OTAUCommand.image_SECTION_END_ERROR:
                responseType = OTAUResponseType.failure
                
                if withData.count >= 4
                //{sectionID = OTAUImageSectionID(rawValue:(withData.subdata(in: NSMakeRange(3, 1)) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(with: NSMakeRange(3, 1)).count).pointee)}
                {sectionID = OTAUImageSectionID(rawValue:(withData.subdata(in: 3..<1) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(in: 3..<1).count).pointee)}
                
                break
                
            case OTAUCommand.image_SECTION_END_NOTIFY_RESP:
                
                responseType = OTAUResponseType.success
                
                if withData.count >= 4
                //{sectionID = OTAUImageSectionID(rawValue:(withData.subdata(in: NSMakeRange(3, 1)) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(with: NSMakeRange(3, 1)).count).pointee)}
                {sectionID = OTAUImageSectionID(rawValue:(withData.subdata(in: 3..<1) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(in: 3..<1).count).pointee)}
                
                break
                
            default:
                responseType = OTAUResponseType.invalid
                break
            }
            
        }
        
    }
    
}

class SectionEndRequestFrame:OTARequestFrame {
    
    var sectionID:OTAUImageSectionID!
   
    
    init(sectionId:OTAUImageSectionID!) {
        super.init()
        
        sectionID = sectionId
        
        length  = OTAFrameLength.sectionEndRequest.rawValue
        command = OTAUCommand.image_SECTION_END_NOTIFY_REQUEST
        
    }
    
    func dataFromFrame()->Data?{
        
        if  sectionID == nil
        {return nil}
        
        
        //16 bitlength data
        
        var dataLength:UInt16 = length!
        let returnFrameData = NSMutableData(bytes: &dataLength, length: MemoryLayout.size(ofValue: dataLength))
        
        //8 bit command data
        
        var commandData:UInt8 = command!.rawValue
        returnFrameData.append(&commandData, length: MemoryLayout.size(ofValue: commandData))
        
        
        //8 bit sectionID  data
        
        var sectID:UInt8 = sectionID.rawValue
        returnFrameData.append(&sectID, length: MemoryLayout.size(ofValue: sectID))
        
       
        
        return returnFrameData as Data
    }
    
}

class SectionUpdateManager: OTATask {
    
    var imageFile:String!
    var sucessBlock:(()->())? = nil
    var failBlock:((_ withError:OTAErrorType,_ errorString:String)->())? = nil
    
    var updateInfo:ImageInfoNotificationResponseFrame!
    
    var currentOTAMode:OTAMode?

    var currentSectionID:OTAUImageSectionID!
    var currentPageNo:UInt16!
    var currentBlockNo:UInt8!
    
    var currentSection:Section?
    var pageUpdateManager:PageUpdateManager?
    var blockUpdateManager:BlockUpdateManager?
    
    init(interface:AtmelOTAInterface, withImageInfoResponse:ImageInfoNotificationResponseFrame){
        
        super.init()
        otaInterface = interface
        otaInterface.delegate = self
        updateInfo = withImageInfoResponse
        currentOTAMode = withImageInfoResponse.currentOTAMode
        currentSectionID = withImageInfoResponse.offsetSectionID
        currentPageNo = withImageInfoResponse.offsetPageNo
        currentBlockNo = withImageInfoResponse.offsetBlockNo
    }
    
    func start(success sucess:@escaping ()->(),fail:@escaping (_ withError:OTAErrorType,_ errorString:String)->()){
        
        currentSection = Section(fromImage: otaInterface.image!, sectionType: currentSectionID,pagesize:updateInfo.pageSize!)
        
        if  currentSection?.sectionData != nil {
            
            otaInterface.updatedProgressInfo("Starting  \(currentSection!.sectionID.description) image download..", progress:0)
            sucessBlock = sucess
            failBlock = fail
            writeSectionData()
        }
            
        else{
            fail(OTAErrorType.localError, "Section data  from image file Could not be obtained")
        }
    }
    
    
    func writeSectionData(){
        
        switch(currentOTAMode!){
            
        case OTAMode.pageMode:
            writeSectionDataPageMode()
            break
            
        case OTAMode.blockMode:
            writeSectionDataBlockMode()
            break
        }
        
    }
    
    func writeSectionDataPageMode(){
        
        print("OTA TASK: Write Section")

        pageUpdateManager = PageUpdateManager(interface: otaInterface, currentPageNumber: currentPageNo, currentSec: currentSection!,withPageSize: updateInfo.pageSize)
        pageUpdateManager?.start(
            
            success:
            { () -> () in
                
                self.sendSectionEndFrame()
                
            },
            fail:
            { (withError:OTAErrorType,errorString:String) -> () in
                
                self.failBlock!(withError,errorString)
            }
        )
    }
    
    func writeSectionDataBlockMode(){
        
        print("OTA TASK: Write Section")

        blockUpdateManager = BlockUpdateManager(interface: otaInterface, currentPageNumber: currentPageNo, currentBlockNumber: currentBlockNo, currentSec: currentSection!, withPageSize: updateInfo.pageSize, withBlockSize: updateInfo.blockSize)
        blockUpdateManager?.start(
            
            success:
            { () -> () in
                
                self.sendSectionEndFrame()
                
            },
            fail:
            { (withError:OTAErrorType,errorString:String) -> () in
                
                self.failBlock!(withError,errorString)
            }
        )
    }
    
    func sendSectionEndFrame(){
        
        print("OTA TASK: Section End")

        otaInterface.delegate = self
        let sectionEndReqFrame = SectionEndRequestFrame(sectionId: currentSection?.sectionID)
        
        if let requestData = sectionEndReqFrame.dataFromFrame(){
            otaInterface.sendFrameData(requestData)
        }
        else{
            
            failBlock!(OTAErrorType.localError, "Section end data  could not be obtained")
        }
    }
    
    
}

extension SectionUpdateManager:AtmelOTAInterfaceDelegate{
    
    func receivedResponseFrameData(_ responseData:Data){
        
        if otaShouldAbort()
        {return}
        
        let respFrame = SectionEndResponseFrame(withData: responseData)
        
        if  respFrame.responseType == OTAUResponseType.success{
            
            //Write next section
            //Write current ota info
            
//            otaInterface..updatedProgressInfo("Finished writing  \(currentSection!.sectionID.description) Section", progress:-1)
            
            print("Finished writing  \(currentSection!.sectionID.description) Section")
            
            currentSection = currentSection?.nextSection()
            
            if currentSection != nil{
                
                currentPageNo = 0
                currentBlockNo = 0
                writeSectionData()
            }
                // call success if there are no sections remaining in image data
                
            else{
                sucessBlock!()
            }
            
            
        }
        else if  respFrame.responseType == OTAUResponseType.failure{
            print("Section End  Fail")

            failBlock!(OTAErrorType.communicationError,"Section End Error ( \(String(describing: respFrame.errorCode)) )")
        }
        
    }
    
    
}

