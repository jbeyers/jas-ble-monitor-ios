/***************************************************************************
 
 GattDBInterface.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import UIKit
import CoreBluetooth

protocol GattDBInterfaceDelegate {
    
    func sourceDeviceDidResondForReadRequestForCharacteristic(_ characteristic: CBCharacteristic, withRespsonse gattReadResponse: GattResponse)
    func sourceDeviceDidResondForWriteRequestForcharacteristic(_ characteristic: CBCharacteristic, withRespsonse gattWriteResponse: GattResponse)
}


protocol GattDBInterfaceDescriptorDelegate {
    
    func sourceDeviceDidDiscoverDescriptorsForCharacteristic(_ characteristic: CBCharacteristic, withError error: NSError?)
    func sourceDeviceDidRespondForReadRequestForDescriptor(_ descriptor: CBDescriptor, withResponse readresponse : GattDescriptorResponse)
    func sourceDeviceDidRespondForWriteRequestForDescriptor(_ descriptor: CBDescriptor, withResponse wrtieResponse : GattDescriptorResponse)
}




struct GattResponse {
    
    var characteristic  : CBCharacteristic!
    var hexValue        : String?
    var asciiValue      : String?
    var date            : String?
    var time            : String?
    var error           : NSError!
}



struct GattDescriptorResponse {
    
    var characteristic      : CBCharacteristic!
    var descriptor          : CBDescriptor!
    var error               : NSError!
    var descriptorHexValue  : String?
    var descriptorValue     : String?
}







class GattDBInterface: NSObject {
    

    var sourcePeripheral : BLESourceDevice?
    var gattOperationsDelegate : GattDBInterfaceDelegate?
    var gattDescriptorOperationsDelegate : GattDBInterfaceDescriptorDelegate?
    
    var writeResponse : GattResponse?

    
    
    
    init(sourceDevice : BLESourceDevice){
        
        super.init()
        
        self.sourcePeripheral = sourceDevice
        self.sourcePeripheral?.operationsDelegate = self
        self.sourcePeripheral?.descriptorDelegate = self
    }
    
    //MARK:- Characteristic operations
    
    func readValueForCharacteristic(_ characteristic : CBCharacteristic?){
        
        self.sourcePeripheral?.readValueForCharacteristic(characteristic)
    }
    
    
    func writeValueForCharacteristic(_ characteristic : CBCharacteristic?, withValue hexValue : String){
        
        let dataToWrite = hexValue.validateHexString().dataFromHexadecimalString()
        
        if dataToWrite != nil && characteristic != nil {
            
            writeResponse = GattResponse()
            writeResponse?.characteristic = characteristic
            writeResponse?.hexValue = dataToWrite?.hexaDecimalConversion()
            writeResponse?.asciiValue = dataToWrite?.asciiFormat()
            
            if (characteristic!.properties.rawValue & CBCharacteristicProperties.write.rawValue) != 0 {
                self.sourcePeripheral?.writeValue(dataToWrite, forCharacteristic: characteristic, withAcknowledgement: true)
            }
            else{
                self.sourcePeripheral?.writeValue(dataToWrite, forCharacteristic: characteristic, withAcknowledgement: false)
                
                if gattOperationsDelegate != nil {
                    
                    writeResponse?.error = nil
                    writeResponse?.date = Date.currentDate()
                    writeResponse?.time = Date.currentTime()
                    gattOperationsDelegate?.sourceDeviceDidResondForWriteRequestForcharacteristic(characteristic!, withRespsonse: writeResponse!)
                }
            }
        }
    }
    
    
    func startNotificationsForcharacteristic(_ characteristic : CBCharacteristic?){
        
        if characteristic?.isNotifying == false{
            self.sourcePeripheral?.setNotifyValue(true, forCharacteristic: characteristic)
        }
    }
    
    func stopNotificationsForcharacteristic(_ characteristic : CBCharacteristic?){
        
        if characteristic?.isNotifying == true {
            self.sourcePeripheral?.setNotifyValue(false, forCharacteristic: characteristic)
        }
    }

    //MARK:- Descriptor operations
    
    func discoverDescriptorsForCharacteristic(_ characteristic : CBCharacteristic?){
        
        self.sourcePeripheral?.getDescriptorsForCharacteristic(characteristic)
    }
    
    
    func readValueForDescriptor(_ descriptor : CBDescriptor?){
        
        self.sourcePeripheral?.readValueForDescriptor(descriptor)
    }
    
    
    
}



extension GattDBInterface : BLESourceDeviceOperationsDelegate{
    
    
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice, didUpdateValueForCharacteristic characteristic: CBCharacteristic!, error: NSError!){
        
        if gattOperationsDelegate != nil{
            
            var readResponse = GattResponse()

            readResponse.characteristic = characteristic
            readResponse.error = error
            
            if error == nil {
                
                readResponse.hexValue = characteristic.value?.hexaDecimalConversion()
                readResponse.asciiValue = characteristic.value?.asciiFormat()
                readResponse.date = Date.currentDate()
                readResponse.time = Date.currentTime()
            }
            
            gattOperationsDelegate?.sourceDeviceDidResondForReadRequestForCharacteristic(characteristic!, withRespsonse: readResponse)
        }
    }
    
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice,didWriteValueForCharacteristic characteristic: CBCharacteristic!, error: NSError!){
        
        if gattOperationsDelegate != nil && writeResponse != nil{
            
            writeResponse?.error = error
            writeResponse?.date = Date.currentDate()
            writeResponse?.time = Date.currentTime()
            gattOperationsDelegate?.sourceDeviceDidResondForWriteRequestForcharacteristic(characteristic, withRespsonse: writeResponse!)
            
        }
        
        
    }
    
    
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice,didUpdateNotificationStateForCharacteristic characteristic: CBCharacteristic!, error: NSError!){
        
    }

    
}



extension GattDBInterface : BLESourceDeviceDescriptorOpertationsDelegate{
    
    
    func bleSourceDevice(_ bleSorceDevice:BLESourceDevice, didDiscoverDescriptorsForCharacteristic characteristic: CBCharacteristic, error: NSError?){
        
        if gattDescriptorOperationsDelegate != nil {
            
            gattDescriptorOperationsDelegate?.sourceDeviceDidDiscoverDescriptorsForCharacteristic(characteristic, withError: error)
        }
    }
    
    func bleSourceDevice(_ bleSorceDevice:BLESourceDevice, didUpdateValueForDescriptor descriptor: CBDescriptor, error: NSError?){
        
        if gattDescriptorOperationsDelegate != nil {
            
            var readResponse = GattDescriptorResponse()
            readResponse.descriptor = descriptor
            readResponse.characteristic = descriptor.characteristic
            readResponse.descriptorHexValue = "\(descriptor.value!)"
            readResponse.descriptorValue = parseDataFromDescriptor(descriptor)
            
            gattDescriptorOperationsDelegate?.sourceDeviceDidRespondForReadRequestForDescriptor(descriptor
                , withResponse: readResponse)
        }
        
    }
    
    func bleSourceDevice(_ bleSorceDevice:BLESourceDevice, didWriteValueForDescriptor descriptor: CBDescriptor, error: NSError?){
        
        
    }
}



extension GattDBInterface{
    
    fileprivate func parseDataFromDescriptor(_ descriptor : CBDescriptor) -> String{
        
        if descriptor.isCharacteristicExtendedProperties() {
            return getValueForCharacteristicPropertiesWithDescriptor(descriptor)
        }
        else if descriptor.isCharactristicUserDescription(){
            return getValueForcharacteristicUserDescriptionWithDescriptor(descriptor)
        }
        else if descriptor.isClientCharacteristicConfiguration(){
            return getValueForClientCharacteristicConfigurationWithDescriptor(descriptor)
        }
        else if descriptor.isServerCharacteristicConfiguration(){
            return getValueForServerCharacteristicConfigurationWithDescriptor(descriptor)
        }
        else if descriptor.isCharacteristicPresentationFormat(){
            return getValueForCharacteristicPresentationformatWithDescriptor(descriptor)
        }
        else if descriptor.isCharacteristicAggregateFormat(){
            return getValueForCharacteristicAggregateFormatFromDescriptor(descriptor)
        }
        else if descriptor.isReportReference(){
            return getValueForReportreferenceFromDescriptor(descriptor)
        }
        else if descriptor.isNumberOfDigitals(){
            return getValueForNumberOfDigitalsFromDescriptor(descriptor)
        }
        else if descriptor.isTimeTriggerSetting(){
            return getValueForValueTriggerSettingFromDescriptor(descriptor)
        }
        else if descriptor.isValueTriggerSetting(){
            return getValueForTimeTriggerSettingFromDescriptor(descriptor)
        }
        
        return ""
    }
    



fileprivate func getValueForCharacteristicPropertiesWithDescriptor(_ descriptor :  CBDescriptor) -> String{
    
    var returnString = ""
    
    let descriptorValue = descriptor.value as? NSNumber
    
    if descriptorValue != nil {
        
        switch descriptorValue!.intValue {
        case 0:
            returnString += "Reliable Write disabled" + "\n" + "Writable Auxiliaries disabled"
        case 1:
            returnString += "Reliable Write enabled"
        case 2:
            returnString += "Writable Auxiliaries enabled"
        case 3:
            returnString += "Reliable Write enabled" + "\n" + "Writable Auxiliaries enabled"
        default: break
            
        }
    }
    return returnString
}


    fileprivate func getValueForcharacteristicUserDescriptionWithDescriptor(_ descriptor : CBDescriptor) -> String{
        
        let descriptorValue = descriptor.value as? String
        
        if  descriptorValue != nil {
            return descriptorValue!
        }
        return ""
    }
    
    
    fileprivate func getValueForClientCharacteristicConfigurationWithDescriptor(_ descriptor : CBDescriptor) -> String{
        
        var returnString = ""
        
        let descriptorValue = descriptor.value as? NSNumber
        
        if descriptorValue != nil {
            
            switch descriptorValue!.intValue {
            case 0:
                returnString += "Notifications disabled" + "\n" + "Indications disabled"
            case 1:
                returnString += "Notifications enabled"
            case 2:
                returnString += "Indications enabled"
            case 3:
                returnString += "Notifications enabled" + "\n" + "Indications enabled"
            default: break
                
            }
        }
        return returnString
    }
    
    
    fileprivate func getValueForServerCharacteristicConfigurationWithDescriptor(_ descriptor : CBDescriptor) -> String{
        
        var returnString = ""
        
        let descriptorValue = descriptor.value as? NSNumber
        
        if descriptorValue != nil {
            
            switch descriptorValue!.intValue {
            case 0:
                returnString += "Broadcasts disabled"
            case 1:
                returnString += "Broadcasts enabled"
            default: break
            }
        }
        return returnString
    }
    
    
    
    
    fileprivate func getValueForCharacteristicPresentationformatWithDescriptor(_ descriptor : CBDescriptor) -> String{
        
        var returnString = ""
        
        
        guard let descriptorValue = descriptor.value as? Data else{
            
            return returnString
        }
        
        var dataPointer = 0
        
        //let formatString = descriptorValue.subdata(in: NSMakeRange(dataPointer, 1)).getDescriptorFormat()
        let formatString = descriptorValue.subdata(in: dataPointer..<1).getDescriptorFormat()
        
        if formatString != "" {
            returnString += "Format " + formatString
        }
        
        dataPointer += 1
        
        //let exponent = (descriptorValue.subdata(in: NSMakeRange(dataPointer, 1)) as NSData).bytes.bindMemory(to: Int8.self, capacity: descriptorValue.subdata(with: NSMakeRange(dataPointer, 1)).count).pointee
        let exponent = (descriptorValue.subdata(in: dataPointer..<dataPointer+1) as NSData).bytes.bindMemory(to: Int8.self, capacity: descriptorValue.subdata(in: dataPointer..<dataPointer+1).count).pointee
        returnString += "\n" + "Exponent " + "\(exponent)"
        
        dataPointer += 1
        
        
        //let unit = (descriptorValue.subdata(in: NSMakeRange(dataPointer, 2)) as NSData).bytes.bindMemory(to: UInt16.self, capacity: descriptorValue.subdata(with: NSMakeRange(dataPointer, 2)).count).pointee
        let unit = (descriptorValue.subdata(in: dataPointer..<dataPointer+2) as NSData).bytes.bindMemory(to: UInt16.self, capacity: descriptorValue.subdata(in: dataPointer..<dataPointer+2).count).pointee
        returnString += "\n" + "Unit " + "\(unit)"
        
        dataPointer += 2
        
        
        //let nameSpace = descriptorValue.subdata(in: NSMakeRange(dataPointer, 1)).getDescriptorNameSpace()
        let nameSpace = descriptorValue.subdata(in: dataPointer..<dataPointer+1).getDescriptorNameSpace()
        
        if nameSpace != "" {
            returnString += "\n" + "NameSpace " + nameSpace
        }
        
        dataPointer += 1
        
        //let descriptionValue = (descriptorValue.subdata(in: NSMakeRange(dataPointer, 2)) as NSData).bytes.bindMemory(to: UInt16.self, capacity: descriptorValue.subdata(with: NSMakeRange(dataPointer, 2)).count).pointee
        let descriptionValue = (descriptorValue.subdata(in: dataPointer..<dataPointer+2) as NSData).bytes.bindMemory(to: UInt16.self, capacity: descriptorValue.subdata(in: dataPointer..<dataPointer+2).count).pointee
        returnString += "\n" + "Description " + "\(descriptionValue)"
        
        
        return returnString
        
    }
    
    
    
    fileprivate func getValueForCharacteristicAggregateFormatFromDescriptor(_ descriptor : CBDescriptor) -> String{
        
        guard let descriptorValue = descriptor.value as? Data else{
            return ""
        }
        
        let listOfHandlesValue =  (descriptorValue as NSData).bytes.bindMemory(to: UInt16.self, capacity: descriptorValue.count).pointee
        return "List of Handles \(listOfHandlesValue)"
    }
    
    
    fileprivate func getValueForReportreferenceFromDescriptor(_ descriptor : CBDescriptor) -> String{
        
        var returnString = ""

        guard let descriptorValue = descriptor.value as? Data else{
            return returnString
        }
        
        //let reportID = (descriptorValue.subdata(in: NSMakeRange(0, 1)) as NSData).bytes.bindMemory(to: UInt8.self, capacity: descriptorValue.subdata(with: NSMakeRange(0, 1)).count).pointee
        let reportID = (descriptorValue.subdata(in: 0..<1) as NSData).bytes.bindMemory(to: UInt8.self, capacity: descriptorValue.subdata(in: 0..<1).count).pointee
        returnString += "ReportID " + "\(reportID)"
        
        //let reportType = descriptorValue.subdata(in: NSMakeRange(1, 1)).getDescriptorReportType()
        let reportType = descriptorValue.subdata(in: 1..<2).getDescriptorReportType()
        if reportType != "" {
            returnString += "\n" + "Report Type " + reportType
        }
        return returnString
    }
    
    
    fileprivate func getValueForNumberOfDigitalsFromDescriptor(_ descriptor : CBDescriptor) -> String{
        
        guard let descriptorValue = descriptor.value as? Data else{
            return ""
        }
        
        let digitalsValue = (descriptorValue as NSData).bytes.bindMemory(to: UInt8.self, capacity: descriptorValue.count).pointee
        return "No of Digitals \(digitalsValue)"
    }
    
    
    fileprivate func getValueForValueTriggerSettingFromDescriptor(_ descriptor : CBDescriptor) -> String{
        
        guard let descriptorValue = descriptor.value as? Data else{
            return ""
        }
        
        var returnString = ""
        var dataPointer = 0
        
        //let conditionValue = (descriptorValue.subdata(in: NSMakeRange(dataPointer, 1)) as NSData).bytes.bindMemory(to: UInt8.self, capacity: descriptorValue.subdata(with: NSMakeRange(dataPointer, 1)).count).pointee
        let conditionValue = (descriptorValue.subdata(in: dataPointer..<dataPointer+1) as NSData).bytes.bindMemory(to: UInt8.self, capacity: descriptorValue.subdata(in: dataPointer..<dataPointer+1).count).pointee
        dataPointer += 1
        
        switch conditionValue {
        case 1,2,3:
            
            //let analogValue = (descriptorValue.subdata(in: NSMakeRange(dataPointer, 2)) as NSData).bytes.bindMemory(to: UInt16.self, capacity: descriptorValue.subdata(with: NSMakeRange(dataPointer, 2)).count).pointee
            let analogValue = (descriptorValue.subdata(in: dataPointer..<dataPointer+2) as NSData).bytes.bindMemory(to: UInt16.self, capacity: descriptorValue.subdata(in: dataPointer..<dataPointer+2).count).pointee
            returnString += "Analog Value " + "\(analogValue)"
        case 4 :
            
            //let bitMaskValue = descriptorValue.subdata(in: NSMakeRange(dataPointer, 1)).getDescriptorBitMaskValue()
            let bitMaskValue = descriptorValue.subdata(in: dataPointer..<dataPointer+1).getDescriptorBitMaskValue()
            returnString += "BitMask " + bitMaskValue
            
        
        case 5,6 :
            
            if descriptorValue.count >= 5 {
                //let analogIntervalValue = (descriptorValue.subdata(in: NSMakeRange(dataPointer, 4)) as NSData).bytes.bindMemory(to: UInt32.self, capacity: descriptorValue.subdata(with: NSMakeRange(dataPointer, 4)).count).pointee
                let analogIntervalValue = (descriptorValue.subdata(in: dataPointer..<dataPointer+4) as NSData).bytes.bindMemory(to: UInt32.self, capacity: descriptorValue.subdata(in: dataPointer..<dataPointer+4).count).pointee
                returnString += "Analog Interval " + "\(analogIntervalValue)"
            }
            
        default:
            break
        }
        
        
        return returnString
    }
    

    
    fileprivate func getValueForTimeTriggerSettingFromDescriptor(_ descriptor : CBDescriptor) -> String{
        
        
        guard let descriptorValue = descriptor.value as? Data else{
            return ""
        }
        
        var returnString = ""
        //let conditionValue = (descriptorValue.subdata(in: NSMakeRange(0, 1)) as NSData).bytes.bindMemory(to: UInt8.self, capacity: descriptorValue.subdata(with: NSMakeRange(0, 1)).count).pointee
        let conditionValue = (descriptorValue.subdata(in: 0..<1) as NSData).bytes.bindMemory(to: UInt8.self, capacity: descriptorValue.subdata(in: 0..<1).count).pointee
        
        switch conditionValue {
        case 0:
            //let value = (descriptorValue.subdata(in: NSMakeRange(1, 1)) as NSData).bytes.bindMemory(to: UInt16.self, capacity: descriptorValue.subdata(with: NSMakeRange(1, 1)).count).pointee
            let value = (descriptorValue.subdata(in: 1..<2) as NSData).bytes.bindMemory(to: UInt16.self, capacity: descriptorValue.subdata(in: 1..<2).count).pointee
            returnString += "No comparison value required " + "\(value)"
        case 1,2:
            
            //let timeInterval = (descriptorValue.subdata(in: NSMakeRange(1, 3)) as NSData).bytes.bindMemory(to: UInt32.self, capacity: descriptorValue.subdata(with: NSMakeRange(1, 3)).count).pointee
            let timeInterval = (descriptorValue.subdata(in: 1..<4) as NSData).bytes.bindMemory(to: UInt32.self, capacity: descriptorValue.subdata(in: 1..<4).count).pointee
            returnString += "Time Interval " + "\(timeInterval)"
            
        case 3:
            
            //let count = (descriptorValue.subdata(in: NSMakeRange(1, 2)) as NSData).bytes.bindMemory(to: UInt16.self, capacity: descriptorValue.subdata(with: NSMakeRange(1, 2)).count).pointee
            let count = (descriptorValue.subdata(in: 1..<3) as NSData).bytes.bindMemory(to: UInt16.self, capacity: descriptorValue.subdata(in: 1..<3).count).pointee
            returnString += "Count " + "\(count)"
            
        default:
            break
        }
        
        return returnString
    }
    
    
    
    
    
    
    
    
    
    
}








