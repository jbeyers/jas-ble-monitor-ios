
/* ***************************************************************************

BLEUtil.swift


Copyright (c) 2015, Atmel Corporation. All rights reserved.
Released under NDA
Licensed under Atmel's Limited License Agreement.


THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Atmel Corporation: http://www.atmel.com

******************************************************************************/

import Foundation
import UIKit
import CoreBluetooth
import CoreLocation

class Util{
    
    class func roundAndFormatFloat(_ floatToReturn : Float, numDecimalPlaces: Int) -> String
    {
        
        let formattedNumber = String(format: "%.\(numDecimalPlaces)f", floatToReturn)
        return formattedNumber
        
    }
    
    class func delay(_ delay:Double, closure:@escaping ()->()) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
    
    
    
    class func getURLForAltBeacon(_ beacon : AtmelAltBeacon) -> String? {
        
        let savedBeaconsList = BeaconRanger.getSavedBeaconItemList()
        
        if  savedBeaconsList != nil && savedBeaconsList?.count != 0 {
            
            for beaconItem in savedBeaconsList! {
                
                if let altBeaconItem = beaconItem as? AtmelAltBeaconItem {
                    
                    //if altBeaconItem.id1.isEqual(beacon.deviceUUID) && altBeaconItem.id2 == beacon.majorID && altBeaconItem.id3 == beacon.minorID {
                    if altBeaconItem.id1 == (beacon.deviceUUID) && altBeaconItem.id2 == beacon.majorID && altBeaconItem.id3 == beacon.minorID{
                        return beaconItem.urlString
                    }
                }
            }
        }
        return nil
    }
    
    
    
   class func getURLForiBeacon(_ beacon : CLBeacon) -> String? {
        
        let savedBeaconsList = BeaconRanger.getSavedBeaconItemList()
        
        if savedBeaconsList != nil && savedBeaconsList?.count != 0 {
            
            for beaconItem in savedBeaconsList! {
                
                if let iBeaconItem = beaconItem as? AtmeliBeaconItem {
                    
                    //if iBeaconItem.uuid.isEqual(beacon.proximityUUID) && iBeaconItem.major == beacon.major && iBeaconItem.minor == beacon.minor {
                    if iBeaconItem.uuid == (beacon.proximityUUID) && iBeaconItem.major == beacon.major && iBeaconItem.minor == beacon.minor {
                        
                        return beaconItem.urlString
                    }
                }
            }
        }
        return nil
    }
    

    class func getTopViewcontroller() -> UIViewController?{
        
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            if let tp = (topController as? UINavigationController)?.visibleViewController
            {
                topController = tp
            }
            else{
                while let presentedViewController = topController.presentedViewController {
                    topController = presentedViewController
                    if let navC = (topController as? UINavigationController)?.visibleViewController
                    {topController = navC}
                }
            }
            return topController
        }
        return nil
    }
    
}


