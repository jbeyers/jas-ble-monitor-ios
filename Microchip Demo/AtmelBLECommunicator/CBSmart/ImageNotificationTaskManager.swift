/***************************************************************************
 
 ImageNotificationTaskManager.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import UIKit

enum OTAUSecurityLevel:UInt8{
    
    case noSecurity = 0x00
    case notDefined = 0x01
    case ignored    = 0xFF
   
}



class ImageNotificationResponseFrame:OTAResponseFrame{
    
    
    var firmwareVersion:FirmWareVersion?
    var hardwareVersion:HardWareVersion?
    var hardwareRevision:UInt8?
    var imageOption:OTAUImageOption?

   

        init(withData:Data,updateType:OTAUpdateType){
        
        super.init(withData: withData)
        
        
        if command != nil {
            switch(command!){
                
            case OTAUCommand.failure:
                responseType = OTAUResponseType.failure
               
                if withData.count >= 4
                    //{errorCode = ResponseErrorCode(rawValue:(withData.subdata(in: NSMakeRange(3, 1)) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(with: NSMakeRange(3, 1)).count).pointee)}
                    {errorCode = ResponseErrorCode(rawValue:(withData.subdata(in: 3..<4) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(in: 3..<4).count).pointee)}
                break
                
            case updateType.resposeCommand:
                
                responseType = OTAUResponseType.success
                
                if withData.count >= 4
                    //{hardwareRevision = (withData.subdata(in: NSMakeRange(3, 1)) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(with: NSMakeRange(3, 1)).count).pointee}
                    {hardwareRevision = (withData.subdata(in: 3..<4) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(in: 3..<4).count).pointee}
                
                if withData.count >= 8
                    //{firmwareVersion = FirmWareVersion(withData: withData.subdata(in: NSMakeRange(4, 4)))}
                    {firmwareVersion = FirmWareVersion(withData: withData.subdata(in: 4..<8))}
                
                if withData.count >= 8
                    //{hardwareVersion = HardWareVersion(withData: withData.subdata(in: NSMakeRange(8, 2)))}
                    {hardwareVersion = HardWareVersion(withData: withData.subdata(in: 8..<10))}
                if withData.count >= 11
                    //{imageOption = OTAUImageOption(rawValue: (withData.subdata(in: NSMakeRange(10, 1)) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(with: NSMakeRange(10, 1)).count).pointee)}
                    {imageOption = OTAUImageOption(rawValue: (withData.subdata(in: 10..<11) as NSData).bytes.bindMemory(to: UInt8.self, capacity: withData.subdata(in: 10..<11).count).pointee)}
                
                break
                
            default:
                responseType = OTAUResponseType.invalid
                break
            }
            
        }
        
    }
    
}
class ImageNotificationRequestFrame:OTARequestFrame {
    
    var productID:UInt16!
    var vendorID:UInt16!
    
    var flagbites:UInt8!

    var IMGTFlag:BoolFlag!
    var PATFlag:BoolFlag!
    var LIBflag:BoolFlag!
    var APPFlag:BoolFlag!
    
    var firmwareVersion:FirmWareVersion!
    var hardwareVersion:HardWareVersion!
    
    var hardwareRevison:UInt8!
    var securityLevel:OTAUSecurityLevel!
    
    init(image:Image,updateType:OTAUpdateType) {
        
        super.init()
        
        length  = OTAFrameLength.imageRequest.rawValue
        command = updateType.requestCommand
        
        //Frame Values
        
        productID = image.productID
        vendorID  = image.vendorID
        
        flagbites = image.flagbites
        IMGTFlag  = image.IMGTFlag
        PATFlag   = image.PATFlag
        LIBflag   = image.LIBflag
        APPFlag   = image.APPFlag
        
        firmwareVersion = image.firmwareVersion
        hardwareVersion = image.hardwareVersion
        hardwareRevison = image.hardwareRevison
        
        securityLevel = image.securityLevel
        
    }
    fileprivate func getFlagWord()->UInt8{
        var returnFlag:UInt8 = 0x00
        
        returnFlag = returnFlag|APPFlag.rawValue
        returnFlag = returnFlag << 1
        
        returnFlag = returnFlag|LIBflag.rawValue
        returnFlag = returnFlag << 1
        
        returnFlag = returnFlag|PATFlag.rawValue
        returnFlag = returnFlag << 1
        
        returnFlag = returnFlag|IMGTFlag.rawValue
       
        
        return returnFlag
    }
    
    func dataFromFrame()->Data?{
        
        if  productID == nil || vendorID == nil || IMGTFlag == nil || PATFlag == nil || LIBflag == nil || APPFlag == nil || firmwareVersion == nil || hardwareRevison == nil || hardwareVersion == nil || securityLevel == nil
        {return nil}
        

        //16 bitlength data
        
        var dataLength:UInt16 = length!
        let returnFrameData = NSMutableData(bytes: &dataLength, length: MemoryLayout.size(ofValue: dataLength))
        
        //8 bit command data
        
        var commandData:UInt8 = command!.rawValue
        returnFrameData.append(&commandData, length: MemoryLayout.size(ofValue: commandData))
        
        // product id data
        
        
        var pid:UInt16 = productID!
        returnFrameData.append(&pid, length: MemoryLayout.size(ofValue: pid))
        
        // vendor id data

        
        var vid:UInt16 = vendorID!
        returnFrameData.append(&vid, length: MemoryLayout.size(ofValue: vid))
        
        // 16 bit flag data
        var flagword:UInt8 = flagbites
        returnFrameData.append(&flagword, length: MemoryLayout.size(ofValue: flagword))
        
        // 32 bit firmware version data

        returnFrameData.append(firmwareVersion.getData() as Data)

        // 16 bit hardware revison data

        returnFrameData.append(hardwareVersion.getData() as Data)
        
        //8 bit hardware version data
        
        var hardwareVers:UInt8 = hardwareRevison!
        returnFrameData.append(&hardwareVers, length: MemoryLayout.size(ofValue: hardwareVers))
        
        //8 bit hardware version data
        
        var securityLev:UInt8 = securityLevel!.rawValue
        returnFrameData.append(&securityLev, length: MemoryLayout.size(ofValue: securityLev))

        return returnFrameData as Data
    }
    
}


class ImageNotificationTaskManager: OTATask {

    var didSendRequest:Bool = false
    var sucessBlock:((_ isResume:Bool)->())? = nil
    var failBlock:((_ withError:OTAErrorType,_ errorString:String)->())? = nil
    
    init(interface:AtmelOTAInterface){
        
        super.init()
        otaInterface = interface
        otaInterface.delegate = self
    }
    
    func start(success sucess:@escaping (_ isResume:Bool)->(),fail:@escaping (_ withError:OTAErrorType,_ errorString:String)->()){
        
        let imageNotificationRequest = ImageNotificationRequestFrame(image:otaInterface.image! ,updateType:otaInterface!.updateType)
        
        //Check for kind of update
        
        if let requestData = imageNotificationRequest.dataFromFrame(){
            
//                otaInterface..updatedProgressInfo("Requesting for OTA update", progress:nil)

                otaInterface.sendFrameData(requestData)
                didSendRequest = true
                sucessBlock = sucess
                failBlock = fail
        }
        
        else{
            fail(OTAErrorType.localError, "Image Notify data  from image file Could not be obtained")
        }
    }
    
    

}

extension ImageNotificationTaskManager:AtmelOTAInterfaceDelegate{
    
    func receivedResponseFrameData(_ responseData:Data){
        if otaShouldAbort()
            {return}
        if didSendRequest{
            let imageResponseFrame = ImageNotificationResponseFrame(withData: responseData,updateType:otaInterface!.updateType)
            
            if imageResponseFrame.responseType == OTAUResponseType.success{
                didSendRequest = false

                //            otaInterface..updatedProgressInfo("OTA update request successful", progress:nil)
                
                sucessBlock!(imageResponseFrame.imageOption == OTAUImageOption.resume)
            }
            else if imageResponseFrame.responseType == OTAUResponseType.failure{
                didSendRequest = false

                failBlock!(OTAErrorType.communicationError," Image Notification fail: (\(String(describing: imageResponseFrame.errorCode)))")
            }
        }
    }
}


