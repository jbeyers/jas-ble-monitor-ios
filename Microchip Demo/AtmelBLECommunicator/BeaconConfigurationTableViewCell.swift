/***************************************************************************
 
 BeaconConfigurationTableViewCell.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import UIKit

protocol BeaconConfigurationTableCellDelegate{
    
    func writeToBeaconWith(_ period : UInt16, field : BeaconConfigurationFields)
}



class BeaconConfigurationTableViewCell: UITableViewCell {
    
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var periodTextField: UITextField!
    @IBOutlet weak var writeButton: UIButton!
    @IBOutlet weak var alertLabel: UILabel!
    
    
    let MIN_BEACON_PERIOD : UInt16 = 100
    let MAX_BEACON_PERIOD : UInt16 = 10240
    
    var delegate : BeaconConfigurationTableCellDelegate?
    var parentTableView : UITableView?
    var tableViewFrame : CGRect?
    fileprivate var tableViewContentOffset : CGPoint?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        periodTextField.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
    @IBAction func writeButtonClicked(_ sender: UIButton) {
        
        if delegate != nil{
            
            let configurationField = BeaconConfigurationFields(rawValue: sender.tag)!
            
            if configurationField == .beaconPeriod{
                
                if periodTextField.text != ""{
                    
                    let value = periodTextField.toUInt16(false)
                    
                    if value != nil {
                        
                        if isBeaconPeriodValid(UInt16(value!.intValue)){
                            delegate?.writeToBeaconWith(UInt16(value!.intValue), field: configurationField)
                        }
                        else{
                            
                            let alertVC = UIAlertController(title: "ERROR!", message: "beacon period values should be in between 100ms and 10240ms", preferredStyle: .alert)
                            let alertAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                            alertVC.addAction(alertAction)
                            
                            if self.parentViewController != nil{
                                self.parentViewController?.present(alertVC, animated: true, completion: nil)
                            }
                        }
                        
                    }
                }
                else{
                    periodTextField.backgroundColor = INVALID_RED
                }
            }
            else{
                
                delegate?.writeToBeaconWith(0, field: configurationField)
            }
         
        }
    }
    
    
    func isBeaconPeriodValid(_ period : UInt16) -> Bool{
        
        if period == 0 {
            return true
        }
        
        if period < MIN_BEACON_PERIOD || period > MAX_BEACON_PERIOD{
            return false
        }
        else{
            return true
        }
    }


}



extension BeaconConfigurationTableViewCell : UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        print("Begin editing")
        
        textField.backgroundColor = UIColor.clear
        tableViewFrame = parentTableView!.frame
        
        if  UIDevice.is_iPad() && UIDevice.is_Landscape() {
            
             parentTableView!.frame = CGRect(x: parentTableView!.frame.origin.x, y: parentTableView!.frame.origin.y, width: parentTableView!.frame.size.width, height: parentTableView!.frame.size.height - 370)
        }
        else{
            
             parentTableView!.frame = CGRect(x: parentTableView!.frame.origin.x, y: parentTableView!.frame.origin.y, width: parentTableView!.frame.size.width, height: parentTableView!.frame.size.height - 300)
        }
        
        tableViewContentOffset = parentTableView?.contentOffset
        let indexPath = IndexPath(row: 5, section: 0)
        parentTableView?.scrollToRow(at: indexPath, at: UITableViewScrollPosition.top, animated: true)
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        print("end editing")
        parentTableView!.frame = tableViewFrame!
        parentTableView?.contentOffset = tableViewContentOffset!
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        // return true if the replacementString only contains numeric characters
        let aSet = CharacterSet(charactersIn:"0123456789").inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        let length = textField.text!.utf16.count + string.utf16.count - range.length
        
        
        if length <= 5 && string == numberFiltered {
            return true
        }
        else{
            return false
        }
    }

    
    
}
