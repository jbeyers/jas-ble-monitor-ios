/***************************************************************************
 
 URLDetailTableViewCell.swift

 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/
import UIKit

protocol URLDetailTableViewCellDelegate{
    
    func writeData(_ data: String, field: BeaconConfigurationFields)
}


class URLDetailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var urlDataTextField: UITextField!
    @IBOutlet weak var writeButton: UIButton!
    
    var delegate : URLDetailTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        urlDataTextField.delegate = self
    }
    
    
    
    @IBAction func writeButtonClicked(_ sender: UIButton) {
        
        if urlDataTextField.text != ""{
            
            if (delegate != nil){
                
                
                if urlDataTextField.toURLString(true) != nil {
                    delegate?.writeData(urlDataTextField.text!, field: BeaconConfigurationFields(rawValue: sender.tag)!)
                }
                else{
                    let alertVC = UIAlertController(title: "ERROR", message: "Please enter a valid URL", preferredStyle: .alert)
                    let alertAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertVC.addAction(alertAction)
                    UIApplication.shared.keyWindow?.rootViewController?.presentedViewController!.present(alertVC, animated: true, completion: nil)
                }
            }
        }
    }
}




extension URLDetailTableViewCell : UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.backgroundColor = UIColor.clear
    }
    
}
