/***************************************************************************
 
 BeaconInfoView.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import UIKit
import CoreLocation

let arrowViewOrginConstant:CGFloat = 16
let boxBorder:CGFloat  = 9
let labelHeight:CGFloat = 12.0
let labelFont = UIFont(name: "Roboto-Medium", size:8.0)

protocol BeaconInfoViewDelegate:NSObjectProtocol{
    
    func beaconInfoViewTapped(_ beaconInfoView:BeaconInfoView)
}
class BeaconInfoView: UIView {
    
    var beaconData:AnyObject!
    var beaconType:BeaconVendorType!
    var RSSI:NSNumber!
    var beaconIconView:BeaconIconView!
    var delegate:BeaconInfoViewDelegate?
    
    var infoViewButton:UIButton!
    var infoLabel1:UILabel!
    var infoLabel2:UILabel!
    var infoLabel3:UILabel!
    var infoLabel4:UILabel!
    
    init(iconView:BeaconIconView,beaconDatas:AnyObject,type:BeaconVendorType,rssi:NSNumber!){
        
        
        super.init(frame:infoViewBoxFrame)
        beaconData = beaconDatas
        beaconType = type
        RSSI = rssi
        beaconIconView = iconView
        designBeaconInfoView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
  
    func updateData(_ beaconDatas:AnyObject,rssi:NSNumber!){
        beaconData = beaconDatas
        RSSI = rssi
        updateViewWithBeaconInfo()
    }
    
    @IBAction func beaconInfoButtonAction(_ sender: UIButton) {
        
        self.delegate?.beaconInfoViewTapped(self)
    }
}
extension BeaconInfoView{
    
    func designBeaconInfoView(){
        
        //Infoview button
        
        infoViewButton = UIButton(frame:self.bounds)
        self.addSubview(infoViewButton)
        infoViewButton.setImage(UIImage(named: "BeaconInfoBox"), for: UIControlState())
        infoViewButton.addTarget(self, action: #selector(BeaconInfoView.beaconInfoButtonAction(_:)), for: UIControlEvents.touchUpInside)
        
        //Info texts
        
        switch(beaconType!){
            
        case BeaconVendorType.nativeiBeacon:
                configureViewForiBeaconOrAltBeaconInfo()
            break
            
        case BeaconVendorType.thirdPatyBeacon:
            
             if let atmelBeacon = beaconData as? AtmelBeacon{
                
                if atmelBeacon.beaconType == .altBeacon {
                    configureViewForiBeaconOrAltBeaconInfo()
                }
                else{
                    configureViewForEddystoneBeaconInfo()
                }
             }
            
            break

        }
       updateViewWithBeaconInfo()
        
    }

    
    func configureViewForiBeaconOrAltBeaconInfo(){
        
        let viewHeight = self.frame.height - boxBorder*2
        let padding = (viewHeight - labelHeight*4)/6
        let marginPadding = 1.5 * padding
        
        var designPointY = marginPadding+boxBorder
        
        infoLabel1 = UILabel(frame: CGRect(x: 0,y: designPointY,width: 30,height: labelHeight))
        infoLabel1.textAlignment = NSTextAlignment.center
        infoLabel1.font = labelFont
        self.addSubview(infoLabel1)

        
        designPointY = (infoLabel1.frame.maxY+padding)
        infoLabel2 = UILabel(frame: CGRect(x: 0,y: designPointY,width: 30,height: labelHeight))
        infoLabel2.textAlignment = NSTextAlignment.center
        infoLabel2.font = labelFont
        self.addSubview(infoLabel2)

        
        designPointY = (infoLabel2.frame.maxY+padding)
        infoLabel3 = UILabel(frame: CGRect(x: 0,y: designPointY,width: 30,height: labelHeight))
        infoLabel3.textAlignment = NSTextAlignment.center
        infoLabel3.font = labelFont
        self.addSubview(infoLabel3)

        
        designPointY = (infoLabel3.frame.maxY+padding)
        infoLabel4 = UILabel(frame: CGRect(x: 0,y: designPointY,width: 30,height: labelHeight))
        infoLabel4.textAlignment = NSTextAlignment.center
        infoLabel4.font = labelFont
        self.addSubview(infoLabel4)
    }
    
    func configureViewForEddystoneBeaconInfo(){
        
        let viewHeight = self.frame.height - boxBorder*2
        var padding = (viewHeight - labelHeight*3)/5
        
        if let atmelEddyStone = beaconData as? AtmelEddyStone{
            
            if atmelEddyStone.uidFrame == nil {
                padding = (viewHeight - labelHeight*2)/4
            }
        }
        
        let marginPadding = 1.5 * padding
        
        var designPointY = marginPadding+boxBorder
        
        infoLabel1 = UILabel(frame: CGRect(x: 0,y: designPointY,width: 30,height: labelHeight))
        infoLabel1.textAlignment = NSTextAlignment.center
        infoLabel1.font = labelFont
        self.addSubview(infoLabel1)
        
        designPointY = (infoLabel1.frame.maxY+padding)
        infoLabel2 = UILabel(frame: CGRect(x: 0,y: designPointY,width: 30,height: labelHeight))
        infoLabel2.textAlignment = NSTextAlignment.center
        infoLabel2.font = labelFont
        self.addSubview(infoLabel2)

        
        designPointY = (infoLabel2.frame.maxY+padding)
        infoLabel3 = UILabel(frame: CGRect(x: 0,y: designPointY,width: 30,height: labelHeight))
        infoLabel3.textAlignment = NSTextAlignment.center
        infoLabel3.font = labelFont
        self.addSubview(infoLabel3)
    }
    
    
    func updateViewWithBeaconInfo(){
        switch(beaconType!){
        
        case BeaconVendorType.nativeiBeacon:
            if let iBeaconInfo = beaconData as? CLBeacon{
                infoLabel1.text = "UUID   : \(iBeaconInfo.proximityUUID.uuidString)"
                infoLabel2.text = "MAJOR  : \(iBeaconInfo.major)"
                infoLabel3.text = "MINOR  : \(iBeaconInfo.minor)"
                infoLabel4.text = "RSSI   : \(RSSI)"
                
                infoLabel1.sizeToFit()
                infoLabel2.sizeToFit()
                infoLabel3.sizeToFit()
                infoLabel4.sizeToFit()
                
                infoLabel1.center = CGPoint(x: self.frame.width/2,y: infoLabel1.center.y)
                infoLabel2.center = CGPoint(x: self.frame.width/2,y: infoLabel2.center.y)
                infoLabel3.center = CGPoint(x: self.frame.width/2,y: infoLabel3.center.y)
                infoLabel4.center = CGPoint(x: self.frame.width/2,y: infoLabel4.center.y)
            }
        break
        
        case BeaconVendorType.thirdPatyBeacon:
            if let atmelBeacon = beaconData as? AtmelBeacon{
                
                switch(atmelBeacon.beaconType){
                case BeaconType.eddystone:
                    if let atmelEddyStone = atmelBeacon as? AtmelEddyStone{
                        
                        if atmelEddyStone.uidFrame != nil{
                            
                        infoLabel1.text = "NAMESPACE ID   : \(atmelEddyStone.uidFrame!.nameSpaceData)"
                        infoLabel2.text = "INSTANCE ID    : \(atmelEddyStone.uidFrame!.instanceComponentData)"
                        infoLabel3.text = "RSSI   : \(RSSI)"
                            
                        }
                        else
                        {
//                            if atmelEddyStone.peripheral!.name != nil {
////                                infoLabel1.text = "NAME   : \(atmelEddyStone.peripheral!.name!)"
//                            }
//                            else{
//                                infoLabel1.text = "NAME   : \(atmelEddyStone.peripheral!.name)"
//                            }
                            
                            infoLabel1.text = "RSSI   : \(RSSI)"
                            
                            if atmelEddyStone.urlFrame != nil && atmelEddyStone.urlFrame?.url != nil {
                                infoLabel2.text = "URL  : \(atmelEddyStone.urlFrame!.url!)"
                            }
                        }
                        
                        infoLabel1.sizeToFit()
                        infoLabel2.sizeToFit()
                        infoLabel3.sizeToFit()
                        
                        
                        infoLabel1.center = CGPoint(x: self.frame.width/2,y: infoLabel1.center.y)
                        infoLabel2.center = CGPoint(x: self.frame.width/2,y: infoLabel2.center.y)
                        infoLabel3.center = CGPoint(x: self.frame.width/2,y: infoLabel3.center.y)
                        
                    }
                    break
                case BeaconType.altBeacon:
                    if let atmelAltBeacon = atmelBeacon as? AtmelAltBeacon{
                       
                        infoLabel1.text = "UUID   : \(atmelAltBeacon.deviceUUID.uuidString)"
                        infoLabel2.text = "ID1  : \(atmelAltBeacon.majorID)"
                        infoLabel3.text = "ID2  : \(atmelAltBeacon.minorID)"
                        infoLabel4.text = "RSSI  : \(RSSI)"
                        
                        infoLabel1.sizeToFit()
                        infoLabel2.sizeToFit()
                        infoLabel3.sizeToFit()
                        infoLabel4.sizeToFit()
                        
                        infoLabel1.center = CGPoint(x: self.frame.width/2,y: infoLabel1.center.y)
                        infoLabel2.center = CGPoint(x: self.frame.width/2,y: infoLabel2.center.y)
                        infoLabel3.center = CGPoint(x: self.frame.width/2,y: infoLabel3.center.y)
                        infoLabel4.center = CGPoint(x: self.frame.width/2,y: infoLabel4.center.y)
                    }
                    break
                default:break
                }
                
            }
            break
        }
    }
}
