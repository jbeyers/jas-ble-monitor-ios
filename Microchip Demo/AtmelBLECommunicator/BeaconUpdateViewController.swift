/***************************************************************************
 
 BeaconUpdateViewController.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import UIKit


let UNWIND_TO_LIST_SEGUE    = "unwindToBeaconListVCSegue"
let MAX_VALUE : UInt16      = 65535

enum ItemListChangeType{
    case update
    case add
}
class BeaconUpdateViewController: BaseViewController {

    var changingItemType:ItemType!
    var changeType:ItemListChangeType!
    var beaconItemType:BeaconItemType!
    
    var updatedItem:AnyObject?
    var uuidListDropDown : DropDown?
    
    @IBOutlet weak var dropDownButton: UIButton!
    
    @IBOutlet weak var contentScrollView: UIScrollView!
    @IBOutlet weak var headerLabel: UILabel!
    
    @IBOutlet weak var field1HeaderLabel: UILabel!
    @IBOutlet weak var field2HeaderLabel: UILabel!
    @IBOutlet weak var field3HeaderLabel: UILabel!
    @IBOutlet weak var field4HeaderLabel: UILabel!
    @IBOutlet weak var field5HeaderLabel: UILabel!
    
    @IBOutlet weak var field1TextField: UITextField!
    @IBOutlet weak var field2TextField: UITextField!
    @IBOutlet weak var field3TextField: UITextField!
    @IBOutlet weak var field4TextField: UITextField!
    @IBOutlet weak var field5TextField: UITextField!

    @IBOutlet weak var isRanging: UISwitch!
    @IBOutlet weak var saveorAddButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateViewWithData()
        // Do any additional setup after loading the view.
        
//        field2TextField.text = "68753A44-4D6F-1226-9C60-0050E4C00067"
        
        if beaconItemType == BeaconItemType.AltBeacon {
            dropDownButton.isHidden = true
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(BeaconUpdateViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(BeaconUpdateViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        
        if beaconItemType == BeaconItemType.iBeacon {
            initiateDropDown()
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func initiateDropDown(){
        
        uuidListDropDown = DropDown()
        
        if let uuidList = BeaconRanger.getAllSavediBeaconUUIDs() {
            
            var dropdownHeight : CGFloat = field2TextField.frame.size.height * CGFloat(uuidList.count)
            
            let maxHeight = self.view.frame.height - (contentScrollView.frame.origin.y + field2TextField.frame.origin.y + field2TextField.frame.height + 80)
            
            if dropdownHeight > maxHeight{
                
                dropdownHeight = maxHeight
            }
            //uuidListDropDown?.initDropDownWith(CGRect(x: 0,y: 0, width: field2TextField.frame.width, height: dropdownHeight), elements: uuidList, button: dropDownButton)
            uuidListDropDown?.initDropDownWith(CGRect(x: 0,y: 0, width: field2TextField.frame.width, height: dropdownHeight), elements: uuidList as NSArray, button: dropDownButton)
            uuidListDropDown?.delgate = self
        }
    }
    
    
    //MARK:SAVE / UPDATE AND NAVIGATION
    func saveBeaconItem(_ beaconItem:AtmelBeaconItem){
        
        }
    func saveBeaconRegion(_ beaconRegion:AtmelBeaconRegion){
        
    }
    //MARK:Alert Methods
    
    func showAddingSuccessAlert(){
        let successAlert = UIAlertController(title: "Added \(changingItemType.rawValue) Data", message: "Successfully Added \(changingItemType.rawValue) data.", preferredStyle: UIAlertControllerStyle.alert)
      
        let goBackToListAction = UIAlertAction(title: "Back to List", style: UIAlertActionStyle.default) {
            UIAlertAction in
            self.performSegue(withIdentifier: UNWIND_TO_LIST_SEGUE, sender: self)
        }
        
        successAlert.addAction(goBackToListAction)

        self.present(successAlert, animated: true, completion: nil)

    }
    
    func showUpdateSuccessAlert(){
        let successAlert = UIAlertController(title: "Updated \(changingItemType.rawValue) Data", message: "Successfully Updated \(changingItemType.rawValue) data.", preferredStyle: UIAlertControllerStyle.alert)
        

        let goBackToListAction = UIAlertAction(title: "Back to List", style: UIAlertActionStyle.default) {
            UIAlertAction in
            self.performSegue(withIdentifier: UNWIND_TO_LIST_SEGUE, sender: self)
        }
        
        successAlert.addAction(goBackToListAction)
        
        self.present(successAlert, animated: true, completion: nil)
        
    }
    
    func showInvalidDataAlert(){

        
        let invalidDataAlert = UIAlertController(title: "Invalid \(changingItemType.rawValue) Data", message: "Invalid \(changingItemType.rawValue) data. Please check marked fields", preferredStyle: UIAlertControllerStyle.alert)
        invalidDataAlert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        

        self.present(invalidDataAlert, animated: true, completion: nil)
    }
    
    func showDuplicateDataAlert(){
        
        let invalidDataAlert = UIAlertController(title: "Duplicate \(changingItemType.rawValue) Data", message: "You already have a similar \(changingItemType.rawValue) data in List", preferredStyle: UIAlertControllerStyle.alert)
        invalidDataAlert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(invalidDataAlert, animated: true, completion: nil)
        
    }
    
    
    //MARK:Beacon Models from View

    func getUpdatediBeaconItem()->AtmeliBeaconItem?{

        let name = field1TextField.toString(true)
        let uuid  = field2TextField.toUUID(true)
        let major = field3TextField.toUInt16(true)
        let minor = field4TextField.toUInt16(true)
        let url = field5TextField.toURLString(true)
        
        if name != nil && uuid != nil && major != nil && minor != nil && url != nil
        {return AtmeliBeaconItem(withUUID:uuid!, majorValue: major!, minorValue: minor!, url: url!, nameString: name!)}
        
        return nil
    }
    func getUpdatediBeaconRegion()->AtmeliBeaconRegion?{
        
        let name = field1TextField.toString(true)
        let uuid  = field2TextField.toUUID(true)
        let major = field3TextField.toUInt16(false)
        let minor = field4TextField.toUInt16(false)
        
        if name != nil && uuid != nil
        {return AtmeliBeaconRegion(withUUID:uuid!, majorValue: major, minorValue: minor, range: isRanging.isOn, nameString: name!)}
        
        return nil

    }
    func getUpdatedAltBeaconItem()->AtmelAltBeaconItem?{
        let name = field1TextField.toString(true)
        let id1  = field2TextField.toUUID(true)
        let id2 = field3TextField.toUInt16(true)
        let id3 = field4TextField.toUInt16(true)
        let url = field5TextField.toURLString(true)
        
        if name != nil && id1 != nil && id2 != nil && id3 != nil && url != nil
        {return AtmelAltBeaconItem(withId1: id1!, id2Value: id2!.uint16Value, id3Value: id3!.uint16Value, url: url!, nameString: name!)}
        
        return nil

    }
    func getUpdatedAltBeaconRegion()->AtmelAltBeaconRegion?{
        
        let name = field1TextField.toString(true)
        let id1  = field2TextField.toUUID(true)
        let id2 = field3TextField.toUInt16(false)
        let id3 = field4TextField.toUInt16(false)
        
        if name != nil && id1 != nil
            {return AtmelAltBeaconRegion(withId1: id1!, id2Value: id2?.uint16Value, id3Value: id3?.uint16Value, range: isRanging.isOn, nameString: name!)}
        
        return nil
    }
    
    
    //MARK : View data filling
    
    func updateViewWithData(){
        
        
        switch(changeType!){
        case ItemListChangeType.add:
            headerLabel.text = "Add"
            break
            
        case ItemListChangeType.update:
            headerLabel.text = "Update"
        }
        
        switch(beaconItemType!){
        case BeaconItemType.iBeacon:
            field1HeaderLabel.text = "Name"
            field2HeaderLabel.text = "UUID"
            field3HeaderLabel.text = "MAJOR"
            field4HeaderLabel.text = "MINOR"
            headerLabel.text = "\(headerLabel.text!) iBeacon"
            
            if let updatediBeacon = updatedItem as? AtmeliBeaconItem{
                field1TextField.text = updatediBeacon.name
                field2TextField.text = updatediBeacon.uuid.uuidString
                field3TextField.text = "\(updatediBeacon.major)"
                field4TextField.text = "\(updatediBeacon.minor)"
                field5TextField.text = updatediBeacon.urlString

            }
            else if let updatediBeaconRegion = updatedItem as? AtmeliBeaconRegion{
                field1TextField.text = updatediBeaconRegion.name
                field2TextField.text = updatediBeaconRegion.uuid.uuidString
                
                if updatediBeaconRegion.major != nil
                    {field3TextField.text = "\(updatediBeaconRegion.major!)"}
                else
                    {field3TextField.placeholder = "Wildcarded (*)"}
                
                if updatediBeaconRegion.minor != nil
                    {field4TextField.text = "\(updatediBeaconRegion.minor!)"}
                else
                    {field4TextField.placeholder = "Wildcarded (*)"}
                isRanging.isOn = updatediBeaconRegion.isRanging
               
            }

            break
        case BeaconItemType.AltBeacon:
            
            field1HeaderLabel.text = "Name"
            field2HeaderLabel.text = "UUID"
            field3HeaderLabel.text = "ID1"
            field4HeaderLabel.text = "ID2"
            headerLabel.text = "\(headerLabel.text!) AltBeacon"
            
            if let updatedAltBeacon = updatedItem as? AtmelAltBeaconItem{
                field1TextField.text = updatedAltBeacon.name
                field2TextField.text = "\(updatedAltBeacon.id1.uuidString)"
                field3TextField.text = "\(updatedAltBeacon.id2)"
                field4TextField.text = "\(updatedAltBeacon.id3)"
                field5TextField.text = updatedAltBeacon.urlString

            }
            else if let updatedAltBeaconRegion = updatedItem as? AtmelAltBeaconRegion{
                field1TextField.text = updatedAltBeaconRegion.name
                field2TextField.text = "\(updatedAltBeaconRegion.id1.uuidString)"
                
                if updatedAltBeaconRegion.id2 != nil
                {field3TextField.text = "\(updatedAltBeaconRegion.id2!)"}
                else
                {field3TextField.placeholder = "Wildcarded (*)"}
                
                if updatedAltBeaconRegion.id3 != nil
                {field4TextField.text = "\(updatedAltBeaconRegion.id3!)"}
                else
                {field4TextField.placeholder = "Wildcarded (*)"}
                isRanging.isOn = updatedAltBeaconRegion.isRanging
               
            }

            
            break
        }
        switch(changingItemType!){
        case ItemType.Region:
            field5HeaderLabel.text = "Is Ranging Enabled?"
            headerLabel.text = "\(headerLabel.text!) Region"
            field5TextField.isHidden = true
            isRanging.isHidden = false
            break
        case ItemType.Beacon:
            field5HeaderLabel.text = "URL"
            field5TextField.isHidden = false
            isRanging.isHidden = true

            break
        }
    }
    
    
    
    //MARK : Save Action
    
    @IBAction func saveorAddButtonAction(_ sender: UIButton) {
   
        switch(changeType!){
            case ItemListChangeType.add:
                switch(changingItemType!){
                    
                case ItemType.Beacon:
                    
                    var beaconItem:AtmelBeaconItem?
                    
                    switch(beaconItemType!){
                        
                    case BeaconItemType.iBeacon:
                         beaconItem = getUpdatediBeaconItem()
                        break;
                    case BeaconItemType.AltBeacon:
                         beaconItem = getUpdatedAltBeaconItem()
                        break;
                    }
                    if beaconItem != nil{
                        if BeaconRanger.isBeaconItemPresentInDB(beaconItem!)
                            {showDuplicateDataAlert()}
                        
                        else{
                            BeaconRanger.saveBeaconItem(beaconItem!)
                            showAddingSuccessAlert()
                            }
                    }
                    else
                        {showInvalidDataAlert()}
                    break;
                    
                case ItemType.Region:
                    var beaconRegion:AtmelBeaconRegion?

                    switch(beaconItemType!){
                        
                    case BeaconItemType.iBeacon:
                        beaconRegion = getUpdatediBeaconRegion()
                        break;
                    case BeaconItemType.AltBeacon:
                        beaconRegion = getUpdatedAltBeaconRegion()
                        break;
                    }
                    if beaconRegion != nil{
                        
                        if BeaconRanger.isBeaconRegionPresentInDB(beaconRegion!)
                        {showDuplicateDataAlert()}
                        
                        else{
                            BeaconRanger.saveBeaconRegion(beaconRegion!)
                            showAddingSuccessAlert()
                        }
                    }
                    else
                        {showInvalidDataAlert()}
                    break;
                }

                break
                
            case ItemListChangeType.update:
                switch(changingItemType!){
                    
                case ItemType.Beacon:
                    var beaconItem:AtmelBeaconItem?
                    var oldBeaconItem : AtmelBeaconItem?
                    switch(beaconItemType!){
                        
                    case BeaconItemType.iBeacon:
                        beaconItem = getUpdatediBeaconItem()
                        oldBeaconItem = updatedItem as? AtmeliBeaconItem
                        break;
                    case BeaconItemType.AltBeacon:
                        beaconItem = getUpdatedAltBeaconItem()
                        oldBeaconItem = updatedItem as? AtmelAltBeaconItem
                        break;
                    }
                    
                    if beaconItem != nil && oldBeaconItem != nil{
                        
                        if BeaconRanger.isBeaconItemPresentInDB(beaconItem!) && !beaconItem!.isEqual(oldBeaconItem!){
                            showDuplicateDataAlert()
                        }
                        else{
                            BeaconRanger.replaceBeacon(oldBeaconItem!, withBeacon: beaconItem!)
                            showUpdateSuccessAlert()
                        }
                    }
                    else{
                        showInvalidDataAlert()
                    }
                    break;
                    
                case ItemType.Region:
                    var beaconRegion:AtmelBeaconRegion?
                    var oldBeaconRegion : AtmelBeaconRegion?
                    
                    switch(beaconItemType!){
                        
                    case BeaconItemType.iBeacon:
                        beaconRegion = getUpdatediBeaconRegion()
                        oldBeaconRegion = updatedItem as? AtmeliBeaconRegion
                        break;
                    case BeaconItemType.AltBeacon:
                        beaconRegion = getUpdatedAltBeaconRegion()
                        oldBeaconRegion = updatedItem as? AtmelAltBeaconRegion
                        break;
                    }
                    if beaconRegion != nil && oldBeaconRegion != nil{
                        
                        if BeaconRanger.isBeaconRegionPresentInDB(beaconRegion!) && !beaconRegion!.isEqual(oldBeaconRegion!){
                            showDuplicateDataAlert()
                        }
                        else{
                            BeaconRanger.replaceRegion(oldBeaconRegion!, withRegion: beaconRegion!)
                            showUpdateSuccessAlert()
                        }
                    }
                    else
                        {showInvalidDataAlert()}
                    break;
                }

                break
                
            }

    }
    
    
    @IBAction func dropdownButtonClicked(_ sender: UIButton) {
        
        if uuidListDropDown!.isShown == false {
            uuidListDropDown?.showDropDownFrom(CGRect(x: field2TextField.frame.origin.x, y: contentScrollView.frame.origin.y + field2TextField.frame.origin.y , width: field2TextField.frame.width, height: field2TextField.frame.height), view: self.view)            
        }
        else{
            uuidListDropDown?.removeDropDown()
        }
        
    }
    
    
    func keyboardWillShow(_ notification:Notification)
    {
        if let userInfo = notification.userInfo
        {
            if let keyboardRect = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            {
             contentScrollView.frame.size.height = (view.frame.size.height*0.8) - keyboardRect.size.height
              
                
            }
        }
    }
    
    func keyboardWillHide(_ notification:Notification)
        {contentScrollView.frame.size.height = (view.frame.size.height*0.8)}

    

   
}
extension BeaconUpdateViewController:UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        textField.backgroundColor = UIColor.clear
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.tag == 3 || textField.tag == 2 {
            // return true if the replacementString only contains numeric characters
            let aSet = CharacterSet(charactersIn:"0123456789").inverted
            
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            let length = textField.text!.utf16.count + string.utf16.count - range.length
            
            if length <= 5 && string == numberFiltered{
                
                // Check whether the value entered is less than the max of UInt16
                let newString = textField.text! + string
                let value = UInt16(newString)
            
                if  value != nil && value! <= MAX_VALUE {
                    return true
                }
            }
        }
        else{
            return true
        }
        return false
    }
}


extension BeaconUpdateViewController : DropDownDelegate{
    
    func didselectRowIndropDownWith(_ element: String){
        field2TextField.text = element
    }

}



