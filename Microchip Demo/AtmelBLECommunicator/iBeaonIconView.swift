/***************************************************************************
 
 iBeaonIconView.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import UIKit
import CoreLocation

class iBeaonIconView: BeaconIconView {
    var beaconObject:CLBeacon!{
        didSet{
            beaconObj = beaconObject
        }
    }
    
    init(beaconInfo:CLBeacon){
        super.init(withUUID: beaconInfo.proximityUUID, vendorType: BeaconVendorType.nativeiBeacon)
        beaconObject = beaconInfo
        beaconObj = beaconInfo

        rssiQueue = Queue(withMaxSize: 4)

        iconImageButton.setImage(UIImage(named: "iBeaconImage"), for: UIControlState())
        self.RSSI = NSNumber(value: beaconObject.rssi as Int)

    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override func updateDataWithBeaconObject(_ beaconObjet:AnyObject){
        if let beaconInfo = beaconObjet as? CLBeacon
        {beaconObject = beaconInfo}
        
        if beaconObject.rssi < 0 {
            self.RSSI = NSNumber(value: beaconObject.rssi as Int)
        }
    }
}
