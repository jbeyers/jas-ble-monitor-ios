/***************************************************************************
 
 BeaconURLViewController.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import UIKit

class BeaconURLViewController: BaseViewController, UIWebViewDelegate {
    
    
    
    @IBOutlet weak var beaconURLWebView: UIWebView!
    var beaconURL   : URL?
    var alertVC     : UIAlertController?
    fileprivate var loadActivityIndicator : UIActivityIndicatorView?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        configureAlert()
        configureLoadActivityIndicator()
        
        if beaconURL != nil {
            updateWebViewWithURL(beaconURL!)
        }
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        loadActivityIndicator = nil
        beaconURL = nil
        
        if alertVC!.isViewLoaded && (alertVC!.view.window != nil) {
            alertVC?.dismiss(animated: false, completion: nil)
        }
        alertVC = nil
    }
    

    /*

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
    
    func configureLoadActivityIndicator(){

        loadActivityIndicator = UIActivityIndicatorView(frame: self.view.bounds)
        loadActivityIndicator?.activityIndicatorViewStyle = .white
        loadActivityIndicator?.backgroundColor = UIColor.lightGray
        loadActivityIndicator?.alpha = 0.5
        loadActivityIndicator?.startAnimating()
    }
    
    
    
    func configureAlert(){
        
        alertVC = UIAlertController(title: "ALERT", message: "The nearest beacon has been changed. Do you want to see the content?", preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "Yes", style: .default) { (action) in
            
            if self.beaconURL != nil{
                self.updateWebViewWithURL(self.beaconURL!)
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alertVC?.addAction(alertAction)
        alertVC?.addAction(cancelAction)
    }
    
    
    func showAlert(){
        
        if (alertVC!.isViewLoaded && (alertVC!.view.window != nil)) == false {
            self.present(alertVC!, animated: true, completion: nil)
        }
        
    }
    
    
    
    func updateWebViewWithURL(_ url : URL)  {
        
        beaconURLWebView.addSubview(loadActivityIndicator!)
        
        beaconURL = url
        let request = URLRequest(url: url, cachePolicy: NSURLRequest.CachePolicy.reloadIgnoringCacheData, timeoutInterval: 60.0)
        beaconURLWebView.loadRequest(request)
    }
    
    // MARK:- UIWebview delegate
    
    
    
    func webViewDidStartLoad(_ webView: UIWebView){
        
//        loadActivityIndicator?.removeFromSuperview()
    }
    
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        loadActivityIndicator?.removeFromSuperview()

    }

    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        
        loadActivityIndicator?.removeFromSuperview()
        
        let loadErrorAlert = UIAlertController(title: "ALERT", message: "Could not load the webpage", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        loadErrorAlert.addAction(cancelAction)
        self.present(loadErrorAlert, animated: true, completion: nil)
    }
    
    
}
