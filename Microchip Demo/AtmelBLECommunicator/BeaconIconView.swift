/***************************************************************************
 
 BeaconIconView.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import UIKit
import CoreLocation

protocol BeaconIconViewDelegate:NSObjectProtocol{
    
    func beaconIconViewTapped(_ beaconIconView:BeaconIconView)
}
enum InfoViewArrowDirection{
    case left
    case up
    case right
    case down
    case none
}



class BeaconIconView: UIView {

    let TIME_INTERVAL = 2.0
    
    var uuid:UUID!
    var beaconObj:AnyObject!
    var type:BeaconVendorType!
    var RSSI:NSNumber!
    {
        didSet{
            rssiQueue?.enqueue(RSSI.intValue)
        }
    }
    
    var maxPower:CGFloat!
    var minPower:CGFloat!

    var delegate:BeaconIconViewDelegate?
    
    var viewArrowDirection:InfoViewArrowDirection!
        {
        didSet{
            updateArrowPosition()
        }
    }
    
    var timer : Timer?
    var rssiQueue : Queue?
    
    var arrowView:UIImageView!
    var iconImageButton:UIButton!
    var infoView : BeaconInfoView?
    
    init(withUUID:UUID ,vendorType:BeaconVendorType) {
        super.init(frame: CGRect(x: 0, y: 0, width: 48.0, height: 48.0))
        
        iconImageButton = UIButton(frame: self.bounds)
        iconImageButton.addTarget(self, action: #selector(BeaconIconView.beaconIconButtonAction(_:)), for: UIControlEvents.touchUpInside)
        self.addSubview(iconImageButton)
        self.clipsToBounds = false
        arrowView = UIImageView(frame: CGRect(x: 0, y: 0, width: 16, height: 9))
        self.addSubview(arrowView)
        viewArrowDirection = InfoViewArrowDirection.none
        uuid = withUUID
        type = vendorType
        timer = scheduleTimer()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    class func getIconViewForBeacon(_ vendorType:BeaconVendorType ,beaconObject:AnyObject)->BeaconIconView?{
        
        switch(vendorType){
            
        case BeaconVendorType.nativeiBeacon:
            if let clBeacon = beaconObject as? CLBeacon {
                return iBeaonIconView(beaconInfo: clBeacon)
            }
            break
            
        case BeaconVendorType.thirdPatyBeacon:
            
            if let beaconInfo = beaconObject as? AtmelBeacon{
            
                switch(beaconInfo.beaconType){
                    
                case BeaconType.altBeacon:
                    return AltBeaconIconView(beaconInfo: beaconInfo)
                    
                case BeaconType.eddystone:
                    return EddystoneIconView(beaconInfo: beaconInfo)
                default:break
                }
            }
            break
        }
        return nil
    }
    
    func isSameBeacon(_ beacon:AnyObject ,bType:BeaconVendorType)->Bool{
        
        if type != bType
        {return false}
        
        switch(type!){
            
        case BeaconVendorType.nativeiBeacon:
            if let clBeacon = beaconObj as? CLBeacon {
                return clBeacon.isSame(beacon as? CLBeacon)
            }
            break
            
        case BeaconVendorType.thirdPatyBeacon:
            
            if let beaconInfo = beaconObj as? AtmelBeacon{
                return (beaconInfo.peripheral.identifier == (beacon as? AtmelBeacon)?.peripheral?.identifier)
            }
            
            break
        }
        return false
    }
    
    func updateArrowPosition(){
        
        let centreX = self.bounds.width/2
        let centreY = self.bounds.height/2
        
        switch(viewArrowDirection!){
        case InfoViewArrowDirection.up:
            
            let arrowViewSize = CGSize(width: 16, height: 9)
            arrowView.frame = CGRect(x: (centreX - arrowViewSize.width/2),y: (self.bounds.maxY+arrowViewSize.height), width: arrowViewSize.width, height: arrowViewSize.height)
           
            arrowView.image = UIImage(named: "BeaconInfoArrowUp")
            arrowView.isHidden = false

            break
        case InfoViewArrowDirection.down:
            
            let arrowViewSize = CGSize(width: 16, height: 9)
            arrowView.frame = CGRect(x: (centreX - arrowViewSize.width/2),y: -(arrowViewSize.height), width: arrowViewSize.width, height: arrowViewSize.height)
            arrowView.image = UIImage(named: "BeaconInfoArrowDown")
            arrowView.isHidden = false

            break
        case InfoViewArrowDirection.left:
            
            let arrowViewSize = CGSize(width: 9, height: 16)
            arrowView.frame = CGRect(x: self.bounds.maxX+0.5,y: (centreY - arrowViewSize.height/2),width: arrowViewSize.width, height: arrowViewSize.height)
            arrowView.image = UIImage(named: "BeaconInfoArrowLeft")
           
            arrowView.isHidden = false

            break
        case InfoViewArrowDirection.right:
            
            let arrowViewSize = CGSize(width: 9, height: 16)
            arrowView.frame = CGRect(x: -(arrowViewSize.width),y: (centreY - arrowViewSize.height/2),width: arrowViewSize.width, height: arrowViewSize.height)
            arrowView.image = UIImage(named: "BeaconInfoArrowRight")
            arrowView.isHidden = false

            break
        case InfoViewArrowDirection.none:
            
            arrowView.isHidden = true
            
            break
        }
        
    }

    @IBAction func beaconIconButtonAction(_ sender: UIButton) {
        
        if delegate != nil{
            self.delegate?.beaconIconViewTapped(self)
        }
    }
    
    
    // MARK:- Handle timer
    
    fileprivate func scheduleTimer() -> Timer{
        
        return Timer.scheduledTimer(timeInterval: TIME_INTERVAL, target: self, selector: #selector(BeaconIconView.removeBeaconFromSuperView), userInfo: nil, repeats: false)
    }
    
    
    func removeBeaconFromSuperView() {
        self.removeFromSuperview()
        
        if infoView != nil && infoView?.superview != nil {
            infoView?.removeFromSuperview()
        }
    }
    
    func updateTimer(){
        if timer != nil {
            timer?.invalidate()
        }
        timer = scheduleTimer()
    }
}

extension BeaconIconView{
    
    // Abstract class method
    func updateDataWithBeaconObject(_ beaconObjet:AnyObject){
        
    }
}
