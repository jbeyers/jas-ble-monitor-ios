/* ***************************************************************************

ImmediateAlertVC.swift

UIViewController Class to  Display theImmediate Alert service feature from BLE peripheral



Copyright (c) 2015, Atmel Corporation. All rights reserved.
Released under NDA
Licensed under Atmel's Limited License Agreement.


THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Atmel Corporation: http://www.atmel.com

******************************************************************************/

import Foundation
import CoreBluetooth
import UIKit

enum RangeCircle
{
    case eSafe
    case eMid
    case eDanger
    case eNone
}


class ImmediateAlertVC: GATTBaseViewController,UITextFieldDelegate {
    
    var proximityInterface:BLEProximityInterface!
    var lastKnownRange : RangeCircle! = RangeCircle.eNone
    var minimumRSSIRead:CGFloat = 0.0

    var dangerZoneAlertLevel:AlertLevel = .highAlert
    var midZoneAlertLevel:AlertLevel = .mildAlert
    var safeZoneAlertLevel:AlertLevel = .noAlert
    
    var minRSSISafe:CGFloat = Constants.RSSI.minRSSISafe
    var minRSSIMid:CGFloat  = Constants.RSSI.minRSSIMid
    
    //To enable Testing view
    let isTestView = false
    
    fileprivate var isReadyToShowSettings = false

    @IBOutlet weak var circleViewWidth: NSLayoutConstraint!

    @IBOutlet weak var rssiValueLabel : UILabel!

    @IBOutlet weak var RangeCircleWidth: NSLayoutConstraint!
    @IBOutlet weak var MidCircleWidth: NSLayoutConstraint!
    @IBOutlet weak var SafeCircleWidth: NSLayoutConstraint!
    @IBOutlet weak var CircleDanger: UIView!
    @IBOutlet weak var CircleMid: UIView!
    @IBOutlet weak var CircleSafe: UIView!
    @IBOutlet weak var RangeIndicatorCircle: CircleView!
    @IBOutlet weak var RangeIndicatorContainer: UIView!
    
    @IBOutlet weak var testRSSITextField: UITextField!
    @IBOutlet weak var AlertStatus: UILabel!
    
    
   
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.layoutFix()
        
            }

    override func viewDidLoad()
    {
        super.viewDidLoad()
        initProximityInterface()
        handleTestView()
    }
    

    
    
    func initProximityInterface()
    {
        self.navigationItem.title = GlobalCommunicator.shared.selectedService?.uuid.getServiceName()
        self.proximityInterface = nil
        self.proximityInterface=BLEProximityInterface(sourceDevice:  GlobalCommunicator.shared.sourceDevice)
        self.proximityInterface.delegate=self;
        self.proximityInterface.readRSSIValue()
    }
    

    override func viewWillAppear(_ animated: Bool) {
        


        super.viewWillAppear(animated)
        
        addListner()
        isReadyToShowSettings = false
        
        if UIDevice.is_iPad()
        {
            UIView.animate(withDuration: 0.20, animations: { () -> Void in
                self.circleViewWidth.constant = CGFloat(Constants.iPadFrame.iPadDefaultWidthSize)
            })
        }
        
        getZoneValues()
        getAlertLevelSettings()
        updateImmediateAlert(lastKnownRange)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    
        removeListner()
    }
    
    
    func getZoneValues()
    {
        
        if  UserDefaults.standard.double(forKey: Constants.UserDefaultsKey.safeZoneValueKey) == 0
        {
            UserDefaults.standard.set(Double(minRSSISafe * -1), forKey: Constants.UserDefaultsKey.safeZoneValueKey)
            UserDefaults.standard.set(Double(minRSSIMid * -1), forKey: Constants.UserDefaultsKey.midZoneValueKey)
          
        }
        else
        {
            minRSSISafe = -CGFloat(UserDefaults.standard.double(forKey: Constants.UserDefaultsKey.safeZoneValueKey))
            minRSSIMid = -CGFloat(UserDefaults.standard.double(forKey: Constants.UserDefaultsKey.midZoneValueKey))
        }

    }
    /*----------------------------------------------------------------------------------------------------------*/
    // MARK: prepareForSegue Methods
    /*----------------------------------------------------------------------------------------------------------*/

    
    @IBAction func onSettingsTouched(_ sender :AnyObject)
    {
        self.view.endEditing(true)
        isReadyToShowSettings = true
        self.performSegue(withIdentifier: ServiceScreenSegueEnum.PS_VC.rawValue, sender: self)
    }


    func setCircleWidth(){
        self.SafeCircleWidth.constant = findlayoutRadiusFromRSSI(NSNumber(value: Float(minRSSISafe) as Float))
        self.MidCircleWidth.constant = findlayoutRadiusFromRSSI(NSNumber(value: Float(minRSSIMid) as Float))
        self.view.layoutIfNeeded()
    }
    
    func getAlertLevelSettings()
    {
        let userDefaults:UserDefaults = UserDefaults.standard
        let safeAlertLevel: NSNumber? = userDefaults.value(forKey: Constants.UserDefaultsKey.safeZoneKey) as? NSNumber
        let midAlertLevel:NSNumber? = userDefaults.value(forKey: Constants.UserDefaultsKey.midZoneKey) as? NSNumber
        let dangerAlertLevel:NSNumber? = userDefaults.value(forKey: Constants.UserDefaultsKey.dangerZoneKey) as? NSNumber
        
        if safeAlertLevel != nil {
            self.safeZoneAlertLevel = AlertLevel(rawValue: (safeAlertLevel?.uint8Value as UInt8?)!)!
        }
        if midAlertLevel != nil {
            self.midZoneAlertLevel = AlertLevel(rawValue: (midAlertLevel?.uint8Value as UInt8?)!)!
        }
        if dangerAlertLevel != nil {
            self.dangerZoneAlertLevel = AlertLevel(rawValue: (dangerAlertLevel?.uint8Value as UInt8?)!)!
        }
    }

    
  
    
    
    /*----------------------------------------------------------------------------------------------------------*/
    // MARK: Debugging View
    /*----------------------------------------------------------------------------------------------------------*/
    
    @IBOutlet weak var TestMinRSSITextField: UITextField!
    @IBOutlet weak var TestMaxRSSITextField: UITextField!
    @IBOutlet weak var TestView: UIView!
    @IBOutlet weak var TestrssiValueLabel: UILabel!
    
    
    func handleTestView()
    {
        TestView.isHidden = !isTestView
        
        
        self.TestMaxRSSITextField.delegate=self
        self.TestMinRSSITextField.delegate=self
        
        self.TestMinRSSITextField.text = Constants.RSSI.minimumRSSI.description
        self.TestMaxRSSITextField.text = Constants.RSSI.maximumRSSI.description
        
        self.TestMinRSSITextField.keyboardType = .numbersAndPunctuation
        self.TestMaxRSSITextField.keyboardType = .numbersAndPunctuation
        
    }

    
    
}


extension ImmediateAlertVC
{
    
    /*----------------------------------------------------------------------------------------------------------*/
    // MARK: RSSI zone calculations Methods
    /*----------------------------------------------------------------------------------------------------------*/
    

    
    func findlayoutRadiusFromRSSI(_ rssi:NSNumber!)->CGFloat
    {
        return 0.0
        /*
        let maximumCircleRadius:CGFloat = self.view.frame.size.width
        let currentRSSI = CGFloat(rssi)
        
        if currentRSSI < minimumRSSIRead {
            
            minimumRSSIRead = currentRSSI
        }
        let radius = ((Constants.RSSI.maximumRSSI - currentRSSI) * ( maximumCircleRadius / (Constants.RSSI.maximumRSSI - Constants.RSSI.minimumRSSI)))
        println("radius = \(radius) width = \(SafeCircleWidth.constant) radius = \(CircleSafe.frame.size.width)")
        return radius
        */
    }
    
    func findlayoutRadiusFromRSSIConstantZoneWidths(_ rssi:NSNumber!)->CGFloat{
        
        var maximumCircleRadius:CGFloat=0
        var minimumZoneRadius:CGFloat=0
        var minimumCircleRadius:CGFloat=0
        var maximumRSSIZone:CGFloat=0
        let currentRSSI = CGFloat(rssi)
        
        if currentRSSI >= minRSSISafe{
            minimumCircleRadius=0
            maximumRSSIZone = Constants.RSSI.maximumRSSI
            minimumZoneRadius=minRSSISafe
            maximumCircleRadius = CircleSafe.frame.size.width
        }
            
        else if currentRSSI >= minRSSIMid {
            minimumCircleRadius=CircleSafe.frame.size.width
            maximumRSSIZone = minRSSISafe
            minimumZoneRadius = minRSSIMid
            maximumCircleRadius = CircleMid.frame.size.width
            
        }
        else {
            minimumCircleRadius=CircleMid.frame.size.width
            maximumRSSIZone = minRSSIMid
            minimumZoneRadius = Constants.RSSI.minimumRSSI
            maximumCircleRadius = CircleDanger.frame.size.width
            
        }
        
        if currentRSSI < minimumRSSIRead {
            
            minimumRSSIRead = currentRSSI
        }
        
        let radius = minimumCircleRadius+((maximumRSSIZone - currentRSSI) * ((maximumCircleRadius - minimumCircleRadius) / (maximumRSSIZone - minimumZoneRadius)))
        
//        println("rssi = \(rssi) radius = \(radius) width = \(SafeCircleWidth.constant) radius = \(CircleMid.frame.size.width)")
        return radius
    }
    
    func getCurrentRangeCircle(_ range :CGFloat) -> RangeCircle
    {
        if range > ( CircleMid.frame.size.width  )
        {
            return RangeCircle.eDanger
        }
        else if range > ( CircleSafe.frame.size.width  )
        {
            return RangeCircle.eMid
        }
        return RangeCircle.eSafe
        
    }



}


/*----------------------------------------------------------------------------------------------------------*/
// MARK: BLEProximityInterfaceDelegate Methods
/*----------------------------------------------------------------------------------------------------------*/


extension ImmediateAlertVC:BLEProximityInterfaceDelegate{
    
    func bleProximityInterfaceDidReadLinkLossLevel(_ alertLevel:AlertLevel){

// -------------Features removed from theImmediateAlertVC
//        println("\n  received linkloss value as= \(alertLevel.rawValue) in ImmediateVC")
//        self.delegate?.readLinkLossLevel(alertLevel)
    }
    
    func bleProximityInterfaceDidWriteValueForLinkLossCharacteristic(_ error: NSError!)
    {
// -------------Features removed from theImmediateAlertVC
//        self.delegate?.didWriteLinkLoss(error)
    }
    
    func bleProximityInterfaceDidReadTransmissionPowerLevel(_ txPowerLevel:Int){
        
    }
    
    func bleProximityInterfaceDidReadRSSIValue(_ RSSI:NSNumber!)
    {
        if let _ = RSSI
        {
            self.TestrssiValueLabel.text=RSSI.stringValue //For Testing
            self.rssiValueLabel.text = " \(RSSI.stringValue) dBm"
            let range = findlayoutRadiusFromRSSIConstantZoneWidths(RSSI)
            self.performAnimation(range,RSSI: RSSI)
            self.findProximityRange(range)
            
        }

    }
    
    
    func performAnimation(_ range : CGFloat,RSSI:NSNumber?)
    {
        self.RangeIndicatorCircle.setNeedsDisplay()
        UIView.animate(withDuration: 1.0, animations: { () -> Void in
            self.RangeCircleWidth.constant = range
            self.view.layoutIfNeeded()
            
            }, completion: { (complete) -> Void in
                self.proximityInterface.readRSSIValue()

        }) 
    }
    
    func findProximityRange(_ range : CGFloat)
    {
        let rangeCircle = getCurrentRangeCircle(range)
        if lastKnownRange == RangeCircle.eNone
        {
            lastKnownRange = rangeCircle
            updateImmediateAlert(rangeCircle)
        }
        else if lastKnownRange != rangeCircle
        {
            lastKnownRange = rangeCircle
            updateImmediateAlert(rangeCircle)
        }
        
    }
    
    
    func updateImmediateAlert(_ rangeCircle : RangeCircle)
    {
        
        switch(rangeCircle)
        {
        case RangeCircle.eDanger:
            self.proximityInterface.writeImmediateAlertLevel(self.dangerZoneAlertLevel)
        case RangeCircle.eMid:
            self.proximityInterface.writeImmediateAlertLevel(self.midZoneAlertLevel)
        case RangeCircle.eSafe:
            self.proximityInterface.writeImmediateAlertLevel(self.safeZoneAlertLevel)
        default:
            print("")
        }
        
    }
    

}

extension ImmediateAlertVC
{
    func addListner()
    {
        if isReadyToShowSettings == false
        {
            NotificationCenter.listenNotification(self, notificationName: Constants.NotificationName.onServiceDiscovered)
        }
    }
    
    func removeListner()
    {
        if isReadyToShowSettings == false
        {
            NotificationCenter.removelistener(self, notificationName: Constants.NotificationName.onServiceDiscovered)
        }
    }
    
    func onServiceDiscovered()
    {
        initProximityInterface()
    }

}
/*
extension ImmediateAlertVC:UITextFieldDelegate{
    
    func textFieldDidEndEditing(textField: UITextField) {
        
        if textField .isEqual(self.TestMinRSSITextField){
            let currentSetMinimumValue:Int?=textField.text.toInt()
            
            if currentSetMinimumValue != nil {
                    minimumRSSI = CGFloat(currentSetMinimumValue!)
            }
        }
        else if textField.isEqual(self.TestMaxRSSITextField){
            
            if textField .isEqual(self.TestMaxRSSITextField){
                let currentSetMaximumValue:Int?=textField.text.toInt()
                
                if currentSetMaximumValue != nil {
                    maximumRSSI = CGFloat(currentSetMaximumValue!)
                }
            }
            
        }
        else if textField.isEqual(testRSSITextField){
            let integerValue:Int = textField.text.toInt()!
            bleProximityInterfaceDidReadRSSIValue(NSNumber(integer: integerValue))
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool{
        
        self.view .endEditing(true)
        return true
    }
}
*/
