/***************************************************************************
 
 OTAProgressInfoView.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}


protocol OTAProgressInfoViewDelegate:NSObjectProtocol{
 
    func otaInfoViewTapped()
}


class OTAProgressInfoView: UIView {
    
    var delegate:OTAProgressInfoViewDelegate?
    
    var iconImageView:UIImageView!
    var progressView:UIProgressView!
    var percentLabel:UILabel!

    init(){
    
    super.init(frame: CGRect(x: 0, y: 0, width: 55, height: 32))
    
    iconImageView = UIImageView(frame: CGRect(x: 0, y: 8, width: 16, height: 16))
    iconImageView.image = UIImage(named:"otaIcon")
    
    percentLabel = UILabel(frame:CGRect(x: 25, y: 5, width: 30, height: 20))
    percentLabel.textAlignment = .center
//    percentLabel.adjustsFontSizeToFitWidth = true
    percentLabel.text = "0%"
    percentLabel.adjustsFontSizeToFitWidth = true
    percentLabel.textColor = UIColor.white
    percentLabel.font = UIFont(name: "Roboto-Regular", size:12.0)
    percentLabel.sizeToFit()
    progressView = UIProgressView(frame: CGRect(x: 17, y: 25, width: percentLabel.frame.width, height: 2))
    progressView.progress = 0.0
//    progressView.progressTintColor = UIColor.colorWithHexString("")
    progressView.progressTintColor = UIColor.white
    progressView.trackTintColor = UIColor.colorWithHexString("004a76")
    
    let button: UIButton = UIButton(type:.custom)
    button.addTarget(self, action:#selector(OTAProgressInfoView.otauButton(_:)), for: UIControlEvents.touchUpInside)
    button.frame = self.bounds
    button.backgroundColor = UIColor.clear
    self.addSubview(iconImageView)
    self.addSubview(percentLabel)
    self.addSubview(progressView)
    self.addSubview(button)

    progressView.center = CGPoint(x: percentLabel.center.x, y: progressView.center.y)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setProgress(_ progress:Float?){
        if progress != nil && progress >= 0 {
            self.percentLabel.text = "\(Int(progress!*100))%"
            self.progressView.progress = progress!
        }
    }
    func setPauseState(_ progress:Float?){

        self.percentLabel.text = "Paused"

        if progress != nil && progress >= 0 {
            self.progressView.progress = progress!
        }
    }
    func otauButton(_ sender: UIButton){
    
        self.delegate?.otaInfoViewTapped()
    }


}

