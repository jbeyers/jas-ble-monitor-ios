/* ***************************************************************************

TxPowerVC.swift

UIViewController Class to  Display  current transmit power level from BLE peripheral



Copyright (c) 2015, Atmel Corporation. All rights reserved.
Released under NDA
Licensed under Atmel's Limited License Agreement.


THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Atmel Corporation: http://www.atmel.com

******************************************************************************/

import Foundation
import CoreBluetooth

import UIKit

class TxPowerVC: GATTBaseViewController
{
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.layoutFix()
        
            }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
               
        initTxPower()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    

    
    
    func initTxPower()
    {
        self.navigationItem.title = GlobalCommunicator.shared.selectedService?.uuid.getServiceName()
        let proximity  = BLEProximityInterface(sourceDevice: GlobalCommunicator.shared.sourceDevice)
        proximity.delegate = self
        proximity.readTransmissionPowerLevelValue()

    }
    
    @IBOutlet weak var txPowerLabel: UILabel!
    
    
}

/*----------------------------------------------------------------------------------------------------------*/
// MARK: BLESourceDevice Delegate Methods
/*----------------------------------------------------------------------------------------------------------*/

extension TxPowerVC: BLEProximityInterfaceDelegate
{
    func bleProximityInterfaceDidReadTransmissionPowerLevel(_ txPowerLevel: Int) {
        
        print(txPowerLevel)
        txPowerLabel.text = "\(txPowerLevel) dBm"
        
        
    }
    
    func bleProximityInterfaceDidReadLinkLossLevel(_ alertLevel: AlertLevel) {
        
    }
    
    func bleProximityInterfaceDidReadRSSIValue(_ RSSI: NSNumber!) {
        
    }
    
    func bleProximityInterfaceDidWriteValueForLinkLossCharacteristic(_ error: NSError!) {
        
    }
    
}



