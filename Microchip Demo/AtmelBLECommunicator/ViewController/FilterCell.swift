/* ***************************************************************************

//FilterCell.swift
//
UITableviewcell subclass;
View for each cell to be populated to the filtertable in Settings Viewcontroller
//

Copyright (c) 2015, Atmel Corporation. All rights reserved.
Released under NDA
Licensed under Atmel's Limited License Agreement.


THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Atmel Corporation: http://www.atmel.com

******************************************************************************/

import Foundation
import UIKit

class FilterCell: UITableViewCell
{
    @IBOutlet var profileName: UILabel!
    @IBOutlet var statusSwitch: UISwitch!
    
    
    func setFilterCell(_ tag : Int)
    {
        let profileText = BLEProfile.allValues[tag].rawValue
        self.profileName.text = profileText
        self.statusSwitch.tag = tag
        

        if let filterArray = UserDefaults.standard.array(forKey: Constants.UserDefaultsKey.filterKey)
        {
            if filterArray.contains(profileText) == true
            {
                self.statusSwitch.isOn = true
            }
            else
            {
                self.statusSwitch.isOn = false
            }
        }
        else
        {
            self.statusSwitch.isOn = false
        }
        
        
        
    }
    
   @IBAction func filterModified(_ switchButton: UISwitch)
    {
        
        let profileText = BLEProfile.allValues[switchButton.tag].rawValue
        var tempFilterArray : Array<AnyObject>
        if let filterArray = UserDefaults.standard.array(forKey: Constants.UserDefaultsKey.filterKey)
        {
            var filterSet = NSSet(array: filterArray) as Set
            if filterSet.contains(profileText as NSObject) == true
            {
                //filterSet.remove(profileText)
                filterSet.remove(profileText as NSObject)
            }
            else
            {
                filterSet.insert(profileText as NSObject)
            }
             tempFilterArray = Array(filterSet)
        }
        else
        {
            tempFilterArray = Array([profileText]) as Array<AnyObject>
        }
        
        //SettingsVC.setFilterValue(tempFilterArray, forKey: Constants.UserDefaultsKey.filterKey)
        SettingsVC.setFilterValue(tempFilterArray as AnyObject, forKey: Constants.UserDefaultsKey.filterKey)
        NotificationCenter.trigger(Constants.NotificationName.refreshMyTableView)
        
    }

}

