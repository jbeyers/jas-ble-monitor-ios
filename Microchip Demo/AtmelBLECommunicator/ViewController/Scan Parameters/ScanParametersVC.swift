
/* ***************************************************************************

ScanParametersVC.swift



Copyright (c) 2015, Atmel Corporation. All rights reserved.
Released under NDA
Licensed under Atmel's Limited License Agreement.


THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Atmel Corporation: http://www.atmel.com

******************************************************************************/
import UIKit


enum ScanUnit:Int{
    case milliSeconds = 1
    case seconds = 2
}
class ScanParametersVC: GATTBaseViewController {


    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var scanIntervalTextField: UITextField!
    @IBOutlet weak var scanIntervalUnitBtn: UIButton!
    
    @IBOutlet weak var scanWindowTextField: UITextField!
    @IBOutlet weak var scanWindowUnitBtn: UIButton!
     @IBOutlet weak var notifySwitch: UISwitch!
    
     @IBOutlet weak var adjustHeihtWithKeyboard: NSLayoutConstraint!
    
    var scanParameterService: ScanParameterService?
    var scanValues:ScanParameters?
    var currentScanIntervalUnit:ScanUnit?
    var currentScanWindowUnit:ScanUnit?
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.layoutFix()
        
            }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.navigationItem.title = GlobalCommunicator.shared.selectedService?.uuid.getServiceName()

        addListner()
        // Do any additional setup after loading the view.
    
        writeInitialValue()
        configureNotifyControl()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        removeListner()
        updateScanParamUserDefault()
        checkAndStopNotify()
        
    }
    
    func checkAndStopNotify()
    {
        if notifySwitch.isOn == true
        {
            stopNotifyScPS()
        }
    }
    
    func stopNotifyScPS()
    {
         self.scanParameterService?.stopScanRefreshNotifications()
    }
    
    func updateScanParamUserDefault()
    {
        
        if let scnInterval = self.scanParameterService?.scanParameters?.scanInterval
        {
            UserDefaults.standard.set(scnInterval, forKey: Constants.UserDefaultsKey.scanParamIntervalValueKey)
            if let scnWindow = self.scanParameterService?.scanParameters?.scanWindow
            {
                UserDefaults.standard.set(scnWindow, forKey: Constants.UserDefaultsKey.scanParamWindowValueKey)
                
                updateScanParamUnit()
                 UserDefaults.standard.synchronize()
            }
        }
    }
    
    
    func updateScanParamUnit()
    {
        UserDefaults.standard.set(currentScanIntervalUnit!.rawValue, forKey: Constants.UserDefaultsKey.scanParamIntervalUnitValueKey)
        UserDefaults.standard.set(currentScanWindowUnit!.rawValue, forKey: Constants.UserDefaultsKey.scanParamWindowUnitValueKey)
        
    }

    
    
    func getScanParamUnit()
    {

        let intervalUnitValue = UserDefaults.standard.integer(forKey: Constants.UserDefaultsKey.scanParamIntervalUnitValueKey)
        let windowUnitValue = UserDefaults.standard.integer(forKey: Constants.UserDefaultsKey.scanParamWindowUnitValueKey)
        currentScanIntervalUnit = ScanUnit(rawValue: intervalUnitValue)
        currentScanWindowUnit = ScanUnit(rawValue: windowUnitValue)
    }
    
    
    func getInitialScanParams() -> ScanParameters
    {
        
        let intervalValue = UserDefaults.standard.integer(forKey: Constants.UserDefaultsKey.scanParamIntervalValueKey)
        if intervalValue < 3
        {
            currentScanIntervalUnit = ScanUnit.milliSeconds
            currentScanWindowUnit = ScanUnit.milliSeconds
            return ScanParameters(scanIntervalValue: 3,scanWindowValue: 3)
        }
        else
        {
            
            getScanParamUnit()
            let windowValue = UserDefaults.standard.integer(forKey: Constants.UserDefaultsKey.scanParamWindowValueKey)
            return ScanParameters(scanIntervalValue: intervalValue,scanWindowValue: windowValue)
        }
    }
    


    func writeInitialValue()
    {
        
        let defaultScanParam = getInitialScanParams()
        setDefaults(defaultScanParam)
        scanParameterService = ScanParameterService(device: GlobalCommunicator.shared.sourceDevice, andDelegate: nil, refreshMode: ScanRefreshMode.active, scanParametersValue: defaultScanParam)
    }
    

    
    func setDefaults(_ scanParam : ScanParameters)
    {
        if currentScanIntervalUnit == ScanUnit.milliSeconds
        {
            self.scanIntervalTextField.text = "\(scanParam.scanInterval!)"
        }
        else
        {
            self.scanIntervalTextField.text = "\(scanParam.scanInterval!/1000)"
             self.scanIntervalUnitBtn.setTitle("s", for: UIControlState())
        }
        if currentScanWindowUnit ==  ScanUnit.milliSeconds
        {
            self.scanWindowTextField.text = "\(scanParam.scanWindow!)"
        }
        else
        {
            self.scanWindowTextField.text = "\(scanParam.scanWindow!/1000)"
            self.scanWindowUnitBtn.setTitle("s", for: UIControlState())
        }
    }
    
    // MARK: Action methods
    
    @IBAction func ScanUnitBtnTapped(_ sender: UIButton)
    {
        print("\(sender.tag)")
        
        let actionSheet = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "ms", "s")
        actionSheet.show(in: self.view)
        
        switch (sender.tag)
        {
        case 100:
            actionSheet.tag = 100
            break;

        case 200:
            actionSheet.tag = 200
            break;

        default:
            print("\(sender.tag)")
            break;

        }
        
        self.scanIntervalTextField.resignFirstResponder()
        self.scanWindowTextField.resignFirstResponder()
        

    }
    
    func configureNotifyControl()
    {
        let state = UserDefaults.getNotificationStatus(Constants.UserDefaultsKey.scanParamStatusKey)
        notifySwitch.isOn = state
        scanRefreshSwitchStateChanged(notifySwitch)
    }
    
    @IBAction func scanRefreshSwitchStateChanged(_ sender: UISwitch)
    {
        if sender.isOn
        {
            self.scanParameterService?.startScanRefreshNotifications()
        }
        else
        {
            self.scanParameterService?.stopScanRefreshNotifications()
        }
         UserDefaults.updateNotifyState(sender.isOn , UserDefaultsKey: Constants.UserDefaultsKey.scanParamStatusKey)
    }
    


    

       // MARK:
    
    @IBAction func viewTapped(_ sender: UITapGestureRecognizer) {
        
        self.view.endEditing(true)
    }
    
    
}


// MARK: UIActionSheet delegate

extension ScanParametersVC:UIActionSheetDelegate{
    
    func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int)
    {
        if buttonIndex == 1 || buttonIndex == 2
        {
            if actionSheet.tag == 100
            {
                self.scanIntervalUnitBtn.setTitle(actionSheet.buttonTitle(at: buttonIndex), for: UIControlState())
                
                if currentScanIntervalUnit != ScanUnit(rawValue: buttonIndex){
                    
                    
                    currentScanIntervalUnit = ScanUnit(rawValue: buttonIndex)
                    
                    var scanIntervalValue = Int(scanIntervalTextField.text!)
                    
                    if currentScanIntervalUnit == ScanUnit.seconds{
                        
                        scanIntervalValue = scanIntervalValue!*1000
                        if !(scanIntervalValue!.isValidScanInterval()){
                            
                            let alertView: UIAlertView = UIAlertView(title: "Alert", message: "Value out of range, Default value set", delegate: nil, cancelButtonTitle: "OK")
                            alertView.tag = 102
                            alertView.show()
                            self.scanIntervalTextField.text = "1"
                            self.scanParameterService?.scanParameters?.scanInterval = 1000
                        }
                        else
                        {
                            self.scanParameterService?.scanParameters?.scanInterval = scanIntervalValue!
                        }
                    }
                    else{
                        if !(scanIntervalValue!.isValidScanInterval()){
                            
                            let alertView: UIAlertView = UIAlertView(title: "Alert", message: "Value out of range, Default value set", delegate: nil, cancelButtonTitle: "OK")
                            alertView.tag = 102
                            alertView.show()
                            self.scanIntervalTextField.text = "3"
                            self.scanParameterService?.scanParameters?.scanInterval = 3
                        }
                        else
                        {
                            self.scanParameterService?.scanParameters?.scanInterval = scanIntervalValue
  
                        }
                        
                    }
                }
                
            }
            else
            {
                self.scanWindowUnitBtn.setTitle(actionSheet.buttonTitle(at: buttonIndex), for: UIControlState())
                
                if(currentScanWindowUnit != ScanUnit(rawValue: buttonIndex)){
                    
                    currentScanWindowUnit = ScanUnit(rawValue: buttonIndex)
                    
                    var scanWindowValue = Int(scanWindowTextField.text!)
                    
                    if currentScanWindowUnit == ScanUnit.seconds{
                        
                        scanWindowValue = scanWindowValue!*1000
                        
                        if !(scanWindowValue!.isValidScanWindow()){
                            
                            let alertView: UIAlertView = UIAlertView(title: "Alert", message: "Value out of range, Default value set", delegate: nil, cancelButtonTitle: "OK")
                            alertView.tag = 102
                            alertView.show()
                            self.scanWindowTextField.text = "1"
                            self.scanParameterService?.scanParameters?.scanWindow = 1000
                        }
                        else
                        {
                            self.scanParameterService?.scanParameters?.scanWindow = scanWindowValue
  
                        }
                    }
                    else{
                        if !(scanWindowValue!.isValidScanWindow()){
                            
                            let alertView: UIAlertView = UIAlertView(title: "Alert", message: "Value out of range, Default value set", delegate: nil, cancelButtonTitle: "OK")
                            alertView.tag = 102
                            alertView.show()
                            self.scanWindowTextField.text = "3"
                            self.scanParameterService?.scanParameters?.scanWindow = 3
                        }
                        else
                        {
                            self.scanParameterService?.scanParameters?.scanWindow = scanWindowValue

                            
                        }
                        
                    }
                }

            }
            
        }
        else
        {
            actionSheet.removeFromSuperview()
        }
        
    }
    
    
}

// MARK: UITextField delegate

extension ScanParametersVC:UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.endEditing(true)
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if textField.isEqual(scanIntervalTextField)
        {
            if var scanIntervalValue = Int(textField.text!)
            {
                
                if currentScanIntervalUnit == ScanUnit.seconds
                {scanIntervalValue = scanIntervalValue*1000}
                
                if scanIntervalValue.isValidScanInterval()
                {scanParameterService?.scanParameters?.scanInterval = scanIntervalValue}
                else
                {
                    if currentScanIntervalUnit == ScanUnit.seconds
                    {textField.text = NSString(format: "%d",(scanParameterService?.scanParameters?.scanInterval)!/1000) as String}
                    else
                    {textField.text = NSString(format: "%d",(scanParameterService?.scanParameters?.scanInterval)!) as String}
                    
                    let alertView: UIAlertView = UIAlertView(title: "Alert", message: "Value out of range", delegate: nil, cancelButtonTitle: "OK")
                    alertView.tag = 101
                    alertView.show()
                }
            }
            else
            {
                let alertView: UIAlertView = UIAlertView(title: "Alert", message: "Please enter a valid integer value", delegate: nil, cancelButtonTitle: "OK")
                alertView.tag = 101
                alertView.show()
                
                if currentScanIntervalUnit == ScanUnit.seconds
                {textField.text = NSString(format: "%d",(scanParameterService?.scanParameters?.scanInterval)!/1000) as String}
                else
                {textField.text = NSString(format: "%d",(scanParameterService?.scanParameters?.scanInterval)!) as String}
            }
        }
        else if textField.isEqual(scanWindowTextField)
        {
            if var scanWindowValue = Int(textField.text!)
            {
                if currentScanWindowUnit == ScanUnit.seconds
                {scanWindowValue = scanWindowValue*1000}
                if scanWindowValue.isValidScanWindow()
                {scanParameterService?.scanParameters?.scanWindow = scanWindowValue}
                else
                {
                    if currentScanWindowUnit == ScanUnit.seconds
                    {textField.text = NSString(format: "%d",(scanParameterService?.scanParameters?.scanWindow)!/1000) as String}
                    else
                    {textField.text = NSString(format: "%d",(scanParameterService?.scanParameters?.scanWindow)!) as String}
                    
                    let alertView: UIAlertView = UIAlertView(title: "Alert", message: "Value out of range", delegate: nil, cancelButtonTitle: "OK")
                    alertView.tag = 102
                    alertView.show()
                }
            }
            else
            {
                let alertView: UIAlertView = UIAlertView(title: "Alert", message: "Please enter a valid integer value", delegate: nil, cancelButtonTitle: "OK")
                alertView.tag = 102
                alertView.show()
                
                if currentScanWindowUnit == ScanUnit.seconds
                {textField.text = NSString(format: "%d",(scanParameterService?.scanParameters?.scanWindow)!/1000) as String}
                else
                {textField.text = NSString(format: "%d",(scanParameterService?.scanParameters?.scanWindow)!) as String}
            }
        }
        
    }
    

}


    extension Int{
        func isValidScanInterval()->Bool{
            
            if (self > 10240) || (self < 3)
                {return false}
            else
                {return true}
        }
        
        func isValidScanWindow()->Bool{
            
            if (self > 10240) || (self < 3)
            {return false}
            else
            {return true}
        }
    }

    extension ScanParametersVC
    {
        //adjustHeihtWithKeyboard
        func addListner()
        {
            NotificationCenter.listenNotification(self, notificationName: Constants.NotificationName.onServiceDiscovered)
            
            NotificationCenter.default.addObserver(self, selector: #selector(ScanParametersVC.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(ScanParametersVC.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

        }
        
        func removeListner()
        {
            NotificationCenter.removelistener(self, notificationName: Constants.NotificationName.onServiceDiscovered)
            
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        }
        
        

        func keyboardWillShow(_ notification : Notification)
        {
            if let userInfo = notification.userInfo
            {
                if let keyboardRect = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
                {
                    mainScrollView.frame.size.height = (view.frame.size.height - 50) - keyboardRect.size.height
                }
            }
         }
        
        func keyboardWillHide()
        {
            mainScrollView.frame.size.height = (view.frame.size.height - 50)
        }
        
        func onServiceDiscovered()
        {
//            initBatteryServiceView()
        }
        
        
}
