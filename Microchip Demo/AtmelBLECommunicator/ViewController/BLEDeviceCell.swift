/* ***************************************************************************

//BLEDeviceCell.swift
//
UITableViewCell subclass; Used to display cells corresponding to each device discovered in the device scan
//


Copyright (c) 2015, Atmel Corporation. All rights reserved.
Released under NDA
Licensed under Atmel's Limited License Agreement.


THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Atmel Corporation: http://www.atmel.com

******************************************************************************/



import Foundation
import CoreBluetooth


import UIKit

class BLEDeviceCell : UITableViewCell  {
    
    @IBOutlet var deviceName: UILabel!
    @IBOutlet var deviceAddress: UILabel!
    @IBOutlet var deviceRSSI: UILabel!
    @IBOutlet var deviceThumbImageView: UIImageView!
    @IBOutlet var signalStrengthView: UIImageView!
    
    var signalImages:[UIImage]!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
      override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setSignalImages()
    }

    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
         setSignalImages()
//        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func setCell(_ bleDeviceData : BLEDeviceInfo!)
    {
        //To get RSSI updates
        
        self.setupUIElements()
        var nameString = bleDeviceData.BLEPeripheralLocalName // First check for Advert data for peripheral name
        if nameString == nil || nameString == "" {
                nameString = "N/A"
        }
        
        self.deviceName.text = nameString
        
        if let serviceUUIDs = bleDeviceData.BLEPeripheralServices
        {
            self.deviceAddress.text = ""//bleDeviceData.BLEPeripheral.identifier.UUIDString
//            Replaced Address label to Service Count
            var service = "service"
            if serviceUUIDs.count > 1
            {
                service = "services"
            }
            self.deviceAddress.text = "\(serviceUUIDs.count) \(service)"
            

        }

        updateSignalImage(bleDeviceData.BLEPeripheralRSSI)
        
        if UIDevice.is_iPad(){
            deviceName.layoutIfNeeded()
            deviceAddress.layoutIfNeeded()
            deviceRSSI.layoutIfNeeded()
            self.layoutIfNeeded()}
        
    }
    
    func setupUIElements(){
        
        deviceName.font = UIFont(name: "Roboto-Bold", size:CGFloat.getFontSizeToScreen(16.0,scale: 0.6));
        deviceAddress.font = UIFont(name: "Roboto-Medium", size:CGFloat.getFontSizeToScreen(14.0,scale: 0.6));
        deviceRSSI.font = UIFont(name: "Roboto-Regular", size:CGFloat.getFontSizeToScreen(13.0,scale:0.6));
        //        Roboto = ["Roboto-Thin", "Roboto-Italic", "Roboto-Light", "Roboto-BoldItalic", "Roboto-LightItalic", "Roboto-ThinItalic", "Roboto-Bold", "Roboto-Regular", "Roboto-Medium"]
    }

    
    func setSignalImages()
    {
        self.signalImages = [UIImage](arrayLiteral: UIImage(named: "SignalOne")!,
            UIImage(named: "SignalTwo")!,
            UIImage(named: "SignalThree")!,
            UIImage(named: "SignalFour")!,
            UIImage(named: "SignalFive")!)
    }
    
    func updateSignalImage(_ RSSI:NSNumber)
    {
        let rssiString = RSSI.stringValue
        signalStrengthView.image = signalImages[BLEDeviceCell.getRSSIsignalIndex(RSSI)]
        deviceRSSI.text = "\(rssiString)"+" dBm"
    }
}

extension BLEDeviceCell
{
    class func getRSSIsignalIndex(_ RSSI:NSNumber) -> Int
    {
    
        let rssiInt = RSSI.intValue
        var index = 0
        
        if rssiInt == 127 {     // value of 127 reserved for RSSI not available
            index = 0
        }
        else if rssiInt <= -84 {
            index = 0
        }
        else if rssiInt <= -72 {
            index = 1
        }
        else if rssiInt <= -60 {
            index = 2
        }
        else if rssiInt <= -48 {
            index = 3
        }
        else {
            index = 4
        }
        
        return index
    }
}



