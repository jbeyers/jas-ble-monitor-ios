/***************************************************************************
 
 ServiceTableViewCell.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import UIKit
import CoreBluetooth

protocol ServiceTableCellDelegate {
    
    func dropdown(shouldShow status: Bool, forCell cell: ServiceTableViewCell)
}


class ServiceTableViewCell: UITableViewCell {
    
    
    let CELL_IDENTIFIER = "chacteristicCell"
    let SEGUE_ID        = "GattCharVC"
    let CHARACTERISTIC_TABLECELL_HEIGHT = 110
    let CHARACTERISTIC_TABLE_TOP_SPACE  = 40
    
    var delegate : ServiceTableCellDelegate?
    var isDropDownShown : Bool = false
    var selectedService : CBService?
    
    @IBOutlet weak var characteristicListTableView: UITableView!
    @IBOutlet weak var tableContentHeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var serviceNameLabel: UILabel!
    @IBOutlet weak var serviceUUIDLabel: UILabel!
    @IBOutlet weak var uuidLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        characteristicListTableView.dataSource = self
        characteristicListTableView.delegate = self
        characteristicListTableView.tableFooterView = UIView()
        
        tableContentHeightConstraint.constant = 0
        self.layoutIfNeeded()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func dropDownButtonClicked(_ sender: UIButton) {
        
        if sender.isSelected {
            
            if delegate != nil {
        
                sender.isSelected = false
                isDropDownShown = false
                delegate?.dropdown(shouldShow: false, forCell: self)
                
            }
        }
        else{
            
            if delegate != nil {
               
                sender.isSelected = true
                isDropDownShown = true
                delegate?.dropdown(shouldShow: true, forCell: self)
               
            }
        }
    }
}



extension ServiceTableViewCell : UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        
        if selectedService == nil {
            tableContentHeightConstraint.constant = 0
        }
        else{
            
            if selectedService!.characteristics?.count != 0 {
                tableContentHeightConstraint.constant = CGFloat ((selectedService!.characteristics!.count * CHARACTERISTIC_TABLECELL_HEIGHT) + CHARACTERISTIC_TABLE_TOP_SPACE)
            }
            else{
                tableContentHeightConstraint.constant = 0
                
            }
        }
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        if selectedService != nil && selectedService!.characteristics?.count != nil {
            return selectedService!.characteristics!.count
        }
    
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER) as? CharacteristicTableViewCell
        
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: CELL_IDENTIFIER) as? CharacteristicTableViewCell
        }
        
        let characteristic = selectedService!.characteristics![indexPath.row]
        
        cell?.characteristicNameLabel.text = characteristic.uuid.getCharacteristicName()
        cell?.characteristicUUIDLabel.text = characteristic.uuid.uuidString
        cell?.characteristicPropertiesLabel.text = characteristic.propertyDescription()

        return cell!
    }
    
    
}


extension ServiceTableViewCell : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        GlobalCommunicator.shared.selectedService = selectedService!
        GlobalCommunicator.shared.selectedCharacteristic = GlobalCommunicator.shared.selectedService.characteristics![indexPath.row]
        tableView.deselectRow(at: indexPath, animated: false)
        self.parentViewController?.performSegue(withIdentifier: SEGUE_ID, sender: self.parentViewController)
    }
    
}
