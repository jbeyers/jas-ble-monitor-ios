/* ***************************************************************************

BLEServiceCollectionCell.swift




Copyright (c) 2015, Atmel Corporation. All rights reserved.
Released under NDA
Licensed under Atmel's Limited License Agreement.


THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Atmel Corporation: http://www.atmel.com

******************************************************************************/
import UIKit
import CoreBluetooth

class BLEServiceCollectionCell: UICollectionViewCell {
    
    @IBOutlet var c_serviceName: UILabel!
    @IBOutlet var c_serviceAddress: UILabel!
    @IBOutlet var c_serviceDescription: UILabel!
    @IBOutlet var c_serviceThumbImageView: UIImageView!
    var servicecSegueEnum: ServiceScreenSegueEnum!
    
    
    func setCell(_ serviceInfo : CBService!)
    {
        self.c_serviceName.text = serviceInfo.uuid.getServiceName()
        self.c_serviceAddress.text = "Assigned Number 0x"+"\(serviceInfo.uuid.uuidString)"
        self.c_serviceAddress.sizeToFit()
        let info = BLEServiceList.getServiceInfo(serviceInfo)
        self.c_serviceThumbImageView.image = info.0 // UIImage for service thumb image
        self.servicecSegueEnum = info.1 // Segue Name for service details screen
    }
    
    
   
    
}
