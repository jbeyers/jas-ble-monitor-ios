
/* ***************************************************************************

BatteryServiceVC.swift



Copyright (c) 2015, Atmel Corporation. All rights reserved.
Released under NDA
Licensed under Atmel's Limited License Agreement.


THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Atmel Corporation: http://www.atmel.com

******************************************************************************/

import UIKit

let OTA_INFO_VC_SEGUE = "TO_OTAINFO_VC"
let OTA_SETTINGS_SEGUE = "TO_OTA_SETTINGS_SEGUE"


class BatteryServiceVC: GATTBaseViewController {

    @IBOutlet weak var batteryLevelBgView: UIView!
    @IBOutlet weak var batteryLevelView: UIView!
    
    @IBOutlet weak var batteryLevelLabel: UILabel!
    
    @IBOutlet weak var batteryBGViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var batteryLevelHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var batteryViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var batteryViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var batteryIndicaitonTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var batteryIndicaitonBottomConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var stopNotifyLabel: UILabel!
    @IBOutlet weak var notifySwitch: UISwitch!
    
   
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        super.prepare(for: segue, sender: sender)
        
       
    
    }
    
    struct iPadBatteryFrame
    {
        static let iPadBatteryBGViewHeight = 465
        
        static let iPadbatteryViewHeightConstant = 202
        
        static let iPadBatteryHeight = 260
        static let iPadBatteryWidth = 130
        
         static let iPadBatteryIndicationGapTop = 18
         static let iPadBatteryIndicationGapBottom = 39
    }
    
    let lowBatteryLevel:CGFloat = 15.0
    let highBatteryLevel:CGFloat = 80.0
    let midBatteryLevel:CGFloat = 50.0
    
    var batteryViewHeightConstant:CGFloat = 135.0
    var lastKnownValue : Int?
    
    
     var batteryService : BatteryService!
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.layoutFix()
        
            }

    override func viewDidLoad() {
        super.viewDidLoad()
        

        // Do any additional setup after loading the view.
        changeBatteryLevelColor(UIColor.white)
        updateUIwithLastValue()
        addListner()
        initBatteryServiceView()
        
        
        if UIDevice.is_iPad()
        {
            batteryBGViewHeightConstraint.constant = CGFloat(BatteryServiceVC.iPadBatteryFrame.iPadBatteryBGViewHeight)
            batteryLevelHeightConstraint.constant = CGFloat(BatteryServiceVC.iPadBatteryFrame.iPadbatteryViewHeightConstant)
            self.batteryViewHeightConstant = CGFloat(BatteryServiceVC.iPadBatteryFrame.iPadbatteryViewHeightConstant)
            
            batteryViewWidthConstraint.constant = CGFloat(BatteryServiceVC.iPadBatteryFrame.iPadBatteryWidth)
            batteryViewHeightConstraint.constant = CGFloat(BatteryServiceVC.iPadBatteryFrame.iPadBatteryHeight)
            
            batteryIndicaitonTopConstraint.constant = CGFloat(BatteryServiceVC.iPadBatteryFrame.iPadBatteryIndicationGapTop)
            batteryIndicaitonBottomConstraint.constant = CGFloat(BatteryServiceVC.iPadBatteryFrame.iPadBatteryIndicationGapBottom)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        configureNotifyControl()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        removeListner()
        checkAndStopNotify()
    }
    @IBAction func unwindToViewController(_ segue: UIStoryboardSegue) {
    }
    
 
    
    func checkAndStopNotify()
    {
        if notifySwitch.isOn == true
        {
            self.batteryService.stopBatteryLevelNotifications()
            keepLastKnownValue()
        }
    }
    
    func updateUIwithLastValue()
    {
        if getLastKnownValue()
        {
            changeBatterLevel(lastKnownValue!)
            
        }
    }
    
    
    func keepLastKnownValue()
    {
        if let _ = lastKnownValue
        {
            UserDefaults.standard.set(lastKnownValue!, forKey: Constants.UserDefaultsKey.batteryValueKey)

        }
    }
    
    func getLastKnownValue() -> Bool
    {
        let val = UserDefaults.standard.integer(forKey: Constants.UserDefaultsKey.batteryValueKey)
        if val > 0
        {
            lastKnownValue = val
            return true
        }
        return false
    }
    
    func initBatteryService()
    {
        self.navigationController!.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "Roboto-Medium", size: CGFloat.getFontSizeToScreen(16.0,scale: 0.6))!,NSForegroundColorAttributeName:UIColor.white]
        self.navigationItem.title = GlobalCommunicator.shared.selectedService?.uuid.getServiceName()
        batteryService = nil
        batteryService = BatteryService(deviceInfoService: GlobalCommunicator.shared.selectedService!, andSourceDevice: GlobalCommunicator.shared.sourceDevice!, andDelegate: self)
    }

    func changeBatterLevel(_ batteryLevel: Int)
    {
        lastKnownValue = batteryLevel //To display Lat value with Alert
        
        if batteryLevel >= 0 && batteryLevel <= 100
        {
            
            self.batteryLevelHeightConstraint.constant =  self.batteryViewHeightConstant * (CGFloat(batteryLevel)/100.0)
            self.batteryLevelLabel.text = "\(batteryLevel)%"
            self.changeBatteryLevelIndicatorColor(CGFloat(batteryLevel))
            
            /*UIView.animateWithDuration(0.5, animations:
                {
                self.batteryLevelHeightConstraint.constant =  self.batteryViewHeightConstant * (CGFloat(batteryLevel)/100.0)
                
                self.view.layoutIfNeeded()
                
            }, completion: { success in
                
                self.batteryLevelLabel.text = "\(batteryLevel)%"
                self.changeBatteryLevelIndicatorColor(CGFloat(batteryLevel))

            })*/
        }
    }
    
    func changeBatteryLevelIndicatorColor(_ batteryLevel: CGFloat)
    {
        if batteryLevel <= lowBatteryLevel
        {
            changeBatteryLevelColor(UIColor.AtmelDarkRedColor())
        }
        else if (batteryLevel > lowBatteryLevel) && (batteryLevel <= midBatteryLevel)
        {
            changeBatteryLevelColor(UIColor.AtmelOrangeColor())
        }
        else if (batteryLevel > midBatteryLevel)
        {
            changeBatteryLevelColor(UIColor.AtmelGreenColor())
        }
        else
        {
            changeBatteryLevelColor(UIColor.white)
        }
    }
 
    
    func changeBatteryLevelColor(_ color : UIColor)
    {
        batteryLevelView.backgroundColor = color
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    

    func initBatteryServiceView()
    {
        
        initBatteryService()
        //Call after  interface initilization to handle previous state of Notify Switch
        configureNotifyControl()
        
    }
    func configureNotifyControl()
    {
        let state = UserDefaults.getNotificationStatus(Constants.UserDefaultsKey.batteryStatusKey)
        notifySwitch.isOn = state
        onNotifyChanged(notifySwitch)
    }
    
    @IBAction func onNotifyChanged(_ sender: UISwitch)
    {
        
        if sender.isOn == true
        {
            stopNotifyLabel.text = Constants.ButtonTitle.STOP_NOTIFY
            self.batteryService.startBatteryLevelNotifications()
        }
        else
        {
            stopNotifyLabel.text = Constants.ButtonTitle.START_NOTIFY
            self.batteryService.stopBatteryLevelNotifications()
            keepLastKnownValue()
        }
        
        UserDefaults.updateNotifyState(sender.isOn , UserDefaultsKey: Constants.UserDefaultsKey.batteryStatusKey)
    }


}


extension BatteryServiceVC:BatteryServiceDelegate {
    
    func batteryServiceDidReadBatteryLevel(_ batteryLevel: UInt8) {

        changeBatterLevel(Int(batteryLevel))
        

        
    }
    
}

extension BatteryServiceVC
{
    func addListner()
    {
        NotificationCenter.listenNotification(self, notificationName: Constants.NotificationName.onServiceDiscovered)
        
        NotificationCenter.listenNotification(self, notificationName: Constants.NotificationName.onEnterBackground)
        NotificationCenter.listenNotification(self, notificationName: Constants.NotificationName.onEnterForeground)
    }
    
    func removeListner()
    {
        NotificationCenter.removelistener(self, notificationName: Constants.NotificationName.onServiceDiscovered)
        NotificationCenter.removelistener(self, notificationName: Constants.NotificationName.onEnterBackground)
        NotificationCenter.removelistener(self, notificationName: Constants.NotificationName.onEnterForeground)
    }
    
    
    func onServiceDiscovered()
    {
        initBatteryServiceView()
    }
    
    func onEnterBackground()
    {
        checkAndStopNotify()
    }
    
    func onEnterForeground()
    {
        configureNotifyControl()
    }

    

    
    
}

