/* ***************************************************************************

//SettingsVC.swift
//
Viewcontroller class for advanced settings view in the scanner page;
contains filter which can be used to  enable filtered scanning
//
Copyright (c) 2015, Atmel Corporation. All rights reserved.
Released under NDA
Licensed under Atmel's Limited License Agreement.


THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Atmel Corporation: http://www.atmel.com

******************************************************************************/

import Foundation

import UIKit

class SettingsVC: BaseViewController ,UITableViewDataSource,UITableViewDelegate
{

    enum SectionValues : String
    {
        case AdvancedFilterSection = "  Advanced Filter Search"
        case FilterSection = " Device Filter"
        case Other  = " Other"
        static let allSection = [AdvancedFilterSection,FilterSection]
    }
    
    
    @IBOutlet var settingsTableView: UITableView!

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.layoutFix()
        
            }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    NotificationCenter.listenNotification(self, notificationName: Constants.NotificationName.refreshMyTableView)
        
 
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func refreshMyTableView()
    {
        self.settingsTableView.reloadData()
    }
    @IBAction func onDoneTouched(_ sender: AnyObject) {
        
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
    }
    
    
    
    /*----------------------------------------------------------------------------------------------------------*/
    // MARK: Filter ON / OFF
    /*----------------------------------------------------------------------------------------------------------*/
    
    
    func onfilterSwitcheDidChange(_ sender : UISwitch)
    {
        SettingsVC.setFilterValue(sender.isOn as AnyObject, forKey: Constants.UserDefaultsKey.filterOnOffKey)
        settingsTableView.reloadSections(IndexSet(integer: SectionValues.FilterSection.hashValue), with: UITableViewRowAnimation.automatic)
//        settingsTableView.reloadData()
        
    }
  
    
    
    
    /*----------------------------------------------------------------------------------------------------------*/
    // MARK: Settings TableView Delegate and DataSource
    /*----------------------------------------------------------------------------------------------------------*/
    

    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return SectionValues.allSection.count
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == SectionValues.AdvancedFilterSection.hashValue
        {
            let label : UILabel = UILabel()
            label.text = SectionValues.allSection[section].rawValue
            label.textColor = UIColor.black
            label.backgroundColor = UIColor.white
            return label

        }
        else if section == SectionValues.FilterSection.hashValue
        {
            let HeaderView = UIView() // The width will be the same as the cell, and the height should be set in tableView:heightForRowAtIndexPath:
            HeaderView.backgroundColor = UIColor.white
            
            
            //Section Title
            let sectionTitle = UILabel()
            sectionTitle.text =  SectionValues.allSection[section].rawValue
            sectionTitle.translatesAutoresizingMaskIntoConstraints = false
            
            //Filter switch to enable / disable filter feature
            let filterSwitch=UISwitch(frame:CGRect(x: 150, y: 300, width: 0, height: 0));
            filterSwitch.setOn(SettingsVC.getFilterStatus(), animated: false);
            filterSwitch.onTintColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.45)
            filterSwitch.thumbTintColor = UIColor.black
            filterSwitch.addTarget(self, action: #selector(SettingsVC.onfilterSwitcheDidChange(_:)), for: .valueChanged);
            filterSwitch.translatesAutoresizingMaskIntoConstraints = false
            filterSwitch.backgroundColor = UIColor.AtmelLightGrayColor()
            filterSwitch.layer.cornerRadius = 16
            filterSwitch.clipsToBounds = true
            
            //Autolayout
            let views = ["label": sectionTitle,"button":filterSwitch,"view": HeaderView]
            HeaderView.addSubview(sectionTitle)
            HeaderView.addSubview(filterSwitch)
            let horizontallayoutContraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[label]-60-[button]-10-|", options: .alignAllCenterY, metrics: nil, views: views)
            HeaderView.addConstraints(horizontallayoutContraints)
            
            let verticalLayoutContraint = NSLayoutConstraint(item: sectionTitle, attribute: .centerY, relatedBy: .equal, toItem: HeaderView, attribute: .centerY, multiplier: 1, constant: 0)
            HeaderView.addConstraint(verticalLayoutContraint)
            return HeaderView
        }
        return nil
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 40
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == SectionValues.FilterSection.hashValue
        {
            if SettingsVC.getFilterStatus() == true
            {
                return  BLEProfile.allValues.count
            }
            else
            {
                return 0
            }
        }
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let servicecellIdentifier = "SettingsCell"
        let cell = self.settingsTableView.dequeueReusableCell(withIdentifier: servicecellIdentifier) as! FilterCell
        
        if indexPath.section == SectionValues.FilterSection.hashValue
        {
            cell.setFilterCell(indexPath.row)
        }
       
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        let fooView = UIView(frame: CGRect(x: 00, y: 0, width: 10, height: 2))
        return fooView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any!)
    {
       
    }

}

extension SettingsVC
{
    class func getFilterStatus() -> Bool
    {
        let filterStatus = UserDefaults.standard.bool(forKey: Constants.UserDefaultsKey.filterOnOffKey)
        if filterStatus
        {
            return filterStatus
        }
        return false
    }
    
    class func setFilterValue(_ value: AnyObject?, forKey key: String)
    {
        UserDefaults.standard.set(value, forKey: key)
        UserDefaults.standard.synchronize()

    }
    
    
}
