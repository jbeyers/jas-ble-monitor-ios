

/* ***************************************************************************

AEnum.swift



Copyright (c) 2015, Atmel Corporation. All rights reserved.
Released under NDA
Licensed under Atmel's Limited License Agreement.


THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Atmel Corporation: http://www.atmel.com

******************************************************************************/

import Foundation
import UIKit
import CoreBluetooth
import AVFoundation



struct Constants
{
    static let TIME_OUT_DELAY = 10.0
    
    struct RSSI {
    
        static let  minimumRSSI:CGFloat = -120.0//-110.0
        static let  maximumRSSI:CGFloat = -30.0//-40.0
        static let  minRSSISafe:CGFloat = -80.0
        static let  minRSSIMid:CGFloat  = -95.0

    }
    
    struct AlertTitle {
        
        static let ProductName = "Atmel SmartConnect"
        static let Warnig = "Warning !"
        static let OKButton = "Ok"
        static let CancelButton = "Cancel"
        static let Settings = "Settings"
        static let ConnectionStopped = "The Connection has Stopped Unexpectedly"
        static let Reconnecting = "Reconnecting... "
        static let BatteryUpdating = "Updating Battery Value..."
        static let AllowBluetooth = "Turn On Bluetooth to Allow \(ProductName) to Connect to Accessories "

    }
    
    struct UserDefaultsKey
    {
        static let safeZoneKey = "safeZoneAlertLevel"
        static let midZoneKey = "midZoneAlertLevel"
        static let dangerZoneKey = "dangerZoneAlertLevel"
        
        static let safeZoneValueKey = "safeZoneValue"
        static let midZoneValueKey = "midZoneValue"
        
        static let filterKey = "FilterKey"
        static let filterOnOffKey = "FilterOnOffKey"
        
        static let findMeAlertStatusKey = "findMeAlertStatusKey"
        static let healthThermoMeterStatusKey = "healthThermoMeterStatusKey"
        static let heartRateStatusKey = "heartRateStatusKey"
        static let bloodPressureStatusKey = "bloodPressureStatusKey"
        static let batteryStatusKey = "batteryStatusKey"
        static let batteryValueKey = "batteryValueKey"
        static let scanParamStatusKey = "scanParamStatusKey"
        static let scanParamIntervalValueKey = "scanParamIntervalValueKey"
        static let scanParamIntervalUnitValueKey = "scanParamIntervalUnitValueKey"
        static let scanParamWindowValueKey = "scanParamWindowValueKey"
        static let scanParamWindowUnitValueKey = "scanParamWindowUnitValueKey"

    }
    
    struct Path
    {
        static let Documents = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] 
        static let Tmp = NSTemporaryDirectory()
    }
    
    struct NotificationName
    {
        static let refreshMyTableView   = "refreshMyTableView"
        static let onDeviceDisconned    = "onDeviceDisconned"
        static let onDeviceConnected    = "onDeviceConnected"
        static let onServiceDiscovered  = "onServiceDiscovered"
        
        static let onEnterForeground  = "onEnterForeground"
        static let onEnterBackground  = "onEnterBackground"
    }
    
    struct ButtonTitle
    {
    
        static let START_NOTIFY = "  START NOTIFY"
        static let STOP_NOTIFY  = "  STOP NOTIFY"
    }
    
    struct iPadFrame
    {
        static let iPadDefaultWidthSize = 500
    }
    
    

    
}


enum ServiceScreenSegueEnum:String{
    
    case  LLS_VC    = "LinkLossView"
    case  IAS_VC    = "ImmediateAlertView"
    case  TPS_VC    = "TxPowerView"
    case  PS_VC     = "ProximitySettingsView"
    case  DIS_VC    = "DeviceInformationView"
    case  HTS_VC    = "HealthThermometerView"
    case  FIND_VC   = "FindMeView"
    case  HRS_VC    = "HeartRateView"
    case  BLS_VC    = "BloodPressureView"
    case  BAS_VC    = "BatteryView"
    case  GATT_VC   = "GattView"
    case  SPP_VC    = "SerialPortView"
    case  ScPS_VC   = "ScanParameterView"
    case  GATTDB_VC = "GattDBServiceVC"
}




enum DisplayList
{
    case addItem
    case removeItem
    case removeAll
}



enum ResultDisplay
{
    case eNil
    case eStart
    case eStop
    case eFound
    case eNoneFound
    case eConnect
    case eConnectionFailed
    case eSearchFinished
    case eBLEOFF
}

enum AlertType :String
{
    case eFailedToConnect = "Failed to connect"
    case eScanning = " Scanning..."
    case eConnecting = " Connecting..."
    case eDisconnected = "The connection has lost !"
    case eDisconnectedWithError = "The connection has stopped unexpectedly !"
    case eBlueToothOff = "Bluetooth is currently powered off"
    case eFilterApplied = "Filter has been applied"
    case eNone = ""
}



