

/* ***************************************************************************

FindMeVC.swift



Copyright (c) 2015, Atmel Corporation. All rights reserved.
Released under NDA
Licensed under Atmel's Limited License Agreement.


THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Atmel Corporation: http://www.atmel.com

******************************************************************************/

import UIKit
import CoreBluetooth

enum FindMeAnimDuration : TimeInterval
{

    case noAlertDuration = 0.0
    case mildAlertDuration = 1.0
    case highAlertDuration = 0.25
}


class FindMeVC: GATTBaseViewController {
    
    
    @IBOutlet weak var alertStatusLabel: UILabel!
    @IBOutlet weak var radarImage: UIImageView!
    @IBOutlet weak var noAlertButton: UIButton!
    @IBOutlet weak var mildAlertButton: UIButton!
    @IBOutlet weak var highAlertButton: UIButton!
    
    
    @IBOutlet weak var peripheralDevice: UIImageView!
    @IBOutlet weak var peripheralDeviceContainer: UIView!
    @IBOutlet weak var peripheralDeviceContainerConstant:NSLayoutConstraint!
    
    var findMeInterface : BLEFindMeInterface!
    var radarImageWidth : CGFloat!
    var isDiconnected : Bool = false
    
    
   
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.layoutFix()
        
            }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        addListner()
        initFindMeInterface()
        startRadarAnimation()
        radarImageWidth = self.peripheralDeviceContainer.frame.size.width
        
        
        initAlertButon()
        
        // Do any additional setup after loading the view.
    }

    override func viewWillDisappear(_ animated: Bool) {
        removeListner()
        super.viewWillDisappear(animated)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func initFindMeInterface()
    {
        self.navigationItem.title = GlobalCommunicator.shared.selectedService?.uuid.getServiceName()
        self.findMeInterface = BLEFindMeInterface(sourceDevice: GlobalCommunicator.shared.sourceDevice, delgateObject: self)
        self.findMeInterface.readRSSIValue()
//
    }
    
    
    
    func changeAlert(_ alert  : AlertLevel)
    {
        var imageColor = "PeripheralDeviceHigh"
        var duration = FindMeAnimDuration.noAlertDuration
        var alertText = "NoAlert"
        switch(alert)
        {
        case AlertLevel.noAlert:
            imageColor = "PeripheralDevice"
            alertText = "No Alert"
            duration = FindMeAnimDuration.noAlertDuration
            noAlertButton.isSelected = true
        case AlertLevel.mildAlert:
            imageColor = "PeripheralDeviceMild"
            alertText = "Mild Alert"
            duration = FindMeAnimDuration.mildAlertDuration
            mildAlertButton.isSelected = true
        case AlertLevel.highAlert:
            imageColor = "PeripheralDeviceHigh"
            alertText = "High Alert"
            duration = FindMeAnimDuration.highAlertDuration
            highAlertButton.isSelected = true
        }
        displayStatusLable(alertText)
        peripheralDevice.image = UIImage(named: imageColor)
        blinkKeyBob(duration)
        
    }
    
    
    func stopKeyBobAnimation(_ alpha : CGFloat)
    {
        
        print("Alpha  Value = \(alpha)")
        self.peripheralDevice.layer.removeAllAnimations()
        self.peripheralDevice.alpha = alpha
    }
    
    
    func blinkKeyBob(_ duration : FindMeAnimDuration )
    {
        
        if duration == FindMeAnimDuration.noAlertDuration
        {
            stopKeyBobAnimation(1.0)
            return
        }
        
        self.peripheralDevice.alpha = 1.0
            print("Alpha  Value ==== 1.0")
        UIView.animate(withDuration: duration.rawValue, delay: 0.0, options: [UIViewAnimationOptions.repeat, UIViewAnimationOptions.autoreverse], animations: { () -> Void in
            print("Alpha  Value ==== 0.0")
            self.peripheralDevice.alpha = 0.0
            }, completion: nil)
    }
    
    
    
    func deselectAllAlert()
    {
        
        noAlertButton.isSelected = false
        mildAlertButton.isSelected = false
        highAlertButton.isSelected = false
    }
    
    @IBAction func onAlertButtonTouced(_ sender: UIButton)
    {
        deselectAllAlert()
        writeAlert(sender.tag)
        
    }
    
    func writeAlert(_ alertTag:Int)
    {
        let selectAlertLevelRawValue = UInt8(alertTag)
        self.findMeInterface.findMeWithAlertLevel(AlertLevel(rawValue: selectAlertLevelRawValue)!)
        changeAlert(AlertLevel(rawValue: selectAlertLevelRawValue)!)
        UserDefaults.standard.set(alertTag, forKey: Constants.UserDefaultsKey.findMeAlertStatusKey)
    }
    
    func initAlertButon()
    {
       let tag =  UserDefaults.standard.integer(forKey: Constants.UserDefaultsKey.findMeAlertStatusKey)
        
        if tag < 3 && tag >= 0
        {
            writeAlert(tag)
        }
    
    }
    
    
    
    func displayStatusLable(_ alert : String)
    {
        self.alertStatusLabel.alpha = 1.0
        alertStatusLabel.text = "\(alert) notification has been set"
        Util.delay(1.0)
            {
                UIView.animate(withDuration: 1.0, animations: { () -> Void in
                    self.alertStatusLabel.alpha = 0.0
                })
        }

    }
    func startRadarAnimation()
    {
        self.radarImage.layer.removeAllAnimations()
       repeatRadarAnim()
    }
    
    func repeatRadarAnim()
    {
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveLinear, animations: { () -> Void in
            self.radarImage.transform = self.radarImage.transform.rotated(by: CGFloat(Double.pi/2))
            }) { (finished) -> Void in
                
                if self.isDiconnected == false
                {
                    self.repeatRadarAnim()
                }
        }
    }
    

   

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension FindMeVC:BLEFindMeInterfaceDelegate{
    
    func bleFindMeInterfaceDidReadRSSIValue(_ RSSI:NSNumber!){
        
        if let _ = RSSI
        {
            findlayoutRadiusFromRSSI(RSSI)
            
        }
        
    }
    
    
    func findlayoutRadiusFromRSSI(_ rssi:NSNumber!)
    {
        
        let maximumCircleRadius:CGFloat = radarImageWidth / 2.0
        let currentRSSI = CGFloat(rssi)
        
        print(currentRSSI)
        

        let radius = ((Constants.RSSI.maximumRSSI - currentRSSI) * ( maximumCircleRadius / (Constants.RSSI.maximumRSSI - Constants.RSSI.minimumRSSI)))
        
        UIView.animate(withDuration: 2.0, animations: { () -> Void in
            self.peripheralDeviceContainerConstant.constant = radius * 2.0
            self.view.layoutIfNeeded()
            
            }, completion: { (complete) -> Void in
                self.findMeInterface.readRSSIValue()
                
        }) 

        
    }

    
}

extension FindMeVC
{
    func addListner()
    {
        NotificationCenter.listenNotification(self, notificationName: Constants.NotificationName.onDeviceDisconned)
        NotificationCenter.listenNotification(self, notificationName: Constants.NotificationName.onServiceDiscovered)
        NotificationCenter.listenNotification(self, notificationName: Constants.NotificationName.onEnterForeground)
        
    }
    
    func removeListner()
    {
        NotificationCenter.removelistener(self, notificationName: Constants.NotificationName.onDeviceDisconned)
        NotificationCenter.removelistener(self, notificationName: Constants.NotificationName.onServiceDiscovered)
        
        NotificationCenter.removelistener(self, notificationName: Constants.NotificationName.onEnterForeground)
    }
    
    func onEnterForeground()
    {
        let tag =  UserDefaults.standard.integer(forKey: Constants.UserDefaultsKey.findMeAlertStatusKey)
        if tag < 3 && tag >= 0
        {
            let selectAlertLevelRawValue = UInt8(tag)
            changeAlert(AlertLevel(rawValue: selectAlertLevelRawValue)!)
        }
        
        
    }


    func onDeviceDisconned()
    {
        deselectAllAlert()
        isDiconnected  = true
        stopKeyBobAnimation(0.0)
        UserDefaults.standard.set(3, forKey: Constants.UserDefaultsKey.findMeAlertStatusKey)
    }
    
    func onServiceDiscovered()
    {
        isDiconnected  = false
        initFindMeInterface()
        startRadarAnimation()
        stopKeyBobAnimation(1.0)
        peripheralDevice.image = UIImage(named: "PeripheralDevice")
    }
    
    
}

