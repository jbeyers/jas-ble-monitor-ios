
import UIKit
import QuartzCore


class ProgressCircleView: UIView {

    
    let strokeWidth : CGFloat =  12.0
    var lastEndValue : CGFloat = 0.0
    var progressCircle : CAShapeLayer?
    var progressCircleStroke : CAShapeLayer?

    var strokeEndAngle: CGFloat = 0.00 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var strokePathDisplay : Bool = true {
        didSet{
            
            setNeedsDisplay()
        }
    }
    
    func updateUI()
    {
        setNeedsDisplay()
    }

    override func draw(_ rect: CGRect) {
        // Get the Graphics Context
//        self.layer.sublayers = nil
        _ = UIGraphicsGetCurrentContext()
        

        
        
        
        let centerPoint = CGPoint (x: rect.size.width / 2, y: rect.size.height / 2)
        let circleRadius : CGFloat = rect.size.width / 2 * 0.9
        
        let circlePath = UIBezierPath(arcCenter: centerPoint, radius: circleRadius, startAngle: CGFloat(0.5 * Double.pi), endAngle: CGFloat(2.5 * Double.pi), clockwise: true    )
        
//        if strokePathDisplay == true
//        {
        
        if let _ = progressCircleStroke
        {
            progressCircleStroke!.removeFromSuperlayer()
        }
        
            progressCircleStroke = CAShapeLayer()
            progressCircleStroke!.path = circlePath.cgPath
            progressCircleStroke!.strokeColor = UIColor.AtmelLightGrayColor().cgColor
            progressCircleStroke!.fillColor = UIColor.clear.cgColor
            progressCircleStroke!.lineWidth = strokeWidth
            progressCircleStroke!.strokeStart = 0
            progressCircleStroke!.strokeEnd = 1
            self.layer.addSublayer(progressCircleStroke!)
            
//        }

        if let _ = progressCircle
        {
            progressCircle!.removeFromSuperlayer()
        }
        
        progressCircle = CAShapeLayer ()
        progressCircle!.path = circlePath.cgPath
        progressCircle!.strokeColor = UIColor.AtmelRedColor().cgColor
        progressCircle!.fillColor = UIColor.clear.cgColor
        progressCircle!.lineWidth = strokeWidth
        progressCircle!.strokeStart = 0
        progressCircle!.strokeEnd = strokeEndAngle

        self.layer.addSublayer(progressCircle!)
        
        
        //Animate path
        progressCircle!.removeAllAnimations()
        let pathAnimation = CABasicAnimation(keyPath: "strokeEnd")
        pathAnimation.duration = 0.5
        pathAnimation.fromValue = lastEndValue
        pathAnimation.toValue =  strokeEndAngle
        lastEndValue = strokeEndAngle
        progressCircle!.add(pathAnimation, forKey: "strokeEnd")
        
      
        
    }
}




func degree2radian(_ a:CGFloat)->CGFloat {
    let b = CGFloat(Double.pi) * a/180
    return b
}


func circleCircumferencePoints(_ sides:Int,x:CGFloat,y:CGFloat,radius:CGFloat,adjustment:CGFloat=0)->[CGPoint] {
    let angle = degree2radian(360/CGFloat(sides))
    let cx = x // x origin
    let cy = y // y origin
    let r  = radius // radius of circle
    var i = sides
    var points = [CGPoint]()
    while points.count <= sides {
        let xpo = cx - r * cos(angle * CGFloat(i)+degree2radian(adjustment))
        let ypo = cy - r * sin(angle * CGFloat(i)+degree2radian(adjustment))
        
//        println("\(xpo)   --- \(ypo)")
        points.append(CGPoint(x: xpo, y: ypo))
        i -= 1;
    }
    return points
}

func secondMarkers(ctx:CGContext, x:CGFloat, y:CGFloat, radius:CGFloat, sides:Int, color:UIColor) {
    // retrieve points
    let points = circleCircumferencePoints(sides,x: x,y: y,radius: radius - 20)
    // create path
    let path = CGMutablePath()
    // determine length of marker as a fraction of the total radius
    let divider:CGFloat = 1/4
    for p in points.enumerated() {
        let xn = p.element.x + divider*(x-p.element.x)
        let yn = p.element.y + divider*(y-p.element.y)
        // build path
        
        //CGPathMoveToPoint(path, nil, p.element.x, p.element.y)
        path.move(to: CGPoint(x:p.element.x, y: p.element.y))
        //CGPathAddLineToPoint(path, nil, xn, yn)
        path.move(to: CGPoint(x:xn, y:yn))
        path.closeSubpath()
        // add path to context
        ctx.addPath(path)
    }
    // set path color
    let cgcolor = color.cgColor
    ctx.setStrokeColor(cgcolor)
    ctx.setLineWidth(1.0)
    ctx.strokePath()
    
}
func drawText(rect:CGRect, ctx:CGContext, x:CGFloat, y:CGFloat, radius:CGFloat, sides:Int, color:UIColor, isMmHgUnit:Bool) {
    
  
    ctx.translateBy(x: 0.0, y: rect.height)
    ctx.scaleBy(x: 1.0, y: -1.0)
    // dictates on how inset the ring of numbers will be
    let inset:CGFloat = 10
    // An adjustment of 270 degrees to position numbers correctly
    let points = circleCircumferencePoints(sides,x: x,y: y,radius: radius-inset,adjustment:180)
//    let path = CGPathCreateMutable()
    // see
    
    let mmHgTextArray = ["0","   0 - 240","60","120","180","240"]
    let kPaTextArray = ["0","  0 - 32","8","16","24","32"]
    var textArray = ["0"]
    if isMmHgUnit == true
    {
        textArray = mmHgTextArray
    }
    else
    {
        textArray = kPaTextArray
    }
    for p in points.enumerated() {
        
        //***if p.index > 0 {
        if p.offset > 0 {
            // Font name must be written exactly the same as the system stores it (some names are hyphenated, some aren't) and must exist on the user's device. Otherwise there will be a crash. (In real use checks and fallbacks woud be created.) For a list of iOS 7 fonts see here
            let aFont = UIFont(name: "Helvetica", size: 12.0)
            // create a dictionary of attributes to be applied to the string
            //***let attr:CFDictionary = [NSFontAttributeName:aFont!,NSForegroundColorAttributeName:UIColor.AtmelRedColor()]
            let attr = [NSFontAttributeName:aFont!,NSForegroundColorAttributeName:UIColor.AtmelRedColor()]
            // create the attributed string
            //***let text = CFAttributedStringCreate(nil, textArray[p.index], attr)
            let text = CFAttributedStringCreate(nil, textArray[p.offset] as CFString, attr as CFDictionary)
            // create the line of text
            //***let line = CTLineCreateWithAttributedString(text)
            let line = CTLineCreateWithAttributedString(text!)
            // retrieve the bounds of the text
            let bounds = CTLineGetBoundsWithOptions(line, CTLineBoundsOptions.useOpticalBounds)
            // set the line width to stroke the text with
            ctx.setLineWidth(0.2)
            // set the drawing mode to stroke
            ctx.setTextDrawingMode(CGTextDrawingMode.fillStroke)
            // Set text position and draw the line into the graphics context, text length and height is adjusted for
            let xn = p.element.x - bounds.width/2
            let yn = p.element.y - bounds.midY
            //***CGContextSetTextPosition(ctx, xn, yn)
            ctx.textPosition = CGPoint(x:xn, y:yn)
            // the line of text is drawn - see https://developer.apple.com/library/ios/DOCUMENTATION/StringsTextFonts/Conceptual/CoreText_Programming/LayoutOperations/LayoutOperations.html
            // draw the line of text
            CTLineDraw(line, ctx)
        }
    }
    
}

class MarkerView: UIView {
    
    var ismmHg: Bool = true {
        didSet {
            setNeedsDisplay()
        }
    }

    
    
    override func draw(_ rect:CGRect)
        
    {
        
        // obtain context
        let ctx = UIGraphicsGetCurrentContext()
        
        // decide on radius
        let rad = rect.width/2.0
        
        
        secondMarkers(ctx: ctx!, x: rect.midX, y: rect.midY, radius: rad, sides: 4, color: UIColor.AtmelDarkGrayColor())
        
        drawText(rect:rect, ctx: ctx!, x: rect.midX, y: rect.midY, radius: rad, sides: 4, color: UIColor.AtmelBlueColor(),isMmHgUnit: ismmHg)
        
        
        
        
    }
}
