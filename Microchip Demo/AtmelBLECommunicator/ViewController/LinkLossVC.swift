/* ***************************************************************************

LinkLossVC.swift

UIViewController Class to interact with user / Display the Link Loss feature values



Copyright (c) 2015, Atmel Corporation. All rights reserved.
Released under NDA
Licensed under Atmel's Limited License Agreement.


THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Atmel Corporation: http://www.atmel.com

******************************************************************************/

import Foundation
import CoreBluetooth
import UIKit
import AVFoundation

class LinkLossVC: GATTBaseViewController
{
    
    
    var audioPlayer = AVAudioPlayer()

    var proximityInterface:BLEProximityInterface!
    @IBOutlet weak var LinkLossControl: UISegmentedControl!
    @IBOutlet weak var LinkLossImage: UIImageView!
    @IBOutlet weak var LinkLossImageRed: UIImageView!
    @IBOutlet weak var LinkLossBlackView: UIView!
    @IBOutlet weak var linkLossImageWidth: NSLayoutConstraint!
    @IBOutlet weak var linkLossImageHeight: NSLayoutConstraint!
    

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.layoutFix()
        
            }

    override func viewDidLoad() {
        super.viewDidLoad()
        
         addListner()
        
        LinkLossControl.roundRectView()
        initProximityInterface()
        
        
        if UIDevice.is_iPad()
        {
            let ratio = CGFloat(Constants.iPadFrame.iPadDefaultWidthSize) / linkLossImageWidth.constant
            linkLossImageWidth.constant = ratio * linkLossImageWidth.constant
            linkLossImageHeight.constant = ratio * linkLossImageHeight.constant
        }


        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeListner()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func initProximityInterface()
    {
        self.navigationItem.title = GlobalCommunicator.shared.selectedService?.uuid.getServiceName()
        self.proximityInterface = nil
        self.proximityInterface=BLEProximityInterface(sourceDevice: GlobalCommunicator.shared.sourceDevice)
        self.proximityInterface.delegate=self;
        self.proximityInterface?.readLinkLinkLossLevel()
    }
    
    

    

    
    @IBAction func onValueChanged(_ sender: AnyObject)
    {
        if let segmentedControl = sender as? UISegmentedControl
        {
            let selectAlertLevelRawValue = UInt8(segmentedControl.selectedSegmentIndex)
                switch selectAlertLevelRawValue {
                case 0:
                    
                    segmentedControl.firstSegment()?.tintColor = UIColor.AtmelRedColor()
                    segmentedControl.secondSegment()?.tintColor = UIColor.AtmelDarkGrayColor()
                    segmentedControl.thirdSegment()?.tintColor =  UIColor.AtmelDarkGrayColor()
                    
                case 1:
                    
                    segmentedControl.firstSegment()?.tintColor = UIColor.AtmelDarkGrayColor()
                    segmentedControl.secondSegment()?.tintColor = UIColor.AtmelRedColor()
                    segmentedControl.thirdSegment()?.tintColor =  UIColor.AtmelDarkGrayColor()
                default:
                   
                    segmentedControl.firstSegment()?.tintColor = UIColor.AtmelDarkGrayColor()
                    segmentedControl.secondSegment()?.tintColor = UIColor.AtmelDarkGrayColor()
                    segmentedControl.thirdSegment()?.tintColor =  UIColor.AtmelRedColor()
                }
            
            print("Selected index = \(selectAlertLevelRawValue)")
            
            self.proximityInterface?.writeLinkLossAlertLevel(AlertLevel(rawValue: selectAlertLevelRawValue)!, withAcknowledgement: true)
        }
    }
    

    
    func pathLossTriggered()
    {
        self.LinkLossImage.isHighlighted = true
        LinkLossControl.isEnabled = false
        self.LinkLossBlackView.isHidden = false
        blinkLockRed()
        //audioPlayer = AVAudioPlayer(fileName: "pathLoss", withFormat: "wav")
        audioPlayer.playSound()

    }

    func connectionEstablished()
    {
        self.LinkLossImage.isHighlighted = false
        self.LinkLossImageRed.alpha = 0.0
        self.LinkLossBlackView.isHidden = true
        self.LinkLossImageRed.layer.removeAllAnimations()
        LinkLossControl.isEnabled = true
        
    }
    

    
    func blinkLockRed()
    {
        
        self.LinkLossImageRed.alpha = 1.0
        UIView.animate(withDuration: 0.15, delay: 0.0, options: [UIViewAnimationOptions.repeat, UIViewAnimationOptions.autoreverse], animations: { () -> Void in
            
            self.LinkLossImageRed.alpha = 0.0
            }, completion: nil)
        }
    

}


/*----------------------------------------------------------------------------------------------------------*/
// MARK: BLEProximityInterfaceDelegate Methods
/*----------------------------------------------------------------------------------------------------------*/


extension LinkLossVC:BLEProximityInterfaceDelegate{
    
    func bleProximityInterfaceDidReadLinkLossLevel(_ alertLevel:AlertLevel){
        
        print("\n  received linkloss value as= \(alertLevel.rawValue) in ImmediateVC")
        self.LinkLossControl.selectedSegmentIndex = Int(alertLevel.rawValue)
        onValueChanged(self.LinkLossControl)
    }
    
    func bleProximityInterfaceDidWriteValueForLinkLossCharacteristic(_ error: NSError!)
    {
        print("Success")
    }
    
    func bleProximityInterfaceDidReadTransmissionPowerLevel(_ txPowerLevel:Int){
        
    }
    
    func bleProximityInterfaceDidReadRSSIValue(_ RSSI:NSNumber!)
    {
        
        
    }
}



extension LinkLossVC
{
    
    
    func addListner()
    {
        NotificationCenter.listenNotification(self, notificationName: Constants.NotificationName.onDeviceDisconned)
        NotificationCenter.listenNotification(self, notificationName: Constants.NotificationName.onServiceDiscovered)
    }
    
    func removeListner()
    {
        NotificationCenter.removelistener(self, notificationName: Constants.NotificationName.onDeviceDisconned)
        NotificationCenter.removelistener(self, notificationName: Constants.NotificationName.onServiceDiscovered)
    }
    
    
    func onDeviceDisconned()
    {
        pathLossTriggered()
        
    }
    
    func onServiceDiscovered()
    {
        connectionEstablished()
        initProximityInterface()
    }
    
    
    

}
