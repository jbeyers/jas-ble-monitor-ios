/***************************************************************************
 
 OTAInfoViewController.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class OTAInfoViewController: BaseViewController {

    @IBOutlet weak var productIDLabel: UILabel!
    @IBOutlet weak var vendorIDLabel: UILabel!
    @IBOutlet weak var hardwareRevisionLabel: UILabel!
    @IBOutlet weak var hardwareVersionLabel: UILabel!
    @IBOutlet weak var firmwareVersionLabel: UILabel!
    
    @IBOutlet weak var productIDHeaderLabel: UILabel!
    @IBOutlet weak var vendorIDHeaderLabel: UILabel!
    @IBOutlet weak var hardwareRevisionHeaderLabel: UILabel!
    @IBOutlet weak var hardwareVersionHeaderLabel: UILabel!
    @IBOutlet weak var firmwareVersionHeaderLabel: UILabel!
    
    @IBOutlet weak var otaInfoLabel: UILabel!
    @IBOutlet weak var otaActivityIndicator: UIActivityIndicatorView!

    @IBOutlet weak var otaPercentageLabel: UILabel!
    @IBOutlet weak var upgradingCircularProgressView: KDCircularProgress!
    
    var currentProgress : Int = 0
    let progressMargin = 98
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customNavigationItem()
        setupUIElements()
       
        
        // Do any additional setup after loading the view.
    }
    
    func back(_ sender: UIBarButtonItem?) {
        navigationItem.leftBarButtonItems = nil
        self.performSegue(withIdentifier: "UNWIND_TO_VC", sender: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpgradeInfoLabel()
        setProgressViewStartAngle()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        GlobalCommunicator.shared.otaInterface?.infoDelegate = self
        updateOTAStatusOnUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func OTAPauseAction(_ sender: UIBarButtonItem){
        GlobalCommunicator.shared.otaInterface?.abortAndPause()
        
    }
    func OTAResumeAction(_ sender: UIBarButtonItem){
        GlobalCommunicator.shared.otaInterface?.initiateResumeRequest()

    }

    func removeRightBarButton(){
        
        self.navigationItem.rightBarButtonItem = nil

    }
    
    func setUpgradeInfoLabel(){
        
        if GlobalCommunicator.shared.otaInterface!.updateType.updateType != "" {
            otaInfoLabel.text = GlobalCommunicator.shared.otaInterface!.updateType.updateType
        }
    }
    
    
    
    func addPauseButtonToNavigationControl(){
        
        removeRightBarButton()
        let button: UIButton = UIButton(type:.custom)
        //set image for button
        button.setTitle("Pause", for: UIControlState())
        //add function for button
        button.addTarget(self, action:#selector(OTAInfoViewController.OTAPauseAction(_:)), for: UIControlEvents.touchUpInside)
        //set frame
        button.frame = CGRect(x: 0, y: 0,width: 80,height: 27)
        
        //Disable pause button in the case of force update
        
        if GlobalCommunicator.shared.otaInterface!.updateType == .forcedUpdate {
            button.isUserInteractionEnabled = false
            button.setTitleColor(UIColor.lightGray, for: UIControlState())
        }
        
        // Disable pause button if progress is greater than 98%

         if currentProgress >= progressMargin{
            
            button.isUserInteractionEnabled = false
            button.setTitleColor(UIColor.lightGray, for: UIControlState())
        }
        
        
        let newOtauButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = newOtauButton
    }
    
    
    func addResumeButtonToNavigationControl(){
        
        removeRightBarButton()

        let button: UIButton = UIButton(type:.custom)
        //set image for button
        button.setTitle("Resume", for: UIControlState())
        //add function for button
        button.addTarget(self, action:#selector(OTAInfoViewController.OTAResumeAction(_:)), for: UIControlEvents.touchUpInside)
        //set frame
        button.frame = CGRect(x: 0, y: 0,width: 80,height: 27)
        
        let newOtauButton = UIBarButtonItem(customView: button)
        
        self.navigationItem.rightBarButtonItem = newOtauButton
    }
  
    func customNavigationItem()
    {

        self.navigationController!.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "Roboto-Medium", size: CGFloat.getFontSizeToScreen(16.0,scale: 0.6))!,NSForegroundColorAttributeName:UIColor.white]
        self.navigationItem.title = "Updating Firmware"
        
        self.navigationItem.backBarButtonItem = nil
        
        let backImg: UIImage = UIImage(named: "BackButton")!
        let newbackBtn = UIButton(type: UIButtonType.custom)
        
        let stretchImage = backImg.resizableImage(withCapInsets: UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0))

        newbackBtn.setImage(stretchImage, for: UIControlState())
        newbackBtn.frame = CGRect(x: 0.0, y: 0.0, width: backImg.size.width + 18, height: backImg.size.height)
        newbackBtn.addTarget(self, action:  #selector(OTAInfoViewController.back(_:)), for: .touchUpInside)
        newbackBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.fill
        
        
        let negativeSpacebar = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeSpacebar.width = -8
        navigationItem.leftBarButtonItems = [negativeSpacebar, UIBarButtonItem(customView: newbackBtn)]
    }
    
    func setupUIElements(){
        
        productIDLabel.font = UIFont(name: "Roboto-Medium", size:CGFloat.getFontSizeToScreen(14.0,scale: 0.6))
        vendorIDLabel.font = UIFont(name: "Roboto-Medium", size:CGFloat.getFontSizeToScreen(14.0,scale: 0.6))
        hardwareRevisionLabel.font = UIFont(name: "Roboto-Medium", size:CGFloat.getFontSizeToScreen(14.0,scale: 0.6))
        hardwareVersionLabel.font = UIFont(name: "Roboto-Medium", size:CGFloat.getFontSizeToScreen(14.0,scale: 0.6))
        firmwareVersionLabel.font = UIFont(name: "Roboto-Medium", size:CGFloat.getFontSizeToScreen(14.0,scale: 0.6))
        
        productIDHeaderLabel.font = UIFont(name: "Roboto-Medium", size:CGFloat.getFontSizeToScreen(14.0,scale: 0.6))
        vendorIDHeaderLabel.font = UIFont(name: "Roboto-Medium", size:CGFloat.getFontSizeToScreen(14.0,scale: 0.6))
        hardwareRevisionHeaderLabel.font = UIFont(name: "Roboto-Medium", size:CGFloat.getFontSizeToScreen(14.0,scale: 0.6))
        hardwareVersionHeaderLabel.font = UIFont(name: "Roboto-Medium", size:CGFloat.getFontSizeToScreen(14.0,scale: 0.6))
        firmwareVersionHeaderLabel.font = UIFont(name: "Roboto-Medium", size:CGFloat.getFontSizeToScreen(14.0,scale: 0.6))

        
        otaInfoLabel.font = UIFont(name: "Roboto-Medium", size:CGFloat.getFontSizeToScreen(14.0,scale:0.6));
        otaPercentageLabel.font = UIFont(name: "Roboto-Medium", size:CGFloat.getFontSizeToScreen(27.0,scale:0.6));
        
        productIDLabel.textColor = UIColor.black
        vendorIDLabel.textColor = UIColor.black
        hardwareRevisionLabel.textColor = UIColor.black
        hardwareVersionLabel.textColor = UIColor.black
        firmwareVersionLabel.textColor = UIColor.black
        
        productIDHeaderLabel.textColor = UIColor.black
        vendorIDHeaderLabel.textColor = UIColor.black
        hardwareRevisionHeaderLabel.textColor = UIColor.black
        hardwareVersionHeaderLabel.textColor = UIColor.black
        firmwareVersionHeaderLabel.textColor = UIColor.black
        
        otaInfoLabel.textColor = UIColor.black
        otaPercentageLabel.textColor = UIColor.black
       
        
        //        Roboto = ["Roboto-Thin", "Roboto-Italic", "Roboto-Light", "Roboto-BoldItalic", "Roboto-LightItalic", "Roboto-ThinItalic", "Roboto-Bold", "Roboto-Regular", "Roboto-Medium"]
    }
    
    func updateOTAStatusOnUI(){
        
        DispatchQueue.main.async(execute: {
            
        switch(GlobalCommunicator.shared.otaInterface!.updateStatus){
            
        case OTAUpdateStatus.otaAvailable:
            
            break
            
        case OTAUpdateStatus.updateAvailable:
     
            self.otaInfoLabel.text = "Update Available"
            self.otaActivityIndicator.stopAnimating()
            self.removeRightBarButton()
            break
            
        case OTAUpdateStatus.updated:
            self.otaInfoLabel.text = "Update Finished"
            self.otaActivityIndicator.stopAnimating()
            self.removeRightBarButton()

            break
        case OTAUpdateStatus.updating:
            
            self.otaInfoLabel.text = "Updating to firmware version \(GlobalCommunicator.shared.otaInterface!.image!.firmwareVersion.description)"
            self.otaActivityIndicator.startAnimating()
            self.addPauseButtonToNavigationControl()
            break
        case OTAUpdateStatus.paused:
            
            self.otaInfoLabel.text = "Paused"
            self.otaPercentageLabel.text = "Paused"
            self.otaActivityIndicator.stopAnimating()
            self.addResumeButtonToNavigationControl()

            break
        }
        })
        
        
        if var productID = GlobalCommunicator.shared.otaInterface!.currentDeviceInformation!.productID
        {
            //productIDLabel.text = "\(Data(bytes: UnsafePointer<UInt8>(&productID), count:2).hex0xFormat())".trimmingCharacters(in: CharacterSet(charactersIn: "<>"))
            productIDLabel.text = "\(Data(bytes: &productID, count:2).hex0xFormat())".trimmingCharacters(in: CharacterSet(charactersIn: "<>"))
        }
        if var vendorID = GlobalCommunicator.shared.otaInterface!.currentDeviceInformation!.vendorID
        {
            //vendorIDLabel.text = "\(Data(bytes: UnsafePointer<UInt8>(&vendorID), count:2).hex0xFormat())".trimmingCharacters(in: CharacterSet(charactersIn: "<>"))
            vendorIDLabel.text = "\(Data(bytes: &vendorID, count:2).hex0xFormat())".trimmingCharacters(in: CharacterSet(charactersIn: "<>"))
        }
        if let fw = GlobalCommunicator.shared.otaInterface!.currentDeviceInformation!.firmwareVersion
            {firmwareVersionLabel.text = fw.description}
        if let hw = GlobalCommunicator.shared.otaInterface!.currentDeviceInformation!.hardwareVersion
            {hardwareVersionLabel.text = hw.description}
        if let hr = GlobalCommunicator.shared.otaInterface!.currentDeviceInformation!.hardwareRevision
            {hardwareRevisionLabel.text = "\(hr)"}
    }
    
    func updateprogressView(_ withString:String,progress:Float?){
        
        
        DispatchQueue.main.async(execute: {
            
        self.otaInfoLabel.text = withString
            
        if progress != nil {
            
            //Keep the progress value for non deterministic
            
            if progress > 0
            {
                self.upgradingCircularProgressView.animateToAngle(Int(Float(360)*progress!), duration: 0.1, completion: nil)
                self.otaPercentageLabel.text = "\(Int(progress!*100))%"
                
                // Disable pause button if the progress reaches greater than 98%
                
                
                if Int(progress!*100) >= self.progressMargin {
                    
                    let pauseButton = self.navigationItem.rightBarButtonItem?.customView as? UIButton
                    
                    if pauseButton != nil && pauseButton!.isUserInteractionEnabled{
                        pauseButton!.isUserInteractionEnabled = false
                        pauseButton!.setTitleColor(UIColor.lightGray, for: UIControlState())
                    }
                }
            }
            
            self.currentProgress = Int(progress!*100)
            
            //Keep the progress value and show activity
            
            if progress == -2{
                
                self.upgradingCircularProgressView.animateToAngle(360, duration: 0.1, completion: nil)
                self.otaInfoLabel.text = withString
            }
            
        }
        
            
        })
    }
    
    
    func setProgressViewStartAngle(){
        
        if  GlobalCommunicator.shared.otaInterface?.updationProgress != nil && self.upgradingCircularProgressView.angle == 0{
            self.upgradingCircularProgressView.animateToAngle(Int(Float(360)*(GlobalCommunicator.shared.otaInterface?.updationProgress!)!), duration: 0.1, completion: nil)
            currentProgress = Int((GlobalCommunicator.shared.otaInterface?.updationProgress!)!*100)
             self.otaPercentageLabel.text = "\(Int((GlobalCommunicator.shared.otaInterface?.updationProgress!)!*100))%"
        }
    }
    
    
    //OTA Progress
    
    func showOTAFailureAlert(_ withError:NSError){
        
        let otaAlert = UIAlertController(title:"OTA Failed",message: withError.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
        
        
        let IgnoreAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
            UIAlertAction in
            
            GlobalCommunicator.shared.otaInterface?.updateStatus = OTAUpdateStatus.updateAvailable
            self.back(nil)

            
            
        }
        
        otaAlert.addAction(IgnoreAction)
        self.present(otaAlert, animated: true, completion: nil)
    }

    
}
extension OTAInfoViewController:AtmelOTAInterfaceInfoDelegate{
    
    func atmelOtaInterfaceUpdateValueChanged(){
        
        updateOTAStatusOnUI()
        
    }
    func updatedProgressDataInfo(_ withString:String,progress:Float?){
        updateprogressView(withString, progress: progress)
    }
    func otaFailed(_ error:NSError){
        showOTAFailureAlert(error)
    }
    
}
