/* ***************************************************************************

//BLEServiceCell.swift
//
UITableViewCell subclass;
Each cell corresponds to each service discovered to be populated in the service list tableview in BLEServiceListVC
//


Copyright (c) 2015, Atmel Corporation. All rights reserved.
Released under NDA
Licensed under Atmel's Limited License Agreement.


THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Atmel Corporation: http://www.atmel.com

******************************************************************************/

import Foundation
import CoreBluetooth


import UIKit

class BLEServiceCell : UITableViewCell
{
    
    @IBOutlet var serviceName: UILabel!
    @IBOutlet var serviceAddress: UILabel!
    @IBOutlet var serviceDescription: UILabel!
    @IBOutlet var serviceThumbImageView: UIImageView!
    var servicecSegueEnum: ServiceScreenSegueEnum!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setCell(_ serviceInfo : CBService!)
    {
        self.setupUIElements()
        self.serviceName.text = serviceInfo.uuid.getServiceName()
        
        let AssignedNo = "Assigned Number";
        
        // Handle GattDB feature
        
        if !serviceInfo.isGattService() {
            
            if serviceInfo.uuid.uuidString.characters.count > 16
            {
                self.serviceAddress.text = AssignedNo
                
                let AddressText = "0x\(serviceInfo.uuid.uuidString)\n\(serviceInfo.uuid.getServiceDescription())"
                let range = (AddressText as NSString).range(of: "0x\(serviceInfo.uuid.uuidString)")
                let totalRange = (AddressText as NSString).range(of: AddressText)
                let attributedAddressString = NSMutableAttributedString(string:AddressText)
                attributedAddressString.addAttribute(NSFontAttributeName, value: UIFont(name: "Helvetica", size: 15)! , range: totalRange)
                attributedAddressString.addAttribute(NSFontAttributeName, value: UIFont(name: "Helvetica", size: 11)! , range: range)
                attributedAddressString.addAttribute(NSForegroundColorAttributeName, value: UIColor.black , range: range)
                
                self.serviceDescription.attributedText = attributedAddressString
            }
            else
            {
                let AddressText = "\(AssignedNo) 0x"+"\(serviceInfo.uuid.uuidString)"
                self.serviceAddress.text = AddressText
                self.serviceDescription.text = serviceInfo.uuid.getServiceDescription()
            }
        }
        else{
            self.serviceName.text = "Generic Information"
            self.serviceAddress.text =  "\(AssignedNo) " + "NA"
            self.serviceDescription.text = "Generic Information allows to Debug/Verify any Custom/Adopted profiles"
        }
        

        let info = BLEServiceList.getServiceInfo(serviceInfo)
        self.serviceThumbImageView.image = info.0 // UIImage for service thumb image
        self.servicecSegueEnum = info.1 // Segue Name for service details screen
        
        if UIDevice.is_iPad(){
        serviceName.layoutIfNeeded()
        serviceAddress.layoutIfNeeded()
        serviceDescription.layoutIfNeeded()
            self.layoutIfNeeded()}
        
    }
    
    func setupUIElements(){
        
        serviceName.font = UIFont(name: "Roboto-Bold", size:CGFloat.getFontSizeToScreen(16.0,scale:0.5));
        serviceAddress.font = UIFont(name: "Roboto-Medium", size:CGFloat.getFontSizeToScreen(16.0,scale:0.5));
        serviceDescription.font = UIFont(name: "Roboto-Regular", size:CGFloat.getFontSizeToScreen(14.0,scale:0.5));
    }
    
}
