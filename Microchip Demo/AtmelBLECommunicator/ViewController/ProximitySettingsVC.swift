/* ***************************************************************************

ProximitySettingsVC.swift

UIViewController Class for modify the predefined/ default values of Proximity profile .
User can Change the Alert level for Immediate alert and Link Loss service.



Copyright (c) 2015, Atmel Corporation. All rights reserved.
Released under NDA
Licensed under Atmel's Limited License Agreement.


THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Atmel Corporation: http://www.atmel.com

******************************************************************************/

import Foundation
import CoreBluetooth
import UIKit

class ProximitySettingsVC: GATTBaseViewController ,RangeSliderDelegate{
    
    
    var rangeSlider = RangeSlider(frame: CGRect.zero)
    
    
    var minimumRSSI:Double = abs(Double(Constants.RSSI.minimumRSSI))
    var maximumRSSI:Double = abs(Double(Constants.RSSI.maximumRSSI))
    var minRSSISafe:Double = abs(Double(Constants.RSSI.minRSSISafe))
    var minRSSIMid:Double  = abs(Double(Constants.RSSI.minRSSIMid))

  
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationItem.title = "Settings"
        setAlertLevelSettings()

        // Do any additional setup after loading the view, typically from a nib.
                SafeControl.roundRectView()
                MidControl.roundRectView()
                DangerControl.roundRectView()

        minRSSISafe = UserDefaults.standard.double(forKey: Constants.UserDefaultsKey.safeZoneValueKey)
        minRSSIMid = UserDefaults.standard.double(forKey: Constants.UserDefaultsKey.midZoneValueKey)
        initCustomSlider()
        
    }
    
    func initCustomSlider()
    {
      
        self.rangeSlider.curvaceousness = 2.0
        self.rangeSlider.RangeDelegate = self
        self.rangeSlider.minimumValue = maximumRSSI
        self.rangeSlider.maximumValue  = minimumRSSI
        self.rangeSlider.lowerValue = minRSSISafe
        self.rangeSlider.upperValue = minRSSIMid
        self.rangeSlider.rangeLowerValue=15
        self.rangeSlider.rangeMidValue=15
        self.rangeSlider.rangeUpperValue=15
        
        self.rangeSlider.isRanged = true
        ZoneSelectionView.addSubview(rangeSlider)
    }
    

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.layoutFix()
        let margin: CGFloat = 5.0
        let width = view.bounds.width - 2.0 * margin
        let yDistance = margin + topLayoutGuide.length + 40
        rangeSlider.frame = CGRect(x: margin, y: yDistance,
            width: width, height: 31.0)
//        rangeSlider.layer.cornerRadius = 16
//        rangeSlider.clipsToBounds = true
//        rangeSlider.layer.masksToBounds = true
        
        
        
    }
    
    @IBOutlet weak var SafeControl: UISegmentedControl!
    @IBOutlet weak var MidControl: UISegmentedControl!
    @IBOutlet weak var DangerControl: UISegmentedControl!
    
    @IBOutlet weak var ZoneSelectionView: UIView!
 
    
    func setAlertLevelSettings(){
        
        let userDefaults:UserDefaults = UserDefaults.standard
        let safeAlertLevel: NSNumber? = userDefaults.value(forKey: Constants.UserDefaultsKey.safeZoneKey) as? NSNumber
        let midAlertLevel:NSNumber? = userDefaults.value(forKey: Constants.UserDefaultsKey.midZoneKey) as? NSNumber
        let dangerAlertLevel:NSNumber? = userDefaults.value(forKey: Constants.UserDefaultsKey.dangerZoneKey) as? NSNumber
        
        if safeAlertLevel != nil {
            SafeControl.selectedSegmentIndex = safeAlertLevel!.intValue
            setSegmentcontrolSelectionFor(SafeControl)
        }
        else{
            setSegmentcontrolSelectionFor(SafeControl)
        }
        
        if midAlertLevel != nil {
            MidControl.selectedSegmentIndex = midAlertLevel!.intValue
            setSegmentcontrolSelectionFor(MidControl)
        }
        else{
            setSegmentcontrolSelectionFor(MidControl)
        }
        if dangerAlertLevel != nil {
            DangerControl.selectedSegmentIndex = dangerAlertLevel!.intValue
            setSegmentcontrolSelectionFor(DangerControl)
        }
        else{
            setSegmentcontrolSelectionFor(DangerControl)
        }
    }

    @IBAction func onValueChanged(_ sender: AnyObject)
    {
        if let segmentedControl = sender as? UISegmentedControl
        {
            let selectAlertLevelRawValue = UInt8(segmentedControl.selectedSegmentIndex)
            let selectedLevel:NSNumber = NSNumber(value: selectAlertLevelRawValue as UInt8)
            setSegmentcontrolSelectionFor(segmentedControl)
            switch segmentedControl.tag
            {
            case 2: UserDefaults.standard.set(selectedLevel, forKey: Constants.UserDefaultsKey.dangerZoneKey)
            case 3:UserDefaults.standard.set(selectedLevel, forKey: Constants.UserDefaultsKey.midZoneKey)
            case 4:UserDefaults.standard.set(selectedLevel, forKey: Constants.UserDefaultsKey.safeZoneKey)
            default:
                print(" ")
                
            }
        }
        
    }
    
    func onValueChangedSlider() {
        print("Range Self value changed: (\(rangeSlider.minimumValue) (\(rangeSlider.lowerValue) \(rangeSlider.upperValue))(\(rangeSlider.maximumValue)")
        
        
        
        UserDefaults.standard.set(rangeSlider.lowerValue, forKey: Constants.UserDefaultsKey.safeZoneValueKey)
        UserDefaults.standard.set(rangeSlider.upperValue, forKey: Constants.UserDefaultsKey.midZoneValueKey)
    }
    
    
    func setSegmentcontrolSelectionFor(_ segmentedControl: UISegmentedControl){
        
        let selectAlertLevelRawValue = UInt8(segmentedControl.selectedSegmentIndex)
        switch selectAlertLevelRawValue {
        case 0:
            
            segmentedControl.firstSegment()?.tintColor = UIColor.AtmelRedColor()
            segmentedControl.secondSegment()?.tintColor = UIColor.AtmelDarkGrayColor()
            segmentedControl.thirdSegment()?.tintColor =  UIColor.AtmelDarkGrayColor()
            
        case 1:
            
            segmentedControl.firstSegment()?.tintColor = UIColor.AtmelDarkGrayColor()
            segmentedControl.secondSegment()?.tintColor = UIColor.AtmelRedColor()
            segmentedControl.thirdSegment()?.tintColor =  UIColor.AtmelDarkGrayColor()
        default:
            
            segmentedControl.firstSegment()?.tintColor = UIColor.AtmelDarkGrayColor()
            segmentedControl.secondSegment()?.tintColor = UIColor.AtmelDarkGrayColor()
            segmentedControl.thirdSegment()?.tintColor =  UIColor.AtmelRedColor()
        }

    }
    
    
    
}
