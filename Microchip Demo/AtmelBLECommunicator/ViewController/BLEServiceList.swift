/* ***************************************************************************

//BLEServiceList.swift
//
ViewController for Listing all discovered services on the current peripheral connected
//


Copyright (c) 2015, Atmel Corporation. All rights reserved.
Released under NDA
Licensed under Atmel's Limited License Agreement.


THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Atmel Corporation: http://www.atmel.com

******************************************************************************/


import Foundation
import UIKit
import CoreBluetooth

class BLEServiceList: GATTBaseViewController
{
    @IBOutlet weak var BLEServiceNotAvailable: UIView!
    @IBOutlet weak var BLEServiceListTable: UITableView!
    @IBOutlet weak var BLEServiceListCollection: UICollectionView!
    var myDevice = GlobalCommunicator.shared.connectedDevice!.BLEPeripheral
    var myDeviceServices = [CBService]()
    
    var displayTimer : Timer?
    @IBOutlet weak var BlueIndicator : UIActivityIndicatorView!

   
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.layoutFix()
        
            }

    override func viewDidLoad()
    {
        super.viewDidLoad()
        addListner()
        //Add observer to events
        
        defaultUserDefaultValues()
        configureServiceListScreen()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if myDevice?.services == nil
        {
            myDeviceServices.removeAll()
        }
        else
        {
            self.BLEServiceListTable.reloadData()
        }

    }
   
    override func viewDidAppear(_ animated: Bool)
    {
        if myDevice?.services == nil
        {
            getServiceList()
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    
        super.viewWillDisappear(animated)
    }
    
    @IBAction func unwindToViewController(_ segue: UIStoryboardSegue) {
    }

    
    
    //OTA
    func configureServiceListScreen()
    {
        customNavigationItem()
        configureServiceList()
        //To avoid space between Nav Bar and TableView Cell

    }
    

    func configureServiceList()
    {
//        if UIDevice.is_iPad()
//        {
//            self.BLEServiceListCollection.hidden = false
//            self.BLEServiceListTable.hidden = true
//            self.BLEServiceListTable.delegate = nil
//            self.BLEServiceListTable.dataSource = nil
//        }
//        else
//        {
//            self.BLEServiceListCollection.hidden = true
//            self.BLEServiceListCollection.delegate = nil
//            self.BLEServiceListCollection.dataSource = nil
//            self.BLEServiceListTable.hidden = false
//            
//        }
        
        
      
        self.BLEServiceListTable.isHidden = false
    }
    
    func customNavigationItem()
    {
         self.navigationController!.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "Roboto-Medium", size: CGFloat.getFontSizeToScreen(16.0,scale: 0.6))!,NSForegroundColorAttributeName:UIColor.white]

    }
    
    
    func getServiceList()
    {
        
        NSLog("Getting service List");
        if myDevice?.state == CBPeripheralState.connected
        {
            if let _ = myDevice
            {
                NSLog("Getting all services and Characteristics from = \(String(describing: myDevice))");

                GlobalCommunicator.shared.sourceDevice = BLESourceDevice( peripheral: myDevice!, withDelegate: self)
                GlobalCommunicator.shared.sourceDevice.getAllServicesAndCharactesristics()
                
            }
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func defaultUserDefaultValues()
    {
        UserDefaults.updateNotifyState(true , UserDefaultsKey: Constants.UserDefaultsKey.healthThermoMeterStatusKey)
        UserDefaults.updateNotifyState(true , UserDefaultsKey: Constants.UserDefaultsKey.heartRateStatusKey)
        UserDefaults.updateNotifyState(true , UserDefaultsKey: Constants.UserDefaultsKey.bloodPressureStatusKey)
        UserDefaults.updateNotifyState(true , UserDefaultsKey: Constants.UserDefaultsKey.batteryStatusKey)
        UserDefaults.updateNotifyState(true , UserDefaultsKey: Constants.UserDefaultsKey.scanParamStatusKey)
        
        UserDefaults.standard.set(3, forKey: Constants.UserDefaultsKey.findMeAlertStatusKey)
        UserDefaults.standard.set(0, forKey: Constants.UserDefaultsKey.batteryValueKey)
        UserDefaults.standard.set(0, forKey: Constants.UserDefaultsKey.scanParamIntervalValueKey)
        UserDefaults.standard.set(0, forKey: Constants.UserDefaultsKey.scanParamWindowValueKey)
        
        GlobalCommunicator.shared.sppMessages.removeAll(keepingCapacity: false)
        //        GlobalCommunicator.shared.batteryService = nil
    }

    //GlobalCommunicator.shared.serialPortInterface.
    
    func isCustomChatService()
    {
        if BLEDeviceListVC.isCustomSerialChat()
        {
            GlobalCommunicator.shared.serialPortInterface.initialSet()
            let MESSAGE_CHAR_LIMIT = 150
            GlobalCommunicator.shared.serialPortInterface.startMessageBoxWithCharLimit(MESSAGE_CHAR_LIMIT)
        }
        
       
    }

    
    func navigateToSelectedServiceScreen(_ selectedService : ServiceScreenSegueEnum)
    {
 
        var destinationView = selectedService
        if selectedService == ServiceScreenSegueEnum.IAS_VC
        {
            for profile in GlobalCommunicator.shared.sourceDevice.activeProfiles
            {
                if profile == BLEProfile.FindMe
                {
                    destinationView = ServiceScreenSegueEnum.FIND_VC
                    break
                }
            }

        }
        self.performSegue(withIdentifier: destinationView.rawValue, sender: self)
    }
    
    
    
}

/*----------------------------------------------------------------------------------------------------------*/
// MARK: BLECommunicatorDelegate Methods
/*----------------------------------------------------------------------------------------------------------*/


extension BLEServiceList: BLESourceDeviceDelegate
{
    func bleSourceDevice(_ bleSourceDevice: BLESourceDevice, didDiscoverServicesWithError error: NSError!) {
        
        if error == nil
        {
            serviceUpdated()
        }
        
    }
    
    func bleSourceDevice(_ bleSourceDevice: BLESourceDevice, didDiscoverCharacteristicsForService service: CBService, withError error: NSError!)
    {
        if error == nil
        {
            NSLog("didDiscoverCharacteristicsForService")
        }
    }
    
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice, didDiscoverAllServicesAndCharacteristicsWithError error:NSError!){
        
        if error == nil
        {
            serviceUpdated()
        }

    }
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice, DidInitializedOTAService isOTAPresent: Bool){
        
        if isOTAPresent {
            GlobalCommunicator.shared.otaInterface!.infoDelegate = self
            updateOTAStatusOnUI()
        }
        else{
            removeOTAStatusFromUI()
        }
       
    }

    
    func serviceUpdated()
    {
        
        reloadServiceList()
        
        if getServiceCount() > 0
        {
            isCustomChatService()
            
            //Changed trigger position
        }
    }
}


extension BLEServiceList : UITableViewDelegate,UITableViewDataSource ,UICollectionViewDataSource, UICollectionViewDelegate
{
    
    /*----------------------------------------------------------------------------------------------------------*/
    // MARK: BLE Device List TableView Delegate and DataSource
    /*----------------------------------------------------------------------------------------------------------*/
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return CGFloat.getFontSizeToScreen(205.0,scale:0.5)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return getServiceCount()
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let servicecellIdentifier = "BleServiceCell"
        //BleServiceCollectionCell
        let cell = self.BLEServiceListTable.dequeueReusableCell(withIdentifier: servicecellIdentifier) as! BLEServiceCell
        cell .setCell(myDeviceServices[indexPath.row])
        return cell
        
    }
    
  
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat{
        
            return 10.0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        
        let tempFooterView = UIView(frame: CGRect(x: 00, y: 0, width: tableView.bounds.size.width, height: 10))
        return tempFooterView
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        let selectedService  = myDeviceServices[indexPath.row]
        
        if selectedService.uuid.isEqual(CBUUID(bleService: BLEService.GenericAccessService)) && AtmelOTAInterface.hasOTA(GlobalCommunicator.shared.sourceDevice.CBSPeripheral){
            showGattDBInvalidAlertForOTA()
            return
        }
        
        GlobalCommunicator.shared.selectedService = selectedService
        let cell = self.BLEServiceListTable.cellForRow(at: indexPath) as! BLEServiceCell
        navigateToSelectedServiceScreen(cell.servicecSegueEnum)
    }
    
    
    
    /*----------------------------------------------------------------------------------------------------------*/
    // MARK: BLE Service List CollectionView Delegate and DataSource
    /*----------------------------------------------------------------------------------------------------------*/
    
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
       return getServiceCount()
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        
        let servicecellIdentifier = "BleServiceCollectionCell"
        let cell = self.BLEServiceListCollection.dequeueReusableCell(withReuseIdentifier: servicecellIdentifier, for: indexPath) as! BLEServiceCollectionCell
        
        cell.setCell(myDevice!.services![indexPath.row])
        return cell
    }
    
    // --- UICollectionViewDelegate protocol ---
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        
        GlobalCommunicator.shared.selectedService = myDevice!.services![indexPath.row]
        let cell = self.BLEServiceListCollection.cellForItem(at: indexPath) as! BLEServiceCollectionCell
        navigateToSelectedServiceScreen(cell.servicecSegueEnum)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, insetForSectionAtIndex section: NSInteger) -> UIEdgeInsets {
        
        // (top, left, bottom, right)
        if let _ = myDevice!.services
        {
            let space = getFlowLayout( myDevice!.services!.count)
            return UIEdgeInsetsMake(0, space, 0, space)
        }
        
        return UIEdgeInsets.zero
        
    }
    
    func collectionView(_ collectionView: UICollectionView!, layout collectionViewLayout: UICollectionViewLayout!,        minimumInteritemSpacingForSectionAtIndex section:Int)->CGFloat
    {
        if let _ = myDevice!.services
        {
            return getFlowLayout(myDevice!.services!.count)
        }
        return 0
    }
    
    
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval)
    {
        self.BLEServiceListTable.reloadData()
    }
    
    
    
    
    /*----------------------------------------------------------------------------------------------------------*/
    // MARK: BLE Device List Updating Common
    /*----------------------------------------------------------------------------------------------------------*/
    
    func getServiceCount() -> Int
    {
        //BLEServiceNotAvailable
        
        myDeviceServices = (myDevice?.servicesHandlingOTAAndGattDB())!
        
        if myDeviceServices.count != 1 {
            hideNoserviceView()
            return myDeviceServices.count
        }
        
        showNoServiceView()
        return 0
    }
    
    
    func hideNoserviceView()
    {
        displayTimer?.invalidate()
        displayTimer = nil
        BLEServiceNotAvailable.isHidden = true
        BlueIndicator.stopAnimating()
    }
    
    func showNoServiceView()
    {
        if displayTimer == nil
        {

            displayTimer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(BLEServiceList.showAfterDelay(_:)), userInfo: nil, repeats: false)
        }
    }
    
    func showAfterDelay(_ timer : Timer) {
       
        BLEServiceNotAvailable.isHidden = false
        // here code to perform
    }

    func showGattDBInvalidAlertForOTA(){
        
        let gattInvalidAlert = UIAlertController(title: "", message: "Generic Information feature is disabled for OTA service.", preferredStyle: .alert)
        let doneAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        gattInvalidAlert.addAction(doneAction)
        self.present(gattInvalidAlert, animated: true, completion: nil)
    }
    
    
    func getFlowLayout(_ count : Int) -> CGFloat
    {
        let cellSize = CGSize(width: 250,height: 360)
        let width = self.BLEServiceListCollection.frame.size.width
        let MAX_ROW_COUNT = Int(width / cellSize.width)
        if count >= MAX_ROW_COUNT || count == 0
        {
            return 0
        }
        else
        {
            //   ||-----|_cell_|-----||
            //   ||--XX--XX--XX--XX--||
            //   ||-----XX----XX-----||
            //   ||--------XX--------||
            let blankSpace = width  - ( cellSize.width * CGFloat(count))
            return blankSpace / (2.0 * CGFloat(count))
        }
    }
    
    func reloadServiceList()
    {
//        if UIDevice.is_iPad()
//        {
//            self.BLEServiceListCollection.reloadData()
//        }
//        else
//        {
//            self.BLEServiceListTable.reloadData()
//        }
        
        self.BLEServiceListTable.reloadData()

    }
    
    func getSelectedCellIndex()-> Int
    {
        
//        if UIDevice.is_iPad()
//        {
//            let indexPaths : NSArray = self.BLEServiceListCollection.indexPathsForSelectedItems()!
//            let indexPath : NSIndexPath = indexPaths[0] as! NSIndexPath
//            return indexPath.row
//            
//        }
//        else
//        {
//            let indexPathq = self.BLEServiceListTable.indexPathForSelectedRow
//            return indexPathq!.row
//        }

        let indexPathq = self.BLEServiceListTable.indexPathForSelectedRow
        return indexPathq!.row
    }

}

extension BLEServiceList
{
    func addListner()
    {
        NotificationCenter.listenNotification(self, notificationName: Constants.NotificationName.onDeviceConnected)
        NotificationCenter.listenNotification(self, notificationName: Constants.NotificationName.onDeviceDisconned)
    }
    //Function to handle device connection
    
    func onDeviceConnected()
    {
        print("Notification received on connect")
        getServiceList()
    }
    
    //
    
    func onDeviceDisconned()
    {
        //To Do
        UserDefaults.standard.set(3, forKey: Constants.UserDefaultsKey.findMeAlertStatusKey)

    }
    
    class func getServiceInfo(_ serviceInfo : CBService!) -> (UIImage , ServiceScreenSegueEnum)
    {
        
        var serviceThumb = "GattDB"
        var servicecSegue = ServiceScreenSegueEnum.GATT_VC
        if serviceInfo.isImmediateAlert()
        {
            serviceThumb = "IAS"
            servicecSegue = ServiceScreenSegueEnum.IAS_VC
        }
        else if serviceInfo.isLinkLoss()
        {
            serviceThumb = "LLS"
            servicecSegue = ServiceScreenSegueEnum.LLS_VC
        }
        else if serviceInfo.isTxPower()
        {
            serviceThumb = "TPS"
            servicecSegue = ServiceScreenSegueEnum.TPS_VC
        }
        else if serviceInfo.isDeviceInfo()
        {
            serviceThumb = "DIS"
            servicecSegue = ServiceScreenSegueEnum.DIS_VC
        }
        else if serviceInfo.isThermoMeter()
        {
            serviceThumb = "HTS"
            servicecSegue = ServiceScreenSegueEnum.HTS_VC
        }
        else if serviceInfo.isHeartRate()
        {
            serviceThumb = "HRS"
            servicecSegue = ServiceScreenSegueEnum.HRS_VC
        }
        else if serviceInfo.isBloodPressure()
        {
            serviceThumb = "BLS"
            servicecSegue = ServiceScreenSegueEnum.BLS_VC
        }
        else if serviceInfo.isBattery()
        {
            serviceThumb = "BAS"
            servicecSegue = ServiceScreenSegueEnum.BAS_VC
        }
        else if serviceInfo.isSerialPort()
        {
             serviceThumb = "SPP"
            servicecSegue = ServiceScreenSegueEnum.SPP_VC

        }
        else if serviceInfo.isScanParam()
        {
            serviceThumb = "ScPS"
            servicecSegue = ServiceScreenSegueEnum.ScPS_VC
            
        }
        else if serviceInfo.isGattService(){
            
            serviceThumb = "GattDB"
            servicecSegue = ServiceScreenSegueEnum.GATTDB_VC
        }
//        if UIDevice.is_iPad()
//        {
//            serviceThumb = serviceThumb+"_iPad"
//        }
        //defdfdfdfdf/d/fdf/df/df/df/df/df/Ayyooooooo
//          servicecSegue = ServiceScreenSegueEnum.GATT_VC
        return (UIImage(named: serviceThumb)!,servicecSegue)
    }

}
   
