/***************************************************************************
 
 GattDBServiceDetailsVC.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import UIKit
import CoreBluetooth

enum BLECharacteristicProperties : String{
    
    case PropertyWrite      = "Write"
    case PropertyRead       = "Read"
    case PropertyIndicate   = "Indicate"
    case Propertynotify     = "Notify"
}

class GattDBServiceDetailsVC: GATTBaseViewController {
    

    let details = ["ASCII","HEX","DATE","TIME"]
    let CELL_IDENTIFIER = "serviceDetailCell"
    let SEGUE_IDENTIFIER = "DescriptorVC"
    let SEPERATOR_WIDTH : CGFloat = 4.0
    
    let gattInterface = GlobalCommunicator.shared.gattDBInterface
    var isHexAlertShown : Bool = false
    
    @IBOutlet weak var gattDetailsTableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var gattDetailsTableView: UITableView!
    
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var readViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var writeViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var notifyViewWidthConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var readViewSeperatorWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var writeViewSeperatorWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var notifyViewSeperatorWidthConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var serviceNameLabel: UILabel!
    @IBOutlet weak var characteristicNameLabel: UILabel!
    @IBOutlet weak var descriptorButton: UIButton!
    @IBOutlet weak var notifyButton: UIButton!
    @IBOutlet weak var indicateButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        gattInterface?.gattOperationsDelegate = self
        GlobalCommunicator.shared.gattDBInterface?.gattDescriptorOperationsDelegate = self

        initializeUI()
        
        GlobalCommunicator.shared.gattDBInterface?.discoverDescriptorsForCharacteristic(GlobalCommunicator.shared.selectedCharacteristic)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "GATT DB"
        checkAndSetNotifyButtons()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        configureView()
        configurePropertyButtons()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if self.navigationController?.viewControllers.contains(self) == false{
            handleCharacteristicNotifications()
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    @IBAction func readButtonClicked(_ sender: UIButton) {
     
        gattInterface?.readValueForCharacteristic(GlobalCommunicator.shared.selectedCharacteristic)
    }
  
    

    @IBAction func writeButtonClicked(_ sender: UIButton) {
        
        if !isHexAlertShown {
            showEnterHexAlert()
        }
    }
    
    
    
    @IBAction func notifyButtonClicked(_ sender: UIButton) {
        
        if sender.isSelected {
            gattInterface?.stopNotificationsForcharacteristic(GlobalCommunicator.shared.selectedCharacteristic)
            sender.isSelected = false
        }
        else{
            gattInterface?.startNotificationsForcharacteristic(GlobalCommunicator.shared.selectedCharacteristic)
            sender.isSelected = true
        }
    }
    
    
    @IBAction func indicateButtonClicked(_ sender: UIButton) {
        
        if sender.isSelected {
            gattInterface?.stopNotificationsForcharacteristic(GlobalCommunicator.shared.selectedCharacteristic)
            sender.isSelected = false
        }
        else{
            gattInterface?.startNotificationsForcharacteristic(GlobalCommunicator.shared.selectedCharacteristic)
            sender.isSelected = true
        }
    }
    
    
    @IBAction func descriptorButtonClicked(_ sender: UIButton) {
        self.performSegue(withIdentifier: SEGUE_IDENTIFIER, sender: self)
    }
    
    
    
    
}


extension GattDBServiceDetailsVC : UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return details.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER) as? DetailsTableViewCell
        
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: CELL_IDENTIFIER) as? DetailsTableViewCell
        }
        
        cell?.detailsOptionLabel.text = details[indexPath.row]
        cell?.detailsTextField.tag = indexPath.row
        cell?.detailsTextField.delegate = self
        
        // Textfield is shown only for hex and ascii fields
        switch indexPath.row {
        case 0,1:
            cell?.detailsLabel.isHidden = true
            cell?.detailsTextField.isHidden = false
        default:
            cell?.detailsLabel.isHidden = false
            cell?.detailsTextField.isHidden = true
        }
        
        
        return cell!
    }
}


extension GattDBServiceDetailsVC : UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return gattDetailsTableViewHeightConstraint.constant/4.0
    }
}


// MARK:- UI

extension GattDBServiceDetailsVC {
    
    
    
    fileprivate func initializeUI(){
        
        serviceNameLabel.text = GlobalCommunicator.shared.selectedService.uuid.getServiceName()
        characteristicNameLabel.text = GlobalCommunicator.shared.selectedCharacteristic?.uuid.getCharacteristicName()
        descriptorButton.isHidden = true
    }
    
    
    fileprivate func configureView(){
        
        gattDetailsTableView.tableFooterView = UIView()
        gattDetailsTableViewHeightConstraint.constant = topView.frame.height * 0.6
        self.view.layoutIfNeeded()
        
        gattDetailsTableView.reloadData()
    }
    
    
    fileprivate func initializePropertyButtonFrames(){
        
        readViewWidthConstraint.constant = 0.0
        writeViewWidthConstraint.constant = 0.0
        notifyViewWidthConstraint.constant = 0.0
        
        readViewSeperatorWidthConstraint.constant = 0.0
        writeViewSeperatorWidthConstraint.constant = 0.0
        notifyViewSeperatorWidthConstraint.constant = 0.0
        
        self.view.layoutIfNeeded()
    }
    
    
    
    fileprivate func configurePropertyButtons(){
        
        let characteristciProperties = GlobalCommunicator.shared.selectedCharacteristic?.propertyStrings()
        
        if characteristciProperties != nil && characteristciProperties?.count != 0 {
            initializePropertyButtonFrames()
            
            let buttonConstraintValue = (self.view.frame.width - (CGFloat(characteristciProperties!.count - 1)*SEPERATOR_WIDTH)) / CGFloat(characteristciProperties!.count)
            for (index, propery) in characteristciProperties!.enumerated() {
                
                switch propery {
                case BLECharacteristicProperties.PropertyRead.rawValue:
                    readViewWidthConstraint.constant = buttonConstraintValue
                    if index != characteristciProperties!.count-1 {
                        readViewSeperatorWidthConstraint.constant = SEPERATOR_WIDTH
                    }
                case BLECharacteristicProperties.PropertyWrite.rawValue:
                    writeViewWidthConstraint.constant = buttonConstraintValue
                    if index != characteristciProperties!.count-1 {
                        writeViewSeperatorWidthConstraint.constant = SEPERATOR_WIDTH
                    }
                case BLECharacteristicProperties.Propertynotify.rawValue:
                    notifyViewWidthConstraint.constant = buttonConstraintValue
                    if index != characteristciProperties!.count-1 {
                        notifyViewSeperatorWidthConstraint.constant = SEPERATOR_WIDTH
                    }
                    
                default:
                    break
                }
            }
            self.view.layoutIfNeeded()
        }
        else{
            bottomView.isHidden = true
        }
    }
    
    
    
    fileprivate func updateUIFieldsWithResponse(_ response : GattResponse){
        
        if response.asciiValue != nil{
            
            let asciiValueCell = gattDetailsTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! DetailsTableViewCell
            asciiValueCell.detailsTextField.text = response.asciiValue!
        }
        
        if response.hexValue != nil {
            
            let hexValueCell = gattDetailsTableView.cellForRow(at: IndexPath(row: 1, section: 0)) as! DetailsTableViewCell
            hexValueCell.detailsTextField.text = response.hexValue!
        }
        
        if response.date != nil {
            let dateCell = gattDetailsTableView.cellForRow(at: IndexPath(row: 2, section: 0)) as! DetailsTableViewCell
            dateCell.detailsLabel.text = response.date
        }
        
        if response.time != nil {
            let timeCell = gattDetailsTableView.cellForRow(at: IndexPath(row: 3, section: 0)) as! DetailsTableViewCell
            timeCell.detailsLabel.text = response.time
        }
        
        gattDetailsTableView.beginUpdates()
        gattDetailsTableView.endUpdates()
    }
    
    
    
    fileprivate func checkAndSetNotifyButtons(){
        
        if GlobalCommunicator.shared.selectedCharacteristic?.hasNotifyProperty() == true {
            
            if GlobalCommunicator.shared.selectedCharacteristic?.isNotifying == true {
                notifyButton.isSelected = true
            }
            else{
                notifyButton.isSelected = false
            }
        }
        
        
        if GlobalCommunicator.shared.selectedCharacteristic?.hasIndicateProperty() == true {
          
            if GlobalCommunicator.shared.selectedCharacteristic?.isNotifying == true {
                indicateButton.isSelected = true
            }
            else{
                indicateButton.isSelected = false
            }
        }
    }
    
    
    fileprivate func handleCharacteristicNotifications(){
        
        var alertMessage = "The characteristic "
        
        if GlobalCommunicator.shared.selectedCharacteristic?.isNotifying == true{
            
            if GlobalCommunicator.shared.selectedCharacteristic?.hasNotifyProperty() == true {
                alertMessage += "notifications are disabled."
            }
            else{
                alertMessage += "indications are disabled."
            }

            gattInterface?.stopNotificationsForcharacteristic(GlobalCommunicator.shared.selectedCharacteristic)
            
            let disableAlert = UIAlertController(title: "ALERT", message: alertMessage, preferredStyle: .alert)
            let alertAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            disableAlert.addAction(alertAction)
            self.navigationController?.topViewController?.present(disableAlert, animated: true, completion: nil)
        }
    }
    
    
    // Show alertview for entering hex value
    
    fileprivate func showEnterHexAlert(){
        
        let hexAlert = UIAlertController(title: "Enter Hex Value", message: "", preferredStyle: .alert)
        let doneAction = UIAlertAction(title: "Done", style: .default) { (action) in
            self.isHexAlertShown = false
            
            let hexTextField = hexAlert.textFields![0]
            
            if hexTextField.text != nil && hexTextField.text!.removeInvalidHexCharacters() != ""{
                self.gattInterface?.writeValueForCharacteristic(GlobalCommunicator.shared.selectedCharacteristic, withValue: hexTextField.text!.removeInvalidHexCharacters())
            }
            else{
                self.showInvalidValueAlert(alertMessage: "Please enter a valid hex value.")
            }
        }
        
        hexAlert.addTextField { (textfield) in
            
            let keyboardFrame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 305)

            let hexKeyboard = CustomHexKeyboard(frame: keyboardFrame, textField: textfield)
            textfield.inputView = hexKeyboard
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            self.isHexAlertShown = false
        })
        
        
        hexAlert.addAction(doneAction)
        hexAlert.addAction(cancelAction)
        
        
        self.present(hexAlert, animated: true) {
            self.isHexAlertShown = true
        }
        
    }
    
    
    
    // Show alertview for entering hex value
    
    fileprivate func showEnterASCIIAlert(){
        
        let asciiAlert = UIAlertController(title: "Enter ASCII Value", message: "", preferredStyle: .alert)
        let doneAction = UIAlertAction(title: "Done", style: .default) { (action) in
            
            let asciiTextField = asciiAlert.textFields![0]
            
            if asciiTextField.text != nil && asciiTextField.text != ""{
               
                self.gattInterface?.writeValueForCharacteristic(GlobalCommunicator.shared.selectedCharacteristic, withValue: asciiTextField.text!.getHexStringFromAscii())
                
            }
            else{
                self.showInvalidValueAlert(alertMessage: "Please enter a valid ASCII value.")
            }
        }
        
        asciiAlert.addTextField { (textfield) in
            textfield.tag = 10
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
        })
        
        asciiAlert.addAction(doneAction)
        asciiAlert.addAction(cancelAction)
        
        self.present(asciiAlert, animated: true) {
        }
    }
    
    
    fileprivate func showInvalidValueAlert(alertMessage message : String){
        
        let errorAlert = UIAlertController(title: "ALERT", message: message, preferredStyle: .alert)
        let doneAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        errorAlert.addAction(doneAction)
        self.present(errorAlert, animated: true, completion: nil)
    }
}


extension GattDBServiceDetailsVC : GattDBInterfaceDelegate{
    
    
    func sourceDeviceDidResondForReadRequestForCharacteristic(_ characteristic: CBCharacteristic, withRespsonse gattReadResponse: GattResponse) {
    
        if characteristic.isEqual(GlobalCommunicator.shared.selectedCharacteristic) {
         
            if gattReadResponse.error == nil {
                updateUIFieldsWithResponse(gattReadResponse)
            }
            else{
                
                let readErrorAlert = UIAlertController(title: "ALERT", message: "Failed to read Value. Error : \(gattReadResponse.error.localizedDescription) \n Please try again.", preferredStyle: .alert)
                let doneAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                readErrorAlert.addAction(doneAction)
                self.present(readErrorAlert, animated: true, completion: nil)
            }
        }
        
    }
    
    func sourceDeviceDidResondForWriteRequestForcharacteristic(_ characteristic: CBCharacteristic, withRespsonse gattWriteResponse: GattResponse){
       
        if characteristic.isEqual(GlobalCommunicator.shared.selectedCharacteristic) {
            
            if gattWriteResponse.error == nil{
                updateUIFieldsWithResponse(gattWriteResponse)
            }
            else{
                let writeErrorAlert = UIAlertController(title: "ALERT", message: "Failed to write value. Error : \(gattWriteResponse.error.localizedDescription)\n Please try again.", preferredStyle: .alert)
                let doneAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                writeErrorAlert.addAction(doneAction)
                self.present(writeErrorAlert, animated: true, completion: nil)
            }
        }
    }
    
}


extension GattDBServiceDetailsVC : UITextFieldDelegate{
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField.tag <= 1 {
            
            // Show alert with textfield when the user click on the textfield
            if  case textField.tag = 0 {
                self.showEnterASCIIAlert()
            }
            else{
                showEnterHexAlert()
            }
            // Disable editing in hex and ASCII textfields
            return false
        }
        return true
    }
    
}


extension GattDBServiceDetailsVC : GattDBInterfaceDescriptorDelegate{
    
    func sourceDeviceDidDiscoverDescriptorsForCharacteristic(_ characteristic: CBCharacteristic, withError error: NSError?){
        
        if error == nil {
            
            if GlobalCommunicator.shared.selectedCharacteristic?.descriptors?.count != 0 {
                descriptorButton.isHidden = false
            }
        }
    }
    
    
    func sourceDeviceDidRespondForReadRequestForDescriptor(_ descriptor: CBDescriptor, withResponse readresponse : GattDescriptorResponse){
        
    }
    func sourceDeviceDidRespondForWriteRequestForDescriptor(_ descriptor: CBDescriptor, withResponse wrtieResponse : GattDescriptorResponse){
        
    }
    
}
