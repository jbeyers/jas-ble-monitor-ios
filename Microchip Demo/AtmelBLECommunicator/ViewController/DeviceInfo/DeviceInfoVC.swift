

/* ***************************************************************************

DeviceInfoVC.swift



Copyright (c) 2015, Atmel Corporation. All rights reserved.
Released under NDA
Licensed under Atmel's Limited License Agreement.


THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Atmel Corporation: http://www.atmel.com

******************************************************************************/


import UIKit
import CoreBluetooth

class DeviceInfoVC: GATTBaseViewController{

    @IBOutlet weak var deviceInfoTableView: UITableView!
    var deviceInfoDataArray:NSArray!=NSArray()
    var refreshControl:UIRefreshControl!

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.layoutFix()
        
            }

    override func viewDidLoad() {
        super.viewDidLoad()
        initRefreshView()
        addListner()
        
        initDeviceInfoInterface()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeListner()
    }
    
    
    var deviceInfoService : DeviceInfoService!
    
    func initDeviceInfoInterface()
    {
        self.navigationItem.title = GlobalCommunicator.shared.selectedService?.uuid.getServiceName()
        deviceInfoService = DeviceInfoService(deviceInfoService: GlobalCommunicator.shared.selectedService!, andSourceDevice: GlobalCommunicator.shared.sourceDevice!, andDelegate: self)
        deviceInfoService.readDeviceInfo()
    }
    
        

   }
extension DeviceInfoVC:UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return deviceInfoDataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let deviceInfoDataDictionary: NSDictionary = deviceInfoDataArray.object(at: indexPath.row) as! NSDictionary
        let infoDetailName: String = deviceInfoDataDictionary.allKeys.first as! String
        
        if infoDetailName == "System ID" {
            
            let devicellIdentifier = "infoCell"
            let cell:UITableViewCell = (self.deviceInfoTableView.dequeueReusableCell(withIdentifier: devicellIdentifier) )!
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            
            if let infoDetailData: AnyObject  = deviceInfoDataDictionary.value(forKey: infoDetailName) as AnyObject
            {
                let systemID: SystemID  = infoDetailData as! SystemID
                
//                var manufacturerID : String = NSString(format: "%d",systemID.manufacturerIdentifier! ) as String
//                var uniqueID : String = NSString(format: "%d",systemID.organizationallyUniqueIdentifier! ) as String
                
                let manufacturerID : String = systemID.manufacturerIdentifierData!.hex0xFormat()
                let uniqueID : String = systemID.organizationallyUniqueIdentifierData!.hex0xFormat()
                
                cell.textLabel!.text  = "\(infoDetailName)"
                cell.detailTextLabel?.numberOfLines = 4
                
                let main_string = "Manufacturer Identifier\n\(manufacturerID)\nOrganizationally Unique Identifier\n\(uniqueID)"
                
                let range1 = (main_string as NSString).range(of: "Manufacturer Identifier")
                let range2 = (main_string as NSString).range(of: manufacturerID)
                let range3 = (main_string as NSString).range(of: "Organizationally Unique Identifier")
                let range4 = (main_string as NSString).range(of: uniqueID)
                
                
                let attributedString = NSMutableAttributedString(string:main_string)
                attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.AtmelDarkGrayColor() , range: range1)
                attributedString.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: 14) , range: range1)
                
                attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.AtmelDarkGrayColor() , range: range2)
                
                attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.AtmelDarkGrayColor() , range: range3)
                attributedString.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: 14) , range: range3)
                
                attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.AtmelDarkGrayColor() , range: range4)
                
                cell.detailTextLabel!.attributedText = attributedString
            }
            else
            {
                cell.textLabel!.text = "   \(infoDetailName) \n   Not available"
            }
            return cell
        }
        else if infoDetailName == "Pnp ID"{
            
            let devicellIdentifier = "infoCell"
            let cell:UITableViewCell = (self.deviceInfoTableView.dequeueReusableCell(withIdentifier: devicellIdentifier) )!
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            
            if let infoDetailData: AnyObject  = deviceInfoDataDictionary.value(forKey: infoDetailName) as AnyObject
            {
                let pnpID: PnPID  = infoDetailData as! PnPID
                
//                var vendorIDSource : String = NSString(format: "%d",pnpID.vendorIDSource! ) as String
//                var vendorID : String = NSString(format: "%d",pnpID.vendorID! ) as String
//                var productID : String = NSString(format: "%d",pnpID.productID! ) as String
//                var productVersion : String = NSString(format: "%d",pnpID.productVersion! ) as String
                
                let vendorIDSource : String = pnpID.vendorIDSourceData!.hex0xFormat()
                let vendorID : String = pnpID.vendorIDData!.hex0xFormat()
                let productID : String = pnpID.productIDData!.hex0xFormat()
                let productVersion : String = pnpID.productVersionData!.hex0xFormat()
                
                let main_string:String = "Vendor ID Source\n\(vendorIDSource)\nVendor ID\n\(vendorID)\nProduct ID\n\(productID)\nProduct Version\n\(productVersion)"
                
                let range1 = (main_string as NSString).range(of: "Vendor ID Source")
                let range2 = (main_string as NSString).range(of: vendorIDSource)
                let range3 = (main_string as NSString).range(of: "Vendor ID", options: [], range: NSMakeRange(range1.length, main_string.characters.count - range1.length))
                let range4 = (main_string as NSString).range(of: vendorID)
                let range5 = (main_string as NSString).range(of: "Product ID")
                let range6 = (main_string as NSString).range(of: productID)
                let range7 = (main_string as NSString).range(of: "Product Version")
                let range8 = (main_string as NSString).range(of: productVersion)
                
                let attributedString = NSMutableAttributedString(string:main_string)
                
                attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.AtmelDarkGrayColor() , range: range1)
                attributedString.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: 14) , range: range1)
                
                attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.AtmelDarkGrayColor() , range: range2)
                
                attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.AtmelDarkGrayColor() , range: range3)
                attributedString.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: 14) , range: range3)
                
                attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.AtmelDarkGrayColor() , range: range4)
                
                attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.AtmelDarkGrayColor() , range: range5)
                attributedString.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: 14) , range: range5)
                
                attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.AtmelDarkGrayColor() , range: range6)
                
                attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.AtmelDarkGrayColor() , range: range7)
                attributedString.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: 14) , range: range7)
                
                attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.AtmelDarkGrayColor() , range: range8)
                
                cell.detailTextLabel!.attributedText = attributedString
                
                cell.textLabel!.text  = "\(infoDetailName)"
                cell.detailTextLabel?.numberOfLines = 8
                cell.detailTextLabel?.attributedText = attributedString
            }
            else
            {
                cell.textLabel!.text = "   \(infoDetailName) \n   Not available"
            }
            return cell
        }
        else if infoDetailName == "Regulatory Certificate Data"{
            
            let devicellIdentifier = "infoCell"
            
            let cell:UITableViewCell = (self.deviceInfoTableView.dequeueReusableCell(withIdentifier: devicellIdentifier))!
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            
            if let infoDetailData: Data  = deviceInfoDataDictionary.value(forKey: infoDetailName) as? Data
            {
                cell.textLabel!.text  = "\(infoDetailName)"
                cell.detailTextLabel?.text = infoDetailData.hex0xFormat()
            }
            else
            {
                cell.textLabel!.text = "   \(infoDetailName) \n   Not available"
            }
            return cell
            
        }
            
        else {
            let devicellIdentifier = "infoCell"
            
            let cell:UITableViewCell = (self.deviceInfoTableView.dequeueReusableCell(withIdentifier: devicellIdentifier))!
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            
            if let infoDetailData: AnyObject  = deviceInfoDataDictionary.value(forKey: infoDetailName) as AnyObject
            {
                cell.textLabel!.text  = "\(infoDetailName)"
                cell.detailTextLabel?.text = "\(infoDetailData)"
            }
            else
            {
                cell.textLabel!.text = "   \(infoDetailName) \n   Not available"
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat
    {
        if deviceInfoDataArray.count == 0{
            
            return 52
        }
        else {
            
            let deviceInfoDataDictionary: NSDictionary = deviceInfoDataArray.object(at: indexPath.row) as! NSDictionary

            let infoDetailName: String = deviceInfoDataDictionary.allKeys.first as! String
            
            if infoDetailName == "System ID" {
                return 90
            }
            else if infoDetailName == "Pnp ID" {
                return 140
            }
            else {
                return 52
            }
        }
        
    }
    
     func tableView(_ _tableView: UITableView, willDisplayCell cell: UITableViewCell,forRowAtIndexPath indexPath: IndexPath) {
            
            if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
                cell.separatorInset = UIEdgeInsets.zero
            }
            if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
                cell.layoutMargins = UIEdgeInsets.zero
            }
            if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)) {
                cell.preservesSuperviewLayoutMargins = false
            }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerView = UIImageView(image: UIImage(named: "DIS"))
        headerView.backgroundColor = UIColor.AtmelRedColor()
        headerView.contentMode = UIViewContentMode.center
        return headerView;
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 140
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        let tempFooterView = UIView(frame: CGRect(x: 00, y: 0, width: 10, height: 10))
        return tempFooterView
    }
}

extension DeviceInfoVC:DeviceInfoServiceDelegate {
    
    func deviceInfoServiceDidReadAllData(_ deviceInfoService:DeviceInfoService){
        
        if self.refreshControl.isRefreshing
        {
            self.refreshControl.endRefreshing()
        }
        self.deviceInfoDataArray = deviceInfoService.deviceInfo!.getValuesAsArray()
        deviceInfoTableView.reloadData()
        
    }
}

extension DeviceInfoVC
{
    
     func initRefreshView()
    {
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to Refresh")
        self.refreshControl.addTarget(self, action: #selector(DeviceInfoVC.refresh(_:)), for: UIControlEvents.valueChanged)
        self.deviceInfoTableView.addSubview(refreshControl)
    }
    
    func refresh(_ sender:AnyObject)
    {
        
        if let _ = deviceInfoService
        {
            deviceInfoService.readDeviceInfo()
        }
        // Code to refresh table view
    }
}

extension DeviceInfoVC
{
    func addListner()
    {
        NotificationCenter.listenNotification(self, notificationName: Constants.NotificationName.onServiceDiscovered)
    }
    
    func removeListner()
    {
        NotificationCenter.removelistener(self, notificationName: Constants.NotificationName.onServiceDiscovered)
    }
    

    func onServiceDiscovered()
    {
        initDeviceInfoInterface()
    }
    

}

