/***************************************************************************
 
 Queue.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import UIKit

class Queue:NSObject {
    
    fileprivate var front:Node?
    fileprivate var back:Node?
    fileprivate var maxSize:Int = 0
    fileprivate var count:Int = 0
    
    init(withMaxSize:Int){
        super.init()
        
        maxSize = withMaxSize
    }
    
    
    
    func enqueue(_ value:Int){
        let node = Node(nodeValue: value)
        
        if front == nil{
            front = node
            back = front
        }
        else{
            if maxSize > 0 && count == maxSize
            {dequeue()}
            back!.next = node
            back = back!.next
            
        }
        count += 1
        
    }
    func dequeue(){
        if front == nil
        {return}
        else
        {
            front = front!.next
            count -= 1
        }
    }
    
    func printQ(){
        
        if front == nil
        {return }
        var node = front
        print("\n\(node!.value)")
        node = node!.next!
        
        while (node != nil) {
            print("\n,\(node!.value)")
            node = node!.next
            
        }
        
    }
    func average()->Float{
        if front == nil
        {return Float(0)}
        var node = front
        var sum = node!.value
        node = node!.next
        
        while (node != nil) {
            sum += node!.value
            node = node!.next
            
        }
        return (Float(sum)/Float(count))
    }
fileprivate class Node:NSObject{
        var value:Int = 0
        var next:Node?
        
        init(nodeValue:Int){
            super.init()
            value = Int(nodeValue)
            next = nil
        }
    }
    
}
