/***************************************************************************
 
 CustomHexKeyboard.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import UIKit

class CustomHexKeyboard: UIView {

    
    weak var hexTextField : UITextField?
    let hexKeys = ["1", "2", "3", "A", "4", "5", "6", "B", "7", "8", "9", "C", "0", "F", "E", "D", "0x", "Back", "Done"]
    
    let rowCount : CGFloat = 5.0
    let columnCount : CGFloat = 4.0
    
    var hexDigitCollectionView : UICollectionView?
    
    init(frame : CGRect, textField : UITextField){
        super.init(frame:frame)
        hexTextField = textField
        self.backgroundColor = UIColor.white
        configureKeyboardView()
        addObserver()
        hexTextField?.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureKeyboardView(){
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 5
        layout.minimumLineSpacing = 10
    
        hexDigitCollectionView = UICollectionView(frame: self.bounds, collectionViewLayout: layout)
        hexDigitCollectionView!.delegate = self
        hexDigitCollectionView!.dataSource = self
        hexDigitCollectionView!.backgroundColor = UIColor.clear
        hexDigitCollectionView!.isScrollEnabled = false
        hexDigitCollectionView!.showsVerticalScrollIndicator = false
        
        hexDigitCollectionView!.register(UICollectionViewCell.self,forCellWithReuseIdentifier: "cell")
        self.addSubview(hexDigitCollectionView!)
    }
}




extension CustomHexKeyboard : UICollectionViewDataSource{
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return hexKeys.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        cell.backgroundColor = UIColor.lightGray
        
        cell.subviews.forEach(){$0.removeFromSuperview()}
        
        let digitLabel = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: cell.frame.size.width,height: cell.frame.size.height))
        digitLabel.text = hexKeys[indexPath.row]
        digitLabel.textColor = UIColor.black
        digitLabel.backgroundColor = UIColor.clear
        digitLabel.clipsToBounds = true
        digitLabel.textAlignment = .center
        
        // Set Label font
        
        cell.addSubview(digitLabel)
        return cell
    }
    
    
}



extension CustomHexKeyboard : UICollectionViewDelegateFlowLayout{
 
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        // Handling for back button
        if indexPath.row == 17 {
            return CGSize( width: 20 + 2 * ((self.frame.width - 40)/columnCount), height: (self.frame.height - 50)/rowCount)
        }
        
        return CGSize(width: (self.frame.width - 40)/columnCount, height: (self.frame.height - 50)/rowCount)
    }

        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }

}


extension CustomHexKeyboard : UICollectionViewDelegate{
 
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        UIView.animate(withDuration: 0.05, animations: {
            
            let cell = collectionView.cellForItem(at: indexPath)
            cell?.backgroundColor = UIColor.darkGray
            
            }, completion: { (completion) in
                
                let cell = collectionView.cellForItem(at: indexPath)
                cell?.backgroundColor = UIColor.lightGray
        }) 
        
        
        let selectedKey = hexKeys[indexPath.row]
        
        if selectedKey == "Done" {
            removeKeyboard()
        }
        else if selectedKey == "Back"{
            removeText()
        }
        else{
            manageTextFieldWithText(selectedKey)
        }
    }

}

extension CustomHexKeyboard : UITextFieldDelegate   {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        removeObserver()
    }
    
}


extension CustomHexKeyboard {
    
    
    fileprivate func manageTextFieldWithText(_ text : String){
        
        if hexTextField != nil {
            
            var hexTextFieldText = hexTextField!.text!
            
            if text == "0x" {
                
                if  hexTextFieldText.characters.count == 0 {
                    hexTextFieldText += text
                }
                else if (hexTextFieldText.characters.count % 5 == 4){
                    hexTextFieldText += " \(text)"
                }
            }
            else{
                
                if hexTextFieldText.characters.count == 0 {
                    hexTextFieldText += "0x\(text)"
                }
                else if(hexTextFieldText.characters.count % 5 == 4){
                    hexTextFieldText += " 0x\(text)"
                }
                else{
                    hexTextFieldText += text
                }
            }
            
            hexTextField?.text = hexTextFieldText
        }
    }
    
    
    fileprivate func removeKeyboard(){
        
        if hexTextField != nil {
            hexTextField?.resignFirstResponder()
        }
    }
    
    
    fileprivate func removeText(){
    
        if hexTextField != nil {
            
            var hexTextFieldText = hexTextField!.text!
            
            if hexTextFieldText.characters.count == 0 {
                return
            }
            
            if hexTextFieldText.characters.last == "x" {
                
                hexTextFieldText.remove(at: hexTextFieldText.characters.index(before: hexTextFieldText.characters.endIndex))
                hexTextFieldText.remove(at: hexTextFieldText.characters.index(before: hexTextFieldText.characters.endIndex))

                if hexTextFieldText.characters.last == " " {
                    hexTextFieldText.remove(at: hexTextFieldText.characters.index(before: hexTextFieldText.characters.endIndex))
                }
                
            }
            else{
                hexTextFieldText.remove(at: hexTextFieldText.characters.index(before: hexTextFieldText.characters.endIndex))
            }
            
            hexTextField?.text = hexTextFieldText
        }
    
    }
    
    
    
    fileprivate func addObserver(){
        NotificationCenter.default.addObserver(self, selector: #selector(CustomHexKeyboard.deviceOrientationDidChanged(_:)), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    }
    
    
    fileprivate func removeObserver(){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    }
    
    
    func deviceOrientationDidChanged(_ notification : Notification){
        
        if self.superview != nil {
            self.frame = CGRect(x: self.frame.origin.x, y: self.frame.origin.y, width: self.superview!.frame.size.width, height: self.frame.height)
            hexDigitCollectionView!.frame = self.bounds
            hexDigitCollectionView!.reloadData()
        }
    }
    
}










