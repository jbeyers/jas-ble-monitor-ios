/***************************************************************************
 
 OTASettingsViewController.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import UIKit

let FILE_SELECT_VC_SEGUE = "TO_FILE_SELECT_VC_SEGUE"

class OTASettingsViewController: BaseViewController {
    
    @IBOutlet weak var productIDLabel: UILabel!
    @IBOutlet weak var vendorIDLabel: UILabel!
    @IBOutlet weak var hardwareRevisionLabel: UILabel!
    @IBOutlet weak var hardwareVersionLabel: UILabel!
    @IBOutlet weak var firmwareVersionLabel: UILabel!
    
    @IBOutlet weak var productIDHeaderLabel: UILabel!
    @IBOutlet weak var vendorIDHeaderLabel: UILabel!
    @IBOutlet weak var hardwareRevisionHeaderLabel: UILabel!
    @IBOutlet weak var hardwareVersionHeaderLabel: UILabel!
    @IBOutlet weak var firmwareVersionHeaderLabel: UILabel!
    
    @IBOutlet weak var fileFirmwareVersionHeaderLabel: UILabel!
    @IBOutlet weak var fileSizeHeaderLabel: UILabel!

    @IBOutlet weak var fileFirmwareVersionLabel: UILabel!
    @IBOutlet weak var fileSizeLabel: UILabel!

    @IBOutlet weak var fileInfoView: UIView!
    @IBOutlet weak var selectedFileTextField: UITextField!
    
    @IBOutlet weak var currentDeviceInfoHeaderLabel: UILabel!
    
    @IBOutlet weak var startButton: UIButton!
    var otaInterface:AtmelOTAInterface?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUIElements()

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        otaInterface = GlobalCommunicator.shared.otaInterface
        updateViewWithData()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
  
    func setupUIElements(){
        
        self.navigationController!.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "Roboto-Medium", size: CGFloat.getFontSizeToScreen(16.0,scale: 0.6))!,NSForegroundColorAttributeName:UIColor.white]
        self.navigationItem.title = "Select Firmware Image"
        
        
        if GlobalCommunicator.shared.otaInterface!.currentDeviceInformation == nil {
            
            let alertVC = UIAlertController(title: "ALERT", message: "The image file is invalid. Reason : Device info request acknowledgement not received.", preferredStyle: .alert)
            let alertAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertVC.addAction(alertAction)
            self.present(alertVC, animated: true, completion: nil)
        }

       
        productIDLabel.font = UIFont(name: "Roboto-Medium", size:CGFloat.getFontSizeToScreen(12.5,scale: 0.6))
        vendorIDLabel.font = UIFont(name: "Roboto-Medium", size:CGFloat.getFontSizeToScreen(12.5,scale: 0.6))
        hardwareRevisionLabel.font = UIFont(name: "Roboto-Medium", size:CGFloat.getFontSizeToScreen(12.5,scale: 0.6))
        hardwareVersionLabel.font = UIFont(name: "Roboto-Medium", size:CGFloat.getFontSizeToScreen(12.5,scale: 0.6))
        firmwareVersionLabel.font = UIFont(name: "Roboto-Medium", size:CGFloat.getFontSizeToScreen(12.5,scale: 0.6))
        
        productIDHeaderLabel.font = UIFont(name: "Roboto-Medium", size:CGFloat.getFontSizeToScreen(12.5,scale: 0.6))
        vendorIDHeaderLabel.font = UIFont(name: "Roboto-Medium", size:CGFloat.getFontSizeToScreen(12.5,scale: 0.6))
        hardwareRevisionHeaderLabel.font = UIFont(name: "Roboto-Medium", size:CGFloat.getFontSizeToScreen(12.5,scale: 0.6))
        hardwareVersionHeaderLabel.font = UIFont(name: "Roboto-Medium", size:CGFloat.getFontSizeToScreen(12.5,scale: 0.6))
        firmwareVersionHeaderLabel.font = UIFont(name: "Roboto-Medium", size:CGFloat.getFontSizeToScreen(12.5,scale: 0.6))
        
        
        fileFirmwareVersionLabel.font = UIFont(name: "Roboto-Medium", size:CGFloat.getFontSizeToScreen(12.5,scale:0.6))
        fileSizeLabel.font = UIFont(name: "Roboto-Medium", size:CGFloat.getFontSizeToScreen(12.5,scale:0.6))
        
        fileFirmwareVersionHeaderLabel.font = UIFont(name: "Roboto-Medium", size:CGFloat.getFontSizeToScreen(12.5,scale:0.6))
        fileSizeHeaderLabel.font = UIFont(name: "Roboto-Medium", size:CGFloat.getFontSizeToScreen(12.5,scale:0.6))
        selectedFileTextField.font = UIFont(name: "Roboto-Medium", size:CGFloat.getFontSizeToScreen(12.5,scale:0.6))
        
        
        currentDeviceInfoHeaderLabel.font = UIFont(name: "Roboto-Medium", size:CGFloat.getFontSizeToScreen(12.5,scale:0.6))

        
        productIDLabel.textColor = UIColor.black
        vendorIDLabel.textColor = UIColor.black
        hardwareRevisionLabel.textColor = UIColor.black
        hardwareVersionLabel.textColor = UIColor.black
        firmwareVersionLabel.textColor = UIColor.black
        
        productIDHeaderLabel.textColor = UIColor.black
        vendorIDHeaderLabel.textColor = UIColor.black
        hardwareRevisionHeaderLabel.textColor = UIColor.black
        hardwareVersionHeaderLabel.textColor = UIColor.black
        firmwareVersionHeaderLabel.textColor = UIColor.black
        
        fileFirmwareVersionLabel.textColor = UIColor.black
        fileSizeLabel.textColor = UIColor.black
        fileFirmwareVersionHeaderLabel.textColor = UIColor.black
        fileSizeHeaderLabel.textColor = UIColor.black
        selectedFileTextField.textColor = UIColor.black

        
        currentDeviceInfoHeaderLabel.textColor = UIColor.black

        startButton.titleLabel?.font = UIFont(name: "Roboto-Medium", size:CGFloat.getFontSizeToScreen(14.0,scale:0.6))
       
        
    }
    
    func updateFileDetails(){
        
        if let _ = otaInterface?.image{
            
            startButton.isHidden = false
            fileInfoView.isHidden = false
            otaInterface?.getOTAUpdateType()
            
            switch(otaInterface!.updateType){
                
            case OTAUpdateType.downgrade:
                startButton.setTitle("Force Downgrade", for: UIControlState())
                startButton.titleLabel?.textColor = UIColor.white

                startButton.backgroundColor = UIColor.colorWithHexString("b70707")

                break
            case OTAUpdateType.forcedUpdate:
                startButton.setTitle("Force Upgrade", for: UIControlState())
                startButton.titleLabel?.textColor = UIColor.white

                 startButton.backgroundColor = UIColor.colorWithHexString("6c6b6b")

                break
            case OTAUpdateType.upgrade:
                startButton.setTitle("Start Upgrade", for: UIControlState())
                startButton.titleLabel?.textColor = UIColor.white

                 startButton.backgroundColor = UIColor.AtmelRedColor()

                break
                
            }
        }
        else
        {
            startButton.isHidden = true
            fileInfoView.isHidden = true
        }
    }

    @IBAction func startUpdateButtonAction(_ sender: UIButton) {
        otaInterface?.forcedImageNotification()
        self.performSegue(withIdentifier: OTA_INFO_VC_SEGUE, sender: self)
    }
    @IBAction func selectFileButtonAction(_ sender: UIButton) {
      self.performSegue(withIdentifier: FILE_SELECT_VC_SEGUE, sender: self)
    }
    
    func updateViewWithData(){
        
        updateFileDetails()
        
        if var productID = otaInterface?.currentDeviceInformation?.productID
            //{productIDLabel.text = "\(Data(bytes: UnsafePointer<UInt8>(&productID), count:2).hex0xFormat())"}
            {productIDLabel.text = "\(Data(bytes: &productID, count:2).hex0xFormat())"}
        if var vendorID = otaInterface?.currentDeviceInformation?.vendorID
            //{vendorIDLabel.text = "\(Data(bytes: UnsafePointer<UInt8>(&vendorID), count:2).hex0xFormat())"}
            {vendorIDLabel.text = "\(Data(bytes: &vendorID, count:2).hex0xFormat())"}
        
        if otaInterface!.currentDeviceInformation != nil {
          
            hardwareRevisionLabel.text = "\(otaInterface!.currentDeviceInformation!.hardwareRevision)"
            hardwareVersionLabel.text = "\(otaInterface!.currentDeviceInformation!.hardwareVersion.description)"
            firmwareVersionLabel.text = "\(otaInterface!.currentDeviceInformation!.firmwareVersion.description)"
        }
    
        selectedFileTextField.text = ""

        
        if let image = otaInterface?.image{
            
            fileFirmwareVersionLabel.text = "\(image.firmwareVersion.description)"
            selectedFileTextField.text = image.name!
            fileSizeLabel.text = "\(image.totalImageSize)"
        }

    }

    

}
