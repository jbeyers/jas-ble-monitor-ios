/* ***************************************************************************

//AboutVC.swift
//
Viewcontroller class for about app;
About app contains contact details and additional info about the app
//



Copyright (c) 2015, Atmel Corporation. All rights reserved.
Released under NDA
Licensed under Atmel's Limited License Agreement.


THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Atmel Corporation: http://www.atmel.com

******************************************************************************/

import Foundation
import UIKit


class AboutVC: BaseViewController {
    
    
     @IBOutlet var bundleVersion: UILabel!
    
    override func viewDidLoad() { 
        
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            print(version)
            
            
            guard let buildNumber = Bundle.main.infoDictionary?["CFBundleVersion"] as? String else{
                bundleVersion.text = "Build Version \(version)"
                return
            }
            
            bundleVersion.text = "Build Version \(version)" + "." + buildNumber
        }
    }
    
    
    @IBAction func onDoneTouched(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
    }

}
