/***************************************************************************
 
 GattDBDescriptorListVC.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import UIKit
import CoreBluetooth

class GattDBDescriptorListVC: GATTBaseViewController {
    
    let CELL_IDENTIFIER  = "descriptorCell"
    let SEGUE_IDENTIFIER = "DescriptorDetailsVC"
    
    @IBOutlet weak var descriptorListTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        descriptorListTableView.tableFooterView = UIView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "GATT DB"
    }

}





extension GattDBDescriptorListVC : UITableViewDataSource{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if GlobalCommunicator.shared.selectedCharacteristic?.descriptors != nil &&  GlobalCommunicator.shared.selectedCharacteristic?.descriptors?.count != 0 {
            return GlobalCommunicator.shared.selectedCharacteristic!.descriptors!.count
        }
        
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER) as? DescriptorTableViewCell
        
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: CELL_IDENTIFIER) as? DescriptorTableViewCell
        }
        
        let descriptor =  GlobalCommunicator.shared.selectedCharacteristic?.descriptors![indexPath.row]
        
        cell?.descriptorUUIDLabel.text = descriptor?.uuid.uuidString
        cell?.descriptorNameLabel.text = descriptor?.uuid.getDescriptorName()
        return cell!
    }
    
}


extension GattDBDescriptorListVC :  UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        GlobalCommunicator.shared.selectedDescriptor = GlobalCommunicator.shared.selectedCharacteristic?.descriptors![indexPath.row]
        self.performSegue(withIdentifier: SEGUE_IDENTIFIER, sender: self)
        tableView.deselectRow(at: indexPath, animated: false)
    }
}




