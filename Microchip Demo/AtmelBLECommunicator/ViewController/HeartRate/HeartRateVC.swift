
/* ***************************************************************************

HeartRateVC.swift



Copyright (c) 2015, Atmel Corporation. All rights reserved.
Released under NDA
Licensed under Atmel's Limited License Agreement.


THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Atmel Corporation: http://www.atmel.com

******************************************************************************/


import UIKit
import CoreBluetooth

class HeartRateVC: GATTBaseViewController  {
    
    
    @IBOutlet weak var sensorLocationLabel: UILabel!
    @IBOutlet weak var energyExpended: UILabel!
    @IBOutlet weak var heartRateValueLabel: UILabel!
    @IBOutlet weak var stopNotifyLabel: UILabel!
    @IBOutlet weak var notifySwitch: UISwitch!
    
    var heartRateInterface : BLEHeartRateMonitorInterface!
    

    ///WORKAROUND FOR IOS 9.0 iPad Mini issue
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.layoutFix()
        
            }

    override func viewDidLoad() {
        super.viewDidLoad()
        addListner()
        
        initHeartRateView()
        
        if UIDevice.is_iPad()
        {
            
        }
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        removeListner()
        checkAndHandleNotification()
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
      
        

    }
    
    

    
    override var shouldAutorotate : Bool {
        return true
    }
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        if UIDevice.is_iPad(){
            
            return UIInterfaceOrientationMask.all
        }
        else
        {
            return UIInterfaceOrientationMask.portrait

        }
    }
    
//         override func supportedInterfaceOrientations() -> Int {
//            return visibleViewController.supportedInterfaceOrientations()
//        }
//         override func shouldAutorotate() -> Bool {
//            return visibleViewController.shouldAutorotate()
//        }
    
    
    
    
    
    func checkAndHandleNotification()
    {
        if notifySwitch.isOn == true
        {
            self.heartRateInterface.stopHeartRateMeasurmentNotifications()
        }
    }
    
    
    func initHeartRateInterface()
    {
        self.navigationItem.title = GlobalCommunicator.shared.selectedService?.uuid.getServiceName()
        self.heartRateInterface = BLEHeartRateMonitorInterface(sourceDevice: GlobalCommunicator.shared.sourceDevice,delgateObject: self)
        self.heartRateInterface.readHeartRateBodySensorLocation()
    }
    
    func initHeartRateView()
    {
        
        initHeartRateInterface()
        //Call after  interface initilization to handle previous state of Notify Switch
        configureNotifyControl()
        
    }
    func configureNotifyControl()
    {
        let state = UserDefaults.getNotificationStatus(Constants.UserDefaultsKey.heartRateStatusKey)
        notifySwitch.isOn = state
        onNotifyChanged(notifySwitch)
    }
    
    
    @IBAction func onResetTouched(_ sender: UIButton)
    {
        if notifySwitch.isOn == true
        {
            self.heartRateInterface.resetEnergyExpended()
        }
    }
    
    @IBAction func onButtuonPress(_ sender: AnyObject) {
        self.view.setNeedsLayout()
        self.view.layoutSubviews()

    }
    
    @IBAction func onNotifyChanged(_ sender: UISwitch)
    {
        
        
        if sender.isOn == true
        {
            stopNotifyLabel.text = Constants.ButtonTitle.STOP_NOTIFY
            self.heartRateInterface.startHeartRateMeasurmentNotifications()
            self.heartRateInterface.readHeartRateBodySensorLocation()
            
        }
        else
        {
            stopNotifyLabel.text = Constants.ButtonTitle.START_NOTIFY
            self.heartRateInterface.stopHeartRateMeasurmentNotifications()
        }
        
        UserDefaults.updateNotifyState(sender.isOn , UserDefaultsKey: Constants.UserDefaultsKey.heartRateStatusKey)
    }
    
    func updateSensorLocation(_ type:String)
    {
        if notifySwitch.isOn == true
        {
            sensorLocationLabel.text = type
        }
    }

    
    func updateHeartRateValue(_ heartRateData:HeartRateData)
    {
        if let _ = heartRateData.heartRateMeasurementValue
        {
            self.heartRateValueLabel.text = "\(heartRateData.heartRateMeasurementValue!) bpm"
        }
        
        if let _ = heartRateData.enegyExpended
        {
            self.energyExpended.text = "\(heartRateData.enegyExpended!) kj"
        }
        
    }
    

}


/*----------------------------------------------------------------------------------------------------------*/
// MARK: BLEHeartRateMonitorInterfaceDelegate Methods
/*----------------------------------------------------------------------------------------------------------*/


extension HeartRateVC : BLEHeartRateMonitorInterfaceDelegate
{
    func bleHeartRateMonitorInterfaceDidReadMeasurementData(_ heartRateData:HeartRateData?)
    {
        if let _ = heartRateData
        {
            updateHeartRateValue(heartRateData!)
        }
    }
    func bleHeartRateMonitorInterfaceDidReadBodySensorLocation(_ heartSensorLocation:HRMSensorLocation?)
    {
        if let _ = heartSensorLocation
        {
            updateSensorLocation(heartSensorLocation!.rawValue)
        }
    }
    func bleHeartRateMonitorInterfaceUpdateNotificationStateForCharacteristic(_ characteristic: CBCharacteristic!, error: NSError!)
    {
        
    }
}




/*----------------------------------------------------------------------------------------------------------*/
// MARK: NSNotificationCenter Methods
/*----------------------------------------------------------------------------------------------------------*/


extension HeartRateVC
{
    
    func addListner()
    {
        NotificationCenter.listenNotification(self, notificationName: Constants.NotificationName.onDeviceDisconned)
        NotificationCenter.listenNotification(self, notificationName: Constants.NotificationName.onServiceDiscovered)
        NotificationCenter.listenNotification(self, notificationName: Constants.NotificationName.onEnterForeground)
        NotificationCenter.listenNotification(self, notificationName: Constants.NotificationName.onEnterBackground)
    }
    
    func removeListner()
    {
        NotificationCenter.removelistener(self, notificationName: Constants.NotificationName.onDeviceDisconned)
        NotificationCenter.removelistener(self, notificationName: Constants.NotificationName.onServiceDiscovered)
        NotificationCenter.removelistener(self, notificationName: Constants.NotificationName.onEnterForeground)
        NotificationCenter.removelistener(self, notificationName: Constants.NotificationName.onEnterBackground)
    }
    
    func onEnterBackground()
    {
        checkAndHandleNotification()
    }
    
    func onEnterForeground()
    {
        configureNotifyControl()
    }

    
    
    func onDeviceDisconned()
    {
        
    }
    
    func onServiceDiscovered()
    {
        initHeartRateView()
    }
    
    
    
}



