/***************************************************************************
 
 GattDBServiceVC.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import UIKit



class GattDBServiceVC: GATTBaseViewController {
    
    
    @IBOutlet weak var serviceListTableView: UITableView!
    
    let CELL_IDENTIFIER = "serviceCell"
    
    var myPeripheral = GlobalCommunicator.shared.connectedDevice.BLEPeripheral

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        serviceListTableView.estimatedRowHeight = 250.0
        serviceListTableView.rowHeight = UITableViewAutomaticDimension
        serviceListTableView.tableFooterView = UIView()
        
        // Inintialize GATTDB interface
        
        GlobalCommunicator.shared.gattDBInterface = GattDBInterface(sourceDevice: GlobalCommunicator.shared.sourceDevice)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "GATT DB"
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}




extension GattDBServiceVC : UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if myPeripheral?.services?.count != 0 {
            return myPeripheral!.services!.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER) as? ServiceTableViewCell
        
        if  cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: CELL_IDENTIFIER) as? ServiceTableViewCell
        }
        cell?.serviceNameLabel.text = myPeripheral?.services![indexPath.row].uuid.getServiceName()
        cell?.serviceUUIDLabel.text = myPeripheral?.services![indexPath.row].uuid.uuidString
        
        cell?.delegate = self
        return cell!
    }
}


extension GattDBServiceVC : UITableViewDelegate{
 
    
      
}


extension GattDBServiceVC : ServiceTableCellDelegate{
    
    func dropdown(shouldShow status: Bool, forCell cell: ServiceTableViewCell) {
        
        if status == true {
            
            let selectedIndexPath = serviceListTableView.indexPath(for: cell)
            
            cell.selectedService = myPeripheral?.services![selectedIndexPath!.row]
            // Change Cell text color
            cell.serviceNameLabel.textColor = UIColor.black
            cell.serviceUUIDLabel.textColor = UIColor.black
            cell.uuidLabel.textColor = UIColor.black

            
            cell.characteristicListTableView.reloadData()
            serviceListTableView.beginUpdates()
            serviceListTableView.endUpdates()
        }
        else{
            
            
            cell.selectedService = nil
            
            cell.serviceNameLabel.textColor = UIColor.AtmelDarkGrayColor()
            cell.serviceUUIDLabel.textColor = UIColor.AtmelDarkGrayColor()
            cell.uuidLabel.textColor = UIColor.AtmelDarkGrayColor()

            cell.characteristicListTableView.reloadData()
            serviceListTableView.beginUpdates()
            serviceListTableView.endUpdates()

        }
    }
    
 
}










