
/* ***************************************************************************

BloodPressureVC.swift



Copyright (c) 2015, Atmel Corporation. All rights reserved.
Released under NDA
Licensed under Atmel's Limited License Agreement.


THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Atmel Corporation: http://www.atmel.com

******************************************************************************/


import UIKit
import CoreBluetooth

class BloodPressureVC: GATTBaseViewController  {
    
    var  MAX_BP_VALUE : Float = 240.0
    var  MAX_BP_VALUE_KPA : Float = 32.0
//    var diaDisplayTimer : NSTimer?
//    var cuffPressureValueCounter = 0
    
    //iPad Layout
    @IBOutlet weak var SYSViewContainer : UIView!
    @IBOutlet weak var DIAViewContainer : UIView!
    @IBOutlet weak var DIAViewTwoBottomSpace: NSLayoutConstraint!
    @IBOutlet weak var DIAViewTwoTrailing: NSLayoutConstraint!
    @IBOutlet weak var SYSViewOneTopSpace: NSLayoutConstraint!
    @IBOutlet weak var SYSViewOneLeading: NSLayoutConstraint!
    
    @IBOutlet weak var SYSView : ProgressCircleView!
    @IBOutlet weak var DIAView : ProgressCircleView!
    @IBOutlet weak var SYSViewMarkerView : MarkerView!
     @IBOutlet weak var DIAViewMarkerView : MarkerView!
    
    @IBOutlet weak var sysValue: UILabel!
    @IBOutlet weak var sysValueTitle: UILabel!
    @IBOutlet weak var diaValue: UILabel!
    @IBOutlet weak var pulseValue: UILabel!
    @IBOutlet weak var pulseValueYConstraint: NSLayoutConstraint!

    
    @IBOutlet weak var infoButton: UIButton!
    
    @IBOutlet weak var stopNotifyLabel: UILabel!
    @IBOutlet weak var notifySwitch: UISwitch!
    var bpInterface : BLEBloodPressureMonitorInterface!
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.layoutFix()
        
            }

    override func viewDidLoad() {
        super.viewDidLoad()
        addListner()
        initBpView()
        
        if UIDevice.is_iPad()
        {
            NotificationCenter.default.addObserver(self, selector: #selector(BloodPressureVC.rotated), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
            SYSViewContainer.isHidden = true
            DIAViewContainer.isHidden = true
        }
        // Do any additional setup after loading the view.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
       
            
//            Util.delay(0.4)
//                {
//            self.rotated()
//            self.SYSViewContainer.hidden = false
//            self.DIAViewContainer.hidden = false
//            }
       
                self.rotated()
                self.SYSViewContainer.isHidden = false
                self.DIAViewContainer.isHidden = false
      

    }
    func landscapeLayout()
    {
        SYSViewOneTopSpace.constant = 200
        SYSViewOneLeading.constant = 50
        
        DIAViewTwoBottomSpace.constant = 200
        DIAViewTwoTrailing.constant = 50
        self.view.setNeedsLayout()
    }
    
    func portraitLayout()
    {
        SYSViewOneTopSpace.constant = 120
        SYSViewOneLeading.constant = 220
        
        DIAViewTwoBottomSpace.constant = 120
        DIAViewTwoTrailing.constant = 220
        self.view.setNeedsLayout()
    }
    
    func rotated()
    {
        
        if UIDevice.current.orientation == .faceUp || UIDevice.current.orientation == .faceDown || UIDevice.current.orientation == .unknown {
            
            if UIInterfaceOrientationIsLandscape(UIApplication.shared.statusBarOrientation) {
                landscapeLayout()
            }
            
            if UIInterfaceOrientationIsPortrait(UIApplication.shared.statusBarOrientation) {
                portraitLayout()
            }
        }
        else{
            
            if(UIDeviceOrientationIsLandscape(UIDevice.current.orientation))
            {
                print("landscape")
                
                landscapeLayout()
            }
            
            if(UIDeviceOrientationIsPortrait(UIDevice.current.orientation))
            {
                print("Portrait")
                portraitLayout()
            }
        }
        
        
        SYSView.updateUI()
        DIAView.updateUI()
        
    }
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        removeListner()
        checkAndStopNotify()
    }
    
    func checkAndStopNotify()
    {
        if notifySwitch.isOn == true
        {
            stopNotifyBP()
        }
    }
    func initBpInterface()
    {
        self.navigationItem.title = GlobalCommunicator.shared.selectedService?.uuid.getServiceName()
        self.bpInterface = nil
        self.bpInterface = BLEBloodPressureMonitorInterface(sourceDevice: GlobalCommunicator.shared.sourceDevice,delgateObject: self)
        self.bpInterface.readBloodPressureFeatures()

    }
    
    func initBpView()
    {
        infoButton.isEnabled = false
        initBpInterface()
        //Call after  interface initilization to handle previous state of Notify Switch
        configureNotifyControl()
        
    }
    func configureNotifyControl()
    {
        let state = UserDefaults.getNotificationStatus(Constants.UserDefaultsKey.bloodPressureStatusKey)
        notifySwitch.isOn = state
        onNotifyChanged(notifySwitch)
    }
    
    @IBAction func onNotifyChanged(_ sender: UISwitch)
    {

        if sender.isOn == true
        {
            stopNotifyLabel.text = Constants.ButtonTitle.STOP_NOTIFY
            startNotifyBP()
            self.bpInterface.readBloodPressureFeatures()
            
        }
        else
        {
            stopNotifyLabel.text = Constants.ButtonTitle.START_NOTIFY
            stopNotifyBP()
        }
        
        UserDefaults.updateNotifyState(sender.isOn , UserDefaultsKey: Constants.UserDefaultsKey.bloodPressureStatusKey)
    }
    
    func stopNotifyBP()
    {
        self.bpInterface.stopIntermediateCuffPressureNotifications()
        self.bpInterface.stopBloodPressureMeasurmentNotifications()
    }
    
    func startNotifyBP()
    {
        self.bpInterface.startBloodPressureMeasurmentNotifications()
        self.bpInterface.startIntermediateCuffPressureNotifications()
    }
    
    func updateBpUI(_ bpData : BloodPressureData)
    {
        showDIAview()
        
//        println("BP ====\(bpData.systolicBPM) ==== \(bpData.diastolicBPM)")

        SYSView.strokeEndAngle = getProgresEndAngleFromBpValue(bpData.systolicBPM!, bpmUnit: bpData.bpmUnit)
        DIAView.strokeEndAngle = getProgresEndAngleFromBpValue(bpData.diastolicBPM!, bpmUnit: bpData.bpmUnit)
        
        
        
        sysValue.text = getBpValueWithUnit(bpData.systolicBPM!, bpmUnit: bpData.bpmUnit)
        diaValue.text = getBpValueWithUnit(bpData.diastolicBPM!, bpmUnit: bpData.bpmUnit)
        
        if let pulse = bpData.pulseRate
        {
            pulseValue.text = "PULSE  \(pulse) bpm"
        }
        else
        {
            pulseValue.text =  ""
        }
    }
    
    func hideDIAview()
    {
            infoButton.isHidden=true
            self.pulseValueYConstraint.constant = 160
            sysValueTitle.text = "Current Cuff Pressure"
            DIAView.isHidden = true
            DIAViewMarkerView.isHidden = true
        
    }
    func showDIAview()
    {
        infoButton.isHidden = false
        self.pulseValueYConstraint.constant = 0
        sysValueTitle.text = "SYS"
        DIAView.isHidden = false
        DIAViewMarkerView.isHidden = false
    }

    

    
    func updateBpUI(_ bpData : IntermediateCuffPressureData)
    {
        
        hideDIAview()
        
//        println("IMC === \(bpData.systolicICM) ==== \(bpData.diastolicICM)")

        SYSView.strokeEndAngle = getProgresEndAngleFromBpValue(bpData.systolicICM!, bpmUnit: bpData.icmUnit)
        DIAView.strokeEndAngle = getProgresEndAngleFromBpValue(bpData.diastolicICM!, bpmUnit: bpData.icmUnit)
        
        
        
        sysValue.text = getBpValueWithUnit(bpData.systolicICM!, bpmUnit: bpData.icmUnit)
        diaValue.text = getBpValueWithUnit(bpData.diastolicICM!, bpmUnit: bpData.icmUnit)
        
        if let pulse = bpData.pulseRate
        {
            pulseValue.text = "PULSE  \(pulse) bpm"
        }
        else
        {
            pulseValue.text =  ""
        }
        
    }
    
    
    func getProgresEndAngleFromBpValue(_ value : Float,bpmUnit:BPMUnit? ) -> CGFloat
    {
        var scaleVal = MAX_BP_VALUE
        if let unit = bpmUnit
        {
            switch (unit)
            {
            case BPMUnit.kPa:
                scaleVal = MAX_BP_VALUE_KPA
            case BPMUnit.mmHg:
                scaleVal = MAX_BP_VALUE
            }
        }
        
        return CGFloat(value / scaleVal)
        /*if MAX_BP_VALUE > value
        {
            return CGFloat(value / MAX_BP_VALUE)
        }
        else
        {
            MAX_BP_VALUE = value
            return 1.0
        }*/
    }
        
    
    func getBpValueWithUnit(_ value : Float ,bpmUnit:BPMUnit? ) -> String
    {
        if let unit = bpmUnit
        {
            switch (unit)
            {
            case BPMUnit.kPa:
                SYSViewMarkerView.ismmHg = false
                DIAViewMarkerView.ismmHg = false
                return "\(value) kpa"
                

            case BPMUnit.mmHg:
                SYSViewMarkerView.ismmHg = true
                DIAViewMarkerView.ismmHg = true
                return "\(value) mmHg"
                

                
            }
        }
        //default unit
        return "\(value) mmHg"
    }
    
    
   @IBOutlet weak var   verticalHeightContraint: NSLayoutConstraint!
   @IBOutlet weak var   BodyMovementDetectionButton : UIButton!
   @IBOutlet weak var   CuffFitDetectionButton : UIButton!
   @IBOutlet weak var   IrregularPulseDetectionButton : UIButton!
   @IBOutlet weak var   PulseRateDetectionButton : UIButton!
   @IBOutlet weak var   MeasurementPositionDetectionButton : UIButton!
   @IBOutlet weak var   MultipleBondSupportButton : UIButton!
    
    func enableInfoView(_ bpmFeatureStatus:BPMFeatureStatus)
    {
        infoButton.isEnabled=true
        BodyMovementDetectionButton.isSelected = bpmFeatureStatus.isBodyMovementDetection!
        CuffFitDetectionButton.isSelected = bpmFeatureStatus.isCuffFitDetection!
        IrregularPulseDetectionButton.isSelected = bpmFeatureStatus.isIrregularPulseDetection!
        PulseRateDetectionButton.isSelected = bpmFeatureStatus.isPulseRateDetection!
        MeasurementPositionDetectionButton.isSelected = bpmFeatureStatus.isMeasurementPositionDetection!
        MultipleBondSupportButton.isSelected = bpmFeatureStatus.isMultipleBondSupport!
        
    }
    
    @IBAction func onInfoTouched(_ sender : UIButton)
    {
        var cHeight : CGFloat = 0.0
        if verticalHeightContraint.constant < 10
        {
            
             cHeight = self.view.frame.size.height
            if UIDevice.is_iPad()
            {
                cHeight += CGFloat(Constants.iPadFrame.iPadDefaultWidthSize)
            }
        }
        
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            
            self.verticalHeightContraint.constant = cHeight
            self.view.layoutIfNeeded()
            
            if cHeight != 0{
                self.infoButton.isHidden = false
            }
            else{
                self.infoButton.isHidden = true
            }
            
        })
        
    }
}


/*----------------------------------------------------------------------------------------------------------*/
// MARK: BLEBloodPressureMonitorInterfaceDelegate Methods
/*----------------------------------------------------------------------------------------------------------*/




extension BloodPressureVC : BLEBloodPressureMonitorInterfaceDelegate
{
    func bleBloodPressureMonitorInterfaceDidReadBPMData(_ bloodPressureData:BloodPressureData?)
    {
        if let _ = bloodPressureData
        {
            updateBpUI(bloodPressureData!)
        }
        
    }
    func bleBloodPressureMonitorInterfaceDidReadICMData(_ bloodPressureData:IntermediateCuffPressureData?)
    {
        if let _ = bloodPressureData
        {
            updateBpUI(bloodPressureData!)
        }
    }
    func bleBloodPressureMonitorInterfaceDidReadBPMFeatureData(_ bpmFeatureStatus:BPMFeatureStatus?)
    {
        if let _ = bpmFeatureStatus
        {
            enableInfoView(bpmFeatureStatus!)
        }
    }
    
    func bleBloodPressureMonitorInterfaceUpdateNotificationStateForCharacteristic(_ characteristic: CBCharacteristic!, error: NSError!)
    {
        
    }
}
/*----------------------------------------------------------------------------------------------------------*/
// MARK: NSNotificationCenter Methods
/*----------------------------------------------------------------------------------------------------------*/



extension BloodPressureVC
{
    
    func addListner()
    {
        NotificationCenter.listenNotification(self, notificationName: Constants.NotificationName.onDeviceDisconned)
        NotificationCenter.listenNotification(self, notificationName: Constants.NotificationName.onServiceDiscovered)
        NotificationCenter.listenNotification(self, notificationName: Constants.NotificationName.onEnterForeground)
        NotificationCenter.listenNotification(self, notificationName: Constants.NotificationName.onEnterBackground)
    }
    
    func removeListner()
    {
        NotificationCenter.removelistener(self, notificationName: Constants.NotificationName.onDeviceDisconned)
        NotificationCenter.removelistener(self, notificationName: Constants.NotificationName.onServiceDiscovered)
        NotificationCenter.removelistener(self, notificationName: Constants.NotificationName.onEnterForeground)
         NotificationCenter.removelistener(self, notificationName: Constants.NotificationName.onEnterBackground)
    }
    
    func onEnterBackground()
    {
        checkAndStopNotify()
    }
    
    func onEnterForeground()
    {
        configureNotifyControl()
    }
    func onDeviceDisconned()
    {
        
    }
    
    func onServiceDiscovered()
    {
        initBpView()
    }
    
    
    
}


