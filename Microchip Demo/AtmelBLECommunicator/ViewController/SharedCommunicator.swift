

/* ***************************************************************************

SharedCommunicator.swift




Copyright (c) 2015, Atmel Corporation. All rights reserved.
Released under NDA
Licensed under Atmel's Limited License Agreement.


THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Atmel Corporation: http://www.atmel.com

******************************************************************************/

import Foundation
import CoreBluetooth

class GlobalCommunicator
{
    
    var communicator:BLECommunicator!
    var sourceDevice : BLESourceDevice!
    var connectedDevice : BLEDeviceInfo!
    var selectedService : CBService!
    var sppMessages = [Message]()
    var serialPortInterface : BLESerialPortInterface!
    var otaInterface:AtmelOTAInterface?
    
    var gattDBInterface : GattDBInterface?
    
    
    var selectedCharacteristic : CBCharacteristic?
    var selectedDescriptor : CBDescriptor?
    
    
    //1
    class var shared: GlobalCommunicator {
        //2
        struct Singleton {
            //3
            static let instance = GlobalCommunicator()
        }
        //4
        return Singleton.instance
    }
    
    init()
    {
        let message = Message(text: "", sender: "")
        self.sppMessages.append(message)
    }
    
}
extension GlobalCommunicator
{
    class func connectPeripheralObject(){
        
        GlobalCommunicator.shared.communicator.connect(GlobalCommunicator.shared.connectedDevice!.BLEPeripheral)
    }
    class func disconnectPeripheralObject(){
        GlobalCommunicator.shared.communicator.disconnect(GlobalCommunicator.shared.connectedDevice!.BLEPeripheral)
    }
}
