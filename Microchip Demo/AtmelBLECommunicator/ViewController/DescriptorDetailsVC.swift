/***************************************************************************
 
 DescriptorDetailsVC.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import UIKit
import CoreBluetooth

class DescriptorDetailsVC: GATTBaseViewController {
    
    
    let descriptorDetails   = ["Characteristic", "Descriptor", "Descriptor HEX Value", "Descriptor Value"]
    let CELL_IDENTIFIER     = "descriptorDetailCell"
    let seperatorViewWidth : CGFloat = 1.0
    
    @IBOutlet weak var descriptorDetailTableView: UITableView!
        @IBOutlet weak var notifyButton: UIButton!
    @IBOutlet weak var indicateButton: UIButton!
    @IBOutlet weak var readButtonViewWidthConstraint: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        descriptorDetailTableView.tableFooterView = UIView()
        
        descriptorDetailTableView.estimatedRowHeight = 80.0
        descriptorDetailTableView.rowHeight = UITableViewAutomaticDimension
        
        
        GlobalCommunicator.shared.gattDBInterface?.gattDescriptorOperationsDelegate = self
        readDescriptorValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        configureView()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "GATT DB"
    }
    
    
    
    
    @IBAction func readButtonClicked(_ sender: UIButton) {
        readDescriptorValue()
    }
    
    
    
    @IBAction func indicateButtonClicked(_ sender: UIButton) {
        
        if sender.isSelected {
           
            GlobalCommunicator.shared.gattDBInterface?.stopNotificationsForcharacteristic(GlobalCommunicator.shared.selectedCharacteristic)
            sender.isSelected = false
        }
        else{
            GlobalCommunicator.shared.gattDBInterface?.startNotificationsForcharacteristic(GlobalCommunicator.shared.selectedCharacteristic)
            sender.isSelected = true
        }
        
        readDescriptorValue()
    }
    

    @IBAction func notifyButtonClicked(_ sender: UIButton) {
        
        if sender.isSelected {
            
            GlobalCommunicator.shared.gattDBInterface?.stopNotificationsForcharacteristic(GlobalCommunicator.shared.selectedCharacteristic)
            sender.isSelected = false
        }
        else{
            GlobalCommunicator.shared.gattDBInterface?.startNotificationsForcharacteristic(GlobalCommunicator.shared.selectedCharacteristic)
            sender.isSelected = true
        }
        readDescriptorValue()
    }
    
    
    func readDescriptorValue(){
        GlobalCommunicator.shared.gattDBInterface?.readValueForDescriptor(GlobalCommunicator.shared.selectedDescriptor)
    }
}


extension DescriptorDetailsVC{
    
    
    func configureView(){
        
        notifyButton.isHidden = true
        indicateButton.isHidden = true
    
        
        readButtonViewWidthConstraint.constant = self.view.frame.width

        if GlobalCommunicator.shared.selectedCharacteristic?.hasNotifyProperty() == true {
            notifyButton.isHidden = false
            
            if GlobalCommunicator.shared.selectedCharacteristic?.isNotifying == true {
                notifyButton.isSelected = true
            }
            readButtonViewWidthConstraint.constant = (self.view.frame.width/2.0) - seperatorViewWidth

        }
        else if GlobalCommunicator.shared.selectedCharacteristic?.hasIndicateProperty() == true {
            indicateButton.isHidden = false
            
            if GlobalCommunicator.shared.selectedCharacteristic?.isNotifying == true {
                indicateButton.isSelected = true
            }
            readButtonViewWidthConstraint.constant = (self.view.frame.width/2.0) - seperatorViewWidth
        }
        
        self.view.layoutIfNeeded()
        
    }
    
    
    func updateUIWithResponse(_ response : GattDescriptorResponse){
        
        let characteristicCell = descriptorDetailTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! DescriptorDetailTableViewCell
        characteristicCell.descriptorDetailValueLabel.text = response.characteristic.uuid.getCharacteristicName()
        
        
        let descriptorCell = descriptorDetailTableView.cellForRow(at: IndexPath(row: 1, section: 0)) as! DescriptorDetailTableViewCell
        descriptorCell.descriptorDetailValueLabel.text = response.descriptor.uuid.getDescriptorName()
        
        
        if response.descriptorHexValue != nil {
            
            let descriptorHexValueCell = descriptorDetailTableView.cellForRow(at: IndexPath(row: 2, section: 0)) as! DescriptorDetailTableViewCell
            descriptorHexValueCell.descriptorDetailValueLabel.text = response.descriptorHexValue!
        }
        
        
        if response.descriptorValue != nil{
            
            let descriptorValueCell = descriptorDetailTableView.cellForRow(at: IndexPath(row: 3, section: 0)) as! DescriptorDetailTableViewCell
            descriptorValueCell.descriptorDetailValueLabel.text = response.descriptorValue!
        }
        
        descriptorDetailTableView.beginUpdates()
        descriptorDetailTableView.endUpdates()
    }
    
    
    
}


extension DescriptorDetailsVC : UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return descriptorDetails.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER) as? DescriptorDetailTableViewCell
        
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: CELL_IDENTIFIER) as? DescriptorDetailTableViewCell
        }
        
        cell?.descriptorDetailLabel.text = descriptorDetails[indexPath.row]
        
        return cell!
    }
}



extension DescriptorDetailsVC : GattDBInterfaceDescriptorDelegate{
    
    func sourceDeviceDidDiscoverDescriptorsForCharacteristic(_ characteristic: CBCharacteristic, withError error: NSError?){
        
    }

    
    func sourceDeviceDidRespondForReadRequestForDescriptor(_ descriptor: CBDescriptor, withResponse readresponse : GattDescriptorResponse){
    
        if readresponse.error == nil {
            updateUIWithResponse(readresponse)
        }
        else{
            let descriptorReadErrorAlert = UIAlertController(title: "ALERT", message: "Filed to read value from descriptor with error \(readresponse.error.localizedDescription)", preferredStyle: .alert)
            let doneAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            descriptorReadErrorAlert.addAction(doneAction)
            self.present(descriptorReadErrorAlert, animated: true, completion: nil)
        }
    }
    
    
    
    func sourceDeviceDidRespondForWriteRequestForDescriptor(_ descriptor: CBDescriptor, withResponse wrtieResponse : GattDescriptorResponse){
        
    }
}


















