/***************************************************************************
 
 DropDown.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/
import Foundation
import UIKit

protocol DropDownDelegate{
    func didselectRowIndropDownWith(_ element: String)
}

class DropDown: UIView,UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate{

    fileprivate var dropDownTableView : UITableView?
    fileprivate var tableDataSourceArray: NSArray?
    fileprivate var dropDownViewHeight:CGFloat?
    fileprivate var tapGestureRecognizer:UITapGestureRecognizer?
    fileprivate var parentButton:UIButton?
    
    
    var delgate:DropDownDelegate?
    var isShown:Bool?

    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK:- Initialization Methods
    /**
    *
    * Initialize dropDown with frame and elements
    */

    func initDropDownWith(_ frame : CGRect, elements: NSArray) -> DropDown
    {
        dropDownViewHeight = frame.height
        self.frame = frame
        tableDataSourceArray = elements;
        self.initDropDownTableViewWith(frame)
       
        return self
    }
    
    /**
    *
    * Intitialize drop down from a button
    */
    func initDropDownWith(_ frame: CGRect, elements:NSArray, button: UIButton) -> DropDown
    {
        dropDownViewHeight = frame.height
        self.frame = frame
        tableDataSourceArray = elements;
        self.initDropDownTableViewWith(frame)
        parentButton = button
        isShown = false
        return self
    }
    
    fileprivate func initDropDownTableViewWith(_ frame : CGRect)
    {
        dropDownTableView = UITableView(frame: frame, style: UITableViewStyle.plain)
        dropDownTableView?.dataSource = self
        dropDownTableView?.delegate = self
        dropDownTableView?.showsVerticalScrollIndicator = false
        dropDownTableView?.bounces = false
        
        self.addSubview(dropDownTableView!)
        
        let borderWidth = CGFloat(1.0)

        let leftBorderLayer     = sideBorderlayerWithFrame(CGRect(x: 0, y: 0, width:borderWidth, height: self.frame.size.height))
        let rightBorderLayer    = sideBorderlayerWithFrame(CGRect(x: self.frame.width - borderWidth, y: 0, width:borderWidth, height: self.frame.size.height))
        let bottomBorderLayer   = sideBorderlayerWithFrame(CGRect(x: 0, y: self.frame.height - borderWidth, width:self.frame.size.width, height: borderWidth))
        
        self.layer.addSublayer(leftBorderLayer)
        self.layer.addSublayer(rightBorderLayer)
        self.layer.addSublayer(bottomBorderLayer)
        self.layer.masksToBounds = true
    }
    
    
    func sideBorderlayerWithFrame(_ frame : CGRect) -> CALayer{
        
        let layer = CALayer()
        layer.frame = frame
        layer.borderColor = UIColor.black.cgColor
        layer.borderWidth = 1.0
        return layer
    }
    
    
    
    // MARK:- TableVIew DataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableDataSourceArray != nil
        {
            return tableDataSourceArray!.count
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var tableCell: UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "cell")
        if tableCell == nil
        {
            tableCell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "cell")
        }
        
        tableCell!.textLabel?.text = tableDataSourceArray?.object(at: indexPath.row) as? String
        tableCell!.selectionStyle = UITableViewCellSelectionStyle.none
        tableCell!.textLabel?.font = UIFont(name: "Roboto-Light", size: 15)
        return tableCell!
    }
    
    
    // MARK:- TableView Delegate
    
       func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if delgate != nil
        {
            delgate?.didselectRowIndropDownWith(tableDataSourceArray!.object(at: indexPath.row) as! String)
        }
        
        tableView.deselectRow(at: indexPath, animated: false)
        self.removeDropDown()
    }
    
    // MARK:- Utility methods
    
    func showDropDownFrom(_ rect: CGRect, view:UIView)
    {
        isShown = true;
        
        // Add tap gesture recognizer to parentView
        
        tapGestureRecognizer = UITapGestureRecognizer()
        tapGestureRecognizer?.delegate = self
        view.addGestureRecognizer(tapGestureRecognizer!)
        
        
        self.frame = CGRect(x: rect.origin.x, y: (rect.origin.y + rect.size.height), width: self.frame.size.width, height: 0)
        dropDownTableView?.frame = self.bounds
        view.addSubview(self);

        
        
        if self.parentButton != nil{
            self.parentButton?.isSelected = true
        }
        
        UIView.animate(withDuration: 0.2, delay: 0, options: UIViewAnimationOptions.curveEaseIn, animations: {

            self.frame = CGRect(x: self.frame.origin.x, y: self.frame.origin.y, width: self.frame.size.width, height: self.dropDownViewHeight!)
            self.dropDownTableView?.frame = self.bounds
            
            }, completion: nil)
    }
    
    
    func removeDropDown()
    {
        isShown = false
        
        if self.parentButton != nil{
            self.parentButton?.isSelected = false            
        }
        
        UIView.animate(withDuration: 0.2, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            self.frame = CGRect(x: self.frame.origin.x, y: self.frame.origin.y, width: self.frame.width, height: 0)
            self.dropDownTableView?.frame = self.bounds
        }) { (Bool) -> Void in
            self.superview?.removeGestureRecognizer(self.tapGestureRecognizer!)
            self.removeFromSuperview()
        }
    }
    
    
    
    // MARK:- Tap gesture recognizer delegate
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        let touchPoint = touch.location(in: self.superview)
        
        let touchPointInDropDown = self.convert(touchPoint, from: self.superview)
        
        // Check whether the touch is in the button
        if parentButton != nil
        {
            let touchPointRelToButton = parentButton?.convert(touchPoint, from: self.superview)
    
            if parentButton!.bounds.contains(touchPointRelToButton!)
            {
                return false
            }
        }
        
        // Check whether the touch is inside the dropdown
        if self.bounds.contains(touchPointInDropDown)
        {
            return false
        }
        else
        {
            self.removeDropDown()
            return true
        }
    }
    
    
    
}
