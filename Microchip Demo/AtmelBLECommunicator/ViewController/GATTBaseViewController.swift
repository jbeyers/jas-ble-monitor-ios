/***************************************************************************
 
 GATTBaseViewController.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import UIKit

class GATTBaseViewController: BaseViewController {

    //OTA Progress outlets
    
    var navInfo:OTAProgressInfoView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        GlobalCommunicator.shared.otaInterface?.infoDelegate = self
        updateOTAStatusOnUI()
  
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        super.prepare(for: segue, sender: sender)
     
    }
    //OTA
    
    
    func otauButton(_ sender: UIBarButtonItem){
        
        self.performSegue(withIdentifier: OTA_SETTINGS_SEGUE, sender: self)
    }
    
  
    func customizeNavigationControl(_ isOTAActive:Bool){
        
        if isOTAActive
        {
            let otauImg: UIImage = UIImage(named: "otauBarButton")!
            
            let button: UIButton = UIButton(type:.custom)
            //set image for button
            button.setImage(otauImg, for: UIControlState())
            //add function for button
            button.addTarget(self, action:#selector(GATTBaseViewController.otauButton(_:)), for: UIControlEvents.touchUpInside)
            //set frame
            button.frame = CGRect(x: 0, y: 0,width: 25,height: 27)
            
            let newOtauButton = UIBarButtonItem(customView: button)
            newOtauButton.setBackgroundImage(otauImg, for: UIControlState(), barMetrics: .compact)
            self.navigationItem.rightBarButtonItem = newOtauButton
        }
        
        //        else
        //            {self.navigationItem.rightBarButtonItem = nil}
    }
    
    func updateOTAStatusOnUI(){
        
        if let _ = GlobalCommunicator.shared.otaInterface{
            DispatchQueue.main.async(execute: {
                
                var isOTAActive = false
                switch(GlobalCommunicator.shared.otaInterface!.updateStatus){
                    
                case OTAUpdateStatus.otaAvailable:
                    //Show OTA button
                    isOTAActive = true
                    
                    break
                    
                case OTAUpdateStatus.updateAvailable:
                
                    isOTAActive = true
                    
                    
                    break
                    
                case OTAUpdateStatus.updated:
                    isOTAActive = true

                    break
                    
                    
                case OTAUpdateStatus.updating:
                    self.updateNavBarProgressInfo(nil)
                case OTAUpdateStatus.paused:
                    self.setPauseStateProgressBar(nil)
                    break
                    
                }
                self.customizeNavigationControl(isOTAActive)
            })
        }
    }
    
    func removeOTAStatusFromUI(){
        self.navigationItem.rightBarButtonItem = nil
    }
    
    
    func progressNavigationButton(){
        
        if navInfo == nil {
           
            navInfo = OTAProgressInfoView()
            navInfo?.delegate = self
        }
        let newOtauButton = UIBarButtonItem(customView: navInfo!)
        self.navigationItem.rightBarButtonItem = newOtauButton
    }
    
    
    func updateNavBarProgressInfo(_ progress:Float?){
        progressNavigationButton()
        navInfo?.setProgress(progress)
    }
    func setPauseStateProgressBar(_ progress:Float?){
        progressNavigationButton()
        navInfo?.setPauseState(progress)
    }
    
    func showOTAFailureAlert(_ withError:NSError){
        
        let otaAlert = UIAlertController(title:"OTA Failed",message:withError.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
            
        
            let IgnoreAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                UIAlertAction in
                
                GlobalCommunicator.shared.otaInterface?.updateStatus = OTAUpdateStatus.updateAvailable
                otaAlert.dismiss(animated: true, completion:nil)
                
            }
            
            otaAlert.addAction(IgnoreAction)
            self.present(otaAlert, animated: true, completion: nil)
        }

}
extension GATTBaseViewController:AtmelOTAInterfaceInfoDelegate{
    
    func atmelOtaInterfaceUpdateValueChanged(){
        
        updateOTAStatusOnUI()
        
    }
    func updatedProgressDataInfo(_ withString:String,progress:Float?){
        updateNavBarProgressInfo(progress)
    }
    func otaFailed(_ error:NSError){
        showOTAFailureAlert(error)
    }

    
}
extension GATTBaseViewController:OTAProgressInfoViewDelegate{
    
    func otaInfoViewTapped(){
        
        if let _  = GlobalCommunicator.shared.otaInterface{
            switch(GlobalCommunicator.shared.otaInterface!.updateStatus){
                
            case OTAUpdateStatus.otaAvailable:
                break
            case OTAUpdateStatus.updateAvailable:
                GlobalCommunicator.shared.otaInterface!.imageInfoNotification()
                self.performSegue(withIdentifier: OTA_INFO_VC_SEGUE, sender: self)
                break
                
            case OTAUpdateStatus.updated:
                break
            case OTAUpdateStatus.updating:
                self.performSegue(withIdentifier: OTA_INFO_VC_SEGUE, sender: self)
                break
            case OTAUpdateStatus.paused:
                self.performSegue(withIdentifier: OTA_INFO_VC_SEGUE, sender: self)
                break
            }
            
        }
    }

}
