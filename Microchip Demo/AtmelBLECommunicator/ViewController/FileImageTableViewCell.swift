/***************************************************************************
 
 FileImageTableViewCell.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import UIKit

class FileImageTableViewCell: UITableViewCell {

    @IBOutlet var fileName: UILabel!
    @IBOutlet var firmwareVersion: UILabel!
    @IBOutlet var updateTypeLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setupUIElements(){
        
        fileName.font = UIFont(name: "Roboto-Medium", size:CGFloat.getFontSizeToScreen(13.0,scale: 0.7));
        firmwareVersion.font = UIFont(name: "Roboto-Medium", size:CGFloat.getFontSizeToScreen(11.0,scale: 0.7));
        updateTypeLabel.font = UIFont(name: "Roboto-Regular", size:CGFloat.getFontSizeToScreen(10.5,scale:0.7));
        
        fileName.textColor = UIColor.colorWithHexString("666666")
        firmwareVersion.textColor = UIColor.colorWithHexString("808080")
        

        //        Roboto = ["Roboto-Thin", "Roboto-Italic", "Roboto-Light", "Roboto-BoldItalic", "Roboto-LightItalic", "Roboto-ThinItalic", "Roboto-Bold", "Roboto-Regular", "Roboto-Medium"]
    }
}
