/***************************************************************************
 
 OTAFileSelectViewController.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import UIKit

class OTAFileSelectViewController: BaseViewController {

    
    @IBOutlet weak var OTAfileListTable: UITableView!
    
    var imagesArray:[ImageHeader]?
    var filePathsArray:[String]?
    var selectedImageHeader:ImageHeader?
    var selectedFirmware:FirmWareVersion?
    
    let FORCE_UPGRADE   = "Force Upgrade"
    let FORCE_DOWNGRADE = "Force Downgrade"
    let UPGRADE         = "Upgrade"
    

    override func viewDidLoad() {

        super.viewDidLoad()
        self.navigationController!.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "Roboto-Medium", size: CGFloat.getFontSizeToScreen(16.0,scale: 0.6))!,NSForegroundColorAttributeName:UIColor.white]
        self.navigationItem.title = "Select Firmware Image File"

        // Do any additional setup after loading the view.
        OTAfileListTable.allowsSelectionDuringEditing = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
       
//        if selectedImageHeader == nil {
//            selectedImageHeader = (UIApplication.sharedApplication().delegate as! AppDelegate).savedImageHeader
//        }
        
        if let interface = GlobalCommunicator.shared.otaInterface{
            if selectedImageHeader == nil && interface.currentDeviceInformation != nil {
                selectedImageHeader = ImageHeader(withDeviceInfo:interface.currentDeviceInformation!)
            }
            selectedFirmware = interface.currentDeviceInformation?.firmwareVersion
        }
        
        if selectedImageHeader != nil {
            filePathsArray = Image.filterImageFilesWith(selectedImageHeader!)
        }
        else{
            filePathsArray = Image.getAllImageFileUrlArray()
        }
        
        imagesArray = filePathsArray?.getImageHeaderArray()
        OTAfileListTable.reloadData()
    }
   
    
    func removeFileAtPath(_ filePath : String?){
        
        if  filePath != nil{
            
            do{
                try  FileManager.default.removeItem(at: URL(string: filePath!)!)
            }
            catch{
                print("File deletion failed with error :\(error)")
            }
        }
    }
   
}

extension OTAFileSelectViewController:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if let _ = imagesArray
            {return imagesArray!.count}
       return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "fileCell") as? FileImageTableViewCell{
            
            cell.setupUIElements()
            cell.fileName.text = URL(string: filePathsArray![indexPath.row])!.lastPathComponent
            let currentFirmware = imagesArray![indexPath.row].firmwareVersion
            cell.firmwareVersion.text = currentFirmware?.description
            
            if selectedFirmware != nil {
                
                cell.updateTypeLabel.isHidden = false
                
                //if selectedFirmware!.isHigherThan(currentFirmware){
                if selectedFirmware!.isHigherThan(currentFirmware!){
                    
                    cell.updateTypeLabel.textColor = UIColor.colorWithHexString("b70707")
                    cell.updateTypeLabel.text = FORCE_DOWNGRADE

                    
                }
                //else if selectedFirmware!.isEqualTo(currentFirmware){
                else if selectedFirmware!.isEqualTo(currentFirmware!){
                    cell.updateTypeLabel.textColor = UIColor.colorWithHexString("6c6b6b")
                    cell.updateTypeLabel.text = FORCE_UPGRADE
                }
                else{
                    
                    cell.updateTypeLabel.textColor = UIColor.colorWithHexString("0079c1")
                    cell.updateTypeLabel.text =  UPGRADE
                }

            }
            else
            {
                cell.updateTypeLabel.isHidden = true
            }
            return cell

        }
        return UITableViewCell()
    }
    
    
    
    
}
extension OTAFileSelectViewController:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return CGFloat.getFontSizeToScreen(90.0, scale: 0.7)
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == UITableViewCellEditingStyle.delete {
            
            if  let _ = imagesArray {
                
                removeFileAtPath(filePathsArray![indexPath.row])
                
                if filePathsArray?.count != 0 {
                    filePathsArray?.remove(at: indexPath.row)
                }
                
                imagesArray?.remove(at: indexPath.row)
                OTAfileListTable.reloadData()
            }
        }
    }
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        let selectedCell = tableView.cellForRow(at: indexPath) as! FileImageTableViewCell
        
        
        if selectedCell.updateTypeLabel.isHidden == true{
            return
        }
        
        if  selectedCell.updateTypeLabel.text != FORCE_DOWNGRADE {
           
            GlobalCommunicator.shared.otaInterface!.image = Image(withFile: filePathsArray![indexPath.row])
            self.navigationController?.popViewController(animated: true)
        }
        else{
            
            let alertVC = UIAlertController(title: "ALERT", message: selectedCell.updateTypeLabel.text! + " is not possible!", preferredStyle: .alert)
            let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertVC.addAction(alertAction)
            self.present(alertVC, animated: true, completion: nil)
        }
    }
}
