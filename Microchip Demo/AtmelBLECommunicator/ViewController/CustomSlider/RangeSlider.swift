/* ***************************************************************************

RangeSlider.swift

Custom UISlider for control the proximity range of the peripheral device


Copyright (c) 2015, Atmel Corporation. All rights reserved.
Released under NDA
Licensed under Atmel's Limited License Agreement.


THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Atmel Corporation: http://www.atmel.com

******************************************************************************/



import UIKit
import QuartzCore
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


protocol RangeSliderDelegate:NSObjectProtocol
{
    func onValueChangedSlider()
    
}

class RangeSlider: UIControl {
    
    
    var RangeDelegate:RangeSliderDelegate?
    
    let lowTrackLayer = SafeSliderTrackLayer()
    let highTrackLayer = DangerSliderTrackLayer()
    let trackLayer = RangeSliderTrackLayer()
    let lowerThumbLayer = RangeSliderThumbLayer()
    let lowerThumbValueLayer = CATextLayer()
    let upperThumbValueLayer = CATextLayer()
    let upperThumbLayer = RangeSliderThumbLayer()
    var previousLocation = CGPoint()
    
    var minimumValueToSet: Double?
    
    var rangeMidValue:Double = 15
    var rangeLowerValue:Double = 0
    var rangeUpperValue:Double = 0
    
    var isRanged:Bool = false{
        didSet{
            if isRanged{
                minimumValueToSet = minimumValue+rangeLowerValue
                maximumValueToSet = maximumValue-rangeUpperValue
            }
            else{
                minimumValueToSet = minimumValue
                maximumValueToSet = maximumValue

            }

        }
    }

    var minimumValue: Double = 0.0 {
        didSet {
            updateLayerFrames()
            minimumValueToSet = minimumValue+rangeLowerValue
        }
    }
    
    var maximumValueToSet: Double?
    
    var maximumValue: Double = 100.0 {
        didSet {
            print("set maxvalue= '\(maximumValue)")
            updateLayerFrames()
            maximumValueToSet = maximumValue-rangeUpperValue

        }
    }
    
    
    var lowerValue: Double = 30.0 {
        didSet {
            updateLayerFrames()

        }
    }
    

    var upperValue: Double = 60.0 {
        didSet {
            updateLayerFrames()

        }
    }


    var trackMidlightTintColor: UIColor = UIColor.AtmelOrangeColor() {//orangeColor
        didSet {
            trackLayer.setNeedsDisplay()
            lowTrackLayer.setNeedsDisplay()
            highTrackLayer.setNeedsDisplay()

        }
    }
    
    var trackSafelightTintColor: UIColor = UIColor.AtmelGreenColor() {
        didSet {
            trackLayer.setNeedsDisplay()
            lowTrackLayer.setNeedsDisplay()
            highTrackLayer.setNeedsDisplay()
            
        }
    }
    
    var trackDangerlightTintColor: UIColor = UIColor.AtmelRedColor() {
        didSet {
            trackLayer.setNeedsDisplay()
            lowTrackLayer.setNeedsDisplay()
            highTrackLayer.setNeedsDisplay()
            
        }
    }

    var thumbTintColor: UIColor = UIColor.white {
        didSet {
            lowerThumbLayer.setNeedsDisplay()
            upperThumbLayer.setNeedsDisplay()
        }
    }

    var curvaceousness: CGFloat = 1.0 {
        didSet {
            trackLayer.setNeedsDisplay()
            lowerThumbLayer.setNeedsDisplay()
            upperThumbLayer.setNeedsDisplay()
            lowTrackLayer.setNeedsDisplay()
            highTrackLayer.setNeedsDisplay()

        }
    }

    var thumbWidth: CGFloat {
        return CGFloat(bounds.height)
    }

    override var frame: CGRect {
        didSet {
            updateLayerFrames()
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        trackLayer.rangeSlider = self
        trackLayer.contentsScale = UIScreen.main.scale
        layer.addSublayer(trackLayer)
        
        
        lowTrackLayer.rangeSlider = self
        lowTrackLayer.contentsScale = UIScreen.main.scale
        layer.addSublayer(lowTrackLayer)
        
        
        highTrackLayer.rangeSlider = self
        highTrackLayer.contentsScale = UIScreen.main.scale
        layer.addSublayer(highTrackLayer)
        
        /////dfdfdfdfdfdfdfdfd///////dfdfdfdfd
        

        prepareTextLayer(lowerThumbValueLayer)
        prepareTextLayer(upperThumbValueLayer)


        lowerThumbLayer.rangeSlider = self
        lowerThumbLayer.contentsScale = UIScreen.main.scale
        layer.addSublayer(lowerThumbLayer)

        upperThumbLayer.rangeSlider = self
        upperThumbLayer.contentsScale = UIScreen.main.scale
        layer.addSublayer(upperThumbLayer)
    }
    
    func prepareTextLayer(_ text : CATextLayer)
    {
        text.fontSize = 14.0
        text.foregroundColor = UIColor.white.cgColor
        text.backgroundColor = UIColor.AtmelRedColor().cgColor
        text.cornerRadius = 4.0
        text.alignmentMode = "center"
        text.isWrapped = true
        
        text.contentsScale = UIScreen.main.scale
        layer.addSublayer(text)
    }
    

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    func updateLayerFrames() {
        CATransaction.begin()
        CATransaction.setDisableActions(true)

        trackLayer.frame = bounds.insetBy(dx: 0.0, dy: bounds.height / 3)
        trackLayer.setNeedsDisplay()
        
        lowTrackLayer.frame = bounds.insetBy(dx: 0.0, dy: bounds.height / 3)
        lowTrackLayer.setNeedsDisplay()
        
        highTrackLayer.frame = bounds.insetBy(dx: 0.0, dy: bounds.height / 3)
        highTrackLayer.setNeedsDisplay()

        let lowerThumbCenter = CGFloat(positionForValue(lowerValue))

        lowerThumbLayer.frame = CGRect(x: lowerThumbCenter - thumbWidth / 2.0, y: 0.0, width: thumbWidth, height: thumbWidth)
        lowerThumbLayer.setNeedsDisplay()
        
        //**********************************************************/////
        
        lowerThumbValueLayer.frame = CGRect(x: lowerThumbCenter - (thumbWidth*1.25) / 2.0, y: -(thumbWidth/2.0), width: (thumbWidth*1.25), height: (thumbWidth/2.0))
        lowerThumbValueLayer.string = NSString(format:"%.0f", self.lowerValue)
        lowerThumbValueLayer.setNeedsDisplay()
        
    


        let upperThumbCenter = CGFloat(positionForValue(upperValue))
        upperThumbLayer.frame = CGRect(x: upperThumbCenter - thumbWidth / 2.0, y: 0.0,
            width: thumbWidth, height: thumbWidth)
        upperThumbLayer.setNeedsDisplay()
        
        //**********************************************************/////
        
        upperThumbValueLayer.frame = CGRect(x: upperThumbCenter - (thumbWidth*1.25) / 2.0, y: -(thumbWidth/2.0), width: (thumbWidth*1.25), height: (thumbWidth/2.0))
        upperThumbValueLayer.string = NSString(format:"%.0f", self.upperValue)
        upperThumbValueLayer.setNeedsDisplay()

        CATransaction.commit()
    }

    func positionForValue(_ value: Double) -> Double {
        _ = Double(thumbWidth)
        return Double(bounds.width - thumbWidth) * (value - minimumValue) /
            (maximumValue - minimumValue) + Double(thumbWidth / 2.0)
    }

    // Touch handlers
    override func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        previousLocation = touch.location(in: self)

        // Hit test the thumb layers
        if lowerThumbLayer.frame.contains(previousLocation) {
            lowerThumbLayer.highlighted = true
        } else if upperThumbLayer.frame.contains(previousLocation) {
            upperThumbLayer.highlighted = true
        }

        return lowerThumbLayer.highlighted || upperThumbLayer.highlighted
    }

    func boundValue(_ value: Double, toLowerValue lowerValue: Double, upperValue: Double) -> Double {
        return min(max(value, lowerValue), upperValue)
    }

    override func continueTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        let location = touch.location(in: self)

        // 1. Determine by how much the user has dragged
        let deltaLocation = Double(location.x - previousLocation.x)
        let deltaValue = (maximumValue - minimumValue) * deltaLocation / Double(bounds.width - bounds.height)

        previousLocation = location

        // 2. Update the values
        if lowerThumbLayer.highlighted {
            let newVal = lowerValue + deltaValue
            if   (newVal + rangeMidValue) < upperValue && (!isRanged || (newVal > minimumValueToSet))
            {
                lowerValue = newVal
                lowerValue = boundValue(lowerValue, toLowerValue: minimumValue, upperValue: upperValue)
            }
            else{
                print("\(newVal)  < \(lowerValue) && \(newVal) > \(String(describing: minimumValueToSet))")
            }
        } else if upperThumbLayer.highlighted {
            
            let newVal = upperValue + deltaValue
            if   (newVal - rangeMidValue)   >  lowerValue && (!isRanged || (newVal < maximumValueToSet))
            {
                upperValue = newVal
                upperValue = boundValue(upperValue, toLowerValue: lowerValue, upperValue: maximumValue)

            }
            else
            {
                 print("\(newVal)  < \(upperValue) && \(newVal) > \(String(describing: maximumValueToSet))")
            }
            
        }

        return true
    }

    override func endTracking(_ touch: UITouch?, with event: UIEvent?)
    {
        
        print("Range Self value changed: (\(self.lowerValue) \(self.upperValue))")
        lowerThumbLayer.highlighted = false
        upperThumbLayer.highlighted = false
        self.RangeDelegate?.onValueChangedSlider()
    }
}
