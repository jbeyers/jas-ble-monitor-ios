
/* ***************************************************************************

CircleView.swift

Custom UIView for creating circular view in Viewcontroller



Copyright (c) 2015, Atmel Corporation. All rights reserved.
Released under NDA
Licensed under Atmel's Limited License Agreement.


THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Atmel Corporation: http://www.atmel.com

******************************************************************************/

import UIKit

class CircleView: UIView {

    
    let ProximityTag = 100
    
    var myColor : UIColor!
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        myColor = self.backgroundColor
        self.backgroundColor = UIColor.clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
         myColor = self.backgroundColor
         self.backgroundColor = UIColor.clear
    }
    

    
    override func draw(_ rect: CGRect) {
        // Get the Graphics Context
        let context = UIGraphicsGetCurrentContext();
        
        
      
        
        
        
        if self.tag == ProximityTag
        {
            
            myColor.set()
            let p = UIBezierPath(ovalIn:rect)
            p.fill()
            // Set the circle outerline-width
            context?.setLineWidth(1.0);
            // Set the circle outerline-colour
//            UIColor.AtmelBrightBlueColor().set()
            UIColor.black.set()
            
            // Create Circle
            //CGContextAddArc(context, (frame.size.width)/2, frame.size.height/2, (frame.size.width-6 )/2, 0.0, CGFloat(.pi * 2.0), 1)
            
            let center = CGPoint(x: frame.size.width/2, y: frame.size.height/2)
            let radius = (frame.size.width-6 )/2
            context?.addArc(center: center, radius: radius, startAngle: 0, endAngle: CGFloat(.pi * 2.0), clockwise: true)
            
        }
        else
        {
            myColor.set()
            let p = UIBezierPath(ovalIn:rect)
            p.fill()
            // Set the circle outerline-width
            context?.setLineWidth(5.0);
            // Set the circle outerline-colour
            UIColor.white.set()
            
            // Create Circle
            //CGContextAddArc(context, (frame.size.width)/2, frame.size.height/2, (frame.size.width - 6 )/2, 0.0, CGFloat(M_PI * 2.0), 1)
            
            let center = CGPoint(x: frame.size.width/2, y: frame.size.height/2)
            let radius = (frame.size.width-6 )/2
            context?.addArc(center: center, radius: radius, startAngle: 0, endAngle: CGFloat(.pi * 2.0), clockwise: true)
            
        }
        // Draw
        context?.strokePath()
        

    }

}


