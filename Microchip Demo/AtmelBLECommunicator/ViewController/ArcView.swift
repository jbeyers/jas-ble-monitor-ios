import UIKit
import CoreGraphics

@IBDesignable
class ArcView: UIView {
    
    @IBInspectable var fillColor: UIColor = UIColor(red: 72/255.0, green: 218/255.0, blue: 190/255.0, alpha: 1.0)
    @IBInspectable var strokeWidth : CGFloat = 0.0
    @IBInspectable var index : CGFloat = 0

    
    override func draw(_ rect: CGRect) {
        drawArc()
    }
    
    
    func drawArc(){
        
        //create the path
       
        var plusPath = UIBezierPath()
        
        if UIDevice.is_iPad() && UIDevice.is_Landscape() {
            
            let viewHeight = (self.superview!.frame.size.width - 130)/3
            
            plusPath = UIBezierPath(arcCenter: CGPoint(x: self.bounds.origin.x + self.bounds.size.width/2.0, y: self.bounds.origin.y + viewHeight * (3 + index) + strokeWidth), radius: viewHeight * (3+index), startAngle: 0, endAngle: 2.0 * 3.14, clockwise: true)
        }
        else{
            
             plusPath = UIBezierPath(arcCenter: CGPoint(x: self.bounds.origin.x + (self.bounds.size.width/2.0),y: self.bounds.origin.y + (self.bounds.size.height*3)+(self.bounds.size.height*index) + strokeWidth), radius: self.bounds.size.height*(3+index) , startAngle: 0, endAngle: 2.0 * 3.14, clockwise: true)
        }
        
        
        fillColor.setFill()
        plusPath.fill()
        plusPath.lineWidth = strokeWidth
        
        
        //set the stroke color
        UIColor.white.setStroke()
        
        //draw the stroke
        plusPath.stroke()
    }
    
}
