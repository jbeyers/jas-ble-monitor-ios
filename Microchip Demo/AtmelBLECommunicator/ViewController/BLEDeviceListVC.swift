/* ***************************************************************************

//BLEDeviceListVC.swift
//
Viewcontroller class for scanning and listing devices;
//


Copyright (c) 2015, Atmel Corporation. All rights reserved.
Released under NDA
Licensed under Atmel's Limited License Agreement.


THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Atmel Corporation: http://www.atmel.com

******************************************************************************/


import Foundation
import UIKit
import CoreBluetooth
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class BLEDeviceListVC: BaseViewController,UISearchBarDelegate{
    
    
    /*----------------------------------------------------------------------------------------------------------*/
    // MARK: ScanDashboard Outlets
    /*----------------------------------------------------------------------------------------------------------*/
    
    @IBOutlet weak var ConnectingAlertTitle: UILabel!
    @IBOutlet weak var ConnectingAlertIndicator: UIActivityIndicatorView!
    @IBOutlet weak var ConnectingAlertView: UIView!
    @IBOutlet weak var ScanDashboardView: UIView!
    @IBOutlet weak var ScanIndicator: UIActivityIndicatorView!
    @IBOutlet weak var ScanFilterIcon: UIImageView!
    @IBOutlet weak var ScanControlButton: UIButton!
    @IBOutlet weak var ScanResultLabel: UILabel!
    @IBOutlet weak var ScanResultSearchBar: UISearchBar!
    @IBOutlet weak var ScanSearchControlView: UIView!
    @IBOutlet weak var ScanSearchControlAdjustConstraint: NSLayoutConstraint!
    @IBOutlet weak var ScanFilterIconAdjustConstraint: NSLayoutConstraint!
    @IBOutlet weak var BLEDeviceListTable: UITableView!
    @IBOutlet weak var AtmelLogoAnimBGView: UIView!
    @IBOutlet weak var AtmelAnimCircleImage: UIImageView!
    @IBOutlet weak var BLEDeviceListCollectionView: UICollectionView!

    
    
    
    let START_SCAN = "START SCAN"
    let STOP_SCAN = "STOP SCAN"
    

    
    let ZER0 = 0
    var SearchButton : UIButton!
    
    
    var bleDeviceListForTableView : NSMutableArray?//[AnyObject]()
    var bleDeviceListForTableViewBackup : NSMutableArray?//[AnyObject]()
    
    var selectedProfiles:Array<BLEProfile>?
    
    fileprivate var reconnectionAlertViewC : UIAlertController!
    fileprivate var lastSigUpdate:Double = Date.timeIntervalSinceReferenceDate
    fileprivate let updateIntvl = 3.0

    fileprivate var StopAllAnimation = false
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.layoutFix()
        
            }

    override func viewDidLoad()
    {
        super.viewDidLoad()
        


        addListner()
        
        self.bleDeviceListForTableView = NSMutableArray()
        self.bleDeviceListForTableViewBackup = NSMutableArray()
        
        //Adding Custom View in NavigationBar
        customNavigationItem()
        
        configureDashboardView()
        configureDeviceList()
        
        activateBLEforScanning()
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        applyFilterValues()
        super.viewWillAppear(animated)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        StopAllAnimation = false
        DisconnectDevice()
        
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    func setUpUIElements()
    {
//        ConnectingAlertTitle.font = UIFont(name: "Roboto-Medium", size:CGFloat.getFontSizeToScreen(10.0));
//         ScanControlButton.titleLabel!.font = UIFont(name: "Roboto-Medium", size:CGFloat.getFontSizeToScreen(10.0));
//        ScanResultLabel.font = UIFont(name: "Roboto-Medium", size:CGFloat.getFontSizeToScreen(12.0));
//        
        //        Roboto = ["Roboto-Thin", "Roboto-Italic", "Roboto-Light", "Roboto-BoldItalic", "Roboto-LightItalic", "Roboto-ThinItalic", "Roboto-Bold", "Roboto-Regular", "Roboto-Medium"]
        
//        self.navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName:UIFont(name: "Roboto-Medium", size:CGFloat.getFontSizeToScreen(16.0))!,NSForegroundColorAttributeName:UIColor.whiteColor()]
//      ScanResultSearchBar
    }
    /*----------------------------------------------------------------------------------------------------------*/
    // MARK: Filter Selected Profiles
    /*----------------------------------------------------------------------------------------------------------*/
    
    
    func filterApplied(_ startScan : Bool)
    {
//        ConnectionAlert(Alert:AlertType.eFilterApplied, Delay: 0.4)
        if self.ScanControlButton.isSelected && startScan
        {
            
            print("======================StartScan from filter")
            startScanDevices()
        }
    }
    
    func isFilterStateChanged() -> Bool
    {
        if SettingsVC.getFilterStatus() != ScanFilterIcon.isHidden
        {
            return false
        }
        return true
    }
    
    func applyFilterValues()
    {
        selectedProfiles  = nil
        if SettingsVC.getFilterStatus() == true
        {
            
            if let filterArray = UserDefaults.standard.array(forKey: Constants.UserDefaultsKey.filterKey)
            {
                if filterArray.count > 0
                {
                    selectedProfiles = Array<BLEProfile>()
                }
                for filter in filterArray
                {
                    let filterProfile = BLEProfile(rawValue: filter as! String)!
                    selectedProfiles?.append(filterProfile)
                }
            }
            filterApplied(isFilterStateChanged())
            showFilterIcon()
        }
        else
        {   filterApplied(isFilterStateChanged())
            hideFilterIcon()
        }
        


    }
    
    
    
    func activateBLEforScanning()
    {
        GlobalCommunicator.shared.communicator=BLECommunicator(scanmode: ScanMode.passive, forBLEProfiles: nil, withDelegate: self)
        
        // Do any additional setup after loading the view, typically from a nib.

    }
    
    /*----------------------------------------------------------------------------------------------------------*/
    // MARK: BLE Device scanning
    /*----------------------------------------------------------------------------------------------------------*/
    
    
    func startScanDevices()
    {
        self.ScanControlButton.isSelected = true
        handleDiscoveredDeviceList( action: DisplayList.removeAll)
        animAtmelCircle()
        self.ScanControlButton.titleLabel?.text = STOP_SCAN
        // Check Scan_control_button state and start device scan
        GlobalCommunicator.shared.communicator.startScan(selectedProfiles)
        
        scanResultDisplay(ResultDisplay.eStart)
        hideScanActivity(false)
        
    }
    

    func stopScanning()
    {
        self.ScanControlButton.isSelected = false
        GlobalCommunicator.shared.communicator.stopScan()
        scanResultDisplay(ResultDisplay.eStop)
        self.ScanControlButton.titleLabel?.text = START_SCAN
        hideScanActivity(true)
        
    }
    
    
    func checkAlreadyExist(_ item : BLEDeviceInfo) -> Bool
    {
        var index = 0
        
        while index < bleDeviceListForTableView?.count
        {
            let deviceInfo = bleDeviceListForTableView?.object(at: index) as! BLEDeviceInfo
            if deviceInfo.BLEPeripheral.identifier.uuidString == item.BLEPeripheral.identifier.uuidString
            {
                
                let now = Date.timeIntervalSinceReferenceDate
                if (now - lastSigUpdate) > updateIntvl
                {
                    
                    lastSigUpdate = now
                    deviceInfo.BLEPeripheralRSSI = item.BLEPeripheralRSSI
                    self.reloadList(IndexPath(row:index,section:0))
                    
                }
                return true
            }
            index += 1
            
        }
        return false
    }
    
    func checkSearchCriteriaPassed(_ item : BLEDeviceInfo) -> Bool
    {
        if !ScanResultSearchBar.text!.isEmpty
        {
            if item.BLEPeripheralLocalName!.range(of: ScanResultSearchBar.text!) != nil{
                
                return true
            }
            else
            {return false }
        }
        return true
    }
    
    
    func handleDiscoveredDeviceList(_ item : BLEDeviceInfo? = nil ,action : DisplayList )
    {
        switch(action)
        {
        case DisplayList.addItem:
            
            if !checkAlreadyExist(item!)
            {
                if checkSearchCriteriaPassed(item!)
                {
                    let row = bleDeviceListForTableView!.count
                    
                    if row == ZER0
                    {
                        //To hide default circle animation when a new device found
                        self.AtmelLogoAnimBGView.fadeOut()
                    }
                    
                    bleDeviceListForTableView?.add(item!)
                    reloadList(IndexPath(row:row,section:0), insert: true)
                    // To change "Scanning..." in Scan Result Display
                    scanResultDisplay(ResultDisplay.eFound, count: row+1)
                }
            }
            
            
            
        case DisplayList.removeItem:
            
            bleDeviceListForTableView?.remove(item!)
            
        case DisplayList.removeAll:
            
            bleDeviceListForTableView?.removeAllObjects()
            reloadList(nil)
            
        }

    }
    
    /*----------------------------------------------------------------------------------------------------------*/
    // MARK: Right Menu Buttons
    /*----------------------------------------------------------------------------------------------------------*/
    
    
    func customNavigationItem()
    {
        self.navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName:UIFont(name: "Roboto-Medium",size:CGFloat.getFontSizeToScreen(15.0, scale: 1))!,NSForegroundColorAttributeName:UIColor.white]
        
        let menuBarItem = UIBarButtonItem(customView: rightMenuView())
        self.navigationItem.rightBarButtonItem = menuBarItem
   
        
        let leftBarButtonItem = UIBarButtonItem(customView: leftMenuView())
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
    }

    
    func rightMenuView() -> UIView
    {
        
        
        let RIGHT_MENU_FRAME        = CGRect(x: 00, y: 0, width: 120, height: 40)
        let SEARCH_BUTTON_FRAME     = CGRect(x: 00, y: 0, width: 40, height: 40)
        let MENU_BUTTON_FRAME       = CGRect(x: 40, y: 0, width: 40, height: 40)
        let SETTINGS_BUTTON_FRAME   = CGRect(x: 80, y: 0, width: 40, height: 40)
        
        
        let rightMenuView = UIView(frame: RIGHT_MENU_FRAME)
        
        // Right Menu View |--Search--||--Menu--||--Settings--|
        //Search Button
        SearchButton = UIButton(frame: SEARCH_BUTTON_FRAME)
        SearchButton.setImage(UIImage(named: "SearchIcon"), for: UIControlState())
        SearchButton.addTarget(self, action: #selector(BLEDeviceListVC.onSearchTouched(_:)), for: UIControlEvents.touchUpInside)
        SearchButton.isHidden = true
        rightMenuView.addSubview(SearchButton)

        //Menu Button
        let menuButton = UIButton(frame: MENU_BUTTON_FRAME)
        menuButton.setImage(UIImage(named: "MenuIcon"), for: UIControlState())
        menuButton.addTarget(self, action: #selector(BLEDeviceListVC.onMenuTouched(_:)), for: UIControlEvents.touchUpInside)
        rightMenuView.addSubview(menuButton)
        
        //Settings Button
        let settingsButton = UIButton(frame: SETTINGS_BUTTON_FRAME)
        settingsButton.setImage(UIImage(named: "SettingsIcon"), for: UIControlState())
        settingsButton.addTarget(self, action: #selector(BLEDeviceListVC.onSettingsTouched(_:)), for: UIControlEvents.touchUpInside)
        rightMenuView.addSubview(settingsButton)
        
        
        return rightMenuView
    }
    
    
    func leftMenuView() -> UIView {
        
        let LEFT_MENU_FRAME     = CGRect(x: 0, y: 0, width: 60, height: 40)
        let HOME_BUTTON_FRAME   = CGRect(x: 0, y: 0, width: 60, height: 40)
        
        // Home button
        let homeButton = UIButton(frame: HOME_BUTTON_FRAME)
        homeButton.addTarget(self, action: #selector(BLEDeviceListVC.homeButtonClicked), for: UIControlEvents.touchUpInside)
        homeButton.setTitle("Home", for: UIControlState())
        homeButton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 15)
        homeButton.setTitleColor(UIColor.white, for: UIControlState())
        //homeButton.backgroundColor = UIColor.AtmelRedColor()
        
        let leftMenuView = UIView(frame: LEFT_MENU_FRAME)
        leftMenuView.addSubview(homeButton)
        
        return leftMenuView
    }

    
    func homeButtonClicked(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onMenuTouched(_: AnyObject)
    {
        moveToView("presentAboutView")
//        moveToView("presentMenuView")
        //presentAboutView
    }
    
    
    @IBAction func onSettingsTouched(_: AnyObject)
    {
        moveToView("presentSettingsView")
    }
    
    
    @IBAction func onSearchTouched(_ sender:  AnyObject)
    {
        
        if bleDeviceListForTableView?.count > ZER0 // Need to modify with device list count
        {
            
            scanSearchControlVisibility()
        }
        
    }
    
    func onDeviceListStartUpdating()
    {

        if bleDeviceListForTableView?.count > ZER0 // Need to modify with device list count
        {
            SearchButton.isHidden = false
            
        }
        else
        {
            SearchButton.isHidden = true
        }
    }
    
    /*
    if let searchButton = sender as? UIButton
    {
    }
    */
    
    

    
    func configureDashboardView()
    {
        self.ScanSearchControlAdjustConstraint.constant = getSearchControlLeadingConstant(true)
        scanResultDisplay(ResultDisplay.eNil)
        // Enable Scan control button when Central manager initilized successfully
        enableScanControl(false)
        customizeSearchBar()
        hideScanActivity(true)
        hideFilterIcon()
        AtmelAnimCircleImage.isHidden = true
        ConnectingAlertView.isHidden = true

    }
    
    func configureDeviceList()
    {
//        if UIDevice.is_iPad()
//        {
//            self.BLEDeviceListCollectionView.hidden = false
//            self.BLEDeviceListTable.hidden = true
//            self.BLEDeviceListTable.delegate = nil
//            self.BLEDeviceListTable.dataSource = nil
//        }
//        else
//        {
//            self.BLEDeviceListCollectionView.hidden = true
//            self.BLEDeviceListCollectionView.delegate = nil
//            self.BLEDeviceListCollectionView.dataSource = nil
//            self.BLEDeviceListTable.hidden = false
//
//        }
        
        self.BLEDeviceListCollectionView.isHidden = true
        self.BLEDeviceListCollectionView.delegate = nil
        self.BLEDeviceListCollectionView.dataSource = nil
        self.BLEDeviceListTable.isHidden = false
    }
    
    func hideFilterIcon()
    {
        ScanFilterIcon.isHidden = true
    }
    
    func showFilterIcon()
    {
        ScanFilterIcon.isHidden = false
    }
    
    func hideScanActivity(_ state : Bool)
    {
        var constraint : CGFloat = 10.0
        if state == true
        {
            self.ScanIndicator.stopAnimating()
            constraint = -20.0

        }
        else
        {

            self.ScanIndicator.isHidden = false ;
            self.ScanIndicator.startAnimating()
        }
        
            UIView.animate(withDuration: 0.5, animations: { () -> Void in
                self.ScanFilterIconAdjustConstraint.constant = constraint
                self.view.layoutIfNeeded()
            })
//
    }
    
    func enableScanControl(_ state : Bool)
    {
         ScanControlButton.isEnabled = state
        if ScanControlButton.isSelected  && state == false
        {
            stopScanning()
            
        }
    }
    
    func bluetoothStateChanged(PoweredON state : Bool , statusMessage :ResultDisplay)
    {
        
        self.scanResultDisplay(statusMessage)
        enableScanControl(state)
        ConnectionAlert(Hidden: state, Alert: AlertType.eBlueToothOff)
        handleDiscoveredDeviceList(action: DisplayList.removeAll)

        
       /* self.view.userInteractionEnabled  = state
        if state // if powered ON
        {
            self.view.alpha = 1.0
           
        }
        else
        {
            self.view.alpha = 0.5
            handleDiscoveredDeviceList(action: DisplayList.removeAll)
        }*/
    }
    
    /*----------------------------------------------------------------------------------------------------------*/
    // MARK: ScanResultLabel
    /*----------------------------------------------------------------------------------------------------------*/
    
    func scanResultDisplay(_ eDisplay : ResultDisplay, count : Int = 0 )
    {
        
        switch (eDisplay)
        {
        case .eNil:
            self.ScanResultLabel.text = ""
            
        case .eStart:
            self.ScanResultLabel.text = "Scanning....."
            
        case .eStop:
            //To change scan result appropriate message
            //if GlobalCommunicator.shared.communicator.CBSManager.state == CBCentralManagerState.poweredOn
            if #available(iOS 10.0, *) {
                if GlobalCommunicator.shared.communicator.CBSManager.state == CBManagerState.poweredOn
                {updateDispayLabelAfterDelay()}
            } else {
                // Fallback on earlier versions
            }
            
        case .eFound:
            var label = "Device"
            if count > 1
            {label = "Devices" }
            self.ScanResultLabel.text = "\(count)"+" \(label) found"
            
        case .eNoneFound:
            self.ScanResultLabel.text = "No Devices found"
            
        case .eConnect:
            self.ScanResultLabel.text =  " Connecting.."
            
        case .eConnectionFailed:
            self.ScanResultLabel.text =  "Device is disconnected"
//            updateDispayLabelAfterDelay()
            
        case .eSearchFinished:
//            self.ScanResultLabel.text = "Searching finished "
            updateDispayLabelAfterDelay()
            
        case .eBLEOFF:
            self.ScanResultLabel.text = "Bluetooth is currently off"
        
        }
        
    }
    
    
    func updateDispayLabelAfterDelay()
    {
        let deviceCount = bleDeviceListForTableView!.count
        //To change scan result appropriate message
        if deviceCount > self.ZER0
        {self.scanResultDisplay(ResultDisplay.eFound, count:deviceCount )}
        else
        {self.scanResultDisplay(ResultDisplay.eNoneFound)}

    }
 
    /*----------------------------------------------------------------------------------------------------------*/
    // MARK: UISearchBar Custom Methods
    /*----------------------------------------------------------------------------------------------------------*/
    
    func customizeSearchBar()
    {
        let textFieldInsideSearchBar = self.ScanResultSearchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.textColor = UIColor.white
        textFieldInsideSearchBar?.borderStyle = UITextBorderStyle.none
        
        let cancelButton = self.ScanResultSearchBar.value(forKey: "cancelButton") as! UIButton
        cancelButton.setTitleColor(UIColor.white, for: UIControlState())
        
        
        if textFieldInsideSearchBar?.responds(to: #selector(setter: UITextField.attributedPlaceholder)) == true
        {
            let dict = [NSForegroundColorAttributeName: UIColor.white]
            textFieldInsideSearchBar?.attributedPlaceholder = NSAttributedString(string: "Name", attributes: dict)
        }

    }
    
    
    func getSearchControlLeadingConstant(_ hide : Bool)-> CGFloat
    {
        //To hide search contrl leading space to superview change to view size max
        if hide == true
        {
           return max(self.view.frame.size.width,self.view.frame.size.height)
        }
        return 0.0
    }
    
    func isSearchControlHidden()->Bool
    {
        //To check Search control out of view bounds 
        //if it is zero it is visible.. above zero is hidden
        if self.ScanSearchControlAdjustConstraint.constant > 10
        {
            return true
        }
        return false
    }
    
    
    func scanSearchControlVisibility()
    {
        self.ScanResultSearchBar.text = ""
        var frame_x : CGFloat
        if isSearchControlHidden()
        {
            //Currently Search bar is hidden ... Animate to visible area
            frame_x = getSearchControlLeadingConstant(false)
            startSearchFunction()
        }
        else
        {
            frame_x = getSearchControlLeadingConstant(true)
            //Currently search bar is visible
            refreshTableViewWithAllDeviceList()
        }
        
        UIView.animate( withDuration: 0.3, animations: { () -> Void in
            self.ScanSearchControlAdjustConstraint.constant = frame_x;
            self.view.layoutIfNeeded()
        })
    }
    /*----------------------------------------------------------------------------------------------------------*/
    // MARK: START / STOP SCAN Methods
    /*----------------------------------------------------------------------------------------------------------*/
    
    
    @IBAction func onScanControlTouched(_ sender: AnyObject) {
        
        if self.ScanControlButton.isSelected
        {
            
            // Check Scan_control_button state and stop device scan
            self.AtmelAnimCircleImage.layer.removeAllAnimations()
            stopScanning()
            // Stop activity Indicator
        }
        else
        {
            startScanDevices()
        }
    }
    
    /*----------------------------------------------------------------------------------------------------------*/
    // MARK: UISearchBar Delegate Methods
    /*----------------------------------------------------------------------------------------------------------*/
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        self.view.endEditing(true)
        
        let searchPredicate = NSPredicate(format: "BLEPeripheralLocalName contains [c] %@", searchBar.text!)
        let array = bleDeviceListForTableView!.filtered(using: searchPredicate)
        
        handleDiscoveredDeviceList( action: DisplayList.removeAll)
        for object  in array
        {
            //Special case List Updation
            bleDeviceListForTableView?.add(object as! BLEDeviceInfo)
        }
        scanResultDisplay(ResultDisplay.eSearchFinished)
        reloadList(nil)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
    {

        //bleDeviceListForTableViewBackup
        self.view.endEditing(true)
        scanSearchControlVisibility()
    }
    
    
    func refreshTableViewWithAllDeviceList()
    {
        bleDeviceListForTableView = bleDeviceListForTableViewBackup
        reloadList(nil)
    }
    
    func startSearchFunction()
    {
        bleDeviceListForTableViewBackup = bleDeviceListForTableView
    }
    /*----------------------------------------------------------------------------------------------------------*/
    // MARK: Atmel Logo Animation
    /*----------------------------------------------------------------------------------------------------------*/
    
    func animAtmelCircle()
    {
        AtmelAnimCircleImage.isHidden = false
        rotateView()
    }
    
    
    func rotateView() {
        
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveLinear, animations: { () -> Void in
            self.AtmelAnimCircleImage.transform = self.AtmelAnimCircleImage.transform.rotated(by: CGFloat(Double.pi/2))
            }) { (finished) -> Void in
                
                if self.bleDeviceListForTableView?.count > self.ZER0
                {
                    self.AtmelLogoAnimBGView.fadeOut()
                }
                else
                {
                    if self.ScanControlButton.isSelected
                    {self.rotateView()}
                }

                
        }
    }
    

    /*----------------------------------------------------------------------------------------------------------*/
    // MARK: Serial Port Checking
    /*----------------------------------------------------------------------------------------------------------*/
    

    
    /*----------------------------------------------------------------------------------------------------------*/
    // MARK: Connect / Disconnect BLEDevice
    /*----------------------------------------------------------------------------------------------------------*/
    
    class func isCustomSerialChat() ->Bool
    {
        if GlobalCommunicator.shared.connectedDevice.BLEPeripheralServices?.contains(CBUUID(bleService: BLEService.SerialPortService)) == true
        {
            return true
        }
        return false
    }
    func handleSerialPortProfile()->Bool
    {
        if BLEDeviceListVC.isCustomSerialChat()
        {
            if let _ = GlobalCommunicator.shared.serialPortInterface
            {GlobalCommunicator.shared.serialPortInterface.stopServices()}
            
            GlobalCommunicator.shared.serialPortInterface = nil
            //To DO
            GlobalCommunicator.shared.serialPortInterface = BLESerialPortInterface(sourceDevice: nil, delgateObject: nil)
            GlobalCommunicator.shared.serialPortInterface.profileReadyDelegate = self
            return true
        }
        return false
    }

    func ConnectDevice(_ row : Int)
    {
        GlobalCommunicator.shared.connectedDevice = bleDeviceListForTableView?.object(at: row) as? BLEDeviceInfo
        if(!handleSerialPortProfile()){
            GlobalCommunicator.connectPeripheralObject()
        }
        scanResultDisplay(ResultDisplay.eConnect, count: 0)
        ConnectionAlert(Hidden: false)
    }
    
    func DisconnectDevice()
    {
        if let _ = GlobalCommunicator.shared.connectedDevice
        {
            GlobalCommunicator.disconnectPeripheralObject()
            clearConnectionData()
            
        }
    }
    
    func clearConnectionData()
    {
        
        print( "Clear connection DATA")
        GlobalCommunicator.shared.connectedDevice = nil
        GlobalCommunicator.shared.sourceDevice = nil
        GlobalCommunicator.shared.selectedService = nil
        if GlobalCommunicator.shared.serialPortInterface != nil{
            
            GlobalCommunicator.shared.serialPortInterface.stopServices()
            GlobalCommunicator.shared.serialPortInterface = nil
        }
    }
    
    func ConnectionAlert(Hidden state : Bool = false , Alert message : AlertType = AlertType.eConnecting , Delay delay : Double = 0.0 )
    {
        ConnectingAlertView.isHidden = state
        switch(message)
        {
        case AlertType.eConnecting:
            ConnectingAlertIndicator.startAnimating()
            if let _ = GlobalCommunicator.shared.connectedDevice
            {
                ConnectingAlertTitle.text = "\(GlobalCommunicator.shared.connectedDevice!.BLEPeripheralLocalName!) \(message.rawValue)"
            }
            else
            {
                ConnectingAlertTitle.text = message.rawValue
            }
        default:
           ConnectingAlertIndicator.stopAnimating()
           ConnectingAlertTitle.text = message.rawValue
        }
        
        if delay != 0.0
        {
            Util.delay(delay)
                {
                    self.ConnectionAlert(Hidden: true, Alert: AlertType.eNone)
            }

        }
        
    }
    
    func onDisconnection()
    {
        showScanView()
        startScanDevices()
       
    }
    
    /*----------------------------------------------------------------------------------------------------------*/
    // MARK:Segue With Identifier
    /*----------------------------------------------------------------------------------------------------------*/
    
    
    func ConnectionSucceeded()
    {

        self.ConnectionAlert(Hidden:true)
        self.stopScanning()
        self.StopAllAnimation = true
        
    }
    
    func moveToView(_ identifier : String)
    {
        self.performSegue(withIdentifier: identifier, sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "showServiceLIstView"
        {
            
            if let _ = GlobalCommunicator.shared.connectedDevice
            {
                let serviceList: BLEServiceList = segue.destination as! BLEServiceList
                serviceList.navigationItem.title = GlobalCommunicator.shared.connectedDevice!.BLEPeripheralLocalName!
            }
            else
            {
                print("error")
            }
            
            handleDiscoveredDeviceList( action: DisplayList.removeAll)
            scanResultDisplay(ResultDisplay.eNil)
        }


    }
}

/*----------------------------------------------------------------------------------------------------------*/
// MARK: BLE Device List
/*----------------------------------------------------------------------------------------------------------*/


extension BLEDeviceListVC : UITableViewDelegate,UITableViewDataSource ,UICollectionViewDataSource, UICollectionViewDelegate
{
    
    /*----------------------------------------------------------------------------------------------------------*/
    // MARK: BLE Device List TableView Delegate and DataSource
    /*----------------------------------------------------------------------------------------------------------*/
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return CGFloat.getFontSizeToScreen(95.0,scale:0.7)
        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        onDeviceListStartUpdating()
        return bleDeviceListForTableView!.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let devicellIdentifier = "BleDeviceCell"
        let cell = self.BLEDeviceListTable.dequeueReusableCell(withIdentifier: devicellIdentifier) as! BLEDeviceCell
        cell.setCell(bleDeviceListForTableView?.object(at: indexPath.row) as! BLEDeviceInfo)
        //bleDeviceListForTableView[indexPath.row] as! [String : AnyObject]
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        let tempFooterView = UIView(frame: CGRect(x: 00, y: 0, width: 10, height: 10))
        return tempFooterView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        ConnectDevice(indexPath.row)
    }
    
    
    /*----------------------------------------------------------------------------------------------------------*/
    // MARK: BLE Device List CollectionView Delegate and DataSource
    /*----------------------------------------------------------------------------------------------------------*/
    
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        onDeviceListStartUpdating()
        return bleDeviceListForTableView!.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        
        let deviceCollectionCellIdentifier = "BleDeviceCollectionCell"
        // get a reference to our storyboard cell
        let cell = self.BLEDeviceListCollectionView.dequeueReusableCell(withReuseIdentifier: deviceCollectionCellIdentifier, for: indexPath) as! BLEDevicelCollectionCell
        cell.setCollectionCell(self.bleDeviceListForTableView?.object(at: indexPath.row) as! BLEDeviceInfo)
        
        
        return cell
    }
    
    // --- UICollectionViewDelegate protocol ---
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!", terminator: "")
        ConnectDevice(indexPath.row)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, insetForSectionAtIndex section: NSInteger) -> UIEdgeInsets {
        
        // (top, left, bottom, right)
        let space = getFlowLayout(bleDeviceListForTableView!.count)
        return UIEdgeInsetsMake(0, space, 0, space)
        
    }
    
    func collectionView(_ collectionView: UICollectionView!, layout collectionViewLayout: UICollectionViewLayout!,        minimumInteritemSpacingForSectionAtIndex section:Int)->CGFloat
    {
        return getFlowLayout(bleDeviceListForTableView!.count)
    }
    
    
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval)
    {
        self.BLEDeviceListTable.reloadData()
    }
    
    /*----------------------------------------------------------------------------------------------------------*/
    // MARK: BLE Device List Updating Common
    /*----------------------------------------------------------------------------------------------------------*/
    
    
    func getFlowLayout(_ count : Int) -> CGFloat
    {
        let cellSize = CGSize(width: 250,height: 360)
        let width = self.BLEDeviceListCollectionView.frame.size.width
        let MAX_ROW_COUNT = Int(width / cellSize.width)
        if count >= MAX_ROW_COUNT || count == ZER0
        {
            return 0
        }
        else
        {
            //   ||-----|_cell_|-----||
            //   ||--XX--XX--XX--XX--||
            //   ||-----XX----XX-----||
            //   ||--------XX--------||
            let blankSpace = width  - ( cellSize.width * CGFloat(count))
            return blankSpace / (2.0 * CGFloat(count))
        }
    }
    
    func reloadList(_ indexPath : IndexPath? , insert : Bool = false )
    {
        
    if indexPath != nil
        {
            if insert == true
            {
                self.BLEDeviceListTable.insertRows(at: [indexPath!], with: UITableViewRowAnimation.none)
            }
            else
            {
                self.BLEDeviceListTable.reloadRows(at: [indexPath!], with: UITableViewRowAnimation.none)
                
            }
        }
        else
        {
            self.BLEDeviceListTable.reloadData()
        }
    
//        if UIDevice.is_iPad()
//        {
//            if indexPath != nil
//            {
//                if insert == true
//                {
//                    self.BLEDeviceListCollectionView.insertItemsAtIndexPaths([indexPath!])
//                }
//                else
//                {
//                    self.BLEDeviceListCollectionView.reloadItemsAtIndexPaths([indexPath!])
//                }
//            }
//            else
//            {
//                self.BLEDeviceListCollectionView.reloadData()
//            }
//            
//        }
//        else
//        {
//            if indexPath != nil
//            {
//                if insert == true
//                {
//                    self.BLEDeviceListTable.insertRowsAtIndexPaths([indexPath!], withRowAnimation: UITableViewRowAnimation.None)
//                }
//                else
//                {
//                    self.BLEDeviceListTable.reloadRowsAtIndexPaths([indexPath!], withRowAnimation: UITableViewRowAnimation.None)
//                    
//                }
//            }
//            else
//            {
//                self.BLEDeviceListTable.reloadData()
//            }
//        }
        
    }
}
/*----------------------------------------------------------------------------------------------------------*/
// MARK: BLESerialPortProfileReadyDelegate Methods
/*----------------------------------------------------------------------------------------------------------*/


extension BLEDeviceListVC:BLESerialPortProfileReadyDelegate{
    
    func bleSerialPortReady(){
        
        if isReconnectionAlertDisplayed()
        {
            proceedWithReconnection()
        }
        else
        {
            GlobalCommunicator.connectPeripheralObject()
        }
        
    }
}

/*----------------------------------------------------------------------------------------------------------*/
// MARK: BLECommunicatorDelegate Methods
/*----------------------------------------------------------------------------------------------------------*/


extension BLEDeviceListVC: BLECommunicatorDelegate {
    
    
    
    func bleCommunicator(_ blecommunicator: BLECommunicator, communicatorInitializationFailWithBluetoothState state: BluetoothState)
    {
        showScanView()
        bluetoothStateChanged(PoweredON: false, statusMessage: ResultDisplay.eNil)
    }
    
    func bleCommunicator(_ blecommunicator: BLECommunicator, communicatorInitializationSuccessWithBluetoothState state: BluetoothState)
    {
        NSLog("central manager’s state is updated :  ON");
        bluetoothStateChanged(PoweredON: true, statusMessage: ResultDisplay.eNil)
    }
    
    func bleCommunicator(_ bleCommunicator: BLECommunicator!, didDiscoverPeripheral peripheral: CBPeripheral!, advertisementData: [AnyHashable: Any]!, RSSI: NSNumber!) {
        
//            NSLog("Device");
    }
    
    
    func bleCommunicator(_ bleCommunicator: BLECommunicator!, didDiscoverDevice deviceInfo: BLEDeviceInfo!) {
        
        handleDiscoveredDeviceList(deviceInfo, action: DisplayList.addItem)
    }
    
    func bleCommunicator(_ bleCommunicator: BLECommunicator!,didRetrievePeripherals peripherals: [AnyObject]!){
        
        //To Do
        
    }

    func bleCommunicator(_ bleCommunicator: BLECommunicator!, didConnectDevice device: CBPeripheral!)
    {
        self.ConnectionSucceeded()
        if self.navigationController?.topViewController?.isKind(of: BLEDeviceListVC.self) == true
        {
            NSLog("Moving to the serviceListView")
            self.moveToView("showServiceLIstView")
        }
        else
        {
            NotificationCenter.trigger(Constants.NotificationName.onDeviceConnected)
        }
        
        
    }
    
    
    func bleCommunicator(_ bleCommunicator:BLECommunicator!,didDisconnectDevice device: CBPeripheral! , error: NSError!)
   
    {
        if let _ =  GlobalCommunicator.shared.connectedDevice
        {
            if isReconnectionEnabled()  && isDisconnectionDueToPairingCanceled(error) == false 
            {
                
                print("start reconnection")
                startReconnection()
            }
            else
            {
                self.onDisconnection()
                ConnectionAlert(Alert:AlertType.eDisconnectedWithError, Delay: 0.5)
            }
        }
        else
        {
            //Disconnected Manually using DisconnectDevice() method
            ConnectionAlert(Alert:AlertType.eDisconnected, Delay: 0.5)
            startScanDevices()
        }
        
        removeOTAAlert()
    }
    func bleCommunicator(_ bleCommunicator:BLECommunicator!,didFailToConnectDevice device: CBPeripheral!){
        
        self.onDisconnection()
        ConnectionAlert(Alert:AlertType.eFailedToConnect, Delay: 0.5)
        
    }
//    func ConnectDdevice(row : Int)
//    {
//        GlobalCommunicator.shared.connectedDevice = bleDeviceListForTableView?.objectAtIndex(row) as? BLEDeviceInfo
//        
//        if(!handleSerialPortProfile()){
//            
//            GlobalCommunicator.connectPeripheralObject()
//        }
//        scanResultDisplay(ResultDisplay.eConnect, count: 0)
//        ConnectionAlert(Hidden: false)
//    }
//    
    func getReconnectionPeripheral() -> CBPeripheral?
    {
        let peripheralArray = GlobalCommunicator.shared.communicator.CBSManager.retrievePeripherals(withIdentifiers: [GlobalCommunicator.shared.connectedDevice.BLEPeripheral.identifier])
        let currentPeripheralUUID = GlobalCommunicator.shared.connectedDevice.BLEPeripheral.identifier
        let connectedPeripheral = peripheralArray.filter(){($0.identifier == currentPeripheralUUID)}.first
        return connectedPeripheral
        
    }
    
    
    func startReconnection()
    {
        NotificationCenter.trigger(Constants.NotificationName.onDeviceDisconned)
        ShowReconnectingAlert()
        if(!handleSerialPortProfile())
        {
            proceedWithReconnection()
        }
        
    }
    
    func removeOTAAlert(){
     
        if  GlobalCommunicator.shared.otaInterface != nil && GlobalCommunicator.shared.otaInterface?.otaAlert != nil{
            
            if GlobalCommunicator.shared.otaInterface?.otaAlert?.presentingViewController != nil{
                GlobalCommunicator.shared.otaInterface?.otaAlert?.dismiss(animated: false, completion: {
                    self.navigationController?.popToRootViewController(animated: false)
                })
                
            }
        }
    }
    
    
    func proceedWithReconnection()
    {

        if let connectedPeripheral = getReconnectionPeripheral()
        {
            GlobalCommunicator.shared.communicator.connect(connectedPeripheral , timeOut: false)
        }
        else
        {
            self.onDisconnection()
            ConnectionAlert(Alert:AlertType.eDisconnectedWithError, Delay: 0.5)
        }
    }
    
    //Work
    func isDisconnectionDueToPairingCanceled(_ error: NSError?) -> Bool
    {
        if error?.code == CBError.Code.peripheralDisconnected.rawValue
        {
            return true
        }
        return false
       /* else if error.code == CBError.ConnectionTimeout.rawValue
        {
            
        }*/
    }
    
    func isReconnectionEnabled() -> Bool
    {
        //For FindME profile and Proximity Profile and SPP
        
        if let _ = GlobalCommunicator.shared.sourceDevice
        {
            if let _ = GlobalCommunicator.shared.sourceDevice.activeProfiles
            {
                for profile in GlobalCommunicator.shared.sourceDevice.activeProfiles
                {
                    if profile == BLEProfile.FindMe || profile == BLEProfile.Proximity || profile == BLEProfile.SerialPort
                    {
                        return true
                    }
                }
            }
        }
        return false
    }


}

extension BLEDeviceListVC{
    
    /*----------------------------------------------------------------------------------------------------------*/
    // MARK: AlertView Methods
    /*----------------------------------------------------------------------------------------------------------*/
    
    func BLEStatusAlert(_ statusMessage : String)
    {
        let AlertViewC = UIAlertController(title: Constants.AlertTitle.ProductName, message: statusMessage, preferredStyle: UIAlertControllerStyle.alert)
        AlertViewC.addAction(UIAlertAction(title: Constants.AlertTitle.OKButton, style: .cancel, handler:nil))
        self.present(AlertViewC, animated: true, completion: nil)
        
    }
    
    /*func timeOutWhileReconnectingMethod()
    {
        Util.delay(Constants.TIME_OUT_DELAY)
            {
                if GlobalCommunicator.shared.connectedDevice!.BLEPeripheral.state != CBPeripheralState.Connected
                {
                    println("showScanView Delay after \(Constants.TIME_OUT_DELAY) seconds")
                    self.hideReconnectingAlert()
                    self.onDisconnection()
                }
                
        }
    }8*/
    
    func ShowReconnectingAlert()
    {
        hideReconnectingAlert()
        reconnectionAlertViewC = nil
        reconnectionAlertViewC = UIAlertController(title: "", message: Constants.AlertTitle.Reconnecting, preferredStyle: UIAlertControllerStyle.alert)
        reconnectionAlertViewC.addAction(UIAlertAction(title: Constants.AlertTitle.CancelButton, style: UIAlertActionStyle.cancel, handler:
            { action in
                
                self.showScanView()
        }))
        
        let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 60, height: 60)) as UIActivityIndicatorView
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        loadingIndicator.startAnimating();
        reconnectionAlertViewC.view.addSubview(loadingIndicator)
        self.present(reconnectionAlertViewC, animated: true, completion: nil)
    }
    
    
    func isReconnectionAlertDisplayed() -> Bool
    {
        if  reconnectionAlertViewC != nil
        {
            return true
            
        }
        return false
    }
    func hideReconnectingAlert()
    {
        if  reconnectionAlertViewC != nil
        {
            reconnectionAlertViewC.dismiss(animated: false, completion: nil)
        }
    }
    
    func showScanView()
    {
        hideReconnectingAlert()
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    func showAlertWithSettings()
    {
        let alertController = UIAlertController (title: Constants.AlertTitle.ProductName, message: Constants.AlertTitle.AllowBluetooth , preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: Constants.AlertTitle.Settings, style: .default) { (_) -> Void in
            let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
            if let url = settingsUrl {
                UIApplication.shared.openURL(url)
            }
        }
        
        let cancelAction = UIAlertAction(title: Constants.AlertTitle.CancelButton, style: .default, handler: nil)
        alertController.addAction(settingsAction)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil);
    }
}


extension BLEDeviceListVC
{
    func addListner()
    {
        NotificationCenter.listenNotification(self, notificationName: Constants.NotificationName.onServiceDiscovered)
    }
    func onServiceDiscovered()
    {
        hideReconnectingAlert()
    }
    
    

}






