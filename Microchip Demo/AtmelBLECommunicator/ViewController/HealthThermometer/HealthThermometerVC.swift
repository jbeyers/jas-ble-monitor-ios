

/* ***************************************************************************

HealthThermometerVC.swift



Copyright (c) 2015, Atmel Corporation. All rights reserved.
Released under NDA
Licensed under Atmel's Limited License Agreement.


THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Atmel Corporation: http://www.atmel.com

******************************************************************************/

import UIKit
import CoreBluetooth


class HealthThermometerVC: GATTBaseViewController  {
    

    let MAX_CELSIUS : Float = 50.0
    let MAX_FARENHEIT : Float = 120.0
    let MIN_CELSIUS : Float = 00.0
    let MIN_FARENHEIT : Float = 00.0
    let mercuryViewHeightConst : Float = 164.0
    
    @IBOutlet weak var temperatureTypeLabel: UILabel!
    @IBOutlet weak var topValueLabel: UILabel!
    @IBOutlet weak var bottomValueLabel: UILabel!
    @IBOutlet weak var temperatureValueLabel: UILabel!
    @IBOutlet weak var stopNotifyLabel: UILabel!
    @IBOutlet weak var mercuryView: UIView!
    @IBOutlet weak var notifySwitch: UISwitch!
    @IBOutlet weak var mercuryViewHeight: NSLayoutConstraint!
    @IBOutlet weak var thermoMeterImageWidth: NSLayoutConstraint!
    
    var thermoMetrInterface : BLEHealthThermometerInterface!
    
    
    
   
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.layoutFix()
        
            }

    override func viewDidLoad() {
        super.viewDidLoad()
        addListner()
        
        initThermoMeterView()
        
        if UIDevice.is_iPad()
        {
            thermoMeterImageWidth.constant = CGFloat(Constants.iPadFrame.iPadDefaultWidthSize)
        }
        // Do any additional setup after loading the view.
    }
    
    
    func initThermoMeterView()
    {
        
        initThermoMeterInterface()
        //Call after interface initilization to handle previous state of Notify Switch
        configureNotifyControl()
        configureTemperatureValueLabels()

    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
//        mercuryViewHeightConst = 164.0//Float(mercuryView.frame.size.height)
        
        

    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        removeListner()
        checkAndStopNotify()
    }
    
    func checkAndStopNotify()
    {
        if notifySwitch.isOn == true
        {
            self.thermoMetrInterface.stopTemperatureMeasurementNotifications()
        }
    }
    

    
    func configureTemperatureValueLabels()
    {
        temperatureValueLabel.text = ""
        topValueLabel.text = "50.0˚C"
        bottomValueLabel.text = "0.0˚C"
    }
    
    func configureNotifyControl()
    {
        let state = UserDefaults.getNotificationStatus(Constants.UserDefaultsKey.healthThermoMeterStatusKey)
        notifySwitch.isOn = state
        onNotifyChanged(notifySwitch)
    }
    
    func initThermoMeterInterface()
    {
        self.navigationItem.title = GlobalCommunicator.shared.selectedService?.uuid.getServiceName()
        self.thermoMetrInterface=BLEHealthThermometerInterface(sourceDevice: GlobalCommunicator.shared.sourceDevice)
        self.thermoMetrInterface.delegate=self;
        self.thermoMetrInterface.readTemperatureType()
    }

    @IBAction func onNotifyChanged(_ sender: UISwitch)
    {
        if sender.isOn == true
        {
            stopNotifyLabel.text = Constants.ButtonTitle.STOP_NOTIFY
            self.thermoMetrInterface.startTemperatureMeasurementNotifications()
            
        }
        else
        {
            stopNotifyLabel.text = Constants.ButtonTitle.START_NOTIFY
            self.thermoMetrInterface.stopTemperatureMeasurementNotifications()
        }
         UserDefaults.updateNotifyState(sender.isOn , UserDefaultsKey: Constants.UserDefaultsKey.healthThermoMeterStatusKey)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func updateTemperature(_ valueMeasurement:Float,currentUnit unit:TemperatureUnit)
    {
        switch(unit)
        {
        case TemperatureUnit.celsius:
            

            topValueLabel.text = "\(MAX_CELSIUS)˚C"
            bottomValueLabel.text = "\(MIN_CELSIUS)˚C"

            temperatureValueLabel.text = "\(valueMeasurement)˚C"
            mercuryViewHeight.constant = CGFloat(self.getMercuryLevel(valueMeasurement, Min: self.MIN_CELSIUS, Max: self.MAX_CELSIUS) )
            
            
            /*UIView.animateWithDuration(0.3, animations: { () -> Void in
                
                self.mercuryViewHeight.constant = CGFloat(self.getMercuryLevel(valueMeasurement, Min: self.MIN_CELSIUS, Max: self.MAX_CELSIUS) )
                self.view.layoutIfNeeded()
                
                }) { (complete) -> Void in
                    self.temperatureValueLabel.text = "\(valueMeasurement)˚C"
                    println("\(valueMeasurement ) ˚C")
                    
            }*/


        case TemperatureUnit.farenheit:
            
            topValueLabel.text = "\(MAX_FARENHEIT)˚F"
            bottomValueLabel.text = "\(MIN_FARENHEIT)˚F"
            
            temperatureValueLabel.text = "\(valueMeasurement)˚F"
            mercuryViewHeight.constant = CGFloat(self.getMercuryLevel(valueMeasurement, Min: self.MIN_FARENHEIT, Max: self.MAX_FARENHEIT))
            
            
            /*UIView.animateWithDuration(0.3, animations: { () -> Void in
                
                self.mercuryViewHeight.constant = CGFloat(self.getMercuryLevel(valueMeasurement, Min: self.MIN_FARENHEIT, Max: self.MAX_FARENHEIT))
                self.view.layoutIfNeeded()
                
                }) { (complete) -> Void in
                    self.temperatureValueLabel.text = "\(valueMeasurement)˚F"
                    println("\(valueMeasurement ) ˚F")
                    
            }*/
            
  
        }
    }
    
    func getMercuryLevel(_ temperatureValue : Float ,Min :Float , Max : Float ) -> Float
    {
       var value = ((mercuryViewHeightConst / Max) * temperatureValue)
        if value > Max
        {
            value = Max
        }
        return value
    }

    func updateTemperatureType(_ type:String)
    {
        if notifySwitch.isOn == true
        {
            temperatureTypeLabel.text = type
        }
    }
}


/*----------------------------------------------------------------------------------------------------------*/
// MARK: BLEHealthThermometerInterfaceDelegate Methods
/*----------------------------------------------------------------------------------------------------------*/


extension HealthThermometerVC :BLEHealthThermometerInterfaceDelegate
{
    func bleHealthThermometerInterfaceDidReadTemperatureMeasurements(_ valueMeasurement: Float, currentUnit unit: TemperatureUnit, TemperatureType type: String?)
    {
        
        print("Temp === \(valueMeasurement) :: Unit == \(unit.rawValue)")
        updateTemperature(valueMeasurement, currentUnit: unit)
        if type != nil
        {
         updateTemperatureType(type!)
        }
    }

    func bleHealthThermometerInterfaceDidReadTemperatureType(_ type:String)
    {
//        updateTemperatureType(type)
    }
   
    func bleHealthThermometerInterfaceDidReadDate(_ date:BLETime?)
    {
        
    }
}

/*----------------------------------------------------------------------------------------------------------*/
// MARK: NSNotificationCenter Methods
/*----------------------------------------------------------------------------------------------------------*/


extension HealthThermometerVC
{
    
    func addListner()
    {
        NotificationCenter.listenNotification(self, notificationName: Constants.NotificationName.onDeviceDisconned)
        NotificationCenter.listenNotification(self, notificationName: Constants.NotificationName.onServiceDiscovered)
        
        NotificationCenter.listenNotification(self, notificationName: Constants.NotificationName.onEnterBackground)
        NotificationCenter.listenNotification(self, notificationName: Constants.NotificationName.onEnterForeground)
    }
    
    func removeListner()
    {
        NotificationCenter.removelistener(self, notificationName: Constants.NotificationName.onDeviceDisconned)
        NotificationCenter.removelistener(self, notificationName: Constants.NotificationName.onServiceDiscovered)
        NotificationCenter.removelistener(self, notificationName: Constants.NotificationName.onEnterBackground)
        NotificationCenter.removelistener(self, notificationName: Constants.NotificationName.onEnterForeground)
    }
    
    func onEnterBackground()
    {
        checkAndStopNotify()
    }
    
    func onEnterForeground()
    {
        configureNotifyControl()
    }
    
    
    
    func onDeviceDisconned()
    {
        
    }
    
    func onServiceDiscovered()
    {
        initThermoMeterView()
    }
    
    
    
    
    
    
}
