
/* ***************************************************************************

AExtensions.swift

Extensions add new functionality to an existing class,



Copyright (c) 2015, Atmel Corporation. All rights reserved.
Released under NDA
Licensed under Atmel's Limited License Agreement.


THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Atmel Corporation: http://www.atmel.com

******************************************************************************/

import Foundation
import UIKit

import CoreBluetooth
import AVFoundation




/*----------------------------------------------------------------------------------------------------------*/
// MARK: extension UIColor
/*----------------------------------------------------------------------------------------------------------*/


extension UIColor {
    
    
    
    
    class func AtmelRedColor() -> UIColor {
        return UIColor(red: 238.0/255.0, green: 42.0/255.0, blue: 36.0/255.0, alpha: 1.0)
    }
    
    class func AtmelDarkRedColor() -> UIColor {
        return UIColor(red: 200.0/255.0, green: 35.0/255.0, blue: 30.0/255.0, alpha: 1.0)
    }
    
    class func AtmelDarkGrayColor() -> UIColor {
        return UIColor(red: 109.0/255.0, green: 110.0/255.0, blue: 112.0/255.0, alpha: 1.0)
    }
    
    class func AtmelLightGrayColor() -> UIColor {
        return  UIColor(red: 229.0/255.0, green: 229.0/255.0, blue: 229.0/255.0, alpha: 1.0)
    }

    
    
    class func AtmelBlueColor() -> UIColor {
        return UIColor(red: 0.0/255.0, green: 121.0/255.0, blue: 193.0/255.0, alpha: 1.0)
    }
    
    class func AtmelDarkBlueColor() -> UIColor {
        return UIColor(red: 0.0/255.0, green: 100.0/255.0, blue: 160.0/255.0, alpha: 1.0)
    }
    
    class func AtmelBrightBlueColor() -> UIColor {
        return UIColor(red: 11.0/255.0, green: 118.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    }
    
    
    
    class func AtmelGreenColor() -> UIColor {
        return UIColor(red: 123.0/255.0, green: 184.0/255.0, blue: 30.0/255.0, alpha: 1.0)
    }
    class func AtmelOrangeColor() -> UIColor {
        return UIColor(red: 255.0/255.0, green: 131.0/255.0, blue: 0.0/255.0, alpha: 1.0)
    }
    
    
    class func colorWithHexString (_ hex:String) -> UIColor {
        //var cString:String = hex.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()).uppercased()
        var cString:String = hex.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString = cString.substring(from: cString.characters.index(cString.startIndex, offsetBy: 1))
        }
        
        if (cString.characters.count != 6) {
            return UIColor.gray
        }
        
        let rString = cString.substring(to: cString.characters.index(cString.startIndex, offsetBy: 2))
        let gString = cString.substring(from: cString.characters.index(cString.startIndex, offsetBy: 2)).substring(to: cString.characters.index(cString.startIndex, offsetBy: 2))
        let bString = cString.substring(from: cString.characters.index(cString.startIndex, offsetBy: 4)).substring(to: cString.characters.index(cString.startIndex, offsetBy: 2))
        
        var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
   
        Scanner(string: rString).scanHexInt32(&r)
        Scanner(string: gString).scanHexInt32(&g)
        Scanner(string: bString).scanHexInt32(&b)

        
        return UIColor(red:CGFloat(r)/CGFloat(255.0), green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(1))
    }
}

/*----------------------------------------------------------------------------------------------------------*/
// MARK: extension UISegmentedControl
/*----------------------------------------------------------------------------------------------------------*/

extension UISegmentedControl
{
    
    func roundRectView(_ cornerRad : CGFloat = 12)
    {
        self.layer.cornerRadius = self.frame.size.height / 2.0
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.masksToBounds = true
        self.backgroundColor = UIColor.AtmelLightGrayColor()
    }
    
    
    
    func firstSegment() -> UIView?{
        
        let firstView = self.subviews.filter(){$0.frame.origin.x == 0}.first
        return firstView
    }
    
    func secondSegment() -> UIView?{
        
        let secondView = self.subviews.filter(){$0.frame.origin.x == ($0.frame.width + 1)}.first

        return secondView
    }
    
    func thirdSegment() -> UIView?{
        
        let thirdView = self.subviews.filter(){$0.frame.origin.x == (($0.frame.width * 2) + 2)}.first
        return thirdView
    }
    
}


/*----------------------------------------------------------------------------------------------------------*/
// MARK: extension AVAudioPlayer
/*----------------------------------------------------------------------------------------------------------*/

extension AVAudioPlayer
{
    
    //convenience init(fileName : String, withFormat type : String)
    //{
     //   let alertSound = URL(fileURLWithPath: Bundle.main.path(forResource: fileName, ofType: type)!)
     //
     //   do{try self.init(contentsOf: alertSound)}catch{}
    //}
    func playSound()
    {
        do {
            // Removed deprecated use of AVAudioSessionDelegate protocol
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
        } catch _ {
        }
        do {
            try AVAudioSession.sharedInstance().setActive(true)
        } catch _ {
        }
        
        self.prepareToPlay()
        self.play()
    }
}



/*----------------------------------------------------------------------------------------------------------*/
// MARK: UIView extension for Animation Category
/*----------------------------------------------------------------------------------------------------------*/

extension UIView {
    
    func fadeOut(_ duration: TimeInterval = 0.5, delay: TimeInterval = 0.0 )
    {
        UIView.animate(withDuration: duration, delay: delay, options: UIViewAnimationOptions.curveEaseIn, animations: { () -> Void in
            self.alpha = 0.0
            }) { (Bool) -> Void in
                
        }
    }
    
}


extension UIViewController{
    
    func layoutFix(){
        
        if(UIDevice.is_iPad()){
            if UIScreen.main.bounds.width != self.view.frame.size.width{
                
                let navigationItemheight:CGFloat = UIScreen.main.bounds.width - self.view.bounds.height
                
                NSLog("Screen height,viewheight=\(UIScreen.main.bounds.height),\(self.view.frame.size.height)   Screen width,view width = \(UIScreen.main.bounds.width),\(self.view.frame.size.width)" )
                self.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - navigationItemheight)
            }
            
        }
        
        self.view.layoutSubviews()

        
    }
}
/*----------------------------------------------------------------------------------------------------------*/
// MARK: UIDevice extension for Animation Category
/*----------------------------------------------------------------------------------------------------------*/

extension UIDevice
{
    class func is_iPad()->Bool
    {
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            return true
        }
        return false
    }
    
    class func is_Landscape()->Bool
    {
        if UIDevice.current.orientation ==  UIDeviceOrientation.landscapeLeft ||
            UIDevice.current.orientation == UIDeviceOrientation.landscapeRight
        {
            return true
        }
        return false
    }
}


/*----------------------------------------------------------------------------------------------------------*/
// MARK: NSNotificationCenter extension for Animation Category
/*----------------------------------------------------------------------------------------------------------*/
extension CGFloat
{
    static func getFontSizeToScreen(_ value:CGFloat, scale:CGFloat)->CGFloat{
        if !(UIDevice.is_iPad())
        {return value}
        let screenWidth:CGFloat = UIScreen.main.bounds.size.width
//        let screenHeight = UIScreen.mainScreen().bounds.size.height
        
        let benchMarkWidth:CGFloat = 320.0
        
        let fontMultiplier = (screenWidth / benchMarkWidth)
        
        return (value*fontMultiplier*scale);
        
    }
}

extension NotificationCenter
{

    class func trigger(_ notificationName : String)
    {
        self.default.post(name: Notification.Name(rawValue: notificationName), object: nil)
    }
    
    
    class func listenNotification(_ observer: AnyObject, notificationName: String? )
    {
        self.default.addObserver(observer, selector: Selector(notificationName!), name: notificationName.map { NSNotification.Name(rawValue: $0) }, object: nil)
    }
    
    class func removelistener(_ observer: AnyObject, notificationName: String? )
    {
        self.default.removeObserver(observer, name: notificationName.map { NSNotification.Name(rawValue: $0) }, object: nil)
    }
    
    

}

/*----------------------------------------------------------------------------------------------------------*/
// MARK: extension NSUserDefaults
/*----------------------------------------------------------------------------------------------------------*/


extension UserDefaults
{
    class func getNotificationStatus(_ UserDefaultsKey : String) -> Bool
    {
        return UserDefaults.standard.bool(forKey: UserDefaultsKey)
    }
    
    class func updateNotifyState(_ state : Bool , UserDefaultsKey : String)
    {
        UserDefaults.standard.set(state, forKey: UserDefaultsKey)
    }
}

/*extension Data {         **** Its Me *****
    
    var hexString : String {
        let buf = UnsafePointer<UInt8>(bytes)
        let charA = UInt8(UnicodeScalar("a").value)
        let char0 = UInt8(UnicodeScalar("0").value)
        
        func itoh(_ i: UInt8) -> UInt8 {
            return (i > 9) ? (charA + i - 10) : (char0 + i)
        }
        
        let p = UnsafeMutablePointer<UInt8>.allocate(capacity: count * 2)
        
        for i in 0..<count {
            p[i*2] = itoh((buf[i] >> 4) & 0xF)
            p[i*2+1] = itoh(buf[i] & 0xF)
        }
        
        return NSString(bytesNoCopy: p, length: count*2, encoding: String.Encoding.utf8.rawValue, freeWhenDone: true)! as String
    }
}*/

extension Data {
    
    var hexString : String {
        
        let charA = UInt8(UnicodeScalar("a").value)
        let char0 = UInt8(UnicodeScalar("0").value)
        
        func itoh(_ i: UInt8) -> UInt8 {
            return (i > 9) ? (charA + i - 10) : (char0 + i)
        }
        
        let p = UnsafeMutablePointer<UInt8>.allocate(capacity: count * 2)
        withUnsafeBytes { (buf: UnsafePointer<UInt8>) in
            for i in 0..<count {
                p[i*2] = itoh((buf[i] >> 4) & 0xF)
                p[i*2+1] = itoh(buf[i] & 0xF)
            }
        }
        return String(bytesNoCopy: p, length: count * 2, encoding: .utf8, freeWhenDone: true)!
    }
}

extension String
{
    //For Test
    static func log(_ str : String)
    {
        var newLog = "=====================\(str)\n"
        print(newLog)
        if let prevLog = UserDefaults.standard.object(forKey: "bbnb") as? String
        {
            newLog = "\(prevLog)\\n\(newLog)"
        }
        UserDefaults.standard.set(newLog, forKey: "bbnb")
    }
    

        static func length(_ str : String) ->  Int
            { return str.characters.count }  // Swift 1.2
    
}

/*----------------------------------------------------------------------------------------------------------*/
// MARK: extension String
/*----------------------------------------------------------------------------------------------------------*/



