/* ***************************************************************************

BLEDevicelCollectionCell.swift




Copyright (c) 2015, Atmel Corporation. All rights reserved.
Released under NDA
Licensed under Atmel's Limited License Agreement.


THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Atmel Corporation: http://www.atmel.com

******************************************************************************/


import UIKit

class BLEDevicelCollectionCell: UICollectionViewCell  {
    
    @IBOutlet var c_deviceName: UILabel!
    @IBOutlet var c_deviceAddress: UILabel!
    @IBOutlet var c_deviceRSSI: UILabel!
    @IBOutlet var c_deviceThumbImageView: UIImageView!
    @IBOutlet var c_signalStrengthView: UIImageView!
    
    var signalImages:[UIImage]!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
         setSignalImages()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
         setSignalImages()
    }
    


    func setCollectionCell(_ bleDeviceData : BLEDeviceInfo!)
    {
        //To get RSSI updates
        
        var nameString = bleDeviceData.BLEPeripheralLocalName // First check for Advert data for peripheral name
        if nameString == nil || nameString == "" {
            nameString = "N/A"
        }
        
        self.c_deviceName.text = nameString
        if let serviceUUIDs = bleDeviceData.BLEPeripheralServices
        {
            self.c_deviceAddress.text = ""//bleDeviceData.BLEPeripheral.identifier.UUIDString
            //            Replaced Address label to Service Count
            var service = "Service"
            if serviceUUIDs.count > 1
            {
                service = "Services"
            }
            self.c_deviceAddress.text = "\(serviceUUIDs.count) \(service)"
            
            
        }
        
        updateSignalImage(bleDeviceData.BLEPeripheralRSSI)
        
    }
    
    func setSignalImages()
    {
//        self.c_deviceThumbImageView.image = UIImage(named: "BLEDeviceThumb_iPad")

        if self.signalImages == nil
        {
        self.signalImages = [UIImage](arrayLiteral: UIImage(named: "SignalOne")!,
            UIImage(named: "SignalTwo")!,
            UIImage(named: "SignalThree")!,
            UIImage(named: "SignalFour")!,
            UIImage(named: "SignalFive")!)
        }
    }
    
    func updateSignalImage(_ RSSI:NSNumber)
    {
        let rssiString = RSSI.stringValue
        c_signalStrengthView.image = signalImages[BLEDeviceCell.getRSSIsignalIndex(RSSI)]
        c_deviceRSSI.text = "\(rssiString)"+" dBm"
    }
}
