
/* ***************************************************************************

SerialPortVC.swift



Copyright (c) 2015, Atmel Corporation. All rights reserved.
Released under NDA
Licensed under Atmel's Limited License Agreement.


THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Atmel Corporation: http://www.atmel.com

******************************************************************************/

import UIKit

let MESSAGE_CHAR_LIMIT = 150
let SPP_PLACEHOLDER = "Type New Text"

class SerialPortVC: JSQMessagesViewController {

//    var serialPortInterface:BLESerialPortInterface?
    

    
    
    var avatars = Dictionary<String, UIImage>()
    var tempMssageList = [Message]()
    
    let mobileSender    = "Application"
    let deviceSender    = "Device"
    var outgoingBubbleImageView = JSQMessagesBubbleImageFactory.outgoingMessageBubbleImageView(with: UIColor.AtmelRedColor())
    var incomingBubbleImageView = JSQMessagesBubbleImageFactory.incomingMessageBubbleImageView(with: UIColor.white)

    
    
   
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.layoutFix()
        
            }

    override func viewDidLoad() {
        super.viewDidLoad()
        initSPP()
        initChatView()
        
        addListner()
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        collectionView!.collectionViewLayout.springinessEnabled = true
        inputToolbar!.contentView!.textView!.becomeFirstResponder()
        
        

        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeListner()
    }

    

    func initSPP()
    {
        self.navigationItem.title = GlobalCommunicator.shared.selectedService?.uuid.getServiceName()

        GlobalCommunicator.shared.serialPortInterface.set(
            GlobalCommunicator.shared.sourceDevice, delgateObject: self)
        
//        GlobalCommunicator.shared.serialPortInterface.startMessageBoxWithCharLimit(MESSAGE_CHAR_LIMIT)

        checkAfterDelay()

//        remainingCharLabel.text = "\(MESSAGE_CHAR_LIMIT-(sendTextView.text as NSString).length)"
        


    }

}

extension SerialPortVC:BLESerialPortInterfaceDelegate{
    
    func bleSerialPortDidReceiveMessageFromSender(_ message:String?)
    {
        
        if message != nil
        {
            showIncomingMessage(message)
        }
    }
    func blePeripheralReadyForCommunication(){
//        sendButton.enabled = true
    }
    func blePeripheralNotReadyForComunication(){
//        sendButton.enabled = false

    }
    func blePeripheralMessageSuccessfullySent(){
//        sendTextView.text = ""
    }
    func blePeripheralMessageSendingFailed(){
        
    }
}

/*----------------------------------------------------------------------------------------------------------*/
// MARK: extension 
/*----------------------------------------------------------------------------------------------------------*/

extension SerialPortVC
{
    
    func initChatView()
    {
        inputToolbar!.contentView!.leftBarButtonItem = nil
        automaticallyScrollsToMostRecentMessage = true
        
        sender = mobileSender
        setupAvatarImage(mobileSender, imageUrl: "mobileUser", incoming: false)
        setupAvatarImage(deviceSender, imageUrl: "deviceUser", incoming: true)
        
        inputToolbar!.contentView!.textView!.placeHolder = SPP_PLACEHOLDER
        collectionView!.backgroundColor = UIColor.AtmelLightGrayColor()
        

    }
    
    func getTrimmedString(_ text : String , StartingIndex : Int = 0 ) ->String
    {
        let myNSString = text as NSString
        if String.length(text) > ((StartingIndex+1)*MESSAGE_CHAR_LIMIT)
        {
            return myNSString.substring(with: NSMakeRange(StartingIndex*MESSAGE_CHAR_LIMIT, MESSAGE_CHAR_LIMIT))
        }
        let left =  String.length(text) - (StartingIndex*MESSAGE_CHAR_LIMIT) 
        return myNSString.substring(with: NSMakeRange(StartingIndex*MESSAGE_CHAR_LIMIT, left))
    }
    

    func sendMessageFromTempList()
    {
        if let tempMessage = tempMssageList.first
        {
            tempMssageList.remove(at: 0)
            GlobalCommunicator.shared.serialPortInterface.sendMessage(tempMessage.text_)
            Util.delay(0.2)
            {
                    self.sendMessageFromTempList()
            }
        }
    }
    
    func sendMessage(_ text: String!, sender: String? = "Application")
    {
        let limit = String.length(text)/MESSAGE_CHAR_LIMIT
        for index in 0...limit
        {
            let trimmedMessage = getTrimmedString(text,StartingIndex: index)
//            GlobalCommunicator.shared.serialPortInterface.sendMessage(trimmedMessage)
            let message = Message(text: trimmedMessage, sender: sender, imageUrl: nil)
            tempMssageList.append(message)
            if String.length(trimmedMessage) < MESSAGE_CHAR_LIMIT
            {
                break
            }
        }


        sendMessageFromTempList()
        let message = Message(text: text, sender: sender, imageUrl: nil)
        GlobalCommunicator.shared.sppMessages.append(message)
        finishSendingMessage()
        
    }
    
    func showIncomingMessage(_ text: String!)
    {
        print("Show message on view and append = \(text)")
        var tempMessage = ""
        
        if inputToolbar!.contentView!.textView!.hasText()
        {
            tempMessage = inputToolbar!.contentView!.textView!.text
        }
        
        finishSendingMessage()
        
        if tempMessage.isEmpty == false
        {
            inputToolbar!.contentView!.textView!.text = tempMessage
            inputToolbar!.toggleSendButtonEnabled()
        }
    }

    
    
    
    
    
    
    func setupAvatarImage(_ name: String, imageUrl: String?, incoming: Bool) {
        if let _ = imageUrl {
            let image = UIImage(named: imageUrl!)
            let diameter = incoming ? UInt(collectionView!.collectionViewLayout.incomingAvatarViewSize.width) : UInt(collectionView!.collectionViewLayout.outgoingAvatarViewSize.width)
            let avatarImage = JSQMessagesAvatarFactory.avatar(with: image, diameter: diameter)
            avatars[name] = avatarImage
            return
            
        }
        
        // At some point, we failed at getting the image (probably broken URL), so default to avatarColor
        setupAvatarColor(name, incoming: incoming)
    }
    
    func setupAvatarColor(_ name: String, incoming: Bool) {
        let diameter = incoming ? UInt(collectionView!.collectionViewLayout.incomingAvatarViewSize.width) : UInt(collectionView!.collectionViewLayout.outgoingAvatarViewSize.width)
        
        let rgbValue = name.hash
        let r = CGFloat(Float((rgbValue & 0xFF0000) >> 16)/255.0)
        let g = CGFloat(Float((rgbValue & 0xFF00) >> 8)/255.0)
        let b = CGFloat(Float(rgbValue & 0xFF)/255.0)
        let color = UIColor(red: r, green: g, blue: b, alpha: 0.5)
        
        let nameLength = name.characters.count
        let initials : String? = name.substring(to: sender.index(sender.startIndex, offsetBy: min(3, nameLength)))
        let userImage = JSQMessagesAvatarFactory.avatar(withUserInitials: initials, backgroundColor: color, textColor: UIColor.black, font: UIFont.systemFont(ofSize: CGFloat(13)), diameter: diameter)
        
        avatars[name] = userImage
    }
    
}

extension SerialPortVC
{
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, sender: String!, date: Date!) {
        sendMessage(text, sender: sender)
        
    }
    
    override func didPressAccessoryButton(_ sender: UIButton!) {
        print("Camera pressed!")
    }
    
    
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return GlobalCommunicator.shared.sppMessages[indexPath.item]
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, bubbleImageViewForItemAt indexPath: IndexPath!) -> UIImageView! {
        let message = GlobalCommunicator.shared.sppMessages[indexPath.item]
        
        if message.sender() == sender {
            return UIImageView(image: outgoingBubbleImageView!.image, highlightedImage: outgoingBubbleImageView!.highlightedImage)
        }
        
        return UIImageView(image: incomingBubbleImageView!.image, highlightedImage: incomingBubbleImageView!.highlightedImage)
    }
    
    
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageViewForItemAt indexPath: IndexPath!) -> UIImageView! {
        let message = GlobalCommunicator.shared.sppMessages[indexPath.item]
        if let avatar = avatars[message.sender()] {
            return UIImageView(image: avatar)
        } else {
            setupAvatarImage(message.sender(), imageUrl: message.imageUrl(), incoming: true)
            return UIImageView(image:avatars[message.sender()])
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return GlobalCommunicator.shared.sppMessages.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        
        let message = GlobalCommunicator.shared.sppMessages[indexPath.item]
        if message.sender() == sender {
            cell.textView!.textColor = UIColor.white
        } else {
            cell.textView!.textColor = UIColor.black
        }
        
        let attributes : [String:AnyObject] = [NSForegroundColorAttributeName:cell.textView!.textColor!, NSUnderlineStyleAttributeName: 1 as AnyObject]
        cell.textView!.linkTextAttributes = attributes
        
        return cell
    }
    
    
    // View  usernames above bubbles
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        let message = GlobalCommunicator.shared.sppMessages[indexPath.item];
        
        // Sent by me, skip
        if message.sender() == sender {
            return nil;
        }
        
        // Same as previous sender, skip
        if indexPath.item > 0 {
            let previousMessage = GlobalCommunicator.shared.sppMessages[indexPath.item - 1];
            if previousMessage.sender() == message.sender() {
                return nil;
            }
        }
        
        return NSAttributedString(string:message.sender())
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat {
        let message = GlobalCommunicator.shared.sppMessages[indexPath.item]
        
        // Sent by me, skip
        if message.sender() == sender {
            return CGFloat(0.0);
        }
        
        // Same as previous sender, skip
        if indexPath.item > 0 {
            let previousMessage = GlobalCommunicator.shared.sppMessages[indexPath.item - 1];
            if previousMessage.sender() == message.sender() {
                return CGFloat(0.0);
            }
        }
        
        return kJSQMessagesCollectionViewCellLabelHeightDefault
    }
    

}

extension SerialPortVC
{
    func addListner()
    {
        NotificationCenter.listenNotification(self, notificationName: Constants.NotificationName.onDeviceDisconned)
        NotificationCenter.listenNotification(self, notificationName: Constants.NotificationName.onServiceDiscovered)
        NotificationCenter.listenNotification(self, notificationName: Constants.NotificationName.onEnterForeground)
        
    }
    
    func removeListner()
    {
        NotificationCenter.removelistener(self, notificationName: Constants.NotificationName.onDeviceDisconned)
        NotificationCenter.removelistener(self, notificationName: Constants.NotificationName.onServiceDiscovered)
        
        NotificationCenter.removelistener(self, notificationName: Constants.NotificationName.onEnterForeground)
    }
    
    func onEnterForeground()
    {
         GlobalCommunicator.shared.serialPortInterface.startMessageBoxWithCharLimit(MESSAGE_CHAR_LIMIT)
    }
    
    
    func onDeviceDisconned()
    {
        print("Stopping Notifications.....")
        GlobalCommunicator.shared.serialPortInterface.stopEndPointNotifications()
    }
    
    func onServiceDiscovered()
    {
        initSPP()
      
    }
    
    
    func checkAfterDelay()
    {
        GlobalCommunicator.shared.serialPortInterface.checkReceiverStatus()
    }
    
}

