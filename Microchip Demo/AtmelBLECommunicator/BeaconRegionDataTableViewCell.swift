/***************************************************************************
 
 BeaconRegionDataTableViewCell.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import UIKit


protocol BeaconRegionDataTableViewCellDelegate {
    
    func deleteRegionAtIndex(_ index: Int)
}



class BeaconRegionDataTableViewCell: UITableViewCell {

    
    var beaconRegion:AtmelBeaconRegion!{
        didSet{
            updateCellWithBeaconRegion()
        }
    }
    var delegate : BeaconRegionDataTableViewCellDelegate?
    
    @IBOutlet weak var uuidHeaderLabel: UILabel!
    @IBOutlet weak var id2HeaderLabel: UILabel!
    @IBOutlet weak var id3HeaderLabel: UILabel!
    
    
    @IBOutlet weak var isRangingSwitch: UISwitch!
    @IBOutlet weak var beaconIconImage: UIImageView!
    
    @IBOutlet weak var id3Label: UILabel!
    @IBOutlet weak var id2Label: UILabel!
    @IBOutlet weak var uuidLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var deleteButton : UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func updateCellWithBeaconRegion(){
        
        switch(beaconRegion.beaconType!){
        case BeaconItemType.iBeacon:
            uuidHeaderLabel.text = "UUID :"
            id2HeaderLabel.text =  "Major :"
            id3HeaderLabel.text =  "Minor :"
            beaconIconImage.image = UIImage(named: "iBeaconImage")
            
            if let iBeaconRegion = beaconRegion as? AtmeliBeaconRegion{
                
                nameLabel.text = iBeaconRegion.name
                uuidLabel.text = iBeaconRegion.uuid.uuidString
                
                if let major = iBeaconRegion.major
                    {id2Label.text = "\(major)"}
                else
                    {id2Label.text = "Wildcarded(*)"}
                
                if let minor = iBeaconRegion.minor
                    {id3Label.text = "\(minor)"}
                else
                    {id3Label.text = "Wildcarded(*)"}
                
                isRangingSwitch.isOn = iBeaconRegion.isRanging
            }
            
            
            
            break
            
        case BeaconItemType.AltBeacon:
            uuidHeaderLabel.text = "UUID :"
            id2HeaderLabel.text =  "ID1 :"
            id3HeaderLabel.text =  "ID2 :"
            beaconIconImage.image = UIImage(named: "AltBeaconImage")
            
            if let altBeaconRegion = beaconRegion as? AtmelAltBeaconRegion{
                
                nameLabel.text = altBeaconRegion.name
                
                uuidLabel.text = "\(altBeaconRegion.id1!.uuidString)"
                
                if let id2 = altBeaconRegion.id2
                    {id2Label.text = "\(id2)"}
                else
                    {id2Label.text = "Wildcarded(*)"}
                
                if let id3 = altBeaconRegion.id3
                    {id3Label.text = "\(id3)"}
                else
                    {id3Label.text = "Wildcarded(*)"}
                
                isRangingSwitch.isOn = beaconRegion.isRanging
            }
            break
        }
        
    }

    @IBAction func changedValue(_ sender: UISwitch) {
        if beaconRegion.isRanging != sender.isOn{
            beaconRegion.isRanging = sender.isOn
            BeaconRanger.saveBeaconRegion(beaconRegion)
        }
        
    }
    
    
    
    @IBAction func deleteButtonClicked(_ sender : UIButton){
        
        let alertVC = UIAlertController(title: "ALERT", message: "Do you want to delete this region?", preferredStyle: .alert)
        let deleteAction  = UIAlertAction(title: "Yes", style: .default) { (action) in
            
            if  self.delegate != nil {
                self.delegate?.deleteRegionAtIndex(sender.tag)
            }
        }
        
        let cancelAction = UIAlertAction(title: "No", style: .cancel, handler: nil)
        alertVC.addAction(deleteAction)
        alertVC.addAction(cancelAction)
        self.parentViewController?.present(alertVC, animated: true, completion: nil)
    }

}
