/***************************************************************************
 
 BeaconListViewController.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import UIKit

enum ItemType:String{
    case Region = "Region"
    case Beacon = "Beacon"
}

enum ActionType : Int{
    case add
    case update
}

let BeaconCellIdentifier = "BeaconDataCell"
let RegionCellIdentifier = "RegionDataCell"
let TO_ADDVC_SEGUE = "AddItemSegue"
let TO_BEACON_UPDATE_SEGUE = "toBeaconUpdateSegue"
let BeaconListRowHeight:CGFloat = 170
let RegionListRowHeight:CGFloat = 130

class BeaconListViewController: BaseViewController {

    
    var listViewType:ItemType!
    var listItems:[AnyObject]?
    var rowHeight:CGFloat!
    var selectedIndex = -1
    var selectedItemType:BeaconItemType!
    var selectedActionType : ActionType?
    var beaconSelectionVC : BeaconSelectionViewController?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var beaconRangeTableView: UITableView!
    @IBOutlet weak var addButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        beaconSelectionVC = self.storyboard?.instantiateViewController(withIdentifier: "beaconSelectVC") as? BeaconSelectionViewController
        beaconSelectionVC?.parentVC = self
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureViewAndData()
        configureNavBar()
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func unwindToBeaconListViewController(_ segue: UIStoryboardSegue) {
    }
  
    func configureViewAndData(){
        
        switch(listViewType!){
            
        case ItemType.Beacon:
            
            listItems = BeaconRanger.getSavedBeaconItemList()
            rowHeight = BeaconListRowHeight
            break
        case ItemType.Region:
            
            listItems = BeaconRanger.getSavedBeaconRegionList()
            rowHeight = RegionListRowHeight
            break
        }
        
        beaconRangeTableView?.reloadData()

    }
    
    
    func configureNavBar(){
        
        if listViewType! == ItemType.Beacon{
            self.navigationItem.title = "Beacons List"
        }
        else{
            self.navigationItem.title = "Region Monitoring List"
        }
        
        let addImageView = UIImageView(image: UIImage(named: "add_icon"))
        let customRightBarButton = UIButton(frame: addImageView.frame)
        customRightBarButton.setImage(UIImage(named: "add_icon"), for: UIControlState())
        customRightBarButton.addTarget(self, action: #selector(BeaconListViewController.addButtonAction(_:)), for: .touchUpInside)
        let barButtonItem = UIBarButtonItem(customView: customRightBarButton)
        self.navigationItem.rightBarButtonItem = barButtonItem

    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        if segue.identifier == TO_BEACON_UPDATE_SEGUE{
            
            if selectedActionType == .add{
                
                beaconSelectionVC!.view.removeFromSuperview()
                
                let destinationVC = segue.destination as! BeaconUpdateViewController
                destinationVC.changeType = ItemListChangeType.add
                destinationVC.changingItemType = beaconSelectionVC!.addingItemType
                destinationVC.beaconItemType = beaconSelectionVC!.selectedBeaconItemType
                
            }
            else {
                
                let destinationVC = segue.destination as! BeaconUpdateViewController
                destinationVC.changeType = ItemListChangeType.update
                destinationVC.changingItemType = listViewType
                destinationVC.beaconItemType = selectedItemType
                destinationVC.updatedItem = listItems![selectedIndex]
            }
        }
    }
    
    func addButtonAction(_ sender: UIButton) {
        
        selectedActionType = .add
        
        if beaconSelectionVC != nil{
            
            beaconSelectionVC!.addingItemType = listViewType
            self.view.addSubview(beaconSelectionVC!.view)
        }
    }

}
extension BeaconListViewController:UITableViewDataSource{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
       
        if  listItems != nil
        {return listItems!.count}
        
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return 1
    }
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        switch(listViewType!){
            
        case ItemType.Beacon:
            if let beaconData = listItems![indexPath.section+indexPath.row] as? AtmelBeaconItem{
                let cell = tableView.dequeueReusableCell(withIdentifier: BeaconCellIdentifier) as! BeaconDataTableViewCell
                cell.beaconItem = beaconData
                cell.delegate = self
                cell.deleteButton.tag = indexPath.section
                return cell
            }
            break
        case ItemType.Region:
            if let regionData = listItems![indexPath.section+indexPath.row] as? AtmelBeaconRegion{
                let cell = tableView.dequeueReusableCell(withIdentifier: RegionCellIdentifier) as! BeaconRegionDataTableViewCell
                cell.beaconRegion = regionData
                cell.delegate = self
                cell.deleteButton.tag = indexPath.section
                return cell
            }
            break
        }
        return UITableViewCell()
    }
}
extension BeaconListViewController:UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView()
        return footerView
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return rowHeight
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        tableView.deselectRow(at: indexPath, animated: true)
        selectedIndex = indexPath.section+indexPath.row
        
        switch(listViewType!){
            
        case ItemType.Beacon:
            if let beaconData = listItems![indexPath.section+indexPath.row] as? AtmelBeaconItem
                {selectedItemType = beaconData.beaconType}
            break
        case ItemType.Region:
            if let regionData = listItems![indexPath.section+indexPath.row] as? AtmelBeaconRegion
               {selectedItemType = regionData.beaconType}
            
            break
        }
        selectedActionType = .update
        self.performSegue(withIdentifier: TO_BEACON_UPDATE_SEGUE, sender: self)
    }
}



extension BeaconListViewController : BeaconDataTableViewCellDelegate{
    
    func deleteBeaconAtindex(_ index: Int) {
        
        let beaconItem = listItems![index] as? AtmelBeaconItem
        
        if beaconItem != nil {
            BeaconRanger.removeBeaconFromDB(beaconItem!)
        }
        listItems?.remove(at: index)
        beaconRangeTableView?.reloadData()
    }
}


extension BeaconListViewController : BeaconRegionDataTableViewCellDelegate{
    
    func deleteRegionAtIndex(_ index: Int) {
        
        
        let beaconRegion = listItems![index] as? AtmelBeaconRegion
        
        if beaconRegion != nil {
            BeaconRanger.removeBeaconRegionFromDB(beaconRegion!)
        }
        
        listItems?.remove(at: index)
        beaconRangeTableView?.reloadData()
    }
}






