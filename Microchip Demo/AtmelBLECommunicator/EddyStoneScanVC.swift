/***************************************************************************
 
 EddystoneScanVC.swift

 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import UIKit
import CoreBluetooth

let cellIdentifier          = "eddystoneCellID"
let configurationVCSegue    = "toConfigurationVCID"

class EddyStoneScanVC: BaseViewController {
    
    @IBOutlet weak var beaconCountLabel: UILabel!
    @IBOutlet weak var eddyStoneTableView: UITableView!
    @IBOutlet weak var startScanButton: UIButton!
    @IBOutlet weak var progressView: UIView!
    
    var eddystoneArray  : NSMutableArray?
    fileprivate var scanActivityIndicator : UIActivityIndicatorView?
    fileprivate var connectionActivityIndicator : UIActivityIndicatorView?
    fileprivate var presentSelectedDeviceIndex : Int?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        GlobalCommunicator.shared.communicator=BLECommunicator(scanmode: ScanMode.passive, forBLEProfiles: nil, withDelegate: self)
        eddystoneArray = NSMutableArray()
        
        eddyStoneTableView.tableFooterView = UIView(frame: CGRect.zero)
        
        configureScanActivityIndicator()
        configureConnectionActivityIndicator()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customNavigationItem()
        if startScanButton.isSelected{
            GlobalCommunicator.shared.communicator.startScan([.EddyStone])
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        GlobalCommunicator.shared.communicator.stopScan()
        progressView.isHidden = true
        startScanButton.isSelected = false
        
        // Check whether back button is pressed. Then disconnect all connected beacons.
        if self.navigationController?.viewControllers.contains(self) == false{
            disconnectAllconnectedBeacons()
        }
        
    }
    
    func customNavigationItem()
    {
        self.navigationItem.title = "Beacons"
     
    }

    
    @IBAction func startScanButtonClicked(_ sender: UIButton) {
        
        if sender.isSelected == false{
            GlobalCommunicator.shared.communicator.startScan([.EddyStone])
            sender.isSelected = true
            progressView.isHidden = false
        }
        else{
            GlobalCommunicator.shared.communicator.stopScan()
            sender.isSelected = false
            progressView.isHidden = true
        }
    }
    
    func configureScanActivityIndicator(){
        
        scanActivityIndicator = UIActivityIndicatorView(frame: progressView.bounds)
        scanActivityIndicator?.activityIndicatorViewStyle = .white
        progressView.addSubview(scanActivityIndicator!)
        scanActivityIndicator?.startAnimating()
        progressView.isHidden = true
    }
    
    func configureConnectionActivityIndicator(){
        
        connectionActivityIndicator = UIActivityIndicatorView(frame: self.view.bounds)
        connectionActivityIndicator?.activityIndicatorViewStyle = .white
        connectionActivityIndicator?.backgroundColor = UIColor.lightGray
        connectionActivityIndicator?.alpha = 0.5
    }
    
    
    func disconnectAllconnectedBeacons() {
        
        if  eddystoneArray?.count != 0 {
            
            for index in 0...(eddystoneArray!.count - 1) {
                
               let beacon = eddystoneArray?.object(at: index) as! BLEDeviceInfo
                if beacon.BLEPeripheral.state == .connected {
                    GlobalCommunicator.shared.communicator.disconnect(beacon.BLEPeripheral)
                }
            }
        }
    }
}


extension EddyStoneScanVC : UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return eddystoneArray!.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! EddyStoneTableViewCell
        let beacon = eddystoneArray![indexPath.row] as! BLEDeviceInfo
        cell.beaconNameLabel.text = beacon.BLEPeripheralLocalName!
        cell.connectButton.tag = indexPath.row
        cell.delegate = self
        
        if beacon.BLEPeripheral.state == .connected {
            cell.connectButton.isSelected = true
        }
        else{
            cell.connectButton.isSelected = false
        }
        
        return cell
    }
}

extension EddyStoneScanVC : UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let deviceSelected = eddystoneArray?.object(at: indexPath.row) as? BLEDeviceInfo
        
        if deviceSelected?.BLEPeripheral.state == .connected {
            
            GlobalCommunicator.shared.connectedDevice = deviceSelected
            self.performSegue(withIdentifier: configurationVCSegue, sender: self)
        }
    }
}


extension EddyStoneScanVC : EddystoneTableCellDelegate{
    
    func connectButtonClickedAtCellWithIndex(_ index: Int) {
        
        self.view.addSubview(connectionActivityIndicator!)
        connectionActivityIndicator?.startAnimating()
        
        presentSelectedDeviceIndex = index
        
        let deviceSelected = eddystoneArray?.object(at: index) as? BLEDeviceInfo
        GlobalCommunicator.shared.connectedDevice = deviceSelected
        GlobalCommunicator.connectPeripheralObject()
    }
    
    
    func disconnectButtonClickedAtCellWithIndex(_ index: Int) {
        
        let connectedDevice = eddystoneArray?.object(at: index) as? BLEDeviceInfo
        
        if connectedDevice?.BLEPeripheral.state == .connected {
            GlobalCommunicator.shared.communicator.disconnect(connectedDevice!.BLEPeripheral)
        }
        
        eddystoneArray?.removeObject(at: index)
        beaconCountLabel.text = eddystoneArray!.count.format("02")
        eddyStoneTableView.reloadData()
    }
}



extension EddyStoneScanVC : BLECommunicatorDelegate{
    
    
    func bleCommunicator(_ blecommunicator:BLECommunicator ,communicatorInitializationSuccessWithBluetoothState state:BluetoothState){
        
    }
    
    func bleCommunicator(_ blecommunicator:BLECommunicator ,communicatorInitializationFailWithBluetoothState state:BluetoothState){
        
    }
    
    func bleCommunicator(_ bleCommunicator: BLECommunicator!, didDiscoverPeripheral peripheral: CBPeripheral!, advertisementData: [AnyHashable: Any]!, RSSI: NSNumber!){
        
    }
    
    func bleCommunicator(_ bleCommunicator: BLECommunicator!,didRetrievePeripherals peripherals: [AnyObject]!){
        
    }
    
    func bleCommunicator(_ bleCommunicator:BLECommunicator!,didDiscoverDevice deviceInfo: BLEDeviceInfo! ){
        
        if !checkAlreadyExist(deviceInfo){
            eddystoneArray?.add(deviceInfo)
            beaconCountLabel.text = eddystoneArray!.count.format("02")
            eddyStoneTableView.reloadData()
        }
    }
    
    func bleCommunicator(_ bleCommunicator:BLECommunicator!,didConnectDevice device: CBPeripheral!){
        
        connectionActivityIndicator?.removeFromSuperview()
        self.performSegue(withIdentifier: configurationVCSegue, sender: self)
    }
    
    func bleCommunicator(_ bleCommunicator:BLECommunicator!,didFailToConnectDevice device: CBPeripheral!){
        
        print("failed to connect to device")
        
        connectionActivityIndicator?.removeFromSuperview()
        
        // Moving the disconnect button to the connect state
        if presentSelectedDeviceIndex != nil {
            
            let cellIndexPath = IndexPath(row: presentSelectedDeviceIndex!, section: 0)
            let cell = eddyStoneTableView.cellForRow(at: cellIndexPath) as? EddyStoneTableViewCell
            
            if  cell != nil {
                cell!.connectButton.isSelected = false
            }
        }
        
        let alertVC = UIAlertController(title: "ERROR!", message: "Could not connect to Eddystone", preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alertVC.addAction(alertAction)
        self.present(alertVC, animated: true, completion: nil)
        
    }
    
    func bleCommunicator(_ bleCommunicator:BLECommunicator!,didDisconnectDevice device: CBPeripheral! , error: NSError!){
        
        print("Device disconnected")
        
        if self.navigationController?.topViewController != self {
            self.navigationController?.popToViewController(self, animated: true)
        }        
        
    }
}


extension EddyStoneScanVC {
    
    fileprivate func checkAlreadyExist(_ item : BLEDeviceInfo) -> Bool
    {
        if eddystoneArray?.count != 0 {
           
            for index in 0...(eddystoneArray!.count-1)
            {
                let deviceInfo = eddystoneArray?.object(at: index) as! BLEDeviceInfo
                if deviceInfo.BLEPeripheral.identifier.uuidString == item.BLEPeripheral.identifier.uuidString
                {
                    return true
                }
            }
        }
        return false
    }

}



