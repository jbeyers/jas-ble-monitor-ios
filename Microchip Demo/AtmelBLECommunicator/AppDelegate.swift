
/* ***************************************************************************

AppDelegate.swift


Copyright (c) 2015, Atmel Corporation. All rights reserved.
Released under NDA
Licensed under Atmel's Limited License Agreement.


THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Atmel Corporation: http://www.atmel.com

******************************************************************************/


import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var savedImageHeader:ImageHeader?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
//        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        NotificationCenter.trigger(Constants.NotificationName.onEnterBackground)
    }
    


    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        NotificationCenter.trigger(Constants.NotificationName.onEnterForeground)
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    func application(_ app: UIApplication,open url: URL, options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool{

        let detail = Image.saveFile(url)
        if (detail.0){
            savedImageHeader = detail.1
            showAddSuccessAlert()
            return true
        }
        else{
            showDuplicateFileAlert()
        }
        return false
    }
    
    func application(_ application: UIApplication, handleOpen url: URL) -> Bool {

        let detail = Image.saveFile(url)
        if (detail.0){
            savedImageHeader = detail.1
            showAddSuccessAlert()
            return true
        }
        else{
            showDuplicateFileAlert()
        }

        return false
    }
    

}
extension AppDelegate{
    
    func showAddSuccessAlert(){
        
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            if let tp = (topController as? UINavigationController)?.visibleViewController
            {
                topController = tp
            }
            else{
                while let presentedViewController = topController.presentedViewController {
                    topController = presentedViewController
                    if let navC = (topController as? UINavigationController)?.visibleViewController
                    {topController = navC}
                    
                }
            }
            let otaAlert = UIAlertController(title:"Successfully Added",message: "Firmware Image file successfully added to app directory", preferredStyle: UIAlertControllerStyle.alert)
            let IgnoreAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
            otaAlert.addAction(IgnoreAction)

            topController.present(otaAlert, animated: true, completion: nil)
        }
    }
    
    
    
    func showDuplicateFileAlert() {
        
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            if let tp = (topController as? UINavigationController)?.visibleViewController
            {
                topController = tp
            }
            else{
                while let presentedViewController = topController.presentedViewController {
                    topController = presentedViewController
                    if let navC = (topController as? UINavigationController)?.visibleViewController
                    {topController = navC}
                    
                }
            }
            let otaAlert = UIAlertController(title:"Duplicate File",message: "This firmware Image file already exists in app directory", preferredStyle: UIAlertControllerStyle.alert)
            let IgnoreAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
            otaAlert.addAction(IgnoreAction)
            
            topController.present(otaAlert, animated: true, completion: nil)
        }

    }
    
    
  
    
    
    
    
}
