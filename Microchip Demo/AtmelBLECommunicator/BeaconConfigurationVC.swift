/***************************************************************************
 
 BeaconConfigurationVC.swift
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import UIKit
import CoreBluetooth

let urlDetailCellIdentifier = "URLDetailCell"
let powerDetailCellIdentifier = "PowerDetailCell"
let beaconDetailCellIdentifier = "BeaconDetailCell"


let urlDetailCellHeight : CGFloat = 110
let powerDetailsCellHeight : CGFloat = 165  
let beaconDetailCellHeight : CGFloat = 100

let configurationFields = ["URI Data", "URI Flags", "Advertised Tx Power Levels", "Tx Power Mode", "Beacon Period", "Reset eddystone values to defaults"]

enum BeaconConfigurationFields : Int{
    case urlData
    case urlFlags
    case txPowerLevel
    case txPowerMode
    case beaconPeriod
    case reset
}


class BeaconConfigurationVC: BaseViewController {
    
    
    @IBOutlet weak var beaconConfigTableView: UITableView!
    
    var eddystoneInterface      : AtmelEddystoneInterface?
    var eddystoneServiceHandler : BLEServiceDiscoverHandler?
    var beaconInfo              : EddystoneDetails?
    
    
    @IBOutlet weak var beaconStatusSwitch: UISwitch!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initializeServiceDiscoverHandler()
        
        beaconConfigTableView.tableFooterView = UIView(frame: CGRect.zero)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customNavigationItem()
    }

    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        removeCellObserver()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    func customNavigationItem()
    {
        self.navigationItem.title = "Eddystone Configurations"
    }

    
    func initializeServiceDiscoverHandler(){
     
        eddystoneServiceHandler = BLEServiceDiscoverHandler(connectedDevice: GlobalCommunicator.shared.connectedDevice!.BLEPeripheral)
        eddystoneServiceHandler?.delegate = self
    }
    
    func initializeInterface(){
        
        eddystoneInterface = AtmelEddystoneInterface(sourceDevice: GlobalCommunicator.shared.sourceDevice)
        eddystoneInterface?.delegate = self
        eddystoneInterface?.readBeaconLockState()
        
        // Read value for all other characteristics
        eddystoneInterface?.readValueForAllCharacteristics()
    }
    
    
    func removeCellObserver() {
        
        let cell = beaconConfigTableView.cellForRow(at: IndexPath(row: 2, section: 0)) as! PowerDetailTableViewCell
        cell.removeObservers()
    }
    
    
    // MARK :- Handle Eddystone lock state
  
    @IBAction func beaconLockSwitched(_ sender: UISwitch) {
        
        showLockOrUnlockAlert()
    }
    
    
    func showLockOrUnlockAlert()
    {
        let alertVC = UIAlertController(title: "", message: "Enter Password", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            self.setLockSwitchWithCurrentBeaconLockStatus()
        }
        
        let doneAction = UIAlertAction(title: "OK", style: .default) { (action) in
            
            let passwordTextField = alertVC.textFields?.first
            
            if passwordTextField?.text != nil && passwordTextField?.text != ""{
                
                if self.eddystoneInterface?.beaconLockStatus == LockStatus.unLocked {
                    self.eddystoneInterface!.lockBeaconWithPassword(UInt(passwordTextField!.text!)!)
                }
                else{
                    self.eddystoneInterface!.unlockBeaconWithPassword(UInt(passwordTextField!.text!)!)
                }
            }
            else{
                print("Paasword not valid")
                self.setLockSwitchWithCurrentBeaconLockStatus()
            }
        }
        
        alertVC.addTextField { (passwordTextField : UITextField) in
            passwordTextField.delegate = self
        }
        
        alertVC.addAction(cancelAction)
        alertVC.addAction(doneAction)
        self.present(alertVC, animated: true, completion: nil)
    }
    
    func setLockSwitchWithCurrentBeaconLockStatus(){

        if eddystoneInterface?.beaconLockStatus == .locked {
            beaconStatusSwitch.isOn = true
        }
        else{
            beaconStatusSwitch.isOn = false
        }

    }
    
}


extension BeaconConfigurationVC : UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return configurationFields.count
    }
    
    
    func setEnableStatusOfButton(_ button : UIButton){
        
        if beaconStatusSwitch.isOn == false{
            button.isEnabled = true
            button.backgroundColor = UIColor.black
        }
        else{
            button.isEnabled = false
            button.backgroundColor = UIColor.lightGray
        }

    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let userSelection = BeaconConfigurationFields(rawValue: indexPath.row)!
        
        switch userSelection{
            
        case .urlData :
            
            let cell = tableView.dequeueReusableCell(withIdentifier: urlDetailCellIdentifier) as! URLDetailTableViewCell
            cell.headerLabel.text = configurationFields[indexPath.row]
            cell.subTitleLabel.isHidden = true
            cell.writeButton.tag = indexPath.row
            cell.delegate = self
            cell.urlDataTextField.isUserInteractionEnabled = true
            cell.writeButton.isHidden = false

            setEnableStatusOfButton(cell.writeButton)
            
            if beaconInfo != nil && beaconInfo?.urlString != nil{
                cell.urlDataTextField.text = "\(beaconInfo!.urlString!)"
            }
            return cell
            
            
        case .urlFlags :
            
            let cell = tableView.dequeueReusableCell(withIdentifier: urlDetailCellIdentifier) as! URLDetailTableViewCell
            cell.headerLabel.text = configurationFields[indexPath.row]
            cell.subTitleLabel.isHidden = false
            cell.subTitleLabel.isHidden = true

            cell.writeButton.tag = indexPath.row
            cell.delegate = self
            cell.urlDataTextField.isUserInteractionEnabled = false
            cell.writeButton.isHidden = true
            
            if beaconInfo != nil && beaconInfo?.urlFlag != nil{
                cell.urlDataTextField.text = String(format: "%2X", beaconInfo!.urlFlag!) 
            }
            return cell
            
            
        case .txPowerLevel :
            
            let cell = tableView.dequeueReusableCell(withIdentifier: powerDetailCellIdentifier) as! PowerDetailTableViewCell
            cell.headerLabel.text = configurationFields[indexPath.row]
            cell.writeButton.tag = indexPath.row
            cell.delegate = self
            cell.cellMode = .categoryPowerLevelCell
            cell.parentTableView = beaconConfigTableView
            cell.alertLabel.isHidden = false
            setEnableStatusOfButton(cell.writeButton)

            if beaconInfo != nil && beaconInfo?.txPowerLevelsArray != nil{
                
            // Transmission power level array
                cell.setPowerLevelWith(beaconInfo!.txPowerLevelsArray!)
            
            }
            
            return cell
            
            
        case .txPowerMode :
            
            let cell = tableView.dequeueReusableCell(withIdentifier: powerDetailCellIdentifier) as! PowerDetailTableViewCell
            cell.headerLabel.text = configurationFields[indexPath.row]
            cell.writeButton.tag = indexPath.row
            cell.delegate = self
            cell.cellMode = .categoryPowerModeCell
            cell.alertLabel.isHidden = true
            setEnableStatusOfButton(cell.writeButton)
            
            if beaconInfo != nil && beaconInfo?.txPowerMode != nil{
                
                // Transmission power level mode
                cell.setTransmissionPowermode(beaconInfo!.txPowerMode!)
            }
            
            return cell
            
            
        case .beaconPeriod :
            
            let cell = tableView.dequeueReusableCell(withIdentifier: beaconDetailCellIdentifier) as! BeaconConfigurationTableViewCell
            cell.headerLabel.text = configurationFields[indexPath.row]
            cell.periodTextField.isHidden = false
            cell.writeButton.tag = indexPath.row
            cell.writeButton.setTitle("Write", for: UIControlState())
            cell.delegate = self
            setEnableStatusOfButton(cell.writeButton)
            cell.alertLabel.isHidden = false
            cell.parentTableView = beaconConfigTableView
            if beaconInfo != nil && beaconInfo?.beaconPeriod != nil{
                
                cell.periodTextField.text = "\(beaconInfo!.beaconPeriod!)"
            }

            
            return cell
            
            
        case .reset :
            
            let cell = tableView.dequeueReusableCell(withIdentifier: beaconDetailCellIdentifier) as! BeaconConfigurationTableViewCell
            cell.headerLabel.text = configurationFields[indexPath.row]
            cell.periodTextField.isHidden = true
            cell.writeButton.tag = indexPath.row
            cell.delegate = self
            cell.writeButton.setTitle("Reset", for: UIControlState())
            cell.alertLabel.isHidden = true
            setEnableStatusOfButton(cell.writeButton)
            return cell
            
        }
    }
}


extension BeaconConfigurationVC : UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let field = BeaconConfigurationFields(rawValue: indexPath.row)!
        
        if field == .urlData || field == .urlFlags{
            return urlDetailCellHeight
        }
        else if field == .txPowerLevel || field == .txPowerMode{
            return powerDetailsCellHeight
        }
        else{
            return beaconDetailCellHeight
        }
    }
}



extension BeaconConfigurationVC : URLDetailTableViewCellDelegate{
    
    func writeData(_ data: String, field: BeaconConfigurationFields) {
        
        print("URL String = \(data)")
        eddystoneInterface?.writeURLToBeaconWith(data)
    }
    
}


extension BeaconConfigurationVC : PowerDetailTableCellDelegate{
    
    func writeToBeaconWithPowerLevel(_ powerLevelsArray : [Int8], field : BeaconConfigurationFields){
        
        print("Power levels array = \(powerLevelsArray)")
        
        eddystoneInterface?.setAdvertisedPowerLevelValues(powerLevelsArray)
    }
    
    func writeToBeaconWithPowerMode(_ powerMode : TxPowerModes, field : BeaconConfigurationFields){
        
        print("Power mode value = \(powerMode.rawValue)")
        eddystoneInterface?.setBeaconPowerModeToMode(powerMode)
        
    }
}



extension BeaconConfigurationVC : BeaconConfigurationTableCellDelegate{
    
    func writeToBeaconWith(_ period : UInt16, field : BeaconConfigurationFields){
        
        if field == .beaconPeriod{
            
            print("period = \(period)")
            eddystoneInterface?.setBeaconPeriodToValue(period)
        }
        else{
            
            print("Reset value")
            eddystoneInterface?.resetBeacon()
        }
    }

}


extension BeaconConfigurationVC : AtmelEddyStoneDelegate{

    func configurationServiceDidReadLockStatus(_ status: LockStatus) {
        
        if status == .unLocked{
           
            print("Device is unlocked")
            beaconStatusSwitch.isOn = false
        }
        else{
            
            print("Device is locked")
            beaconStatusSwitch.isOn = true
        }
        
        // If lock nad unlock characteristics are not present, then disable the lock switch
        if eddystoneInterface?.lockCharacteristic == nil || eddystoneInterface?.unlockCharacteristic == nil {
            beaconStatusSwitch.isUserInteractionEnabled = false
        }
        
        
    }
    
    func configurationServiceDidReadValueForCharacteristic(_ characteristic : CBCharacteristic,beaconInfo : EddystoneDetails?){
        
        if beaconInfo != nil{
            self.beaconInfo = beaconInfo
            beaconConfigTableView.reloadData()
        }
    }

  
    func configurationServiceDidWriteValueForCharacteristic(_ characteristic : CBCharacteristic, withError errorString: String?){
    
        if errorString == nil{
            
            // Check whether it is reset characteristic
            
            if characteristic.uuid.isEqual(CBUUID(string: BLECharacteristic.EddyStoneResetCharacteristics.rawValue)){
                eddystoneInterface?.readValueForAllCharacteristics()
            }
            else if characteristic.uuid.isEqual(CBUUID(string: BLECharacteristic.EddyStoneLockCharacteristics.rawValue)){
                eddystoneInterface?.beaconLockStatus = .locked
                beaconStatusSwitch.isOn = true
                beaconConfigTableView.reloadData()
            }
            else if characteristic.uuid.isEqual(CBUUID(string: BLECharacteristic.EddyStoneUnLockCharacteristics.rawValue)){
                eddystoneInterface?.beaconLockStatus = .unLocked
                beaconStatusSwitch.isOn = false
                beaconConfigTableView.reloadData()
            }
            else{
                
                eddystoneInterface?.readValueForCharacteristic(characteristic)
                
                let alertVC = UIAlertController(title: "Success", message: "Successfully written values to Eddystone", preferredStyle: .alert)
                let alertAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                alertVC.addAction(alertAction)
                self.present(alertVC, animated: true, completion: nil)
            }
        }
        else{
            
            if (characteristic.uuid.isEqual(CBUUID(string: BLECharacteristic.EddyStoneLockCharacteristics.rawValue))) || (characteristic.uuid.isEqual(CBUUID(string: BLECharacteristic.EddyStoneUnLockCharacteristics.rawValue))){
                
                setLockSwitchWithCurrentBeaconLockStatus()
            }
            
            if errorString != ""{
                
                let alertVC = UIAlertController(title: "Eddystone Error", message: errorString, preferredStyle: .alert)
                let alertAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                alertVC.addAction(alertAction)
                self.present(alertVC, animated: true, completion: nil)
            }
        }
      
    }

    
}



extension BeaconConfigurationVC : BLEServicesDiscoverHandlerDelegate{
    
    func discoveredDeviceServicesWithStatus(_ status: Bool) {
            initializeInterface()
    }
}



extension BeaconConfigurationVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let digitsInvertedSet = CharacterSet(charactersIn: "0123456789").inverted
        let componetsArray = string.components(separatedBy: digitsInvertedSet)
        let digitString = componetsArray.joined(separator: "")
        let length = textField.text!.utf16.count + string.utf16.count - range.length
        
        if string == digitString && length <= 4{
            return true
        }
        else{
            return false
        }
    }
    
}








