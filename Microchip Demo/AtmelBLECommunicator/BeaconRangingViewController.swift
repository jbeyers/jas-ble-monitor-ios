/***************************************************************************
 
 BeaconRangingViewController.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import UIKit
import CoreBluetooth
import CoreLocation
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


let marginForPlotter:CGFloat = 30
let infoViewBoxFrame = CGRect(x: 0, y: 0, width: 212, height: 81)

let TO_BEACON_LIST_SEGUE    = "toBeaconListSegue"
let TO_BEACON_DETAIL_SEGUE  = "BeaconDetailSegue"
let TO_BEACON_URL_SEGUE     = "toURLVCSegue"
let TO_BEACON_SCAN_VC_SEGUE = "toScanVC"

let DEFAULT_BEACON_UUID     = "218af652-73e3-40b3-b41c-1953242c72f4"
let DEFAULT_MAJOR : UInt16  = 187
let DEFAULT_MINOR : UInt16  = 69
let DEFAULT_BEACON_URL      = "http://www.atmel.com/technologies/wireless/bluetooth.aspx"
let DEFAULT_BEACON_NAME     = "Atmel_iBeacon"


struct NeighbouringBeacons{
    var leftBeacon:BeaconIconView?
    var rightBeacon:BeaconIconView?
    
    init(left:BeaconIconView?,right:BeaconIconView?){
        leftBeacon = left
        rightBeacon = right
    }
}


class BeaconRangingViewController: BaseViewController {

    var h:CGFloat = 0
    var k:CGFloat = 0
    
    var selectedListType :ItemType!
    
    var beconIconViewArray:[BeaconIconView]=[BeaconIconView]()
    var beaconRanger:BeaconRanger!
    
    var targettedBeacon:AnyObject!
    var targettedBeaconType:BeaconVendorType!
    
    var nearestBeacon : AnyObject?
    var nearestBeaconType : BeaconVendorType?
    var nearestBeaconIconView : BeaconIconView?
    
    var immediateMinRSSI:CGFloat = -40
    var nearMinRSSI:CGFloat = -70
    var maxRSSI:CGFloat = -10
    var minRSSI:CGFloat = -100
 
    var beaconInfoView:BeaconInfoView?
    

    @IBOutlet weak var farView: ArcView!
    @IBOutlet weak var nearView: ArcView!
    @IBOutlet weak var immediateView: ArcView!
    @IBOutlet weak var bottomView: ArcView!

    @IBOutlet weak var radarPlotView: UIView!
    @IBOutlet weak var beaconconfigButton: UIButton!
    
    
    @IBOutlet weak var proximityButton: UIButton!
    @IBOutlet weak var distanceButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        beaconRanger = BeaconRanger(withDelegate: self)
        // Do any additional setup after loading the view.
        distanceButton.isSelected = true
        saveDefaultBeacons()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        configureView()
        cleanUpRangeScreen()
        configureNavigationBar()
        NotificationCenter.default.addObserver(self, selector: #selector(BeaconRangingViewController.deviceOrientationDidChange(_:)), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        beaconRanger.startRanging()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        h = radarPlotView.center.x
        k = radarPlotView.frame.height*(5/3)

    }
    
    func configureView(){
        
        if nearestBeacon != nil {
            
            distanceButton.isSelected = true
            proximityButton.isSelected = false
            nearestBeacon = nil
            nearestBeaconIconView = nil
        }
    }
    
    func configureNavigationBar(){
        
        self.navigationItem.title = "Beacons"
   
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        let rightBarbuttonItem = UIBarButtonItem(customView: rightMenuView())
        self.navigationItem.rightBarButtonItem = rightBarbuttonItem
        
        
        let leftBarButtonItem = UIBarButtonItem(customView: leftMenuView())
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
    }
    
    func cleanUpRangeScreen(){
        
        beaconRanger.clearBeacons()

        for beaconIconView in beconIconViewArray
            {beaconIconView.removeFromSuperview()}
        beconIconViewArray.removeAll()
    }
    
    @IBAction func regionListButtonAction(_ sender: UIButton) {
        selectedListType = ItemType.Region
        self.performSegue(withIdentifier: TO_BEACON_LIST_SEGUE, sender: self)
    }
    @IBAction func beaconListButtonAction(_ sender: UIButton) {
        selectedListType = ItemType.Beacon
        self.performSegue(withIdentifier: TO_BEACON_LIST_SEGUE, sender: self)
    }
    
    
    @IBAction func proximityButtonClicked(_ sender: UIButton) {
        
        if distanceButton.isSelected {
            distanceButton.isSelected = false
        }
        
        proximityButton.isSelected = true
    }
    
    @IBAction func distanceButtonClicked(_ sender: UIButton) {
        
        if proximityButton.isSelected {
            proximityButton.isSelected = false
        }
        
        distanceButton.isSelected = true
    }
    
    @IBAction func tappedOnRadarView(_ sender: UITapGestureRecognizer) {
        
        removeBeaconInfoview()
    }
    
    
    func saveDefaultBeacons(){
        
        let defaultiBeaconUUID = UUID(uuidString: DEFAULT_BEACON_UUID)
        
        let defaultiBeaconRegion = AtmeliBeaconRegion(withUUID: defaultiBeaconUUID!, majorValue: nil, minorValue: nil, range: true, nameString: DEFAULT_BEACON_NAME)

        if !BeaconRanger.isBeaconRegionPresentInDB(defaultiBeaconRegion){
            BeaconRanger.saveBeaconRegion(defaultiBeaconRegion)
        }
        
        let defaultiBeacon = AtmeliBeaconItem(withUUID: defaultiBeaconUUID!, majorValue: NSNumber(value: DEFAULT_MAJOR as UInt16), minorValue: NSNumber(value: DEFAULT_MINOR as UInt16), url: DEFAULT_BEACON_URL, nameString: DEFAULT_BEACON_NAME)
        
        if !BeaconRanger.isBeaconItemPresentInDB(defaultiBeacon) {
            BeaconRanger.saveBeaconItem(defaultiBeacon)
        }
        
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        removeBeaconInfoview()
        if segue.identifier == TO_BEACON_DETAIL_SEGUE{
            
            let destinationVC = segue.destination as! BeaconDetailViewController
            
            switch(targettedBeaconType!){
                
            case BeaconVendorType.nativeiBeacon:
                destinationVC.beaconData = targettedBeacon
                destinationVC.beaconType = BeaconVendorType.nativeiBeacon
                beaconRanger.targettediBeacon = (targettedBeacon as! CLBeacon)
                beaconRanger.targettedBeaconDelegate = destinationVC
                beaconRanger.targettedBeaconUUID = nil
                break
            case BeaconVendorType.thirdPatyBeacon:
                destinationVC.beaconData = targettedBeacon
                destinationVC.beaconType = BeaconVendorType.thirdPatyBeacon
                beaconRanger.targettedBeaconUUID = (targettedBeacon as! AtmelBeacon).peripheral.identifier
                beaconRanger.targettedBeaconDelegate = destinationVC
                break
            }
        }
        else if segue.identifier == TO_BEACON_LIST_SEGUE{
            let destinationVC = segue.destination as! BeaconListViewController
            destinationVC.listViewType = selectedListType

        }
        else if segue.identifier == TO_BEACON_URL_SEGUE{
            let destinationVC = segue.destination as! BeaconURLViewController
            //destinationVC.beaconURL = getURLForBeacon(nearestBeacon!, beaconType: nearestBeaconType) as! NSURL
            destinationVC.beaconURL = getURLForBeacon(nearestBeacon!, beaconType: nearestBeaconType)
        }
        
    }
    
    // MARK:- Navigation bar right menu configuration
    
    
    func rightMenuView() -> UIView
    {
        let RIGHT_MENU_FRAME            = CGRect(x: 00, y: 0, width: 120, height: 40)
        let BEACON_SCAN_BUTTON_FRAME    = CGRect(x: 00, y: 0, width: 40, height: 40)
        let REGION_BUTTON_FRAME         = CGRect(x: 40, y: 0, width: 40, height: 40)
        let BEACON_BUTTON_FRAME         = CGRect(x: 80, y: 0, width: 40, height: 40)
        
        let rightMenuView = UIView(frame: RIGHT_MENU_FRAME)
        
        // Right Menu View |--Eddystone List--||--Region List--||--beacon List--|
        
        
        // Beacon scan button
        
        let beaconScanButton = UIButton(frame: BEACON_SCAN_BUTTON_FRAME)
        beaconScanButton.setImage(UIImage(named: "beacon_scan_image"), for: UIControlState())
        beaconScanButton.addTarget(self, action: #selector(BeaconRangingViewController.beaconScanButtonClicked(_:)), for: UIControlEvents.touchUpInside)
        rightMenuView.addSubview(beaconScanButton)
        
        //Region Button
        let regionButton = UIButton(frame: REGION_BUTTON_FRAME)
        regionButton.setImage(UIImage(named: "regionListIcon"), for: UIControlState())
        regionButton.addTarget(self, action: #selector(BeaconRangingViewController.regionListButtonClicked(_:)), for: UIControlEvents.touchUpInside)
        rightMenuView.addSubview(regionButton)
        
        //Beacon Button
        let beaconButton = UIButton(frame: BEACON_BUTTON_FRAME)
        beaconButton.setImage(UIImage(named: "beaconListIcon"), for: UIControlState())
        beaconButton.addTarget(self, action: #selector(BeaconRangingViewController.beaconListButtonClicked(_:)), for: UIControlEvents.touchUpInside)
        rightMenuView.addSubview(beaconButton)
        
        return rightMenuView
    }
    
    
    func leftMenuView() -> UIView{
        
        let LEFT_MENU_FRAME     = CGRect(x: 0, y: 0, width: 60, height: 40)
        let HOME_BUTTON_FRAME   = CGRect(x: 0, y: 0, width: 60, height: 40)
        
        // Home button
        let homeButton = UIButton(frame: HOME_BUTTON_FRAME)
        homeButton.addTarget(self, action: #selector(BeaconRangingViewController.homeButtonClicked), for: UIControlEvents.touchUpInside)
        homeButton.setTitle("Home", for: UIControlState())
        homeButton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 15)
        homeButton.setTitleColor(UIColor.white, for: UIControlState())
        homeButton.backgroundColor = UIColor.clear
        
        let leftMenuView = UIView(frame: LEFT_MENU_FRAME)
        leftMenuView.addSubview(homeButton)
        
        return leftMenuView
    }
    
    
    func beaconScanButtonClicked(_ sender : UIButton){
            self.performSegue(withIdentifier: TO_BEACON_SCAN_VC_SEGUE, sender: self)
    }
    
    

    func regionListButtonClicked(_ sender : UIButton){
        
        selectedListType = ItemType.Region
        self.performSegue(withIdentifier: TO_BEACON_LIST_SEGUE, sender: self)
    }
    
    
    func beaconListButtonClicked(_ sender : UIButton){
        
        selectedListType = ItemType.Beacon
        self.performSegue(withIdentifier: TO_BEACON_LIST_SEGUE, sender: self)
    }
    
    
    func homeButtonClicked(){
        beaconRanger.stopRanging()
        self.dismiss(animated: true, completion: nil)
    }
    
    func deviceOrientationDidChange(_ notification : Notification){
        
        if UIDevice.is_iPad() {
        
            for iconView in beconIconViewArray {
                
                updateIconPosition(iconView, withAnimation: true)
            }
        }
    }
}


extension BeaconRangingViewController{
    
    func removeBeaconInfoview(){
        
        if (beaconInfoView?.beaconIconView != nil)
            {beaconInfoView?.beaconIconView.viewArrowDirection = InfoViewArrowDirection.none}
        beaconInfoView?.removeFromSuperview()
        beaconInfoView = nil

    }
    func findSpaceForRangedBeacon()->CGFloat{
        if beconIconViewArray.isEmpty{
            return radarPlotView.frame.width/2
        }
        var maxSpace:CGFloat = 0
        var maxSpacePoint = radarPlotView.frame.width/2
        
        for beaconIcon in beconIconViewArray{
            
            var maxBeaconSpace:CGFloat!
            var maxBeaconSpaceCentre:CGFloat!
            
            var minLeftDistance:CGFloat!
            var minRightDistance:CGFloat!
            
            let nearestBeacons = findNeighbouringBeacons(beaconIcon)
            
            if nearestBeacons.leftBeacon == nil && nearestBeacons.rightBeacon == nil {
                
                minLeftDistance = beaconIcon.center.x
                minRightDistance = radarPlotView.frame.width - beaconIcon.center.x
                
            }
            else if nearestBeacons.leftBeacon == nil{
                
                minLeftDistance = beaconIcon.center.x
                minRightDistance = nearestBeacons.rightBeacon!.center.x - beaconIcon.center.x
                
            }
            else if nearestBeacons.rightBeacon == nil{
                minLeftDistance = beaconIcon.center.x - nearestBeacons.leftBeacon!.center.x
                minRightDistance = radarPlotView.frame.width - beaconIcon.center.x
            }
            else{
                minLeftDistance = beaconIcon.center.x - nearestBeacons.leftBeacon!.center.x
                minRightDistance = nearestBeacons.rightBeacon!.center.x - beaconIcon.center.x

            }
            
            if minLeftDistance > minRightDistance{
                maxBeaconSpace = minLeftDistance
                maxBeaconSpaceCentre = beaconIcon.frame.origin.x - minLeftDistance/2
            }
            else{
                maxBeaconSpace = minRightDistance
                maxBeaconSpaceCentre = beaconIcon.frame.maxX+(minRightDistance/2)
            }
            
            
            if maxSpace < maxBeaconSpace{
                
                maxSpace = maxBeaconSpace
                maxSpacePoint = maxBeaconSpaceCentre
            }
        }
        return maxSpacePoint
        
    }
    
    func findNeighbouringBeacons(_ beaconView:BeaconIconView) -> NeighbouringBeacons {
        
        var minLeftDistance = beaconView.frame.origin.x
        var minRightDistance = radarPlotView.frame.width - beaconView.frame.maxX
        
        var nearestLeftBeaconIcon:BeaconIconView?
        var nearestRightBeaconIcon:BeaconIconView?
        
        for beaconIcon in beconIconViewArray{
            if !beaconIcon.isEqual(beaconView){
                let distance = beaconIcon.center.x - beaconView.center.x
                
                if distance > 0{
                    if distance < minRightDistance{
                        
                        minRightDistance = distance
                        nearestRightBeaconIcon = beaconIcon
                    }
                }
                else{
                    if abs(distance) < minLeftDistance{
                        
                        minLeftDistance = distance
                        nearestLeftBeaconIcon = beaconIcon
                    }
                    
                }
                
            }
        }
        return NeighbouringBeacons(left: nearestLeftBeaconIcon,right: nearestRightBeaconIcon)
        
    }

    func updateBeaconIcon(_ icon:BeaconIconView,with beaconInfo:AnyObject){
        
        icon.updateDataWithBeaconObject(beaconInfo)
        icon.updateTimer()
        
        if icon.viewArrowDirection != InfoViewArrowDirection.none{
            beaconInfoView?.updateData(icon.beaconObj, rssi: icon.RSSI)
        }
        updateIconPosition(icon,withAnimation: true)
    }

    func plotRangedBeacon(_ beacon:AnyObject ,type:BeaconVendorType,uuid:UUID){
        
                    let indexOfBecon = beconIconViewArray.index(){$0.isSameBeacon(beacon, bType: type)}
                    if indexOfBecon != nil {
                        
                        // Removing the beacon without superview
                        if beconIconViewArray[indexOfBecon!].superview == nil {
                            beconIconViewArray.remove(at: indexOfBecon!)
                            return
                        }
                        updateBeaconIcon(beconIconViewArray[indexOfBecon!], with: beacon)
                    }
                    else{
                        if let newBeaconIcon = BeaconIconView.getIconViewForBeacon(type, beaconObject: beacon){
                            
                            let centreX = findSpaceForRangedBeacon()

                            beconIconViewArray.append(newBeaconIcon)
                            radarPlotView.addSubview(newBeaconIcon)
                            newBeaconIcon.delegate = self
                            newBeaconIcon.center = CGPoint(x: centreX, y: radarPlotView.center.y)
                            updateIconPosition(newBeaconIcon,withAnimation: false)
                        }
                        
                    }
    }
    

    
    
    
    func getRefRSSIForBeaconIcon(_ iconView : BeaconIconView) -> CGFloat{
        
        var refRSSI: CGFloat?
        
        
        switch(iconView.type!){
            
        case BeaconVendorType.nativeiBeacon:
            refRSSI = CGFloat(iconView.RSSI.floatValue) - 41
            break
            
        case BeaconVendorType.thirdPatyBeacon:
            
            if let beaconInfo = iconView.beaconObj as? AtmelBeacon{
                
                switch(beaconInfo.beaconType){
                    
                case BeaconType.altBeacon:
                    refRSSI = CGFloat(iconView.RSSI.floatValue) - 41
                    
                case BeaconType.eddystone:
                    refRSSI = CGFloat(iconView.RSSI.floatValue)
                default:break
                }
            }
            break
        }
        
        return refRSSI!
    }
    
    
    
    
    func getURLForBeacon(_ beaconData : AnyObject, beaconType : BeaconVendorType?) -> URL? {
        
        
        switch(beaconType!){
        case BeaconVendorType.nativeiBeacon:
            
            let iBeaconData = beaconData as! CLBeacon
            
            let urlString = Util.getURLForiBeacon(iBeaconData)
            if urlString != nil {
                return URL(string: urlString!)
            }
            return nil
            
        case BeaconVendorType.thirdPatyBeacon:
            let atmelBeaconData = beaconData as! AtmelBeacon
            
            switch(atmelBeaconData.beaconType){
            case BeaconType.eddystone:
               let  eddystoneBeaconData = atmelBeaconData as! AtmelEddyStone
               
               if eddystoneBeaconData.urlFrame != nil {
                return (eddystoneBeaconData.urlFrame?.url as! URL)
               }
               return nil
            
            case BeaconType.altBeacon:
               let altBeaconData = atmelBeaconData as! AtmelAltBeacon
                
                let urlString = Util.getURLForAltBeacon(altBeaconData)
                
                if urlString != nil {
                    altBeaconData.url = URL(string: urlString!)
                }
                else{
                    altBeaconData.url = nil
                }
               return (altBeaconData.url as! URL)
            default: break
            }
            break
        }
        
        return nil
    }

    
    
    
    fileprivate func showURLPageFor(_ iconView : BeaconIconView){
        
        
        if !proximityButton.isSelected {
            return
        }
        
        if nearestBeacon == nil {
            nearestBeacon = iconView.beaconObj
            nearestBeaconType = iconView.type
            nearestBeaconIconView = iconView
            
            if  getURLForBeacon(nearestBeacon!, beaconType: nearestBeaconType) != nil && self.navigationController?.topViewController == self{
                self.performSegue(withIdentifier: TO_BEACON_URL_SEGUE, sender: self)
            }
            else{
                nearestBeacon = nil
                nearestBeaconIconView = nil
            }
        }
        else{
            

            
            if nearestBeaconIconView?.isSameBeacon(iconView.beaconObj!, bType: nearestBeaconIconView!.type) == false {
                
                // Check which beacon is more nearer, the current nearest beacon or the new one came
                if nearestBeaconIconView?.rssiQueue?.average() > iconView.rssiQueue?.average() {
                    return
                }
                
                nearestBeacon = iconView.beaconObj
                nearestBeaconType = iconView.type
                nearestBeaconIconView = iconView

                let url = getURLForBeacon(nearestBeacon!, beaconType: nearestBeaconType)
                
                if url != nil {
                    
                    if self.navigationController?.topViewController == self {
                        self.performSegue(withIdentifier: TO_BEACON_URL_SEGUE, sender: self)
                    }
                    else{
                        
                        if  let beaconURLVC = self.navigationController?.topViewController as? BeaconURLViewController {
//                            beaconURLVC.updateWebViewWithURL(url!)
                            beaconURLVC.beaconURL = url!
                            beaconURLVC.showAlert()
                        }
                    }
                }
            }
        }

    }
    
    
    fileprivate func removeURLPageFor(_ iconView : BeaconIconView){

        if !proximityButton.isSelected {
            return
        }
        
        if nearestBeacon == nil {
            return
        }
        
        if  self.navigationController?.topViewController != self {
            
            if nearestBeaconIconView?.isSameBeacon(iconView.beaconObj!, bType: nearestBeaconIconView!.type) == true {
              
                print("rssi while removing beacon \(String(describing: iconView.RSSI))")
                nearestBeacon = nil
                nearestBeaconIconView = nil
                self.navigationController?.popToViewController(self, animated: true)
            }            
        }
    }
    
    
    
    func updateIconPosition(_ iconView:BeaconIconView,withAnimation:Bool){
        
        var maxY:CGFloat,minY:CGFloat,minSectRSSI:CGFloat,maxSecRSSI:CGFloat,r:CGFloat
        
        var RSSI = CGFloat((iconView.rssiQueue?.average())!)//getRefRSSIForBeaconIcon(iconView)
        
        //print("RSSI = \(iconView.rssiQueue?.average() ?? <#default value#>)")

        
        if iconView.type == BeaconVendorType.nativeiBeacon {
            
            let iBeaconData = iconView.beaconObj as! CLBeacon
            
            switch iBeaconData.proximity {
            case .immediate:
                RSSI = (maxRSSI + immediateMinRSSI)/2
            case .near :
                RSSI = (immediateMinRSSI + nearMinRSSI)/2
            case .far :
                RSSI = (minRSSI + nearMinRSSI)/2
            default:
                break
            }
        }
        
        if RSSI > immediateMinRSSI{
            
            maxY = radarPlotView.frame.height - marginForPlotter
            minY = (radarPlotView.frame.height/3)*2
            maxSecRSSI = maxRSSI
            minSectRSSI = immediateMinRSSI
            
            
            // Must show the URL associated with the beacon if any, in webView
            showURLPageFor(iconView)
        }
        else if RSSI > nearMinRSSI{
            
            maxY = (radarPlotView.frame.height/3)*2
            minY = radarPlotView.frame.height/3
            maxSecRSSI = immediateMinRSSI
            minSectRSSI = nearMinRSSI
            
            removeURLPageFor(iconView)
        }
        else{
            maxY = radarPlotView.frame.height/3
            minY = marginForPlotter
            maxSecRSSI = nearMinRSSI
            minSectRSSI = minRSSI
            
            removeURLPageFor(iconView)
        }
        
        let plotLength = maxY - minY
        let plotScale = plotLength/abs(maxSecRSSI - minSectRSSI)
        let scaledValueOffSet = abs(maxSecRSSI - RSSI) * plotScale
        let point = maxY - scaledValueOffSet
        
        r = k - point
        
        let centreX = iconView.center.x
        

        let centreY = abs (getYFor(r:r, x: centreX, h: h, k: k))
        let currentFrame = iconView.frame

        
        let newIconFrame = CGRect(x: currentFrame.origin.x-(iconView.center.x - centreX), y: currentFrame.origin.y-(iconView.center.y - centreY), width: currentFrame.width,  height: currentFrame.height)
        var infoViewRect:CGRect?
        if iconView.viewArrowDirection != InfoViewArrowDirection.none
        {infoViewRect = positionInfoViewForForIcon(iconView, iconViewFrame: newIconFrame)}
        DispatchQueue.main.async(execute: {
            
            if withAnimation
            {
                
                UIView.animate(withDuration: 0.3, delay: 0, options: .allowUserInteraction, animations: { () -> Void in
                    
                    iconView.frame = newIconFrame
                    if infoViewRect != nil
                    {self.beaconInfoView?.frame = infoViewRect!}

                    }, completion: nil)
            }
            else{
                
                iconView.center = CGPoint(x: centreX, y: centreY)
                if infoViewRect != nil
                {self.beaconInfoView?.frame = infoViewRect!}
            }
            
        })
        
    }
    
    func getYFor(r:CGFloat,x:CGFloat,h:CGFloat,k:CGFloat)->CGFloat{
        
        return (sqrt(((r*r)-((x-h)*(x-h))))-k)
    }
    
}
extension BeaconRangingViewController{
    
    func findInfoViewArrowPositionForIcon(_ iconView:BeaconIconView)->InfoViewArrowDirection{
        let iconViewFrame = iconView.frame
        
        if (iconViewFrame.maxX+infoViewBoxFrame.width < radarPlotView.frame.width){
            return InfoViewArrowDirection.left
        }
        else if (iconViewFrame.origin.x - infoViewBoxFrame.width > 0){
            return InfoViewArrowDirection.right
        }
        else if(iconViewFrame.maxY+infoViewBoxFrame.height<radarPlotView.frame.height)
        {
            return InfoViewArrowDirection.down

        }
        else
        {
            return InfoViewArrowDirection.up

        }
    }
    
    func positionInfoViewForForIcon(_ iconView:BeaconIconView,iconViewFrame:CGRect)->CGRect?{
        
        
        var beaconInfoViewFrame:CGRect!
        
        switch(iconView.viewArrowDirection!)
        {
        case InfoViewArrowDirection.left:
            

            beaconInfoViewFrame = CGRect(x: iconViewFrame.maxX,y: (iconViewFrame.origin.y+iconViewFrame.height/2) - infoViewBoxFrame.height/2, width: infoViewBoxFrame.width, height: infoViewBoxFrame.height)
            
            if beaconInfoViewFrame.origin.y < 0
                {beaconInfoViewFrame = CGRect(x: beaconInfoViewFrame.origin.x,y: 0, width: beaconInfoViewFrame.width, height: beaconInfoViewFrame.height)}
            if beaconInfoViewFrame.maxY > radarPlotView.frame.height
                {beaconInfoViewFrame = CGRect(x: beaconInfoViewFrame.origin.x,y: radarPlotView.frame.height - beaconInfoViewFrame.height, width: beaconInfoViewFrame.width, height: beaconInfoViewFrame.height)}
            break
            
        case InfoViewArrowDirection.right:
            
            beaconInfoViewFrame = CGRect(x: (iconViewFrame.origin.x - infoViewBoxFrame.width) ,y: (iconViewFrame.origin.y+iconViewFrame.height/2) - infoViewBoxFrame.height/2, width: infoViewBoxFrame.width, height: infoViewBoxFrame.height)
            
            if beaconInfoViewFrame.origin.y < 0
            {beaconInfoViewFrame = CGRect(x: beaconInfoViewFrame.origin.x,y: 0, width: beaconInfoViewFrame.width, height: beaconInfoViewFrame.height)}
            
            if beaconInfoViewFrame.maxY > radarPlotView.frame.height
            {beaconInfoViewFrame = CGRect(x: beaconInfoViewFrame.origin.x,y: radarPlotView.frame.height - beaconInfoViewFrame.height, width: beaconInfoViewFrame.width, height: beaconInfoViewFrame.height)}
            
            break
            
        case InfoViewArrowDirection.up:
            
              beaconInfoViewFrame = CGRect(x: (iconViewFrame.origin.x+iconViewFrame.width/2) - infoViewBoxFrame.width/2,y: iconViewFrame.maxY, width: infoViewBoxFrame.width, height: infoViewBoxFrame.height)
              
              if beaconInfoViewFrame.origin.x < 0
                {beaconInfoViewFrame = CGRect(x: 0,y: beaconInfoViewFrame.origin.y, width: beaconInfoViewFrame.width, height: beaconInfoViewFrame.height)}
              
              if beaconInfoViewFrame.maxX > radarPlotView.frame.width
                {beaconInfoViewFrame = CGRect(x: radarPlotView.frame.width - beaconInfoViewFrame.width,y: beaconInfoViewFrame.origin.y, width: beaconInfoViewFrame.width, height: beaconInfoViewFrame.height)}
            break
            
        case InfoViewArrowDirection.down:
             beaconInfoViewFrame = CGRect(x: (iconViewFrame.origin.x+iconViewFrame.width/2) - infoViewBoxFrame.width/2,y: (iconViewFrame.origin.y - infoViewBoxFrame.height), width: infoViewBoxFrame.width, height: infoViewBoxFrame.height)
             if beaconInfoViewFrame.origin.x < 0
                {beaconInfoViewFrame = CGRect(x: 0,y: beaconInfoViewFrame.origin.y, width: beaconInfoViewFrame.width, height: beaconInfoViewFrame.height)}
             
             if beaconInfoViewFrame.maxX > radarPlotView.frame.width
                {beaconInfoViewFrame = CGRect(x: radarPlotView.frame.width - beaconInfoViewFrame.width,y: beaconInfoViewFrame.origin.y, width: beaconInfoViewFrame.width, height: beaconInfoViewFrame.height)}
             
            break
        
        case InfoViewArrowDirection.none:
            return nil
        }
        
        if beaconInfoView != nil
            {radarPlotView.bringSubview(toFront: beaconInfoView!)}
        
        radarPlotView.bringSubview(toFront: iconView)
        return beaconInfoViewFrame
        }
    
    }
    
extension BeaconRangingViewController:BeaconInfoViewDelegate{
    
    func beaconInfoViewTapped(_ beaconInfoView:BeaconInfoView){
        
        targettedBeacon = beaconInfoView.beaconData
        targettedBeaconType = beaconInfoView.beaconType
        
        self.performSegue(withIdentifier: TO_BEACON_DETAIL_SEGUE, sender: self)
    }
}
extension BeaconRangingViewController:BeaconIconViewDelegate{
    
    func beaconIconViewTapped(_ beaconIconView:BeaconIconView){
        
        if beaconIconView.viewArrowDirection == InfoViewArrowDirection.none{
            
            //Remove existing beaconview
            
            removeBeaconInfoview()
            
            //position it accordingly
            
            beaconIconView.viewArrowDirection = findInfoViewArrowPositionForIcon(beaconIconView)
            
            //create info view for clicked icon according to icon and add it to radarplot
            
            beaconInfoView = BeaconInfoView(iconView: beaconIconView,beaconDatas: beaconIconView.beaconObj, type: beaconIconView.type, rssi: beaconIconView.RSSI)
            
            beaconInfoView?.delegate = self
            
            if beaconInfoView != nil
            {
                radarPlotView.addSubview(beaconInfoView!)
                beaconIconView.infoView = beaconInfoView
            }
            
            // find out frame rect accordingly to Let right Up or Down
            
            let rectFor:CGRect? = positionInfoViewForForIcon(beaconIconView,iconViewFrame:beaconIconView.frame)
            
            if rectFor != nil
            {beaconInfoView?.frame = rectFor!}
        }
            
        else{
            
            //remove icon view
            
            removeBeaconInfoview()
        }
        
    }
}
extension BeaconRangingViewController:BeaconRangerDelegate{
    
    func beaconRangerDidRangeBeacon(_ beacon:AnyObject,type:BeaconVendorType,uuid:UUID)
        {plotRangedBeacon(beacon, type: type,uuid: uuid)}
    

    
}
