/***************************************************************************
 
 BeaconSelectionViewController.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import UIKit
let TO_BEACONUPDATEVC_SEGUE = "toBeaconUpdateVC"

class BeaconSelectionViewController: BaseViewController {
    
    
    @IBOutlet weak var headerLabel: UILabel!
    
    var addingItemType:ItemType!
    var selectedBeaconItemType:BeaconItemType!
    var parentVC : BaseViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        updateViewWithItemType()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func altbeaconButtonPressed(_ sender: UIButton) {
        
        selectedBeaconItemType = BeaconItemType.AltBeacon
        self.parentVC!.performSegue(withIdentifier: TO_BEACON_UPDATE_SEGUE, sender: self)
    }
    
    @IBAction func iBeaconButtonPressed(_ sender: UIButton) {
        
        selectedBeaconItemType = BeaconItemType.iBeacon
        self.parentVC!.performSegue(withIdentifier: TO_BEACON_UPDATE_SEGUE, sender: self)
    }
    
    func updateViewWithItemType(){
        
        switch(addingItemType!){
        case ItemType.Beacon:
            headerLabel.text = "Select beacon type"
            break
        case ItemType.Region:
            headerLabel.text = "Select beacon region type"
            break
        }
    }


}
