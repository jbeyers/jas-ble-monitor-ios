/***************************************************************************
 
 BeaconDetailViewController.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import UIKit
import CoreLocation

let detailCellIdentifier    = "BeaconDetailCell"
let tlmCellIdentifier       = "TLMDetailCell"
let beaconScanVCSegue       = "toBeaconVCID"

let DetailCellHeight:CGFloat = 60
let TLMDetailcellHeight:CGFloat = 150

class BeaconDetailViewController: BaseViewController {

    
    var beaconData:AnyObject!
    var beaconType:BeaconVendorType!
    
    var iBeaconData:CLBeacon!
    var eddystoneBeaconData:AtmelEddyStone!
    var altBeaconData:AtmelAltBeacon!
    
    var beaconDataType:BeaconType!
    var iBeaconURL : URL? = nil
    
    @IBOutlet weak var beaconDetailTableView: UITableView!
    @IBOutlet weak var configureButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureButton.isHidden = true
        findoutTypeOfBeacon()
        beaconDetailTableView.reloadData()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func findoutTypeOfBeacon(){
        
        var navBarTitle = ""
        
        switch(beaconType!){
        case BeaconVendorType.nativeiBeacon:
            
            iBeaconData = beaconData as! CLBeacon
            beaconDataType = BeaconType.iBeacon
            navBarTitle = "iBeacon"
            
            let urlString = Util.getURLForiBeacon(iBeaconData)
            if urlString != nil {
                iBeaconURL = URL(string: urlString!)
            }
            
            break
        case BeaconVendorType.thirdPatyBeacon:
            
            let atmelBeaconData = beaconData as! AtmelBeacon
            beaconDataType = atmelBeaconData.beaconType
            switch(atmelBeaconData.beaconType){
            case BeaconType.eddystone:
                eddystoneBeaconData = atmelBeaconData as! AtmelEddyStone
                
                if eddystoneBeaconData.urlFrame != nil
                {
                    configureButton.isHidden = false // Show configure button only when URL frame is present
                }

                navBarTitle = "Eddystone"
                break
            case BeaconType.altBeacon:
                altBeaconData = atmelBeaconData as! AtmelAltBeacon
                
                let urlString = Util.getURLForAltBeacon(altBeaconData)
                
                if urlString != nil {
                    altBeaconData.url = URL(string: urlString!)
                }
                else{
                    altBeaconData.url = nil
                }
                navBarTitle = "AltBeacon"
                break
            default: break
            }
            break
        }
        self.navigationItem.title = navBarTitle
    }
    
    
    
    @IBAction func configureButtonAction(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "", message: "Press OK button after long pressing SW0 button in hardware", preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "OK", style: .default) { (UIAlertAction) -> Void in
            
            DispatchQueue.main.async(execute: { () -> Void in
                self.performSegue(withIdentifier: beaconScanVCSegue, sender: self)
            })
        }
        let alertCancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alert.addAction(alertAction)
        alert.addAction(alertCancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }


}
extension BeaconDetailViewController:TargettedBeaconDelegate{
    func beaconRangerDidRangeTargettedBeacon(_ beacon:AnyObject,type:BeaconVendorType){
        beaconData = beacon

        if type == beaconType {
        
            findoutTypeOfBeacon()
            
            if beaconDetailTableView != nil {
                beaconDetailTableView.reloadData()
            }
        }
    }

}
extension BeaconDetailViewController:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        
        if beaconDataType == BeaconType.eddystone && indexPath.row == 5
        {return TLMDetailcellHeight}
        else
        {return DetailCellHeight}
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if (beaconDataType == BeaconType.eddystone && indexPath.row == 3) || (beaconDataType == BeaconType.iBeacon && indexPath.row == 5) || (beaconDataType == BeaconType.altBeacon && indexPath.row == 5) {
            
            let cell = tableView.cellForRow(at: indexPath) as! BeaconDetailTableViewCell
            
            let url = URL(string: cell.detalLabel.text!)
            
            if url != nil {
                UIApplication.shared.openURL(url!)
            }
        }
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
}
extension BeaconDetailViewController:UITableViewDataSource{
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        switch(beaconDataType!){
        case BeaconType.altBeacon:
            
            if altBeaconData.url != nil {
                return 6
            }
            return 5
            
        case BeaconType.eddystone:
                return 6
        case BeaconType.iBeacon:
            
            if iBeaconURL != nil {
                return 6
            }
            return 5
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        switch(beaconDataType!){
        
        case BeaconType.altBeacon:
            
            switch(indexPath.row){
            case 0:
                let devicellIdentifier = "BeaconDetailCell"
                let cell = tableView.dequeueReusableCell(withIdentifier: devicellIdentifier) as! BeaconDetailTableViewCell
                cell.headerLabel.text = "UUID"
                cell.detalLabel.text =  "\(altBeaconData.deviceUUID.uuidString)"
                return cell
            case 1:
                let devicellIdentifier = "BeaconDetailCell"
                let cell = tableView.dequeueReusableCell(withIdentifier: devicellIdentifier) as! BeaconDetailTableViewCell
                cell.headerLabel.text = "MANUFACTURING ID"
                
                if  altBeaconData.manufacturingID != nil {
                    cell.detalLabel.text =  "\(altBeaconData.manufacturingID!)"
                }else{
                    cell.detalLabel.text =  "\(String(describing: altBeaconData.manufacturingID))"
                }
                
                return cell
            case 2:
                let devicellIdentifier = "BeaconDetailCell"
                let cell = tableView.dequeueReusableCell(withIdentifier: devicellIdentifier) as! BeaconDetailTableViewCell
                cell.headerLabel.text =  "ID1"
                cell.detalLabel.text  =  "0X" + String(format: "%04X", altBeaconData.majorID)
                return cell
            case 3:
                let devicellIdentifier = "BeaconDetailCell"
                let cell = tableView.dequeueReusableCell(withIdentifier: devicellIdentifier) as! BeaconDetailTableViewCell
                cell.headerLabel.text =  "ID2"
                cell.detalLabel.text  =  "0X" + String(format: "%04X", altBeaconData.minorID)
                return cell
            case 4:
                let devicellIdentifier = "BeaconDetailCell"
                let cell = tableView.dequeueReusableCell(withIdentifier: devicellIdentifier) as! BeaconDetailTableViewCell
                cell.headerLabel.text =  "RSSI"
                cell.detalLabel.text  =  "\(altBeaconData.RSSIValue) dBm"
                return cell
            case 5:
                let devicellIdentifier = "BeaconDetailCell"
                let cell = tableView.dequeueReusableCell(withIdentifier: devicellIdentifier) as! BeaconDetailTableViewCell
                cell.headerLabel.text =  "URL"
                
                if altBeaconData.url != nil {
                    cell.detalLabel.text  =  "\(altBeaconData.url!)"
                }
                else{
                    cell.detalLabel.text  =  "\(String(describing: altBeaconData.url))"
                }
                
                return cell
                
            default:break
                
            }
            
        case BeaconType.eddystone:
            switch(indexPath.row){
            case 0:
                let devicellIdentifier = "BeaconDetailCell"
                let cell = tableView.dequeueReusableCell(withIdentifier: devicellIdentifier) as! BeaconDetailTableViewCell
                cell.headerLabel.text = "NAMESPACE ID"
                
                if eddystoneBeaconData.uidFrame != nil
                    {cell.detalLabel.text =  "\(eddystoneBeaconData.uidFrame!.nameSpaceData)"}
               
                
                return cell
            case 1:
                let devicellIdentifier = "BeaconDetailCell"
                let cell = tableView.dequeueReusableCell(withIdentifier: devicellIdentifier) as! BeaconDetailTableViewCell
                cell.headerLabel.text =  "INSTANCE ID"
                if eddystoneBeaconData.uidFrame != nil
                    {cell.detalLabel.text  =  "\(eddystoneBeaconData.uidFrame!.instanceComponentData)"}
              
                 return cell
            case 2:
                let devicellIdentifier = "BeaconDetailCell"
                let cell = tableView.dequeueReusableCell(withIdentifier: devicellIdentifier) as! BeaconDetailTableViewCell
                cell.headerLabel.text =  "TX Power"
                
                if eddystoneBeaconData.uidFrame != nil{
                    cell.detalLabel.text  =  "\(eddystoneBeaconData.uidFrame!.txPower) dBm"
                }
                else if eddystoneBeaconData.urlFrame != nil {
                    cell.detalLabel.text  =  "\(eddystoneBeaconData.urlFrame!.txPower) dBm"
                }
              
                
                 return cell
            case 3:
                let devicellIdentifier = "BeaconDetailCell"
                let cell = tableView.dequeueReusableCell(withIdentifier: devicellIdentifier) as! BeaconDetailTableViewCell
                cell.headerLabel.text =  "URL"
                
                if eddystoneBeaconData.urlFrame != nil
                    {cell.detalLabel.text  =  "\(eddystoneBeaconData.urlFrame!.url!)"}
            

               
                 return cell
            case 4:
                let devicellIdentifier = "BeaconDetailCell"
                let cell = tableView.dequeueReusableCell(withIdentifier: devicellIdentifier) as! BeaconDetailTableViewCell
                cell.headerLabel.text =  "RSSI"
                
                cell.detalLabel.text  =  "\(eddystoneBeaconData.RSSIValue) dBm"
                 return cell
            case 5:
                let devicellIdentifier = "TLMDetailCell"
                let cell = tableView.dequeueReusableCell(withIdentifier: devicellIdentifier) as! TLMDetailTableViewCell
                
                if eddystoneBeaconData.tlmFrame != nil
                {
                    if eddystoneBeaconData.tlmFrame!.batteryVoltage != nil
                        {cell.batteryVoltageLabel.text = "\(eddystoneBeaconData.tlmFrame!.batteryVoltage!) mV"}
                    if eddystoneBeaconData.tlmFrame!.beaconTemperature != nil
                        {cell.beaconTemperatureLabel.text = "\(eddystoneBeaconData.tlmFrame!.beaconTemperature!) °C"}
                    if eddystoneBeaconData.tlmFrame!.advPDUCount != nil
                        {cell.pduCountLabel.text = "\(eddystoneBeaconData.tlmFrame!.advPDUCount!)"}
                    if eddystoneBeaconData.tlmFrame!.timeSinceBoot != nil
                        {cell.beaconUptimeLabel.text = "\(eddystoneBeaconData.tlmFrame!.timeSinceBoot!)"}
                }
                return cell
            default:break
            }
        case BeaconType.iBeacon:
            switch(indexPath.row){
            case 0:
                let devicellIdentifier = "BeaconDetailCell"
                let cell = tableView.dequeueReusableCell(withIdentifier: devicellIdentifier) as! BeaconDetailTableViewCell
                cell.headerLabel.text = "UUID"
                cell.detalLabel.text =  "\(iBeaconData.proximityUUID.uuidString)"
                 return cell
            case 1:
                let devicellIdentifier = "BeaconDetailCell"
                let cell = tableView.dequeueReusableCell(withIdentifier: devicellIdentifier) as! BeaconDetailTableViewCell
                cell.headerLabel.text =  "MAJOR"
                cell.detalLabel.text  =  "\(iBeaconData.major)"
              return cell
            case 2:
                let devicellIdentifier = "BeaconDetailCell"
                let cell = tableView.dequeueReusableCell(withIdentifier: devicellIdentifier) as! BeaconDetailTableViewCell
                cell.headerLabel.text =  "MINOR"
                cell.detalLabel.text  =  "\(iBeaconData.minor)"
                return cell
            case 3:
                let devicellIdentifier = "BeaconDetailCell"
                let cell = tableView.dequeueReusableCell(withIdentifier: devicellIdentifier) as! BeaconDetailTableViewCell
                cell.headerLabel.text =  "RSSI"
                cell.detalLabel.text  =  "\(iBeaconData.rssi) dBm"
                 return cell
            case 4:
                let devicellIdentifier = "BeaconDetailCell"
                let cell = tableView.dequeueReusableCell(withIdentifier: devicellIdentifier) as! BeaconDetailTableViewCell
                cell.headerLabel.text =  "Proximity"
                cell.detalLabel.text  =  "\(String.getProximityString(iBeaconData.proximity))"
                return cell
            case 5 :
                let devicellIdentifier = "BeaconDetailCell"
                let cell = tableView.dequeueReusableCell(withIdentifier: devicellIdentifier) as! BeaconDetailTableViewCell
                cell.headerLabel.text =  "URL"
                cell.detalLabel.text  =  "\(iBeaconURL!)"
                return cell

                

            default:break

            }

            
        }
        return UITableViewCell()
    }
}





