//
//  ImmediateAlertService.swift

/* ***************************************************************************

ImmediateAlertService.swift



Copyright (c) 2015, Atmel Corporation. All rights reserved.
Released under NDA
Licensed under Atmel's Limited License Agreement.


THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Atmel Corporation: http://www.atmel.com

******************************************************************************/


import Foundation
import CoreBluetooth



class ImmediateAlertService{
    var alertLevelCharacteristics:CBCharacteristic?
    var service:CBService?
    var sourceDevice:BLESourceDevice?

    init(deviceInfoService:CBService?, andSourceDevice device:BLESourceDevice?){
        service = deviceInfoService
        sourceDevice = device
        alertLevelCharacteristics=service?.characteristics?.filter(){$0.uuid.isEqual(CBUUID(blecharacteristic: .AlertLevelCharacteristic))}.first
    }
    
    func writeImmediateAlertLevel(_ alertLevel:AlertLevel){
        
        var alertLevels=alertLevel.rawValue
        //let alertData:Data = Data(bytes: UnsafePointer<UInt8>(&alertLevels), count: 1)
        let alertData:Data = Data(bytes: &alertLevels, count: 1)
        self.sourceDevice?.writeValue(alertData, forCharacteristic: alertLevelCharacteristics, withAcknowledgement: false)
    }

}
