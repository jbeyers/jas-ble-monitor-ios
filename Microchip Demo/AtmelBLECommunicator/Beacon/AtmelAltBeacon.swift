/***************************************************************************
 
 AtmelAltBeacon.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import Foundation
import CoreBluetooth

let ATMEL_MFGID : UInt16 = 0x0013

class AtmelAltBeacon:AtmelBeacon{

    
    var deviceUUID          : UUID!
    var majorID             : UInt16!
    var minorID             : UInt16!
    var manufacturingID     : String?
    var url                 : URL?
    var rssiAtZeroMeter      : Int!
    
    init(frameData:Data , peripheral:CBPeripheral, RSSI: NSNumber!){
        
        super.init(peripheralObject: peripheral, type: BeaconType.altBeacon,RSSI:RSSI)
        
        let count = frameData.count
        
        if count >= 2 {
            
            //let mfgData = frameData.subdata(in: NSMakeRange(0, 2))
            let mfgData = frameData.subdata(in: 0..<2)
            let mfgID : UInt16 = CFSwapInt16LittleToHost((mfgData as NSData).bytes.bindMemory(to: UInt16.self, capacity: mfgData.count).pointee)
            
            if mfgID == ATMEL_MFGID {
                manufacturingID = "Atmel Corporation"
            }
            else{
                manufacturingID = String(format: "0x%2X", mfgID)
            }
        }
        
        if count >= 24
        {
            //let uuid = CBUUID(data: frameData.subdata(in: NSMakeRange(4, 16)))
            let uuid = CBUUID(data: frameData.subdata(in: 4..<20))
            deviceUUID = UUID(uuidString: uuid.uuidString)
            
            // Swapping is required since data is UUID, major and minor ids are represented in big endian format
            
            //let majorData = frameData.subdata(in: NSMakeRange(20, 2))
            let majorData = frameData.subdata(in: 20..<22)
            
            //let minorData = frameData.subdata(in: NSMakeRange(22, 2))
            let minorData = frameData.subdata(in: 22..<24)
            
            majorID = CFSwapInt16BigToHost((majorData as NSData).bytes.bindMemory(to: UInt16.self, capacity: majorData.count).pointee)
            minorID = CFSwapInt16BigToHost((minorData as NSData).bytes.bindMemory(to: UInt16.self, capacity: minorData.count).pointee)
        }
        
        if count >= 25 {
            
            var rssiAtOneMeter : Int16 = 0
            var signFlag : Bool = false
            
            //let refRSSIData = frameData.subdata(in: NSMakeRange(24, 1))
            let refRSSIData = frameData.subdata(in: 24..<25)
            (refRSSIData as NSData).getBytes(&rssiAtOneMeter, range: NSMakeRange(0, 1))
            
            if rssiAtOneMeter & 0x80 == 0x80 {
                rssiAtOneMeter -= 0x1
                rssiAtOneMeter = ~(rssiAtOneMeter) & 0x00FF
                signFlag = true
            }

            rssiAtZeroMeter = (signFlag ? -1 * Int(rssiAtOneMeter) : Int(rssiAtOneMeter)) + 41
        }
    }
    
    
    
    override func addBeaconData(_ peripheralObject:CBPeripheral!,advData:[AnyHashable: Any]!, RSSI: NSNumber!){
        
        super.addBeaconData(peripheralObject, advData: advData, RSSI: RSSI)
        
        
        if let frameData = advData[CBAdvertisementDataManufacturerDataKey] as? Data{
            
            if frameData.isAltBeaconAdvertisementPacket(){
                let count = frameData.count
                
                if count >= 2 {
                    
                    //let mfgData = frameData.subdata(in: NSMakeRange(0, 2))
                    let mfgData = frameData.subdata(in: 0..<2)
                    let mfgID : UInt16 = CFSwapInt16LittleToHost((mfgData as NSData).bytes.bindMemory(to: UInt16.self, capacity: mfgData.count).pointee)
                    
                    if mfgID == ATMEL_MFGID {
                        manufacturingID = "Atmel Corporation"
                    }
                    else{
                        manufacturingID = String(format: "0x%2X", mfgID)
                    }
                }

                
                if count >= 26
                {
                    //let uuid = CBUUID(data: frameData.subdata(in: NSMakeRange(4, 16)))
                    let uuid = CBUUID(data: frameData.subdata(in: 4..<20))
                    deviceUUID = UUID(uuidString: uuid.uuidString)
                    
                    //let majorData = frameData.subdata(in: NSMakeRange(20, 2))
                    let majorData = frameData.subdata(in: 20..<22)
                    //let minorData = frameData.subdata(in: NSMakeRange(22, 2))
                    let minorData = frameData.subdata(in: 22..<24)
                    
                    majorID = CFSwapInt16BigToHost((majorData as NSData).bytes.bindMemory(to: UInt16.self, capacity: majorData.count).pointee)
                    minorID = CFSwapInt16BigToHost((minorData as NSData).bytes.bindMemory(to: UInt16.self, capacity: minorData.count).pointee)
                }
            }
        }
    }


    
}
