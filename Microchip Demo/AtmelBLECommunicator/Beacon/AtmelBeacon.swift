/***************************************************************************
 
 AtmelBeacon.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import Foundation
import CoreBluetooth


class AtmelBeacon:NSObject{
    
    var peripheral:CBPeripheral!
    var beaconType:BeaconType
    var RSSIValue:NSNumber!
   
    
     init(peripheralObject:CBPeripheral, type:BeaconType, RSSI: NSNumber!){
        peripheral = peripheralObject
        beaconType = type
        RSSIValue = RSSI
    }
    func addBeaconData(_ peripheralObject:CBPeripheral!,advData:[AnyHashable: Any]!, RSSI: NSNumber!){
        if peripheralObject != nil
            {peripheral = peripheralObject}
        RSSIValue = RSSI
    }
    class func addNewRangedBeacon(_ advData:[AnyHashable: Any]! ,peripheral:CBPeripheral!, RSSI: NSNumber!)->AtmelBeacon?{
        
        // Check whether the beacon is an Eddystone
        
        if let advertisementFrameList = advData[CBAdvertisementDataServiceDataKey]
            as? [AnyHashable: Any]
        {
                let uuid = CBUUID(string: "FEAA")
                if let frameData = (advertisementFrameList[uuid] as? Data)
                {
                    return AtmelEddyStone(frameData: frameData, peripheral: peripheral, RSSI:RSSI)
                }
        }
        
        
        // Check whether the beacon is AltBeacon
        
        if let beaconData = advData[CBAdvertisementDataManufacturerDataKey] as? Data{
                        
            if beaconData.isAltBeaconAdvertisementPacket(){
                return AtmelAltBeacon(frameData: beaconData, peripheral: peripheral, RSSI: RSSI)
            }
        }
        return nil
    }
}
