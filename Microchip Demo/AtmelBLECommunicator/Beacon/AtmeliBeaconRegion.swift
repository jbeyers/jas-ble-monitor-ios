/***************************************************************************
 
 AtmeliBeaconRegion.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import UIKit
import CoreLocation
class AtmeliBeaconRegion: AtmelBeaconRegion {
    
    var uuid:UUID!
    var major:NSNumber?
    var minor:NSNumber?
    
    init(withUUID:UUID,majorValue:NSNumber?,minorValue:NSNumber?,range:Bool,nameString:String){
        super.init()
        name = nameString
        isRanging = range
        beaconType = BeaconItemType.iBeacon
        uuid = withUUID
        major = majorValue
        minor = minorValue
    }
    
    init(withDictionary:NSDictionary){
        super.init()
        
        beaconType = BeaconItemType.iBeacon
        isRanging = withDictionary.value(forKey: RANGEKEY) as! Bool
        
        name = withDictionary.value(forKey: NAMEKEY) as! String
        uuid = UUID(uuidString:withDictionary.value(forKey: UUIDKEY) as! String)
        major = withDictionary.value(forKey: MAJORKEY) as? NSNumber
        minor = withDictionary.value(forKey: MINORKEY) as? NSNumber
    }
    override func isEqual(_ object: Any?) -> Bool {
        
        if let aiBeaconItem = object as? AtmeliBeaconRegion{
            
            //if aiBeaconItem.uuid.isEqual(self.uuid){
            if aiBeaconItem.uuid == (self.uuid){
                
                if aiBeaconItem.major == nil && self.major == nil
                    {return true}
                else if aiBeaconItem.major != nil && self.major != nil {
                    
                    if aiBeaconItem.major!.isEqual(to: self.major!){
                        if aiBeaconItem.minor == nil && self.minor == nil
                            {return true}
                        else if aiBeaconItem.minor != nil && self.minor != nil
                        {
                            if aiBeaconItem.minor!.isEqual(to: self.minor!)
                            {return true}
                        }
                    }
                    
                    
                }
                
            }
        }
        return false
    }

    
    override func convertToDict()->NSDictionary?{
        let returndict = NSMutableDictionary()
        
        returndict.setObject(uuid.uuidString, forKey: UUIDKEY as NSCopying)
        returndict.setObject(isRanging, forKey: RANGEKEY as NSCopying)

        if major != nil
            {returndict.setObject(major!, forKey: MAJORKEY as NSCopying)}
        if minor != nil
            {returndict.setObject(minor!, forKey: MINORKEY as NSCopying)}

        returndict.setObject(name, forKey: NAMEKEY as NSCopying)
        returndict.setObject(beaconType.rawValue, forKey: BEACONTYPEKEY as NSCopying)
        return returndict
    }
    
    func toCLBeaconRegion()->CLBeaconRegion{
        if minor != nil
        {return CLBeaconRegion(proximityUUID: uuid, major: UInt16(major!.intValue), minor: UInt16(minor!.intValue), identifier: name)}
        if major != nil
        {return CLBeaconRegion(proximityUUID:uuid, major: UInt16(major!.intValue),identifier: name)}
        else
        {return CLBeaconRegion(proximityUUID: uuid,identifier: name)}
    }

}
