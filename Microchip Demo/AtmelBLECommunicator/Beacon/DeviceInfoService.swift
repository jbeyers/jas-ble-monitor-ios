//
//  DeviceInfoService.swift

/* ***************************************************************************

DeviceInfoService.swift



Copyright (c) 2015, Atmel Corporation. All rights reserved.
Released under NDA
Licensed under Atmel's Limited License Agreement.


THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Atmel Corporation: http://www.atmel.com

******************************************************************************/


import Foundation
import CoreBluetooth

protocol DeviceInfoServiceDelegate:NSObjectProtocol{
    func deviceInfoServiceDidReadAllData(_ deviceInfoService:DeviceInfoService)
}

class DeviceInfoDetails {
    var manufactureName:String?
    var modelNumber:String?
    var serialNumber:String?
    var hardwareRevision:String?
    var firmwareRevision:String?
    var softwareRevision:String?
    var systemId:SystemID?
    var regulatoryCertificateData:Data?
    var pnpID:PnPID?
    
    
}


//Manufacturer Name, Model Number, Serial Number, Hardware Revision, Firmware Revision, Software Revision, System ID, IEEE 11073-20601 Regulatory Certification Data List, PnP ID.
//
//2. The System ID and PnP ID are hexadecimal so it has to be printed in hexadecimal with 0x appended at first.
extension DeviceInfoDetails{
    func getValuesAsArray() -> NSArray{
    
    let returnArray:NSMutableArray = NSMutableArray()
       
        if let _manufactureName = self.manufactureName{
            
            //Manufacturer
            returnArray.add([ "Manufacturer Name":_manufactureName])
        }
        if let _modelNumber = self.modelNumber{
            
            returnArray.add([ "Model Number":_modelNumber])

        }
        if let _serialNumber = self.serialNumber{
            
            returnArray.add(["Serial Number":_serialNumber])

        }
        if let _hardwareRevision = self.hardwareRevision{
            
            returnArray.add([ "Hardware Revision":_hardwareRevision])

        }
        if let _firmwareRevision = self.firmwareRevision{
            
            returnArray.add([ "Firmware Revision":_firmwareRevision])

        }
        if let _softwareRevision = self.softwareRevision{
            
            returnArray.add(["Software Revision":_softwareRevision])

        }
        if let _systemId = self.systemId{
            
            returnArray.add(["System ID":_systemId])

        }
        if let _regulatoryCertificateData = self.regulatoryCertificateData{
            
            returnArray.add(["Regulatory Certificate Data":_regulatoryCertificateData])
 
        }
        if let _pnpID = self.pnpID{
            
            returnArray.add(["Pnp ID":_pnpID])

        }
        
    return returnArray
    
    
    }
}
class PnPID{
    var vendorIDSource:UInt8?
    var vendorID:UInt16?
    var productID:UInt16?
    var productVersion:UInt16?
    
    var vendorIDSourceData:Data?
    var vendorIDData:Data?
    var productIDData:Data?
    var productVersionData:Data?
    
    init(data:Data){
//        var byteArray8 = [UInt8](count:data.length, repeatedValue: 0)

        
        //vendorIDSourceData = data.subdata(in: NSMakeRange(0, 1))
        vendorIDSourceData = data.subdata(in: 0..<1)
        vendorIDSource = (vendorIDSourceData! as NSData).bytes.bindMemory(to: UInt8.self, capacity: vendorIDSourceData!.count).pointee
        

        
        //vendorIDData = data.subdata(in: NSMakeRange(1, 2))
        vendorIDData = data.subdata(in: 1..<3)
        vendorID = (vendorIDData! as NSData).bytes.bindMemory(to: UInt16.self, capacity: vendorIDData!.count).pointee

   
       
        //productIDData = data.subdata(in: NSMakeRange(3, 2))
        productIDData = data.subdata(in: 3..<5)
        productID = (productIDData! as NSData).bytes.bindMemory(to: UInt16.self, capacity: productIDData!.count).pointee

       
        //productVersionData = data.subdata(in: NSMakeRange(5, 2))
        productVersionData = data.subdata(in: 5..<7)
        productVersion = (productVersionData! as NSData).bytes.bindMemory(to: UInt16.self, capacity: productVersionData!.count).pointee
    }
    
}
class SystemID{
    var manufacturerIdentifier:UInt64?
    var organizationallyUniqueIdentifier:UInt32?
    
    var manufacturerIdentifierData:Data!
    var organizationallyUniqueIdentifierData:Data!
    
    init(data:Data){
        var byteArray = [UInt8](repeating: 0, count: data.count)
        
        (data as NSData).getBytes(&byteArray, length: data.count)
        var rawValue:UInt32 = 0
        
        for i in 0...3{
            let shiftAmount:UInt = UInt(i*8)
            let currentshiftedValue:UInt32 = UInt32(byteArray[i])
            rawValue = rawValue | ( currentshiftedValue << UInt32(shiftAmount))
        }
        let value:UInt64 = UInt64(byteArray[4])
        var asd :UInt64 = UInt64(value << 32)
        asd = asd + UInt64(rawValue)
        
        rawValue=0

        for i in 5...7{
            let shiftAmount:UInt = UInt((i-5)*8)
            let currentshiftedValue:UInt32 = UInt32(byteArray[i])
            rawValue = rawValue | ( currentshiftedValue << UInt32(shiftAmount))
        }
        manufacturerIdentifier = asd
        organizationallyUniqueIdentifier = rawValue
        
        //manufacturerIdentifierData = data.subdata(in: NSMakeRange(0, 5))
        //organizationallyUniqueIdentifierData = data.subdata(in: NSMakeRange(5, 3))
        manufacturerIdentifierData = data.subdata(in: 0..<5)
        organizationallyUniqueIdentifierData = data.subdata(in: 5..<8)
    }
}

class DeviceInfoService{
    
    var delegate:DeviceInfoServiceDelegate?
    var deviceInfo:DeviceInfoDetails?
    var service:CBService?
    var sourceDevice:BLESourceDevice?
    
    var manufacturerNameStringCharacteristics:CBCharacteristic?
    var modelNumberStringCharacteristics:CBCharacteristic?
    var serialNumberStringCharacteristics:CBCharacteristic?
    var hardwareRevisionStringCharacteristics:CBCharacteristic?
    var firmwareRevisionStringCharacteristics:CBCharacteristic?
    var softwareRevisionStringCharacteristics:CBCharacteristic?
    var systemIDCharacteristics:CBCharacteristic?
    var IEEE_RegulatoryCertificationDataListCharacteristics:CBCharacteristic?
    var PnP_IDCharacteristics:CBCharacteristic?
    
    var charArray:Array<AnyObject?>
    var numberOfActiveChars:Int = 0
    var didReadChars:Int = 0

    init(deviceInfoService:CBService, andSourceDevice device:BLESourceDevice, andDelegate delegate:DeviceInfoServiceDelegate){
    service = deviceInfoService
    sourceDevice = device
    self.delegate = delegate
    manufacturerNameStringCharacteristics=(service?.characteristics )?.filter(){$0.uuid.isEqual(CBUUID(blecharacteristic: BLECharacteristic.ManufacturerNameStringCharacteristics))}.first
    modelNumberStringCharacteristics=(service?.characteristics )?.filter(){$0.uuid.isEqual(CBUUID(blecharacteristic: BLECharacteristic.ModelNumberStringCharacteristics))}.first
    serialNumberStringCharacteristics=(service?.characteristics )?.filter(){$0.uuid.isEqual(CBUUID(blecharacteristic: BLECharacteristic.SerialNumberStringCharacteristics))}.first
    hardwareRevisionStringCharacteristics=(service?.characteristics )?.filter(){$0.uuid.isEqual(CBUUID(blecharacteristic: BLECharacteristic.HardwareRevisionStringCharacteristics))}.first
    firmwareRevisionStringCharacteristics=(service?.characteristics )?.filter(){$0.uuid.isEqual(CBUUID(blecharacteristic: BLECharacteristic.FirmwareRevisionStringCharacteristics))}.first
    softwareRevisionStringCharacteristics=(service?.characteristics )?.filter(){$0.uuid.isEqual(CBUUID(blecharacteristic: BLECharacteristic.SoftwareRevisionStringCharacteristics))}.first
    systemIDCharacteristics=(service?.characteristics )?.filter(){$0.uuid.isEqual(CBUUID(blecharacteristic: BLECharacteristic.SystemIDCharacteristics))}.first
    IEEE_RegulatoryCertificationDataListCharacteristics=(service?.characteristics )?.filter(){$0.uuid.isEqual(CBUUID(blecharacteristic: BLECharacteristic.IEEERegulatoryCertificationCharacteristics))}.first
    PnP_IDCharacteristics=(service?.characteristics )?.filter(){$0.uuid.isEqual(CBUUID(blecharacteristic: BLECharacteristic.PnPIDCharacteristics))}.first
        
     charArray = [manufacturerNameStringCharacteristics,modelNumberStringCharacteristics,serialNumberStringCharacteristics,hardwareRevisionStringCharacteristics,firmwareRevisionStringCharacteristics,softwareRevisionStringCharacteristics,systemIDCharacteristics,IEEE_RegulatoryCertificationDataListCharacteristics,PnP_IDCharacteristics]
        sourceDevice?.operationsDelegate = self

    }
    
        func readDeviceInfo(){
            
            didReadChars = 0
            deviceInfo = DeviceInfoDetails()
            var i = 0
            
            numberOfActiveChars = 0
           
            while i<charArray.count{
                
                let obj: AnyObject? = charArray [i]
                
                if (obj != nil) {
                    
                    self.numberOfActiveChars += 1
//                    sourceDevice?.readValueForCharacteristic(obj as? CBCharacteristic)
                }
                i += 1
            }

        readcharacteristicAtIndex(0)
    }
    
    // Read characteristics one by one
    
    func readcharacteristicAtIndex(_ index: Int){
        
        if index < charArray.count {
            sourceDevice?.readValueForCharacteristic(charArray[index] as? CBCharacteristic)
        }
        
        if didReadChars == numberOfActiveChars
        {
            self.delegate?.deviceInfoServiceDidReadAllData(self)
        }
    }
    
    
    
    func didReadDataforCharacteristic(_ serviceChar:CBCharacteristic){
        
        if didReadChars < numberOfActiveChars {
            didReadChars += 1
        }
        
        if serviceChar == manufacturerNameStringCharacteristics
            {deviceInfo?.manufactureName = NSString(data: serviceChar.value!, encoding: String.Encoding.utf8.rawValue) as? String}
       
        else if serviceChar == modelNumberStringCharacteristics
            {deviceInfo?.modelNumber = NSString(data: serviceChar.value!, encoding: String.Encoding.utf8.rawValue) as? String}
            
        else if serviceChar == serialNumberStringCharacteristics
            {deviceInfo?.serialNumber = NSString(data: serviceChar.value!, encoding: String.Encoding.utf8.rawValue) as? String}
            
        else if serviceChar == hardwareRevisionStringCharacteristics
            {deviceInfo?.hardwareRevision = NSString(data: serviceChar.value!, encoding: String.Encoding.utf8.rawValue) as? String}
            
        else if serviceChar == firmwareRevisionStringCharacteristics
            {deviceInfo?.firmwareRevision = NSString(data: serviceChar.value!, encoding: String.Encoding.utf8.rawValue) as? String}
            
        else if serviceChar == softwareRevisionStringCharacteristics
            {deviceInfo?.softwareRevision = NSString(data: serviceChar.value!, encoding: String.Encoding.utf8.rawValue) as? String}
            
        else if serviceChar == systemIDCharacteristics
            {deviceInfo?.systemId = SystemID(data: serviceChar.value!)}
            
        else if serviceChar == IEEE_RegulatoryCertificationDataListCharacteristics
            {deviceInfo?.regulatoryCertificateData = serviceChar.value}
            
        else if serviceChar == PnP_IDCharacteristics
            {deviceInfo?.pnpID = PnPID(data: serviceChar.value!)}
        
        // If all available characteristics are read call the delegate 
        
//        if didReadChars == numberOfActiveChars
//            {self.delegate?.deviceInfoServiceDidReadAllData(self)}
        
        readcharacteristicAtIndex(didReadChars)
        
    }
}
extension DeviceInfoService:BLESourceDeviceOperationsDelegate{
    
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice, didUpdateValueForCharacteristic characteristic: CBCharacteristic!, error: NSError!){
        
       didReadDataforCharacteristic(characteristic)
    }
    
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice,didWriteValueForCharacteristic characteristic: CBCharacteristic!, error: NSError!){
        
        //NEGLECT
    }
    
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice,didUpdateNotificationStateForCharacteristic characteristic: CBCharacteristic!, error: NSError!){
        
        //NEGLECT

    }
}
