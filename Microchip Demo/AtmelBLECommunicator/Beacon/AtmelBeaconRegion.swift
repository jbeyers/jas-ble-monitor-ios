/***************************************************************************
 
 AtmelBeaconRegion.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/
import UIKit

class AtmelBeaconRegion: NSObject {
    
    var name:String!
    var beaconType:BeaconItemType!
    var isRanging:Bool!
    
    override init(){
        super.init()
    }
    class func beaconRangeWith(_ dictionary:NSDictionary)->AtmelBeaconRegion?{
        
        if let beacontype = dictionary.value(forKey: BEACONTYPEKEY) as? String{
            switch(BeaconItemType(rawValue: beacontype)!){
                
            case BeaconItemType.AltBeacon:
                return AtmelAltBeaconRegion(withDictionary: dictionary)
            case BeaconItemType.iBeacon:
                return AtmeliBeaconRegion(withDictionary: dictionary)
                
            }
        }
        return nil
    }
    class func getDictArray(_ fromArray:[AtmelBeaconRegion])->[NSDictionary]{
        
        var newBeaconRangeList = [NSDictionary]()

        for beaconRange in fromArray{
            if let beaconItemDict = beaconRange.convertToDict(){
                newBeaconRangeList.append(beaconItemDict)
               
            }
        }
        
        return newBeaconRangeList
    }
     func convertToDict()->NSDictionary?{
        return nil
    }
}
