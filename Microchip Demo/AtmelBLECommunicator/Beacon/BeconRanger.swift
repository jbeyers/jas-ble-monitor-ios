/***************************************************************************
 
 BeaconRanger.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import Foundation
import CoreBluetooth
import CoreLocation

let BEACONSITEMS = "beaconArrayList"
let BEACONREGIONS = "beaconRangesList"

enum BeaconVendorType{
    
    case nativeiBeacon
    case thirdPatyBeacon
    
}
let PROXIMITYUUID = "f7826da6-4fa2-4e98-8024-bc5b71e0893e"
let beaconidentifier = "cloudbeacon"
let MAJOR = NSNumber(value: 4660 as Int)
let MINOR = NSNumber(value: 22136 as Int)



protocol TargettedBeaconDelegate:NSObjectProtocol{
    
    func beaconRangerDidRangeTargettedBeacon(_ beacon:AnyObject,type:BeaconVendorType)
}

protocol BeaconRangerDelegate:NSObjectProtocol{
    
    func beaconRangerDidRangeBeacon(_ beacon:AnyObject,type:BeaconVendorType,uuid:UUID)
}
class BeaconRanger:NSObject{
    
    fileprivate var rangedBeaconsArray = [AtmelBeacon]()
    var delegate:BeaconRangerDelegate?
    var locationManager:CLLocationManager!
    var communicator:BLECommunicator!
    
    var activeiBeaconRegions:[AtmeliBeaconRegion]?
    var inactiveiBeaconRegions:[AtmeliBeaconRegion]?
    var altBeaconRegions:[AtmelAltBeaconRegion]?
    var inactiveAltBeaconRegions :[AtmelAltBeaconRegion]?
    
    var shouldRangeiBeacons:Bool = false
    var shouldRangeAltBeacons:Bool = false
    
    var isIntialized = false
    var shouldStartScan = false
    
    var targettedBeaconUUID:UUID?
    var targettediBeacon:CLBeacon?

    var targettedBeaconDelegate:TargettedBeaconDelegate?
    
    init(withDelegate:BeaconRangerDelegate?) {
        super.init()
        communicator = BLECommunicator(scanmode: ScanMode.passive, forBLEProfiles: nil, withDelegate: self)
        print("Beacon Ranger Initialized")
        delegate = withDelegate
    }
    
    func updateBeaconRanges(){
        if let activeRegions = (BeaconRanger.getSavedBeaconRegionList()?.filter(){$0.isRanging}){
            activeiBeaconRegions = (activeRegions.filter(){$0.beaconType == BeaconItemType.iBeacon}) as? [AtmeliBeaconRegion]
            altBeaconRegions = (activeRegions.filter(){$0.beaconType == BeaconItemType.AltBeacon}) as? [AtmelAltBeaconRegion]
            
            }
        if let inactiveRegions = (BeaconRanger.getSavedBeaconRegionList()?.filter(){!($0.isRanging)}){
             inactiveiBeaconRegions = (inactiveRegions.filter(){$0.beaconType == BeaconItemType.iBeacon}) as? [AtmeliBeaconRegion]
            inactiveAltBeaconRegions =  (inactiveRegions.filter(){$0.beaconType == BeaconItemType.AltBeacon}) as? [AtmelAltBeaconRegion]
        }
        if activeiBeaconRegions != nil{
            shouldRangeiBeacons = (activeiBeaconRegions!.count != 0)
        }
        
        if altBeaconRegions != nil{
            shouldRangeAltBeacons = (altBeaconRegions!.count != 0)
        }
    }
    
    func startRanging(){
        shouldStartScan = true

        if isIntialized{
            updateBeaconRanges()
            communicator.startScan(nil)
            startiBeaconRanging()
        }
    }
    
    func stopRanging(){
        shouldStartScan = false
        communicator.stopScan()
        stopiBeaconRanging()
    }
    
    func clearBeacons() {
        
        if rangedBeaconsArray.count != 0 {
            rangedBeaconsArray.removeAll()
        }
    }
    
}
//MARK: Beacon DB Methods

extension BeaconRanger{
    
    class func getSavedBeaconItemList() -> [AtmelBeaconItem]?{
     
        if let beaconDictArray = UserDefaults.standard.value(forKey: BEACONSITEMS) as? [NSDictionary]{
            
           if beaconDictArray.count > 0 {
            
            var returnArray = [AtmelBeaconItem]()
            for dict in beaconDictArray{
               
                if let beaconItem = AtmelBeaconItem.beaconItemWith(dict)
                    {returnArray.append(beaconItem)}
            }
            if returnArray.count > 0
                {return returnArray}
            }
        }
        return nil
    }
    
    class func isBeaconItemPresentInDB(_ beaconItem:AtmelBeaconItem)->Bool{
        if let beaconList = getSavedBeaconItemList(){
            if let _ = beaconList.index(where: {$0.isEqual(beaconItem)}){
                return true
            }
        }
        return false
    }
    
    
    class func removeBeaconFromDB(_ beaconItem : AtmelBeaconItem){
        
        if var beaconList = getSavedBeaconItemList(){
            if let index = beaconList.index(where: {$0.isEqual(beaconItem)}){
                beaconList.remove(at: index)
            }
            UserDefaults.standard.set(AtmelBeaconItem.getDictArray(beaconList), forKey: BEACONSITEMS)
        }
    }
    
    
    
    class func saveBeaconItem(_ beaconItem:AtmelBeaconItem){
        
        if var beaconList = getSavedBeaconItemList(){
            if let index = beaconList.index(where: {$0.isEqual(beaconItem)}){
                beaconList[index] = beaconItem
            }
            else{
                beaconList.append(beaconItem)
            }
            UserDefaults.standard.set(AtmelBeaconItem.getDictArray(beaconList), forKey: BEACONSITEMS)
        }
        else{
            if let beaconItemDict = beaconItem.convertToDict(){
                var newBeaconList = [NSDictionary]()
                newBeaconList.append(beaconItemDict)
                UserDefaults.standard.set(newBeaconList, forKey: BEACONSITEMS)
            }
        }
    }
    
    
    class func replaceBeacon(_ beaconItem:AtmelBeaconItem, withBeacon newBeaconItem:AtmelBeaconItem){
        
        if var beaconList = getSavedBeaconItemList(){
            if let index = beaconList.index(where: {$0.isEqual(beaconItem)}){
                beaconList[index] = newBeaconItem
            }
            
            UserDefaults.standard.set(AtmelBeaconItem.getDictArray(beaconList), forKey: BEACONSITEMS)
        }
    }
    
    
    class func getSavedBeaconRegionList() -> [AtmelBeaconRegion]?{
        if let rangeDictArray = UserDefaults.standard.value(forKey: BEACONREGIONS) as? [NSDictionary]{
            
            if rangeDictArray.count > 0 {
                
                var returnArray = [AtmelBeaconRegion]()
                for dict in rangeDictArray{
                    
                    if let beaconItem = AtmelBeaconRegion.beaconRangeWith(dict)
                    {returnArray.append(beaconItem)}
                }
                if returnArray.count > 0
                {return returnArray}
            }
        }
        
        return nil
    }
    
    class func isBeaconRegionPresentInDB(_ beaconRange:AtmelBeaconRegion)->Bool{
        
        if let beaconRangeList = getSavedBeaconRegionList(){
            if let _ = beaconRangeList.index(where: {$0.isEqual(beaconRange)}){
                return true
            }
        }
        return false
    }
    
    
    class func removeBeaconRegionFromDB(_ beaconRange:AtmelBeaconRegion){
        
        if var beaconRangeList = getSavedBeaconRegionList(){
            if let index = beaconRangeList.index(where: {$0.isEqual(beaconRange)}){
                beaconRangeList.remove(at: index)
            }
            UserDefaults.standard.set(AtmelBeaconRegion.getDictArray(beaconRangeList), forKey: BEACONREGIONS)
        }
    }
    
    
    
    
    class func saveBeaconRegion(_ beaconRange:AtmelBeaconRegion){
        if var beaconRangeList = getSavedBeaconRegionList(){
            if let index = beaconRangeList.index(where: {$0.isEqual(beaconRange)}){
                beaconRangeList[index] = beaconRange
            }
            else{
                beaconRangeList.append(beaconRange)
            }
            UserDefaults.standard.set(AtmelBeaconRegion.getDictArray(beaconRangeList), forKey: BEACONREGIONS)
        }
        else{
            if let beaconItemDict = beaconRange.convertToDict(){
                var newBeaconRangeList = [NSDictionary]()
                newBeaconRangeList.append(beaconItemDict)
                UserDefaults.standard.set(newBeaconRangeList, forKey: BEACONREGIONS)
            }
        }
    }
    
    class func replaceRegion(_ beaconRegion : AtmelBeaconRegion, withRegion newBeaconRegion: AtmelBeaconRegion){
        
        if var beaconRangeList = getSavedBeaconRegionList(){
            if let index = beaconRangeList.index(where: {$0.isEqual(beaconRegion)}){
                beaconRangeList[index] = newBeaconRegion
            }
            UserDefaults.standard.set(AtmelBeaconRegion.getDictArray(beaconRangeList), forKey: BEACONREGIONS)
        }
    }
    
    
    class func getAllSavediBeaconUUIDs() -> [String]?{
        
        var uuidArray = [String]()
        
        if let beaconList = getSavedBeaconItemList() {
            let iBeaconList = beaconList.filter(){$0.beaconType == BeaconItemType.iBeacon}
            
            if iBeaconList.count != 0 {
                
                for ibeacon in iBeaconList {
                    uuidArray.append((ibeacon as! AtmeliBeaconItem).uuid.uuidString)
                }
            }
            
            if uuidArray.count > 0 {
                return uuidArray
            }
        }
        return nil
    }
    

}

//MARK: iBeacon Methods

extension BeaconRanger{
    
    func stopiBeaconRanging(){
        
        if locationManager == nil{
            return
        }
        
        for region in locationManager.monitoredRegions{
            if let bRegion = region as? CLBeaconRegion{
                
                locationManager.stopRangingBeacons(in: bRegion)
                locationManager.stopMonitoring(for: bRegion)
            }
        }
    }
    func startiBeaconRanging(){
       
            
            locationManager = CLLocationManager();
            locationManager.delegate = self
            locationManager.requestAlwaysAuthorization()
        if let activeRegions = activeiBeaconRegions{
            
            for beaconRegions in activeRegions{
                locationManager.stopRangingBeacons(in: beaconRegions.toCLBeaconRegion())
                locationManager.stopMonitoring(for: beaconRegions.toCLBeaconRegion())
                locationManager.startMonitoring(for: beaconRegions.toCLBeaconRegion())
                locationManager.startRangingBeacons(in: beaconRegions.toCLBeaconRegion())
                
            }
        }
        if let inactiveRegions = inactiveiBeaconRegions{
            
            for beaconRegions in inactiveRegions{
                
                locationManager.stopRangingBeacons(in: beaconRegions.toCLBeaconRegion())
                locationManager.stopMonitoring(for: beaconRegions.toCLBeaconRegion())
                
            }
        }
    }
  
    func updateBeaconRangerWithiBeacons(_ beacons:[CLBeacon]){
        
        for beacon in beacons{
            
            if beacon.rssi < 0 {
                self.delegate?.beaconRangerDidRangeBeacon(beacon, type: BeaconVendorType.nativeiBeacon,uuid: beacon.proximityUUID)
                if targettediBeacon != nil {
                    if beacon.isSame(targettediBeacon!){
                        self.targettedBeaconDelegate?.beaconRangerDidRangeTargettedBeacon(beacon, type: BeaconVendorType.nativeiBeacon)
                    }
                }
            }
        }
    }
}
extension BeaconRanger:CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion){
        
        updateBeaconRangerWithiBeacons(beacons)
        
        }
    
    
     func locationManager(_ manager: CLLocationManager, rangingBeaconsDidFailFor region: CLBeaconRegion, withError error: Error){
        
    }
    
   
     func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion){
        
    }
    
   
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion){
        
    }
    
   
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error){
        
    }
    
    
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error){
        
    }
    
   
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus){
        
    }
    
    
     func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion){
        
    }
    
  
    
}

extension BeaconRanger:BLECommunicatorDelegate{
    func bleCommunicator(_ blecommunicator:BLECommunicator ,communicatorInitializationSuccessWithBluetoothState state:BluetoothState){
        isIntialized = true
        if shouldStartScan{
            startRanging()
        }
        
    }
    func bleCommunicator(_ blecommunicator:BLECommunicator ,communicatorInitializationFailWithBluetoothState state:BluetoothState){
        
    }
    
    
    func bleCommunicator(_ bleCommunicator: BLECommunicator!, didDiscoverPeripheral peripheral: CBPeripheral!, advertisementData: [AnyHashable: Any]!, RSSI: NSNumber!){
        
       
        var rangedBeacon:AtmelBeacon? = rangedBeaconsArray.getAtmelBeaconFromArrayOfPeripheral(peripheral)
        var needToRangeAltBeacon : Bool = false
        
        if rangedBeacon != nil
        {
            // Check whether the device detected is altbeacon
            
            if  rangedBeacon?.beaconType == .altBeacon {
  
                if inactiveAltBeaconRegions != nil && inactiveAltBeaconRegions?.count != 0 {
                    
                    if (inactiveAltBeaconRegions!.map(){$0.hasAltBeacon(rangedBeacon as! AtmelAltBeacon)}.reduce(false, {$0 || $1})) == true {
                        rangedBeaconsArray.remove(at: rangedBeaconsArray.index(of: rangedBeacon!)!)
                    }
                    else{
                        needToRangeAltBeacon = true
                        rangedBeacon?.addBeaconData(peripheral, advData: advertisementData,RSSI: RSSI)
                    }
                }
                else{
                    needToRangeAltBeacon = true
                    rangedBeacon?.addBeaconData(peripheral, advData: advertisementData,RSSI: RSSI)
                }
   
            }
            else{
                rangedBeacon?.addBeaconData(peripheral, advData: advertisementData,RSSI: RSSI)
            }
        }
        else
        {
            rangedBeacon = AtmelBeacon.addNewRangedBeacon(advertisementData, peripheral: peripheral,RSSI: RSSI)
            
            if rangedBeacon != nil
            {
                if  rangedBeacon?.beaconType == .altBeacon {
                    
                    if inactiveAltBeaconRegions != nil && inactiveAltBeaconRegions?.count != 0 {
                        
                        if (inactiveAltBeaconRegions!.map(){$0.hasAltBeacon(rangedBeacon as! AtmelAltBeacon)}.reduce(false, {$0 || $1})) == false {
                            needToRangeAltBeacon = true
                            rangedBeaconsArray.append(rangedBeacon!)
                        }
                    }
                    else{
                        needToRangeAltBeacon = true
                        rangedBeaconsArray.append(rangedBeacon!)
                    }
                }
                else{
                    rangedBeaconsArray.append(rangedBeacon!)
                }
            }
        }
        
        if rangedBeacon != nil{
            
            if rangedBeacon?.beaconType == .altBeacon {
                if needToRangeAltBeacon {
                    self.delegate?.beaconRangerDidRangeBeacon(rangedBeacon!, type: BeaconVendorType.thirdPatyBeacon,uuid: rangedBeacon!.peripheral.identifier)
                }
            }
            else{
                self.delegate?.beaconRangerDidRangeBeacon(rangedBeacon!, type: BeaconVendorType.thirdPatyBeacon,uuid: rangedBeacon!.peripheral.identifier)
             }
        }
        
        
        if let uuid = targettedBeaconUUID{
            if (peripheral.identifier == uuid) && rangedBeacon != nil {
                self.targettedBeaconDelegate?.beaconRangerDidRangeTargettedBeacon(rangedBeacon!, type: BeaconVendorType.thirdPatyBeacon)
            }
        }
        
    
    }
    func bleCommunicator(_ bleCommunicator: BLECommunicator!,didRetrievePeripherals peripherals: [AnyObject]!){
        
    }
    func bleCommunicator(_ bleCommunicator:BLECommunicator!,didDiscoverDevice deviceInfo: BLEDeviceInfo! ){
        
    }
    
    
    func bleCommunicator(_ bleCommunicator:BLECommunicator!,didConnectDevice device: CBPeripheral!){
        
    }
    func bleCommunicator(_ bleCommunicator:BLECommunicator!,didFailToConnectDevice device: CBPeripheral!){
        
    }
    func bleCommunicator(_ bleCommunicator:BLECommunicator!,didDisconnectDevice device: CBPeripheral! , error: NSError!){
        
    }
}
