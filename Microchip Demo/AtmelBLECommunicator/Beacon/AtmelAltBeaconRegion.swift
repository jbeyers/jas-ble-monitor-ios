/***************************************************************************
 
 AtmelAltBeaconRegion.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import UIKit

class AtmelAltBeaconRegion: AtmelBeaconRegion {
    
    var id1:UUID!
    var id2:UInt16?
    var id3:UInt16?
    
    init(withId1:UUID,id2Value:UInt16?,id3Value:UInt16?,range:Bool,nameString:String){
        super.init()
        name = nameString
        isRanging = range
        beaconType = BeaconItemType.AltBeacon
        id1 = withId1
        id2 = id2Value
        id3 = id3Value
    }
    init(withDictionary:NSDictionary){
        super.init()
        beaconType = BeaconItemType.AltBeacon
        isRanging = withDictionary.value(forKey: RANGEKEY) as! Bool

        id1 = UUID(uuidString: withDictionary.value(forKey: ID1KEY) as! String) 
        id2 = (withDictionary.value(forKey: ID2KEY) as? NSNumber)?.uint16Value
        id3 = (withDictionary.value(forKey: ID3KEY) as? NSNumber)?.uint16Value
        name = withDictionary.value(forKey: NAMEKEY) as! String

    }
    
    override func isEqual(_ object: Any?) -> Bool {
        
        if let altBeaconRange = object as? AtmelAltBeaconRegion{
            
            //if altBeaconRange.id1.isEqual(self.id1){
            if altBeaconRange.id1 == (self.id1){
                
                if altBeaconRange.id2 == nil && self.id2 == nil
                {return true}
                else if altBeaconRange.id2 != nil && self.id2 != nil {
                    
                    if altBeaconRange.id2! == (self.id2!){
                        if altBeaconRange.id3 == nil && self.id3 == nil
                        {return true}
                        else if altBeaconRange.id3 != nil && self.id3 != nil
                        {
                            if altBeaconRange.id3! == (self.id3!)
                            {return true}
                        }
                    }
                    
                    
                }
                
            }
        }
        return false
    }

    override func convertToDict()->NSDictionary?{
        let returndict = NSMutableDictionary()
        returndict.setObject(id1.uuidString, forKey: ID1KEY as NSCopying)
        returndict.setObject(isRanging, forKey: RANGEKEY as NSCopying)
        returndict.setObject(name, forKey: NAMEKEY as NSCopying)

        if id2 != nil
            {returndict.setObject(NSNumber(value: id2! as UInt16), forKey: ID2KEY as NSCopying)}
        if id3 != nil
            {returndict.setObject(NSNumber(value: id3! as UInt16), forKey: ID3KEY as NSCopying)}
        
        returndict.setObject(beaconType.rawValue, forKey: BEACONTYPEKEY as NSCopying)
        return returndict
    }
    
    
    func hasAltBeacon(_ altBeacon : AtmelAltBeacon) -> Bool {
        
        
        //if altBeacon.deviceUUID.isEqual(self.id1) {
        if altBeacon.deviceUUID == (self.id1) {
            
            if self.id2 != nil {
                
                if self.id2 == altBeacon.majorID {
                    
                    if self.id3 != nil {
                        
                        if self.id3 == altBeacon.minorID {
                            
                            return true
                        }
                        else{
                            return false
                        }
                        
                    }
                    else
                    {
                        return true
                    }
                    
                }
                else{
                    return false
                }
            }
            else
            {
                return true
            }
        }

        return false
    }
    
    
    
    
}
