//
//  BatteryService.swift

/* ***************************************************************************

BatteryService.swift



Copyright (c) 2015, Atmel Corporation. All rights reserved.
Released under NDA
Licensed under Atmel's Limited License Agreement.


THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Atmel Corporation: http://www.atmel.com

******************************************************************************/

import Foundation
import CoreBluetooth


protocol BatteryServiceDelegate:NSObjectProtocol{
    func batteryServiceDidReadBatteryLevel(_ batteryLevel:UInt8)
}

class BatteryService {
    var delegate:BatteryServiceDelegate?
    var batteryLevelCharacteristic:CBCharacteristic?
    var service:CBService?
    var sourceDevice:BLESourceDevice?
    
    init(deviceInfoService:CBService?, andSourceDevice device:BLESourceDevice, andDelegate bDelegate:BatteryServiceDelegate){
        service = deviceInfoService
        sourceDevice = device
        delegate = bDelegate
        sourceDevice?.operationsDelegate = self
        batteryLevelCharacteristic = service?.characteristics?.filter(){$0.uuid.isEqual(CBUUID(blecharacteristic: .BatteryLevelCharacteristic))}.first
    }
    
    
}


extension BatteryService {
    
    func startBatteryLevelNotifications()
        {self.sourceDevice?.setNotifyValue(true, forCharacteristic: batteryLevelCharacteristic)}
    
    func stopBatteryLevelNotifications()
        {self.sourceDevice?.setNotifyValue(false, forCharacteristic: batteryLevelCharacteristic)}
    
    func didReadDataforBatteryCharacteristic(_ serviceChar:CBCharacteristic){
        
        if(serviceChar == batteryLevelCharacteristic){
            var batteryLevelPercentage:UInt8 = 0
            (serviceChar.value as NSData?)?.getBytes(&batteryLevelPercentage, length: MemoryLayout.size(ofValue: batteryLevelPercentage))
            self.delegate?.batteryServiceDidReadBatteryLevel(batteryLevelPercentage)
        }
    }

}

extension BatteryService:BLESourceDeviceOperationsDelegate{
    
    
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice, didUpdateValueForCharacteristic characteristic: CBCharacteristic!, error: NSError!)
    {
        didReadDataforBatteryCharacteristic(characteristic)
    }
    
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice,didWriteValueForCharacteristic characteristic: CBCharacteristic!, error: NSError!){
        
        //NEGLECT
    }
    
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice,didUpdateNotificationStateForCharacteristic characteristic: CBCharacteristic!, error: NSError!){
        
//NEGLECT
    }
    
 
}
