

//
//  ScanParameterService.swift


/* ***************************************************************************

ScanParameterService.swift



Copyright (c) 2015, Atmel Corporation. All rights reserved.
Released under NDA
Licensed under Atmel's Limited License Agreement.


THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Atmel Corporation: http://www.atmel.com

******************************************************************************/


import Foundation
import CoreBluetooth


struct ScanParameters {
    
    var scanInterval:Int?
    var scanWindow:Int?
    init(scanIntervalValue:Int , scanWindowValue:Int){
        scanInterval =  scanIntervalValue
        scanWindow   =  scanWindowValue
    }
    
    func toData()->Data?{
        
        if(self.scanInterval != nil) && (self.scanWindow != nil){
            
            var scanIntervalValue:UInt16 = UInt16(self.scanInterval!)
            var scanWindowValue:UInt16 = UInt16(self.scanWindow!)
//            var dataArray:Array<UInt16> = [scanIntervalValue,scanWindowValue]
            print("sizeofValue = \(MemoryLayout.size(ofValue: scanWindowValue))")
            let writeData:NSMutableData = NSMutableData(bytes: &scanIntervalValue, length: MemoryLayout.size(ofValue: scanIntervalValue))
            //writeData.append(Data(bytes: UnsafePointer<UInt8>(&scanWindowValue), count: sizeofValue(scanWindowValue)))
            writeData.append(Data(bytes: &scanWindowValue, count: MemoryLayout.size(ofValue: scanWindowValue)))
            return writeData as Data
        }
        return nil
    }

}

enum ScanRefreshMode{
    case active
    case passive
}

protocol ScanParameterServiceDelegate:NSObjectProtocol{
    func scanParameterService(didReadScanRefreshNeed refrshStatus:Int)
    func scanParameterServicedidUpdateNotificationStateForCharacteristic(_ characteristic: CBCharacteristic!, error: NSError!)
}


class ScanParameterService{
    
    var delegate:ScanParameterServiceDelegate?
    var service:CBService?
    var sourceDevice:BLESourceDevice?
    
    var scanIntervalWindowCharacteristics:CBCharacteristic?
    var scanRefreshCharacteristics:CBCharacteristic?
    var currentRefreshMode:ScanRefreshMode
    var scanParameters:ScanParameters?
    
    init(device:BLESourceDevice?, andDelegate delegate:ScanParameterServiceDelegate?, refreshMode:ScanRefreshMode , scanParametersValue:ScanParameters?){
       
        let serviceArray = device?.CBSPeripheral.services
        service =  serviceArray?.filter(){$0.uuid.isEqual(CBUUID(bleService:BLEService.ScanParameterService))}.first
        sourceDevice = device
        self.delegate = delegate
        currentRefreshMode = refreshMode
        sourceDevice?.operationsDelegate = self

        scanIntervalWindowCharacteristics=(service?.characteristics)?.filter(){$0.uuid.isEqual(CBUUID(blecharacteristic: BLECharacteristic.ScanIntervalWindowCharacteristics))}.first
        scanRefreshCharacteristics=(service?.characteristics)?.filter(){$0.uuid.isEqual(CBUUID(blecharacteristic: BLECharacteristic.ScanRefreshCharacteristics))}.first
       
        sourceDevice?.operationsDelegate = self
        scanParameters = scanParametersValue
        
        
    }
    
  }

extension ScanParameterService{
    
 
    
    func writeScanParameters(){
        self.sourceDevice?.writeValue(scanParameters?.toData(), forCharacteristic: scanIntervalWindowCharacteristics, withAcknowledgement: false)
     
    }
    func startScanRefreshNotifications()
        
    {self.sourceDevice?.setNotifyValue(true, forCharacteristic: scanRefreshCharacteristics)}
    
    func stopScanRefreshNotifications()
        
    {self.sourceDevice?.setNotifyValue(false, forCharacteristic: scanRefreshCharacteristics)}
}

extension ScanParameterService{
    
    fileprivate func scanRefreshCheck(_ data:Data){
        var scanRefreshStatus:UInt8=0
        (data as NSData).getBytes(&scanRefreshStatus, length: MemoryLayout.size(ofValue: scanRefreshStatus))
        
        switch(currentRefreshMode){
            
        case ScanRefreshMode.active:
            if scanRefreshStatus == 0
                {self.writeScanParameters()}
            break
            
        case ScanRefreshMode.passive:
            self.delegate?.scanParameterService(didReadScanRefreshNeed: Int(scanRefreshStatus))
            break
            
        }
        
    }
}
extension ScanParameterService:BLESourceDeviceOperationsDelegate{
    
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice, didUpdateValueForCharacteristic characteristic: CBCharacteristic!, error: NSError!){
        
        if characteristic.isEqual(scanRefreshCharacteristics){
            scanRefreshCheck(scanRefreshCharacteristics!.value!)
        }
        
    }
    
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice,didWriteValueForCharacteristic characteristic: CBCharacteristic!, error: NSError!){
        
        //NEGLECT
    }
    
    func bleSourceDevice(_ bleSourceDevice:BLESourceDevice,didUpdateNotificationStateForCharacteristic characteristic: CBCharacteristic!, error: NSError!){
        
        self.delegate?.scanParameterServicedidUpdateNotificationStateForCharacteristic(characteristic, error: error)
        
    }
}
