/***************************************************************************
 
 AtmelAltBeaconItem.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import UIKit


class AtmelAltBeaconItem: AtmelBeaconItem {
    
    var id1:UUID!
    var id2:UInt16!
    var id3:UInt16!
    
    init(withId1:UUID,id2Value:UInt16,id3Value:UInt16,url:String,nameString:String){
        super.init()
        name = nameString
        beaconType = BeaconItemType.AltBeacon
        urlString = url
        id1 = withId1
        id2 = id2Value
        id3 = id3Value
    }
    
    init(withDictionary:NSDictionary){
        super.init()
        beaconType = BeaconItemType.AltBeacon
        
         id1 = UUID(uuidString: withDictionary.value(forKey: ID1KEY) as! String)
         id2 = (withDictionary.value(forKey: ID2KEY) as? NSNumber)?.uint16Value
         id3 = (withDictionary.value(forKey: ID3KEY) as? NSNumber)?.uint16Value
         urlString = withDictionary.value(forKey: URLKEY) as! String
         name = withDictionary.value(forKey: NAMEKEY) as! String
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        
       if let altBEaconItem = object as? AtmelAltBeaconItem{
        
        //if altBEaconItem.id1.isEqual(self.id1) && altBEaconItem.id2 == self.id2 && altBEaconItem.id3 == self.id3
        if altBEaconItem.id1 == (self.id1) && altBEaconItem.id2 == self.id2 && altBEaconItem.id3 == self.id3
            {return true}
        else
            {return false}
        }
        return false
    }
    
    override func convertToDict()->NSDictionary?{
        let returndict = NSMutableDictionary()
        returndict.setObject(id1.uuidString, forKey: ID1KEY as NSCopying)
        returndict.setObject(NSNumber(value: id2 as UInt16), forKey: ID2KEY as NSCopying)
        returndict.setObject(NSNumber(value: id3 as UInt16), forKey: ID3KEY as NSCopying)
        returndict.setObject(urlString, forKey: URLKEY as NSCopying)
        returndict.setObject(name, forKey: NAMEKEY as NSCopying)
        returndict.setObject(beaconType.rawValue, forKey: BEACONTYPEKEY as NSCopying)
        return returndict
    }
}
