/***************************************************************************
 
 AtmelEddyStone.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import Foundation
import CoreBluetooth


enum EddystoneURLPrefixFlag:UInt8{
    
    case scheme0Flag = 0x00
    case scheme1Flag = 0x01
    case scheme2Flag = 0x02
    case scheme3Flag = 0x03
    
    var schemeURL:String{
        
        switch(self){
            
        case .scheme0Flag:
            return "http://www."
        case .scheme1Flag:
            return  "https://www."
            
        case .scheme2Flag:
            return  "http://"
            
        case .scheme3Flag :
            return  "https://"
            
        }
    }
}


enum EddystoneURLPostfixFlag:UInt8{
    
    case scheme0Flag  = 0x00
    case scheme1Flag  = 0x01
    case scheme2Flag  = 0x02
    case scheme3Flag  = 0x03
    case scheme4Flag  = 0x04
    case scheme5Flag  = 0x05
    case scheme6Flag  = 0x06
    case scheme7Flag  = 0x07
    case scheme8Flag  = 0x08
    case scheme9Flag  = 0x09
    case scheme10Flag = 0x0a
    case scheme11Flag = 0x0b
    case scheme12Flag = 0x0c
    case scheme13Flag = 0x0d
    
    var schemeURL:String{
        
        switch(self){
            
        case .scheme0Flag:
            return ".com/"
        case .scheme1Flag:
            return  ".org/"
            
        case .scheme2Flag:
            return  ".edu/"
        
        case .scheme3Flag :
            return  ".net/"
            
        case .scheme4Flag:
            return  ".info/"
            
        case .scheme5Flag :
            return  ".biz/"
            
        case .scheme6Flag :
            return  ".gov/"
            
        case .scheme7Flag :
            return  ".com"
            
        case .scheme8Flag :
            return  ".org"
        case .scheme9Flag :
            return  ".edu"
        case .scheme10Flag :
            return  ".net"
            
        case .scheme11Flag :
            return  ".info"
            
        case .scheme12Flag :
            return  ".biz"
            
        case .scheme13Flag :
            return  ".gov"
            
        }
    }

}




enum EddystoneFrameFlags:UInt8{

    case uidFrameFlag = 0x00
    case urlFrameFlag = 0x10
    case tlmFrameFlag = 0x20
}



struct UIDFrame {
    
    var nameSpaceData:Data!
    var instanceComponentData:Data!
    var txPower:Int!
    
    init(data:Data)
    {
        if  data.count >= 2
            //{txPower = Int(((data.subdata(in: NSMakeRange(1, 1))) as NSData).bytes.bindMemory(to: Int8.self, capacity: (data.subdata(with: NSMakeRange(1, 1))).count).pointee)}
            {txPower = Int(((data.subdata(in: 1..<2)) as NSData).bytes.bindMemory(to: Int8.self, capacity: (data.subdata(in: 1..<2)).count).pointee)}
        if data.count >= 12
            //{nameSpaceData = data.subdata(in: NSMakeRange(2, 10))}
            {nameSpaceData = data.subdata(in: 2..<12)}
        if data.count >= 18
            //{instanceComponentData = data.subdata(in: NSMakeRange(12, 6))}
            {instanceComponentData = data.subdata(in: 12..<18)}

    }
}
struct TLMFrame {
    
    var batteryVoltage:UInt16?
    var beaconTemperature:Float?
    var advPDUCount:UInt32?
    var timeSinceBoot:UInt32?
    
    init(data:Data)
    {
        if  data.count >= 4
        {
            //batteryVoltage = CFSwapInt16BigToHost(((data.subdata(in: NSMakeRange(2, 2))) as NSData).bytes.bindMemory(to: UInt16.self, capacity: (data.subdata(with: NSMakeRange(2, 2))).count).pointee)
            batteryVoltage = CFSwapInt16BigToHost(((data.subdata(in: 2..<4)) as NSData).bytes.bindMemory(to: UInt16.self, capacity: (data.subdata(in: 2..<4)).count).pointee)
        }
        
        if  data.count >= 6{
            //beaconTemperature = (data.subdata(in: NSMakeRange(4, 2))).fixedPointToFloat()
            beaconTemperature = (data.subdata(in: 4..<6)).fixedPointToFloat()
            if beaconTemperature == -128.0
            {beaconTemperature = nil}
        }
        
        if  data.count >= 10
        //{advPDUCount = CFSwapInt32BigToHost(((data.subdata(in: NSMakeRange(6, 4))) as NSData).bytes.bindMemory(to: UInt32.self, capacity: (data.subdata(with: NSMakeRange(6, 4))).count).pointee)}
        {advPDUCount = CFSwapInt32BigToHost(((data.subdata(in: 6..<10)) as NSData).bytes.bindMemory(to: UInt32.self, capacity: (data.subdata(in: 6..<10)).count).pointee)}
        if  data.count >= 14
        //{timeSinceBoot = CFSwapInt32BigToHost(((data.subdata(in: NSMakeRange(10, 4))) as NSData).bytes.bindMemory(to: UInt32.self, capacity: (data.subdata(with: NSMakeRange(10, 4))).count).pointee)}
        {timeSinceBoot = CFSwapInt32BigToHost(((data.subdata(in: 10..<14)) as NSData).bytes.bindMemory(to: UInt32.self, capacity: (data.subdata(in: 10..<14)).count).pointee)}
        
    }
}

struct URLFrame {
    
    var txPower:Int!
    var url:URL?
   
    
    init(data:Data)
    {
        
        var urlPrefix:String?
        var urlPostfix:String?
        var urlMiddleString:String?
        
        if  data.count >= 2
            //{txPower = Int(((data.subdata(in: NSMakeRange(1, 1))) as NSData).bytes.bindMemory(to: Int8.self, capacity: (data.subdata(with: NSMakeRange(1, 1))).count).pointee)}
            {txPower = Int(((data.subdata(in: 1..<2)) as NSData).bytes.bindMemory(to: Int8.self, capacity: (data.subdata(in: 1..<2)).count).pointee)}
        
        if  data.count >= 3
        {
            
            //let urlflag:EddystoneURLPrefixFlag? = EddystoneURLPrefixFlag(rawValue:((data.subdata(in: NSMakeRange(2, 1))) as NSData).bytes.bindMemory(to: UInt8.self, capacity: (data.subdata(with: NSMakeRange(2, 1))).count).pointee)
            let urlflag:EddystoneURLPrefixFlag? = EddystoneURLPrefixFlag(rawValue:((data.subdata(in: 2..<3)) as NSData).bytes.bindMemory(to: UInt8.self, capacity: (data.subdata(in: 2..<3)).count).pointee)
            if urlflag != nil
                {urlPrefix = urlflag?.schemeURL}
            
        }
        
        if data.count>=8{
            
            //let urlflag:EddystoneURLPostfixFlag? = EddystoneURLPostfixFlag(rawValue:((data.subdata(in: NSMakeRange((data.count-1), 1))) as NSData).bytes.bindMemory(to: UInt8.self, capacity: (data.subdata(with: NSMakeRange((data.count-1), 1))).count).pointee)
            let urlflag:EddystoneURLPostfixFlag? = EddystoneURLPostfixFlag(rawValue:((data.subdata(in: (data.count-1)..<data.count)) as NSData).bytes.bindMemory(to: UInt8.self, capacity: (data.subdata(in: (data.count-1)..<data.count)).count).pointee)
            
            if urlflag != nil
                {urlPostfix = urlflag?.schemeURL}
            
            let urlEncodedData : Data?
            
            if urlflag != nil{
                //urlEncodedData = data.subdata(in: NSMakeRange(3, (data.count-4)))
                urlEncodedData = data.subdata(in: 3..<(data.count-1))
            }
            else{
                //urlEncodedData = data.subdata(in: NSMakeRange(3, (data.count-3)))
                urlEncodedData = data.subdata(in: 3..<data.count)
            }
            
            if let urlString = urlEncodedData!.USASCIICoverstion()
            {urlMiddleString = urlString}
        }
    
        
        var URLString = ""
        
        if urlPrefix != nil{
            URLString += urlPrefix!
        }
        
        if urlMiddleString != nil{
            URLString += urlMiddleString!
        }
        
        if urlPostfix != nil{
            URLString += urlPostfix!
        }
        
        self.url = URL(string:URLString)
        


    }
}

class AtmelEddyStone:AtmelBeacon{
    
    var tlmFrame:TLMFrame?
    var uidFrame:UIDFrame?
    var urlFrame:URLFrame?
    
    init(frameData:Data , peripheral:CBPeripheral, RSSI: NSNumber!){
        
        super.init(peripheralObject: peripheral, type: BeaconType.eddystone,RSSI:RSSI)
        
        if frameData.count > 1
        {
            let count = frameData.count
            var frameBytes = [UInt8](repeating: 0, count: count)
            (frameData as NSData).getBytes(&frameBytes, length: count)
            let frameFlag:EddystoneFrameFlags? = EddystoneFrameFlags(rawValue:frameBytes[0])
            
            if frameFlag != nil {
                
                switch(frameFlag!){
                    
                case EddystoneFrameFlags.tlmFrameFlag:
                    tlmFrame = TLMFrame(data: frameData)
                    break
                case EddystoneFrameFlags.urlFrameFlag:
                    urlFrame = URLFrame(data: frameData)
                    
                    break
                case EddystoneFrameFlags.uidFrameFlag:
                    uidFrame = UIDFrame(data: frameData)
                    
                    break
                    
                }
            }
        }
    }
    
    override func addBeaconData(_ peripheralObject:CBPeripheral!,advData:[AnyHashable: Any]!, RSSI: NSNumber!){
        
        super.addBeaconData(peripheralObject, advData: advData, RSSI: RSSI)
        

        if let advertisementFrameList = advData[CBAdvertisementDataServiceDataKey]
            as? [AnyHashable: Any]
        {
            let uuid = CBUUID(string: "FEAA")
            if let frameData = (advertisementFrameList[uuid] as? Data)
            {
                
                if frameData.count > 1
                {
                    let count = frameData.count
                    var frameBytes = [UInt8](repeating: 0, count: count)
                    (frameData as NSData).getBytes(&frameBytes, length: count)
                    let frameFlag:EddystoneFrameFlags? = EddystoneFrameFlags(rawValue:frameBytes[0])
                    
                    if frameFlag != nil {
                        
                        switch(frameFlag!){
                            
                        case EddystoneFrameFlags.tlmFrameFlag:
                            tlmFrame = TLMFrame(data: frameData)

                            break
                        case EddystoneFrameFlags.urlFrameFlag:
                            urlFrame = URLFrame(data: frameData)

                            break
                        case EddystoneFrameFlags.uidFrameFlag:
                            uidFrame = UIDFrame(data: frameData)

                            break
                            
                        }
                    }
                }
                
            }
        }
        
        
    }
}
