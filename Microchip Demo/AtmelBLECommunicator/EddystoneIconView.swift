/***************************************************************************
 
 EddystoneIconView.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import UIKit

class EddystoneIconView: BeaconIconView {
    var beaconObject:AtmelBeacon!
        {
        didSet{
            beaconObj = beaconObject
        }
    }
    init(beaconInfo:AtmelBeacon){
        
        super.init(withUUID: beaconInfo.peripheral.identifier, vendorType: BeaconVendorType.thirdPatyBeacon)
        rssiQueue = Queue(withMaxSize: 4)

        beaconObject = beaconInfo
        beaconObj = beaconInfo

        iconImageButton.setImage(UIImage(named: "EddystoneImage"), for: UIControlState())
        
        self.RSSI = beaconInfo.RSSIValue
    }
   
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override func updateDataWithBeaconObject(_ beaconObjet:AnyObject){
        if let beaconInfo = beaconObjet as? AtmelBeacon
            {beaconObject = beaconInfo}
        
        if let eddystone = beaconObjet as? AtmelEddyStone {
            var rssiAtZeroMeter : Int = 0
            
            if eddystone.uidFrame != nil {
                rssiAtZeroMeter = eddystone.uidFrame!.txPower
            }
            
            if eddystone.urlFrame != nil {
                rssiAtZeroMeter = eddystone.urlFrame!.txPower
            }
            
            if beaconObject.RSSIValue.intValue < 0 {
                self.RSSI = NSNumber(value: beaconObject.RSSIValue.intValue - rssiAtZeroMeter as Int)
            }
        }
    }
}
