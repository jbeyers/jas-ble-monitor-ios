/***************************************************************************
 
 BeaconDataTableViewCell.swift
 
 
 Copyright (c) 2015, Atmel Corporation. All rights reserved.
 Released under NDA
 Licensed under Atmel's Limited License Agreement.
 
 
 THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Atmel Corporation: http://www.atmel.com
 
 ******************************************************************************/

import UIKit

protocol BeaconDataTableViewCellDelegate {
    
    func deleteBeaconAtindex(_ index : Int)
}


class BeaconDataTableViewCell: UITableViewCell {

    var beaconItem:AtmelBeaconItem!{
        didSet{
            updateCellWithBeaconItem()
        }
    }
    
    var delegate : BeaconDataTableViewCellDelegate?
    
    @IBOutlet weak var nameHeaderLabel: UILabel!
    @IBOutlet weak var uuidHeaderLabel: UILabel!
    @IBOutlet weak var id2HeaderLabel: UILabel!
   
    @IBOutlet weak var id3HeaderLabel: UILabel!
    @IBOutlet weak var urlHeaderLabel: UILabel!
    
    @IBOutlet weak var urlLabel: UILabel!
    
    @IBOutlet weak var beaconIconImage: UIImageView!
    
    @IBOutlet weak var id3Label: UILabel!
    @IBOutlet weak var id2Label: UILabel!
    @IBOutlet weak var uuidLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var deleteButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func updateCellWithBeaconItem(){
        
        switch(beaconItem.beaconType!){
            case BeaconItemType.iBeacon:
                uuidHeaderLabel.text = "UUID :"
                id2HeaderLabel.text =  "Major :"
                id3HeaderLabel.text =  "Minor :"
                urlHeaderLabel.text =  "URL :"
                beaconIconImage.image = UIImage(named: "iBeaconImage")
                
                if let iBeaconItem = beaconItem as? AtmeliBeaconItem{
                  
                    nameLabel.text = iBeaconItem.name
                     uuidLabel.text = iBeaconItem.uuid.uuidString
                    id2Label.text = "\(iBeaconItem.major!)"
                    id3Label.text = "\(iBeaconItem.minor!)"
                    urlLabel.text = iBeaconItem.urlString
                }
            break
            
        case BeaconItemType.AltBeacon:
            uuidHeaderLabel.text = "UUID :"
            id2HeaderLabel.text =  "ID1 :"
            id3HeaderLabel.text =  "ID2 :"
            urlHeaderLabel.text =  "URL :"
            beaconIconImage.image = UIImage(named: "AltBeaconImage")
            
            if let altBeaconItem = beaconItem as? AtmelAltBeaconItem{
                
                nameLabel.text = altBeaconItem.name
                uuidLabel.text = "\(altBeaconItem.id1!.uuidString)"
                id2Label.text = "\(altBeaconItem.id2!)"
                id3Label.text = "\(altBeaconItem.id3!)"
                
                urlLabel.text = altBeaconItem.urlString
            }
            break
        }
        
    }

    @IBAction func deleteButtonClicked(_ sender: UIButton) {
        
        let alertVC = UIAlertController(title: "ALERT", message: "Do you want to delete this beacon?", preferredStyle: .alert)
        let deleteAction  = UIAlertAction(title: "Yes", style: .default) { (action) in
            
            if self.delegate != nil {
                self.delegate?.deleteBeaconAtindex(sender.tag)
            }
        }
        
        let cancelAction = UIAlertAction(title: "No", style: .cancel, handler: nil)
        alertVC.addAction(deleteAction)
        alertVC.addAction(cancelAction)
        self.parentViewController?.present(alertVC, animated: true, completion: nil)
    }
}
