//
//  MasterViewController.m
//  Atmel Provision
//
//  Created by Geoff Macdonald on 06/05/2015.
//  Copyright (c) 2015 Atmel. All rights reserved.
//

// TODO:
//
// Start a timer when the scan results are requested. If nothing is received in TBDs, then silently disconnect the peripheral and
// reconnect.
//
// Find a way of reconnecting to the peripheral if it has been added to the known BT devices list (because the user requested
// a connection and entered the security key (000000).

#import "MasterViewController.h"
#import "DetailViewController.h"

@interface MasterViewController ()
{
    DetailViewController *detailView;
    CBPeripheral *connectedPeripheral;
    bool wantProvisionResult;
    bool deviceConnecting;
    bool scanDone;
}
@property NSMutableArray *objects;
@end

@implementation MasterViewController

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    detailView=nil;
    connectedPeripheral=nil;
    wantProvisionResult=false;
    deviceConnecting=false;
    scanDone=true;
    
    // Do any additional setup after loading the view, typically from a nib.
//  This adds an edit button to the view.  Not required as the table is not user editable.
//    self.navigationItem.leftBarButtonItem = self.editButtonItem;

    CBCentralManager *centralManager=[[CBCentralManager alloc] initWithDelegate:self queue:nil];
    self.centralManager=centralManager;
// This adds a "+"button to the view.  Could be used to add a rescan button if required, but probably not necessary.
//    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject:)];
//    self.navigationItem.rightBarButtonItem = addButton;
    
    if (!self.objects) {
        self.objects = [[NSMutableArray alloc] init];
    }
    
    if(!self.compatiblePeripherals) {
        self.compatiblePeripherals=[[NSMutableArray alloc] init];
    }
 }

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    //self.centralManager = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)HomeClicked:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)ScanClicked:(id)sender {
    self.centralManager = nil;
    [self.objects removeAllObjects];
    [self.tableView reloadData];
    CBCentralManager *centralManager=[[CBCentralManager alloc] initWithDelegate:self queue:nil];
    self.centralManager=centralManager;
}

- (void)insertNewObject:(id)sender {
}

#pragma mark - Segues

// Prepare to open up a detail view and connect to the selected peripheral.
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSInteger row=indexPath.row;
        NSDate *object = self.objects[row];
        CBPeripheral *peripheral=self.compatiblePeripherals[row];
        [[segue destinationViewController] setDetailItem:object];
        [[segue destinationViewController] setMasterView:self];
        [self.centralManager connectPeripheral:peripheral options:nil];
        detailView=[segue destinationViewController];
    }
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.objects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];

    NSDate *object = self.objects[indexPath.row];
    cell.textLabel.text = [object description];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.objects removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}

// Uses CBCentralManager to check whether the current platform/hardware supports Bluetooth LE. An alert is raised if Bluetooth
// is not enabled or is not supported.
// TODO: Display error conditions
- (BOOL) isLECapableHardware
{
    NSArray *services=@[[CBUUID UUIDWithString:@"77880001-d229-11e4-8689-0002a5d5c51b"]];
    
    switch ([self.centralManager state])
    {
        case CBCentralManagerStateUnsupported:
            //self.statusControl.text=@"BLE not supported";
            break;
        case CBCentralManagerStateUnauthorized:
            //self.statusControl.text=@"Not authorized to use BLE";
            break;
        case CBCentralManagerStatePoweredOff:
            //self.statusControl.text=@"BLE is currently powered off";
            break;
        case CBCentralManagerStatePoweredOn:
            //self.statusControl.text=@"Searching for device...";
            [self.centralManager scanForPeripheralsWithServices:services options:nil];
            [self.centralManager retrieveConnectedPeripheralsWithServices:services];
            return TRUE;
        case CBCentralManagerStateUnknown:
            //self.statusControl.text=@"BLE state unknown";
            break;
        case CBCentralManagerStateResetting:
            //self.statusControl.text=@"BLE resetting";
            break;
    }
    
    return FALSE;
}

#pragma mark - CBCentralManagerDelegate

// Invoked whenever the central manager's state is updated.
- (void) centralManagerDidUpdateState:(CBCentralManager *)central
{
    [self isLECapableHardware];
}

// Method called with the CBPeripheral class as its main input parameter.
// This contains most of the information that there is about the BLE peripheral.
- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    if( ![self.compatiblePeripherals containsObject:peripheral] )
    {
        [self.compatiblePeripherals addObject:peripheral];
         NSString *deviceName=[NSString stringWithString:advertisementData[@"kCBAdvDataLocalName"]];
        [_objects addObject:deviceName];
        [self.tableView reloadData];
    }
}

// Method called whenever we successfully connect to the BLE epripheral
- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    [peripheral setDelegate:self];
    [peripheral discoverServices:nil];
    connectedPeripheral = peripheral;
}

// Invoked whenever an existing connection with the peripheral is torn down.
- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)aPeripheral error:(NSError *)error
{
    NSArray *services=@[[CBUUID UUIDWithString:@"77880001-D229-11E4-8689-0002A5D5C51B"]];
    [self.centralManager scanForPeripheralsWithServices:services options:nil];
}

#pragma mark - CBPeripheralDelegate

// Method called when we discover the peripheral's available services
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    for (CBService *aService in peripheral.services)
    {
        NSLog(@"Service found with UUID: %@", aService.UUID);
        
        /* Connect Service */
        if ([aService.UUID isEqual:[CBUUID UUIDWithString:@"77880001-D229-11E4-8689-0002A5D5C51B"]])
        {
            [peripheral discoverCharacteristics:nil forService:aService];
        }
        
        /* Scan service */
        // Ask peripheral for scan results
        if ([aService.UUID isEqual:[CBUUID UUIDWithString:@"FB8C0001-D224-11E4-85A1-0002A5D5C51B"]])
        {
            [peripheral discoverCharacteristics:nil forService:aService];
        }
    }
}

- (void)doScan {
    NSLog(@"doScan");
    if(connectedPeripheral!=nil)
    {
        // Search for the correct service and characteristic
        for(CBService *service in connectedPeripheral.services)
        {
            if([service.UUID isEqual:[CBUUID UUIDWithString:@"FB8C0001-D224-11E4-85A1-0002A5D5C51B"]])
            {
                for(CBCharacteristic *characteristic in service.characteristics)
                {
                    if([characteristic.UUID isEqual:[CBUUID UUIDWithString:@"FB8C0002-D224-11E4-85A1-0002A5D5C51B"]])
                    {
                        UInt8 scanDataBuffer[1];
                        scanDataBuffer[0]=1;
                        NSData *scanData = [NSData dataWithBytes:&scanDataBuffer length:1];
                        NSLog(@"Request to connectedPeripheral");
                        [connectedPeripheral writeValue:scanData forCharacteristic:characteristic type:CBCharacteristicWriteWithResponse];
                        //[connectedPeripheral discoverCharacteristics:nil forService:service];
                        [connectedPeripheral readValueForCharacteristic:characteristic];
                        scanDone = false;
                    }
                }
            }
        }
    }
}

// Called when characteristics have been discovered for the connected device.  If a scan service characteristic is
// discovered, ask for its value.
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    if ([service.UUID isEqual:[CBUUID UUIDWithString:@"77880001-D229-11E4-8689-0002A5D5C51B"]])
    {
        NSLog(@"Got connect service characteristic");
    }
    else
    if ( [service.UUID isEqual:[CBUUID UUIDWithString:@"FB8C0001-D224-11E4-85A1-0002A5D5C51B"]] )
    {
        NSLog(@"Got scan service characteristic");
        for(CBCharacteristic *aChar in service.characteristics)
        {
            [peripheral readValueForCharacteristic:aChar];
        }
    }
}

// Method called when we retrieve a specified characteristic's value or when the peripheral device notifies
// us that the value has changed.
// If a scan result is received, call the detail view to add the scan results to the AP list.
// TODO: check that the detailView is valid.
- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:@"FB8C0100-D224-11E4-85A1-0002A5D5C51B"]])
        [detailView addScanResult:characteristic.value];
    else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:@"FB8C0002-D224-11E4-85A1-0002A5D5C51B"]])
    {
        const char mode=*((const char *)[characteristic.value bytes]);
        NSLog(@"WiFiscanningMode %d", mode);
        if (mode == 2) {
            if (scanDone == false) {
                for(CBService *service in connectedPeripheral.services)
                {
                    if([service.UUID isEqual:[CBUUID UUIDWithString:@"FB8C0001-D224-11E4-85A1-0002A5D5C51B"]])
                    {
                        [connectedPeripheral discoverCharacteristics:nil forService:service];
                    }
                }
            }
            scanDone = true;
        }
        else if (mode == 1) {
            if (scanDone == false) {
                [connectedPeripheral readValueForCharacteristic:characteristic];
            }
        }
    }
    else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:@"77880002-D229-11E4-8689-0002A5D5C51B"]])
    {
        const char *conState=(const char *)[characteristic.value bytes];
        NSLog(@"Nofity connection state %d want=%d connecting=%d", conState[0], wantProvisionResult, deviceConnecting);
        if(wantProvisionResult)
        {
            if((!deviceConnecting) && (*conState==1))
               deviceConnecting=true;
            if(deviceConnecting)
            {
                if(*conState==2)
                {
                    [detailView activityOff];
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success!" message:@"Provisioning Complete" delegate:self cancelButtonTitle:@"ok"   otherButtonTitles:nil];
                    [alert show];
                    wantProvisionResult=false;
                    deviceConnecting=false;
                }
                else if(*conState==0)
                {
                    [detailView activityOff];
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!" message:@"Provisioning Failed" delegate:nil cancelButtonTitle:@"ok"   otherButtonTitles:nil];
                    [alert show];
                    wantProvisionResult=false;
                    deviceConnecting=false;
                }
            }
        }
    }
    else
    {
        NSLog(@"didUpdateValueForCharacteristic %@", characteristic.UUID);
        
    }
}


// Called from the detail view when the user has pressed the "connect" button.  Create a byte array containing the
// security, ssid and passphrase information, then send it to the device.
// TODO: Error check the security, ssid and passphrase values.
// Display a message upon completion.
- (void)doProvision : (NSString *)ssidText : (NSString *)passphraseText : (NSInteger)security{
    
    NSLog(@"doProvision ssidText=%@ passphraseText=%@ security=%d", ssidText, passphraseText, security);
    if(connectedPeripheral!=nil)
    {
        if (security==2 && (passphraseText.length == 5 || passphraseText.length == 13)) {
            NSString * hexStr = [NSString stringWithFormat:@"%@",
                                [NSData dataWithBytes:[passphraseText cStringUsingEncoding:NSUTF8StringEncoding]
                                               length:strlen([passphraseText cStringUsingEncoding:NSUTF8StringEncoding])]];
            
            for(NSString * toRemove in [NSArray arrayWithObjects:@"<", @">", @" ", nil])
                hexStr = [hexStr stringByReplacingOccurrencesOfString:toRemove withString:@""];
            NSLog(@"%@", hexStr);
            passphraseText = hexStr;
        }
        // Construct the byte array containing the data.
        NSData *ssidData = [ssidText dataUsingEncoding:NSUTF8StringEncoding];
        NSData *passphraseData = [passphraseText dataUsingEncoding:NSUTF8StringEncoding];
        UInt8 connectDataBuffer[98];
        UInt8 *lpb=connectDataBuffer;
        *lpb++ = security+1;
        *lpb++ = (UInt8)ssidText.length;
        [ssidData getBytes:lpb length:32];
        lpb+=32;
        
        //NSLog(@"'%@' %d", passphraseText, passphraseText.length);
        *lpb++ = (UInt8)passphraseText.length;
        [passphraseData getBytes:lpb length:63];

        // Search for the correct service and characteristic
        for(CBService *service in connectedPeripheral.services)
        {
            if([service.UUID isEqual:[CBUUID UUIDWithString:@"77880001-D229-11E4-8689-0002A5D5C51B"]])
            {
                for(CBCharacteristic *characteristic in service.characteristics)
                {
                    if([characteristic.UUID isEqual:[CBUUID UUIDWithString:@"77880003-D229-11E4-8689-0002A5D5C51B"]])
                    {
                        NSData *connectData = [NSData dataWithBytes:&connectDataBuffer length:98];
                        [connectedPeripheral writeValue:connectData forCharacteristic:characteristic type:CBCharacteristicWriteWithResponse];
                    }
                    for(CBCharacteristic *characteristic in service.characteristics)
                    {
                        if([characteristic.UUID isEqual:[CBUUID UUIDWithString:@"77880002-D229-11E4-8689-0002A5D5C51B"]])
                        {
                            deviceConnecting=false;
                            wantProvisionResult=true;
                            UInt8 doConnectDataBuffer[1];
                            doConnectDataBuffer[0]=1;
                            NSData *doConnectData = [NSData dataWithBytes:&doConnectDataBuffer length:1];
                            [connectedPeripheral writeValue:doConnectData forCharacteristic:characteristic type:CBCharacteristicWriteWithResponse];
                            [connectedPeripheral discoverCharacteristics:nil forService:service];
                            for (CBCharacteristic *aChar in service.characteristics)
                            {
                                [connectedPeripheral readValueForCharacteristic:aChar];
                                if ([aChar.UUID isEqual:[CBUUID UUIDWithString:@"77880002-D229-11E4-8689-0002A5D5C51B"]])
                                    [connectedPeripheral setNotifyValue:YES forCharacteristic:aChar];
                            }
                            [detailView activityOn];
                        }
                    }
                }
            }
        }
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        //[detailView dismissViewControllerAnimated:YES completion:nil];
    }
}

@end
