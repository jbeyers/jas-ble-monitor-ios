//
//  DetailViewController.h
//  Atmel Provision
//
//  Created by Geoff Macdonald on 06/05/2015.
//  Copyright (c) 2015 Atmel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterViewController.h"

@interface DetailViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) UIActivityIndicatorView *activityView;
@property (strong, nonatomic) id detailItem;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
- (IBAction)BtnProvisionTouch:(id)sender;
- (IBAction)BtnScanTouch:(id)sender;
- (IBAction)BtnClearTouch:(id)sender;
@property (weak, nonatomic) IBOutlet UINavigationItem *detailTitle;
@property (weak, nonatomic) IBOutlet UITableView *apList;
@property (weak, nonatomic) IBOutlet UITextField *editSSID;
@property (weak, nonatomic) IBOutlet UITextField *editPassphrase;
@property (nonatomic, retain) NSTimer *activityTimer;
@property (weak, nonatomic) IBOutlet UISegmentedControl *ctrlSecurity;
@property (weak, nonatomic) IBOutlet UIButton *btnShowPassphrase;
- (void)doTimer;
- (void)doProvision;
- (void)addScanResult:(NSData *)value;
- (void)setMasterView:(MasterViewController *)aMasterView;
- (IBAction)btnShowPassphraseTouch:(id)sender;
- (void)activityOn;
- (void)activityOff;
@end

