//
//  DetailViewController.m
//  Atmel Provision
//
//  Created by Geoff Macdonald on 06/05/2015.
//  Copyright (c) 2015 Atmel. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()
{
    NSMutableArray *tableData;
    int activityTimerCount;
    bool showPassphrase;
     MasterViewController *masterView;
}
@end

@implementation DetailViewController

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem {
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;

        // Update the view.
        [self configureView];
    }
    masterView=nil;
    activityTimerCount=0;
    _activityTimer=[NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(doTimer) userInfo:nil repeats:YES];
    _activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    _activityView.color=[UIColor blackColor];
    _activityView.center=self.view.center;
    showPassphrase=false;
    [self.view addSubview:_activityView];
    [_activityView startAnimating];
 }

- (void)setShowPassphraseButton {
    if(showPassphrase)
        [_btnShowPassphrase setTitle:@"☑︎" forState:UIControlStateNormal];
    else
        [_btnShowPassphrase setTitle:@"☐" forState:UIControlStateNormal];
    self.editPassphrase.secureTextEntry=(showPassphrase?NO:YES);
    UITextRange *selectedRange = [self.editPassphrase selectedTextRange];
    
    int start = 0;
    int end = 0;
    UITextPosition *startPosition = [self.editPassphrase positionFromPosition:self.editPassphrase.beginningOfDocument offset:start];
    UITextPosition *endPosition = [self.editPassphrase positionFromPosition:self.editPassphrase.beginningOfDocument offset:end];
    UITextRange *selection = [self.editPassphrase textRangeFromPosition:startPosition toPosition:endPosition];
    self.editPassphrase.selectedTextRange=selection;

    self.editPassphrase.selectedTextRange=selectedRange;
}

- (void)configureView {
    // Update the user interface for the detail item.
    if (self.detailItem) {
        self.detailDescriptionLabel.text = [self.detailItem description];
        self.detailTitle.title=self.detailItem;
        [self setShowPassphraseButton];
   }
}

- (void)activityOn {
    [_activityView startAnimating];
}

- (void)activityOff {
    [_activityView stopAnimating];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self configureView];
    tableData=[[NSMutableArray alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated. 
}


-(BOOL)shouldAutorotate {
    return NO;
}


- (void)setMasterView:(MasterViewController *)aMasterView {
    masterView = aMasterView;  
}

- (IBAction)btnShowPassphraseTouch:(id)sender {
    showPassphrase = !showPassphrase;
    [self setShowPassphraseButton];
}


// Method called when the master view receives a scan result from the selected peripheral.  Adds the AP information to the
// AP list and resets the timer which hides the busy indicator to 1s.
// TODO: Add the securoty type to an array so that if the user clicks on an AP in the list, the didSelectRowAtIndexPath method
// can set the security from the array, rather than relying on the first character in the string.
- (void)addScanResult:(NSData*)value
{
    // Received scan result from peripheral.  data format is:
    // <security><rssi><ssidlength><ssid>
    // where <security> is
    //    1 = open
    //    2 = wpa
    //    3 = wep
    const char *ssidData=(const char *)[value bytes];
    char ssidLength=ssidData[2];
    NSString *ssid=[[NSString alloc] initWithData:[value subdataWithRange:NSMakeRange(3,ssidLength)] encoding:NSUTF8StringEncoding];
    NSMutableArray *securityType=[NSMutableArray arrayWithObjects:@" \t", @" \t", @"🔐\t", @"🔒\t", nil];
    char security=ssidData[0];
    NSLog(@"addScanResult ssid=%@ ssidLength=%d security=%d", ssid, ssidLength, security);
    if((security>=0) && (security<=3) && (ssidLength>0))
    {
        NSString *tableEntry=[NSString stringWithFormat:@"%@%@", securityType[security], ssid];
        [tableData addObject:tableEntry];
        NSArray *tempArray=[tableData sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
        [tableData removeAllObjects];
        [tableData addObjectsFromArray:tempArray];
        [self.apList reloadData];
        activityTimerCount=10;
    }

}

// Clear the activity indicator 1 second after the last AP has been received.
- (void)doTimer {
    if(activityTimerCount>0) {
        activityTimerCount--;
        if((activityTimerCount==0) && ([_activityTimer isValid])) {
            [_activityView stopAnimating];
            [_activityTimer invalidate];
        }
    }
}


// Called when the user touches the "connect" button.  If the security type is "Open", then displays a warning dialog before
// provisioning.  If "WPA" or "WEP", then starts provisioning immediately.
- (IBAction)BtnProvisionTouch:(id)sender {
    [self.view endEditing:YES];
    if(_ctrlSecurity.selectedSegmentIndex==0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning!" message:@"Insecure network.  Are you sure that you want to connect?" delegate:self cancelButtonTitle:@"Yes - connect" otherButtonTitles:@"Cancel", nil];
        [alert show];
    }
    else
        [self doProvision];
}

- (IBAction)BtnScanTouch:(id)sender {
    [tableData removeAllObjects];
    [self.apList reloadData];
    [masterView doScan];
}

- (IBAction)BtnClearTouch:(id)sender {
    self.editSSID.text = @"";
    self.editPassphrase.text = @"";
    _ctrlSecurity.selectedSegmentIndex = 0;
}

// Called if the open AP dialog was displayed.  Starts provisioning if the "yes - connect" button was pressed.
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    // the user clicked connect
    if (buttonIndex == 0)
        [self doProvision];
}


// Send the provisioning data to the connected device.
// This is handled by the master veiw.  The reuired provisiing data is:
//    Security type
//    Access point SSID
//    Passphrase
- (void)doProvision {
    NSString *ssidText=[NSString stringWithString:self.editSSID.text];
    NSString *passphraseText=[NSString stringWithString:self.editPassphrase.text];

    UIAlertView * alert = NULL;
    
    if (ssidText.length==0)
    {
        alert = [[UIAlertView alloc] initWithTitle: @"Error" message: @"No SSID Entry." delegate: nil cancelButtonTitle: @"OK" otherButtonTitles: nil];
    }
    /* WPA */
    else if (_ctrlSecurity.selectedSegmentIndex == 1 && (passphraseText.length < 8 || passphraseText.length > 63))
    {
        alert = [[UIAlertView alloc] initWithTitle: @"Error" message: @"Invalid Passphrase." delegate: nil cancelButtonTitle: @"OK" otherButtonTitles: nil];
    }
    /* WEP */
    else if (_ctrlSecurity.selectedSegmentIndex == 2 && (passphraseText.length != 5 && passphraseText.length != 10 && passphraseText.length != 13))
    {
        alert = [[UIAlertView alloc] initWithTitle: @"Error" message: @"Invalid Passphrase." delegate: nil cancelButtonTitle: @"OK" otherButtonTitles: nil];
    }
    
    if (alert != NULL)
        [alert show];
    else
        [masterView doProvision:ssidText:passphraseText:_ctrlSecurity.selectedSegmentIndex];
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier=@"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if(cell==nil) {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.text=[tableData objectAtIndex:indexPath.row];
    return cell;
}


// Called when the user selects an AP from the AP list.
// Fills out the SSID and security controls.  The security is a bit of a hack and uses the first character of
// the entry text to determine the security type.
// TODO: Currently relies on the first character of the table entry to set the security type.  This should be changed to
// preferably access some hidden information in the table cell, or from a separate array.
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell=[tableView cellForRowAtIndexPath:indexPath];
    NSMutableString *tableCellText;
    tableCellText = [NSMutableString stringWithString:cell.textLabel.text];
    NSRange prefixEnd;
    prefixEnd=[tableCellText rangeOfString:@"\t"];
    NSString *ssid;
    if(prefixEnd.location == NSNotFound)
        ssid = [NSString stringWithString:tableCellText];
    else
        ssid=[tableCellText substringFromIndex:prefixEnd.location+1];
    self.editSSID.text = ssid;
    [self.view endEditing:YES];

    NSInteger securityType;
    NSString *securityChar;
    NSRange range;
    range=[tableCellText rangeOfComposedCharacterSequenceAtIndex:0];
    securityChar = [tableCellText substringWithRange:range];
    if([securityChar compare:@"🔐"]==NSOrderedSame)
        securityType=1;
    else if([securityChar compare:@"🔒"]==NSOrderedSame)
        securityType=2;
    else
        securityType=0;
    _ctrlSecurity.selectedSegmentIndex=securityType;
}
@end
