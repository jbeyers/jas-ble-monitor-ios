//
//  MasterViewController.h
//  Atmel Provision
//
//  Created by Geoff Macdonald on 06/05/2015.
//  Copyright (c) 2015 Atmel. All rights reserved.
//

#import <UIKit/UIKit.h>
@import CoreBluetooth;

@interface MasterViewController : UITableViewController <CBCentralManagerDelegate, CBPeripheralDelegate, UIAlertViewDelegate>
@property (nonatomic, strong) CBCentralManager *centralManager;
@property (nonatomic, strong) CBPeripheral *winc3400device;
@property (retain) NSMutableArray *compatiblePeripherals;
- (void)doProvision : (NSString *)ssidText : (NSString *)passphraseText : (NSInteger)security;
- (void)doScan;

-(IBAction)HomeClicked:(id)sender;
-(IBAction)ScanClicked:(id)sender;
@end

