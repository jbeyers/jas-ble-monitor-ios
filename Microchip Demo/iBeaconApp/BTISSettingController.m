//
//  BTISSettingController.m
//  BeaconThings
//
//  Created by ILYAS on 16/02/16.
//  Copyright © 2016 Rick. All rights reserved.
//

#import "BTISSettingController.h"
#import "ISSCButton.h"

#import "AppDelegateProtocol.h"
#import "BeaconDataObject.h"

#import "BTISWeightController.h"

//#define DebugMode

@interface BTISSettingController ()<UITextFieldDelegate> {
    UITextField *textField;
    UITextField *ServicetextField;
    UISegmentedControl *unit;
    UITextField *uuidTextField;
    //ISSCButton *uuidButton;
    UISegmentedControl *Alarm;
    UISegmentedControl *Vibrate;
    UISwitch *beaconSwitch;
    NSMutableArray *_uuids;
    CLLocationManager *_locationManager;
    NSMutableArray *_beaconRegions;
    UIBackgroundTaskIdentifier _bgTask;
    UITableView *_uuidsTableView;
    NSFileHandle *_beaconListFileHandle;
    NSString *BeaconUUID;
    NSInteger connectIndication;
    NSInteger BeaconNumber;
    NSString *BeaconThingsUUID;
#ifdef DebugMode
    NSTimer *DebugTimer;
#endif
}

@end

@implementation BTISSettingController

- (instancetype)initWithStyle:(UITableViewStyle)style {
    self = [super initWithStyle:style];
    if (self) {
        NSLog(@"ISSettingController init");
        BeaconThingsUUID = nil;
        _beaconDatabase = [[NSMutableDictionary alloc] init];
        BeaconNumber = 0;
        [self monitorBeaconSetting];
        _uuids = [[NSMutableArray alloc] init];
        /*
         NSString *path = [[NSString alloc] initWithFormat:@"%@/Documents/beaconUUID.txt",NSHomeDirectory()];
         if ([[NSFileManager defaultManager] fileExistsAtPath:path] == NO) {
         NSLog(@"no light list, create a file");
         [[NSFileManager defaultManager] createFileAtPath:path contents:nil attributes:nil];
         }
         _beaconListFileHandle = [NSFileHandle fileHandleForUpdatingAtPath:path];
         if (_beaconListFileHandle) {
         NSString *str = [[NSString alloc] initWithData:[_beaconListFileHandle readDataToEndOfFile] encoding:NSASCIIStringEncoding];
         NSArray *beaconUUIDs = [str componentsSeparatedByString:@"\n"];
         for (NSString *uuidStr in beaconUUIDs) {
         NSLog(@"uuidstr = %@", str);
         if ([uuidStr length])
         [_uuids addObject:uuidStr];
         }
         }
         else {
         NSLog(@"light list file handle nil");
         }
         */
        
        self.cbController.beaconTriggerConnection = [[NSUserDefaults standardUserDefaults] boolForKey:@"BM70_BeaconThings"];
        
        BeaconUUID = [[NSUserDefaults standardUserDefaults] stringForKey:@"UUID_BeaconThings"];
        if(BeaconUUID == nil) {
            //NSString *str = @"49535343-FE7D-4AE5-8FA9-9FAFD205E499";
            NSString *str = @"49535343-1E14-4C6F-8E45-F1EE8D0EAD44";    //create by UUIDGen for BeaconThingsDemo
            [_uuids addObject:str];
        }
        else
        {
            NSMutableString *str1= [[NSMutableString alloc] initWithString:BeaconUUID];
            [str1 insertString:@"-" atIndex:20];
            [str1 insertString:@"-" atIndex:16];
            [str1 insertString:@"-" atIndex:12];
            [str1 insertString:@"-" atIndex:8];
            
            NSLog(@"%@",str1);
            [_uuids addObject:str1];
        }
        
        ///*
        BeaconDataObject* theDataObject = [self theAppDataObject];
        if((theDataObject.APPConfig & BeaconThingsOnOff) == BeaconThingsOnOff) {
            //[self startToMonitorRegion];
            NSLog(@"BeaconThings is ON");
        }
        else{
            NSLog(@"BeaconThings is OFF");
        }
        //*/
        [self startToMonitorRegion];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //self.title = @"Setting";
    
    //[self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_bg.png"] forBarMetrics:UIBarMetricsDefault];
    
    //self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"rsz_issc_logo.png"]];
    self.tabBarController.navigationItem.title = @"Configuration";
    
    UIButton *Logo = [UIButton buttonWithType:UIButtonTypeCustom];
    [Logo setFrame:CGRectMake(0, 0, 63, 40)];
    [Logo setImage:[UIImage imageNamed:@"rsz_microchip_logo.png"] forState:UIControlStateNormal];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc]initWithCustomView:Logo];
    self.navigationItem.leftBarButtonItem = barButton;
    
    //self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"iPad_iSSC_background.png"]];
    /*UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"iPad_iSSC_background.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];*/
    
    //#ifdef DebugMode
    self.tabBarController.navigationItem.leftBarButtonItem = nil;
    self.tabBarController.navigationItem.rightBarButtonItem = nil;
#if 0
    UIBarButtonItem *SearchItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(DebugDebug)];
    self.tabBarController.navigationItem.rightBarButtonItem = SearchItem;
#endif
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    textField = [[UITextField alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 160.0f, 30.0f)];
    textField.borderStyle = UITextBorderStyleRoundedRect;
    //textField.keyboardType = UIKeyboardTypeASCIICapable;
    textField.keyboardType = UIKeyboardAppearanceDefault;
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"DeviceName"]) {
        textField.text = [[[NSUserDefaults standardUserDefaults] objectForKey:@"DeviceName"] stringValue];
    }
    [textField setDelegate:self];
    [textField setText:@"BT"];
    textField.tag = 300;
    textField.enabled = NO;
    
    ServicetextField = [[UITextField alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 160.0f, 30.0f)];
    ServicetextField.borderStyle = UITextBorderStyleRoundedRect;
    [ServicetextField setDelegate:self];
    ServicetextField.tag = 200;
    ServicetextField.text = @"495353434E64482B9F8E668002726F2D";
    ServicetextField.enabled = NO;
    //ServicetextField.text = @"49535343FE7D4AE58FA99FAFD205E499";
    //ServicetextField.keyboardType = UIKeyboardTypeDecimalPad;
    
    //unit = [[UISegmentedControl alloc] initWithItems:@[@"kg/cm",@"lb/inch"]];
    //unit = [[UISegmentedControl alloc] initWithItems:@[@"Near",@"Far"]];
    unit = [[UISegmentedControl alloc] initWithItems:@[@"Immediate",@"Near",@"Far"]];
    //unit.frame = CGRectMake(0.0f, 0.0f, 100.0f, 30.0f);
    unit.frame = CGRectMake(0.0f, 0.0f, 220.0f, 30.0f);
    unit.selectedSegmentIndex = [[NSUserDefaults standardUserDefaults] integerForKey:@"Region"];
    [unit addTarget:self action:@selector(setProximityValue:) forControlEvents:UIControlEventValueChanged];
    
    connectIndication = 0;
    
    connectIndication = [[NSUserDefaults standardUserDefaults] integerForKey:@"ConnectIndication"];
    
    Alarm = [[UISegmentedControl alloc] initWithItems:@[@"Enable",@"Disable"]];
    Alarm.frame = CGRectMake(0.0f, 0.0f, 150.0f, 30.0f);
    //Alarm.selectedSegmentIndex = [[NSUserDefaults standardUserDefaults] integerForKey:@"Alarm"];
    if((connectIndication & 0x01) == 0x01)
        Alarm.selectedSegmentIndex = 0;//Enable
    else
        Alarm.selectedSegmentIndex = 1;//Disable
    [Alarm addTarget:self action:@selector(setAlarm:) forControlEvents:UIControlEventValueChanged];
    
    Vibrate = [[UISegmentedControl alloc] initWithItems:@[@"Enable",@"Disable"]];
    Vibrate.frame = CGRectMake(0.0f, 0.0f, 150.0f, 30.0f);
    //Vibrate.selectedSegmentIndex = [[NSUserDefaults standardUserDefaults] integerForKey:@"Vibrate"];
    if((connectIndication & 0x02) == 0x02)
        Vibrate.selectedSegmentIndex = 0;//Enable
    else
        Vibrate.selectedSegmentIndex = 1;//Disable
    [Vibrate addTarget:self action:@selector(setVibrate:) forControlEvents:UIControlEventValueChanged];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    uuidTextField = [[UITextField alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 290.0f, 30.0f)];
    [uuidTextField setBorderStyle:UITextBorderStyleRoundedRect];
    [uuidTextField setDelegate:self];
    [uuidTextField setFont:[UIFont systemFontOfSize:12]];
    //[uuidTextField setText:@"49535343FE7D4AE58FA99FAFD205E499"];
    BeaconUUID = [[NSUserDefaults standardUserDefaults] stringForKey:@"UUID_BeaconThings"];
    if(BeaconUUID == nil) {
        [uuidTextField setText:@"495353431E144C6F8E45F1EE8D0EAD44"];
        //[uuidTextField setText:@"49535343FE7D4AE58FA99FAFD205E499"];
    }
    else{
        [uuidTextField setText:BeaconUUID];
    }
    uuidTextField.tag = 100;
    /*
     uuidButton= [ISSCButton buttonWithType:UIButtonTypeCustom];
     uuidButton.frame = CGRectMake(0.0f, 0.0f, 100.0f, 30.0f);
     [uuidButton addTarget:self action:@selector(addToList:) forControlEvents:UIControlEventTouchUpInside];
     [uuidButton setTitle:@"Add" forState:UIControlStateNormal];
     uuidButton.titleLabel.adjustsFontSizeToFitWidth = YES;
     */
    
    beaconSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 100.0f, 30.0f)];
    [beaconSwitch addTarget:self action:@selector(beaconOnOff:) forControlEvents:UIControlEventValueChanged];
    //beaconSwitch.on = YES;
    beaconSwitch.on = [[NSUserDefaults standardUserDefaults] boolForKey:@"BM70_BeaconThings"];
    
    
    //self.cbController.beaconTriggerConnection = beaconSwitch.on;
    
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.tabBarController.navigationItem.title = @"Configuration";
    self.tabBarController.navigationItem.rightBarButtonItem = nil;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    //BeaconDataObject* theDataObject = [self theAppDataObject];
    //NSLog(@"[ISSettingController] %@",theDataObject.UUIDStr);
    
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    if (![textField.text isEqualToString:@""]) {
        [[NSUserDefaults standardUserDefaults] setObject:@([textField.text floatValue]) forKey:@"Height"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
#ifdef DebugMode
    [DebugTimer invalidate];
#endif
}

- (void)dealloc {
    [super dealloc];
    //[self stopMonitoringRegion];
    [_locationManager stopUpdatingLocation];
    _locationManager.delegate = nil;
    //[self.cbController release];
    //_locationManager = nil;
    //_delegate = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BeaconDataObject *) theAppDataObject;
{
    id<AppDelegateProtocol> theDelegate = (id<AppDelegateProtocol>) [UIApplication sharedApplication].delegate;
    BeaconDataObject *theDataObject;
    theDataObject = (BeaconDataObject*) theDelegate.theAppDataObject;
    return theDataObject;
}

#ifdef DebugMode
- (void)DebugDebug {
    //NSLog(@"Debug function,%@",_StrServiceUUID);
    
    [self stopMonitoringRegion];
    
    [self performSelector:@selector(startToMonitorRegion) withObject:@"Reset" afterDelay:3.0];
}
#endif

- (void)setProximityValue:(UISegmentedControl *)segment {
    //[[NSUserDefaults standardUserDefaults] setObject:@(segment.selectedSegmentIndex) forKey:@"Unit"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
    
    BeaconDataObject* theDataObject = [self theAppDataObject];
    theDataObject.APPConfig &= ~(0x38);
    switch(segment.selectedSegmentIndex){
        case 1:
            theDataObject.APPConfig |= Proximity_Near;
            break;
        case 2:
            theDataObject.APPConfig |= Proximity_Far;
            break;
        case 0:
        default:
            theDataObject.APPConfig |= Proximity_Immediate;
            break;
    }
    
    if((theDataObject.APPConfig & BTStateConnected) == BTStateConnected) {
        UINavigationController *NavVC = (UINavigationController *)[self.tabBarController.viewControllers objectAtIndex:0];
        BTISWeightController *myVC = (BTISWeightController *)NavVC.topViewController;
        self.cbController.beaconData = nil;
        [myVC Manual_LE_Disconnect];
    }
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(BeaconDelay) object:nil];
    
    theDataObject.APPConfig |= CheckBeaconRanging;
    
    [self performSelector:@selector(BeaconDelay) withObject:nil afterDelay:5.0];
    
}

- (void)setAlarm:(UISegmentedControl *)segment {
    //NSLog(@"setAlarm = %d",(int)segment.selectedSegmentIndex);
    BeaconDataObject* theDataObject = [self theAppDataObject];
    if(segment.selectedSegmentIndex == 0)
    {
        connectIndication |= 0x01;
        theDataObject.APPConfig |= RingAlarm;
    }
    else
    {
        connectIndication &= ~(0x01);
        theDataObject.APPConfig &= ~(RingAlarm);
    }
    
    [[NSUserDefaults standardUserDefaults] setInteger:connectIndication forKey:@"ConnectIndication"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSLog(@"%d",(int)connectIndication);
}

- (void)setVibrate:(UISegmentedControl *)segment {
    //NSLog(@"setVibrate = %d",(int)segment.selectedSegmentIndex);
    BeaconDataObject* theDataObject = [self theAppDataObject];
    if(segment.selectedSegmentIndex == 0)
    {
        connectIndication |= 0x02;
        theDataObject.APPConfig |= Vibration;
    }
    else
    {
        connectIndication &= ~(0x02);
        theDataObject.APPConfig &= ~(Vibration);
    }
    
    [[NSUserDefaults standardUserDefaults] setInteger:connectIndication forKey:@"ConnectIndication"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSLog(@"%d",(int)connectIndication);
}

- (void)BeaconDelay{
    
    BeaconDataObject* theDataObject = [self theAppDataObject];
    theDataObject.APPConfig &= ~(CheckBeaconRanging);
}

- (void)beaconOnOff:(id)sender {
    UISwitch *tmp = (UISwitch *)sender;
    //NSLog(@"Switch state = %d",tmp.on);
    
    beaconSwitch.on = tmp.on;
    [self.tableView reloadData];
    
    self.cbController.beaconTriggerConnection = beaconSwitch.on;
    
    BeaconDataObject* theDataObject = [self theAppDataObject];
    
    //theDataObject.APPConfig |= CheckBeaconRanging;
    if((theDataObject.APPConfig & (ResetBeaconDetection | BeaconStart)) == 0){
        theDataObject.APPConfig |= ResetBeaconDetection;
        
        if((theDataObject.APPConfig & BTStateConnected) == BTStateConnected) {
            UINavigationController *NavVC = (UINavigationController *)[self.tabBarController.viewControllers objectAtIndex:0];
            BTISWeightController *myVC = (BTISWeightController *)NavVC.topViewController;
            [myVC Manual_LE_Disconnect];
        }
        
        theDataObject.UUIDStr = nil;
        self.cbController.beaconData = nil;
    }
    
    if(beaconSwitch.on)
    {
        NSLog(@"BeaconThings ON!");
        
        theDataObject.APPConfig |= BeaconThingsOnOff;
    }
    else
    {
        theDataObject.APPConfig &= ~(BeaconThingsOnOff);
        
        //[self stopMonitoringRegion];
        
        NSLog(@"BeaconThings OFF!");
    }
    
    //[NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(BeaconDelay) object:nil];
    
    //[self performSelector:@selector(BeaconDelay) withObject:nil afterDelay:5.0];
    
    
    [[NSUserDefaults standardUserDefaults] setBool:tmp.on forKey:@"BM70_BeaconThings"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //NSLog(@"APPConfig = %d",theDataObject.APPConfig);
}

- (void)addToList:(id)sender {
    if ([uuidTextField.text length] != 32) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Invalid UUID Setting" message:@"UUID must be 128-bit format" delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alertView show];
        // [alertView release];
        return;
    }
    
    NSMutableString *str= [[NSMutableString alloc] initWithString:uuidTextField.text];
    [str insertString:@"-" atIndex:20];
    [str insertString:@"-" atIndex:16];
    [str insertString:@"-" atIndex:12];
    [str insertString:@"-" atIndex:8];
    
    BOOL exist = NO;
    for (NSString *tmp in _uuids) {
        if ([tmp isEqualToString:str]) {
            exist = YES;
            break;
        }
    }
    if (!exist) {
        [_uuids addObject:str];
        [self updateListToFile];
        [self.tableView reloadData];
        [self startToMonitorRegion];
    }
}

- (void)updateListToFile {
    [_beaconListFileHandle truncateFileAtOffset:0];
    for (NSString *str in _uuids) {
        
        [_beaconListFileHandle writeData:[str dataUsingEncoding:NSASCIIStringEncoding]];
        [_beaconListFileHandle seekToFileOffset:0];
        NSLog(@"beacon uuid updateListToFile :%@", [[NSString alloc] initWithData:[_beaconListFileHandle readDataToEndOfFile] encoding:NSASCIIStringEncoding]);
    }
}

- (NSData *)convertHexToData:(NSString *)hexString
{
    NSMutableData *data = [[NSMutableData alloc] init];
    @try {
        NSUInteger location = 0;
        NSString *substring;
        unsigned intValue;
        while (1) {
            substring = [hexString substringWithRange:NSMakeRange(location, 2)];
            location+=2;
            NSScanner *scanner = [NSScanner scannerWithString:[NSString stringWithFormat:@"0x%@", substring]];
            if ([scanner scanHexInt:&intValue]) {
                [data appendBytes:&intValue length:1];
            }
            if (location >= [hexString length]) {
                break;
            }
        }
    }
    @catch (NSException * e) {
        
    }
    return data;
}


- (NSData *)setProximityUUID:(NSString *)uuid majorNumber:(NSString *)major minorNumber:(NSString *)minor {
    // self.uuidStr = uuid;
    uint16_t *tmp = (uint16_t *)[[self convertHexToData:major] bytes];
    // self.majorValue = CFSwapInt16BigToHost(*tmp);
    tmp = (uint16_t *)[[self convertHexToData:minor] bytes];
    // self.minorValue = CFSwapInt16BigToHost(*tmp);;
    NSString *str = [uuid stringByReplacingOccurrencesOfString:@"-" withString:@""];
    NSMutableData *data = [[NSMutableData alloc] initWithData:[self convertHexToData:str]];
    //NSMutableData *data = [[NSMutableData alloc] init];
    //[data appendData:[self convertHexToData:str]];
    [data appendData:[self convertHexToData:major]];
    [data appendData:[self convertHexToData:minor]];
    // NSLog(@"major = %@, minor = %@, beacon data = %@",major, minor, data);
    return data;
}

//Beacon setting
- (void)monitorBeaconSetting {
    NSLog(@"[ConnectViewCntroller] monitorBeaconSetting");
    _beaconRegions = [[NSMutableArray alloc] init];
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    if ([(NSObject *)_locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [_locationManager requestAlwaysAuthorization];
    }
    
    //   NSArray *uuids = [[NSArray alloc]initWithObjects:@"49535343-FE7D-4AE5-8FA9-9FAFD205E455", /*@"49535343-FE7D-4AE5-8FA9-9FAFD205E456",
    //                                                                                             @"49535343-FE7D-4AE5-8FA9-9FAFD205E457",*/ @"49535343-FE7D-4AE5-8FA9-9FAFD205E4FF"/*,UUIDSTR_ISSC_PROPRIETARY_SERVICE*/, nil];
    /*   for (int i = 0; i < [uuids count]; i++) {
     CLBeaconRegion *region = [[CLBeaconRegion alloc] initWithProximityUUID:[[NSUUID alloc] initWithUUIDString:[uuids objectAtIndex:i]] identifier:[NSString stringWithFormat:@"UUID%d", i]];
     region.notifyOnEntry = YES;
     region.notifyOnExit = YES;
     [_beaconRegions addObject:region];
     }*/
}

- (void)stopMonitoringRegion {
    NSLog(@"stop monitoring region,Region count = %d, UUID count = %ld",(int)[_beaconRegions count], [_uuids count]);
    CLBeaconRegion *region = nil;
    
    for (int i = 0; i < [_beaconRegions count]; i++) {
        region = [_beaconRegions objectAtIndex:i];
        [_locationManager stopRangingBeaconsInRegion:region];
        [_locationManager stopMonitoringForRegion:region];
    }
    NSLog(@"stop monitoring region: %@", [_locationManager monitoredRegions]);
    
    //   ISAppDelegate *appDelegate = (ISAppDelegate *)[[UIApplication sharedApplication] delegate];
    //
    //    if ([[[appDelegate navigationController] viewControllers] containsObject:self.cbController] == FALSE) {
    //        [[appDelegate navigationController] pushViewController:self.cbController animated:YES];
    //    }
}

- (void)startToMonitorRegion {
    CLBeaconRegion *region = nil;
    
    //[self stopMonitoringRegion]; //stop monitoring previous beacons
    //[_beaconRegions removeAllObjects];
    short i = 0;
    for (NSString *str in _uuids) {
        NSLog(@"str = %@", str);
        region = [[CLBeaconRegion alloc] initWithProximityUUID:[[NSUUID alloc] initWithUUIDString:str] identifier:[NSString stringWithFormat:@"UUID%d",i++]];
        region.notifyOnEntry = YES;
        region.notifyOnExit = YES;
        [_locationManager startMonitoringForRegion:region];
        //[_locationManager startRangingBeaconsInRegion:beacon];
        [_locationManager requestStateForRegion:region];
        [_beaconRegions addObject:region];
    }
    
    [_locationManager startUpdatingLocation];
    NSLog(@"start to monitor region, count = %d, %@", (int)[_beaconRegions count], [_locationManager monitoredRegions]);
#ifdef DebugMode
    DebugTimer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self
                                                selector:@selector(DebugFunction)
                                                userInfo:nil repeats:YES];
#endif
}

#ifdef DebugMode
- (void)DebugFunction{
    static int timer = 0;
    timer++;
    
    CLBeaconRegion *region = nil;
    BeaconDataObject* theDataObject = [self theAppDataObject];
    
    if((theDataObject.APPConfig & ResetBeaconDetection) == ResetBeaconDetection)
    {
        NSLog(@".");
        theDataObject.APPConfig &= ~(ResetBeaconDetection);
        NSLog(@"[ResetBeaconDetection] , Region count = %d,%ld",(int)[_beaconRegions count] , [_uuids count]);
        for (int i = 0; i < [_beaconRegions count]; i++) {
            region = [_beaconRegions objectAtIndex:i];
            [_locationManager stopRangingBeaconsInRegion:region];
            [_locationManager stopMonitoringForRegion:region];
            
            NSLog(@"**** Stop monitoring region: %@", [_locationManager monitoredRegions]);
            
            [_beaconRegions removeAllObjects];
            timer = 0;
            theDataObject.APPConfig |= BeaconStart;
        }
    }
    else if((theDataObject.APPConfig & BeaconStart) == BeaconStart)
    {
        NSLog(@".");
        if(timer == 5)
        {
            theDataObject.APPConfig &= ~(BeaconStart);
            timer = 0;
            NSLog(@"[Beaconstart] , Region count = %d,%ld",(int)[_beaconRegions count] , [_uuids count]);
            [self startToMonitorRegion];
        }
    }
    
    /*
     static int timer = 0;
     CLBeaconRegion *region = nil;
     
     timer++;
     NSLog(@"Region count = %d,%d",(int)[_beaconRegions count] , [_uuids count]);
     if(timer == 8)
     {
     for (int i = 0; i < [_beaconRegions count]; i++) {
     region = [_beaconRegions objectAtIndex:i];
     [_locationManager stopRangingBeaconsInRegion:region];
     [_locationManager stopMonitoringForRegion:region];
     
     NSLog(@"**** Stop monitoring region: %@", [_locationManager monitoredRegions]);
     
     [_beaconRegions removeAllObjects];
     }
     
     //[DebugTimer invalidate];
     }
     else if(timer == 15)
     {
     [self startToMonitorRegion];
     }*/
}
#endif

- (void)inRangeSetup:(CLBeaconRegion *)beaconRegion {
    [_locationManager startRangingBeaconsInRegion:beaconRegion];
    self.cbController.beaconData = nil;
    NSLog(@"inRangeSetup");
    
    UILocalNotification *localNotification = [[UILocalNotification alloc] init]; //Create the localNotification object
    
    //[localNotification setAlertAction:@"Weight Scale APP WakeUp"]; //The button's text that launches the application and is shown in the alert
    [localNotification setAlertAction:@"BeaconThings APP WakeUp"]; //The button's text that launches the application and is shown in the alert
    [localNotification setAlertBody:@"Enter region. Find a accessory"]; //Set the message in the notification from the textField's text
    [localNotification setHasAction: YES]; //Set that pushing the button will launch the application
    [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
    UIApplication *application = [UIApplication sharedApplication];
    
    if ([application applicationState] == UIApplicationStateBackground) {
        NSLog(@"running background");
        _bgTask = [application beginBackgroundTaskWithExpirationHandler:^{
            // Clean up any unfinished task business by marking where you
            // stopped or ending the task outright.
            [application endBackgroundTask:_bgTask];
            _bgTask = UIBackgroundTaskInvalid;
        }];
    }
    //  [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(test) userInfo:nil repeats:YES];
}

- (void)locationManager:(CLLocationManager *)manager
         didEnterRegion:(CLRegion *)region {
    CLBeaconRegion *beaconRegion = (CLBeaconRegion *)region;
    NSLog(@"didEnterRegion, %@, major = %d, minor = %d", region.identifier, [beaconRegion.major intValue], [beaconRegion.minor intValue]);
    [self inRangeSetup:beaconRegion];
    
}

- (void)test {
    NSLog(@"remaind time = %f", [[UIApplication sharedApplication] backgroundTimeRemaining]);
}

- (void)locationManager:(CLLocationManager *)manager
          didExitRegion:(CLRegion *)region {
    NSLog(@"didExitRegion, %@.............................", region.identifier);
    UILocalNotification *localNotification = [[UILocalNotification alloc] init]; //Create the localNotification object
    
    //[localNotification setAlertAction:@"Lighting APP WakeUp"]; //The button's text that launches the application and is shown in the alert
    [localNotification setAlertAction:@"BeaconThings APP WakeUp"]; //The button's text that launches the application and is shown in the alert
    [localNotification setAlertBody:@"Exit region"]; //Set the message in the notification from the textField's text
    [localNotification setHasAction: YES]; //Set that pushing the button will launch the application
    [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
    //self.cbController.beaconTriggerConnection = NO;
    
    BeaconDataObject* theDataObject = [self theAppDataObject];
    NSRange range = NSMakeRange(0, 36);
    NSString *UUIDStr = [theDataObject.UUIDStr substringWithRange:range];
    NSLog(@"%@",UUIDStr);
    
    CLBeaconRegion *beaconRegion = (CLBeaconRegion *)region;
    NSUUID *uuid = beaconRegion.proximityUUID;
    NSLog(@"%@",[uuid UUIDString]);
    
    if([UUIDStr isEqualToString:[uuid UUIDString]]) {
        NSString *Str1 = [theDataObject.BeaconDatabase objectForKey:theDataObject.UUIDStr];
        if(Str1 != nil)
        {
            [theDataObject.BeaconDatabase removeObjectForKey:theDataObject.UUIDStr];
            NSLog(@"Clear BeaconDatabase");
        }
        theDataObject.UUIDStr = nil;
    }
}

- (void)locationManager:(CLLocationManager *)manager
        didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region {
    CLBeacon *nearbyBeacon = nil;
    BeaconDataObject* theDataObject = [self theAppDataObject];
    bool ProximityTrigger = false;
    static int count = 0;
    static int ProximityChangeCount = 0;
    
    //  NSLog(@"didRangeBeacons: flag=%d, %@", _showBeaconFlag, _beaconDatabase);
    for (int i = 0; i < [beacons count]; i++) {
        nearbyBeacon = [beacons objectAtIndex:i];
        NSData *beaconData = [self setProximityUUID:[nearbyBeacon.proximityUUID UUIDString] majorNumber:[NSString stringWithFormat:@"%04X", [nearbyBeacon.major intValue]] minorNumber:[NSString stringWithFormat:@"%04X", [nearbyBeacon.minor intValue]]];
        
        //NSLog(@"[didRangeBeacons] Proximity UUID = %@",[nearbyBeacon.proximityUUID UUIDString]);
        
         if([self.delegate respondsToSelector:@selector(UpdateBeaconData:)]) {
         [self.delegate UpdateBeaconData:[NSString stringWithFormat:@"%@%04X%04X", [nearbyBeacon.proximityUUID UUIDString], [nearbyBeacon.major intValue], [nearbyBeacon.minor intValue]]];
         }
        
        if(nearbyBeacon.proximity != theDataObject.New_Proximity)
        {
            ProximityChangeCount++;
            if(ProximityChangeCount > 3){
                ProximityChangeCount = 0;
                theDataObject.New_Proximity = nearbyBeacon.proximity;
            }
        }
        
        count++;
        if(count >= 3)
        {
            count = 0;
            NSLog(@"[didRangeBeacons] count=%d, major=%d, minor=%d, rssi=%d, proximity=%d, accuracy=%f", (int)[beacons count], [nearbyBeacon.major intValue], [nearbyBeacon.minor intValue], (int)nearbyBeacon.rssi, (int)nearbyBeacon.proximity, nearbyBeacon.accuracy);
        }
        
        if(nearbyBeacon.proximity != CLProximityUnknown){
            if((theDataObject.APPConfig & CheckBeaconRanging) == CheckBeaconRanging)
            {
                return;
            }
        }
        
        if(nearbyBeacon.proximity != CLProximityUnknown)
        {
            //NSString *str = [NSString stringWithFormat:@"%@%04X%04X", [nearbyBeacon.proximityUUID UUIDString], [nearbyBeacon.major intValue], [nearbyBeacon.minor intValue]];
            NSString *str = [NSString stringWithFormat:@"%@%04X%04X%0X", [nearbyBeacon.proximityUUID UUIDString], [nearbyBeacon.major intValue], [nearbyBeacon.minor intValue], nearbyBeacon.proximity];
            
            //NSString *str = [NSString stringWithFormat:@"%@%04X%04X%X%X", [nearbyBeacon.proximityUUID UUIDString], [nearbyBeacon.major intValue], [nearbyBeacon.minor intValue], nearbyBeacon.rssi , nearbyBeacon.proximity];
            
            //NSString *Number = [_beaconDatabase objectForKey:str];
            NSString *Str1 = [theDataObject.BeaconDatabase objectForKey:str];
            if(Str1 == nil)
            {
                theDataObject.UUIDStr = [NSString stringWithFormat:@"%@",str];
                
                [theDataObject.BeaconDatabase setObject:[NSString stringWithFormat:@"%X",BeaconNumber] forKey:str];
                NSLog(@"Add beacon info to database,%d",(int)BeaconNumber);
            }
        }
        
        if (self.cbController.beaconData == nil) {
            //self.beaconData = [self setProximityUUID:[nearbyBeacon.proximityUUID UUIDString] majorNumber:[NSString stringWithFormat:@"%04X", [nearbyBeacon.major intValue]] minorNumber:[NSString stringWithFormat:@"%04X", [nearbyBeacon.minor intValue]]];
            
            //self.cbController.beaconTriggerConnection = YES;
            //if(self.cbController.beaconTriggerConnection == YES)
            if((theDataObject.APPConfig & BeaconThingsOnOff) == BeaconThingsOnOff)
            {
                NSLog(@"APPConfig = %x\n",theDataObject.APPConfig);
                if(nearbyBeacon.proximity == CLProximityUnknown)
                    return;
                else {
                    int i = (int)nearbyBeacon.proximity;
                    if((i == 1) && ((theDataObject.APPConfig & Proximity_Immediate) == Proximity_Immediate)){
                        ProximityTrigger = true;
                    }
                    else if((i == 2) && ((theDataObject.APPConfig & Proximity_Near) == Proximity_Near)){
                        ProximityTrigger = true;
                    }
                    else if((i == 3) && ((theDataObject.APPConfig & Proximity_Far) == Proximity_Far)){
                        ProximityTrigger = true;
                    }
                }
                
                if(!ProximityTrigger)
                    return;
                
                if(theDataObject.UUIDStr == nil){
                    [theDataObject.BeaconDatabase removeAllObjects];
                    //theDataObject.APPConfig &= ~(BTStateConnected);
                    NSLog(@"#### Reset BeacoDatabase,%x",theDataObject.APPConfig);
                }
                
                [self.cbController setBeaconData:beaconData];
                //BeaconThingsUUID = [NSString stringWithFormat:@"%@%04X%04X", [nearbyBeacon.proximityUUID UUIDString], [nearbyBeacon.major intValue], [nearbyBeacon.minor intValue]];
                //_StrServiceUUID = [NSString stringWithFormat:@"%@%04X%04X", [nearbyBeacon.proximityUUID UUIDString], [nearbyBeacon.major intValue], [nearbyBeacon.minor intValue]];
                
                [self.delegate UpdateBeaconData:[NSString stringWithFormat:@"%@%04X%04X", [nearbyBeacon.proximityUUID UUIDString], [nearbyBeacon.major intValue], [nearbyBeacon.minor intValue]]];
                
                NSLog(@"***Trigger LE-Connection,UUID = %@",[NSString stringWithFormat:@"%@%04X%04X", [nearbyBeacon.proximityUUID UUIDString], [nearbyBeacon.major intValue], [nearbyBeacon.minor intValue]]);
                //NSArray *cbuuids = @[[CBUUID UUIDWithString:[nearbyBeacon.proximityUUID UUIDString]]];
                NSArray *cbuuids = @[[CBUUID UUIDWithString:UUIDSTR_ISSC_PERIPHERAL_SERVICE]];
                [self.cbController startScanWithUUID:cbuuids];
                //[self.cbController startScan];
            }
        }
        else {
            //      [[NSNotificationCenter defaultCenter] postNotificationName:ISPeripheralNotificationSignalReport object:nearbyBeacon];
        }
        //else if (nearbyBeacon.proximity == CLProximityFar || nearbyBeacon.accuracy == -1) {
        /*    else if (nearbyBeacon.accuracy > 2 || nearbyBeacon.accuracy == -1) {
         [[NSNotificationCenter defaultCenter] postNotificationName:ISPeripheralNotificationSignalWeak object:beaconData];
         // [self.cbController disconnectPeripheralWithBeacon:beaconData];
         }
         else {
         [[NSNotificationCenter defaultCenter] postNotificationName:ISPeripheralNotificationSignalNormal object:beaconData];
         }*/
    }
}

- (void)locationManager:(CLLocationManager *)manager
      didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region {
    CLBeaconRegion *beaconRegion = (CLBeaconRegion *)region;
    NSLog(@"didDetermineState: %d -->%@, %@", (int)state, beaconRegion.identifier,[beaconRegion.proximityUUID UUIDString]);
    if (state == CLRegionStateInside) {
        NSLog(@"didDetermineState, inside");
        [self inRangeSetup:beaconRegion];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField1 {
    
    bool Error = false;
    
    switch (textField1.tag) {
        case 100:
            NSLog(@"iBeacon UUID");
            if([[textField1 text] length] != 32)
                Error = true;
            else{
                [[NSUserDefaults standardUserDefaults] setObject:textField1.text forKey:@"UUID_BeaconThings"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                NSMutableString *str= [[NSMutableString alloc] initWithString:textField1.text];
                [str insertString:@"-" atIndex:20];
                [str insertString:@"-" atIndex:16];
                [str insertString:@"-" atIndex:12];
                [str insertString:@"-" atIndex:8];
                //NSString *BeaconUUID = [NSString stringWithString:str];
                NSLog(@"iBeacon UUID = %@",str);
            }
            break;
        case 200:
        {
            NSLog(@"Service UUID");
            if([[textField1 text] length] != 32)
                Error = true;
            else
            {
                [[NSUserDefaults standardUserDefaults] setObject:textField1.text forKey:@"TriggerServiceUUID"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                NSMutableString *str= [[NSMutableString alloc] initWithString:textField1.text];
                [str insertString:@"-" atIndex:20];
                [str insertString:@"-" atIndex:16];
                [str insertString:@"-" atIndex:12];
                [str insertString:@"-" atIndex:8];
                _StrServiceUUID = [NSString stringWithString:str];
                NSLog(@"%@",_StrServiceUUID);
            }
        }
            //_StrServiceUUID = [textField1 text];
            break;
        case 300:
            NSLog(@"Device Name");
            [[NSUserDefaults standardUserDefaults] setObject:textField1.text forKey:@"TriggerDeviceName"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        default:
            break;
    }
    if(Error)
    {
        textField1.text = @"";
    }
    
    [textField1 resignFirstResponder];
    return TRUE;
}

- (BOOL)textField:(UITextField *)textField1 shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSCharacterSet *unacceptedInput = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890aAbBcCdDeEfF"] invertedSet];
    if ([[string componentsSeparatedByCharactersInSet:unacceptedInput] count] > 1)
        return NO;
    else {
        NSUInteger newLength = [textField1.text length] + [string length] - range.length;
        return (newLength > 32) ? NO : YES;
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger num = 1;
    switch (section) {
        case 0:
            num = 2;
            break;
        case 1:
            num = 3;
            break;
        case 2:
            num = 2;
            //num = [_uuids count];
            break;
        default:
            break;
    }
    return num;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *title = nil;
    switch (section) {
        case 0:
            //title = @"Weight Scale";
            title = @"iBeacon UUID Register";
            break;
        case 1:
            //title = @"Add Beacon UUID";
            title = @"Trigger Connection";
            break;
        case 2:
            //title = @"Listened Beacons";
            title = @"Connection Indication";
            break;
        default:
            title = @"xxx";
            break;
    }
    return title;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    switch (indexPath.section) {
        case 0:
            switch (indexPath.row) {
                case 0:
                    cell.textLabel.text = @"UUID";
                    //cell.accessoryView = textField;
                    cell.accessoryView = uuidTextField;
                    break;
                case 1:
                    //cell.textLabel.text = @"Beacon On/OFF";
                    if(beaconSwitch.on)
                        cell.textLabel.text = @"BeaconThings On";
                    else
                        cell.textLabel.text = @"BeaconThings Off";
                    cell.accessoryView = beaconSwitch;
                    break;
                default:
                    break;
            }
            break;
        case 1:
            switch (indexPath.row) {
                case 0:
                    cell.textLabel.text = @"Region";
                    cell.accessoryView = unit;
                    break;
                case 1:
                    cell.textLabel.text = @"Service UUID";
                    cell.accessoryView = ServicetextField;
                    break;
                case 2:
                    cell.textLabel.text = @"Device Name";
                    cell.accessoryView = textField;
                    break;
                default:
                    break;
            }
            break;
        case 2:
            if(indexPath.row == 0) {
                cell.textLabel.text = @"Ring Alarm";
                cell.accessoryView = Alarm;
            }
            else if(indexPath.row == 1) {
                cell.textLabel.text = @"Vibrate";
                cell.accessoryView = Vibrate;
            }
            //cell.textLabel.text = [_uuids objectAtIndex:indexPath.row];
            //[cell.textLabel setFont:[UIFont systemFontOfSize:12]];
            break;
        default:
            break;
    }
    
    
    // Configure the cell...
    
    return cell;
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

