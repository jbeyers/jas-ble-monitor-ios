//
//  BTConnectViewController.m
//  BeaconThings
//
//  Created by ILYAS on 16/02/16.
//  Copyright © 2016 Rick. All rights reserved.
//

#import "AppDelegate.h"
#import "BTConnectViewController.h"
#import "ISSCButton.h"

@interface BTConnectViewController ()

@end

@implementation BTConnectViewController

//@synthesize actionButton;
@synthesize activityIndicatorView;
@synthesize statusLabel;
@synthesize connectionStatus;
@synthesize versionLabel;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
            self.edgesForExtendedLayout = UIRectEdgeNone;
        }
        
        /*UIBarButtonItem *backButton = [[UIBarButtonItem alloc] init];
        backButton.title = @"Back";
        self.tabBarController.navigationItem.backBarButtonItem = backButton;*/
        
        
        /*UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(200, 28, 57, 57)];
         [titleLabel setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Icon_old"]]];
         [titleLabel setTextAlignment:NSTextAlignmentCenter];//aaa
         self.navigationItem.titleView = titleLabel;*/
        
        self.title = @"Scan";
        ISSCButton *button = [ISSCButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(0.0f, 0.0f, 80.0f, 30.0f);
        [button addTarget:self action:@selector(refreshDeviceList:) forControlEvents:UIControlEventTouchUpInside];
        [button setTitle:@"Refresh" forState:UIControlStateNormal];
        refreshButton = [[UIBarButtonItem alloc] initWithCustomView:button];
        //refreshButton = [[UIBarButtonItem alloc] initWithTitle:@"Refresh" style:UIBarButtonItemStyleBordered target:self action:@selector(refreshDeviceList:)];
        button = [ISSCButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(0.0f, 0.0f, 80.0f, 30.0f);
        [button addTarget:self action:@selector(startScan) forControlEvents:UIControlEventTouchUpInside];
        [button setTitle:@"  Scan  " forState:UIControlStateNormal];
        scanButton = [[UIBarButtonItem alloc] initWithCustomView:button];
        //scanButton = [[UIBarButtonItem alloc] initWithTitle:@"  Scan  " style:UIBarButtonItemStyleBordered target:self action:@selector(actionButtonStartScan:)];
        button = [ISSCButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(0.0f, 0.0f, 80.0f, 30.0f);
        [button addTarget:self action:@selector(actionButtonCancelScan:) forControlEvents:UIControlEventTouchUpInside];
        [button setTitle:@"Cancel" forState:UIControlStateNormal];
        cancelButton = [[UIBarButtonItem alloc] initWithCustomView:button];
        //cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(actionButtonCancelScan:)];
        button = [ISSCButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(0.0f, 0.0f, 90.0f, 30.0f);
        [button addTarget:self action:@selector(manualUUIDSetting:) forControlEvents:UIControlEventTouchUpInside];
        [button setTitle:@"UUID Setting" forState:UIControlStateNormal];
        button.titleLabel.adjustsFontSizeToFitWidth = YES;
        uuidSettingButton = [[UIBarButtonItem alloc] initWithCustomView:button];
        //uuidSettingButton = [[UIBarButtonItem alloc] initWithTitle:@"UUID Setting" style:UIBarButtonItemStyleBordered target:self action:@selector(manualUUIDSetting:)];
        
        //refreshButton = [[UIBarButtonItem alloc] initWithTitle:@"Refresh" style:UIBarButtonItemStyleBordered target:self action:@selector(refreshDeviceList:)];
        //scanButton = [[UIBarButtonItem alloc] initWithTitle:@"  Scan  " style:UIBarButtonItemStyleBordered target:self action:@selector(startScan)];
        //cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(actionButtonCancelScan:)];
        
        connectedDeviceInfo = [NSMutableArray new];
        connectingList = [NSMutableArray new];
        
        refreshDeviceListTimer = nil;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    [self setConnectionStatus:BT_LE_STATUS_IDLE];
    ReliableBurstData *data = [[ReliableBurstData alloc] init];
    //[versionLabel setText:[NSString stringWithFormat:@"WeightScale %@, library %@, %s",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"],[data version], __DATE__]];
    [versionLabel setText:[NSString stringWithFormat:@"BeaconThings %@, library %@, %s",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"],[data version], __DATE__]];
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self performSelector:@selector(startScan) withObject:nil afterDelay:0.1];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self stopScan];
}

- (void)viewDidUnload
{
    devicesTableView = nil;
    [self setVersionLabel:nil];
    refreshButton = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    NSLog(@"[ConnectViewController] didReceiveMemoryWarning");
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)dealloc {
    [self stopScan];
    [super dealloc];
    
}

- (void) displayDevicesList {
    [devicesTableView reloadData];
}

- (void) switchToMainFeaturePage {
    NSLog(@"[ConnectViewController] switchToMainFeaturePage");
}

- (int) connectionStatus {
    return connectionStatus;
}

- (void) setConnectionStatus:(int)status {
    if (status == BT_LE_STATUS_IDLE) {
        statusLabel.textColor = [UIColor redColor];
    }
    else {
        statusLabel.textColor = [UIColor blackColor];
    }
    connectionStatus = status;
    
    switch (status) {
        case BT_LE_STATUS_IDLE:
            statusLabel.text = @"Idle";
            [activityIndicatorView stopAnimating];
            break;
        case BT_LE_STATUS_SCANNING:
            [devicesTableView reloadData];
            statusLabel.text = @"Scanning...";
            [activityIndicatorView startAnimating];
            break;
        default:
            break;
    }
    [self updateButtonType];
}

- (IBAction)actionButtonCancelScan:(id)sender {
    NSLog(@"[ConnectViewController] actionButtonCancelScan");
    [self stopScan];
    [self setConnectionStatus:BT_LE_STATUS_IDLE];
}

- (void)startScan {
    [super startScan];
    if ([connectingList count] > 0) {
        for (int i=0; i< [connectingList count]; i++) {
            BTMyPeripheral *connectingPeripheral = [connectingList objectAtIndex:i];
            
            if (connectingPeripheral.connectStaus == BT_MYPERIPHERAL_CONNECT_STATUS_CONNECTING) {
                //NSLog(@"startScan add connecting List: %@",connectingPeripheral.advName);
                [devicesList addObject:connectingPeripheral];
            }
            else {
                [connectingList removeObjectAtIndex:i];
                //NSLog(@"startScan remove connecting List: %@",connectingPeripheral.advName);
            }
        }
    }
    [self setConnectionStatus:BT_LE_STATUS_SCANNING];
}

- (void)stopScan {
    [super stopScan];
    if (refreshDeviceListTimer) {
        [refreshDeviceListTimer invalidate];
        refreshDeviceListTimer = nil;
    }
}

-(void)popToRootPage {
    
}

- (void)updateDiscoverPeripherals {
    [super updateDiscoverPeripherals];
    if (self.beaconTriggerConnection) {
        NSRange searchRange;
        for (BTMyPeripheral *myperipheral in devicesList) {
            
            if (myperipheral.serviceData && self.beaconData)
            {
                NSArray *serviceDatas = [myperipheral.serviceData allValues];
                for (int i = 0; i < [serviceDatas count]; i++) {
                    if (myperipheral.connectStaus == BT_MYPERIPHERAL_CONNECT_STATUS_IDLE) {
                        id data = [serviceDatas objectAtIndex:i];
                        //NSLog(@"[ConnectViewController] updateDiscoverPeripherals servicedata,%d, %@, %@",[serviceDatas count] ,data, myperipheral.serviceData);
                        if ([data isKindOfClass:[NSData class]]) {
                            searchRange.location = 0;
                            searchRange.length = [self.beaconData length];
                            NSLog(@"***===== beaconData = %@\n,serviceData = %@",self.beaconData, data);
                            NSRange range = [self.beaconData rangeOfData:data options:NSDataSearchBackwards range:searchRange];
                            if (range.location != NSNotFound && range.length != 0) {
                                [self connectDevice:myperipheral];
                                myperipheral.connectStaus = BT_MYPERIPHERAL_CONNECT_STATUS_CONNECTING;
                                [devicesList replaceObjectAtIndex:i withObject:myperipheral];
                                NSLog(@"BeaconThings connect..");
                                [connectingList addObject:myperipheral];
                                [self setBeaconData:nil];
                                break;
                            }
                        }
                    }
                }
            }
            
            /*
             //if([myperipheral.advName isEqualToString:@"BLESDK01"]){
             if([myperipheral.advName isEqualToString:@"BT"]){
             NSLog(@"***Find BM70_BeaconThing device");
             if(myperipheral.serviceData == nil)
             NSLog(@"No Service data");
             
             if(myperipheral.serviceUUIDs == nil)
             NSLog(@"No Service UUID");
             else{
             NSLog(@"%d", [myperipheral.serviceUUIDs count]);
             }
             
             if(self.beaconData)
             NSLog(@"beaconData = %@",self.beaconData);
             
             if (myperipheral.serviceData && self.beaconData)
             {
             NSArray *serviceDatas = [myperipheral.serviceData allValues];
             for (int i = 0; i < [serviceDatas count]; i++) {
             id data = [serviceDatas objectAtIndex:i];
             NSLog(@"[ConnectViewController] updateDiscoverPeripherals servicedata,%d, %@, %@",[serviceDatas count] ,data, myperipheral.serviceData);
             
             if ([data isKindOfClass:[NSData class]])
             {
             searchRange.location = 0;
             searchRange.length = [self.beaconData length];
             NSLog(@"***===== beaconData = %@\n,serviceData = %@",self.beaconData, data);
             NSRange range = [self.beaconData rangeOfData:data options:NSDataSearchBackwards range:searchRange];
             if (range.location != NSNotFound && range.length != 0) {
             NSLog(@"BeaconThings OK OK");
             }
             }
             }
             }
             
             NSLog(@"Connect..");
             [self connectDevice:myperipheral];
             myperipheral.connectStaus = MYPERIPHERAL_CONNECT_STATUS_CONNECTING;
             [connectingList addObject:myperipheral];
             [self setBeaconData:nil];
             break;
             }*/
        }
    }
    [devicesTableView reloadData];
}

- (void)updateMyPeripheralForDisconnect:(BTMyPeripheral *)myPeripheral {
    NSLog(@"updateMyPeripheralForDisconnect");//, %@", myPeripheral.advName);
    [super updateMyPeripheralForDisconnect:myPeripheral];
    if (myPeripheral == controlPeripheral) {
        [NSTimer scheduledTimerWithTimeInterval:0.03 target:self selector:@selector(popToRootPage) userInfo:nil repeats:NO];
    }
    
    for (int idx =0; idx< [connectingList count]; idx++) {
        BTMyPeripheral *tmpPeripheral = [connectingList objectAtIndex:idx];
        if (tmpPeripheral == myPeripheral) {
            [connectingList removeObjectAtIndex:idx];
            //NSLog(@"updateMyPeripheralForDisconnect2");
            break;
        }
        else{
            //NSLog(@"updateMyPeripheralForDisconnect3 %@, %@", tmpPeripheral.advName, myPeripheral.advName);
        }
        
    }
    
    [self displayDevicesList];
    [self updateButtonType];
    
    if(connectionStatus == BT_LE_STATUS_SCANNING){
        /*[self stopScan];
         [self startScan];
         [devicesTableView reloadData];*/
    }
}

- (void)updateMyPeripheralForNewConnected:(BTMyPeripheral *)myPeripheral {
    NSLog(@"[ConnectViewController] updateMyPeripheralForNewConnected");
    /*Connected List Filter*/
    
    for (int idx =0; idx< [connectingList count]; idx++) {
        BTMyPeripheral *tmpPeripheral = [connectingList objectAtIndex:idx];
        if (tmpPeripheral == myPeripheral) {
            //NSLog(@"connectingList removeObject:%@",tmpPeripheral.advName);
            [connectingList removeObjectAtIndex:idx];
            break;
        }
    }
    
    for (int idx =0; idx< [devicesList count]; idx++) {
        BTMyPeripheral *tmpPeripheral = [devicesList objectAtIndex:idx];
        if (tmpPeripheral == myPeripheral) {
            //NSLog(@"devicesList removeObject:%@",tmpPeripheral.advName);
            [devicesList removeObjectAtIndex:idx];
            break;
        }
    }
    [self displayDevicesList];
    [self updateButtonType];
}

// DataSource methods
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //NSLog(@"[ConnectViewController] numberOfRowsInSection,device count = %d", [devicesList count]);
    /*switch (section) {
     case 0:
     return [connectedDeviceInfo count];
     case 1:*/
    return [devicesList count];
    /*default:
     return 0;
     }
     }*/
}
- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell;
    
    /*switch (indexPath.section) {
     case 0:
     {
     //NSLog(@"[ConnectViewController] CellForRowAtIndexPath section 0, Row = %d",[indexPath row]);
     cell = [tableView dequeueReusableCellWithIdentifier:@"connectedList"];
     if (cell == nil) {
     cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"connectedList"];
     }
     cell.detailTextLabel.text = @"connected";
     cell.accessoryView = nil;
     if (cell.textLabel.text == nil)
     cell.textLabel.text = @"Unknow";
     
     UIButton *accessoryButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
     [accessoryButton addTarget:self action:@selector(actionButtonDisconnect:)  forControlEvents:UIControlEventTouchUpInside];
     accessoryButton.tag = indexPath.row;
     [accessoryButton setTitle:@"Disconnect" forState:UIControlStateNormal];
     [accessoryButton setFrame:CGRectMake(0,0,100,35)];
     cell.accessoryView  = accessoryButton;
     }
     break;
     
     case 1:
     {*/
    //NSLog(@"[ConnectViewController] CellForRowAtIndexPath section 1, Row = %d",[indexPath row]);
    
    cell = [tableView dequeueReusableCellWithIdentifier:@"devicesList"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"devicesList"] ;
    }
    BTMyPeripheral *tmpPeripheral = [devicesList objectAtIndex:indexPath.row];
    cell.textLabel.text = tmpPeripheral.advName;
    cell.detailTextLabel.text = @"";
    cell.accessoryView = nil;
    if (tmpPeripheral.connectStaus == BT_MYPERIPHERAL_CONNECT_STATUS_CONNECTING) {
        cell.detailTextLabel.text = @"connecting...";
        UIButton *accessoryButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [accessoryButton addTarget:self action:@selector(actionButtonCancelConnect:)  forControlEvents:UIControlEventTouchUpInside];
        accessoryButton.tag = indexPath.row;
        [accessoryButton setTitle:@"Cancel" forState:UIControlStateNormal];
        [accessoryButton setFrame:CGRectMake(0,0,100,35)];
        cell.accessoryView  = accessoryButton;
        
    }
    
    if (cell.textLabel.text == nil)
        cell.textLabel.text = @"Unknow";
    /*}
     break;
     }*/
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    NSString *title = nil;
    /*switch (section) {
     case 0:
     //title = @"Connected Device:";
     break;
     case 1:*/
    title = @"Discovered Devices:";
    /*		break;
     
     default:
     break;
     }*/
    return title;
}


-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    /*switch (indexPath.section) {
     case 0:
     {
     //NSLog(@"[ConnectViewController] didSelectRowAtIndexPath section 0, Row = %d",[indexPath row]);
     [self stopScan];
     [self setConnectionStatus:LE_STATUS_IDLE];
     [activityIndicatorView stopAnimating];
     if (refreshDeviceListTimer) {
     [refreshDeviceListTimer invalidate];
     refreshDeviceListTimer = nil;
     }
     [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(switchToMainFeaturePage) userInfo:nil repeats:NO];
     }
     break;
     case 1:
     {*/
    //Derek
    NSLog(@"[ConnectViewController] didSelectRowAtIndexPath section 0, Row = %ld",(long)[indexPath row]);
    int count = (int)[devicesList count];
    if ((count != 0) && count > indexPath.row) {
        /*MyPeripheral *tmpPeripheral = [devicesList objectAtIndex:indexPath.row];
         if (tmpPeripheral.connectStaus != MYPERIPHERAL_CONNECT_STATUS_IDLE) {
         //NSLog(@"Device is not idle - break");
         break;
         }
         [self connectDevice:tmpPeripheral];
         tmpPeripheral.connectStaus = MYPERIPHERAL_CONNECT_STATUS_CONNECTING;
         [devicesList replaceObjectAtIndex:indexPath.row withObject:tmpPeripheral];
         [connectingList addObject:tmpPeripheral];
         [self displayDevicesList];
         [self updateButtonType];*/
        BTMyPeripheral *tmpPeripheral = [devicesList objectAtIndex:indexPath.row];
        NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
        [def setObject:tmpPeripheral.peripheral.identifier.UUIDString forKey:@"selectedDevice"];
        [def synchronize];
        [self connectDevice:tmpPeripheral];
    }
    /*break;
     }
     default:
     break;
     }*/
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)refreshDeviceList:(id)sender {
    NSLog(@"[ConnectViewController] refreshDeviceList");
    [self stopScan];
    [self startScan];
    [devicesTableView reloadData];
}

- (IBAction)manualUUIDSetting:(id)sender {
    
}

//Derek
- (IBAction)actionButtonDisconnect:(id)sender {
    
}

//Derek
- (IBAction)actionButtonCancelConnect:(id)sender {
    //NSLog(@"[ConnectViewController] actionButtonCancelConnect idx = %d",[sender tag]);
    int idx = (int)[sender tag];
    BTMyPeripheral *tmpPeripheral = [devicesList objectAtIndex:idx];
    tmpPeripheral.connectStaus = BT_MYPERIPHERAL_CONNECT_STATUS_IDLE;
    [devicesList replaceObjectAtIndex:idx withObject:tmpPeripheral];
    
    for (int idx =0; idx< [connectingList count]; idx++) {
        BTMyPeripheral *tmpConnectingPeripheral = [connectingList objectAtIndex:idx];
        if (tmpConnectingPeripheral == tmpPeripheral) {
            [connectingList removeObjectAtIndex:idx];
            break;
        }
    }
    
    [self disconnectDevice:tmpPeripheral];
    [self displayDevicesList];
    [self updateButtonType];
}

- (void) updateButtonType {
    NSArray *toolbarItems = nil;
    switch (connectionStatus) {
        case BT_LE_STATUS_IDLE:
            if (([connectedDeviceInfo count] > 0)||([connectingList count] > 0)) {
                toolbarItems = [[NSArray alloc] initWithObjects:scanButton, nil];
            }
            else {
                toolbarItems = [[NSArray alloc] initWithObjects:scanButton, uuidSettingButton, nil];
            }
            [self setToolbarItems:toolbarItems animated:NO];
            break;
        case BT_LE_STATUS_SCANNING:
            if (([connectedDeviceInfo count] > 0)||([connectingList count] > 0)) {
                toolbarItems = [[NSArray alloc] initWithObjects:refreshButton,cancelButton , nil];
            }
            else {
                toolbarItems = [[NSArray alloc] initWithObjects: refreshButton,cancelButton, uuidSettingButton, nil];
            }
            [self setToolbarItems:toolbarItems animated:NO];
            break;
    }
}

- (void)showAutotest:(id)sender {
    [self stopScan];
}

@end
