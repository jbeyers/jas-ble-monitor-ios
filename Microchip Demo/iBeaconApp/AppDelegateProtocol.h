//
//  ISBeaconViewController.h
//  BeaconThings
//
//  Created by Rick on 2015/11/27.
//  Copyright © 2015年 Rick. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AppDataObject;

@protocol AppDelegateProtocol
- (AppDataObject*) theAppDataObject;
@end
