//
//  ISBeaconViewController.m
//  BeaconThings
//
//  Created by Rick on 2015/11/27.
//  Copyright © 2015年 Rick. All rights reserved.
//

#import "ISBeaconViewController.h"
#import "BTISSettingController.h"
#import "BTISWeightController.h"

#import "AppDelegateProtocol.h"
#import "BeaconDataObject.h"


#define ESTIMOTE_BEACON_UUID    @"B9407F30-F5F8-466E-AFF9-25556B57FE6D"
#define ESTIMOTE_IDENTIFIER     @"Estimode"
//#define ISSC_BEACON_UUID        @"49535343-FE7D-4AE5-8FA9-9FAFD205E456"
#define ISSC_BEACON_UUID        @"49535343-fe7d-4ae5-8fa9-9fafd205e499"
#define ISSC_IDENTIFIER         @"ISSC"

#define ISSC_DEFAULT_BEACON_INFO1   @"49535343-FE7D-4AE5-8FA9-9FAFD205E499007C01C9"
#define ISSC_DEFAULT_BEACON_INFO2   @"49535343-FE7D-4AE5-8FA9-9FAFD205E45500030004"
#define ISSC_DEFAULT_BEACON_INFO3   @"49535343-FE7D-4AE5-8FA9-9FAFD205E45500050006"

@interface ISBeaconViewController ()
{
    NSTimer     *BeaconProcessTimer;
    CLProximity OLD_proximity;
    bool        BTIsConnected;
    int         RSSI;
}
@end

@implementation ISBeaconViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"BeaconThings";
    
    //[self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_bg.png"] forBarMetrics:UIBarMetricsDefault];
    
    //self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"rsz_issc_logo.png"]];
    
    /*UIButton *Logo = [UIButton buttonWithType:UIButtonTypeCustom];
    [Logo setFrame:CGRectMake(0, 0, 63, 40)];
    [Logo setImage:[UIImage imageNamed:@"rsz_microchip_logo.png"] forState:UIControlStateNormal];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc]initWithCustomView:Logo];
    self.tabBarController.navigationItem.leftBarButtonItem = barButton;*/
    
    self.tabBarController.navigationItem.leftBarButtonItem = nil;
    self.tabBarController.navigationItem.rightBarButtonItem = nil;
    
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"iPad_iSSC_background.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    
#if (RSSI_TEST == 1)
    CGPoint center = {147,167};
#else
    CGPoint center = {147,247};
#endif
    
    CGRect rect = [[UIScreen mainScreen]bounds];
    center = CGPointMake((rect.size.width/2)-8, rect.size.height/2);

    _iPhoneView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iPhone.png"]];
    _iPhoneView.frame = CGRectMake(center.x, center.y, 17, 34);
    [_iPhoneView setUserInteractionEnabled:YES];
    [self.view addSubview:_iPhoneView];
    
    UIImageView *rangeView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"range.png"]];
    rangeView.frame = CGRectMake(_iPhoneView.center.x - kMaxRadius, _iPhoneView.center.y - kMaxRadius, kMaxRadius*2, kMaxRadius*2);
    [self.view insertSubview:rangeView belowSubview:_iPhoneView];
    self.halo = [PulsingHaloLayer layer];
    self.halo.position = _iPhoneView.center;
    [self.view.layer insertSublayer:self.halo below:_iPhoneView.layer];
    
    _showBeaconFlag = 0;
    
    [self initBeaconIcons];
}

- (void)viewDidAppear:(BOOL)animated {
    
    //NSLog(@"[ISBeaconView] viewDidAppear");
    
    //self.navigationItem.title = (
    self.tabBarController.navigationItem.rightBarButtonItem = nil;
    
    BeaconDataObject* theDataObject = [self theAppDataObject];
    NSLog(@"%@",theDataObject.UUIDStr);
    
    self.tabBarController.navigationItem.title = (theDataObject.APPConfig & BeaconThingsOnOff) == BeaconThingsOnOff ? @"BeaconThings ON" : @"BeaconThings Off";
    
    if(theDataObject.UUIDStr != nil)
    {
        //NSLog(@"Range = %@",[theDataObject.UUIDStr substringWithRange:range]);
        
        NSMutableArray *BeaconArray = [NSMutableArray array];
        [BeaconArray addObject:theDataObject.BeaconDatabase];
        
        for(id key in theDataObject.BeaconDatabase)
            NSLog(@"key: %@ , value: %@",key,[theDataObject.BeaconDatabase objectForKey:key]);
        
        for(NSInteger i = 0 ; i < [BeaconArray count]; i++)
            NSLog(@"%@",[BeaconArray objectAtIndex:i]);
        
        //[self DrawBeacon:CLProximityNear];
        NSRange range = NSMakeRange(44, 1);
        NSLog(@"[ISBeaconViewController] %@,%ld",theDataObject.UUIDStr, [theDataObject.UUIDStr length]);
        NSString *RangeStr = [theDataObject.UUIDStr substringWithRange:range];
        NSLog(@"Proximity = %ld", [RangeStr integerValue]);
        if([RangeStr integerValue] == 1){
            [self DrawBeacon:CLProximityImmediate];
        }
        else if([RangeStr integerValue] == 2){
            [self DrawBeacon:CLProximityNear];
        }
        else if([RangeStr integerValue] == 3) {
            [self DrawBeacon:CLProximityFar];
        }
    }
    else
    {
        for(int i = (int)self.view.subviews.count-1; i>=0; i--)
        {
            UIView *view = [self.view.subviews objectAtIndex:i];
            if([view isKindOfClass:[UIButton class]]){
                [view removeFromSuperview];
            }
            //[[self.view.subviews objectAtIndex:i] removeFromSuperview];
        }
    }
    
    if((theDataObject.APPConfig & BTStateConnected) == BTStateConnected)
        BTIsConnected = true;
    else
        BTIsConnected = false;
    
    if([BeaconProcessTimer isValid])
    {
        NSLog(@"BeaconTimer isValid");
    }
    else
    {
        //if((theDataObject.APPConfig & BTStateConnected) == BTStateConnected)
        {
            NSLog(@"Create BeaconTimer");
            
            OLD_proximity = theDataObject.New_Proximity;
            
            BeaconProcessTimer = [[NSTimer scheduledTimerWithTimeInterval:1.0 target:self
                                                         selector:@selector(BeaconStatusUpdate)
                                                                 userInfo:nil repeats:YES]retain];
        }
    }
 }

- (void)viewDidDisappear:(BOOL)animated {
    [BeaconProcessTimer invalidate];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [super dealloc];
    [BeaconProcessTimer invalidate];
}

- (BeaconDataObject *) theAppDataObject;
{
    id<AppDelegateProtocol> theDelegate = (id<AppDelegateProtocol>) [UIApplication sharedApplication].delegate;
    BeaconDataObject *theDataObject;
    theDataObject = (BeaconDataObject*) theDelegate.theAppDataObject;
    return theDataObject;
}

- (void)UpdateBeaconData:(NSString *)BeaconData{
    NSLog(@"[ISBeaconView] BeaconThings delegate , %@",BeaconData);
    if(BeaconData != nil)
    {
        [self DrawBeacon:CLProximityNear];
        //NSLog(@"[ISBeaconView] BeaconThings delegate , %@",BeaconData);
    }
}

- (void)BeaconStatusUpdate
{
    BeaconDataObject* theDataObject = [self theAppDataObject];
    
    NSLog(@"Beacon timer --%ld , %@", (theDataObject.APPConfig & BTStateConnected) , theDataObject.UUIDStr);
    
    if(theDataObject.New_Proximity != CLProximityUnknown){
        if(theDataObject.UUIDStr != nil)
        {
            if(OLD_proximity != theDataObject.New_Proximity)
            {
                //Remove All beacon image
                for(int i = (int)self.view.subviews.count-1; i>=0; i--)
                {
                    UIView *view = [self.view.subviews objectAtIndex:i];
                    if([view isKindOfClass:[UIButton class]]){
                        [view removeFromSuperview];
                    }
                }
                OLD_proximity = theDataObject.New_Proximity;
                [self DrawBeacon:OLD_proximity];
                NSLog(@"Beacon : Proximity changed!");
            }
        }
        else
        {
            for(int i = (int)self.view.subviews.count-1; i>=0; i--)
            {
                UIView *view = [self.view.subviews objectAtIndex:i];
                if([view isKindOfClass:[UIButton class]]){
                    [view removeFromSuperview];
                }
            }
        }
    }
    
    if((BTIsConnected == true) && ((theDataObject.APPConfig & BTStateConnected) == 0))
    {
        BTIsConnected = false;
        //Remove All beacon image
        for(int i = (int)self.view.subviews.count-1; i>=0; i--)
        {
            UIView *view = [self.view.subviews objectAtIndex:i];
            if([view isKindOfClass:[UIButton class]]){
                [view removeFromSuperview];
            }
        }
        [self DrawBeacon:OLD_proximity];
        NSLog(@"BT status changed!,disconnect");
    }
    else if((BTIsConnected == false) && ((theDataObject.APPConfig & BTStateConnected) == BTStateConnected))
    {
        BTIsConnected = true;
        //Remove All beacon image
        for(int i = (int)self.view.subviews.count-1; i>=0; i--)
        {
            UIView *view = [self.view.subviews objectAtIndex:i];
            if([view isKindOfClass:[UIButton class]]){
                [view removeFromSuperview];
            }
        }
        [self DrawBeacon:OLD_proximity];
        NSLog(@"BT status changed!,connect");
    }
    
    if((theDataObject.UUIDStr == nil) && ((theDataObject.APPConfig & BTStateConnected) == 0))
    {
        for(int i = (int)self.view.subviews.count-1; i>=0; i--)
        {
            UIView *view = [self.view.subviews objectAtIndex:i];
            if([view isKindOfClass:[UIButton class]]){
                [view removeFromSuperview];
            }
        }
        
        if(BTIsConnected)
            BTIsConnected = false;
        
        if(OLD_proximity != CLProximityUnknown)
            OLD_proximity = CLProximityUnknown;
    }
}

- (void)initBeaconIcons {
    UIButton *btn1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [btn1 setImage:[UIImage imageNamed:@"signal_green.png"] forState:UIControlStateNormal];
    UIButton *btn2 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [btn2 setImage:[UIImage imageNamed:@"signal_pink.png"] forState:UIControlStateNormal];
    UIButton *btn3 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [btn3 setImage:[UIImage imageNamed:@"signal_blue.png"] forState:UIControlStateNormal];
    
    _beaconIcons = [[NSMutableDictionary alloc] initWithObjectsAndKeys:btn1, ISSC_DEFAULT_BEACON_INFO1, btn2, ISSC_DEFAULT_BEACON_INFO2,btn3, ISSC_DEFAULT_BEACON_INFO3, nil];
}

- (void)DrawBeacon:(CLProximity)Proximity{
    float offsetX = 0;
    float offsetY = 0;
    const uint8_t MAX_BEACON = 8;
    const float radius = 160/MAX_BEACON;
    //float proximityOffset = (40 * (nearbyBeacon.proximity - 1));
    float proximityOffset = (40 * (Proximity - 1));
    UIButton *btn;
    
    BeaconDataObject* theDataObject = [self theAppDataObject];
    if((theDataObject.APPConfig & BTStateConnected) == BTStateConnected) {
        btn = [_beaconIcons objectForKey:ISSC_DEFAULT_BEACON_INFO3];
    }
    else
    {
        btn = [_beaconIcons objectForKey:ISSC_DEFAULT_BEACON_INFO1];
    }
    
    if(btn){
        [btn addTarget:self action:@selector(beaconProcess:) forControlEvents: UIControlEventTouchUpInside];
        for (NSInteger i = 0; i < MAX_BEACON; i++) {
            if ((_showBeaconFlag & (0x01 << i)) == 0x00) {
                _showBeaconFlag |= (0x01 << i);
                btn.tag = i;
                NSLog(@"btn tag = %d, flag = %d",(int)i,(int)_showBeaconFlag);
                break;
            }
        }
        
        if (btn.tag >= MAX_BEACON/2) {
            if (btn.tag >= 3*MAX_BEACON/4) {
                offsetX += (MAX_BEACON - btn.tag) * radius;
                offsetY -= (btn.tag - 3*MAX_BEACON/4) * radius;
                
                offsetX += proximityOffset ;
                if (btn.tag != 3*MAX_BEACON/4)
                    offsetY -= proximityOffset;
            }
            else {
                offsetX += (btn.tag - MAX_BEACON/2) * radius;
                offsetY += (3*MAX_BEACON/4 - btn.tag) * radius;
                if (btn.tag != MAX_BEACON/2)
                    offsetX += proximityOffset;
                offsetY += proximityOffset;
            }
        }
        else {
            if (btn.tag >= MAX_BEACON/4) {
                offsetX -= (MAX_BEACON/2 - btn.tag) * radius;
                offsetY += (btn.tag - MAX_BEACON/4) * radius;
                
                offsetX -= proximityOffset;
                if (btn.tag != MAX_BEACON/4)
                    offsetY += proximityOffset;;
            }
            else {
                offsetX -= (btn.tag) * radius;
                offsetY -= (MAX_BEACON/4 - btn.tag) * radius;
                if (btn.tag)
                    offsetX -= proximityOffset;
                offsetY -= proximityOffset;
            }
        }
        CGPoint center = btn.center;
        center.x = _iPhoneView.center.x + offsetX;
        center.y = _iPhoneView.center.y + offsetY;
        btn.center = center;
        
        if ([[self.view subviews] containsObject:btn] == FALSE) {
            [self.view insertSubview:btn belowSubview:_iPhoneView];
            NSLog(@"center = (%f,%f), btn.tag = %ld, offsetX=%f, offsetY=%f", btn.center.x, btn.center.y, btn.tag, offsetX, offsetY);
            NSLog(@"DrawBeacon");
        }
    }
}

- (void)beaconProcess:(id) sender {
    BeaconDataObject* theDataObject = [self theAppDataObject];
    NSLog(@"beaconProcess");
    
    NSRange Range = NSMakeRange(0, 36);
    //NSString *uuid = [theDataObject.UUIDStr substringWithRange:Range];
    NSString *uuid = [NSString stringWithFormat:@"UUID:%@",[theDataObject.UUIDStr substringWithRange:Range]];
    //NSLog(@"UUID:%@",uuid);
    Range = NSMakeRange(36, 4);
    //NSLog(@"Major:%@",[theDataObject.UUIDStr substringWithRange:Range]);
    NSString *Major = [NSString stringWithFormat:@", Major:%@",[theDataObject.UUIDStr substringWithRange:Range]];
    Range = NSMakeRange(40, 4);
    //NSLog(@"Minor:%@",[theDataObject.UUIDStr substringWithRange:Range]);
    NSString *Minor = [NSString stringWithFormat:@", Minor:%@",[theDataObject.UUIDStr substringWithRange:Range]];
    Range = NSMakeRange(44, 1);
    NSString *Pro = [theDataObject.UUIDStr substringWithRange:Range];
    NSInteger i = [Pro integerValue];
    
    uuid = [uuid stringByAppendingString:Major];
    uuid = [uuid stringByAppendingString:Minor];
    if(i == 1){
        uuid = [uuid stringByAppendingString:@", Proximity : Immediate"];
    }
    else if(i == 2){
        uuid = [uuid stringByAppendingString:@", Proximity : Near"];
    }
    else {
        uuid = [uuid stringByAppendingString:@", Proximity : Far"];
    }
    
    if((theDataObject.APPConfig & BTStateConnected) == BTStateConnected)
        uuid = [uuid stringByAppendingString:@", BT:Connected"];
    else
        uuid = [uuid stringByAppendingString:@", BT:Disconnected"];
    
    //UIAlertController *myAlert = [UIAlertController alertControllerWithTitle:@"Beacon data" message:theDataObject.UUIDStr preferredStyle:UIAlertControllerStyleAlert];
    UIAlertController *myAlert = [UIAlertController alertControllerWithTitle:@"Beacon data" message:uuid preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){[self dismissViewControllerAnimated:YES completion:nil];}];
    [myAlert addAction:okAction];
    [self presentViewController:myAlert animated:YES completion:nil];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
