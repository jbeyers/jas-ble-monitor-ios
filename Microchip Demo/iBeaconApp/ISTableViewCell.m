//
//  ISTableViewCell.m
//  Weight Scale
//
//  Created by Rick on 2014/10/7.
//  Copyright (c) 2014年 Rick. All rights reserved.
//

#import "ISTableViewCell.h"

@implementation ISTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _weight = [[UILabel alloc] initWithFrame:CGRectMake(5.0f, 10.0f, 110.0f, 60.0f)];
        //_weight.text = @"100.00 KG";
        _weight.font = [UIFont systemFontOfSize:22.0f];
        _weight.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_weight];
        _height = [[UILabel alloc] initWithFrame:CGRectMake(120.0f, 5.0f, 110.0f, 18.0f)];
        //_height.text = @"200.0 CM";
        _height.font = [UIFont systemFontOfSize:16.0f];
        [self.contentView addSubview:_height];
        _bmi = [[UILabel alloc] initWithFrame:CGRectMake(230.0f, 5.0f, 110.0f, 18.0f)];
        //_bmi.text = @"BMI:99.9";
        _bmi.font = [UIFont systemFontOfSize:16.0f];
        [self.contentView addSubview:_bmi];
        _user_id = [[UILabel alloc] initWithFrame:CGRectMake(120.0f, 30.0f, 110.0f, 18.0f)];
        //_user_id.text = @"User ID:1";
        _user_id.font = [UIFont systemFontOfSize:16.0f];
        [self.contentView addSubview:_user_id];
        _time = [[UILabel alloc] initWithFrame:CGRectMake(120.0f, 55.0f, 220.0f, 18.0f)];
        //_time.text = @"2014/10/10 12:12:12";
        _time.font = [UIFont systemFontOfSize:16.0f];
        [self.contentView addSubview:_time];

    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
