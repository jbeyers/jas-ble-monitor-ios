//
//  BTISWeightController.m
//  BeaconThings
//
//  Created by ILYAS on 16/02/16.
//  Copyright © 2016 Rick. All rights reserved.
//

#import "BTISWeightController.h"
#import "BTConnectViewController.h"
#import "BTCBController.h"
#import "ISTableViewController.h"
#import "BTISGraphViewController.h"
#import "BTISSettingController.h"

#import "AppDelegateProtocol.h"
#import "BeaconDataObject.h"


//#define buttonPressedColor  [UIColor colorWithRed:152.0/255.0 green:152.0/255.0 blue:0 alpha:1.0]
//#define buttonReleasedColor [UIColor colorWithRed:243.0/255.0 green:152.0/255.0 blue:0 alpha:1.0]
//#define buttonSideColor     [UIColor colorWithRed:170.0/255.0 green:105.0/255.0 blue:0 alpha:1.0]

#define buttonPressedColor  [UIColor colorWithRed:152.0/255.0 green:152.0/255.0 blue:0 alpha:1.0]
#define buttonReleasedColor [UIColor colorWithRed:166.0/255.0 green:166.0/255.0 blue:210.0/255.0 alpha:1.0]
#define buttonSideColor     [UIColor colorWithRed:90.0/255.0 green:90.0/255.0 blue:173.0/255.0 alpha:1.0]

@interface BTISWeightController () <CBControllerDelegate,MyPeripheralDelegate> {
    UILabel *unit;
    UILabel *weight;
    UILabel *battery_l;
    UILabel *time;
    BTConnectViewController *scan;
    //UIImageView *connect_icon;
    BTMyPeripheral *connect_peripheral;
    NSDateFormatter *formater;
    //UIButton *scan_but;
    //ISSettingController *settingView;
    bool DeviceIsConnect;
    
}

@end

@implementation BTISWeightController
/*
 - (void)loadView {
 
 if ([[UIScreen mainScreen] bounds].size.height > 480.0f) {
 [super loadView];
 }
 else {
 UIScrollView *view = [[UIScrollView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
 view.contentSize = CGSizeMake(320.0f, 568.0f);
 view.contentInset = UIEdgeInsetsMake(-64.0f, 0, 0, 0);
 self.view = view;
 }
 }
 */

- (BeaconDataObject *) theAppDataObject;
{
    id<AppDelegateProtocol> theDelegate = (id<AppDelegateProtocol>) [UIApplication sharedApplication].delegate;
    BeaconDataObject *theDataObject;
    theDataObject = (BeaconDataObject*) theDelegate.theAppDataObject;
    return theDataObject;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    //self.title = @"BM70_BeaconThings";
    NSInteger tabViewIndex = 0;
    
    BeaconDataObject* theDataObject = [self theAppDataObject];
    theDataObject.BeaconDatabase = [[NSMutableDictionary alloc] init];
    
    if((theDataObject.APPConfig & 0x38) == 0)
        theDataObject.APPConfig |= Proximity_Immediate;
    
    //[self.tabBarController.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_bg.png"] forBarMetrics:UIBarMetricsDefault];
    
    //self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"rsz_issc_logo.png"]];
    
    //self.navigationItem.title = @"BM70_BeaconThings";
    self.tabBarController.navigationItem.title = @"BT Disconnect";
    
    //self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"iPad_iSSC_background.png"]];
    
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"iPad_iSSC_background.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    
    /*UIButton *Logo = [UIButton buttonWithType:UIButtonTypeCustom];
    [Logo setFrame:CGRectMake(0, 0, 63, 40)];
    [Logo setImage:[UIImage imageNamed:@"rsz_microchip_logo.png"] forState:UIControlStateNormal];
    [Logo addTarget: self action:@selector(ShowAPPVersion) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc]initWithCustomView:Logo];
    self.tabBarController.navigationItem.leftBarButtonItem = barButton;*/
    
    DeviceIsConnect = false;
    
    //NSArray *actionButtonItems = @[AddItem, EditItem];
    //self.navigationItem.rightBarButtonItems = actionButtonItems;
    //self.navigationItem.leftBarButtonItem = DoneItem;
    
    /*
     UIImageView *connectImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"rsz_btconnect.png"]];
     UIImageView *disconnectImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"rsz_btdisconnect.png"]];
     CGRect frame = connectImageView.frame;
     frame.size.height = 20;
     frame.size.width = 20;
     connectImageView.frame = frame;
     frame = disconnectImageView.frame;
     frame.size.height = 20;
     frame.size.width = 20;
     disconnectImageView.frame = frame;
     UIButton *connectButton = [UIButton buttonWithType:UIButtonTypeCustom];
     [connectButton setBackgroundImage:disconnectImageView.image forState:UIControlStateNormal];
     connectButton.frame = CGRectMake(0.0f, 0.0f, 20.0f, 20.0f);
     [connectButton addTarget: self action:@selector(PressDisonnectButton:) forControlEvents:UIControlEventTouchUpInside];
     
     if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_5_1) {
     _disconnectBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:connectButton];
     }
     else {
     _disconnectBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:disconnectImageView];
     }
     
     self.navigationItem.rightBarButtonItem = _disconnectBarButtonItem;
     */
    
    if(tabViewIndex == 0) {
        scan = [[BTConnectViewController alloc] initWithNibName:nil bundle:nil];
        scan.delegate = self;
        //scan.beaconTriggerConnection = NO;
        //scan.beaconTriggerConnection = YES;
        scan.beaconTriggerConnection = [[NSUserDefaults standardUserDefaults] boolForKey:@"BM70_BeaconThings"];
        BeaconDataObject* theDataObject = [self theAppDataObject];
        if(scan.beaconTriggerConnection){
            theDataObject.APPConfig |= BeaconThingsOnOff;
        }
        else
        {
            theDataObject.APPConfig &= ~(BeaconThingsOnOff);
        }
        settingView = [[BTISSettingController alloc] initWithStyle:UITableViewStyleGrouped];
        //settingView.delegate = self;
        [settingView setCbController:scan];
    }
    
    self.Button1.faceColor = buttonReleasedColor;
    self.Button1.faceColorSelected = buttonPressedColor;
    self.Button1.sideColor = buttonSideColor;
    self.Button1.radius = 6.0;
    self.Button1.margin = 7.0;
    self.Button1.depth = 6.0;
    
    self.Button1.titleLabel.font = [UIFont boldSystemFontOfSize:16];
    [self.Button1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    self.Button2.faceColor = buttonReleasedColor;
    self.Button2.faceColorSelected = buttonPressedColor;
    self.Button2.sideColor = buttonSideColor;
    self.Button2.radius = 6.0;
    self.Button2.margin = 7.0;
    self.Button2.depth = 6.0;
    
    self.Button2.titleLabel.font = [UIFont boldSystemFontOfSize:16];
    [self.Button2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    //_LEDLabel.gradientColors = @[[UIColor colorWithRed:239/255.0f green:77/255.0f blue:182/255.0f alpha:1.0f], [UIColor colorWithRed:198/255.0f green:67/255.0f blue:252/255.0f alpha:1.0f]];
    _LEDLabel.gradientColors = @[[UIColor colorWithRed:197/255.0f green:68/255.0f blue:252/255.0f alpha:1.0f], [UIColor colorWithRed:88/255.0f green:86/255.0f blue:214/255.0f alpha:1.0f]];
    
    _LEDLabel.textAlignment = NSTextAlignmentCenter;
    _LEDLabel.textColor = [UIColor whiteColor];
    _LEDLabel.text = NSLocalizedString(@"LED Control", nil);
    _LEDLabel.font = [UIFont boldSystemFontOfSize:20.0f];
    
    //_ButtonLabel.gradientColors = @[[UIColor colorWithRed:239/255.0f green:77/255.0f blue:182/255.0f alpha:1.0f], [UIColor colorWithRed:198/255.0f green:67/255.0f blue:252/255.0f alpha:1.0f]];
    _ButtonLabel.gradientColors = @[[UIColor colorWithRed:197/255.0f green:68/255.0f blue:252/255.0f alpha:1.0f], [UIColor colorWithRed:88/255.0f green:86/255.0f blue:214/255.0f alpha:1.0f]];
    
    _ButtonLabel.textAlignment = NSTextAlignmentCenter;
    _ButtonLabel.textColor = [UIColor whiteColor];
    _ButtonLabel.text = NSLocalizedString(@"Button detect", nil);
    _ButtonLabel.font = [UIFont boldSystemFontOfSize:20.0f];
    
    _LEDSwitch1Status = YES;
    _LEDSwitch2Status = YES;
    
    //Keeping a UIButton pressed
    [self.LED1Button setImage:[UIImage imageNamed:@"led_off-icon.gif"] forState:UIControlStateNormal];
    [self.LED1Button setImage:[UIImage imageNamed:@"yellow_led-icon.gif"] forState:UIControlStateSelected];
    
    [self.LED2Button setImage:[UIImage imageNamed:@"led_off-icon.gif"] forState:UIControlStateNormal];
    [self.LED2Button setImage:[UIImage imageNamed:@"yellow_led-icon.gif"] forState:UIControlStateSelected];
    
    //[self.LED1Button setImage:[UIImage imageNamed:@"yellow_led.png"] forState:UIControlStateNormal];
    //[self.LED2Button setImage:[UIImage imageNamed:@"yellow_led.png"] forState:UIControlStateNormal];
    
    /*
     ZJSwitch *switch0 = nil;
     ZJSwitch *switch1 = nil;
     
     ZJSwitchStyle style = ZJSwitchStyleBorder;
     
     NSInteger i = 0;
     
     switch0 = [[ZJSwitch alloc] initWithFrame:CGRectMake(60-15, i * (10 + 31) + 205, 60, 31)];
     switch0.style = style;
     
     switch0.onTintColor = [UIColor grayColor];
     switch0.tintColor = [UIColor blueColor];
     switch0.thumbTintColor = [UIColor grayColor];
     switch0.onText = @"OFF";
     switch0.offText = @"ON";
     [switch0 addTarget:self action:@selector(handleSwitch1Event:) forControlEvents:UIControlEventValueChanged];
     [self.view addSubview:switch0];
     
     switch0.on = _LEDSwitch1Status;
     
     switch1 = [[ZJSwitch alloc] initWithFrame:CGRectMake(210-15, i * (10 + 31) + 205, 60, 31)];
     switch1.style = style;
     
     switch1.onTintColor = [UIColor grayColor];
     switch1.tintColor = [UIColor blueColor];
     switch1.thumbTintColor = [UIColor grayColor];
     switch1.onText = @"OFF";
     switch1.offText = @"ON";
     [switch1 addTarget:self action:@selector(handleSwitch2Event:) forControlEvents:UIControlEventValueChanged];
     [self.view addSubview:switch1];
     
     switch1.on = _LEDSwitch2Status;
     */
    
    //if(tabViewIndex != 0)
    //[self.tabBarController setSelectedIndex:1];
    
    /*
     self.title = @"Weight Scale";
     
     //self.wantsFullScreenLayout = YES;
     self.view.backgroundColor = [UIColor whiteColor];
     UIView *info_bg = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 350.0f)];
     info_bg.backgroundColor = [UIColor colorWithRed:0.15 green:0.74 blue:0.62 alpha:1.0];
     [self.view addSubview:info_bg];
     UIButton *history = [UIButton buttonWithType:UIButtonTypeCustom];
     history.frame = info_bg.bounds;
     [history addTarget:self action:@selector(showGraph:) forControlEvents:UIControlEventTouchUpInside];
     [info_bg addSubview:history];
     UIView *battery_bg = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 350.0f, 160.0f, 150.0f)];
     battery_bg.backgroundColor = [UIColor colorWithRed:0.95 green:0.62 blue:0.16 alpha:1.0];
     [self.view addSubview:battery_bg];
     UIView *time_bg = [[UIView alloc] initWithFrame:CGRectMake(160.0f, 350.0f, 160.0f, 150.0f)];
     time_bg.backgroundColor = [UIColor colorWithRed:0.90 green:0.30 blue:0.24 alpha:1.0];
     [self.view addSubview:time_bg];
     UIView *scan_bg = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 500.0f, 320.0f, 68.0f)];
     scan_bg.backgroundColor = [UIColor colorWithRed:0.18 green:0.50 blue:0.72 alpha:1.0];
     [self.view addSubview:scan_bg];
     unit = [[UILabel alloc] initWithFrame:CGRectMake(250.0f, 70.0f, 100.0f, 32.0f)];
     //unit.text = @"KG";
     unit.textColor = [UIColor whiteColor];
     unit.textAlignment = NSTextAlignmentCenter;
     [info_bg addSubview:unit];
     weight = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 160.0f, 320.0f, 100.0f)];
     //weight.text = @"0.0";
     weight.font = [UIFont boldSystemFontOfSize:90.0f];
     weight.textColor = [UIColor whiteColor];
     weight.textAlignment = NSTextAlignmentCenter;
     [info_bg addSubview:weight];
     battery_l = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 160.0f, 60.0f)];
     battery_l.center = CGPointMake(80.0f, 75.0f);
     //battery_l.text = @"100%";
     battery_l.font = [UIFont boldSystemFontOfSize:24.0f];
     battery_l.textColor = [UIColor whiteColor];
     battery_l.textAlignment = NSTextAlignmentCenter;
     [battery_bg addSubview:battery_l];
     UIImageView *battery_icon = [[UIImageView alloc] initWithFrame:CGRectMake(5.0f, 5.0f, 24.0f, 24.0f)];
     battery_icon.image = [UIImage imageNamed:@"battery.png"];
     [battery_bg addSubview:battery_icon];
     time = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 160.0f, 60.0f)];
     time.center = CGPointMake(80.0f, 75.0f);
     //time.text = @"2014/10/01\n18:00:00";
     time.font = [UIFont boldSystemFontOfSize:24.0f];
     time.textColor = [UIColor whiteColor];
     time.textAlignment = NSTextAlignmentCenter;
     time.numberOfLines = 2;
     time.lineBreakMode = NSLineBreakByWordWrapping;
     [time_bg addSubview:time];
     scan_but = [UIButton buttonWithType:UIButtonTypeCustom];
     [scan_but setTitle:@"Scan" forState:UIControlStateNormal];
     scan_but.titleLabel.font = [UIFont boldSystemFontOfSize:24.0f];
     scan_but.frame = CGRectMake(0.0f, 0.0f, 320.0f, 68.0f);
     scan_but.center = CGPointMake(160.0f, 34.0f);
     scan_but.tag = 0;
     [scan_bg addSubview:scan_but];
     [scan_but addTarget:self action:@selector(scan) forControlEvents:UIControlEventTouchUpInside];
     scan = [[ConnectViewController alloc] initWithNibName:nil bundle:nil];
     scan.delegate = self;
     connect_icon = [[UIImageView alloc] initWithFrame:CGRectMake(10.0f, 70.0f, 40.0f, 40.0f)];
     connect_icon.image = [UIImage imageNamed:@"ic_scale"];
     [info_bg addSubview:connect_icon];
     connect_icon.alpha = 0.5;
     formater = [[NSDateFormatter alloc] init];
     [formater setDateFormat:@"yyyy-MM-dd\nHH:mm:ss"];
     UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithTitle:@"Setting" style:UIBarButtonItemStyleBordered target:self action:@selector(showSetting:)];
     self.navigationItem.rightBarButtonItem = right;
     self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleBordered target:nil action:nil];
     [self resetText];
     settingView = [[ISSettingController alloc] initWithStyle:UITableViewStyleGrouped];
     [settingView setCbController:scan];
     */
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.tabBarController.navigationItem.title = @"BT Disconnect";
    
    UIBarButtonItem *SearchItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(LEConnectView)];
    self.tabBarController.navigationItem.rightBarButtonItem = SearchItem;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.tabBarController.navigationItem.title = @"Back";
}

- (void)dealloc {
    //id<AppDelegateProtocol> theDelegate = (id<AppDelegateProtocol>) [UIApplication sharedApplication].delegate;
    //theDelegate = nil;
    [self Manual_LE_Disconnect];
    //settingView.delegate = nil;
    [scan killConnection];
    //scan.delegate = nil;
    [scan release];
    //scan = nil;
    [settingView release];
    [super dealloc];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    /*    NSString *uuid_string = [[NSUserDefaults standardUserDefaults] objectForKey:@"selectedDevice"];
     if (uuid_string) {
     NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:uuid_string];
     dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
     //[scan connectDeviceWithIdentifier:uuid];
     });
     }*/
    
    /*
     UINavigationController *NavVC = (UINavigationController *)[self.tabBarController.viewControllers objectAtIndex:2];
     ISSettingController *myVC = (ISSettingController *)NavVC.topViewController;
     if([myVC.StrServiceUUID length] != 0 && myVC.StrServiceUUID != nil)
     NSLog(@"%@",myVC.StrServiceUUID);
     */
    
    //[self.tabBarController setSelectedIndex:1];
    
    //BeaconDataObject* theDataObject = [self theAppDataObject];
    //theDataObject.UUIDStr = @"Test 123abc";
    //NSLog(@"[ISWeightController] %@",theDataObject.UUIDStr);
    //theDataObject.UUIDStr = nil;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)ShowAPPVersion{
    
    NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    
    version = [@"Version : " stringByAppendingString:version];
    
    //NSString *build = [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString *)kCFBundleVersionKey];
    
    //version = [version stringByAppendingString:build];
    
    UIAlertController *ShowVersion = [UIAlertController alertControllerWithTitle:@"APP Info." message:version preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){[self dismissViewControllerAnimated:YES completion:nil];}];
    [ShowVersion addAction:okAction];
    [self presentViewController:ShowVersion animated:YES completion:nil];
}

- (void)Manual_LE_Disconnect{
    if(DeviceIsConnect)
    {
        [self BeaconThingsGPIOOutput:12 State:YES];
        [self BeaconThingsGPIOOutput:13 State:YES];
        NSLog(@"LE Disconnect...");
        [scan disconnectDevice:connect_peripheral];
        BeaconDataObject* theDataObject = [self theAppDataObject];
        theDataObject.APPConfig &= ~(BTStateConnected);
    }
}

/*
 - (void)UpdateBeaconData:(NSString *)BeaconData{
 NSLog(@"*** BeaconThings delegate , %@",BeaconData);
 self.TestString = [NSString stringWithFormat:@"%@",BeaconData];
 NSLog(@"<=====**** %@",self.TestString);
 }
 */
- (void)ringFindAccessoryTone{
    if(findAccessoryPlayer && [findAccessoryPlayer isPlaying])
    {
        [findAccessoryPlayer pause];
    }
    //If the track is not player, play the track and change the play button to "Pause"
    else
    {
        //if (findAccessoryPlayer)
        //    [findAccessoryPlayer release];
        UIApplicationState st = [[UIApplication sharedApplication] applicationState];
        NSLog(@"AppState=%ld",st);
        
        
        AVAudioSession *session = [AVAudioSession sharedInstance];
        
        NSError *sessionError;
        // [session setCategory:AVAudioSessionCategoryPlayAndRecord error:&sessionError];
        
        if(st == UIApplicationStateActive){
            [session setCategory:AVAudioSessionCategoryPlayback error:&sessionError];
            if(session == nil)
                NSLog(@"Error creating session: %@", [sessionError description]);
            else
                [session setActive:YES error:nil];
        }else if(st == UIApplicationStateBackground){
            if(![session setCategory:AVAudioSessionCategoryPlayback withOptions:AVAudioSessionCategoryOptionDuckOthers error:&sessionError])
            {
                NSLog(@"Unable to set audio session category");
            }
            BOOL result = [session setActive:YES error:&sessionError];
            if(!result){
                NSLog(@"Error activating audio session");
            }
            
            BeaconDataObject* theDataObject = [self theAppDataObject];
            if((theDataObject.APPConfig & BTStateConnected) == BTStateConnected){
                theDataObject.APPConfig |= APPWakeUpConnect;
            }
        }
        
        NSError *playerError;
        
        NSString *path;
        NSURL *soundUrl;
        
        path = [NSString stringWithFormat:@"%@/Alarm1.mp3", [[NSBundle mainBundle] resourcePath]];
        
        soundUrl = [NSURL fileURLWithPath:path];
        findAccessoryPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:&playerError];
        findAccessoryPlayer.delegate = self;
        
        if (findAccessoryPlayer == nil)
        {
            NSLog(@"Error creating player: %@", [playerError description]);
            return;
        }
        //findAccessoryPlayer.delegate = self;
        //NSLog(@"Ring tone");
        if([findAccessoryPlayer prepareToPlay]){
            NSLog(@"***** Ring tone");
            [findAccessoryPlayer play];
        }
    }
    
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    if(flag)
    {
        BeaconDataObject* theDataObject = [self theAppDataObject];
        if((theDataObject.APPConfig & APPWakeUpConnect) == APPWakeUpConnect)
        {
            theDataObject.APPConfig &= ~(APPWakeUpConnect);
            
            AVAudioSession *session = [AVAudioSession sharedInstance];
            NSError *error = nil;
            BOOL result = [session setActive:NO error:&error];
            if(!result)
                NSLog(@"Error deactivating audio session");
        }
        NSLog(@"audioPlayerDidFinishPlaying");
    }
}

- (void)handleSwitch1Event:(id)sender
{
    UISwitch *tmp = (UISwitch *)sender;
    //NSLog(@"Switch 1 handler,%d",tmp.on);
    
    _LEDSwitch1Status = tmp.on;
    /*
     if(_LEDSwitch1Status)
     [self.LED1Image setImage:[UIImage imageNamed:@"ledblack.png"]];
     else
     [self.LED1Image setImage:[UIImage imageNamed:@"ledblue.png"]];
     */
    [self BeaconThingsGPIOOutput:12 State:tmp.on];
    
    
}

- (IBAction)LED1_Event_TouchDown:(id)sender {
    //NSLog(@"LED1_Event_TouchDown");
    
    //[self BeaconThingsGPIOOutput:12 State:NO];
    UIButton *btn = (UIButton *)sender;
    btn.selected = !btn.selected;
    
    //NSLog(@"Button = %d",btn.selected);
    
    if(btn.selected)
        [self BeaconThingsGPIOOutput:12 State:NO];
    else
        [self BeaconThingsGPIOOutput:12 State:YES];
    
}

- (IBAction)LED1_Event_TouchUpInside:(id)sender {
    //NSLog(@"LED1_Event_TouchUpInside");
    
    //[self BeaconThingsGPIOOutput:12 State:YES];
    
}

- (IBAction)LED2_Event_TouchDown:(id)sender {
    //[self BeaconThingsGPIOOutput:13 State:NO];
    
    UIButton *btn = (UIButton *)sender;
    btn.selected = !btn.selected;
    
    if(btn.selected)
        [self BeaconThingsGPIOOutput:13 State:NO];
    else
        [self BeaconThingsGPIOOutput:13 State:YES];
}

- (IBAction)LED2_Event_TouchUpInside:(id)sender {
    
    //[self BeaconThingsGPIOOutput:13 State:YES];
    
}

- (void)handleSwitch2Event:(id)sender
{
    UISwitch *tmp = (UISwitch *)sender;
    //NSLog(@"Switch 2 handler,%d",tmp.on);
    
    _LEDSwitch2Status = tmp.on;
    /*
     if(_LEDSwitch2Status)
     [self.LED2Image setImage:[UIImage imageNamed:@"ledblack.png"]];
     else
     [self.LED2Image setImage:[UIImage imageNamed:@"ledblue.png"]];
     */
    [self BeaconThingsGPIOOutput:13 State:tmp.on];
    
    
    
}

-(void)LEConnectView {
    [self.tabBarController.navigationController pushViewController:scan animated:YES];
}

- (IBAction)LEScan:(id)sender {
    
    //[self.navigationController pushViewController:scan animated:YES];
    
    /*
     UIImageView *connectImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"rsz_btconnect.png"]];
     UIButton *connectButton = [UIButton buttonWithType:UIButtonTypeCustom];
     [connectButton setBackgroundImage:connectImageView.image forState:UIControlStateNormal];
     connectButton.frame = CGRectMake(0.0f, 0.0f, 20.0f, 20.0f);
     [connectButton addTarget: self action:@selector(PressConnectButton:) forControlEvents:UIControlEventTouchUpInside];
     _connectBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:connectButton];
     self.navigationItem.rightBarButtonItem = _connectBarButtonItem;
     */
}

- (void)PressDisonnectButton:(id)sender {
    //NSLog(@"LE Scan..");
    
    [self.navigationController pushViewController:scan animated:YES];
}

- (void)PressConnectButton:(id)sender{
    NSLog(@"PressConnectButton");
    
    //[_cbController disconnectDevice:controlPeripheral];
}

/*
 - (void)scan {
 if ([[scan_but titleForState:UIControlStateNormal] isEqualToString:@"Scan"]) {
 [self.navigationController pushViewController:scan animated:YES];
 }
 if (connect_peripheral) {
 [scan disconnectDevice:connect_peripheral];
 connect_peripheral = nil;
 connect_icon.alpha = 0.5;
 [scan_but setTitle:@"Scan" forState:UIControlStateNormal];
 }
 }
 */
- (void)showHistory {
    ISTableViewController *hsitory = [[ISTableViewController alloc] initWithStyle:UITableViewStylePlain];
    [self.navigationController pushViewController:hsitory animated:YES];
}

- (void)showGraph:(id)sender {
    BTISGraphViewController *view = [[BTISGraphViewController alloc] init];
    [self.navigationController pushViewController:view animated:YES];
}

- (void)showSetting:(id)sender {
    [self.navigationController pushViewController:settingView animated:YES];
}

- (void)resetText {
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"Unit"] == 0) {
        unit.text = @"kg";
    }
    else {
        unit.text = @"lb";
    }
    weight.text = @"0.0";
    time.text = @"";
    battery_l.text = @"";
    
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (void)didUpdatePeripheralList:(NSArray *)peripherals {
    
}

- (void)didConnectPeripheral:(BTMyPeripheral *)peripheral {
    NSLog(@"***didConnectPeripheral,%@",peripheral.advName);
    if (peripheral.connectStaus == BT_MYPERIPHERAL_CONNECT_STATUS_CONNECTED) {
        //connect_icon.alpha = 1.0f;
        //[scan_but setTitle:@"Disconnect" forState:UIControlStateNormal];
    }
    else {
        //[scan_but setTitle:@"Scan" forState:UIControlStateNormal];
        //connect_icon.alpha = 0.5f;
    }
    /*
     connect_peripheral = peripheral;
     peripheral.transDataDelegate = self;
     [self resetText];
     */
    
    connect_peripheral = peripheral;
    
    DeviceIsConnect = true;
    NSString *str = @" : ";
    NSString *str1 = [str stringByAppendingString:peripheral.advName];
    NSLog(@"%@",str1);
    
    self.navigationItem.title = [peripheral.advName stringByAppendingString:@" : Connect"];
    
    BeaconDataObject* theDataObject = [self theAppDataObject];
    if((theDataObject.APPConfig & BeaconThingsOnOff) == 0){
        self.navigationItem.leftBarButtonItem = nil;
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setFrame:CGRectMake(0, 0, 60, 40)];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(3, 5, 55, 30)];
        [label setFont:[UIFont systemFontOfSize:10]];
        [label setText:@"Disconnect"];
        //label.textAlignment = UITextAlignmentCenter;
        [label setTextColor:[UIColor blackColor]];
        [button addSubview:label];
        [button addTarget: self action:@selector(Manual_LE_Disconnect) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *barButton = [[UIBarButtonItem alloc]initWithCustomView:button];
        self.navigationItem.leftBarButtonItem = barButton;
    }
    
    theDataObject.APPConfig |= BTStateConnected;
}

- (void)didDisconnectPeripheral:(BTMyPeripheral *)peripheral {
    NSLog(@"***didDisonnectPeripheral");
    //[scan_but setTitle:@"Scan" forState:UIControlStateNormal];
    //connect_icon.alpha = 0.5f;
    peripheral.transDataDelegate = nil;
    
    //self.navigationItem.title = @"Peripheral Control:Disconnect";
    //self.navigationItem.title = @"Peripheral Disconnect";
    self.navigationItem.title = @"BT Disconnect";
    
    DeviceIsConnect = false;
    
    BeaconDataObject* theDataObject = [self theAppDataObject];
    theDataObject.APPConfig &= ~(BTStateConnected);
    
    self.navigationItem.leftBarButtonItem = nil;
    
    UIButton *Logo = [UIButton buttonWithType:UIButtonTypeCustom];
    [Logo setFrame:CGRectMake(0, 0, 63, 40)];
    [Logo setImage:[UIImage imageNamed:@"rsz_microchip_logo.png"] forState:UIControlStateNormal];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc]initWithCustomView:Logo];
    self.navigationItem.leftBarButtonItem = barButton;
    
    [self.Button2 setSelected:NO];
    [self.Button1 setSelected:NO];
    
    [self.LED1Button setSelected:NO];
    [self.LED2Button setSelected:NO];
}

- (void)MyPeripheral:(BTMyPeripheral *)peripheral didUpdateWeihgtScaleData:(NSDictionary *)data error:(NSError *)error {
    unit.text = data[@"unit_weight"];
    weight.text = [NSString stringWithFormat:@"%.2f",[data[@"weight"] floatValue]];
    time.text = [formater stringFromDate:data[@"time"]];
    
    UILocalNotification *localNotification = [[UILocalNotification alloc] init]; //Create the localNotification object
    
    [localNotification setAlertAction:@"Weight Scale APP WakeUp"]; //The button's text that launches the application and is shown in the alert
    [localNotification setAlertBody:[NSString stringWithFormat:@"%@ %@", weight.text, unit.text]]; //Set the message in the notification from the textField's text
    [localNotification setHasAction: YES]; //Set that pushing the button will launch the application
    [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
}

- (void)MyPeripheral:(BTMyPeripheral *)peripheral didUpdateBatteryLevel:(NSInteger)level error:(NSError *)error {
    battery_l.text = [NSString stringWithFormat:@"%ld %%",(long)level];
}

#ifdef Test_DualSPK

- (void)ISSC_SPCP_Ready {
    DeviceIsConnect = true;
    BeaconDataObject* theDataObject = [self theAppDataObject];
    
    NSInteger connectAction = [[NSUserDefaults standardUserDefaults] integerForKey:@"ConnectIndication"];
    
    [self BeaconThingsGPIOInputPolling];
    
    if((connectAction & 0x01) == 0x01)
        theDataObject.APPConfig |= RingAlarm;
    else
        theDataObject.APPConfig &= ~(RingAlarm);
    
    if((connectAction & 0x02) == 0x02)
        theDataObject.APPConfig |= Vibration;
    else
        theDataObject.APPConfig &= ~(Vibration);
    
    /*
     if((connectAction & 0x01) == 0x01)
     [self ringFindAccessoryTone];
     else if((connectAction & 0x02) == 0x02)
     AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
     */
    
    
    NSLog(@"[Alarm/Vibration] APPConfig = %x,%ld",(int)connectAction,theDataObject.APPConfig);
    
    if((theDataObject.APPConfig & RingAlarm) == RingAlarm)
    {
        [self ringFindAccessoryTone];
        //NSLog(@"ring Tone");
    }
    
    if((theDataObject.APPConfig & Vibration) == Vibration)
    {
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
        //NSLog(@"Vibration");
    }
}

- (void)CBDataRxReceived:(NSData *)data
{
    /*
     if(data && ([data length]))
     {
     if(([data length]) <= 15)
     {
     NSLog(@"delegate : CBReceivedData");
     unsigned char buf[15];
     int len = [data length];
     memset(buf, 0x00, sizeof(buf));
     [data getBytes:buf length:len];
     
     for(int i = 0 ; i < 15; i++)
     NSLog(@"%x",buf[i]);
     
     //NSLog(@"Delegate CBRxReceive,len =%d,Rec= %x,%x,%x,%x,%x,%x,%x,%x,%x,%x",len,buf[0],buf[1],buf[2],buf[3],buf[4],buf[5],buf[6],buf[7],buf[8],buf[9]);
     }
     
     }
     */
    
    if(data && ([data length])){
        unsigned char buf[15];
        int len = [data length];
        memset(buf, 0x00, sizeof(buf));
        [data getBytes:buf length:len];
        
        //Button : P20 , P31
        if(buf[0] == 0x00) {    //Status
            switch(buf[1]) {    //Command
                case 0x03:
                    NSLog(@"ACK , op code = 0x03");
                    //[self BeaconThingsGPIOInputEnable];
                    //if(buf[4] == 0x11)
                    //{
                    //    NSLog(@"Initial..");
                    //[self BeaconThingsGPIOInputEnable];
                    //}
                    
                    /*
                     if(buf[4] == 0x91 && buf[5] == 0x4e) {
                     [self.Button2 setSelected:NO];
                     [self.Button1 setSelected:NO];
                     }
                     else if(buf[4] == 0x90 && buf[5] == 0x4e) {
                     [self.Button1 setSelected:YES];
                     //[self.Button2 setSelected:YES];
                     
                     //[self ringFindAccessoryTone];
                     }
                     else if(buf[4] == 0x91 && buf[5] == 0x4c) {
                     [self.Button2 setSelected:YES];
                     
                     //[self ringFindAccessoryTone];
                     }*/
                    
                    if((buf[4] & 0x01) == 0)//Check P2.0
                        [self.Button2 setSelected:YES];
                    else
                        [self.Button2 setSelected:NO];
                    
                    if((buf[5] & 0x02) == 0)//Check P3.1
                        [self.Button1 setSelected:YES];
                    else
                        [self.Button1 setSelected:NO];
                    
                    break;
                case 0x04:
                    NSLog(@"ACK , op code = 0x04");
                    break;
                case 0x05:
                    NSLog(@"ACK , op code = 0x05");
                    break;
            }
            //NSLog(@"Response data = %x,%x,%x,%x",buf[2],buf[3],buf[4],buf[5]);
            NSLog(@"Response P2,P3 data = %x,%x",buf[4],buf[5]);
        }
        else{
            NSLog(@"Error , Op code = %x",buf[1]);
        }
    }
    
}

- (void)SendGPIOCommand {
    
    if(!DeviceIsConnect)
        return;
    
    uint8_t buf[25];
    memset(buf, 0x00, sizeof(buf));
    
    buf[0] = 0xaa; buf[1] = 0; buf[2] = 0x0d; buf[3] = 0x1e;
    buf[4] = 0xcf; buf[5] = 0xff; buf[6] = 0x6f; buf[7] = 0xff;
    buf[8] = 0x10; buf[9] = 0; buf[10] = 0x10; buf[11] = 0;
    buf[12] = 0x10; buf[13] = 0; buf[14] = 0x10; buf[15] = 0; buf[16] = 0x59; buf[17] = 0;
    
    NSMutableData *data = [[NSMutableData alloc] initWithBytes:buf length:17];
    
    [scan LE_SendCommand:connect_peripheral MyData:data];
}

- (void)BeaconThingsGPIOInputPolling{
    
    if(!DeviceIsConnect)
        return;
    
    NSLog(@"BeaconThingsGPIOInputPolling");
    //Button P20 , P31
    uint8_t buf[5];
    buf[0] = 0x03;  //Command ID
    buf[1] = 0;     //P0 polling enable
    buf[2] = 0;     //P1
    buf[3] = 0x01;  //P2
    buf[4] = 0x02;  //P3
    NSMutableData *data = [[NSMutableData alloc] initWithBytes:buf length:5];
    [scan LE_SendCommand:connect_peripheral MyData:data];
}

- (void)BeaconThingsGPIOInputEnable{
    
    if(!DeviceIsConnect)
        return;
    
    NSLog(@"BeaconThingsGPIOInputEnable");
    uint8_t buf[5];
    buf[0] = 0x04;  //Command ID
    buf[1] = 0;     //P0 input enable
    buf[2] = 0;     //P1
    buf[3] = 0x01;  //P2
    buf[4] = 0x02;  //P3
    NSMutableData *data = [[NSMutableData alloc] initWithBytes:buf length:5];
    [scan LE_SendCommand:connect_peripheral MyData:data];
}

- (void)BeaconThingsGPIOOutput:(int)IONumber State:(bool)iostate{
    
    if(!DeviceIsConnect)
        return;
    
    NSLog(@"BeaconThingsGPIOOutput,P%d = %d",IONumber,iostate);
    /*
     //LED P12 , P13
     uint8_t buf[9];
     buf[0] = 0x05;  //Command ID
     buf[1] = 0;     //P0 output enable
     if(IONumber == 12)  //P1.2
     buf[2] = 0x04;
     else if(IONumber == 13)  //P1.3
     buf[2] = 0x08;
     buf[3] = 0;     //P2
     buf[4] = 0;     //P3
     buf[5] = 0xff;  //P0 output value
     */
    //LED P32 , P33
    uint8_t buf[9];
    buf[0] = 0x05;  //Command ID
    buf[1] = 0;     //P0 output enable
    buf[2] = 0;
    buf[3] = 0;     //P2
    if(IONumber == 12)     //P32
        buf[4] = 0x04;
    else                    //P33
        buf[4] = 0x08;
    buf[5] = 0xff;  //P0 output value
    buf[6] = 0xff;
    /*
     if(IONumber == 12)  //P1.2
     {
     if(iostate == false)
     buf[6] = 0xfB;
     else
     buf[6] = 0xff;
     }
     else if(IONumber == 13) {
     if(iostate == false)
     buf[6] = 0xf7;
     else
     buf[6] = 0xff;
     }
     */
    buf[7] = 0xff;
    if(IONumber == 12)
    {
        if(iostate == false)
            buf[8] = 0xfB;
        else
            buf[8] = 0xff;
        //buf[8] = 0xff;
    }
    else{
        if(iostate == false)
            buf[8] = 0xf7;
        else
            buf[8] = 0xff;
        //buf[8] =
    }
    NSMutableData *data = [[NSMutableData alloc] initWithBytes:buf length:9];
    [scan LE_SendCommand:connect_peripheral MyData:data];
}


- (IBAction)TestTest:(id)sender {
    
    if(!DeviceIsConnect)
        return;
    
    NSLog(@"TestTest!");
    [self SendGPIOCommand];
}

#endif
@end
