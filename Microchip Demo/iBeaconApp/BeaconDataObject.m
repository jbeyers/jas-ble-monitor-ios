//
//  ISBeaconViewController.h
//  BeaconThings
//
//  Created by Rick on 2015/11/27.
//  Copyright © 2015年 Rick. All rights reserved.
//

#import "BeaconDataObject.h"

@implementation BeaconDataObject
@synthesize UUIDStr,APPConfig,BeaconDatabase,New_Proximity;

- (void)dealloc
{
    UUIDStr = nil;
    APPConfig = 0;
    BeaconDatabase = nil;
    New_Proximity = CLProximityUnknown;
    [super dealloc];
}

@end
