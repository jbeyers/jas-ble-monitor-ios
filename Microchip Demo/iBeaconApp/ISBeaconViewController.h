//
//  ISBeaconViewController.h
//  BeaconThings
//
//  Created by Rick on 2015/11/27.
//  Copyright © 2015年 Rick. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <CoreLocation/CoreLocation.h>
#import "PulsingHaloLayer.h"
#import "BTISSettingController.h"

@interface ISBeaconViewController : UIViewController <BeaconThingsDelegate>
{
    UIImageView *_iPhoneView;
    NSInteger _showBeaconFlag;
    NSMutableDictionary *_beaconIcons;
    
#if (RSSI_TEST == 1)
    UIImageView *_rssiTrackerView;
    int _isscRSSIMonitorCount;
    int _estimoteRSSIMonitorCount;
#endif

}
@property (nonatomic, strong) PulsingHaloLayer *halo;
@property (retain) CLBeacon *reportingBeacon;
@end
