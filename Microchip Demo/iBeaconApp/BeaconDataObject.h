//
//  ISBeaconViewController.h
//  BeaconThings
//
//  Created by Rick on 2015/11/27.
//  Copyright © 2015年 Rick. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "AppDataObject.h"

#define BeaconThingsOnOff       0x01
#define RingAlarm               0x02
#define Vibration               0x04
#define Proximity_Immediate     0x08
#define Proximity_Near          0x10
#define Proximity_Far           0x20
#define BTStateConnected        0x40
#define APPWakeUpConnect        0x80
#define CheckBeaconRanging      0x100
#define ResetBeaconDetection    0x200
#define BeaconStart             0x400

@interface BeaconDataObject : AppDataObject
{
    NSString    *UUIDStr;   //UUID+Major+Minor
    NSMutableDictionary *BeaconDatabase;
    NSInteger   APPConfig;
    CLProximity New_Proximity;
    
}
@property (nonatomic , copy) NSString *UUIDStr;
@property (nonatomic) NSInteger APPConfig;
@property (nonatomic , retain) NSMutableDictionary *BeaconDatabase;
@property (nonatomic) CLProximity New_Proximity;
@end
