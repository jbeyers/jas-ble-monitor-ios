//
//  BTCBController.h
//  BeaconThings
//
//  Created by ILYAS on 16/02/16.
//  Copyright © 2016 Rick. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "UUID.h"
#import "BTMyPeripheral.h"

#define Test_DualSPK

enum {
    BT_LE_STATUS_IDLE = 0,
    BT_LE_STATUS_SCANNING,
    BT_LE_STATUS_CONNECTING,
    BT_LE_STATUS_CONNECTED
};

@protocol CBControllerDelegate;
@interface BTCBController : UIViewController<CBCentralManagerDelegate, CBPeripheralDelegate>
{
    CBCentralManager *manager;
    NSMutableArray *devicesList;
    BOOL    notifyState;
    NSMutableArray *_connectedPeripheralList;
    CBUUID *_transServiceUUID;
    CBUUID *_transTxUUID;
    CBUUID *_transRxUUID;
    CBUUID *_disUUID1;
    CBUUID *_disUUID2;
    BOOL    isISSCPeripheral;
    BOOL    _waitingBtPowerOnToScan;
}

@property(assign) id<CBControllerDelegate> delegate;
@property (retain) NSMutableArray *devicesList;
- (void) killConnection;
- (void) startScanWithUUID:(NSArray *)uuids;
- (void) startScan;
- (void) stopScan;
- (void)connectDevice:(BTMyPeripheral *) myPeripheral;
- (void)connectDeviceWithIdentifier:(NSUUID *)identifier;
- (void)disconnectDevice:(BTMyPeripheral *) aPeripheral;
- (NSMutableData *) hexStrToData: (NSString *)hexStr;
- (BOOL) isLECapableHardware;
- (void)addDiscoverPeripheral:(CBPeripheral *)aPeripheral advData:(NSDictionary *)advData;
- (void)updateDiscoverPeripherals;
- (void)updateMyPeripheralForDisconnect:(BTMyPeripheral *)myPeripheral;
- (void)updateMyPeripheralForNewConnected:(BTMyPeripheral *)myPeripheral;
- (void)storeMyPeripheral: (CBPeripheral *)aPeripheral;
- (BTMyPeripheral *)retrieveMyPeripheral: (CBPeripheral *)aPeripheral;
- (void)removeMyPeripheral: (CBPeripheral *) aPeripheral;
- (void)configureTransparentServiceUUID: (NSString *)serviceUUID txUUID:(NSString *)txUUID rxUUID:(NSString *)rxUUID;
- (void)configureDeviceInformationServiceUUID: (NSString *)UUID1 UUID2:(NSString *)UUID2;
#ifdef Test_DualSPK
- (void)LE_SendCommand:(BTMyPeripheral *)aPeripheral MyData:(NSData *)data;
#endif
@end

@protocol CBControllerDelegate<NSObject>
@required
- (void)didUpdatePeripheralList:(NSArray *)peripherals;
- (void)didConnectPeripheral:(BTMyPeripheral *)peripheral;
- (void)didDisconnectPeripheral:(BTMyPeripheral *)peripheral;
#ifdef Test_DualSPK
- (void)CBDataRxReceived:(NSData *)data;
- (void)ISSC_SPCP_Ready;
#endif
@end
