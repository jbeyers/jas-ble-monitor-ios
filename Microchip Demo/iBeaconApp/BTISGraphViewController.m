//
//  BTISGraphViewController.m
//  BeaconThings
//
//  Created by ILYAS on 16/02/16.
//  Copyright © 2016 Rick. All rights reserved.
//

#import "BTISGraphViewController.h"
#import "BEMSimpleLineGraphView.h"
#import "AppDelegate.h"
#import "WeightData.h"
#import "ISTableViewController.h"


@interface BTISGraphViewController () <BEMSimpleLineGraphDataSource, BEMSimpleLineGraphDelegate,UIScrollViewDelegate> {
    NSMutableArray *data;
    NSDateFormatter *formater;
    UILabel *weight;
    UILabel *bmi;
    
}

@end

@implementation BTISGraphViewController

- (void)loadView {
    if ([[UIScreen mainScreen] bounds].size.height > 480.0f) {
        [super loadView];
    }
    else {
        UIScrollView *view = [[UIScrollView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        view.contentSize = CGSizeMake(320.0f, 568.0f);
        view.contentInset = UIEdgeInsetsMake(-64.0f, 0, 0, 0);
        self.view = view;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"History";
    self.view.backgroundColor = [UIColor whiteColor];
    data = [[NSMutableArray alloc] init];
    formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:@"yyyy/MM/dd"];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"WeightData" inManagedObjectContext:appDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    NSSortDescriptor* sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"timestamp" ascending:NO];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    NSArray *temp = [appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:nil];
    [data addObjectsFromArray:temp];
    self.automaticallyAdjustsScrollViewInsets = NO;
    UIScrollView *graphScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 60, 320, 200)];
    graphScroll.tag = 1;
    graphScroll.delegate = self;
    [self.view addSubview:graphScroll];
    CGFloat width = 32+50*data.count;
    if (width < 320.0f) width = 320.0f;
    graphScroll.contentSize = CGSizeMake(width+5, 200);
    BEMSimpleLineGraphView *myGraph = [[BEMSimpleLineGraphView alloc] initWithFrame:CGRectMake(0, 0, width, 200)];
    myGraph.delegate = self;
    myGraph.dataSource = self;
    myGraph.colorTop = [UIColor colorWithRed:31.0/255.0 green:187.0/255.0 blue:166.0/255.0 alpha:1.0];
    myGraph.colorBottom = [UIColor colorWithRed:31.0/255.0 green:187.0/255.0 blue:166.0/255.0 alpha:1.0];
    myGraph.colorLine = [UIColor whiteColor];
    myGraph.colorXaxisLabel = [UIColor whiteColor];
    myGraph.colorYaxisLabel = [UIColor whiteColor];
    myGraph.widthLine = 3.0;
    myGraph.enableTouchReport = YES;
    myGraph.enablePopUpReport = YES;
    //myGraph.enableBezierCurve = YES;
    myGraph.enableYAxisLabel = YES;
    myGraph.autoScaleYAxis = YES;
    myGraph.alwaysDisplayDots = NO;
    myGraph.enableReferenceAxisLines = YES;
    myGraph.enableReferenceAxisFrame = YES;
    myGraph.animationGraphStyle = BEMLineAnimationNone;
    myGraph.tag = 1;
    myGraph.userInteractionEnabled = YES;
    [graphScroll addSubview:myGraph];
    
    UILabel *info = [[UILabel alloc] initWithFrame:CGRectMake(5.0f, 270.0f, 50.0f, 20.0f)];
    info.textColor = [UIColor colorWithRed:0.172 green:0.478 blue:0.733 alpha:1.000];
    info.text = @"BMI";
    [self.view addSubview:info];
    
    graphScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 290, 320, 200)];
    graphScroll.tag = 2;
    graphScroll.delegate = self;
    [self.view addSubview:graphScroll];
    graphScroll.contentSize = CGSizeMake(width+5, 200);
    myGraph = [[BEMSimpleLineGraphView alloc] initWithFrame:CGRectMake(0, 0, width, 200)];
    myGraph.delegate = self;
    myGraph.dataSource = self;
    myGraph.colorTop = [UIColor colorWithRed:0.172 green:0.478 blue:0.733 alpha:1.000];
    myGraph.colorBottom = [UIColor colorWithRed:0.172 green:0.478 blue:0.733 alpha:1.000];
    myGraph.colorLine = [UIColor whiteColor];
    myGraph.colorXaxisLabel = [UIColor whiteColor];
    myGraph.colorYaxisLabel = [UIColor whiteColor];
    myGraph.widthLine = 3.0;
    myGraph.enableTouchReport = YES;
    myGraph.enablePopUpReport = YES;
    //myGraph.enableBezierCurve = YES;
    myGraph.enableYAxisLabel = YES;
    myGraph.autoScaleYAxis = YES;
    myGraph.alwaysDisplayDots = NO;
    myGraph.enableReferenceAxisLines = YES;
    myGraph.enableReferenceAxisFrame = YES;
    myGraph.animationGraphStyle = BEMLineAnimationNone;
    myGraph.tag = 2;
    myGraph.userInteractionEnabled = YES;
    [graphScroll addSubview:myGraph];
    UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithTitle:@"Detail" style:UIBarButtonItemStyleBordered target:self action:@selector(showTable:)];
    self.navigationItem.rightBarButtonItem = right;
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleBordered target:nil action:nil];
    
    weight = [[UILabel alloc] initWithFrame:CGRectMake(10.0f, 500.0f, 140.0f, 50.0f)];
    weight.font = [UIFont systemFontOfSize:20];
    [self.view addSubview:weight];
    
    bmi = [[UILabel alloc] initWithFrame:CGRectMake(160.0f, 500.0f, 140.0f, 50.0f)];
    bmi.font = [UIFont systemFontOfSize:20];
    [self.view addSubview:bmi];
    
}

- (void)showTable:(id)sender {
    ISTableViewController *hsitory = [[ISTableViewController alloc] initWithStyle:UITableViewStylePlain];
    [self.navigationController pushViewController:hsitory animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    UIScrollView *other;
    if (scrollView.tag == 1) {
        other = (UIScrollView *)[self.view viewWithTag:2];
    }
    else {
        other = (UIScrollView *)[self.view viewWithTag:1];
    }
    [other setContentOffset:scrollView.contentOffset];
}
#pragma mark - SimpleLineGraph Data Source

- (NSInteger)numberOfPointsInLineGraph:(BEMSimpleLineGraphView *)graph {
    return data.count?data.count:0;
}

- (CGFloat)lineGraph:(BEMSimpleLineGraphView *)graph valueForPointAtIndex:(NSInteger)index {
    WeightData *dis = [data objectAtIndex:index];
    if (graph.tag == 2) {
        return [dis.bmi floatValue];
    }
    switch ([[NSUserDefaults standardUserDefaults] integerForKey:@"Unit"]) {
        case 0:{
            if (![dis.weightUnit isEqualToString:@"kg"]) {
                return [dis.weight floatValue]*0.45359237;
            }
            return [dis.weight floatValue];
        }
            break;
        case 1:{
            if (![dis.weightUnit isEqualToString:@"lb"]) {
                return [dis.weight floatValue]*2.20462262185;
            }
            return [dis.weight floatValue];
        }
            break;
        default:
            break;
    }
    return 0;
}

#pragma mark - SimpleLineGraph Delegate
- (NSString *)popUpSuffixForlineGraph:(BEMSimpleLineGraphView *)graph {
    switch ([[NSUserDefaults standardUserDefaults] integerForKey:@"Unit"]) {
        case 0:
            if (graph.tag == 1) {
                return @"kg";
            }
            else {
                return @"";
            }
            break;
        case 1:
            if (graph.tag == 1) {
                return @"lb";
            }
            else {
                return @"";
            }
            break;
        default:
            break;
    }
    return @"kg";
}

- (NSInteger)numberOfGapsBetweenLabelsOnLineGraph:(BEMSimpleLineGraphView *)graph {
    return 1;
}

- (NSString *)lineGraph:(BEMSimpleLineGraphView *)graph labelOnXAxisForIndex:(NSInteger)index {
    WeightData *dis = [data objectAtIndex:index];
    return [formater stringFromDate:dis.timestamp];
}

- (void)lineGraph:(BEMSimpleLineGraphView *)graph didTouchGraphWithClosestIndex:(NSInteger)index {
    WeightData *dis = [data objectAtIndex:index];
    switch ([[NSUserDefaults standardUserDefaults] integerForKey:@"Unit"]) {
        case 0:{
            if (![dis.weightUnit isEqualToString:@"kg"]) {
                weight.text = [NSString stringWithFormat:@"Weight:%.2f %@",[dis.weight floatValue]*0.45359237,@"kg"];
            }
            else {
                weight.text = [NSString stringWithFormat:@"Weight:%.2f %@",[dis.weight floatValue],@"kg"];
            }
        }
            break;
        case 1:{
            if (![dis.weightUnit isEqualToString:@"lb"]) {
                weight.text = [NSString stringWithFormat:@"Weight:%.2f %@",[dis.weight floatValue]*2.20462262185,@"lb"];
            }
            else {
                weight.text = [NSString stringWithFormat:@"Weight:%.2f %@",[dis.weight floatValue],@"lb"];
            }
        }
            break;
        default:
            break;
    }
    bmi.text = [NSString stringWithFormat:@"BMI:%.2f",[dis.bmi floatValue]];
}

- (void)lineGraph:(BEMSimpleLineGraphView *)graph didReleaseTouchFromGraphWithClosestIndex:(CGFloat)index {
    
}

- (void)lineGraphDidFinishLoading:(BEMSimpleLineGraphView *)graph {
    
}

@end

