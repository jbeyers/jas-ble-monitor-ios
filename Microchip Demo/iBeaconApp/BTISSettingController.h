//
//  BTISSettingController.h
//  BeaconThings
//
//  Created by ILYAS on 16/02/16.
//  Copyright © 2016 Rick. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "BTConnectViewController.h"

@protocol BeaconThingsDelegate<NSObject>
- (void)UpdateBeaconData:(NSString *)BeaconData;
@end

@interface BTISSettingController : UITableViewController<UITextFieldDelegate, CLLocationManagerDelegate>
//@property (retain)NSData *beaconData;
@property (retain)BTConnectViewController *cbController;
@property(copy) NSString *StrServiceUUID;
@property(copy) NSString *BeaconData;
@property (retain) NSMutableDictionary *beaconDatabase;
@property(assign) id<BeaconThingsDelegate> delegate;
@end
