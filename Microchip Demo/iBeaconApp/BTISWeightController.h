//
//  BTISWeightController.h
//  BeaconThings
//
//  Created by ILYAS on 16/02/16.
//  Copyright © 2016 Rick. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import "QBFlatButton.h"
#import "CRGradientLabel.h"
#import "ZJSwitch.h"
#import "BTISSettingController.h"

//@interface ISWeightController : UIViewController < BeaconThingsDelegate , AVAudioPlayerDelegate>
@interface BTISWeightController : UIViewController < AVAudioPlayerDelegate>
{
    BOOL _LEDSwitch1Status;
    BOOL _LEDSwitch2Status;
    AVAudioPlayer *findAccessoryPlayer;
    BTISSettingController *settingView;
}
@property (nonatomic, retain) IBOutlet UIButton *LED1Button;
@property (nonatomic, retain) IBOutlet UIButton *LED2Button;
@property (nonatomic, retain) IBOutlet QBFlatButton *Button1;
@property (nonatomic, retain) IBOutlet QBFlatButton *Button2;
@property (nonatomic, retain) IBOutlet UIImageView *LED1Image;
@property (nonatomic, retain) IBOutlet UIImageView *LED2Image;
@property (nonatomic, retain) IBOutlet CRGradientLabel *LEDLabel;
@property (nonatomic, retain) IBOutlet CRGradientLabel *ButtonLabel;
@property (retain) UIBarButtonItem *connectBarButtonItem;
@property (retain) UIBarButtonItem *disconnectBarButtonItem;
//@property (retain) ISSettingController *settingView;
//@property (copy) NSString *TestString;
- (void)Manual_LE_Disconnect;
@end
