//
//  AdvertisementTableViewController.swift
//  mchp_ble_discover
//
//  Created by Jayanth Murthy - C13538 on 3/5/15.
//  Copyright (c) 2015 Jayanth Murthy - C13538. All rights reserved.
//

import UIKit
import CoreBluetooth

class AdvertisementTableViewController: UITableViewController, CBCentralManagerDelegate, CBPeripheralDelegate {

    var cbCentralManager: CBCentralManager?
    var peripheralInstance: CBPeripheral?
    fileprivate var logoView: UIView?
    
    fileprivate var advertisementDict: [String:String] = ["LocalName": "-", "ManufacturerData": "-", "ServiceData": "-", "ServiceUUIDs": "-", "OverflowServiceUUIDs": "-", "TxPowerLevel": "-", "IsConnectable": "-", "SolicitedServiceUUIDs": "-"]
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        
        if #available(iOS 10.0, *) {
            switch (central.state) {
            case CBManagerState.poweredOff:
                break
                
            case CBManagerState.poweredOn:
                //cbCentralManager!.scanForPeripheralsWithServices(nil, options: [CBCentralManagerScanOptionAllowDuplicatesKey: true])
                break
                
            case CBManagerState.resetting:
                break
                
            case CBManagerState.unauthorized:
                break
                
            case CBManagerState.unknown:
                break
                
            case CBManagerState.unsupported:
                break
                
                //default:
                //    break
            }
        }
        else {
            switch central.state.rawValue{
            case 4:
                break
                
            case 5:
                break
                
            case 0:
                break
                
            default:
                break
            }
            
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {

        
        if (peripheralInstance?.name == peripheral.name) {
            let peripheralConnectable: AnyObject = advertisementData["kCBAdvDataIsConnectable"]! as AnyObject
            
            if ((peripheral.state == CBPeripheralState.disconnected) && (peripheralConnectable as! NSNumber == 0)) {

                if let localName: AnyObject = advertisementData["kCBAdvDataLocalName"] as AnyObject {
                    advertisementDict.updateValue("\(localName)", forKey: "LocalName")
                }
                
                if let manufacturerData: AnyObject = advertisementData["kCBAdvDataManufacturerData"] as AnyObject {
                    advertisementDict.updateValue(((("\(manufacturerData)").replacingOccurrences(of: "<", with: "", options: NSString.CompareOptions.literal, range: nil)).replacingOccurrences(of: ">", with: "", options: NSString.CompareOptions.literal, range: nil)), forKey: "ManufacturerData")
                }
                
                if let serviceData: AnyObject = advertisementData["kCBAdvDataServiceData"] as AnyObject {
                    advertisementDict.updateValue("\(serviceData)", forKey: "ServiceData")
                }
                
                if let serviceUUIDs: AnyObject = advertisementData["kCBAdvDataServiceUUIDs"] as AnyObject {
                    var serviceUUIDsString: String = ""
                    
                    if serviceUUIDs is NSNull {
                        // null found
                    }
                    else {
                    for serviceUUID in serviceUUIDs as! NSArray {                   //fixed crash
                            serviceUUIDsString = serviceUUIDsString + "\(serviceUUID) "
                        }
                        advertisementDict.updateValue(serviceUUIDsString, forKey: "ServiceUUIDs")
                    }
                }
                
                if let overflowServiceUUIDs: AnyObject = advertisementData["kCBAdvDataOverflowServiceUUIDs"] as AnyObject {
                    var overflowServiceUUIDsString: String = ""
                    
                    if overflowServiceUUIDs is NSNull {
                        // null found
                    }
                    else {
                        for overflowServiceUUID in overflowServiceUUIDs as! NSArray {
                            overflowServiceUUIDsString = overflowServiceUUIDsString + "\(overflowServiceUUID) "
                        }
                        advertisementDict.updateValue(overflowServiceUUIDsString, forKey: "OverflowServiceUUIDs")
                    }
                }
                
                if let txPowerLevel: AnyObject = advertisementData["kCBAdvDataTxPowerLevel"] as AnyObject {
                    advertisementDict.updateValue("\(txPowerLevel)", forKey: "TxPowerLevel")
                }
                
                advertisementDict.updateValue("\(peripheralConnectable)", forKey: "IsConnectable")
                
                if let solicitedServiceUUIDs: AnyObject = advertisementData["kCBAdvDataSolicitedServiceUUIDs"] as AnyObject {
                    var solicitedServiceUUIDsString: String = ""
                    if solicitedServiceUUIDs is NSNull {
                        // null found
                    }
                    else {
                        for solicitedServiceUUID in solicitedServiceUUIDs as! NSArray {
                            solicitedServiceUUIDsString = solicitedServiceUUIDsString + "\(solicitedServiceUUID) "
                        }
                        advertisementDict.updateValue(solicitedServiceUUIDsString, forKey: "SolicitedServiceUUIDs")
                    }
                }
                
                tableView.reloadData()
            }
        }
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.backgroundView = UIImageView(image: UIImage(named: "BackgroundImage.png"))
        self.tableView.backgroundView?.alpha = 0.4
        
        logoView = UIImageView(image: UIImage(named: "microchip_logo.png"))
        logoView!.frame = CGRect(x: self.view.frame.midX-(266/2), y: self.view.frame.midY-(128/2), width: 266, height: 128)
        logoView!.alpha = 0.2
        self.tableView.backgroundView?.addSubview(logoView!)
        self.tableView.backgroundView?.bringSubview(toFront: logoView!)
        
        self.navigationController?.navigationBar.backgroundColor = UIColor.blue
        
        self.tableView.tableFooterView = UIView()
        
        cbCentralManager?.delegate = self
        cbCentralManager!.scanForPeripherals(withServices: nil, options: [CBCentralManagerScanOptionAllowDuplicatesKey: true])
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        logoView!.removeFromSuperview()
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        logoView = UIImageView(image: UIImage(named: "microchip_logo.png"))
        logoView!.frame = CGRect(x: self.view.frame.midX-(266/2), y: self.view.frame.midY-(128/2), width: 266, height: 128)
        logoView!.alpha = 0.2
        self.tableView.backgroundView?.addSubview(logoView!)
        self.tableView.backgroundView?.bringSubview(toFront: logoView!)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return advertisementDict.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "oCell", for: indexPath) 

        // Configure the cell...
        cell.backgroundColor = UIColor.white.withAlphaComponent(0.6)
        //cell.backgroundColor = UIColor.clearColor()
        //cell.textLabel?.font = UIFont(name: "Verdana", size: 22)
        cell.textLabel?.adjustsFontSizeToFitWidth = true
        //cell.detailTextLabel?.font = UIFont(name: "Verdana", size: 14)
        cell.detailTextLabel?.adjustsFontSizeToFitWidth = true
        
        cell.textLabel?.text = Array(advertisementDict.keys)[indexPath.row]
        cell.detailTextLabel?.text = advertisementDict[Array(advertisementDict.keys)[indexPath.row]]
        
        return cell
    }


    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
