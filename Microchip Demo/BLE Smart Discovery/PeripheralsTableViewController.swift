//
//  PeripheralsTableViewController.swift
//  mchp_ble_discover
//
//  Created by Jayanth Murthy - C13538 on 2/24/15.
//  Copyright (c) 2015 Jayanth Murthy - C13538. All rights reserved.
//

//import iAd
import UIKit
import CoreData
import Foundation
import CoreBluetooth

class PeripheralsTableViewController: UITableViewController, UIPopoverPresentationControllerDelegate, CBCentralManagerDelegate { //, ADBannerViewDelegate {
    
    //@IBOutlet weak var adBannerView: ADBannerView!
    @IBOutlet weak var peripheralsIndicator: UIActivityIndicatorView!
    @IBOutlet weak var bannerView: UIImageView!
    
    fileprivate var cbCentralManager: CBCentralManager!
    fileprivate var peripheralInstance: CBPeripheral?
    fileprivate var peripheralDict = [String: PeripheralsStructure]()
    fileprivate var previousPeripheralRSSIValue: Int = 0
    fileprivate var rssiTime: Date = Date()
    fileprivate var localTimer: Timer = Timer()
    fileprivate var logoView: UIView?
    fileprivate var alertControllerBluetooth: UIAlertController?
    fileprivate var bluetoothEnabled: Bool = false
    
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return false
    }
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        
    if #available(iOS 10.0, *) {
            switch (central.state) {
            case CBManagerState.poweredOff:
                bluetoothEnabled = false
                peripheralDict.removeAll()
                tableView.reloadData()
                peripheralsIndicator.startAnimating()
                
                alertControllerBluetooth = UIAlertController(title: "Bluetooth Smart Discover", message: "Bluetooth is turned off.\nTurn Bluetooth on to use the app.", preferredStyle: UIAlertControllerStyle.actionSheet)
                alertControllerBluetooth!.popoverPresentationController?.sourceView = self.view
                alertControllerBluetooth!.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 2.0, y: self.view.bounds.size.height / 1.0, width: 1.0, height: 1.0)
                alertControllerBluetooth!.popoverPresentationController?.delegate = self
                self.view.window?.rootViewController!.present(alertControllerBluetooth!, animated: true, completion: nil)

                break
                
            case CBManagerState.poweredOn:
                bluetoothEnabled = true
                alertControllerBluetooth?.dismiss(animated: true, completion: nil)
                cbCentralManager.scanForPeripherals(withServices: nil, options: [CBCentralManagerScanOptionAllowDuplicatesKey: true])
                break
                
            case CBManagerState.resetting:
                break
                
            case CBManagerState.unauthorized:
                break
                
            case CBManagerState.unknown:
                break
                
            case CBManagerState.unsupported:
                break
                
                //default:
                //    break
            }
            
    } else {
            switch central.state.rawValue{
            case 4:
                bluetoothEnabled = false
                peripheralDict.removeAll()
                tableView.reloadData()
                peripheralsIndicator.startAnimating()
                
                alertControllerBluetooth = UIAlertController(title: "Bluetooth Smart Discover", message: "Bluetooth is turned off.\nTurn Bluetooth on to use the app.", preferredStyle: UIAlertControllerStyle.actionSheet)
                alertControllerBluetooth!.popoverPresentationController?.sourceView = self.view
                alertControllerBluetooth!.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 2.0, y: self.view.bounds.size.height / 1.0, width: 1.0, height: 1.0)
                alertControllerBluetooth!.popoverPresentationController?.delegate = self
                self.view.window?.rootViewController!.present(alertControllerBluetooth!, animated: true, completion: nil)
                break
            case 5:
                bluetoothEnabled = true
                alertControllerBluetooth?.dismiss(animated: true, completion: nil)
                cbCentralManager.scanForPeripherals(withServices: nil, options: [CBCentralManagerScanOptionAllowDuplicatesKey: true])
                break
            case 1:
                break
            case 0:
                break
            case 2:
                break
            case 3:
                break
            default:
                break
                
            }
        }

    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        /*if (peripheral == nil) { //|| peripheral.name == nil || peripheral.name == "") {
        return
        }*/ //JAY check this
        self.peripheralInstance = peripheral
        let peripheralConnectable: AnyObject = advertisementData["kCBAdvDataIsConnectable"]! as AnyObject
        
        if ((self.peripheralInstance == nil || self.peripheralInstance?.state == CBPeripheralState.disconnected) && (peripheralConnectable as! NSNumber == 1)) {
            var peripheralName: String = String()
            if (advertisementData.index(forKey: "kCBAdvDataLocalName") != nil) {
                peripheralName = advertisementData["kCBAdvDataLocalName"] as! String
            }
            if (peripheralName == "" || peripheralName.isEmpty) {
                
                if (peripheral.name == nil || peripheral.name!.isEmpty) {
                    peripheralName = "Unknown"
                } else {
                    peripheralName = peripheral.name!
                }
            }
            
            peripheralDict.updateValue(PeripheralsStructure(peripheralInstance: peripheral, peripheralRSSI: RSSI, peripheralAdv: advertisementData as [String : AnyObject], timeStamp: Date()), forKey: peripheralName)
            peripheralsIndicator.stopAnimating()
            tableView.reloadData()
        }
    }
    
    #if false
    
    func centralManager(central: CBCentralManager!, didDiscoverPeripheral peripheral: CBPeripheral!, advertisementData: [NSObject : AnyObject]!, RSSI: NSNumber!) {
        /*
        self.peripheralInstance = peripheral
        peripheralDict.updateValue(PeripheralsStructure(peripheralInstance: peripheral, peripheralRSSI: RSSI, timeStamp: NSDate()), forKey: peripheral.name)
        peripheralsIndicator.stopAnimating()
        tableView.reloadData()
        */
        
        if (peripheral == nil) { // || peripheral.name == nil || peripheral.name == "") {
            return
        }

        
        self.peripheralInstance = peripheral
        let peripheralConnectable: AnyObject = advertisementData["kCBAdvDataIsConnectable"]!

        
        if ((self.peripheralInstance == nil || self.peripheralInstance?.state == CBPeripheralState.Disconnected) && (peripheralConnectable as! NSNumber == 1)) {
            
            var peripheralName: String = String()
    
            if (advertisementData.indexForKey("kCBAdvDataLocalName") != nil) {
                peripheralName = advertisementData["kCBAdvDataLocalName"] as! String
            }
            
            if (peripheralName == "" || peripheralName.isEmpty) {
                
                if (peripheral.name == nil || peripheral.name!.isEmpty) {
                    peripheralName = "Unknown"
                } else {
                    peripheralName = peripheral.name!
                }
            }
            
            
            peripheralDict.updateValue(PeripheralsStructure(peripheralInstance: peripheral, peripheralRSSI: RSSI, timeStamp: NSDate()), forKey: peripheralName)
            peripheralsIndicator.stopAnimating()
            tableView.reloadData()
        }
        
    }
    
    #endif
    
    /*
    func centralManager(central: CBCentralManager!, didDisconnectPeripheral peripheral: CBPeripheral!, error: NSError!) {
    }
    
    func bannerViewWillLoadAd(banner: ADBannerView!) {
        
    }
    
    func bannerViewDidLoadAd(banner: ADBannerView!) {
        //self.adBannerView.hidden = false
    }
    
    func bannerViewActionDidFinish(banner: ADBannerView!) {
        
    }
    
    func bannerViewActionShouldBegin(banner: ADBannerView!, willLeaveApplication willLeave: Bool) -> Bool {
        return true
    }
    
    func bannerView(banner: ADBannerView!, didFailToReceiveAdWithError error: NSError!) {
        
    }
    */
    
    func homeButtonClicked(){
        //self.dismiss(animated: true, completion: nil)
        let appDelegate: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
        appDelegate.navigationController.popToRootViewController(animated: true)
    }
    
    func leftMenuView() -> UIView{
        
        let LEFT_MENU_FRAME     = CGRect(x: 0, y: 0, width: 60, height: 40)
        let HOME_BUTTON_FRAME   = CGRect(x: 0, y: 0, width: 60, height: 40)
        
        // Home button
        let homeButton = UIButton(frame: HOME_BUTTON_FRAME)
        homeButton.addTarget(self, action: #selector(self.homeButtonClicked), for: UIControlEvents.touchUpInside)
        homeButton.setTitle("Home", for: UIControlState())
        homeButton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 15)
        homeButton.setTitleColor(UIColor.white, for: UIControlState())
        
        let leftMenuView = UIView(frame: LEFT_MENU_FRAME)
        leftMenuView.addSubview(homeButton)
        
        return leftMenuView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.canDisplayBannerAds = true
        //self.adBannerView.delegate = self
        //self.adBannerView.hidden = true
        
        self.tableView.backgroundView = UIImageView(image: UIImage(named: "BackgroundImage.png"))
        self.tableView.backgroundView?.alpha = 0.4
        
        logoView = UIImageView(image: UIImage(named: "microchip_logo.png"))
        logoView!.frame = CGRect(x: self.view.frame.midX-(266/2), y: self.view.frame.midY-(128/2), width: 266, height: 128)
        logoView!.alpha = 0.2
        self.tableView.backgroundView?.addSubview(logoView!)
        self.tableView.backgroundView?.bringSubview(toFront: logoView!)
        
        self.navigationController?.navigationBar.backgroundColor = UIColor.blue
        
        
        let registerNotigication = UIApplication.instancesRespond(to: #selector(UIApplication.registerUserNotificationSettings(_:)))
        if registerNotigication {
            let types: UIUserNotificationType = [UIUserNotificationType.alert, UIUserNotificationType.sound]
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: types, categories: nil))
        }
        
        let leftBarButtonItem = UIBarButtonItem(customView: leftMenuView())
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        peripheralDict.removeAll()
        tableView.reloadData()
        peripheralsIndicator.startAnimating()
        cbCentralManager = CBCentralManager(delegate: self, queue: nil)
        if (peripheralInstance != nil) {
            cbCentralManager.cancelPeripheralConnection(peripheralInstance!)
        }
        localTimer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(PeripheralsTableViewController.interruptLocalTimer), userInfo: nil, repeats: true)
        
        let bannerTouch = UITapGestureRecognizer(target: self, action: #selector(PeripheralsTableViewController.bannerTouchEvent))
        bannerTouch.numberOfTapsRequired = 1
        bannerTouch.numberOfTouchesRequired = 1
        bannerView.addGestureRecognizer(bannerTouch)
    }
    
    func bannerTouchEvent() {
        if (UIApplication.shared.canOpenURL(URL(string: "http://www.microchip.com/bluetooth")!)) {
            UIApplication.shared.openURL(URL(string: "http://www.microchip.com/bluetooth")!)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        localTimer.invalidate()
    }
    
    
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        logoView!.removeFromSuperview()
        if !bluetoothEnabled {
            alertControllerBluetooth?.dismiss(animated: true, completion: nil)
        }
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        logoView = UIImageView(image: UIImage(named: "microchip_logo.png"))
        logoView!.frame = CGRect(x: self.view.frame.midX-(266/2), y: self.view.frame.midY-(128/2), width: 266, height: 128)
        logoView!.alpha = 0.2
        self.tableView.backgroundView?.addSubview(logoView!)
        self.tableView.backgroundView?.bringSubview(toFront: logoView!)
        
        if !bluetoothEnabled {
            alertControllerBluetooth = UIAlertController(title: "Bluetooth Smart Discover", message: "Bluetooth is turned off.\nTurn Bluetooth on to use the app.", preferredStyle: UIAlertControllerStyle.actionSheet)
            alertControllerBluetooth!.popoverPresentationController?.sourceView = self.view
            alertControllerBluetooth!.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 2.0, y: self.view.bounds.size.height / 1.0, width: 1.0, height: 1.0)
            alertControllerBluetooth!.popoverPresentationController?.delegate = self
            present(alertControllerBluetooth!, animated: true, completion: nil)
        }
    }
    
    func interruptLocalTimer() {
        for keyDict in Array(peripheralDict.keys) {
            if ((peripheralDict[keyDict]!.timeStamp!).timeIntervalSinceNow < -15.0) {
                peripheralDict.removeValue(forKey: keyDict)
                if (peripheralDict.count == 0) {
                    tableView.reloadData()
                    peripheralsIndicator.startAnimating()
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return peripheralDict.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "jCell", for: indexPath)
        
        if (peripheralDict.count > indexPath.row) {
            //cell.textLabel?.font = UIFont(name: "Verdana", size: 22)
            cell.textLabel?.adjustsFontSizeToFitWidth = true
            //cell.detailTextLabel?.font = UIFont(name: "Verdana", size: 14)
            cell.detailTextLabel?.adjustsFontSizeToFitWidth = true
            
            //cell.backgroundColor = UIColor.clearColor().colorWithAlphaComponent(0.6)
            //cell.backgroundColor = UIColor.clearColor()
            cell.backgroundColor = UIColor.white.withAlphaComponent(0.6)
            // Configure the cell...
            /*
            if (indexPath.row % 2 == 0) {
                cell.backgroundColor = UIColor.clearColor()
            } else {
                cell.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.2)
                cell.textLabel?.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.0)
            }
            */
            
            var iconImage: UIImage? = UIImage(named: "rssiStrengthNil")
            cell.textLabel?.text = Array(peripheralDict.keys)[indexPath.row]
            
            if (abs(rssiTime.timeIntervalSince(peripheralDict[Array(peripheralDict.keys)[indexPath.row]]!.timeStamp! as Date)) > 2) {
                
                rssiTime = Date()
                
                let peripheralRSSIValue = peripheralDict[Array(peripheralDict.keys)[indexPath.row]]!.peripheralRSSI!


                if (peripheralRSSIValue.intValue < -27 && peripheralRSSIValue.intValue > -110) {
                
                    if (peripheralRSSIValue.intValue <= -27 && peripheralRSSIValue.intValue > -60 ) {
                        iconImage = UIImage(named: "rssiStrength100")
                    }
                    
                    if (peripheralRSSIValue.intValue <= -60 && peripheralRSSIValue.intValue > -70 ) {
                        iconImage = UIImage(named: "rssiStrength75")
                    }
                    
                    if (peripheralRSSIValue.intValue <= -70 && peripheralRSSIValue.intValue > -80 ) {
                        iconImage = UIImage(named: "rssiStrength50")
                    }
                    
                    if (peripheralRSSIValue.intValue <= -80 && peripheralRSSIValue.intValue > -90 ) {
                        iconImage = UIImage(named: "rssiStrength25")
                    }
                    
                    if (peripheralRSSIValue.intValue <= -90 && peripheralRSSIValue.intValue > -110 ) {
                        iconImage = UIImage(named: "rssiStrength0")
                    }
                    
                    
                    cell.detailTextLabel?.text = "RSSI: \(peripheralRSSIValue)dB"
                    cell.imageView?.image = iconImage
                }
            }
            
            
        }
        
        return cell
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the specified item to be editable.
    return true
    }
    */
    
    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    if editingStyle == .Delete {
    // Delete the row from the data source
    tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
    } else if editingStyle == .Insert {
    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
    }
    */
    
    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
    
    }
    */
    
    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the item to be re-orderable.
    return true
    }
    */
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        if (segue.destination.isKind(of: ConnectionTableViewController.self)) {
            let secondScreen = segue.destination as! ConnectionTableViewController
            
            // Pass the selected object to the new view controller.
            if let indexPath = self.tableView.indexPathForSelectedRow {
                cbCentralManager.stopScan()
                peripheralInstance = peripheralDict[Array(peripheralDict.keys)[indexPath.row]]?.peripheralInstance
                secondScreen.peripheralName = Array(peripheralDict.keys)[indexPath.row]
                secondScreen.peripheralInstance = peripheralInstance
                secondScreen.peripheralAdv = (peripheralDict[Array(peripheralDict.keys)[indexPath.row]]?.peripheralAdv)!
                secondScreen.cbCentralManager = cbCentralManager
                peripheralDict.removeAll()
                
                let appDelegate: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
                appDelegate.pStoreConfig = "SmartDiscover";
                let appContext: NSManagedObjectContext = appDelegate.managedObjectContext!
                let appUsers: AnyObject = NSEntityDescription.insertNewObject(forEntityName: "Peripherals", into: appContext)
                appUsers.setValue(peripheralInstance!.name, forKey: "name")
                appUsers.setValue(Date(), forKey: "timeStamp")
                do {
                    try appContext.save()
                } catch _ {
                }
                
            }
        }
    }
    
}
