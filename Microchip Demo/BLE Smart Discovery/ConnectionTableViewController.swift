//
//  ConnectionTableViewController.swift
//  mchp_ble_discover
//
//  Created by Jayanth Murthy - C13538 on 2/24/15.
//  Copyright (c) 2015 Jayanth Murthy - C13538. All rights reserved.
//

import UIKit
import Foundation
import CoreBluetooth

class ConnectionTableViewController: UITableViewController, CBCentralManagerDelegate, CBPeripheralDelegate, CustomCharacteristicValueDelegate {

    fileprivate var peripheralAttributes: [PeripheralAttributes] = [PeripheralAttributes]()
    var characteristicsI: [CBCharacteristic] = [CBCharacteristic]()
    var characteristicValues: [String] = [String]()
    
    var cbCentralManager: CBCentralManager!
    var peripheralInstance: CBPeripheral!
    var peripheralName: String?
    var peripheralAdv: [String : AnyObject]?
    var connectionStatus: Bool = false
    var localNotification = UILocalNotification()
    fileprivate var selectedIndexPath: IndexPath?
    fileprivate var logoView: UIView?
    //var refreshControl: UIRefreshControl!
    
    @IBOutlet weak var attributesIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var uuidLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    
    @IBOutlet weak var notifyTitle: UINavigationItem!

    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        
        if #available(iOS 10.0, *) {
            switch (central.state) {
            case CBManagerState.poweredOff:
                break
                
            case CBManagerState.poweredOn:
                break
                
            case CBManagerState.resetting:
                break
                
            case CBManagerState.unauthorized:
                break
                
            case CBManagerState.unknown:
                break
                
            case CBManagerState.unsupported:
                break
                
                //default:
                //    break
            }

        }
        else {
            switch central.state.rawValue{
            case 4:
                break
            case 5:
                break
            case 1:
                break
            case 0:
                break
            case 2:
                break
            case 3:
                break
            default:
                break
            
            }
        }
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        
            
        if (peripheral.name == nil || peripheral.name!.isEmpty) {
            peripheralName = "Unknown"
        } else {
            peripheralName = peripheral.name!
        }
        
        localNotification.alertBody = "\(peripheralName!) : Connected"
        localNotification.fireDate = Date(timeIntervalSinceNow: 1)
        UIApplication.shared.scheduleLocalNotification(localNotification)
        
        nameLabel.text = peripheralName //"\(peripheral.name)"
        uuidLabel.text = "UUID: \(peripheral.identifier.uuidString)"
        statusLabel.text = "Status: Connected"
        connectionStatus = true
        
        peripheralInstance?.delegate = self
        peripheralInstance?.discoverServices(nil)
        
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        statusLabel.text = "Status: Disconnected"
        
        if (peripheral.name == nil || peripheral.name!.isEmpty) {
            peripheralName = "Unknown"
        } else {
            peripheralName = peripheral.name!
        }
        
        localNotification.alertBody = "\(peripheralName!) : Disconnected"
        localNotification.fireDate = Date(timeIntervalSinceNow: 1)
        UIApplication.shared.scheduleLocalNotification(localNotification)
        
        if let navigationController = self.navigationController {
            navigationController.popViewController(animated: true)
        }
    }
    
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        let alertController = UIAlertController(title: "Bluetooth Smart Discover", message: "Failed to connect to the peripheral! Check if the peripheral is functioning properly and try to reconnect.", preferredStyle: UIAlertControllerStyle.alert)
        let alertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) -> Void in
            if let navigationController = self.navigationController {
                navigationController.popViewController(animated: true)
            }
        })
        alertController.addAction(alertAction)
        alertController.popoverPresentationController?.sourceView = self.view
        alertController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 3.0, y: self.view.bounds.size.height / 3.0, width: 1.0, height: 1.0)
        present(alertController, animated: true, completion: nil)
        
        cbCentralManager.cancelPeripheralConnection(peripheral)
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        
        if (peripheral.services!.isEmpty) {
            let alertController = UIAlertController(title: "Bluetooth Smart Discover", message: "Failed to discover any BLE services and characteristics on the peripheral.", preferredStyle: UIAlertControllerStyle.alert)
            let alertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                if let navigationController = self.navigationController {
                    navigationController.popViewController(animated: true)
                }
            })
            alertController.addAction(alertAction)
            alertController.popoverPresentationController?.sourceView = self.view
            alertController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 3.0, y: self.view.bounds.size.height / 3.0, width: 1.0, height: 1.0)
            present(alertController, animated: true, completion: nil)
            
            cbCentralManager.cancelPeripheralConnection(peripheral)
            return
        }
        
        for service in peripheral.services! {
            self.peripheralInstance?.discoverCharacteristics(nil, for: service )
        }
    
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        characteristicsI.removeAll()
        for characteristic in service.characteristics! {
            if (characteristic.uuid.uuidString.characters.count != 0) {
                characteristicsI.append(characteristic )
                peripheral.readValue(for: characteristic )
            }
            
        }
        peripheralAttributes.append(PeripheralAttributes(service: service, characteristics: characteristicsI, characteristicsValues: characteristicValues))
    }
    
    
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        let index = getIndexAttributes(peripheralAttributes, service: characteristic.service)

        if (index < peripheralAttributes.count) {
            if (characteristic.value != nil) {
                let characteristicUUID = "\(characteristic.uuid)"
                var bytesData = [UInt8] (repeating: 0, count: characteristic.value!.count)
                
                (characteristic.value! as NSData).getBytes(&bytesData, length: characteristic.value!.count)
                
                
                if ((characteristicUUID.range(of: "String")) != nil) {
                    let receivedString = String(bytes: bytesData, encoding: String.Encoding.ascii)
                    peripheralAttributes[index].characteristicsValues.append(receivedString!)
                } else {
                    peripheralAttributes[index].characteristicsValues.append("\(bytesData)")
                }
                
            }
            else {
                peripheralAttributes[index].characteristicsValues.append("")
            }
        }
        
        if (characteristic == peripheralAttributes[peripheralAttributes.count - 1].characteristics.last) {
            attributesIndicator.stopAnimating()
            tableView.reloadData()
            self.refreshControl?.endRefreshing()
            self.refreshControl?.isEnabled = true
            self.tableView.isScrollEnabled = true
            self.tableView.isUserInteractionEnabled = true
            self.refreshControl?.isEnabled = true
        }
        
        if ((characteristic.isNotifying) && (characteristic.value != nil)) {
            var characteristicUUID = bleStandardUUIDs["\(characteristic.uuid)"]
            if ((characteristicUUID?.isEmpty == true) || (characteristicUUID == "") || (characteristicUUID == nil)) {
                characteristicUUID = "\(characteristic.uuid)"
            }
            
            
            notifyTitle.title = "\(characteristicUUID!) : " + (("\(characteristic.value!)").replacingOccurrences(of: "<", with: "", options: NSString.CompareOptions.literal, range: nil)).replacingOccurrences(of: ">", with: "", options: NSString.CompareOptions.literal, range: nil)
        }
        
    }    
    
    func SetCharacteristicValue(_ characteristicValue: String) {
        peripheralAttributes[selectedIndexPath!.section - 1].characteristicsValues[selectedIndexPath!.row] = characteristicValue
    }
    
    func getIndexAttributes(_ pAttributes: [PeripheralAttributes], service: CBService) -> Int {
        var index: Int = -1
        for attribute in pAttributes {
            index += 1
            if (attribute.service == service) {
                return index
            }
        }
        return index
    }
    
    func getProperties(_ properties: UInt) -> String {
        var propertiesString: String = ""
        if ((properties & CBCharacteristicProperties.broadcast.rawValue) != 0) {
            propertiesString += "Broadcast "
        }
        if ((properties & CBCharacteristicProperties.read.rawValue) != 0) {
            propertiesString += "Read "
        }
        if ((properties & CBCharacteristicProperties.writeWithoutResponse.rawValue) != 0) {
            propertiesString += "Write Without Response "
        }
        if ((properties & CBCharacteristicProperties.write.rawValue) != 0) {
            propertiesString += "Write "
        }
        if ((properties & CBCharacteristicProperties.notify.rawValue) != 0) {
            propertiesString += "Notify "
        }
        if ((properties & CBCharacteristicProperties.indicate.rawValue) != 0) {
            propertiesString += "Indicate "
        }
        if ((properties & CBCharacteristicProperties.authenticatedSignedWrites.rawValue) != 0) {
            propertiesString += "Authenticated Signed Writes "
        }
        if ((properties & CBCharacteristicProperties.extendedProperties.rawValue) != 0) {
            propertiesString += "Extended Properties "
        }
        /*
        if ((properties & CBCharacteristicProperties.NotifyEncryptionRequired.rawValue) != 0) {
            propertiesString += "Notify Encryption Required "
        }
        if ((properties | CBCharacteristicProperties.IndicateEncryptionRequired.rawValue) != 0) {
            propertiesString += "Indicate Encryption Required "
        }
        */
        
        return propertiesString
    }
    
    /*
    func refreshCharacteristics () {
        for indexService in 1..<peripheralAttributes.count {
            for indexCharacteritic in 1..<peripheralAttributes[indexService].characteristics.count {
                peripheralInstance.readValueForCharacteristic(peripheralAttributes[indexService].characteristics[indexCharacteritic] as CBCharacteristic)
            }
        }
    }
    */
    
    func refresh(_ sender: AnyObject) {
        if (peripheralInstance.state == CBPeripheralState.connected && self.refreshControl?.isEnabled == true) {
            self.refreshControl?.isEnabled = false
            self.tableView.isScrollEnabled = false
            self.tableView.isUserInteractionEnabled = false
            peripheralAttributes.removeAll(keepingCapacity: false)
            peripheralInstance?.discoverServices(nil)
        }
        
    }
    
    func homeButtonClicked(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func leftMenuView() -> UIView{
        
        let LEFT_MENU_FRAME     = CGRect(x: 0, y: 0, width: 60, height: 40)
        let HOME_BUTTON_FRAME   = CGRect(x: 0, y: 0, width: 60, height: 40)
        
        // Home button
        let homeButton = UIButton(frame: HOME_BUTTON_FRAME)
        homeButton.addTarget(self, action: #selector(self.homeButtonClicked), for: UIControlEvents.touchUpInside)
        homeButton.setTitle("Home", for: UIControlState())
        homeButton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 15)
        homeButton.setTitleColor(UIColor.white, for: UIControlState())
        
        let leftMenuView = UIView(frame: LEFT_MENU_FRAME)
        leftMenuView.addSubview(homeButton)
        
        return leftMenuView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.backgroundView = UIImageView(image: UIImage(named: "BackgroundImage.png"))
        self.tableView.backgroundView?.alpha = 0.4
        
        logoView = UIImageView(image: UIImage(named: "microchip_logo.png"))
        logoView!.frame = CGRect(x: self.view.frame.midX-(266/2), y: self.view.frame.midY-(128/2), width: 266, height: 128)
        logoView!.alpha = 0.2
        self.tableView.backgroundView?.addSubview(logoView!)
        self.tableView.backgroundView?.bringSubview(toFront: logoView!)
        
        self.navigationController?.navigationBar.backgroundColor = UIColor.blue
        
        //self.refreshControl?.attributedTitle = NSAttributedString(string: "Pull down to refresh")
        self.refreshControl?.addTarget(self, action: #selector(ConnectionTableViewController.refresh(_:)), for: UIControlEvents.valueChanged)
        self.refreshControl?.isEnabled = false
        attributesIndicator.startAnimating()
            
        peripheralInstance.delegate = self
        cbCentralManager.delegate = self
        cbCentralManager.connect(peripheralInstance!, options: nil)
        
        //nameLabel.font = UIFont(name: "Verdana", size: 24)
        nameLabel.adjustsFontSizeToFitWidth = true
        //uuidLabel.font = UIFont(name: "Verdana", size: 16)
        uuidLabel.adjustsFontSizeToFitWidth = true
        //statusLabel.font = UIFont(name: "Verdana", size: 16)
        statusLabel.adjustsFontSizeToFitWidth = true
        
        let leftBarButtonItem = UIBarButtonItem(customView: leftMenuView())
        self.navigationItem.backBarButtonItem = leftBarButtonItem
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        peripheralInstance.delegate = self
        
        if (peripheralInstance.state == CBPeripheralState.connected) {
            //peripheralAttributes.removeAll(keepCapacity: false)
            //peripheralInstance?.discoverServices(nil)
            tableView.reloadData()
        } else if (connectionStatus == true) {
            if let navigationController = self.navigationController {
                navigationController.popViewController(animated: true)
            }
        }
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if (self.isMovingFromParentViewController) {
            if (peripheralInstance.state == CBPeripheralState.connected) {
                cbCentralManager.cancelPeripheralConnection(peripheralInstance!)
            }
        }
    }
    
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        logoView!.removeFromSuperview()
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        logoView = UIImageView(image: UIImage(named: "microchip_logo.png"))
        logoView!.frame = CGRect(x: self.view.frame.midX-(266/2), y: self.view.frame.midY-(128/2), width: 266, height: 128)
        logoView!.alpha = 0.2
        self.tableView.backgroundView?.addSubview(logoView!)
        self.tableView.backgroundView?.bringSubview(toFront: logoView!)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        
        return peripheralAttributes.count + 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        
        if section == 0 {
            return (peripheralAdv?.count)!
        }
        
        return peripheralAttributes[section-1].characteristics.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "kCell", for: indexPath)

        cell.backgroundColor = UIColor.white.withAlphaComponent(0.6)
        //cell.backgroundColor = UIColor.clearColor()
        //cell.textLabel?.font = UIFont(name: "Verdana", size: 22)
        cell.textLabel?.adjustsFontSizeToFitWidth = true
        //cell.detailTextLabel?.font = UIFont(name: "Verdana", size: 14)
        cell.detailTextLabel?.adjustsFontSizeToFitWidth = true
        
        if indexPath.section == 0 {
            cell.accessoryType = UITableViewCellAccessoryType.none
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.isUserInteractionEnabled = false
            
            switch Array(peripheralAdv!.keys)[indexPath.row] {
            case "kCBAdvDataIsConnectable":
                cell.textLabel?.text = "Connectable"
                if peripheralAdv![Array(peripheralAdv!.keys)[indexPath.row]] as! NSNumber == 1 {
                    cell.detailTextLabel?.text = "Yes"
                }
                else {
                    cell.detailTextLabel?.text = "No"
                }
                break
                
            case "kCBAdvDataLocalName":
                cell.textLabel?.text = "Local Name"
                cell.detailTextLabel?.text = peripheralAdv![Array(peripheralAdv!.keys)[indexPath.row]] as? String
                break
                
            case "kCBAdvDataManufacturerData":
                cell.textLabel?.text = "Manufacturer Data"
                cell.detailTextLabel?.text = ((("\(peripheralAdv![Array(peripheralAdv!.keys)[indexPath.row]]!)").replacingOccurrences(of: "<", with: "", options: NSString.CompareOptions.literal, range: nil)).replacingOccurrences(of: ">", with: "", options: NSString.CompareOptions.literal, range: nil))
                break
                
            case "kCBAdvDataServiceData":
                cell.textLabel?.text = "Service Data"
                cell.detailTextLabel?.text = "\(String(describing: peripheralAdv!["kCBAdvDataServiceData"]))"
                break
                
            case "kCBAdvDataServiceUUIDs":
                cell.textLabel?.text = "Service UUIDs"
                var serviceUUIDsString: String = ""
                for serviceUUID in peripheralAdv!["kCBAdvDataServiceUUIDs"] as! NSArray {
                    
                    let serviceUUIDString = bleStandardUUIDs["\(serviceUUID)"]
                    if ((serviceUUIDString?.isEmpty == true) || (serviceUUIDString == "") || (serviceUUIDString == nil)) {
                        serviceUUIDsString = serviceUUIDsString + "\(serviceUUID)"

                    } else {
                        serviceUUIDsString = serviceUUIDsString + serviceUUIDString!
                    }
                    
                    if (String(describing: serviceUUID) == String(describing: (peripheralAdv!["kCBAdvDataServiceUUIDs"] as! NSArray).lastObject!)) {
                        serviceUUIDsString = serviceUUIDsString + " "
                    } else {
                        serviceUUIDsString = serviceUUIDsString + ", "
                    }
                    //serviceUUIDsString = serviceUUIDsString + "\(serviceUUID) "
                }
                cell.detailTextLabel?.text = serviceUUIDsString
                break
                
            case "kCBAdvDataOverflowServiceUUIDs":
                cell.textLabel?.text = "Overflow Service UUIDs"
                var overflowServiceUUIDsString: String = ""
                for overflowServiceUUID in peripheralAdv!["kCBAdvDataOverflowServiceUUIDs"] as! NSArray {
                    overflowServiceUUIDsString = overflowServiceUUIDsString + "\(overflowServiceUUID) "
                }
                cell.detailTextLabel?.text = overflowServiceUUIDsString
                break
                
            case "kCBAdvDataTxPowerLevel":
                cell.textLabel?.text = "Tx Power Level"
                cell.detailTextLabel?.text = "\(peripheralAdv!["kCBAdvDataTxPowerLevel"]!)"
                break
            
            case "kCBAdvDataSolicitedServiceUUIDs":
                cell.textLabel?.text = "Solicited Service UUIDs"
                var solicitedServiceUUIDsString: String = ""
                for solicitedServiceUUID in peripheralAdv!["kCBAdvDataSolicitedServiceUUIDs"] as! NSArray {
                    solicitedServiceUUIDsString = solicitedServiceUUIDsString + "\(solicitedServiceUUID) "
                }
                cell.detailTextLabel?.text = solicitedServiceUUIDsString
                break
                
            default:
                break
            }
            
                        
            return cell
        }
        
        // Configure the cell...
        if (indexPath.section-1 < peripheralAttributes.count) {
            if (indexPath.row < peripheralAttributes[indexPath.section-1].characteristics.count) {
                var characteristicUUID = bleStandardUUIDs["\(peripheralAttributes[indexPath.section-1].characteristics[indexPath.row].uuid)"]
                if ((characteristicUUID?.isEmpty == true) || (characteristicUUID == "") || (characteristicUUID == nil)) {
                    characteristicUUID = "\(peripheralAttributes[indexPath.section-1].characteristics[indexPath.row].uuid)"
                }
                cell.textLabel?.text = characteristicUUID
            }
            if (indexPath.row < peripheralAttributes[indexPath.section-1].characteristicsValues.count) {
                if ("\(peripheralAttributes[indexPath.section-1].service.uuid)" == "Device Information") {
                    cell.detailTextLabel?.text = peripheralAttributes[indexPath.section-1].characteristicsValues[indexPath.row]
                }
                else if ("\(peripheralAttributes[indexPath.section-1].service.uuid)" == "Battery") {
                    cell.detailTextLabel?.text = (peripheralAttributes[indexPath.section-1].characteristicsValues[indexPath.row].replacingOccurrences(of: "[", with: "", options: NSString.CompareOptions.literal, range: nil)).replacingOccurrences(of: "]", with: "%", options: NSString.CompareOptions.literal, range: nil)
                }
                else {
                    let propertiesString = getProperties(peripheralAttributes[indexPath.section-1].characteristics[indexPath.row].properties.rawValue)
                    cell.detailTextLabel?.text = "Properties: " + propertiesString  // + "Value:" + peripheralAttributes[indexPath.section].characteristicsValues[indexPath.row]

                }
            }
        }
        return cell
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if section == 0 {
            return "Advertisement"
        }
        
        var serviceUUID = bleStandardUUIDs["\(peripheralAttributes[section-1].service.uuid)"]
        if ((serviceUUID?.isEmpty == true) || (serviceUUID == "") || (serviceUUID == nil)) {
            serviceUUID = "\(peripheralAttributes[section-1].service.uuid)"
        }
        else {
            //serviceUUID = serviceUUID! + " (\(peripheralAttributes[section].service.UUID))"
            serviceUUID = serviceUUID! + " (\(peripheralAttributes[section-1].service.uuid))"
        }
        
        return serviceUUID
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        notifyTitle.title = nil
        
        // Get the new view controller using [segue destinationViewController].
        let thirdScreen = segue.destination as! CharacteristicTableViewController
        
        // Pass the selected object to the new view controller.
        if let indexPath = self.tableView.indexPathForSelectedRow {
            thirdScreen.delegate = self
            selectedIndexPath = indexPath
            thirdScreen.peripheralInstance = peripheralInstance
            thirdScreen.characteristicInstance = peripheralAttributes[indexPath.section-1].characteristics[indexPath.row]
        }
        
    }
    

}
