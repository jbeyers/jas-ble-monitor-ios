//
//  DataLogTableViewController.swift
//  mchp_ble_discover
//
//  Created by Jayanth Murthy - C13538 on 3/4/15.
//  Copyright (c) 2015 Jayanth Murthy - C13538. All rights reserved.
//

import UIKit
import CoreBluetooth
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class DataLogTableViewController: UITableViewController, CBPeripheralDelegate {

    var dataLog: DataLog?
    var peripheralInstance: CBPeripheral?
    var characteristicInstance: CBCharacteristic?
    fileprivate var logoView: UIView?
    
    //@IBOutlet weak var clearButtonOutlet: UIBarButtonItem!
    
    @IBAction func cancelButton(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    
    /*
    @IBAction func clearbutton(sender: AnyObject) {
        clearButtonOutlet.enabled = false
        dataLog?.characteristicsValue.removeAll(keepCapacity: false)
        dataLog?.timestampValue.removeAll(keepCapacity: false)
        
        tableView.reloadData()
    }
    */
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        
        if (characteristic.value != nil) {
            if (characteristic == characteristicInstance) {
                if(dataLog?.characteristicsValue.count > 50 || dataLog?.timestampValue.count > 50) {
                    dataLog?.characteristicsValue.removeLast()
                    dataLog?.timestampValue.removeLast()
                }
                
                dataLog?.characteristicsValue.insert((("\(characteristic.value!)").replacingOccurrences(of: "<", with: "", options: NSString.CompareOptions.literal, range: nil)).replacingOccurrences(of: ">", with: "", options: NSString.CompareOptions.literal, range: nil), at: 0)
                dataLog?.timestampValue.insert(Date(), at: 0)
                tableView.reloadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //clearButtonOutlet.enabled = false
        
        self.tableView.backgroundView = UIImageView(image: UIImage(named: "BackgroundImage.png"))
        self.tableView.backgroundView?.alpha = 0.4
        
        logoView = UIImageView(image: UIImage(named: "microchip_logo.png"))
        logoView!.frame = CGRect(x: self.view.frame.midX-(266/2), y: self.view.frame.midY-(128/2), width: 266, height: 128)
        logoView!.alpha = 0.2
        self.tableView.backgroundView?.addSubview(logoView!)
        self.tableView.backgroundView?.bringSubview(toFront: logoView!)
        
        self.navigationController?.navigationBar.backgroundColor = UIColor.blue
        
        self.tableView.tableFooterView = UIView()
        
        peripheralInstance?.delegate = self
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        logoView!.removeFromSuperview()
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        logoView = UIImageView(image: UIImage(named: "microchip_logo.png"))
        logoView!.frame = CGRect(x: self.view.frame.midX-(266/2), y: self.view.frame.midY-(128/2), width: 266, height: 128)
        logoView!.alpha = 0.2
        self.tableView.backgroundView?.addSubview(logoView!)
        self.tableView.backgroundView?.bringSubview(toFront: logoView!)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        /*
        if (dataLog!.timestampValue.count > 0) {
            clearButtonOutlet.enabled = true
        }
        */
        return dataLog!.timestampValue.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "lCell", for: indexPath)        
        if (dataLog?.timestampValue.count > indexPath.row) {
            cell.backgroundColor = UIColor.white.withAlphaComponent(0.6)
            //cell.backgroundColor = UIColor.clearColor()
            //cell.textLabel?.font = UIFont(name: "Verdana", size: 22)
            cell.textLabel?.adjustsFontSizeToFitWidth = true
            //cell.detailTextLabel?.font = UIFont(name: "Verdana", size: 14)
            cell.detailTextLabel?.adjustsFontSizeToFitWidth = true
            
            // Configure the cell...
            cell.textLabel?.text = dataLog!.characteristicsValue[indexPath.row]
            cell.detailTextLabel?.text = "\(dataLog!.timestampValue[indexPath.row])"
        }

        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
