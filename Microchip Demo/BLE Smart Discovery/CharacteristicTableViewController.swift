//
//  CharacteristicTableViewController.swift
//  SmartDiscover
//
//  Created by Jayanth Murthy - C13538 on 5/27/15.
//  Copyright (c) 2015 Jayanth Murthy - C13538. All rights reserved.
//

import UIKit
import CoreBluetooth

protocol CustomCharacteristicValueDelegate {
    func SetCharacteristicValue(_ characteristicValue: String)
}

class CharacteristicTableViewController: UITableViewController, UITextViewDelegate, UITextFieldDelegate, CBPeripheralDelegate {
    
    var delegate: CustomCharacteristicValueDelegate? = nil
    
    var characteristicInstance: CBCharacteristic?
    var peripheralInstance: CBPeripheral?
    var readDataLog: DataLog = DataLog()
    fileprivate var logoView: UIView?
    fileprivate var keyboardHeight: CGFloat!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var uuidLabel: UILabel!
    @IBOutlet weak var propertiesLabel: UILabel!
    @IBOutlet weak var notifyingLabel: UILabel!
    //@IBOutlet weak var broadcastedLabel: UILabel!
    @IBOutlet weak var notifySwitchOutlet: UISwitch!
    @IBOutlet weak var writeField: UITextField!
    @IBOutlet weak var readField: UITextField!
    @IBOutlet weak var readButtonOutlet: UIButton!
    @IBOutlet weak var writeButtonOutlet: UIButton!
    @IBOutlet weak var readlogButtonOutlet: UIButton!
    
    @IBAction func notifySwitch(_ sender: AnyObject) {
        if (notifySwitchOutlet.isOn) {
            peripheralInstance!.setNotifyValue(true, for: characteristicInstance!)
        }
        else {
            peripheralInstance!.setNotifyValue(false, for: characteristicInstance!)
        }
    }
    
    @IBAction func readButton(_ sender: AnyObject) {
        peripheralInstance!.readValue(for: characteristicInstance!)
    }
    
    @IBAction func writeButton(_ sender: AnyObject) {
        
        var bytesData = hexStringToByteArray(writeField.text!)
        //let writeData = Data(bytes: UnsafePointer<UInt8>(&bytesData), count: bytesData.count)
        let writeData = Data(bytes: &bytesData, count: bytesData.count)
        
        if ((characteristicInstance!.properties.rawValue & CBCharacteristicProperties.write.rawValue) != 0) {
            peripheralInstance!.writeValue(writeData, for: characteristicInstance!, type: CBCharacteristicWriteType.withResponse)
        }
        else {
            peripheralInstance!.writeValue(writeData, for: characteristicInstance!, type: CBCharacteristicWriteType.withoutResponse)
        }
        
        writeField.text = ""
        writeField.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        writeField.resignFirstResponder()
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        writeButtonOutlet.setTitleColor(UIColor(red: 48.0/255, green: 131.0/255, blue: 251/255, alpha: 1), for: UIControlState())
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var result = true
        
        if (textField == writeField) {
            if string.characters.count > 0 {
                let illegalChars = CharacterSet(charactersIn: "0123456789abcdefABCDEF").inverted
                let replacementLegal = string.rangeOfCharacter(from: illegalChars) == nil
                result = replacementLegal
            }
        }
        
        return result
    }
    
    func animateTextField(_ up: Bool) {
        let movement = (up ? -keyboardHeight : keyboardHeight)
        
        UIView.animate(withDuration: 0, animations: {
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement!/2)
        })
    }
    
    func keyboardDidShow(_ notification: Notification) {
        if writeField.isFirstResponder {
            
            if let userInfo = notification.userInfo {
                if let keyboardSize =  (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                    keyboardHeight = keyboardSize.height
                    self.animateTextField(true)
                }
            }
        }
    }
    
    func keyboardWillHide(_ notification: Notification) {
        if writeField.isFirstResponder {
            self.animateTextField(false)
        }
    }
    
    
    /*
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        self.view.endEditing(true)
        super.touchesBegan(touches, withEvent: event)
        
    }
    */
    
    @IBAction func backButton(_ sender: AnyObject) {
        //dismissViewControllerAnimated(true, completion: nil)
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateNotificationStateFor characteristic: CBCharacteristic, error: Error?) {
        notifyingLabel.text = "Notifying: " + "\(characteristic.isNotifying)".capitalized
    }
    
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        writeButtonOutlet.setTitleColor(UIColor.green, for: UIControlState())
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        if ((characteristic == characteristicInstance) && (characteristic.value != nil)) {
            readField.text = (("\(characteristic.value!)").replacingOccurrences(of: "<", with: "", options: NSString.CompareOptions.literal, range: nil)).replacingOccurrences(of: ">", with: "", options: NSString.CompareOptions.literal, range: nil)
            
            let characteristicUUID = "\(characteristic.uuid)"
            var bytesData = [UInt8] (repeating: 0, count: characteristic.value!.count)
            (characteristic.value! as NSData).getBytes(&bytesData, length: characteristic.value!.count)
            if ((characteristicUUID.range(of: "String")) != nil) {
                let receivedString = String(bytes: bytesData, encoding: String.Encoding.ascii)
                self.delegate?.SetCharacteristicValue(receivedString!)
            } else {
                self.delegate?.SetCharacteristicValue("\(bytesData)")
            }
            
            
            if(readDataLog.characteristicsValue.count > 50 || readDataLog.timestampValue.count > 50) {
                readDataLog.characteristicsValue.removeLast()
                readDataLog.timestampValue.removeLast()
            }
            readDataLog.characteristicsValue.insert(readField.text!, at: 0)
            readDataLog.timestampValue.insert(Date(), at: 0)
        }
    }
    
    func getProperties(_ properties: UInt) -> String {
        var propertiesString: String = ""
        if ((properties & CBCharacteristicProperties.broadcast.rawValue) != 0) {
            propertiesString += "Broadcast "
        }
        if ((properties & CBCharacteristicProperties.read.rawValue) != 0) {
            readButtonOutlet.isEnabled = true
            readlogButtonOutlet.isEnabled = true
            propertiesString += "Read "
        }
        if ((properties & CBCharacteristicProperties.writeWithoutResponse.rawValue) != 0) {
            writeButtonOutlet.isEnabled = true
            writeField.isEnabled = true
            propertiesString += "Write Without Response "
        }
        if ((properties & CBCharacteristicProperties.write.rawValue) != 0) {
            writeButtonOutlet.isEnabled = true
            writeField.isEnabled = true
            propertiesString += "Write "
        }
        if ((properties & CBCharacteristicProperties.notify.rawValue) != 0) {
            notifySwitchOutlet.isEnabled = true
            readlogButtonOutlet.isEnabled = true
            propertiesString += "Notify "
        }
        if ((properties & CBCharacteristicProperties.indicate.rawValue) != 0) {
            notifySwitchOutlet.isEnabled = true
            readlogButtonOutlet.isEnabled = true
            propertiesString += "Indicate "
        }
        if ((properties & CBCharacteristicProperties.authenticatedSignedWrites.rawValue) != 0) {
            writeButtonOutlet.isEnabled = true
            writeField.isEnabled = true
            propertiesString += "Authenticated Signed Writes "
        }
        if ((properties & CBCharacteristicProperties.extendedProperties.rawValue) != 0) {
            propertiesString += "Extended Properties "
        }
        /*
        if ((properties & CBCharacteristicProperties.NotifyEncryptionRequired.rawValue) != 0) {
            notifySwitchOutlet.enabled = true
            readlogButtonOutlet.enabled = true
            propertiesString += "Notify Encryption Required "
        }
        if ((properties | CBCharacteristicProperties.IndicateEncryptionRequired.rawValue) != 0) {
            notifySwitchOutlet.enabled = true
            readlogButtonOutlet.enabled = true
            propertiesString += "Indicate Encryption Required "
        }
        */
        
        return propertiesString
    }
    
    func charPairToByte(_ strIn:String) -> UInt8
    {
        var byte:UInt8 = 0
        for c in strIn.characters
        {
            var number:UInt8 = 0
            byte = byte << 4
            switch(c)
            {
            case "0":
                number = 0
            case "1":
                number = 1
            case "2":
                number = 2
            case "3":
                number = 3
            case "4":
                number = 4
            case "5":
                number = 5
            case "6":
                number = 6
            case "7":
                number = 7
            case "8":
                number = 8
            case "9":
                number = 9
            case "A":
                number = 10
            case "B":
                number = 11
            case "C":
                number = 12
            case "D":
                number = 13
            case "E":
                number = 14
            case "F":
                number = 15
            default:
                print("bad char \(c)")
            }
            byte = byte | number
        }
        
        return byte
    }
    
    func hexStringToByteArray(_ stringIn: String) -> Array<UInt8>
    {
        var hexString = ""
        for character in stringIn.characters
        {
            if (character != " ")
            {
                hexString.append(character)
            }
        }
        
        hexString = hexString.uppercased()
        var bytes = [UInt8]()
        var stringLength = hexString.characters.count
        if (stringLength % 2 != 0)
        {
            stringLength -= 1;
        }
        
        //for var i:Int = 0; i < stringLength; i += 2
        
        for i in (0..<stringLength) {
            
            let sub = hexString.substring(with: (hexString.characters.index(hexString.startIndex, offsetBy: i) ..< hexString.characters.index(hexString.startIndex, offsetBy: i+2))) 
            let byte:UInt8 = charPairToByte(sub)
            bytes.append(byte)
        }
        
        return bytes
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.tableView.backgroundView = UIImageView(image: UIImage(named: "BackgroundImage.png"))
        self.tableView.backgroundView?.alpha = 0.4
        
        logoView = UIImageView(image: UIImage(named: "microchip_logo.png"))
        logoView!.frame = CGRect(x: self.view.frame.midX-(266/2), y: self.view.frame.midY-(128/2), width: 266, height: 128)
        logoView!.alpha = 0.2
        self.tableView.backgroundView?.addSubview(logoView!)
        self.tableView.backgroundView?.bringSubview(toFront: logoView!)
        
        self.navigationController?.navigationBar.backgroundColor = UIColor.blue
        
        
        peripheralInstance?.delegate = self
        
        notifySwitchOutlet.isEnabled = false
        readButtonOutlet.isEnabled = false
        readlogButtonOutlet.isEnabled = false
        writeButtonOutlet.isEnabled = false
        writeField.isEnabled = false
        
        //nameLabel.font = UIFont(name: "Verdana", size: 20)
        nameLabel.adjustsFontSizeToFitWidth = true
        //uuidLabel.font = UIFont(name: "Verdana", size: 16)
        uuidLabel.adjustsFontSizeToFitWidth = true
        //propertiesLabel.font = UIFont(name: "Verdana", size: 16)
        propertiesLabel.adjustsFontSizeToFitWidth = true
        //notifyingLabel.font = UIFont(name: "Verdana", size: 16)
        notifyingLabel.adjustsFontSizeToFitWidth = true
        //broadcastedLabel.font = UIFont(name: "Verdana", size: 16)
        //broadcastedLabel.adjustsFontSizeToFitWidth = true
        //readField.font = UIFont(name: "Verdana", size: 14)
        readField.adjustsFontSizeToFitWidth = true
        //writeField.font = UIFont(name: "Verdana", size: 14)
        writeField.adjustsFontSizeToFitWidth = true
        
        // Do any additional setup after loading the view.
        var characteristicUUID = bleStandardUUIDs["\(characteristicInstance!.uuid)"]
        if ((characteristicUUID?.isEmpty == true) || (characteristicUUID == "") || (characteristicUUID == nil)) {
            characteristicUUID = "\(characteristicInstance!.uuid)"
        }
        nameLabel.text = characteristicUUID
        uuidLabel.text = "UUID: \(characteristicInstance!.uuid)"
        
        let propertiesString = getProperties(characteristicInstance!.properties.rawValue)
        propertiesLabel.text = "Properties: " + propertiesString
        
        notifyingLabel.text = "Notifying: " + "\(characteristicInstance!.isNotifying)".capitalized
        
        if (characteristicInstance!.isNotifying) {
            notifySwitchOutlet.setOn(true, animated: true)
        }
        else {
            notifySwitchOutlet.setOn(false, animated: true)
        }
        
        //broadcastedLabel.text = "Broadcasted: \(characteristicInstance!.isBroadcasted)"

        NotificationCenter.default.addObserver(self, selector: #selector(CharacteristicTableViewController.keyboardDidShow(_:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CharacteristicTableViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        let viewTouch = UITapGestureRecognizer(target: self, action: #selector(CharacteristicTableViewController.touchesBegan as (CharacteristicTableViewController) -> () -> ()))
        viewTouch.numberOfTapsRequired = 1
        viewTouch.numberOfTouchesRequired = 1
        self.view.addGestureRecognizer(viewTouch)
    }
    
    func touchesBegan() {
        self.view.endEditing(true)
    }
    
    /*
    func animateTextField(up: Bool) {
        var movement = (up ? -keyboardHeight : keyboardHeight)
        
        UIView.animateWithDuration(0, animations: {
            self.view.frame = CGRectOffset(self.view.frame, 0, movement)
        })
    }
    
    func keyboardDidShow(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            if let keyboardSize =  (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
                keyboardHeight = keyboardSize.height
                self.animateTextField(true)
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        self.animateTextField(false)
    }
    */
    
    override func viewWillAppear(_ animated: Bool) {
        peripheralInstance?.delegate = self
    }
    
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        logoView!.removeFromSuperview()
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        logoView = UIImageView(image: UIImage(named: "microchip_logo.png"))
        logoView!.frame = CGRect(x: self.view.frame.midX-(266/2), y: self.view.frame.midY-(128/2), width: 266, height: 128)
        logoView!.alpha = 0.2
        self.tableView.backgroundView?.addSubview(logoView!)
        self.tableView.backgroundView?.bringSubview(toFront: logoView!)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.white.withAlphaComponent(0.6)
    }
    
    /*
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 0
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return 0
    }
    */
    
    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as! UITableViewCell

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

    // Get the new view controller using segue.destinationViewController.
    let fourthScreenNav = segue.destination as! UINavigationController
    let fourthScreen = fourthScreenNav.viewControllers[0] as! DataLogTableViewController

    // Pass the selected object to the new view controller.
    fourthScreen.dataLog = readDataLog
    fourthScreen.peripheralInstance = peripheralInstance
    fourthScreen.characteristicInstance = characteristicInstance

    }

}
