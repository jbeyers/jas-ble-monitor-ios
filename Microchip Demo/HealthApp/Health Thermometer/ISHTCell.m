//
//  ISHTCell.m
//  Health
//
//  Created by Rick on 2014/12/1.
//  Copyright (c) 2014年 Rick. All rights reserved.
//

#import "ISHTCell.h"

@implementation ISHTCell

- (void)awakeFromNib {
    // Initialization code
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _value = [[UILabel alloc] initWithFrame:CGRectMake(5.0f, 10.0f, 110.0f, 60.0f)];
        //_value.text = @"100.0 ℃";
        _value.font = [UIFont systemFontOfSize:22.0f];
        _value.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_value];
        _temperatureType = [[UILabel alloc] initWithFrame:CGRectMake(120.0f, 10.0f, 200.0f, 18.0f)];
        //_temperatureType.text = @"Type:Gastro-intestinal Tract";
        _temperatureType.font = [UIFont systemFontOfSize:16.0f];
        _temperatureType.adjustsFontSizeToFitWidth = YES;
        [self.contentView addSubview:_temperatureType];
        _time = [[UILabel alloc] initWithFrame:CGRectMake(120.0f, 55.0f, 220.0f, 18.0f)];
        //_time.text = @"2014/10/10 12:12:12";
        _time.font = [UIFont systemFontOfSize:16.0f];
        [self.contentView addSubview:_time];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
