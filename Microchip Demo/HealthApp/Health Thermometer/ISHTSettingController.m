//
//  ISHTSettingController.m
//  Health
//
//  Created by Rick on 2014/12/1.
//  Copyright (c) 2014年 Rick. All rights reserved.
//

#import "ISHTSettingController.h"
#import "HKMyPeripheral.h"
#import "AppDelegate.h"
#import "UUID.h"

@interface ISHTSettingController () {
    UITextField *textField;
    UISegmentedControl *unit;
    UISwitch *healthKitSwitch;
    UIButton *send;
}

@end

@implementation ISHTSettingController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Settings";
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    textField = [[UITextField alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 100.0f, 30.0f)];
    textField.borderStyle = UITextBorderStyleRoundedRect;
    textField.keyboardType = UIKeyboardTypeNumberPad;
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"temperatureInterval"]) {
        textField.text = [[[NSUserDefaults standardUserDefaults] objectForKey:@"temperatureInterval"] stringValue];
    }
    unit = [[UISegmentedControl alloc] initWithItems:@[@"℃",@"℉"]];
    unit.frame = CGRectMake(0.0f, 0.0f, 100.0f, 30.0f);
    unit.selectedSegmentIndex = [[NSUserDefaults standardUserDefaults] integerForKey:@"temperatureUnit"];
    [unit addTarget:self action:@selector(setUnit:) forControlEvents:UIControlEventValueChanged];
    send = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [send setTitle:@"Send" forState:UIControlStateNormal];
    if (!_peripheral) {
        send.enabled = NO;
    }
    send.frame = CGRectMake(0.0f, 0.0f, 50.0f, 30.0f);
    [send addTarget:self action:@selector(sendInterval) forControlEvents:UIControlEventTouchUpInside];
#ifdef USE_HEALTH_KIT
    healthKitSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 50.0f, 30.0f)];
    [healthKitSwitch addTarget:self action:@selector(setUseHealthKit:) forControlEvents:UIControlEventValueChanged];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if ([appDelegate isHealthKitAvailable]) {
        healthKitSwitch.on = [[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"%@HealthKit",UUIDSTR_HEALTH_THERMOMETER_SERVICE]];
    }
#endif
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setUnit:(UISegmentedControl *)segment {
    [[NSUserDefaults standardUserDefaults] setObject:@(segment.selectedSegmentIndex) forKey:@"temperatureUnit"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)setUseHealthKit:(UISwitch *)sender {
    [[NSUserDefaults standardUserDefaults] setObject:@(sender.on) forKey:[NSString stringWithFormat:@"%@HealthKit",UUIDSTR_HEALTH_THERMOMETER_SERVICE]];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)sendInterval {
    if (![textField.text isEqualToString:@""]) {
        [_peripheral setMeasurementInterval:[textField.text integerValue]];
    }
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#ifdef USE_HEALTH_KIT
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if ([appDelegate isHealthKitAvailable]) {
        return 4;
    }
#endif
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    switch (indexPath.row) {
        case 0:
            cell.textLabel.text = @"Measurement Interval";
            cell.accessoryView = textField;
            break;
        case 1:
            cell.textLabel.text = @"";
            cell.accessoryView = send;
            break;
        case 2:
            cell.textLabel.text = @"Unit";
            cell.accessoryView = unit;
            break;
        case 3:
            cell.textLabel.text = @"HealthKit";
            cell.accessoryView = healthKitSwitch;
            break;
        default:
            break;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
