//
//  ISHTDataParser.m
//  Health
//
//  Created by Rick on 2014/12/1.
//  Copyright (c) 2014年 Rick. All rights reserved.
//

#import "ISHTDataParser.h"
#import "ISWeightParser.h"
#import "CovertTools.h"

@interface ISHTDataParser() {
    NSDateFormatter *formater;
}
@end

@implementation ISHTDataParser
- (id)init {
    self = [super init];
    if (self) {
        formater = [[NSDateFormatter alloc] init];
        [formater setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    }
    return self;
}

- (NSDictionary *)decodeData:(NSData *)data {
    uint8_t buffer[1];
    [data getBytes:&buffer length:1];
    HEALTH_THERMOMETER_FLAG *flag = (HEALTH_THERMOMETER_FLAG *)buffer;
    int pointer = 1;
    NSString *uint;
    if (flag->temperature_units == 0) {
        uint = @"℃";
    }
    else {
        uint = @"℉";
    }
    uint32_t int_data;
    [data getBytes:&int_data range:NSMakeRange(pointer, 4)];
    float_t temperature = [CovertTools covertFLOAT:int_data];
    pointer+=4;
    NSDate *time = [NSDate date];
    if (flag->timestamp ==1) {
        uint8_t time_buffer[7];
        [data getBytes:&time_buffer range:NSMakeRange(pointer, 7)];
        pointer+=7;
        TIMESTAMP *t = (TIMESTAMP *)time_buffer;
        NSString *t_s = [NSString stringWithFormat:@"%d-%d-%d %d:%d:%d",t->year,t->month,t->day,t->hour,t->minute,t->second];
        time = [formater dateFromString:t_s];
    }
    uint8_t temperature_type = 0;
    if (flag->temperature_type == 1) {
        [data getBytes:&temperature_type range:NSMakeRange(pointer, 1)];
    }
    return @{@"uint":uint,
             @"temperature":@(temperature),
             @"timestamp":time,
             @"temperature_type":@(temperature_type)};
}

@end
