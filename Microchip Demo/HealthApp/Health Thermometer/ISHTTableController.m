//
//  ISHTTableController.m
//  Health
//
//  Created by Rick on 2014/12/1.
//  Copyright (c) 2014年 Rick. All rights reserved.
//

#import "ISHTTableController.h"
#import "AppDelegate.h"
#import "ISHTCell.h"
#import "HealthThermometerData.h"
#import "HKMyPeripheral.h"

@interface ISHTTableController (){
    NSMutableArray *data;
    NSDateFormatter *formater;
    
}

@end

@implementation ISHTTableController

- (void)viewDidLoad {
    [super viewDidLoad];
    data = [[NSMutableArray alloc] init];
    [self.tableView registerClass:[ISHTCell class] forCellReuseIdentifier:@"Cell"];
    self.title = @"History";
    formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"HealthThermometerData" inManagedObjectContext:appDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    NSSortDescriptor* sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"timestamp" ascending:NO];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    NSArray *temp = [appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:nil];
    [data removeAllObjects];
    [data addObjectsFromArray:temp];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [data count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ISHTCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    HealthThermometerData *dis = [data objectAtIndex:indexPath.row];
    cell.value.text = [NSString stringWithFormat:@"%.2f%@",[dis.value floatValue],dis.unit];
    switch ([dis.type integerValue]) {
        case TemperatureType_Other:
            cell.temperatureType.text = @"Type:Other";
            break;
        case TemperatureType_Armpit:
            cell.temperatureType.text = @"Type:Armpit";
            break;
        case TemperatureType_Body:
            cell.temperatureType.text = @"Type:Body";
            break;
        case TemperatureType_Ear:
            cell.temperatureType.text = @"Type:Ear";
            break;
        case TemperatureType_Finger:
            cell.temperatureType.text = @"Type:Finger";
            break;
        case TemperatureType_GastroIntestinalTract:
            cell.temperatureType.text = @"Type:Gastro-intestinal Tract";
            break;
        case TemperatureType_Mouth:
            cell.temperatureType.text = @"Type:Mouth";
            break;
        case TemperatureType_Rectum:
            cell.temperatureType.text = @"Type:Rectum";
            break;
        case TemperatureType_Toe:
            cell.temperatureType.text = @"Type:Toe";
            break;
        case TemperatureType_Tympanum:
            cell.temperatureType.text = @"Type:Tympanum";
            break;
        default:
            cell.temperatureType.text = @"Type:Other";
            break;
    }
    cell.time.text = [formater stringFromDate:dis.timestamp];
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
