//
//  ISHTGraphController.m
//  Health
//
//  Created by Rick on 2014/12/1.
//  Copyright (c) 2014年 Rick. All rights reserved.
//

#import "ISHTGraphController.h"
#import "BEMSimpleLineGraphView.h"
#import "AppDelegate.h"
#import "UUID.h"
#import "ISHTTableController.h"
#import "HealthThermometerData.h"

@interface ISHTGraphController () <BEMSimpleLineGraphDataSource, BEMSimpleLineGraphDelegate>{
    NSMutableArray *data;
    NSDateFormatter *formater;
}


@end

@implementation ISHTGraphController

- (void)loadView {
    if ([[UIScreen mainScreen] bounds].size.height > 480.0f) {
        [super loadView];
    }
    else {
        UIScrollView *view = [[UIScrollView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        view.contentSize = CGSizeMake(320.0f, 568.0f);
        //view.contentInset = UIEdgeInsetsMake(-64.0f, 0, 0, 0);
        self.view = view;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"History";
    self.view.backgroundColor = [UIColor whiteColor];
    data = [[NSMutableArray alloc] init];
    formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:@"yyyy/MM/dd"];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"HealthThermometerData" inManagedObjectContext:appDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    NSSortDescriptor* sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"timestamp" ascending:NO];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
#ifdef USE_HEALTH_KIT
    if ([appDelegate isHealthKitAvailable] && [[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"%@HealthKit",UUIDSTR_HEALTH_THERMOMETER_SERVICE]]) {
        HKQuantityType *weightType = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierBodyTemperature];
        NSSortDescriptor *timeSortDescriptor = [[NSSortDescriptor alloc] initWithKey:HKSampleSortIdentifierEndDate ascending:YES];
        HKSampleQuery *query = [[HKSampleQuery alloc] initWithSampleType:weightType predicate:nil limit:HKObjectQueryNoLimit sortDescriptors:@[timeSortDescriptor] resultsHandler:^(HKSampleQuery *query, NSArray *results, NSError *error) {
            if  (results) {
                [data addObjectsFromArray:results];
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIScrollView *graphScroll = (UIScrollView *)[self.view viewWithTag:1];
                    CGFloat width = 32+50*data.count;
                    if (width < 320.0f) width = 320.0f;
                    graphScroll.contentSize = CGSizeMake(width+5, 200);
                    BEMSimpleLineGraphView *myGraph = (BEMSimpleLineGraphView *)[[graphScroll subviews] objectAtIndex:0];
                    myGraph.frame = CGRectMake(0, 0, width, 200);
                    [myGraph reloadGraph];
                });
            }
            else {
                NSLog(@"HealthKit Error : %@",error.description);
            }
        }];
        [appDelegate.healthStore executeQuery:query];
    }
    else {
#endif
        NSArray *temp = [appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:nil];
        [data addObjectsFromArray:temp];
#ifdef USE_HEALTH_KIT
    }
#endif
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    UIScrollView *graphScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 60, 320, 200)];
    graphScroll.tag = 1;
    //graphScroll.delegate = self;
    [self.view addSubview:graphScroll];
    CGFloat width = 32+50*data.count;
    if (width < 320.0f) width = 320.0f;
    graphScroll.contentSize = CGSizeMake(width+5, 200);
    BEMSimpleLineGraphView *myGraph = [[BEMSimpleLineGraphView alloc] initWithFrame:CGRectMake(0, 0, width, 200)];
    myGraph.delegate = self;
    myGraph.dataSource = self;
    myGraph.colorTop = [UIColor colorWithRed:0.251 green:0.646 blue:0.820 alpha:1.000];
    myGraph.colorBottom = [UIColor colorWithRed:0.251 green:0.646 blue:0.820 alpha:1.000];
    myGraph.colorLine = [UIColor whiteColor];
    myGraph.colorXaxisLabel = [UIColor whiteColor];
    myGraph.colorYaxisLabel = [UIColor whiteColor];
    myGraph.widthLine = 3.0;
    myGraph.enableTouchReport = YES;
    myGraph.enablePopUpReport = YES;
    //myGraph.enableBezierCurve = YES;
    myGraph.enableYAxisLabel = YES;
    myGraph.autoScaleYAxis = YES;
    myGraph.alwaysDisplayDots = NO;
    myGraph.enableReferenceAxisLines = YES;
    myGraph.enableReferenceAxisFrame = YES;
    myGraph.animationGraphStyle = BEMLineAnimationNone;
    myGraph.animationGraphEntranceTime = 0;
    myGraph.tag = 1;
    myGraph.userInteractionEnabled = YES;
    [graphScroll addSubview:myGraph];
    
#ifdef USE_HEALTH_KIT
    if (![appDelegate isHealthKitAvailable] || ![[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"%@HealthKit",UUIDSTR_HEALTH_THERMOMETER_SERVICE]]) {
#endif
        UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithTitle:@"Detail" style:UIBarButtonItemStylePlain target:self action:@selector(showTable:)];
        self.navigationItem.rightBarButtonItem = right;
#ifdef USE_HEALTH_KIT
    }
#endif
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];

}

- (void)showTable:(id)sender {
    ISHTTableController *hsitory = [[ISHTTableController alloc] initWithStyle:UITableViewStylePlain];
    [self.navigationController pushViewController:hsitory animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - SimpleLineGraph Data Source

- (NSInteger)numberOfPointsInLineGraph:(BEMSimpleLineGraphView *)graph {
    return data.count?data.count:0;
}

- (CGFloat)lineGraph:(BEMSimpleLineGraphView *)graph valueForPointAtIndex:(NSInteger)index {
#ifdef USE_HEALTH_KIT
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if ([appDelegate isHealthKitAvailable] && [[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"%@HealthKit",UUIDSTR_HEALTH_THERMOMETER_SERVICE]]) {
        HKQuantitySample *quantitySample = data[index];
        HKQuantity *quantity = quantitySample.quantity;
        if ([[NSUserDefaults standardUserDefaults] integerForKey:@"temperatureUnit"] == 0) {
            return [quantity doubleValueForUnit:[HKUnit degreeCelsiusUnit]];
        }
        else {
            return [quantity doubleValueForUnit:[HKUnit degreeFahrenheitUnit]];
        }
    }
    else {
#endif
        HealthThermometerData *dis = [data objectAtIndex:index];
        if ([[NSUserDefaults standardUserDefaults] integerForKey:@"temperatureUnit"] == 0) {
            if ([dis.unit isEqualToString:@"℉"]) {
                return ([dis.value floatValue]-32)*5/9;
            }
            return [dis.value floatValue];
        }
        else {
            if ([dis.unit isEqualToString:@"℃"]) {
                return ([dis.value floatValue]*9/5)+32;
            }
            return [dis.value floatValue];
        }
#ifdef USE_HEALTH_KIT
    }
#endif
    return 0;
}

#pragma mark - SimpleLineGraph Delegate
- (NSString *)popUpSuffixForlineGraph:(BEMSimpleLineGraphView *)graph {
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"temperatureUnit"] == 0) {
        return @"℃";
    }
    else {
        return @"℉";
    }
}

- (NSInteger)numberOfGapsBetweenLabelsOnLineGraph:(BEMSimpleLineGraphView *)graph {
    return 1;
}

- (NSString *)lineGraph:(BEMSimpleLineGraphView *)graph labelOnXAxisForIndex:(NSInteger)index {
#ifdef USE_HEALTH_KIT
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if ([appDelegate isHealthKitAvailable] && [[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"%@HealthKit",UUIDSTR_HEALTH_THERMOMETER_SERVICE]]) {
        HKQuantitySample *quantitySample = data[index];
        return [formater stringFromDate:quantitySample.startDate];
    }
#endif
    HealthThermometerData *dis = [data objectAtIndex:index];
    return [formater stringFromDate:dis.timestamp];
}

- (void)lineGraph:(BEMSimpleLineGraphView *)graph didTouchGraphWithClosestIndex:(NSInteger)index {
    
}

- (void)lineGraph:(BEMSimpleLineGraphView *)graph didReleaseTouchFromGraphWithClosestIndex:(CGFloat)index {
    
}

- (void)lineGraphDidFinishLoading:(BEMSimpleLineGraphView *)graph {
    
}

@end
