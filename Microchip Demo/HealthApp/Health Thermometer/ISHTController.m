//
//  ISHTController.m
//  Health
//
//  Created by Rick on 2014/12/1.
//  Copyright (c) 2014年 Rick. All rights reserved.
//

#import "ISHTController.h"
#import "HKConnectViewController.h"
#import "HKCBController.h"
#import "AppDelegate.h"
#import "ISHTSettingController.h"
#import "ISHTGraphController.h"

@interface ISHTController () <HKCBControllerDelegate,MyPeripheralDelegate> {
    HKConnectViewController *scan;
    UIButton *scan_but;
    UIImageView *connect_icon;
    HKMyPeripheral *connect_peripheral;
    UILabel *unit;
    UILabel *value;
    UILabel *intermediate;
    UILabel *time;
    UILabel *sensorLocation;
    NSDateFormatter *formater;

}

@end

@implementation ISHTController

- (void)loadView {
    if ([[UIScreen mainScreen] bounds].size.height > 480.0f) {
        [super loadView];
    }
    else {
        UIScrollView *view = [[UIScrollView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        view.contentSize = CGSizeMake(320.0f, 568.0f);
        view.contentInset = UIEdgeInsetsMake(-64.0f, 0, 0, 0);
        self.view = view;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
#ifdef USE_HEALTH_KIT
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if ([appDelegate isHealthKitAvailable]) {
        [appDelegate.healthStore requestAuthorizationToShareTypes:[NSSet setWithObjects:[HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierBodyTemperature], nil] readTypes:[NSSet setWithObjects:[HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierBodyTemperature], nil] completion:^(BOOL success, NSError *error) {
            if (success) {
                NSLog(@"Authorization Success");
            }
        }];
    }
#endif
    CGRect rect = [[UIScreen mainScreen]bounds];
    self.title = @"Body Temperature";
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:@selector(dismiss)];
    UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithTitle:@"Settings" style:UIBarButtonItemStylePlain target:self action:@selector(showSetting:)];
    self.navigationItem.rightBarButtonItem = right;
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    scan = [[HKConnectViewController alloc] initWithNibName:nil bundle:nil];
    scan.uuids = @[UUIDSTR_HEALTH_THERMOMETER_SERVICE];
    scan.serviceUuids = @[[CBUUID UUIDWithString:UUIDSTR_DEVICE_INFO_SERVICE],[CBUUID UUIDWithString:UUIDSTR_HEALTH_THERMOMETER_SERVICE]];
    scan.delegate = self;
    UIView *info_bg = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, rect.size.width, 350.0f)];
    info_bg.backgroundColor = [UIColor colorWithRed:0.251 green:0.646 blue:0.820 alpha:1.000];
    [self.view addSubview:info_bg];
    unit = [[UILabel alloc] initWithFrame:CGRectMake(250.0f, 70.0f, 100.0f, 32.0f)];
    //unit.text = @"℃";
    unit.textColor = [UIColor whiteColor];
    unit.textAlignment = NSTextAlignmentCenter;
    [info_bg addSubview:unit];
    value = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 160.0f, rect.size.width, 100.0f)];
    value.text = @"0";
    value.font = [UIFont boldSystemFontOfSize:90.0f];
    value.textColor = [UIColor whiteColor];
    value.textAlignment = NSTextAlignmentCenter;
    [info_bg addSubview:value];
    
    connect_icon = [[UIImageView alloc] initWithFrame:CGRectMake(10.0f, 70.0f, 40.0f, 40.0f)];
    connect_icon.image = [UIImage imageNamed:@"thermometer_white.png"];
    connect_icon.alpha = 0.5;
    [info_bg addSubview:connect_icon];
    UIButton *history = [UIButton buttonWithType:UIButtonTypeCustom];
    history.frame = info_bg.bounds;
    [history addTarget:self action:@selector(showGraph:) forControlEvents:UIControlEventTouchUpInside];
    [info_bg addSubview:history];
    sensorLocation = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 300.0f, 315.0f, 50.0f)];
    sensorLocation.numberOfLines = 2;
    //sensorLocation.text = @"Temperature Type\nOther";
    sensorLocation.textColor = [UIColor whiteColor];
    sensorLocation.textAlignment = NSTextAlignmentRight;
    [info_bg addSubview:sensorLocation];

    UIView *intermediate_bg = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 350.0f, rect.size.width/2, 150.0f)];
    intermediate_bg.backgroundColor = [UIColor colorWithRed:0.830 green:0.830 blue:0.340 alpha:1.000];
    [self.view addSubview:intermediate_bg];
    UILabel *intermediate_l = [[UILabel alloc] initWithFrame:CGRectMake(5.0f, 5.0f, 100.0f, 30.0f)];
    intermediate_l.textColor = [UIColor whiteColor];
    intermediate_l.text = @"Intermediate";
    [intermediate_bg addSubview:intermediate_l];
    intermediate = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, rect.size.width/2, 60.0f)];
    intermediate.center = CGPointMake(80.0f, 75.0f);
    //intermediate.text = @"100℃";
    intermediate.font = [UIFont boldSystemFontOfSize:24.0f];
    intermediate.textColor = [UIColor whiteColor];
    intermediate.textAlignment = NSTextAlignmentCenter;
    [intermediate_bg addSubview:intermediate];

    UIView *time_bg = [[UIView alloc] initWithFrame:CGRectMake(rect.size.width/2, 350.0f, rect.size.width/2, 150.0f)];
    time_bg.backgroundColor = [UIColor colorWithRed:0.400 green:1.000 blue:0.800 alpha:1.000];
    [self.view addSubview:time_bg];
    time = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, rect.size.width/2, 60.0f)];
    time.center = CGPointMake(80.0f, 75.0f);
    //time.text = @"2014/10/01\n18:00:00";
    time.font = [UIFont boldSystemFontOfSize:24.0f];
    time.textColor = [UIColor whiteColor];
    time.textAlignment = NSTextAlignmentCenter;
    time.numberOfLines = 2;
    time.lineBreakMode = NSLineBreakByWordWrapping;
    [time_bg addSubview:time];

    UIView *scan_bg = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 500.0f, rect.size.width, 68.0f)];
    scan_bg.backgroundColor = [UIColor colorWithRed:0.290 green:0.828 blue:1.000 alpha:1.000];
    [self.view addSubview:scan_bg];
    scan_but = [UIButton buttonWithType:UIButtonTypeCustom];
    [scan_but setTitle:@"Scan" forState:UIControlStateNormal];
    scan_but.titleLabel.font = [UIFont boldSystemFontOfSize:24.0f];
    scan_but.frame = CGRectMake(0.0f, 0.0f, 320.0f, 68.0f);
    scan_but.center = CGPointMake(rect.size.width/2, 34.0f);
    scan_but.tag = 0;
    [scan_bg addSubview:scan_but];
    [scan_but addTarget:self action:@selector(scan) forControlEvents:UIControlEventTouchUpInside];
    formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:@"yyyy-MM-dd\nHH:mm:ss"];
    [self resetText];

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    NSString *uuid_string = [[NSUserDefaults standardUserDefaults] objectForKey:UUIDSTR_HEALTH_THERMOMETER_SERVICE];
    if (uuid_string) {
        NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:uuid_string];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            //[scan connectDeviceWithIdentifier:uuid];
        });
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dismiss {
    [self dismissViewControllerAnimated:YES completion:^{
        NSLog(@"dismissViewControllerAnimated");
        [scan release];
    }];
}

- (void)scan {
    if ([[scan_but titleForState:UIControlStateNormal] isEqualToString:@"Scan"]) {
        [self.navigationController pushViewController:scan animated:YES];
    }
    if (connect_peripheral) {
        [scan disconnectDevice:connect_peripheral];
        connect_peripheral = nil;
        connect_icon.alpha = 0.5;
        connect_icon.image = [UIImage imageNamed:@"thermometer_white.png"];
        [scan_but setTitle:@"Scan" forState:UIControlStateNormal];
    }
}

- (void)resetText {
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"temperatureUnit"] == 0) {
        unit.text = @"℃";
    }
    else {
        unit.text = @"℉";
    }
    value.text = @"0";
    time.text = @"";
    sensorLocation.text = @"";
    intermediate.text = @"";
}

- (void)showGraph:(id)sender {
    ISHTGraphController *view = [[ISHTGraphController alloc] init];
    [self.navigationController pushViewController:view animated:YES];
}

- (void)showSetting:(id)sender {
    ISHTSettingController *view = [[ISHTSettingController alloc] initWithStyle:UITableViewStyleGrouped];
    view.peripheral = connect_peripheral;
    [self.navigationController pushViewController:view animated:YES];
}

- (void)didUpdatePeripheralList:(NSArray *)peripherals {
    
}

- (void)didConnectPeripheral:(HKMyPeripheral *)peripheral {
    if (peripheral.connectStaus == HK_MYPERIPHERAL_CONNECT_STATUS_CONNECTED) {
        connect_icon.alpha = 1.0f;
        connect_icon.image = [UIImage imageNamed:@"thermometer_red.png"];
        [scan_but setTitle:@"Disconnect" forState:UIControlStateNormal];
    }
    else {
        [scan_but setTitle:@"Scan" forState:UIControlStateNormal];
        connect_icon.alpha = 0.5f;
        connect_icon.image = [UIImage imageNamed:@"thermometer_white.png"];
    }
    connect_peripheral = peripheral;
    peripheral.transDataDelegate = self;
    [self resetText];
}

- (void)didDisconnectPeripheral:(HKMyPeripheral *)peripheral {
    [scan_but setTitle:@"Scan" forState:UIControlStateNormal];
    connect_icon.alpha = 0.5f;
    connect_icon.image = [UIImage imageNamed:@"thermometer_white.png"];
    peripheral.transDataDelegate = nil;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)MyPeripheral:(HKMyPeripheral *)peripheral didUpdateTemperatureData:(NSDictionary *)data isIntermediate:(BOOL)isIntermediate error:(NSError *)error {
    if (isIntermediate) {
        intermediate.text = [NSString stringWithFormat:@"%.2f %@",[data[@"temperature"] floatValue],data[@"uint"]];
    }
    else {
        unit.text = data[@"uint"];
        value.text = [NSString stringWithFormat:@"%.2f",[data[@"temperature"] floatValue]];
        time.text = [formater stringFromDate:data[@"timestamp"]];
#ifdef USE_HEALTH_KIT
        AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
        if ([appDelegate isHealthKitAvailable]) {
            HKUnit *Unit;
            if ([data[@"uint"] isEqualToString:@"℃"]) {
                Unit = [HKUnit degreeCelsiusUnit];
            }
            else {
                Unit = [HKUnit degreeFahrenheitUnit];
            }
            HKQuantity *quantity = [HKQuantity quantityWithUnit:Unit doubleValue:[data[@"temperature"] doubleValue]];
            HKQuantityType *type = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierBodyTemperature];
            NSDate *now = data[@"timestamp"];
            
            HKQuantitySample *sample = [HKQuantitySample quantitySampleWithType:type quantity:quantity startDate:now endDate:now];
            [appDelegate.healthStore saveObject:sample withCompletion:^(BOOL success, NSError *error) {
                if (!success) {
                    NSLog(@"An error occured saving the temperature sample %@. In your app, try to handle this gracefully. The error was: %@.", sample, error);
                }
            }];
        }
#endif
    }
}

- (void)MyPeripheral:(HKMyPeripheral *)peripheral didUpdateTemperatureType:(TemperatureType)type error:(NSError *)error {
    switch (type) {
        case TemperatureType_Other:
            sensorLocation.text = @"Temperature Type\nOther";
            break;
        case TemperatureType_Armpit:
            sensorLocation.text = @"Temperature Type\nArmpit";
            break;
        case TemperatureType_Body:
            sensorLocation.text = @"Temperature Type\nBody";
            break;
        case TemperatureType_Ear:
            sensorLocation.text = @"Temperature Type\nEar";
            break;
        case TemperatureType_Finger:
            sensorLocation.text = @"Temperature Type\nFinger";
            break;
        case TemperatureType_GastroIntestinalTract:
            sensorLocation.text = @"Temperature Type\nGastro-intestinal Tract";
            break;
        case TemperatureType_Mouth:
            sensorLocation.text = @"Temperature Type\nMouth";
            break;
        case TemperatureType_Rectum:
            sensorLocation.text = @"Temperature Type\nRectum";
            break;
        case TemperatureType_Toe:
            sensorLocation.text = @"Temperature Type\nToe";
            break;
        case TemperatureType_Tympanum:
            sensorLocation.text = @"Temperature Type\nTympanum";
            break;
         default:
            sensorLocation.text = @"Temperature Type\nOther";
            break;
    }
}

- (void)MyPeripheral:(HKMyPeripheral *)peripheral didUpdateMeasurementInterval:(NSInteger)interval error:(NSError *)error {
    [[NSUserDefaults standardUserDefaults] setObject:@(interval) forKey:@"temperatureInterval"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
