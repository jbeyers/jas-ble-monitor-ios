//
//  ISHTCell.h
//  Health
//
//  Created by Rick on 2014/12/1.
//  Copyright (c) 2014年 Rick. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISHTCell : UITableViewCell
@property (strong) UILabel *value;
@property (strong) UILabel *temperatureType;
@property (strong) UILabel *time;
@end
