//
//  ISHTSettingController.h
//  Health
//
//  Created by Rick on 2014/12/1.
//  Copyright (c) 2014年 Rick. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HKMyPeripheral;

@interface ISHTSettingController : UITableViewController
@property (nonatomic,strong)HKMyPeripheral *peripheral;

@end
