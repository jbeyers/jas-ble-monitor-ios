//
//  HKMyPeripheral.h
//  Health
//
//  Created by ILYAS on 27/01/16.
//  Copyright © 2016 Rick. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "ReliableBurstData.h"

#define AIR_PATCH_COMMAND_VENDOR_MP_ENABLE      0x03
#define AIR_PATCH_COMMAND_XMEMOTY_READ          0x08
#define AIR_PATCH_COMMAND_XMEMOTY_WRITE         0x09
#define AIR_PATCH_COMMAND_E2PROM_READ           0x0a
#define AIR_PATCH_COMMAND_E2PROM_WRITE          0x0b
#define AIR_PATCH_COMMAND_READ                  0x24

#define AIR_PATCH_SUCCESS   0x00

#define AIR_PATCH_ACTION_CHANGE_DEVICE_NAME     0X01
#define AIR_PATCH_ACTION_READ_ADVERTISE_DATA1   0X02
#define AIR_PATCH_ACTION_READ_ADVERTISE_DATA2   0X03
#define AIR_PATCH_ACTION_UPDATE_ADVERTISE_DATA  0X04
#define AIR_PATCH_ACTION_CHANGE_DEVICE_NAME_MEMORY     0X05
#define AIR_PATCH_ACTION_READ                   0X24

#define ADVERTISE_DATA_TYPE_COMPLETE_DEVICE_NAME 0X09
#define ADVERTISE_DATA_TYPE_SHORTEN_DEVICE_NAME  0X08

enum {
    HK_MYPERIPHERAL_CONNECT_STATUS_IDLE = 0,
    HK_MYPERIPHERAL_CONNECT_STATUS_CONNECTING,
    HK_MYPERIPHERAL_CONNECT_STATUS_CONNECTED,
};

enum {
    HK_UPDATE_PARAMETERS_STEP_PREPARE = 0,
    HK_UPDATE_PARAMETERS_STEP_CHECK_RESULT,
};

typedef NS_ENUM(NSUInteger, SensorLocation) {
    SensorLocation_Other,
    SensorLocation_Chest,
    SensorLocation_Wrist,
    SensorLocation_Finger,
    SensorLocation_Hand,
    SensorLocation_EarLobe,
    SensorLocation_Foot
};

typedef NS_ENUM(NSUInteger, TemperatureType) {
    TemperatureType_Other,
    TemperatureType_Armpit,
    TemperatureType_Body,
    TemperatureType_Ear,
    TemperatureType_Finger,
    TemperatureType_GastroIntestinalTract,
    TemperatureType_Mouth,
    TemperatureType_Rectum,
    TemperatureType_Toe,
    TemperatureType_Tympanum
};

typedef NS_ENUM(NSUInteger, GlucoseSensorLocation) {
    GlucoseSensorLocation_Reserved,
    GlucoseSensorLocation_Finger,
    GlucoseSensorLocation_AST,
    GlucoseSensorLocation_Earlobe,
    GlucoseSensorLocation_ControlSolution,
    GlucoseSensorLocation_NotAvailable = 15,
};

typedef NS_ENUM(NSUInteger, GlucoseType) {
    GlucoseType_Reserved,
    GlucoseType_CapillaryWholeBlood,
    GlucoseType_CapillaryPlasma,
    GlucoseType_VenousWholeBlood,
    GlucoseType_VenousPlasma,
    GlucoseType_ArterialWholeBlood,
    GlucoseType_ArterialPlasma,
    GlucoseType_UndeterminedWholeBlood,
    GlucoseType_UndeterminedPlasma,
    GlucoseType_ISF,
    GlucoseType_ControlSolution
};

typedef NS_ENUM(uint8_t, GlucoseOpCode) {
    GlucoseOpCode_NA,
    GlucoseOpCode_ReportStoredRecords,
    GlucoseOpCode_DeleteStoredRecords,
    GlucoseOpCode_AbortOperation,
    GlucoseOpCode_ReportNumber_of_StoredRecords,
    GlucoseOpCode_Number_of_StoredRecords,
    GlucoseOpCode_ResponseCode
};

typedef NS_ENUM(uint8_t, GlucoseOperator) {
    GlucoseOperator_NA,
    GlucoseOperator_AllRecords,
    GlucoseOperator_Less_or_Equal,
    GlucoseOperator_Greater_or_Equal,
    GlucoseOperator_WithinRange,
    GlucoseOperator_FirstRecord,
    GlucoseOperator_LastRecord
};

typedef NS_ENUM(uint8_t, GlucoseResponseCode ) {
    GlucoseResponseCode_NA,
    GlucoseResponseCode_Success,
    GlucoseResponseCode_OpCodeNotSupported,
    GlucoseResponseCode_InvalidOperator,
    GlucoseResponseCode_OperatorNotSupported,
    GlucoseResponseCode_InvalidOperand,
    GlucoseResponseCode_NoRecordsFound,
    GlucoseResponseCode_AbortUnsuccessful,
    GlucoseResponseCode_ProcedureNotCompleted,
    GlucoseResponseCode_OperandNotSupported
};

typedef struct HK_AIR_PATCH_COMMAND_FORMAT
{
    unsigned char commandID;
    char parameters[19];
}__attribute__((packed)) _AIR_PATCH_COMMAND_FORMAT;

typedef struct HK_AIR_PATCH_EVENT_FORMAT
{
    char    status;
    unsigned char commandID;
    char parameters[16];
}__attribute__((packed)) _AIR_PATCH_EVENT_FORMAT;


typedef struct HK_WRITE_EEPROM_COMMAND_FORMAT
{
    unsigned char addr[2];
    unsigned char length;
    char    data[16];
}__attribute__((packed)) _WRITE_EEPROM_COMMAND_FORMAT;

typedef struct HK_CONNECTION_PARAMETER_FORMAT
{
    unsigned char status;
    unsigned short minInterval;
    unsigned short maxInterval;
    unsigned short latency;
    unsigned short connectionTimeout;
}__attribute__((packed)) _CONNECTION_PARAMETER_FORMAT;


@protocol MyPeripheralDelegate;
@interface HKMyPeripheral : NSObject
{
@private
    char    advData[25];
    char    deviceName[16];
    NSMutableArray *queuedTask;
}
@property(assign) id<MyPeripheralDelegate> transDataDelegate;
@property(assign) id<MyPeripheralDelegate> proprietaryDelegate;
@property(assign) id<MyPeripheralDelegate> deviceInfoDelegate;

@property (retain) CBPeripheral *peripheral;
@property(copy) NSString *advName;
@property(assign) uint8_t connectStaus;
@property (assign) BOOL canSendData;

//DIS
@property(retain) CBCharacteristic *manufactureNameChar;
@property(retain) CBCharacteristic *modelNumberChar;
@property(retain) CBCharacteristic *serialNumberChar;
@property(retain) CBCharacteristic *hardwareRevisionChar;
@property(retain) CBCharacteristic *firmwareRevisionChar;
@property(retain) CBCharacteristic *softwareRevisionChar;
@property(retain) CBCharacteristic *systemIDChar;
@property(retain) CBCharacteristic *certDataListChar;
@property(retain) CBCharacteristic *specificChar1;
@property(retain) CBCharacteristic *specificChar2;

//Proprietary
@property(retain) CBCharacteristic *airPatchChar;
@property(retain) CBCharacteristic *transparentDataWriteChar;
@property(retain) CBCharacteristic *transparentDataReadChar;
@property(retain) CBCharacteristic *connectionParameterChar;
@property(assign) uint8_t   updateConnectionParameterStep;
@property(readonly) ReliableBurstData *transmit;

@property(assign) _CONNECTION_PARAMETER_FORMAT connectionParameter;
@property(assign) _CONNECTION_PARAMETER_FORMAT backupConnectionParameter;


@property(assign) BOOL    vendorMPEnable;
@property(assign) short   airPatchAction;
@property(assign) BOOL    isNotifying;

@property(retain) CBCharacteristic *heartRateControlPoint;

@property(retain) CBCharacteristic *healthManufatureInterval;

@property(retain) CBCharacteristic *glucoseRecordAccessControlPoint;

- (_CONNECTION_PARAMETER_FORMAT *)retrieveBackupConnectionParameter;
- (void)updateBackupConnectionParameter:(_CONNECTION_PARAMETER_FORMAT *)parameter;
- (BOOL)compareBackupConnectionParameter:(_CONNECTION_PARAMETER_FORMAT *)parameter;

- (void)checkConnectionParameterStatus;
- (void)sendVendorMPEnable;
- (void)updateAirPatchEvent: (NSData *)returnEvent;
- (void)writeE2promValue: (short)address length:(short)length data:(char *)data;
- (void)readE2promValue: (short)address length:(short)length;
- (void)writeMemoryValue: (short)address length:(short)length data:(char *)data;
- (void)readMemoryValue: (short)address length:(short)length;
- (CBCharacteristicWriteType)sendTransparentData:(NSData *)data type:(CBCharacteristicWriteType)type;
- (void)setTransDataNotification:(BOOL)notify;

- (NSError *)setMaxConnectionInterval:(unsigned short)maxInterval connectionTimeout:(unsigned short)connectionTimeout connectionLatency:(unsigned short)connectionLatency;
- (void)checkIsAllowUpdateConnectionParameter;
- (void)changePeripheralName: (NSString *)name;

- (void)readManufactureName;
- (void)readModelNumber;
- (void)readSerialNumber;
- (void)readHardwareRevision;
- (void)readFirmwareRevision;
- (void)readSoftwareRevison;
- (void)readSystemID;
- (void)readCertificationData;
- (void)readSpecificUUID1;
- (void)readSpecificUUID2;

- (void)setWeihgtScaleFeature:(NSData *)data;
- (void)saveWeihgtScaleData:(NSData *)data;

- (void)saveBatteryLevel:(NSInteger)level;

- (void)setSensorLocation:(NSData *)data;
- (void)saveHeartRateData:(NSData *)data;
- (void)resetEnergyExpended;

- (void)setTemperatureType:(NSData *)data;
- (void)saveTemperatureData:(NSData *)data isIntermediate:(BOOL)isIntermediate;
- (void)setMeasurementInterval:(NSInteger)interval;
- (void)updateMeasurementInterval:(NSInteger)interval;

- (void)setBloodPressureFeature:(NSData *)data;
- (void)saveBloodPressureData:(NSData *)data isIntermediateCuffPressure:(BOOL)isIntermediate;

- (void)setGlucoseFeature:(NSData *)data;
- (void)saveGlucoseData:(NSData *)data;
- (void)saveGlucoseContextData:(NSData *)data;
- (void)executeGlucoseOp:(GlucoseOpCode)op operator:(GlucoseOperator)Operator andOperand:(NSArray *)operand;
- (void)updateGlucoseOpResponse:(NSData *)data;

@end


@protocol MyPeripheralDelegate<NSObject>
@optional
- (void)MyPeripheral:(HKMyPeripheral *)peripheral didUpdateConnectionParameterAllowStatus:(BOOL)status;
- (void)MyPeripheral:(HKMyPeripheral *)peripheral didUpdateConnectionParameterStatus:(BOOL)status interval:(unsigned short)interval timeout:(unsigned short)timeout  latency:(unsigned short)latency;

- (void)MyPeripheral:(HKMyPeripheral *)peripheral didChangePeripheralName:(NSError *)error;

- (void)MyPeripheral:(HKMyPeripheral *)peripheral didReceiveTransparentData:(NSData *)data;
- (void)MyPeripheral:(HKMyPeripheral *)peripheral didReceiveMemoryAddress:(NSData *)address length:(short)length data:(NSData *)data;
- (void)MyPeripheral:(HKMyPeripheral *)peripheral didWriteMemoryAddress:(NSError *)error;
- (void)MyPeripheral:(HKMyPeripheral *)peripheral didSendTransparentDataStatus:(NSError *) error;
- (void)MyPeripheral:(HKMyPeripheral *)peripheral didUpdateTransDataNotifyStatus:(BOOL)notify;

- (void)MyPeripheral:(HKMyPeripheral *)peripheral didUpdateManufactureName:(NSString *)name error:(NSError *)error;
- (void)MyPeripheral:(HKMyPeripheral *)peripheral didUpdateModelNumber:(NSString *)modelNumber error:(NSError *)error;

- (void)MyPeripheral:(HKMyPeripheral *)peripheral didUpdateSerialNumber:(NSString *)serialNumber error:(NSError *)error;
- (void)MyPeripheral:(HKMyPeripheral *)peripheral didUpdateHardwareRevision:(NSString *)hardwareRevision error:(NSError *)error;
- (void)MyPeripheral:(HKMyPeripheral *)peripheral didUpdateFirmwareRevision:(NSString *)firmwareRevision error:(NSError *)error;
- (void)MyPeripheral:(HKMyPeripheral *)peripheral didUpdateSoftwareRevision:(NSString *)softwareRevision error:(NSError *)error;
- (void)MyPeripheral:(HKMyPeripheral *)peripheral didUpdateSystemId:(NSData *)systemId error:(NSError *)error;
- (void)MyPeripheral:(HKMyPeripheral *)peripheral didUpdateIEEE_11073_20601:(NSData *)IEEE_11073_20601 error:(NSError *)error;
- (void)MyPeripheral:(HKMyPeripheral *)peripheral didUpdateSpecificUUID1:(NSData *)value error:(NSError *)error;
- (void)MyPeripheral:(HKMyPeripheral *)peripheral didUpdateSpecificUUID2:(NSData *)value error:(NSError *)error;

// Weight Scale
- (void)MyPeripheral:(HKMyPeripheral *)peripheral didUpdateWeihgtScaleData:(NSDictionary *)data error:(NSError *)error;
- (void)MyPeripheral:(HKMyPeripheral *)peripheral didUpdateBatteryLevel:(NSInteger)level error:(NSError *)error;

// Heart Rate
- (void)MyPeripheral:(HKMyPeripheral *)peripheral didUpdateHeartRateData:(NSDictionary *)data error:(NSError *)error;
- (void)MyPeripheral:(HKMyPeripheral *)peripheral didUpdateSensorLocation:(SensorLocation)location error:(NSError *)error;

//Health Thermometer
- (void)MyPeripheral:(HKMyPeripheral *)peripheral didUpdateTemperatureData:(NSDictionary *)data isIntermediate:(BOOL)isIntermediate error:(NSError *)error;
- (void)MyPeripheral:(HKMyPeripheral *)peripheral didUpdateTemperatureType:(TemperatureType)type error:(NSError *)error;
- (void)MyPeripheral:(HKMyPeripheral *)peripheral didUpdateMeasurementInterval:(NSInteger)interval error:(NSError *)error;

//Blood Pressure
- (void)MyPeripheral:(HKMyPeripheral *)peripheral didUpdateBloodPressureData:(NSDictionary *)data isIntermediateCuffPressure:(BOOL)isIntermediate error:(NSError *)error;

//Glucose
- (void)MyPeripheral:(HKMyPeripheral *)peripheral didUpdateGlucoseData:(NSDictionary *)data error:(NSError *)error;
- (void)MyPeripheral:(HKMyPeripheral *)peripheral didUpdateGlucoseContextData:(NSDictionary *)data error:(NSError *)error;
- (void)MyPeripheral:(HKMyPeripheral *)peripheral didUpdateGlucoseDataNumber:(NSInteger)number error:(NSError *)error;
- (void)MyPeripheral:(HKMyPeripheral *)peripheral didExecuteGlucoseOp:(GlucoseOpCode)op error:(NSError *)error;

@end
