//
//  ISMenuViewController.m
//  Weight Scale
//
//  Created by Rick on 2014/10/30.
//  Copyright (c) 2014年 Rick. All rights reserved.
//

#import "ISMenuViewController.h"
#import "ISWeightController.h"
#import "ISHeartRateController.h"
#import "ISHTController.h"
#import "ISBloodPressureController.h"
#import "ISGlucoseController.h"
@interface ISMenuViewController ()

@end

@implementation ISMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIGraphicsBeginImageContext(self.view.frame.size);
    float base = 90.0f;
    float dist = 185.0f;
    float inX = 35.0f;
    if ([[UIScreen mainScreen] bounds].size.height == 480.0f) {
        base = 35.0f;
    }
    
    if ([[UIScreen mainScreen] bounds].size.width > 320.0f) {
        dist = 230.0f;
        inX = 45.0f;
    }
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 100, 50)];
    UIImage *img = [UIImage imageNamed:@"microchiplogo.png"];
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 100, 50)];
    imgView.image = img;
    [view addSubview:imgView];
    self.navigationItem.titleView = view;
    [imgView release];
    [view release];
    
    [[UIImage imageNamed:@"iPad_iSSC_background.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    UIButton *weight = [UIButton buttonWithType:UIButtonTypeCustom];
    weight.frame = CGRectMake(inX, base, 100.0f, 100.0f);
    weight.backgroundColor = [UIColor colorWithRed:0.15 green:0.74 blue:0.62 alpha:1.0];
    //weight.layer.borderWidth = 2.0f;
    weight.layer.cornerRadius = 15.0f;
    weight.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
    weight.layer.shadowColor = [UIColor grayColor].CGColor;
    weight.layer.shadowOpacity = 0.5f;
    [weight setImageEdgeInsets:UIEdgeInsetsMake(5, 15, 30, 15)];
    [weight setTitleEdgeInsets:UIEdgeInsetsMake(60, -425, 0, 0)];
    [weight setTitle:@"Weight Scale" forState:UIControlStateNormal];
    //[weight.titleLabel setTextColor:[UIColor whiteColor]];
    [weight setImage:[UIImage imageNamed:@"ic_scale.png"] forState:UIControlStateNormal];
    [self.view addSubview:weight];
    [weight addTarget:self action:@selector(popWeightPage) forControlEvents:UIControlEventTouchUpInside];
    weight.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    
    UIButton *heart_rate = [UIButton buttonWithType:UIButtonTypeCustom];
    heart_rate.frame = CGRectMake(dist, base, 100.0f, 100.0f);
    heart_rate.backgroundColor = [UIColor colorWithRed:1.000 green:0.435 blue:0.812 alpha:1.000];
    heart_rate.layer.cornerRadius = 15.0f;
    heart_rate.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
    heart_rate.layer.shadowColor = [UIColor grayColor].CGColor;
    heart_rate.layer.shadowOpacity = 0.5f;
    [heart_rate setImageEdgeInsets:UIEdgeInsetsMake(5, 25, 40, 25)];
    [heart_rate setTitleEdgeInsets:UIEdgeInsetsMake(60, -125, 0, 0)];
    [heart_rate setTitle:@"Heart Rate" forState:UIControlStateNormal];
    //[heart_rate.titleLabel setTextColor:[UIColor whiteColor]];
    [heart_rate setImage:[UIImage imageNamed:@"heart-white.png"] forState:UIControlStateNormal];
    [self.view addSubview:heart_rate];
    [heart_rate addTarget:self action:@selector(popHeartRatePage) forControlEvents:UIControlEventTouchUpInside];
    heart_rate.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    
    UIButton *thermometer = [UIButton buttonWithType:UIButtonTypeCustom];
    thermometer.frame = CGRectMake(inX, base + 145.0f, 100.0f, 100.0f);
    thermometer.backgroundColor = [UIColor colorWithRed:0.251 green:0.646 blue:0.820 alpha:1.000];
    thermometer.layer.cornerRadius = 15.0f;
    thermometer.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
    thermometer.layer.shadowColor = [UIColor grayColor].CGColor;
    thermometer.layer.shadowOpacity = 0.5f;
    [thermometer setImageEdgeInsets:UIEdgeInsetsMake(5, 25, 40, 25)];
    [thermometer setTitleEdgeInsets:UIEdgeInsetsMake(60, -130, 0, 0)];
    [thermometer setTitle:@"Thermometer" forState:UIControlStateNormal];
    //[thermometer.titleLabel setTextColor:[UIColor whiteColor]];
    [thermometer setImage:[UIImage imageNamed:@"thermometer_white.png"] forState:UIControlStateNormal];
    [self.view addSubview:thermometer];
    [thermometer addTarget:self action:@selector(popHealthThermometerPage) forControlEvents:UIControlEventTouchUpInside];
    thermometer.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    
    UIButton *blood_pressure = [UIButton buttonWithType:UIButtonTypeCustom];
    blood_pressure.frame = CGRectMake(dist, base + 145.0f, 100.0f, 100.0f);
    blood_pressure.backgroundColor = [UIColor colorWithRed:0.820 green:0.534 blue:0.193 alpha:1.000];
    blood_pressure.layer.cornerRadius = 15.0f;
    blood_pressure.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
    blood_pressure.layer.shadowColor = [UIColor grayColor].CGColor;
    blood_pressure.layer.shadowOpacity = 0.5f;
    [blood_pressure setImageEdgeInsets:UIEdgeInsetsMake(5, 25, 40, 25)];
    [blood_pressure setTitleEdgeInsets:UIEdgeInsetsMake(60, -125, 0, 0)];
    [blood_pressure setTitle:@"BP" forState:UIControlStateNormal];
    //[blood_pressure.titleLabel setTextColor:[UIColor whiteColor]];
    [blood_pressure setImage:[UIImage imageNamed:@"blood_pressure"] forState:UIControlStateNormal];
    [self.view addSubview:blood_pressure];
    [blood_pressure addTarget:self action:@selector(popBloodPressurePage) forControlEvents:UIControlEventTouchUpInside];
    blood_pressure.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    
    UIButton *glucose = [UIButton buttonWithType:UIButtonTypeCustom];
    glucose.frame = CGRectMake(inX, base + 290.0f, 100.0f, 100.0f);
    glucose.backgroundColor = [UIColor purpleColor];
    glucose.layer.cornerRadius = 15.0f;
    glucose.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
    glucose.layer.shadowColor = [UIColor grayColor].CGColor;
    glucose.layer.shadowOpacity = 0.5f;
    [glucose setImageEdgeInsets:UIEdgeInsetsMake(5, 25, 40, 25)];
    [glucose setTitleEdgeInsets:UIEdgeInsetsMake(60, -125, 0, 0)];
    [glucose setTitle:@"Glucose" forState:UIControlStateNormal];
    glucose.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    //[glucose.titleLabel setTextColor:[UIColor whiteColor]];
    [glucose setImage:[UIImage imageNamed:@"blood"] forState:UIControlStateNormal];
    [self.view addSubview:glucose];
    [glucose addTarget:self action:@selector(popGlucosePage) forControlEvents:UIControlEventTouchUpInside];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)popWeightPage {
    ISWeightController *controller = [[ISWeightController alloc] initWithNibName:nil bundle:nil];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:controller];
    nav.navigationBar.tintColor = [UIColor redColor];
    [self presentViewController:nav animated:YES completion:nil];
    [controller release];
}

- (void)popHeartRatePage {
    ISHeartRateController *controller = [[ISHeartRateController alloc] initWithNibName:nil bundle:nil];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:controller];
    nav.navigationBar.tintColor = [UIColor redColor];
    [self presentViewController:nav animated:YES completion:nil];
    [controller release];
}

- (void)popHealthThermometerPage {
    ISHTController *controller = [[ISHTController alloc] initWithNibName:nil bundle:nil];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:controller];
    nav.navigationBar.tintColor = [UIColor redColor];
    [self presentViewController:nav animated:YES completion:nil];
    [controller release];
}

- (void)popBloodPressurePage {
    ISBloodPressureController *controller = [[ISBloodPressureController alloc] initWithNibName:nil bundle:nil];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:controller];
    nav.navigationBar.tintColor = [UIColor redColor];
    [self presentViewController:nav animated:YES completion:nil];
    [controller release];
}

- (void)popGlucosePage {
    ISGlucoseController *controller = [[ISGlucoseController alloc] initWithNibName:nil bundle:nil];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:controller];
    nav.navigationBar.tintColor = [UIColor redColor];
    [self presentViewController:nav animated:YES completion:nil];
    [controller release];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
