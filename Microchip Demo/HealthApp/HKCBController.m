//
//  HKCBController.m
//  Health
//
//  Created by ILYAS on 27/01/16.
//  Copyright © 2016 Rick. All rights reserved.
//

#import "HKCBController.h"
#import "HKMyPeripheral.h"
#import "ISWeightParser.h"


@interface HKCBController() {
    CBCharacteristic *weightDataCharacteristic;
    CBCharacteristic *batteryCharacteristic;
    CBCharacteristic *heartRateMeasurement;
    CBCharacteristic *heartRateControlPoint;
    CBCharacteristic *healthManufatureInterval;
    CBCharacteristic *bloodPressureMeasurement;
    CBCharacteristic *intermediateCuffPressure;
    CBCharacteristic *glucoseMeasurement;
    CBCharacteristic *glucoseMeasurementContext;
    
}
@end
@implementation HKCBController
@synthesize delegate;
@synthesize devicesList;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
            self.edgesForExtendedLayout = UIRectEdgeNone;
        }
        devicesList = [[NSMutableArray alloc] init];
        _connectedPeripheralList = [[NSMutableArray alloc] init];
        _transServiceUUID = nil;
        _transTxUUID = nil;
        _transRxUUID = nil;
        _disUUID1 = nil;
        _disUUID2 = nil;
        manager = [CBCentralManager alloc];
        if ([manager respondsToSelector:@selector(initWithDelegate:queue:options:)]) {
            
            NSArray *vComp = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
            if ([[vComp objectAtIndex:0] intValue] >= 10) {
                manager = [[CBCentralManager alloc] initWithDelegate:self queue:dispatch_get_main_queue()];
                [manager scanForPeripheralsWithServices:nil options:@{CBCentralManagerScanOptionAllowDuplicatesKey: @YES}];
            }
            else { // iOS version below 10.0
                manager = [manager initWithDelegate:self queue:dispatch_get_main_queue() options:@{CBCentralManagerOptionRestoreIdentifierKey: ISSC_RestoreIdentifierKey}];
            }

        }
        else {
            manager = [manager initWithDelegate:self queue:nil];
        }
        
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //manager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (void)dealloc {
    NSLog(@"CBController dealloc");
    for (HKMyPeripheral *p in devicesList) {
        [self disconnectDevice:p];
    }
    [manager release];
}

- (void) startScan
{
    NSLog(@"[CBController] start scan");
    [manager scanForPeripheralsWithServices:/*@[[CBUUID UUIDWithString:UUIDSTR_WEIGHT_SCALE_SERVICE]]*/nil options:nil];
    [devicesList removeAllObjects];
    
}

- (void) startScanWithUUID:(NSArray *)uuids
{
    
    _waitingBtPowerOnToScan = NO;
    if ([self isLECapableHardware] == NO) {
        _waitingBtPowerOnToScan = YES;
    }
    else {
        NSLog(@"[CBController] start scan with uuid, %@", uuids);
        [manager scanForPeripheralsWithServices:uuids options:nil];
        [devicesList removeAllObjects];
    }
}

/*
 Request CBCentralManager to stop scanning for heart rate peripherals
 */
- (void) stopScan
{
    NSLog(@"[CBController] stop scan");
    [manager stopScan];
}

- (NSMutableData *) hexStrToData: (NSString *)hexStr
{
    NSMutableData *data= [[NSMutableData alloc] init];
    NSUInteger len = [hexStr length];
    
    unsigned char whole_byte;
    char byte_chars[3] = {'\0','\0','\0'};
    int i;
    for (i=0; i < len/2; i++) {
        byte_chars[0] = [hexStr characterAtIndex:i*2];
        byte_chars[1] = [hexStr characterAtIndex:i*2+1];
        whole_byte = strtol(byte_chars, NULL, 16);
        [data appendBytes:&whole_byte length:1];
    }
    return data;
}

- (void)connectDevice:(HKMyPeripheral *) myPeripheral{
    NSLog(@"[CBController] connectDevice: %@", myPeripheral.advName);
    //if (myPeripheral.connectStaus == MYPERIPHERAL_CONNECT_STATUS_IDLE)
    [manager connectPeripheral:myPeripheral.peripheral options:nil];  //connect to device
    //else if (myPeripheral.connectStaus == MYPERIPHERAL_CONNECT_STATUS_CONNECTED)
    //    [self centralManager:manager didConnectPeripheral:myPeripheral.peripheral];
}

- (void)connectDeviceWithIdentifier:(NSUUID *)identifier {
    NSArray *array = [manager retrievePeripheralsWithIdentifiers:@[identifier]];
    if ([array count]>0) {
        CBPeripheral *peripheral = array[0];
        HKMyPeripheral *myPeripheral = [self retrieveMyPeripheral:peripheral];
        if (!myPeripheral) {
            [self storeMyPeripheral:peripheral];
            myPeripheral = [self retrieveMyPeripheral:peripheral];
        }
        [self connectDevice:myPeripheral];
    }
}

- (void)disconnectDevice: (HKMyPeripheral *)myPeripheral {
    NSLog(@"[CBController] disconnectDevice");
    myPeripheral.canSendData = NO;
    [myPeripheral setTransDataNotification:NO];
    if (batteryCharacteristic) {
        [myPeripheral.peripheral setNotifyValue:NO forCharacteristic:batteryCharacteristic];
        batteryCharacteristic = nil;
    }
    if (weightDataCharacteristic) {
        //[myPeripheral.peripheral setNotifyValue:NO forCharacteristic:weightDataCharacteristic];
        weightDataCharacteristic = nil;
    }
    
    /*if ([myPeripheral.transmit canDisconnect]) {
     [manager cancelPeripheralConnection: myPeripheral.peripheral];
     }
     else {*/
    dispatch_async(dispatch_queue_create("temp", NULL), ^{
        NSLog(@"[CBController] disconnectDevice : Wait for data clear");
        int count = 0;
        while (![myPeripheral.transmit canDisconnect] || myPeripheral.isNotifying) {
            //[NSThread sleepForTimeInterval:0.1];
            sleep(1);
            count++;
            if (count >= 10) {
                break;
            }
        }
        //dispatch_async(dispatch_get_main_queue(), ^{
        [manager cancelPeripheralConnection: myPeripheral.peripheral];
        //});
    });
    //}
}

- (void)updateDiscoverPeripherals {
    
}

- (void)updateMyPeripheralForNewConnected:(HKMyPeripheral *)myPeripheral {
    
}

- (void)updateMyPeripheralForDisconnect:(HKMyPeripheral *)myPeripheral {
    if (delegate && [delegate respondsToSelector:@selector(didDisconnectPeripheral:)]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [delegate didDisconnectPeripheral:myPeripheral];
        });
    }
}

- (void)addDiscoverPeripheral:(CBPeripheral *)aPeripheral advName:(NSString *)advName{
    HKMyPeripheral *myPeripheral = nil;
    for (uint8_t i = 0; i < [devicesList count]; i++) {
        myPeripheral = [devicesList objectAtIndex:i];
        if (myPeripheral.peripheral == aPeripheral) {
            myPeripheral.advName = advName;
            break;
        }
        myPeripheral = nil;
    }
    if (myPeripheral == nil) {
        myPeripheral = [[HKMyPeripheral alloc] init];
        myPeripheral.peripheral = aPeripheral;
        myPeripheral.advName = advName;
        [devicesList addObject:myPeripheral];
    }
    //NSLog(@"[CBController] deviceList count = %d", [devicesList count]);
    [self updateDiscoverPeripherals];
}

- (void)storeMyPeripheral: (CBPeripheral *)aPeripheral {
    HKMyPeripheral *myPeripheral = nil;
    bool b = FALSE;
    for (uint8_t i = 0; i < [devicesList count]; i++) {
        myPeripheral = [devicesList objectAtIndex:i];
        if (myPeripheral.peripheral == aPeripheral) {
            b = TRUE;
            //NSLog(@"storeMyPeripheral 1");
            break;
        }
    }
    if(!b) {
        //NSLog(@"storeMyPeripheral 2");
        myPeripheral = [[HKMyPeripheral alloc] init];
        myPeripheral.peripheral = aPeripheral;
    }
    if (myPeripheral.peripheral.state == CBPeripheralStateConnected) {
        myPeripheral.connectStaus = HK_MYPERIPHERAL_CONNECT_STATUS_CONNECTED;
    }
    else {
        myPeripheral.connectStaus = HK_MYPERIPHERAL_CONNECT_STATUS_IDLE;
    }
    [_connectedPeripheralList addObject:myPeripheral];
    if (delegate && [delegate respondsToSelector:@selector(didConnectPeripheral:)]) {
        [delegate didConnectPeripheral:myPeripheral];
    }
    
}

- (HKMyPeripheral *)retrieveMyPeripheral:(CBPeripheral *)aPeripheral {
    HKMyPeripheral *myPeripheral = nil;
    for (uint8_t i = 0; i < [_connectedPeripheralList count]; i++) {
        myPeripheral = [_connectedPeripheralList objectAtIndex:i];
        if (myPeripheral.peripheral == aPeripheral) {
            break;
        }
    }
    return myPeripheral;
}

- (void)removeMyPeripheral: (CBPeripheral *) aPeripheral {
    HKMyPeripheral *myPeripheral = nil;
    for (uint8_t i = 0; i < [_connectedPeripheralList count]; i++) {
        myPeripheral = [_connectedPeripheralList objectAtIndex:i];
        if (myPeripheral.peripheral == aPeripheral) {
            myPeripheral.connectStaus = HK_MYPERIPHERAL_CONNECT_STATUS_IDLE;
            [self updateMyPeripheralForDisconnect:myPeripheral];
            [_connectedPeripheralList removeObject:myPeripheral];
            return;
        }
    }
    for (uint8_t i = 0; i < [devicesList count]; i++) {
        myPeripheral = [devicesList objectAtIndex:i];
        if (myPeripheral.peripheral == aPeripheral) {
            myPeripheral.connectStaus = HK_MYPERIPHERAL_CONNECT_STATUS_IDLE;
            [self updateMyPeripheralForDisconnect:myPeripheral];
            break;
        }
    }
}

- (void)configureTransparentServiceUUID: (NSString *)serviceUUID txUUID:(NSString *)txUUID rxUUID:(NSString *)rxUUID {
    if (serviceUUID) {
        _transServiceUUID = [CBUUID UUIDWithString:serviceUUID];
        _transTxUUID = [CBUUID UUIDWithString:txUUID];
        _transRxUUID = [CBUUID UUIDWithString:rxUUID];
    }
    else {
        _transServiceUUID = nil;
        _transTxUUID = nil;
        _transRxUUID = nil;
    }
}

- (void)configureDeviceInformationServiceUUID:(NSString *)UUID1 UUID2:(NSString *)UUID2{
    if (UUID1 || UUID2) {
        if (UUID1 != nil) {
            _disUUID1 = [CBUUID UUIDWithString:UUID1];
        }
        else _disUUID1 = nil;
        
        if (UUID2 != nil) {
            _disUUID2 = [CBUUID UUIDWithString:UUID2];
        }
        else _disUUID2 = nil;
    }
    else {
        _disUUID1 = nil;
        _disUUID2 = nil;
    }
}

/*
 Uses CBCentralManager to check whether the current platform/hardware supports Bluetooth LE. An alert is raised if Bluetooth LE is not enabled or is not supported.
 */
- (BOOL) isLECapableHardware
{
    NSString * state = nil;
    
    switch ([manager state])
    {
        case CBCentralManagerStateUnsupported:
            state = @"The platform/hardware doesn't support Bluetooth Low Energy.";
            break;
        case CBCentralManagerStateUnauthorized:
            state = @"The app is not authorized to use Bluetooth Low Energy.";
            break;
        case CBCentralManagerStatePoweredOff:
            state = @"Bluetooth is currently powered off.";
            break;
        case CBCentralManagerStatePoweredOn:
            NSLog(@"Bluetooth power on");
            if (_waitingBtPowerOnToScan) {
                [self startScanWithUUID:nil];
            }
            return TRUE;
        case CBCentralManagerStateUnknown:
        default:
            return FALSE;
            
    }
    
    NSLog(@"Central manager state: %@", state);
    
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Bluetooth alert"  message:state delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles: nil];
    [alertView show];
    return FALSE;
}

#pragma mark - CBCentralManager delegate methods
/*
 Invoked whenever the central manager's state is updated.
 */
- (void) centralManagerDidUpdateState:(CBCentralManager *)central
{
    [self isLECapableHardware];
}

- (void) centralManager:(CBCentralManager *)central willRestoreState:(NSDictionary *)dict {
    NSLog(@"willRestoreState %@",[dict description]);
}

/*
 Invoked when the central discovers heart rate peripheral while scanning.
 */
- (void) centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)aPeripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    //NSLog(@"<---------\n[CBController] didDiscoverPeripheral, count=%u, RSSI=%d, count=%d", [advertisementData count], [RSSI intValue], [devicesList count]);
    NSArray *advDataArray = [advertisementData allValues];
    NSArray *advValueArray = [advertisementData allKeys];
    
    for (int i=0; i < [advertisementData count]; i++)
    {
        NSLog(@"adv data=%@, %@ ", [advDataArray objectAtIndex:i], [advValueArray objectAtIndex:i]);
    }
    NSLog(@"-------->");
    [self addDiscoverPeripheral:aPeripheral advName:[advertisementData valueForKey:CBAdvertisementDataLocalNameKey]];
}

/*
 Invoked when the central manager retrieves the list of known peripherals.
 Automatically connect to first known peripheral
 */
- (void)centralManager:(CBCentralManager *)central didRetrievePeripherals:(NSArray *)peripherals
{
    //NSLog(@"Retrieved peripheral: %u - %@", [peripherals count], peripherals);
    if([peripherals count] >=1)
    {
        [self connectDevice:[peripherals objectAtIndex:0]];
    }
}

/*
 Invoked whenever a connection is succesfully created with the peripheral.
 Discover available services on the peripheral
 */
- (void) centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)aPeripheral
{
    NSLog(@"[CBController] didConnectPeripheral");
    
    [aPeripheral setDelegate:self];
    
    [self storeMyPeripheral:aPeripheral];
    
    isISSCPeripheral = FALSE;
    /*NSMutableArray *uuids = [[NSMutableArray alloc] initWithObjects:[CBUUID UUIDWithString:UUIDSTR_DEVICE_INFO_SERVICE], [CBUUID UUIDWithString:UUIDSTR_ISSC_PROPRIETARY_SERVICE], nil];
     if (_transServiceUUID)
     [uuids addObject:_transServiceUUID];*/
    /*NSArray *uuids = @[[CBUUID UUIDWithString:UUIDSTR_DEVICE_INFO_SERVICE],[CBUUID UUIDWithString:UUIDSTR_BATTERY_SERVICE],[CBUUID UUIDWithString:UUIDSTR_WEIGHT_SCALE_SERVICE]];*/
    [aPeripheral discoverServices:_serviceUuids];
}

/*
 Invoked whenever an existing connection with the peripheral is torn down.
 Reset local variables
 */
- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)aPeripheral error:(NSError *)error
{
    //NSLog(@"[CBController] didDisonnectPeripheral, error msg:%d, %@, %@",error.code ,[error localizedFailureReason], [error localizedDescription]);
    
    [self removeMyPeripheral:aPeripheral];
}

/*
 Invoked whenever the central manager fails to create a connection with the peripheral.
 */
- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)aPeripheral error:(NSError *)error
{
    NSLog(@"[CBController] Fail to connect to peripheral: %@ with error = %@", aPeripheral, [error localizedDescription]);
    [self removeMyPeripheral:aPeripheral];
}

#pragma mark - CBPeripheral delegate methods
/*
 Invoked upon completion of a -[discoverServices:] request.
 Discover available characteristics on interested services
 */
- (void) peripheral:(CBPeripheral *)aPeripheral didDiscoverServices:(NSError *)error
{
    for (CBService *aService in aPeripheral.services)
    {
        NSLog(@"[CBController] Service found with UUID: %@", aService.UUID);
        //  NSArray *uuids = [[NSArray alloc] initWithObjects:[CBUUID UUIDWithString:@"2A4D"], nil];
        [aPeripheral discoverCharacteristics:nil forService:aService];
        //  [uuids release];
    }
}

/*
 Invoked upon completion of a -[discoverCharacteristics:forService:] request.
 Perform appropriate operations on interested characteristics
 */
- (void) peripheral:(CBPeripheral *)aPeripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    NSLog(@"\n[CBController] didDiscoverCharacteristicsForService: %@ %@", service.UUID, service.characteristics);
    CBCharacteristic *aChar = nil;
    HKMyPeripheral *myPeripheral = [self retrieveMyPeripheral:aPeripheral];
    if (myPeripheral == nil) {
        return;
    }
    if ([service.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_WEIGHT_SCALE_SERVICE]]) {
        for (CBCharacteristic *cha in service.characteristics) {
            if ([cha.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_WEIGHT_MANUFACTURE]]) {
                weightDataCharacteristic = cha;
            }
            else if ([cha.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_WEIGHT_FEATURE]]) {
                [aPeripheral readValueForCharacteristic:cha];
            }
        }
    }
    else if ([service.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_BATTERY_SERVICE]]) {
        for (CBCharacteristic *cha in service.characteristics) {
            if ([cha.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_BATTERY_LEVEL]]) {
                batteryCharacteristic = cha;
                [aPeripheral setNotifyValue:YES forCharacteristic:batteryCharacteristic];
            }
        }
    }
    else if ([service.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_HEART_RATE_SERVICE]]) {
        for (CBCharacteristic *cha in service.characteristics) {
            if ([cha.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_HEART_RATE_MANUFACTURE]]) {
                heartRateMeasurement = cha;
                [aPeripheral setNotifyValue:YES forCharacteristic:heartRateMeasurement];
            }
            else if ([cha.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_HEART_RATE_SENSOR_LOCATION]]) {
                [aPeripheral readValueForCharacteristic:cha];
            }
            else if ([cha.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_HEART_RATE_CONTROL_POINT]]) {
                heartRateControlPoint = cha;
                myPeripheral.heartRateControlPoint = cha;
            }
        }
    }
    else if ([service.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_HEALTH_THERMOMETER_SERVICE]]) {
        for (CBCharacteristic *cha in service.characteristics) {
            if ([cha.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_TEMPERATURE_MANUFACTURE]]) {
                //heartRateMeasurement = cha;
                [aPeripheral setNotifyValue:YES forCharacteristic:cha];
            }
            else if ([cha.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_TEMPERATURE_TYPE]]) {
                [aPeripheral readValueForCharacteristic:cha];
            }
            else if ([cha.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_MANUFACTURE_INTERVAL]]) {
                healthManufatureInterval = cha;
                myPeripheral.healthManufatureInterval = cha;
                [aPeripheral setNotifyValue:YES forCharacteristic:cha];
                [aPeripheral readValueForCharacteristic:cha];
            }
            else if ([cha.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_INTERMEDIATE_TEMPERATURE]]) {
                [aPeripheral setNotifyValue:YES forCharacteristic:cha];
            }
        }
    }
    else if ([service.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_BLOOD_PRESSURE_SERVICE]]) {
        for (CBCharacteristic *cha in service.characteristics) {
            if ([cha.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_BLOOD_PRESSURE_MANUFACTURE]]) {
                bloodPressureMeasurement = cha;
            }
            else if ([cha.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_BLOOD_PRESSURE_FEATURE]]) {
                [aPeripheral readValueForCharacteristic:cha];
            }
            else if ([cha.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_INTERMEDIATE_CUFF_PRESSURE]]) {
                intermediateCuffPressure = cha;
            }
        }
    }
    else if ([service.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_GLUCOSE_SERVICE]]) {
        for (CBCharacteristic *cha in service.characteristics) {
            if ([cha.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_GLUCOSE_MANUFACTURE]]) {
                glucoseMeasurement = cha;
            }
            else if ([cha.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_GLUCOSE_MANUFACTURE_CONTEXT]]) {
                glucoseMeasurementContext = cha;
            }
            else if ([cha.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_GLUCOSE_FEATURE]]) {
                [aPeripheral readValueForCharacteristic:cha];
            }
            else if ([cha.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_RECORD_ACCESS_CONTROL_POINT]]) {
                myPeripheral.glucoseRecordAccessControlPoint = cha;
            }
        }
    }
    else if (_transServiceUUID && [service.UUID isEqual:_transServiceUUID]) {
        isISSCPeripheral = TRUE;
        myPeripheral.canSendData = YES;
        for (aChar in service.characteristics)
        {
            if ([aChar.UUID isEqual:_transRxUUID]) {
                [myPeripheral setTransparentDataWriteChar:aChar];
                NSLog(@"found custom TRANS_RX");
            }
            else if ([aChar.UUID isEqual:_transTxUUID]) {
                NSLog(@"found custome TRANS_TX");
                [myPeripheral setTransparentDataReadChar:aChar];
                //  [aPeripheral setNotifyValue:TRUE forCharacteristic:aChar];
            }
        }
    }
    else if ([service.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_ISSC_PROPRIETARY_SERVICE]]) {
        isISSCPeripheral = TRUE;
        myPeripheral.canSendData = YES;
        for (aChar in service.characteristics)
        {
            if ((_transServiceUUID == nil) && [aChar.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_ISSC_TRANS_RX]]) {
                [myPeripheral setTransparentDataWriteChar:aChar];
                NSLog(@"found TRANS_RX");
                
            }
            else if ((_transServiceUUID == nil) && [aChar.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_ISSC_TRANS_TX]]) {
                NSLog(@"found TRANS_TX");
                [myPeripheral setTransparentDataReadChar:aChar];
                //[aPeripheral setNotifyValue:TRUE forCharacteristic:aChar];
            }
            else if ([aChar.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_CONNECTION_PARAMETER_CHAR]]) {
                [myPeripheral setConnectionParameterChar:aChar];
                NSLog(@"found CONNECTION_PARAMETER_CHAR");
            }
            else if ([aChar.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_AIR_PATCH_CHAR]]) {
                [myPeripheral setAirPatchChar:aChar];
                NSLog(@"found UUIDSTR_AIR_PATCH_CHAR");
                [myPeripheral.transmit enableReliableBurstTransmit:myPeripheral.peripheral andAirPatchCharacteristic:myPeripheral.airPatchChar];
            }
        }
    }
    else if([service.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_DEVICE_INFO_SERVICE]]) {
        
        for (aChar in service.characteristics)
        {
            if ([aChar.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_MANUFACTURE_NAME_CHAR]]) {
                [myPeripheral setManufactureNameChar:aChar];
                NSLog(@"found manufacture name char");
            }
            else if ([aChar.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_MODEL_NUMBER_CHAR]]) {
                [myPeripheral setModelNumberChar:aChar];
                NSLog(@"found model number char");
                
            }
            else if ([aChar.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_SERIAL_NUMBER_CHAR]]) {
                [myPeripheral setSerialNumberChar:aChar];
                NSLog(@"found serial number char");
            }
            else if ([aChar.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_HARDWARE_REVISION_CHAR]]) {
                [myPeripheral setHardwareRevisionChar:aChar];
                NSLog(@"found hardware revision char");
            }
            else if ([aChar.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_FIRMWARE_REVISION_CHAR]]) {
                [myPeripheral setFirmwareRevisionChar:aChar];
                NSLog(@"found firmware revision char");
            }
            else if ([aChar.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_SOFTWARE_REVISION_CHAR]]) {
                [myPeripheral setSoftwareRevisionChar:aChar];
                NSLog(@"found software revision char");
            }
            else if ([aChar.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_SYSTEM_ID_CHAR]]) {
                [myPeripheral setSystemIDChar:aChar];
                NSLog(@"[CBController] found system ID char");
            }
            else if ([aChar.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_IEEE_11073_20601_CHAR]]) {
                [myPeripheral setCertDataListChar:aChar];
                NSLog(@"found certification data list char");
            }
            else if (_disUUID1 && [aChar.UUID isEqual:_disUUID1]) {
                [myPeripheral setSpecificChar1:aChar];
                NSLog(@"found specific char1");
            }
            else if (_disUUID2 && [aChar.UUID isEqual:_disUUID2]) {
                [myPeripheral setSpecificChar2:aChar];
                NSLog(@"found specific char2");
            }
        }
    }
    
    if (isISSCPeripheral == TRUE) {
        [self updateMyPeripheralForNewConnected:myPeripheral];
    }
}

/*
 Invoked upon completion of a -[readValueForCharacteristic:] request or on the reception of a notification/indication.
 */
- (void) peripheral:(CBPeripheral *)aPeripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    HKMyPeripheral *myPeripheral = [self retrieveMyPeripheral:aPeripheral];
    if (myPeripheral == nil) {
        return;
    }
    NSLog(@"[CBController] didUpdateValueForCharacteristic %@",[characteristic  value]);
    if ([characteristic.service.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_WEIGHT_SCALE_SERVICE]]) {
        if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_WEIGHT_FEATURE]]) {
            [myPeripheral setWeihgtScaleFeature:characteristic.value];
            for (CBCharacteristic *cha in characteristic.service.characteristics) {
                if ([cha.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_WEIGHT_MANUFACTURE]]) {
                    [aPeripheral setNotifyValue:YES forCharacteristic:cha];
                }
            }
        }
        else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_WEIGHT_MANUFACTURE]]) {
            [myPeripheral saveWeihgtScaleData:characteristic.value];
        }
        
    }
    else if ([characteristic.service.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_BATTERY_SERVICE]]) {
        if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_BATTERY_LEVEL]]) {
            char buffer[1];
            [characteristic.value getBytes:&buffer];
            [myPeripheral saveBatteryLevel:buffer[0]];
        }
    }
    else if ([characteristic.service.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_HEART_RATE_SERVICE]]) {
        if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_HEART_RATE_MANUFACTURE]]) {
            [myPeripheral saveHeartRateData:characteristic.value];
        }
        else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_HEART_RATE_SENSOR_LOCATION]]) {
            [myPeripheral setSensorLocation:characteristic.value];
        }
    }
    else if ([characteristic.service.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_HEALTH_THERMOMETER_SERVICE]]) {
        if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_TEMPERATURE_MANUFACTURE]]) {
            [myPeripheral saveTemperatureData:characteristic.value isIntermediate:NO];
        }
        else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_INTERMEDIATE_TEMPERATURE]]) {
            [myPeripheral saveTemperatureData:characteristic.value isIntermediate:YES];
        }
        else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_TEMPERATURE_TYPE]]) {
            [myPeripheral setTemperatureType:characteristic.value];
        }
        else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_MANUFACTURE_INTERVAL]]) {
            uint16_t buffer;
            [characteristic.value getBytes:&buffer];
            [myPeripheral updateMeasurementInterval:buffer];
        }
    }
    else if ([characteristic.service.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_BLOOD_PRESSURE_SERVICE]]) {
        if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_BLOOD_PRESSURE_FEATURE]]) {
            [myPeripheral setBloodPressureFeature:characteristic.value];
            [aPeripheral setNotifyValue:YES forCharacteristic:bloodPressureMeasurement];
            [aPeripheral setNotifyValue:YES forCharacteristic:intermediateCuffPressure];
        }
        else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_BLOOD_PRESSURE_MANUFACTURE]]) {
            [myPeripheral saveBloodPressureData:characteristic.value isIntermediateCuffPressure:NO];
        }
        else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_INTERMEDIATE_CUFF_PRESSURE]]) {
            [myPeripheral saveBloodPressureData:characteristic.value isIntermediateCuffPressure:YES];
        }
        
    }
    else if ([characteristic.service.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_GLUCOSE_SERVICE]]) {
        if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_GLUCOSE_MANUFACTURE]]) {
            [myPeripheral saveGlucoseData:characteristic.value];
        }
        else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_GLUCOSE_MANUFACTURE_CONTEXT]]) {
            [myPeripheral saveGlucoseContextData:characteristic.value];
        }
        else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_GLUCOSE_FEATURE]]) {
            [myPeripheral setGlucoseFeature:characteristic.value];
            [aPeripheral setNotifyValue:YES forCharacteristic:glucoseMeasurement];
            [aPeripheral setNotifyValue:YES forCharacteristic:glucoseMeasurementContext];
        }
        else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_RECORD_ACCESS_CONTROL_POINT]]) {
            [myPeripheral updateGlucoseOpResponse:characteristic.value];
        }
    }
    else if ([characteristic.service.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_DEVICE_INFO_SERVICE]]) {
        if (myPeripheral.deviceInfoDelegate == nil)
            return;
        if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_MANUFACTURE_NAME_CHAR]]) {
            NSLog(@"[CBController] update manufacture name");
            
            if ([(NSObject *)myPeripheral.deviceInfoDelegate respondsToSelector:@selector(MyPeripheral:didUpdateManufactureName:error:)]) {
                [[myPeripheral deviceInfoDelegate] MyPeripheral:myPeripheral didUpdateManufactureName:[[NSString alloc] initWithData:characteristic.value encoding:NSUTF8StringEncoding] error:error];
            }
        }
        else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_MODEL_NUMBER_CHAR]]) {
            NSLog(@"[CBController] update model number");
            
            
            if ([(NSObject *)myPeripheral.deviceInfoDelegate respondsToSelector:@selector(MyPeripheral:didUpdateModelNumber:error:)]) {
                [myPeripheral.deviceInfoDelegate MyPeripheral:myPeripheral didUpdateModelNumber:[[NSString alloc] initWithData:characteristic.value encoding:NSUTF8StringEncoding] error:error];
            }
        }
        else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_SERIAL_NUMBER_CHAR]]) {
            NSLog(@"[CBController] update serial number");
            
            if ([(NSObject *)myPeripheral.deviceInfoDelegate respondsToSelector:@selector(MyPeripheral:didUpdateSerialNumber:error:)]) {
                [myPeripheral.deviceInfoDelegate MyPeripheral:myPeripheral didUpdateSerialNumber:[[NSString alloc] initWithData:characteristic.value encoding:NSUTF8StringEncoding] error:error];
            }
        }
        else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_HARDWARE_REVISION_CHAR]]) {
            NSLog(@"[CBController] update hardware revision");
            
            
            if ([(NSObject *)myPeripheral.deviceInfoDelegate respondsToSelector:@selector(MyPeripheral:didUpdateHardwareRevision:error:)]){
                [myPeripheral.deviceInfoDelegate MyPeripheral:myPeripheral didUpdateHardwareRevision:[[NSString alloc] initWithData:characteristic.value encoding:NSUTF8StringEncoding] error:error];
            }
        }
        else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_FIRMWARE_REVISION_CHAR]]) {
            NSLog(@"[CBController] update firmware revision");
            
            
            if ([(NSObject *)myPeripheral.deviceInfoDelegate respondsToSelector:@selector(MyPeripheral:didUpdateFirmwareRevision:error:)]){
                [myPeripheral.deviceInfoDelegate MyPeripheral:myPeripheral didUpdateFirmwareRevision:[[NSString alloc] initWithData:characteristic.value encoding:NSUTF8StringEncoding] error:error];
            }
        }
        else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_SOFTWARE_REVISION_CHAR]]) {
            
            NSLog(@"[CBController] update software revision");
            
            if ([(NSObject *)myPeripheral.deviceInfoDelegate respondsToSelector:@selector(MyPeripheral:didUpdateSoftwareRevision:error:)]){
                [myPeripheral.deviceInfoDelegate MyPeripheral:myPeripheral didUpdateSoftwareRevision:[[NSString alloc] initWithData:characteristic.value encoding:NSUTF8StringEncoding] error:error];
            }
        }
        else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_SYSTEM_ID_CHAR]]) {
            NSLog(@"[CBController] update system ID");
            
            if ([(NSObject *)myPeripheral.deviceInfoDelegate respondsToSelector:@selector(MyPeripheral:didUpdateSystemId:error:)]){
                
                [myPeripheral.deviceInfoDelegate MyPeripheral:myPeripheral didUpdateSystemId:characteristic.value error:error];
                
            }
        }
        else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_IEEE_11073_20601_CHAR]]) {
            NSLog(@"[CBController] update IEEE_11073_20601: %@",characteristic.value);
            
            if ([(NSObject *)myPeripheral.deviceInfoDelegate respondsToSelector:@selector(MyPeripheral:didUpdateIEEE_11073_20601:error:)]){
                
                [myPeripheral.deviceInfoDelegate MyPeripheral:myPeripheral didUpdateIEEE_11073_20601:characteristic.value error:error];
                
            }
        }
        else if (_disUUID1 && [characteristic.UUID isEqual:_disUUID1]) {
            NSLog(@"[CBController] update specific UUID 1: %@",characteristic.value);
            
            if ([(NSObject *)myPeripheral.deviceInfoDelegate respondsToSelector:@selector(MyPeripheral:didUpdateSpecificUUID1:error:)]){
                [myPeripheral.deviceInfoDelegate MyPeripheral:myPeripheral didUpdateSpecificUUID1:characteristic.value error:error];
            }
        }
        else if (_disUUID2 && [characteristic.UUID isEqual:_disUUID2]) {
            NSLog(@"[CBController] update specific UUID 2: %@",characteristic.value);
            
            if ([(NSObject *)myPeripheral.deviceInfoDelegate respondsToSelector:@selector(MyPeripheral:didUpdateSpecificUUID2:error:)]){
                [myPeripheral.deviceInfoDelegate MyPeripheral:myPeripheral didUpdateSpecificUUID2:characteristic.value error:error];
            }
        }
    }
    else if ([characteristic.service.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_ISSC_PROPRIETARY_SERVICE]]) {
        if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_CONNECTION_PARAMETER_CHAR]]) {
            NSLog(@"[CBController] update connection parameter: %@", characteristic.value);
            unsigned char buf[10];
            _CONNECTION_PARAMETER_FORMAT *parameter;
            
            [characteristic.value getBytes:&buf[0] length:sizeof(_CONNECTION_PARAMETER_FORMAT)];
            parameter = (_CONNECTION_PARAMETER_FORMAT *)&buf[0];
            
            //NSLog(@"[CBController] %02X, %02x, %02x, %02x, %02X, %02x, %02x, %02x, %02x,status= %d, min= %f,max= %f, latency=%d, timeout=%d", buf[0],buf[1],buf[2],buf[3],buf[4],buf[5],buf[6],buf[7],buf[8],parameter->status, parameter->minInterval*1.25, parameter->maxInterval*1.25, parameter->latency, parameter->connectionTimeout*10);
            
            //first time read
            if ([myPeripheral retrieveBackupConnectionParameter]->status == 0xff) {
                [myPeripheral updateBackupConnectionParameter:parameter];
            }
            else {
                switch (myPeripheral.updateConnectionParameterStep) {
                    case HK_UPDATE_PARAMETERS_STEP_PREPARE:
                        if ((myPeripheral.proprietaryDelegate != nil) && ([(NSObject *)myPeripheral.proprietaryDelegate respondsToSelector:@selector(MyPeripheral:didUpdateConnectionParameterAllowStatus:)]))
                            [myPeripheral.proprietaryDelegate MyPeripheral:myPeripheral didUpdateConnectionParameterAllowStatus:(buf[0] == 0x00)];
                        break;
                    case HK_UPDATE_PARAMETERS_STEP_CHECK_RESULT:
                        if (buf[0] != 0x00) {
                            NSLog(@"[CBController] check connection parameter status again");
                            [myPeripheral checkConnectionParameterStatus];
                        }
                        else {
                            if ((myPeripheral.proprietaryDelegate != nil) && ([(NSObject *)myPeripheral.proprietaryDelegate respondsToSelector:@selector(MyPeripheral:didUpdateConnectionParameterStatus:interval:timeout:latency:)])){
                                if ([myPeripheral compareBackupConnectionParameter:parameter] == TRUE) {
                                    NSLog(@"[CBController] connection parameter no change");
                                    [myPeripheral.proprietaryDelegate MyPeripheral:myPeripheral didUpdateConnectionParameterStatus:FALSE interval:parameter->maxInterval*1.25 timeout:parameter->connectionTimeout*10 latency:parameter->latency];
                                }
                                else {
                                    //NSLog(@"connection parameter update success");
                                    [myPeripheral.proprietaryDelegate MyPeripheral:myPeripheral didUpdateConnectionParameterStatus:TRUE interval:parameter->maxInterval*1.25 timeout:parameter->connectionTimeout*10 latency:parameter->latency];
                                    [myPeripheral updateBackupConnectionParameter:parameter];
                                }
                            }
                        }
                    default:
                        break;
                }
            }
        }
        else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_AIR_PATCH_CHAR]]) {
            [myPeripheral updateAirPatchEvent:characteristic.value];
        }
        else if ((_transServiceUUID == nil) && [characteristic.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_ISSC_TRANS_TX]]) {
            if ((myPeripheral.transDataDelegate != nil) && ([(NSObject *)myPeripheral.transDataDelegate respondsToSelector:@selector(MyPeripheral:didReceiveTransparentData:)])) {
                [myPeripheral.transDataDelegate MyPeripheral:myPeripheral didReceiveTransparentData:characteristic.value];
            }
        }
    }
    else if (_transServiceUUID && [characteristic.service.UUID isEqual:_transServiceUUID]) {
        if ([characteristic.UUID isEqual:_transTxUUID]) {
            if ((myPeripheral.transDataDelegate != nil) && ([(NSObject *)myPeripheral.transDataDelegate respondsToSelector:@selector(MyPeripheral:didReceiveTransparentData:)])) {
                [myPeripheral.transDataDelegate MyPeripheral:myPeripheral didReceiveTransparentData:characteristic.value];
            }
        }
    }
}

- (void) peripheral:(CBPeripheral *)aPeripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    NSLog(@"[CBController] didWriteValueForCharacteristic error msg:%d, %@, %@", error.code ,[error localizedFailureReason], [error localizedDescription]);
    NSLog(@"characteristic data = %@ id = %@",characteristic.value,characteristic.UUID);
    HKMyPeripheral *myPeripheral = [self retrieveMyPeripheral:aPeripheral];
    if (myPeripheral == nil) {
        return;
    }
    if ([myPeripheral.transmit isReliableBurstTransmit:characteristic]) {
        return;
    }
    if ((_transServiceUUID == nil) && [characteristic.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_ISSC_TRANS_RX]]) {
        if ((myPeripheral.transDataDelegate != nil) && ([(NSObject *)myPeripheral.transDataDelegate respondsToSelector:@selector(MyPeripheral:didSendTransparentDataStatus:)])) {
            [myPeripheral.transDataDelegate MyPeripheral:myPeripheral didSendTransparentDataStatus:error];
        }
    }
    else if (_transServiceUUID && [characteristic.UUID isEqual:_transRxUUID]) {
        if ((myPeripheral.transDataDelegate != nil) && ([(NSObject *)myPeripheral.transDataDelegate respondsToSelector:@selector(MyPeripheral:didSendTransparentDataStatus:)])) {
            [myPeripheral.transDataDelegate MyPeripheral:myPeripheral didSendTransparentDataStatus:error];
        }
    }
    else if (myPeripheral.healthManufatureInterval && [characteristic.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_MANUFACTURE_INTERVAL]]) {
        [myPeripheral.peripheral readValueForCharacteristic:myPeripheral.healthManufatureInterval];
    }
}

- (void) peripheral:(CBPeripheral *)aPeripheral didDiscoverDescriptorsForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    NSLog(@"[CBController] didDiscoverDescriptorsForCharacteristic error msg:%d, %@, %@", error.code ,[error localizedFailureReason], [error localizedDescription]);
}

- (void) peripheral:(CBPeripheral *)peripheral didUpdateValueForDescriptor:(CBDescriptor *)descriptor error:(NSError *)error
{
    NSLog(@"[CBController] didUpdateValueForDescriptor");
}

-(void) peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    NSLog(@"[CBController] didUpdateNotificationStateForCharacteristic, UUID = %@", characteristic.UUID);
    HKMyPeripheral *myPeripheral = [self retrieveMyPeripheral:peripheral];
    if (myPeripheral == nil) {
        return;
    }
    if ([characteristic.service.UUID isEqual:[CBUUID UUIDWithString:UUIDSTR_ISSC_PROPRIETARY_SERVICE]] &&(myPeripheral.transDataDelegate != nil) && ([(NSObject *)myPeripheral.transDataDelegate respondsToSelector:@selector(MyPeripheral:didUpdateTransDataNotifyStatus:)])) {
        [myPeripheral.transDataDelegate MyPeripheral:myPeripheral didUpdateTransDataNotifyStatus:characteristic.isNotifying];
        myPeripheral.isNotifying = characteristic.isNotifying;
    }
    
}

@end
