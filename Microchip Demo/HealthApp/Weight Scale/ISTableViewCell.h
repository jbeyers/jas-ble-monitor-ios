//
//  ISTableViewCell.h
//  Weight Scale
//
//  Created by Rick on 2014/10/7.
//  Copyright (c) 2014年 Rick. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISTableViewCell : UITableViewCell
@property (strong) UILabel *weight;
@property (strong) UILabel *height;
@property (strong) UILabel *bmi;
@property (strong) UILabel *user_id;
@property (strong) UILabel *time;
@end
