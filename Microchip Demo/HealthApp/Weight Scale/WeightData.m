//
//  WeightData.m
//  Weight Scale
//
//  Created by Rick on 2014/10/6.
//  Copyright (c) 2014年 Rick. All rights reserved.
//

#import "WeightData.h"


@implementation WeightData

@dynamic flags;
@dynamic weight;
@dynamic timestamp;
@dynamic userID;
@dynamic bmi;
@dynamic height;
@dynamic weightUnit;
@dynamic heightUnit;

@end
