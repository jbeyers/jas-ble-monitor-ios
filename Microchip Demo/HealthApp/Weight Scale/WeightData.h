//
//  WeightData.h
//  Weight Scale
//
//  Created by Rick on 2014/10/6.
//  Copyright (c) 2014年 Rick. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface WeightData : NSManagedObject

@property (nonatomic, retain) NSNumber * flags;
@property (nonatomic, retain) NSNumber * weight;
@property (nonatomic, retain) NSDate * timestamp;
@property (nonatomic, retain) NSNumber * userID;
@property (nonatomic, retain) NSNumber * bmi;
@property (nonatomic, retain) NSNumber * height;
@property (nonatomic, retain) NSString * weightUnit;
@property (nonatomic, retain) NSString * heightUnit;

@end
