//
//  ISWeightController.m
//  Weight Scale
//
//  Created by Rick on 2014/9/30.
//  Copyright (c) 2014年 Rick. All rights reserved.
//

#import "ISWeightController.h"
#import "HKConnectViewController.h"
#import "CBController.h"
#import "ISTableViewController.h"
#import "ISGraphViewController.h"
#import "ISSettingController.h"
#import "AppDelegate.h"

@interface ISWeightController () <HKCBControllerDelegate,MyPeripheralDelegate> {
    UILabel *unit;
    UILabel *weight;
    UILabel *battery_l;
    UILabel *time;
    HKConnectViewController *scan;
    UIImageView *connect_icon;
    HKMyPeripheral *connect_peripheral;
    NSDateFormatter *formater;
    UIButton *scan_but;
}

@end

@implementation ISWeightController

- (void)dealloc {
    [super dealloc];
}

- (void)loadView {
    if ([[UIScreen mainScreen] bounds].size.height > 480.0f) {
        [super loadView];
    }
    else {
        UIScrollView *view = [[UIScrollView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        view.contentSize = CGSizeMake(320.0f, 568.0f);
        view.contentInset = UIEdgeInsetsMake(-64.0f, 0, 0, 0);
        self.view = view;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
#ifdef USE_HEALTH_KIT
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if ([appDelegate isHealthKitAvailable]) {
        [appDelegate.healthStore requestAuthorizationToShareTypes:[NSSet setWithObjects:[HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierBodyMass], nil] readTypes:[NSSet setWithObjects:[HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierBodyMass], nil] completion:^(BOOL success, NSError *error) {
            if (success) {
                NSLog(@"Authorization Success");
            }
        }];
    }
 #endif
    
    CGRect rect = [[UIScreen mainScreen]bounds];
    self.title = @"Weight Scale";
    //self.wantsFullScreenLayout = YES;
    self.view.backgroundColor = [UIColor whiteColor];
    UIView *info_bg = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, rect.size.width, 350.0f)];
    info_bg.backgroundColor = [UIColor colorWithRed:0.15 green:0.74 blue:0.62 alpha:1.0];
    [self.view addSubview:info_bg];
    UIButton *history = [UIButton buttonWithType:UIButtonTypeCustom];
    history.frame = info_bg.bounds;
    [history addTarget:self action:@selector(showGraph:) forControlEvents:UIControlEventTouchUpInside];
    [info_bg addSubview:history];
    UIView *battery_bg = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 350.0f, rect.size.width/2, 150.0f)];
    battery_bg.backgroundColor = [UIColor colorWithRed:0.95 green:0.62 blue:0.16 alpha:1.0];
    [self.view addSubview:battery_bg];
    UIView *time_bg = [[UIView alloc] initWithFrame:CGRectMake(rect.size.width/2, 350.0f, rect.size.width/2, 150.0f)];
    time_bg.backgroundColor = [UIColor colorWithRed:0.90 green:0.30 blue:0.24 alpha:1.0];
    [self.view addSubview:time_bg];
    UIView *scan_bg = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 500.0f, rect.size.width, 68.0f)];
    scan_bg.backgroundColor = [UIColor colorWithRed:0.18 green:0.50 blue:0.72 alpha:1.0];
    [self.view addSubview:scan_bg];
    unit = [[UILabel alloc] initWithFrame:CGRectMake(250.0f, 70.0f, 100.0f, 32.0f)];
    //unit.text = @"KG";
    unit.textColor = [UIColor whiteColor];
    unit.textAlignment = NSTextAlignmentCenter;
    [info_bg addSubview:unit];
    weight = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 160.0f, rect.size.width, 100.0f)];
    //weight.text = @"0.0";
    weight.font = [UIFont boldSystemFontOfSize:90.0f];
    weight.textColor = [UIColor whiteColor];
    weight.textAlignment = NSTextAlignmentCenter;
    [info_bg addSubview:weight];
    battery_l = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 160.0f, 60.0f)];
    battery_l.center = CGPointMake(80.0f, 75.0f);
    //battery_l.text = @"100%";
    battery_l.font = [UIFont boldSystemFontOfSize:24.0f];
    battery_l.textColor = [UIColor whiteColor];
    battery_l.textAlignment = NSTextAlignmentCenter;
    [battery_bg addSubview:battery_l];
    UIImageView *battery_icon = [[UIImageView alloc] initWithFrame:CGRectMake(5.0f, 5.0f, 24.0f, 24.0f)];
    battery_icon.image = [UIImage imageNamed:@"battery.png"];
    [battery_bg addSubview:battery_icon];
    time = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 160.0f, 60.0f)];
    time.center = CGPointMake(80.0f, 75.0f);
    //time.text = @"2014/10/01\n18:00:00";
    time.font = [UIFont boldSystemFontOfSize:24.0f];
    time.textColor = [UIColor whiteColor];
    time.textAlignment = NSTextAlignmentCenter;
    time.numberOfLines = 2;
    time.lineBreakMode = NSLineBreakByWordWrapping;
    [time_bg addSubview:time];
    scan_but = [UIButton buttonWithType:UIButtonTypeCustom];
    [scan_but setTitle:@"Scan" forState:UIControlStateNormal];
    scan_but.titleLabel.font = [UIFont boldSystemFontOfSize:24.0f];
    scan_but.frame = CGRectMake(0.0f, 0.0f, rect.size.width, 68.0f);
    scan_but.center = CGPointMake(rect.size.width/2, 34.0f);
    scan_but.tag = 0;
    [scan_bg addSubview:scan_but];
    [scan_but addTarget:self action:@selector(scan) forControlEvents:UIControlEventTouchUpInside];
    scan = [[HKConnectViewController alloc] initWithNibName:nil bundle:nil];
    scan.uuids = @[UUIDSTR_WEIGHT_SCALE_SERVICE];
    scan.serviceUuids = @[[CBUUID UUIDWithString:UUIDSTR_DEVICE_INFO_SERVICE],[CBUUID UUIDWithString:UUIDSTR_BATTERY_SERVICE],[CBUUID UUIDWithString:UUIDSTR_WEIGHT_SCALE_SERVICE]];
    scan.delegate = self;
    connect_icon = [[UIImageView alloc] initWithFrame:CGRectMake(10.0f, 70.0f, 40.0f, 40.0f)];
    connect_icon.image = [UIImage imageNamed:@"ic_scale"];
    [info_bg addSubview:connect_icon];
    connect_icon.alpha = 0.5;
    formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:@"yyyy-MM-dd\nHH:mm:ss"];
    UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithTitle:@"Settings" style:UIBarButtonItemStylePlain target:self action:@selector(showSetting:)];
    self.navigationItem.rightBarButtonItem = right;
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:@selector(dismiss)];
    [self resetText];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    NSString *uuid_string = [[NSUserDefaults standardUserDefaults] objectForKey:UUIDSTR_WEIGHT_SCALE_SERVICE];
    if (uuid_string) {
        NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:uuid_string];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            //[scan connectDeviceWithIdentifier:uuid];
        });
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)scan {
    if ([[scan_but titleForState:UIControlStateNormal] isEqualToString:@"Scan"]) {
        [self.navigationController pushViewController:scan animated:YES];
    }
    if (connect_peripheral) {
        [scan disconnectDevice:connect_peripheral];
        connect_peripheral = nil;
        connect_icon.alpha = 0.5;
        [scan_but setTitle:@"Scan" forState:UIControlStateNormal];
    }
}

- (void)dismiss {
    [self dismissViewControllerAnimated:YES completion:^{
        NSLog(@"dismissViewControllerAnimated");
        [scan release];
     }];
}

- (void)showHistory {
    ISTableViewController *hsitory = [[ISTableViewController alloc] initWithStyle:UITableViewStylePlain];
    [self.navigationController pushViewController:hsitory animated:YES];
}

- (void)showGraph:(id)sender {
    ISGraphViewController *view = [[ISGraphViewController alloc] init];
    [self.navigationController pushViewController:view animated:YES];
}

- (void)showSetting:(id)sender {
    ISSettingController *view = [[ISSettingController alloc] initWithStyle:UITableViewStyleGrouped];
    [self.navigationController pushViewController:view animated:YES];
}

- (void)resetText {
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"Unit"] == 0) {
        unit.text = @"kg";
    }
    else {
        unit.text = @"lb";
    }
    weight.text = @"0.0";
    time.text = @"";
    battery_l.text = @"";

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)didUpdatePeripheralList:(NSArray *)peripherals {
    
}

- (void)didConnectPeripheral:(HKMyPeripheral *)peripheral {
    if (peripheral.connectStaus == MYPERIPHERAL_CONNECT_STATUS_CONNECTED) {
        connect_icon.alpha = 1.0f;
        [scan_but setTitle:@"Disconnect" forState:UIControlStateNormal];
    }
    else {
        [scan_but setTitle:@"Scan" forState:UIControlStateNormal];
        connect_icon.alpha = 0.5f;
    }
    connect_peripheral = peripheral;
    peripheral.transDataDelegate = self;
    [self resetText];
}

- (void)didDisconnectPeripheral:(MyPeripheral *)peripheral {
    [scan_but setTitle:@"Scan" forState:UIControlStateNormal];
    connect_icon.alpha = 0.5f;
    peripheral.transDataDelegate = nil;
}

- (void)MyPeripheral:(MyPeripheral *)peripheral didUpdateWeihgtScaleData:(NSDictionary *)data error:(NSError *)error {
    unit.text = data[@"unit_weight"];
    weight.text = [NSString stringWithFormat:@"%.2f",[data[@"weight"] floatValue]];
    time.text = [formater stringFromDate:data[@"time"]];
#ifdef USE_HEALTH_KIT
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if ([appDelegate isHealthKitAvailable]) {
        HKUnit *Unit;
        if ([data[@"unit_weight"] isEqualToString:@"kg"]) {
            Unit = [HKUnit gramUnitWithMetricPrefix:HKMetricPrefixKilo];
        }
        else {
            Unit = [HKUnit poundUnit];
        }
        HKQuantity *weightQuantity = [HKQuantity quantityWithUnit:Unit doubleValue:[data[@"weight"] doubleValue]];
        HKQuantityType *weightType = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierBodyMass];
        NSDate *now = data[@"time"];
        
        HKQuantitySample *weightSample = [HKQuantitySample quantitySampleWithType:weightType quantity:weightQuantity startDate:now endDate:now];
        [appDelegate.healthStore saveObject:weightSample withCompletion:^(BOOL success, NSError *error) {
            if (!success) {
                NSLog(@"An error occured saving the weight sample %@. In your app, try to handle this gracefully. The error was: %@.", weightSample, error);
            }
        }];
    }
#endif
}

- (void)MyPeripheral:(MyPeripheral *)peripheral didUpdateBatteryLevel:(NSInteger)level error:(NSError *)error {
    battery_l.text = [NSString stringWithFormat:@"%ld %%",(long)level];
}


@end
