//
//  ISSettingController.m
//  Weight Scale
//
//  Created by Rick on 2014/10/27.
//  Copyright (c) 2014年 Rick. All rights reserved.
//

#import "ISSettingController.h"
#import "AppDelegate.h"
#import "UUID.h"

@interface ISSettingController ()<UITextFieldDelegate> {
    UITextField *textField;
    UISegmentedControl *unit;
    UISwitch *healthKitSwitch;
}

@end

@implementation ISSettingController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Settings";
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    textField = [[UITextField alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 100.0f, 30.0f)];
    textField.borderStyle = UITextBorderStyleRoundedRect;
    textField.keyboardType = UIKeyboardTypeDecimalPad;
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"Height"]) {
        textField.text = [[[NSUserDefaults standardUserDefaults] objectForKey:@"Height"] stringValue];
    }
    unit = [[UISegmentedControl alloc] initWithItems:@[@"kg/cm",@"lb/inch"]];
    unit.frame = CGRectMake(0.0f, 0.0f, 100.0f, 30.0f);
    unit.selectedSegmentIndex = [[NSUserDefaults standardUserDefaults] integerForKey:@"Unit"];
    [unit addTarget:self action:@selector(setUnit:) forControlEvents:UIControlEventValueChanged];
#ifdef USE_HEALTH_KIT
    healthKitSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 50.0f, 30.0f)];
    [healthKitSwitch addTarget:self action:@selector(setUseHealthKit:) forControlEvents:UIControlEventValueChanged];
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if ([appDelegate isHealthKitAvailable]) {
        healthKitSwitch.on = [[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"%@HealthKit",UUIDSTR_WEIGHT_SCALE_SERVICE]];
    }
#endif

}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    if (![textField.text isEqualToString:@""]) {
        [[NSUserDefaults standardUserDefaults] setObject:@([textField.text floatValue]) forKey:@"Height"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setUnit:(UISegmentedControl *)segment {
    [[NSUserDefaults standardUserDefaults] setObject:@(segment.selectedSegmentIndex) forKey:@"Unit"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)setUseHealthKit:(UISwitch *)sender {
    [[NSUserDefaults standardUserDefaults] setObject:@(sender.on) forKey:[NSString stringWithFormat:@"%@HealthKit",UUIDSTR_WEIGHT_SCALE_SERVICE]];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#ifdef USE_HEALTH_KIT
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if ([appDelegate isHealthKitAvailable]) {
        return 3;
    }
#endif
    return 2;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    switch (indexPath.row) {
        case 0:
            cell.textLabel.text = @"Height";
            cell.accessoryView = textField;
            break;
        case 1:
            cell.textLabel.text = @"Unit";
            cell.accessoryView = unit;
            break;
        case 2:
            cell.textLabel.text = @"HealthKit";
            cell.accessoryView = healthKitSwitch;
            break;
        default:
            break;
    }
    // Configure the cell...
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
