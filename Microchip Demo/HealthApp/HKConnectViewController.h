//
//  HKConnectViewController.h
//  Health
//
//  Created by ILYAS on 27/01/16.
//  Copyright © 2016 Rick. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HKCBController.h"

@interface HKConnectViewController : HKCBController<UITableViewDataSource, UITextViewDelegate, UITableViewDelegate>
{
    IBOutlet UITableView *devicesTableView;
    UIActivityIndicatorView *activityIndicatorView;
    UILabel *statusLabel;
    
    NSTimer *refreshDeviceListTimer;
    
    int connectionStatus;
    //Derek
    HKMyPeripheral *controlPeripheral;
    NSMutableArray *connectedDeviceInfo;//stored for DeviceInfo object
    NSMutableArray *connectingList;//stored for MyPeripheral object
    
    UIBarButtonItem *refreshButton;
    UIBarButtonItem *scanButton;
    UIBarButtonItem *cancelButton;
    UIBarButtonItem *uuidSettingButton;
}
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *activityIndicatorView;
@property (nonatomic, retain) IBOutlet UILabel *statusLabel;
@property (assign) int connectionStatus;
@property (retain, nonatomic) IBOutlet UILabel *versionLabel;
@property (strong) NSArray *uuids;

- (IBAction)refreshDeviceList:(id)sender;
- (IBAction)actionButtonCancelScan:(id)sender;
- (IBAction)manualUUIDSetting:(id)sender;
- (IBAction)actionButtonDisconnect:(id)sender;
- (IBAction)actionButtonCancelConnect:(id)sender;
@end
