//
//  HKCBController.h
//  Health
//
//  Created by ILYAS on 27/01/16.
//  Copyright © 2016 Rick. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <CoreBluetooth/CoreBluetooth.h>
#import "UUID.h"
#import "HKMyPeripheral.h"

enum {
    HK_LE_STATUS_IDLE = 0,
    HK_LE_STATUS_SCANNING,
    HK_LE_STATUS_CONNECTING,
    HK_LE_STATUS_CONNECTED
};

@protocol HKCBControllerDelegate;

@interface HKCBController : UIViewController <CBCentralManagerDelegate, CBPeripheralDelegate>
{
    CBCentralManager *manager;
    NSMutableArray *devicesList;
    BOOL    notifyState;
    NSMutableArray *_connectedPeripheralList;
    CBUUID *_transServiceUUID;
    CBUUID *_transTxUUID;
    CBUUID *_transRxUUID;
    CBUUID *_disUUID1;
    CBUUID *_disUUID2;
    BOOL    _waitingBtPowerOnToScan;
    BOOL    isISSCPeripheral;
}

@property(assign) id<HKCBControllerDelegate> delegate;
@property (retain) NSMutableArray *devicesList;
@property (strong) NSArray *serviceUuids;

//- (void) startScan;
- (void) startScanWithUUID:(NSArray *)uuids;
- (void) stopScan;
- (void)connectDevice:(HKMyPeripheral *) myPeripheral;
- (void)connectDeviceWithIdentifier:(NSUUID *)identifier;
- (void)disconnectDevice:(HKMyPeripheral *) aPeripheral;
- (NSMutableData *) hexStrToData: (NSString *)hexStr;
- (BOOL) isLECapableHardware;
- (void)addDiscoverPeripheral:(CBPeripheral *)aPeripheral advName:(NSString *)advName;
- (void)updateDiscoverPeripherals;
- (void)updateMyPeripheralForDisconnect:(HKMyPeripheral *)myPeripheral;
- (void)updateMyPeripheralForNewConnected:(HKMyPeripheral *)myPeripheral;
- (void)storeMyPeripheral: (CBPeripheral *)aPeripheral;
- (HKMyPeripheral *)retrieveMyPeripheral: (CBPeripheral *)aPeripheral;
- (void)removeMyPeripheral: (CBPeripheral *) aPeripheral;
- (void)configureTransparentServiceUUID: (NSString *)serviceUUID txUUID:(NSString *)txUUID rxUUID:(NSString *)rxUUID;
- (void)configureDeviceInformationServiceUUID: (NSString *)UUID1 UUID2:(NSString *)UUID2;
@end

@protocol HKCBControllerDelegate<NSObject>
@required
- (void)didUpdatePeripheralList:(NSArray *)peripherals;
- (void)didConnectPeripheral:(HKMyPeripheral *)peripheral;
- (void)didDisconnectPeripheral:(HKMyPeripheral *)peripheral;
@end
