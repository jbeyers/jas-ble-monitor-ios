//
//  ISBloodPressureDataParser.h
//  Health
//
//  Created by Rick on 2014/12/8.
//  Copyright (c) 2014年 Rick. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef struct _BLOOD_PRESSURE_FLAG {
    uint8_t units:1;
    uint8_t timestamp:1;
    uint8_t pulse_rate:1;
    uint8_t user_id:1;
    uint8_t measurement_status:1;
    uint8_t reserved:3;
}__attribute__((packed)) BLOOD_PRESSURE_FLAG;

typedef struct _BLOOD_PRESSURE_FEATURE {
    uint16_t body_movement:1;
    uint16_t cuff_fit:1;
    uint16_t irregular_pulse:1;
    uint16_t pulse_rate_range:1;
    uint16_t measurement_position:1;
    uint16_t multiple_bond:1;
    uint16_t reserved:10;
}__attribute__((packed)) BLOOD_PRESSURE_FEATURE;

typedef struct _MEASUREMENT_STATUS {
    uint16_t body_movement:1;
    uint16_t cuff_fit:1;
    uint16_t irregular_pulse:1;
    uint16_t pulse_rate_range:2;
    uint16_t measurement_position:1;
    uint16_t reserved:10;
}__attribute__((packed)) MEASUREMENT_STATUS;

@interface ISBloodPressureDataParser : NSObject
- (void)decodeFeature:(NSData *)data;
- (NSDictionary *)decodeData:(NSData *)data;
@end
