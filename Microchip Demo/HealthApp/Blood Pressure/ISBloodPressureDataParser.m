//
//  ISBloodPressureDataParser.m
//  Health
//
//  Created by Rick on 2014/12/8.
//  Copyright (c) 2014年 Rick. All rights reserved.
//

#import "ISBloodPressureDataParser.h"
#import "ISWeightParser.h"
#import "CovertTools.h"

@interface ISBloodPressureDataParser() {
    BLOOD_PRESSURE_FEATURE *feature;
    NSDateFormatter *formater;
}
@end

@implementation ISBloodPressureDataParser
- (id)init {
    self = [super init];
    if (self) {
        feature = nil;
        formater = [[NSDateFormatter alloc] init];
        [formater setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    }
    return self;
}

- (void)dealloc {
    free(feature);
}

- (void)decodeFeature:(NSData *)data {
    uint16_t *buffer = malloc(sizeof(uint16_t));
    [data getBytes:buffer length:[data length]];
    feature = (BLOOD_PRESSURE_FEATURE *)buffer;
}

- (NSDictionary *)decodeData:(NSData *)data {
    uint8_t buffer[1];
    [data getBytes:&buffer length:1];
    BLOOD_PRESSURE_FLAG *flag = (BLOOD_PRESSURE_FLAG *)buffer;
    int pointer = 1;
    NSString *uint;
    if (flag->units == 0) {
        uint = @"mmHg";
    }
    else {
        uint = @"kPa";
    }
    uint16_t temp_data;
    [data getBytes:&temp_data range:NSMakeRange(pointer, 2)];
    float_t systolic = [CovertTools covertSFLOAT:temp_data];
    pointer+=2;
    [data getBytes:&temp_data range:NSMakeRange(pointer, 2)];
    float_t diastolic  = [CovertTools covertSFLOAT:temp_data];
    pointer+=2;
    [data getBytes:&temp_data range:NSMakeRange(pointer, 2)];
    float_t map = [CovertTools covertSFLOAT:temp_data];
    pointer+=2;
    NSDate *time = [NSDate date];
    if (flag->timestamp == 1) {
        uint8_t time_buffer[7];
        [data getBytes:&time_buffer range:NSMakeRange(pointer, 7)];
        pointer+=7;
        TIMESTAMP *t = (TIMESTAMP *)time_buffer;
        NSString *t_s = [NSString stringWithFormat:@"%d-%d-%d %d:%d:%d",t->year,t->month,t->day,t->hour,t->minute,t->second];
        time = [formater dateFromString:t_s];
    }
    float_t pulseRate = 0.0f;
    if (flag->pulse_rate == 1) {
        [data getBytes:&temp_data range:NSMakeRange(pointer, 2)];
        pulseRate = [CovertTools covertSFLOAT:temp_data];
        pointer+=2;
    }
    uint8_t userID = 0;
    if (flag->user_id == 1) {
        [data getBytes:&userID range:NSMakeRange(pointer, 1)];
        pointer+=1;
    }
    NSString *status_s;
    if (flag->measurement_status == 1) {
        uint8_t b[1];
        [data getBytes:&b range:NSMakeRange(pointer, 1)];
        MEASUREMENT_STATUS *status = (MEASUREMENT_STATUS *)b;
        status_s = [NSString stringWithFormat:@"%@|%@|%@|%@|%@|%@",
                    feature->body_movement?@(status->body_movement):@"X",
                    feature->cuff_fit?@(status->cuff_fit):@"X",
                    feature->irregular_pulse?@(status->irregular_pulse):@"X",
                    feature->pulse_rate_range?@(status->pulse_rate_range):@"X",
                    feature->measurement_position?@(status->measurement_position):@"X",
                    feature->multiple_bond?@"O":@"X"
                    ];
    }
    else {
        status_s = [NSString stringWithFormat:@"X|X|X|X|X|%@",
                    feature->multiple_bond?@"O":@"X"
                    ];
    }
    return @{@"unit":uint,
             @"systolic":@(systolic),
             @"diastolic":@(diastolic),
             @"map":@(map),
             @"timestamp":time,
             @"pulse_rate":@(pulseRate),
             @"user_id":@(userID),
             @"status":status_s
             };
}

@end
