//
//  BloodPressureData.h
//  Health
//
//  Created by Rick on 2014/12/11.
//  Copyright (c) 2014年 Rick. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface BloodPressureData : NSManagedObject

@property (nonatomic, retain) NSNumber * diastolic;
@property (nonatomic, retain) NSNumber * map;
@property (nonatomic, retain) NSNumber * pulseRate;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSNumber * systolic;
@property (nonatomic, retain) NSDate * timestamp;
@property (nonatomic, retain) NSNumber * userID;
@property (nonatomic, retain) NSString * unit;

@end
