//
//  ISBloodPressureCell.m
//  Health
//
//  Created by Rick on 2014/12/8.
//  Copyright (c) 2014年 Rick. All rights reserved.
//

#import "ISBloodPressureCell.h"

@implementation ISBloodPressureCell

- (void)awakeFromNib {
    // Initialization code
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _value = [[UILabel alloc] initWithFrame:CGRectMake(5.0f, 10.0f, 110.0f, 60.0f)];
        _value.text = @"S:130.0\nD:90.0\nM:100.0";
        _value.font = [UIFont systemFontOfSize:16.0f];
        _value.numberOfLines = 3;
        _value.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_value];
        _pulseRate = [[UILabel alloc] initWithFrame:CGRectMake(120.0f, 5.0f, 110.0f, 18.0f)];
        _pulseRate.text = @"Pulse Rate:75";
        _pulseRate.font = [UIFont systemFontOfSize:16.0f];
        [self.contentView addSubview:_pulseRate];
        _unit = [[UILabel alloc] initWithFrame:CGRectMake(230.0f, 5.0f, 110.0f, 18.0f)];
        _unit.text = @"Unit:mmHg";
        _unit.font = [UIFont systemFontOfSize:16.0f];
        [self.contentView addSubview:_unit];
        _user_id = [[UILabel alloc] initWithFrame:CGRectMake(120.0f, 30.0f, 110.0f, 18.0f)];
        _user_id.text = @"User ID:1";
        _user_id.font = [UIFont systemFontOfSize:16.0f];
        [self.contentView addSubview:_user_id];
        _time = [[UILabel alloc] initWithFrame:CGRectMake(120.0f, 55.0f, 220.0f, 18.0f)];
        _time.text = @"2014/10/10 12:12:12";
        _time.font = [UIFont systemFontOfSize:16.0f];
        [self.contentView addSubview:_time];
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
