//
//  ISBloodPressureGraphController.m
//  Health
//
//  Created by Rick on 2014/12/8.
//  Copyright (c) 2014年 Rick. All rights reserved.
//

#import "ISBloodPressureGraphController.h"
#import "ISBloodPressureTableController.h"
#import "BEMSimpleLineGraphView.h"
#import "AppDelegate.h"
#import "UUID.h"
#import "BloodPressureData.h"

@interface ISBloodPressureGraphController ()<BEMSimpleLineGraphDataSource, BEMSimpleLineGraphDelegate,UIScrollViewDelegate> {
    NSMutableArray *data;
    NSMutableArray *diastolicData;
    NSDateFormatter *formater;
}

@end

@implementation ISBloodPressureGraphController

- (void)loadView {
#ifdef USE_HEALTH_KIT
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if ([appDelegate isHealthKitAvailable] && [[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"%@HealthKit",UUIDSTR_BLOOD_PRESSURE_SERVICE]]) {
        if ([[UIScreen mainScreen] bounds].size.height > 480.0f) {
            [super loadView];
        }
        else {
            UIScrollView *view = [[UIScrollView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
            view.contentSize = CGSizeMake(320.0f, 568.0f);
            //view.contentInset = UIEdgeInsetsMake(-64.0f, 0, 0, 0);
            self.view = view;
        }
    }
    else {
#endif
        UIScrollView *view = [[UIScrollView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        view.contentSize = CGSizeMake(320.0f, 740.0f);
        self.view = view;
#ifdef USE_HEALTH_KIT
    }
#endif

}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"History";
    self.view.backgroundColor = [UIColor whiteColor];
    data = [[NSMutableArray alloc] init];
    diastolicData = [[NSMutableArray alloc] init];
    formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:@"yyyy/MM/dd"];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"BloodPressureData" inManagedObjectContext:appDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    NSSortDescriptor* sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"timestamp" ascending:NO];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
#ifdef USE_HEALTH_KIT
    if ([appDelegate isHealthKitAvailable] && [[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"%@HealthKit",UUIDSTR_BLOOD_PRESSURE_SERVICE]]) {
        HKQuantityType *weightType = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierBloodPressureSystolic];
        NSSortDescriptor *timeSortDescriptor = [[NSSortDescriptor alloc] initWithKey:HKSampleSortIdentifierEndDate ascending:YES];
        HKSampleQuery *query = [[HKSampleQuery alloc] initWithSampleType:weightType predicate:nil limit:HKObjectQueryNoLimit sortDescriptors:@[timeSortDescriptor] resultsHandler:^(HKSampleQuery *query, NSArray *results, NSError *error) {
            if  (results) {
                [data addObjectsFromArray:results];
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIScrollView *graphScroll = (UIScrollView *)[self.view viewWithTag:1];
                    if (data.count >= 2) {
                        graphScroll.hidden = NO;
                    }
                    CGFloat width = 32+50*data.count;
                    if (width < 320.0f) width = 320.0f;
                    graphScroll.contentSize = CGSizeMake(width+5, 200);
                    BEMSimpleLineGraphView *myGraph = (BEMSimpleLineGraphView *)[[graphScroll subviews] objectAtIndex:0];
                    myGraph.frame = CGRectMake(0, 0, width, 200);
                    [myGraph reloadGraph];
                });
            }
            else {
                NSLog(@"HealthKit Error : %@",error.description);
            }
        }];
        [appDelegate.healthStore executeQuery:query];
        weightType = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierBloodPressureDiastolic];
        timeSortDescriptor = [[NSSortDescriptor alloc] initWithKey:HKSampleSortIdentifierEndDate ascending:YES];
        query = [[HKSampleQuery alloc] initWithSampleType:weightType predicate:nil limit:HKObjectQueryNoLimit sortDescriptors:@[timeSortDescriptor] resultsHandler:^(HKSampleQuery *query, NSArray *results, NSError *error) {
            if  (results) {
                [diastolicData addObjectsFromArray:results];
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIScrollView *graphScroll = (UIScrollView *)[self.view viewWithTag:2];
                    if (diastolicData.count >= 2) {
                        graphScroll.hidden = NO;
                    }
                    CGFloat width = 32+50*diastolicData.count;
                    if (width < 320.0f) width = 320.0f;
                    graphScroll.contentSize = CGSizeMake(width+5, 200);
                    BEMSimpleLineGraphView *myGraph = (BEMSimpleLineGraphView *)[[graphScroll subviews] objectAtIndex:0];
                    myGraph.frame = CGRectMake(0, 0, width, 200);
                    [myGraph reloadGraph];
                });
            }
            else {
                NSLog(@"HealthKit Error : %@",error.description);
            }
        }];
        [appDelegate.healthStore executeQuery:query];
    }
    else {
#endif
        NSArray *temp = [appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:nil];
        [data addObjectsFromArray:temp];
#ifdef USE_HEALTH_KIT
    }
#endif
    self.automaticallyAdjustsScrollViewInsets = NO;
    UILabel *info = [[UILabel alloc] initWithFrame:CGRectMake(5.0f, 65.0f, 250.0f, 20.0f)];
    info.textColor = [UIColor colorWithRed:0.172 green:0.478 blue:0.733 alpha:1.000];
    info.text = @"Systolic";
    [self.view addSubview:info];
    UIScrollView *graphScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 85, 320, 200)];
    graphScroll.tag = 1;
    graphScroll.delegate = self;
    [self.view addSubview:graphScroll];
    CGFloat width = 32+50*data.count;
    if (width < 320.0f) width = 320.0f;
    if (data.count < 2) {
        graphScroll.hidden = YES;
    }
    graphScroll.contentSize = CGSizeMake(width+5, 200);
    BEMSimpleLineGraphView *myGraph = [[BEMSimpleLineGraphView alloc] initWithFrame:CGRectMake(0, 0, width, 200)];
    myGraph.delegate = self;
    myGraph.dataSource = self;
    myGraph.colorTop = [UIColor colorWithRed:0.820 green:0.534 blue:0.193 alpha:1.000];
    myGraph.colorBottom = [UIColor colorWithRed:0.820 green:0.534 blue:0.193 alpha:1.000];
    myGraph.colorLine = [UIColor whiteColor];
    myGraph.colorXaxisLabel = [UIColor whiteColor];
    myGraph.colorYaxisLabel = [UIColor whiteColor];
    myGraph.widthLine = 3.0;
    myGraph.enableTouchReport = YES;
    myGraph.enablePopUpReport = YES;
    //myGraph.enableBezierCurve = YES;
    myGraph.enableYAxisLabel = YES;
    myGraph.autoScaleYAxis = YES;
    myGraph.alwaysDisplayDots = NO;
    myGraph.enableReferenceAxisLines = YES;
    myGraph.enableReferenceAxisFrame = YES;
    myGraph.animationGraphStyle = BEMLineAnimationNone;
    myGraph.animationGraphEntranceTime = 0;
    myGraph.tag = 1;
    myGraph.userInteractionEnabled = YES;
    [graphScroll addSubview:myGraph];
    
    graphScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 315, 320, 200)];
    graphScroll.tag = 2;
    graphScroll.delegate = self;
    [self.view addSubview:graphScroll];
    if (data.count < 2) {
        graphScroll.hidden = YES;
    }
    info = [[UILabel alloc] initWithFrame:CGRectMake(5.0f, 295.0f, 250.0f, 20.0f)];
    info.textColor = [UIColor colorWithRed:0.172 green:0.478 blue:0.733 alpha:1.000];
    info.text = @"Diastolic";
    [self.view addSubview:info];
    graphScroll.contentSize = CGSizeMake(width+5, 200);
    myGraph = [[BEMSimpleLineGraphView alloc] initWithFrame:CGRectMake(0, 0, width, 200)];
    myGraph.delegate = self;
    myGraph.dataSource = self;
    myGraph.colorTop = [UIColor colorWithRed:31.0/255.0 green:187.0/255.0 blue:166.0/255.0 alpha:1.0];
    myGraph.colorBottom = [UIColor colorWithRed:31.0/255.0 green:187.0/255.0 blue:166.0/255.0 alpha:1.0];
    myGraph.colorLine = [UIColor whiteColor];
    myGraph.colorXaxisLabel = [UIColor whiteColor];
    myGraph.colorYaxisLabel = [UIColor whiteColor];
    myGraph.widthLine = 3.0;
    myGraph.enableTouchReport = YES;
    myGraph.enablePopUpReport = YES;
    //myGraph.enableBezierCurve = YES;
    myGraph.enableYAxisLabel = YES;
    myGraph.autoScaleYAxis = YES;
    myGraph.alwaysDisplayDots = NO;
    myGraph.enableReferenceAxisLines = YES;
    myGraph.enableReferenceAxisFrame = YES;
    myGraph.animationGraphStyle = BEMLineAnimationNone;
    myGraph.animationGraphEntranceTime = 0;
    myGraph.tag = 2;
    myGraph.userInteractionEnabled = YES;
    [graphScroll addSubview:myGraph];
 
#ifdef USE_HEALTH_KIT
    if (![appDelegate isHealthKitAvailable] || ![[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"%@HealthKit",UUIDSTR_BLOOD_PRESSURE_SERVICE]]) {
#endif
        info = [[UILabel alloc] initWithFrame:CGRectMake(5.0f, 515.0f, 50.0f, 20.0f)];
        info.textColor = [UIColor colorWithRed:0.172 green:0.478 blue:0.733 alpha:1.000];
        info.text = @"MAP";
        [self.view addSubview:info];
        
        graphScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 535, 320, 200)];
        graphScroll.tag = 3;
        graphScroll.delegate = self;
        [self.view addSubview:graphScroll];
        if (data.count < 2) {
            graphScroll.hidden = YES;
        }
        graphScroll.contentSize = CGSizeMake(width+5, 200);
        myGraph = [[BEMSimpleLineGraphView alloc] initWithFrame:CGRectMake(0, 0, width, 200)];
        myGraph.delegate = self;
        myGraph.dataSource = self;
        myGraph.colorTop = [UIColor colorWithRed:0.172 green:0.478 blue:0.733 alpha:1.000];
        myGraph.colorBottom = [UIColor colorWithRed:0.172 green:0.478 blue:0.733 alpha:1.000];
        myGraph.colorLine = [UIColor whiteColor];
        myGraph.colorXaxisLabel = [UIColor whiteColor];
        myGraph.colorYaxisLabel = [UIColor whiteColor];
        myGraph.widthLine = 3.0;
        myGraph.enableTouchReport = YES;
        myGraph.enablePopUpReport = YES;
        //myGraph.enableBezierCurve = YES;
        myGraph.enableYAxisLabel = YES;
        myGraph.autoScaleYAxis = YES;
        myGraph.alwaysDisplayDots = NO;
        myGraph.enableReferenceAxisLines = YES;
        myGraph.enableReferenceAxisFrame = YES;
        myGraph.animationGraphStyle = BEMLineAnimationNone;
        myGraph.animationGraphEntranceTime = 0;
        myGraph.tag = 3;
        myGraph.userInteractionEnabled = YES;
        [graphScroll addSubview:myGraph];
        UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithTitle:@"Detail" style:UIBarButtonItemStylePlain target:self action:@selector(showTable:)];
        self.navigationItem.rightBarButtonItem = right;
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
#ifdef USE_HEALTH_KIT
    }
#endif

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showTable:(id)sender {
    ISBloodPressureTableController *controller = [[ISBloodPressureTableController alloc] initWithStyle:UITableViewStylePlain];
    [self.navigationController pushViewController:controller animated:YES];
}

- (float)covertUnit:(float)number unit:(NSString *)unit {
    if (number == NAN) {
        //NSLog(@"NAN");
        return 0;
    }
    if (number == INFINITY || number == -INFINITY) {
        //NSLog(@"INFINITY");
        return 0;
    }

    //NSLog(@"%f",number);

    switch ([[NSUserDefaults standardUserDefaults] integerForKey:@"pressureUnit"]) {
        case 0:{
            if ([unit isEqualToString:@"mmHg"]) {
                return number;
            }
            return number * 0.1333;
            break;
        }
        case 1:{
            if ([unit isEqualToString:@"mmHg"]) {
                return number * 7.5006;
            }
            return number;
        }
            break;
        default:
            break;
    }
    return number;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    UIScrollView *other;
    UIScrollView *another;
    if (scrollView.tag == 1) {
        other = (UIScrollView *)[self.view viewWithTag:2];
        another = (UIScrollView *)[self.view viewWithTag:3];
    }
    else if (scrollView.tag == 2) {
        other = (UIScrollView *)[self.view viewWithTag:1];
        another = (UIScrollView *)[self.view viewWithTag:3];
    }
    else {
        other = (UIScrollView *)[self.view viewWithTag:2];
        another = (UIScrollView *)[self.view viewWithTag:3];
    }
    [other setContentOffset:scrollView.contentOffset];
    [another setContentOffset:scrollView.contentOffset];
}

#pragma mark - SimpleLineGraph Data Source

- (NSInteger)numberOfPointsInLineGraph:(BEMSimpleLineGraphView *)graph {
#ifdef USE_HEALTH_KIT
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if ([appDelegate isHealthKitAvailable] && [[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"%@HealthKit",UUIDSTR_BLOOD_PRESSURE_SERVICE]]) {
        if (graph.tag == 1) {
            return data.count?data.count:0;
        }
        else if (graph.tag == 2) {
            return diastolicData.count?diastolicData.count:0;
        }
    }
    else {
#endif
        return data.count?data.count:0;
#ifdef USE_HEALTH_KIT
    }
#endif
    return 0;
}

- (CGFloat)lineGraph:(BEMSimpleLineGraphView *)graph valueForPointAtIndex:(NSInteger)index {
#ifdef USE_HEALTH_KIT
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if ([appDelegate isHealthKitAvailable] && [[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"%@HealthKit",UUIDSTR_BLOOD_PRESSURE_SERVICE]]) {
        HKQuantitySample *quantitySample = data[index];
        HKQuantity *quantity = quantitySample.quantity;
        switch ([[NSUserDefaults standardUserDefaults] integerForKey:@"pressureUnit"]) {
            case 0:{
                return [quantity doubleValueForUnit:[HKUnit millimeterOfMercuryUnit]];
                break;
            }
            case 1:{
                return [quantity doubleValueForUnit:[HKUnit pascalUnitWithMetricPrefix:HKMetricPrefixKilo]];
            }
                break;
            default:
                break;
        }
    }
    else {
#endif
        BloodPressureData *dis = [data objectAtIndex:index];
        if (graph.tag == 1) {
            return [self covertUnit:[dis.systolic floatValue] unit:dis.unit];
        }
        else if (graph.tag == 2) {
            return [self covertUnit:[dis.diastolic floatValue] unit:dis.unit];
        }
        else {
            return [self covertUnit:[dis.map floatValue] unit:dis.unit];
        }

#ifdef USE_HEALTH_KIT
    }
#endif
    return 0;
}

#pragma mark - SimpleLineGraph Delegate
- (NSString *)popUpSuffixForlineGraph:(BEMSimpleLineGraphView *)graph {
    switch ([[NSUserDefaults standardUserDefaults] integerForKey:@"pressureUnit"]) {
        case 0:
            return @"mmHg";
            break;
        case 1:
            return @"kPa";
            break;
        default:
            break;
    }
    return @"mmHg";
}

- (NSInteger)numberOfGapsBetweenLabelsOnLineGraph:(BEMSimpleLineGraphView *)graph {
    return 1;
}

- (NSString *)lineGraph:(BEMSimpleLineGraphView *)graph labelOnXAxisForIndex:(NSInteger)index {
#ifdef USE_HEALTH_KIT
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if ([appDelegate isHealthKitAvailable] && [[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"%@HealthKit",UUIDSTR_BLOOD_PRESSURE_SERVICE]]) {
        if (graph.tag == 1) {
            HKQuantitySample *quantitySample = data[index];
            return [formater stringFromDate:quantitySample.startDate];
        }
        else if (graph.tag == 2) {
            HKQuantitySample *quantitySample = diastolicData[index];
            return [formater stringFromDate:quantitySample.startDate];
        }
    }
#endif
    BloodPressureData *dis = [data objectAtIndex:index];
    return [formater stringFromDate:dis.timestamp];
}

- (void)lineGraph:(BEMSimpleLineGraphView *)graph didTouchGraphWithClosestIndex:(NSInteger)index {
    
}

- (void)lineGraph:(BEMSimpleLineGraphView *)graph didReleaseTouchFromGraphWithClosestIndex:(CGFloat)index {
    
}

- (void)lineGraphDidFinishLoading:(BEMSimpleLineGraphView *)graph {
    
}

@end
