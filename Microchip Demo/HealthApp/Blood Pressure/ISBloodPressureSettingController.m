//
//  ISBloodPressureSettingController.m
//  Health
//
//  Created by Rick on 2014/12/8.
//  Copyright (c) 2014年 Rick. All rights reserved.
//

#import "ISBloodPressureSettingController.h"
#import "AppDelegate.h"
#import "UUID.h"

@interface ISBloodPressureSettingController () {
    UISegmentedControl *unit;
    UISwitch *healthKitSwitch;
}

@end

@implementation ISBloodPressureSettingController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Settings";
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    unit = [[UISegmentedControl alloc] initWithItems:@[@"mmHg",@"kPa"]];
    unit.frame = CGRectMake(0.0f, 0.0f, 100.0f, 30.0f);
    unit.selectedSegmentIndex = [[NSUserDefaults standardUserDefaults] integerForKey:@"pressureUnit"];
    [unit addTarget:self action:@selector(setUnit:) forControlEvents:UIControlEventValueChanged];
#ifdef USE_HEALTH_KIT
    healthKitSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 50.0f, 30.0f)];
    [healthKitSwitch addTarget:self action:@selector(setUseHealthKit:) forControlEvents:UIControlEventValueChanged];
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if ([appDelegate isHealthKitAvailable]) {
        healthKitSwitch.on = [[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"%@HealthKit",UUIDSTR_BLOOD_PRESSURE_SERVICE]];
    }
#endif

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setUnit:(UISegmentedControl *)segment {
    [[NSUserDefaults standardUserDefaults] setObject:@(segment.selectedSegmentIndex) forKey:@"pressureUnit"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)setUseHealthKit:(UISwitch *)sender {
    [[NSUserDefaults standardUserDefaults] setObject:@(sender.on) forKey:[NSString stringWithFormat:@"%@HealthKit",UUIDSTR_BLOOD_PRESSURE_SERVICE]];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#ifdef USE_HEALTH_KIT
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if ([appDelegate isHealthKitAvailable]) {
        return 2;
    }
#endif
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    switch (indexPath.row) {
        case 0:
            cell.textLabel.text = @"Unit";
            cell.accessoryView = unit;
            break;
        case 1:
            cell.textLabel.text = @"HealthKit";
            cell.accessoryView = healthKitSwitch;
            break;
        default:
            break;
    }
    // Configure the cell...
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
