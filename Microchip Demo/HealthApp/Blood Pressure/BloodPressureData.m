//
//  BloodPressureData.m
//  Health
//
//  Created by Rick on 2014/12/11.
//  Copyright (c) 2014年 Rick. All rights reserved.
//

#import "BloodPressureData.h"


@implementation BloodPressureData

@dynamic diastolic;
@dynamic map;
@dynamic pulseRate;
@dynamic status;
@dynamic systolic;
@dynamic timestamp;
@dynamic userID;
@dynamic unit;

@end
