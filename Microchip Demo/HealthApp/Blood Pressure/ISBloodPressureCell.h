//
//  ISBloodPressureCell.h
//  Health
//
//  Created by Rick on 2014/12/8.
//  Copyright (c) 2014年 Rick. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISBloodPressureCell : UITableViewCell
@property (strong) UILabel *value;
@property (strong) UILabel *pulseRate;
@property (strong) UILabel *unit;
@property (strong) UILabel *user_id;
@property (strong) UILabel *time;

@end
