//
//  HKMyPeripheral.m
//  Health
//
//  Created by ILYAS on 27/01/16.
//  Copyright © 2016 Rick. All rights reserved.
//

#import "HKMyPeripheral.h"
#import "ISError.h"
#import "ReliableBurstData.h"
#import "AppDelegate.h"
#import "ISWeightParser.h"
#import "WeightData.h"
#import "ISHeartRateParser.h"
#import "HeartRateData.h"
#import "ISHTDataParser.h"
#import "HealthThermometerData.h"
#import "ISBloodPressureDataParser.h"
#import "BloodPressureData.h"
#import "ISGlucoseDataParser.h"
#import "GlucoseData.h"

@interface HKMyPeripheral () {
    BOOL isEndableTransparent;
    ISWeightParser *weightParser;
    ISHeartRateParser *heartRateParser;
    ISHTDataParser *healthThermometerParser;
    ISBloodPressureDataParser *bloodPressureParser;
    ISGlucoseDataParser *glucoseParser;
}
@end

@implementation HKMyPeripheral
@synthesize peripheral;
@synthesize transDataDelegate;
@synthesize proprietaryDelegate;
@synthesize deviceInfoDelegate;

@synthesize airPatchChar;
@synthesize transparentDataWriteChar;
@synthesize transparentDataReadChar;
@synthesize connectionParameterChar;
@synthesize backupConnectionParameter;
@synthesize connectionParameter;

@synthesize manufactureNameChar;
@synthesize modelNumberChar;
@synthesize serialNumberChar;
@synthesize hardwareRevisionChar;
@synthesize firmwareRevisionChar;
@synthesize softwareRevisionChar;
@synthesize systemIDChar;
@synthesize certDataListChar;
@synthesize specificChar1;
@synthesize specificChar2;

- (HKMyPeripheral *)init {
    self = [super init];
    if (self) {
        transDataDelegate = nil;
        proprietaryDelegate = nil;
        deviceInfoDelegate = nil;
        queuedTask = [[NSMutableArray alloc] init];
        self.transparentDataWriteChar = nil;
        self.transparentDataReadChar = nil;
        self.connectStaus = HK_MYPERIPHERAL_CONNECT_STATUS_IDLE;
        _canSendData = NO;
        _transmit = [[ReliableBurstData alloc] init];
        NSLog(@"%@",[_transmit version]);
        //weightParser = [[ISWeightParser alloc] init];
    }
    return self;
}

//DIS
- (void)readManufactureName {
    // NSLog(@"[MyPeripheral] readManufactureName");
    if (manufactureNameChar == nil) {
    }
    [peripheral readValueForCharacteristic:manufactureNameChar];
}

- (void)readModelNumber {
    // NSLog(@"[MyPeripheral] readModelNumber");
    [peripheral readValueForCharacteristic:modelNumberChar];
    
}

- (void)readSerialNumber {
    // NSLog(@"[MyPeripheral] readSerialNumber");
    [peripheral readValueForCharacteristic:serialNumberChar];
}

- (void)readHardwareRevision {
    //NSLog(@"[MyPeripheral] readHardwareRevision");
    [peripheral readValueForCharacteristic:hardwareRevisionChar];
}

- (void)readFirmwareRevision {
    //NSLog(@"[MyPeripheral] readFirmwareRevision");
    [peripheral readValueForCharacteristic:firmwareRevisionChar];
}

- (void)readSoftwareRevison {
    //NSLog(@"[MyPeripheral] readSoftwareRevison");
    [peripheral readValueForCharacteristic:softwareRevisionChar];
}

- (void)readSystemID {
    //NSLog(@"[MyPeripheral] readSystemID");
    [peripheral readValueForCharacteristic:systemIDChar];
}

- (void)readCertificationData {
    //NSLog(@"[MyPeripheral] readCertificationData");
    [peripheral readValueForCharacteristic:certDataListChar];
}

- (void)readSpecificUUID1 {
    //NSLog(@"[MyPeripheral] readCertificationData");
    [peripheral readValueForCharacteristic:specificChar1];
}

- (void)readSpecificUUID2 {
    //NSLog(@"[MyPeripheral] readCertificationData");
    [peripheral readValueForCharacteristic:specificChar2];
}

//Proprietary
- (CBCharacteristicWriteType)sendTransparentData:(NSData *)data type:(CBCharacteristicWriteType)type {
    NSLog(@"[MyPeripheral] sendTransparentData:%@", data);
    if (!_canSendData) {
        return CBCharacteristicWriteWithResponse;
    }
    if (transparentDataWriteChar == nil) {
        return CBCharacteristicWriteWithResponse;
    }
    CBCharacteristicWriteType actualType = type;
    if (type == CBCharacteristicWriteWithResponse) {
        if (!(transparentDataWriteChar.properties & CBCharacteristicPropertyWrite))
            actualType = CBCharacteristicWriteWithoutResponse;
    }
    else {
        if (!(transparentDataWriteChar.properties & CBCharacteristicPropertyWriteWithoutResponse))
            actualType = CBCharacteristicWriteWithResponse;
    }
    if (actualType == CBCharacteristicWriteWithoutResponse) {
        [_transmit reliableBurstTransmit:data withTransparentCharacteristic:transparentDataWriteChar];
    }
    else {
        [peripheral writeValue:data forCharacteristic:transparentDataWriteChar type:actualType];
    }
    
    return actualType;
}

- (void)setTransDataNotification:(BOOL)notify {
    NSLog(@"[MyPeripheral] setTransDataNotification UUID = %@",transparentDataReadChar.UUID);
    if (transparentDataReadChar != nil && (_isNotifying != notify)) {
        [peripheral setNotifyValue:notify forCharacteristic:transparentDataReadChar];
    }
}

- (void)checkIsAllowUpdateConnectionParameter {
    [self setUpdateConnectionParameterStep:HK_UPDATE_PARAMETERS_STEP_PREPARE];
    [self readConnectionParameters];
}

- (NSError *)setMaxConnectionInterval:(unsigned short)maxInterval connectionTimeout:(unsigned short)connectionTimeout connectionLatency:(unsigned short)connectionLatency{
    if (backupConnectionParameter.status == 0x01) {
        ISError *error = [[ISError alloc] initWithDomain:@"Last operation does not complete yet!" code:-1 userInfo:nil];
        [error setErrorDescription:@"Last operation does not complete yet!"];
        return error;
    }
    if (self.connectionParameterChar == nil) {
        ISError *error = [[ISError alloc] initWithDomain:@"Can't get the characteristic!" code:-1 userInfo:nil];
        [error setErrorDescription:@"Can't get the characteristic!"];
        return error;
    }
    
    if (maxInterval < 20) {
        ISError *error = [[ISError alloc] initWithDomain:@"Max Connection Interval must not be less than 20 ms!" code:-1 userInfo:nil];
        [error setErrorDescription:@"Max Connection Interval must not be less than 20 ms!"];
        return error;
    }
    else if (maxInterval > 2000) {
        ISError *error = [[ISError alloc] initWithDomain:@"Max Connection Interval must be less than 2000 ms!" code:-2 userInfo:nil];
        [error setErrorDescription:@"Max Connection Interval must be less than 2000 ms!"];
        return error;
    }
    else if (connectionTimeout < 500) {
        ISError *error = [[ISError alloc] initWithDomain:@"Connection Timeout must not be less than 500 ms!" code:-3 userInfo:nil];
        [error setErrorDescription:@"Connection Timeout must not be less than 500 ms!"];
        return error;
    }
    else if (connectionTimeout > 6000) {
        ISError *error = [[ISError alloc] initWithDomain:@"Connection Timeout must be less than 6000 ms!" code:-4 userInfo:nil];
        [error setErrorDescription:@"Connection Timeout must be less than 6000 ms!"];
        return error;
    }
    else if (connectionTimeout < maxInterval*3) {
        ISError *error = [[ISError alloc] initWithDomain:@"Connection Timeout must greater than three times Max Connection Interval!" code:-4 userInfo:nil];
        [error setErrorDescription:@"Connection Timeout must greater than three times Max Connection Interval!"];
        return error;
    }
    if (backupConnectionParameter.status != 0xff) {
        backupConnectionParameter.status = 0x01;
    }
    connectionParameter.status = 0xff;
    connectionParameter.maxInterval = maxInterval/1.25 + 0.5; //unit:1.25ms
    if (connectionParameter.maxInterval*1.25 > 2000) {
        connectionParameter.maxInterval--;
    }
    connectionParameter.minInterval = (maxInterval - 20)/1.25;
    if (connectionParameter.minInterval < 8) {
        connectionParameter.minInterval = 8;
    }
    connectionParameter.connectionTimeout = connectionTimeout/10; //unit:10ms
    int latency = 0;
    for (latency = 4; latency >= 0; latency--) {
        if (maxInterval * (latency+1) > 2000) {
            continue;
        }
        else if(connectionTimeout >= (maxInterval * (latency+1) *3)) {
            connectionParameter.latency = latency;
            break;
        }
    }
    
    if(connectionParameter.latency > connectionLatency){
        connectionParameter.latency = connectionLatency;
    }
    char *p = (char *)&connectionParameter;
    NSData *data = [[NSData alloc] initWithBytes:p length:sizeof(connectionParameter)];
    
    [peripheral writeValue:data forCharacteristic:self.connectionParameterChar type:CBCharacteristicWriteWithResponse];
    
    [self setUpdateConnectionParameterStep:HK_UPDATE_PARAMETERS_STEP_CHECK_RESULT];
    [self checkConnectionParameterStatus];
    
    //  }
    
    return nil;
}

- (void)readConnectionParameters {
    [peripheral readValueForCharacteristic:self.connectionParameterChar];
}

- (void)checkConnectionParameterStatus {
    //NSLog(@"[MyPeripheral] checkConnectionParameterStatus");
    [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(readConnectionParameters) userInfo:nil repeats:NO];
}

- (_CONNECTION_PARAMETER_FORMAT *)retrieveBackupConnectionParameter {
    return &backupConnectionParameter;
}

- (BOOL)compareBackupConnectionParameter:(_CONNECTION_PARAMETER_FORMAT *)parameter {
    backupConnectionParameter.status = 0x00; //set result to success for comapre because only compare when success
    if (memcmp(&backupConnectionParameter, parameter, sizeof(_CONNECTION_PARAMETER_FORMAT)))
        return FALSE;
    else
        return TRUE;
}

- (void)updateBackupConnectionParameter:(_CONNECTION_PARAMETER_FORMAT *)parameter {
    backupConnectionParameter.status = parameter->status;
    backupConnectionParameter.minInterval = parameter->minInterval;
    backupConnectionParameter.maxInterval = parameter->maxInterval;
    backupConnectionParameter.latency = parameter->latency;
    backupConnectionParameter.connectionTimeout = parameter->connectionTimeout;
}

- (void)sendVendorMPEnable {
    if (self.airPatchChar == nil) {
        return;
    }
    [peripheral setNotifyValue:TRUE forCharacteristic:self.airPatchChar];
    struct HK_AIR_PATCH_COMMAND_FORMAT command;
    command.commandID = AIR_PATCH_COMMAND_VENDOR_MP_ENABLE;
    NSData *data = [[NSData alloc] initWithBytes:&command length:1];
    //NSLog(@"[MyPeripheral] vendorMPEnable data = %@", data);
    [peripheral writeValue:data forCharacteristic:self.airPatchChar type:CBCharacteristicWriteWithResponse];
    self.vendorMPEnable = true;
}

- (void)writeE2promValue: (short)address length:(short)length data:(char *)data {
    if (self.airPatchChar == nil) {
        return;
    }
    if (self.vendorMPEnable ==false)
        [self sendVendorMPEnable];
    struct HK_AIR_PATCH_COMMAND_FORMAT command;
    command.commandID = AIR_PATCH_COMMAND_E2PROM_WRITE;
    struct HK_WRITE_EEPROM_COMMAND_FORMAT *parameter = (struct _WRITE_EEPROM_COMMAND_FORMAT *)command.parameters;
    parameter->addr[0] = address >> 8;
    parameter->addr[1] = address & 0xff;
    int dataLen = (length > 16) ? 16 : length;
    parameter->length = dataLen;
    memcpy(parameter->data, data, dataLen);
    
    NSData *commandData = [[NSData alloc] initWithBytes:&command length:length+4];
    //NSLog(@"[MyPeripheral] writeE2promValue address = %x, data = %@", address, commandData);
    [peripheral writeValue:commandData forCharacteristic:self.airPatchChar type:CBCharacteristicWriteWithResponse];
}

- (void)readE2promValue: (short)address length:(short)length {
    if (self.airPatchChar == nil) {
        return;
    }
    if (self.vendorMPEnable ==false)
        [self sendVendorMPEnable];
    struct HK_AIR_PATCH_COMMAND_FORMAT command;
    command.commandID = AIR_PATCH_COMMAND_E2PROM_READ;
    struct HK_WRITE_EEPROM_COMMAND_FORMAT *parameter = (struct _WRITE_EEPROM_COMMAND_FORMAT *)command.parameters;
    parameter->addr[0] = address >> 8;
    parameter->addr[1] = address & 0xff;
    parameter->length = length;
    NSData *data = [[NSData alloc] initWithBytes:&command length:4];
    [peripheral writeValue:data forCharacteristic:self.airPatchChar type:CBCharacteristicWriteWithResponse];
}

- (void)writeMemoryValue: (short)address length:(short)length data:(char *)data {
    if (self.airPatchChar == nil) {
        return;
    }
    if (self.vendorMPEnable ==false)
        [self sendVendorMPEnable];
    struct HK_AIR_PATCH_COMMAND_FORMAT command;
    command.commandID = AIR_PATCH_COMMAND_XMEMOTY_WRITE;
    struct HK_WRITE_EEPROM_COMMAND_FORMAT *parameter = (struct _WRITE_EEPROM_COMMAND_FORMAT *)command.parameters;
    parameter->addr[0] = address >> 8;
    parameter->addr[1] = address & 0xff;
    int dataLen = (length > 16) ? 16 : length;
    parameter->length = dataLen;
    memcpy(parameter->data, data, dataLen);
    
    NSData *commandData = [[NSData alloc] initWithBytes:&command length:length+4];
    //NSLog(@"[MyPeripheral] writeE2promValue address = %x, data = %@", address, commandData);
    [peripheral writeValue:commandData forCharacteristic:self.airPatchChar type:CBCharacteristicWriteWithResponse];
}

- (void)readMemoryValue: (short)address length:(short)length {
    if (self.airPatchChar == nil) {
        return;
    }
    if (self.vendorMPEnable ==false)
        [self sendVendorMPEnable];
    struct HK_AIR_PATCH_COMMAND_FORMAT command;
    command.commandID = AIR_PATCH_COMMAND_XMEMOTY_READ;
    struct HK_WRITE_EEPROM_COMMAND_FORMAT *parameter = (struct _WRITE_EEPROM_COMMAND_FORMAT *)command.parameters;
    parameter->addr[0] = address >> 8;
    parameter->addr[1] = address & 0xff;
    parameter->length = length;
    NSData *data = [[NSData alloc] initWithBytes:&command length:4];
    [peripheral writeValue:data forCharacteristic:self.airPatchChar type:CBCharacteristicWriteWithResponse];
}

- (void)changePeripheralName: (NSString *)name {
    if (self.airPatchChar == nil) {
        return;
    }
    
    if (self.vendorMPEnable ==false) {
        [self sendVendorMPEnable];
        NSMethodSignature *sgn = [[self class] instanceMethodSignatureForSelector:@selector(changePeripheralName:)];
        NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:sgn];
        [invocation setTarget:self];
        [invocation setSelector:@selector(changePeripheralName:)];
        [invocation setArgument:&name atIndex:2];
        [invocation retainArguments];
        [queuedTask addObject:invocation];
        return;
    }
    
    struct HK_AIR_PATCH_COMMAND_FORMAT command;
    command.commandID = AIR_PATCH_COMMAND_E2PROM_WRITE;
    struct HK_WRITE_EEPROM_COMMAND_FORMAT *parameter = (struct _WRITE_EEPROM_COMMAND_FORMAT *)command.parameters;
    parameter->addr[0] = 0x00;
    parameter->addr[1] = 0x0b;
    parameter->length = 16;
    memset(parameter->data, 0x00, 16);
    memset(deviceName, 0x20, 16);
    int dataLen = ([name length] > 16) ? 16 : [name length];
    memcpy(parameter->data, [name UTF8String], dataLen);
    memcpy(deviceName, parameter->data, dataLen);
    
    NSData *data = [[NSData alloc] initWithBytes:&command length:20];
    NSLog(@"[MyPeripheral] changePeripheralName data = %@, %x %x %x %x, %@", data, parameter->data[0], parameter->data[1], parameter->data[2], parameter->data[3], name);
    [peripheral writeValue:data forCharacteristic:self.airPatchChar type:CBCharacteristicWriteWithResponse];
    self.airPatchAction = AIR_PATCH_ACTION_CHANGE_DEVICE_NAME;
    NSMethodSignature *sgn = [[self class] instanceMethodSignatureForSelector:@selector(changePeripheralNameMemory:)];
    NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:sgn];
    [invocation setTarget:self];
    [invocation setSelector:@selector(changePeripheralNameMemory:)];
    [invocation setArgument:&name atIndex:2];
    [invocation retainArguments];
    [queuedTask addObject:invocation];
}

- (void)changePeripheralNameMemory:(NSString *)name{
    if (self.airPatchChar == nil) {
        return;
    }
    
    if (self.vendorMPEnable ==false)
        [self sendVendorMPEnable];
    struct HK_AIR_PATCH_COMMAND_FORMAT command;
    command.commandID = AIR_PATCH_COMMAND_XMEMOTY_WRITE;
    struct HK_WRITE_EEPROM_COMMAND_FORMAT *parameter = (struct _WRITE_EEPROM_COMMAND_FORMAT *)command.parameters;
    parameter->addr[0] = 0x4E;
    parameter->addr[1] = 0x0b;
    parameter->length = 16;
    memset(parameter->data, 0x00, 16);
    int dataLen = ([name length] > 16) ? 16 : [name length];
    memcpy(parameter->data, [name UTF8String], dataLen);
    NSData *data = [[NSData alloc] initWithBytes:&command length:20];
    NSLog(@"[MyPeripheral] changePeripheralNameMemory data = %@, %x %x %x %x, len=%d", data, parameter->data[0], parameter->data[1], parameter->data[2], parameter->data[3], dataLen);
    
    [peripheral writeValue:data forCharacteristic:self.airPatchChar type:CBCharacteristicWriteWithResponse];
    self.airPatchAction = AIR_PATCH_ACTION_CHANGE_DEVICE_NAME_MEMORY;
}


- (void)checkQueuedTask {
    if ([queuedTask count]) {
        NSInvocation *invocation = [queuedTask objectAtIndex:0];
        [invocation invoke];
        [queuedTask removeObjectAtIndex:0];
    }
}

- (void)updateAirPatchEvent: (NSData *)returnEvent {
    [_transmit decodeReliableBurstTransmitEvent:returnEvent];
    char buf[20];
    //NSLog(@"[MyPeripheral] updateAirPatchEvent, %@", returnEvent);
    [returnEvent getBytes:buf length:[returnEvent length]];
    _AIR_PATCH_EVENT_FORMAT *receivedEvent = (_AIR_PATCH_EVENT_FORMAT *)buf;
    switch (receivedEvent->commandID) {
        case AIR_PATCH_COMMAND_VENDOR_MP_ENABLE: {
            if (receivedEvent->status == AIR_PATCH_SUCCESS) {
                [self checkQueuedTask];
            }
        }
            break;
        case AIR_PATCH_COMMAND_E2PROM_WRITE:
            if (self.airPatchAction == AIR_PATCH_ACTION_CHANGE_DEVICE_NAME) {
                if (receivedEvent->status == AIR_PATCH_SUCCESS) {
                    [self checkQueuedTask];
                }
                else {
                    if (proprietaryDelegate && [(NSObject *)proprietaryDelegate respondsToSelector:@selector(MyPeripheral:didChangePeripheralName:)]) {
                        ISError *error = [[ISError alloc] initWithDomain:@"Change peripheral name fail!" code:-2 userInfo:nil];
                        [error setErrorDescription:@"Write device name to EEPROM fail!"];
                        [proprietaryDelegate MyPeripheral:self didChangePeripheralName:error];
                    }
                }
            }
            break;
        case AIR_PATCH_COMMAND_XMEMOTY_WRITE:
            if(self.airPatchAction == AIR_PATCH_ACTION_CHANGE_DEVICE_NAME_MEMORY){
                ISError *error =nil;
                if (receivedEvent->status != AIR_PATCH_SUCCESS) {
                    error = [[ISError alloc] initWithDomain:@"Change peripheral name fail!" code:-3 userInfo:nil];
                    [error setErrorDescription:@"Write device name to Xmemory fail!"];
                }
                if (proprietaryDelegate&&[(NSObject *)proprietaryDelegate respondsToSelector:@selector(MyPeripheral:didChangePeripheralName:)]) {
                    [proprietaryDelegate MyPeripheral:self didChangePeripheralName:error];
                }
            }
            else {
                ISError *error =nil;
                if (receivedEvent->status != AIR_PATCH_SUCCESS) {
                    error = [[ISError alloc] initWithDomain:@"Write Memory Address fail!" code:-3 userInfo:nil];
                    [error setErrorDescription:@"Write to Xmemory fail!"];
                }
                if ([proprietaryDelegate respondsToSelector:@selector(MyPeripheral:didWriteMemoryAddress:)]) {
                    [proprietaryDelegate MyPeripheral:self didWriteMemoryAddress:error];
                }
            }
            break;
        case AIR_PATCH_COMMAND_XMEMOTY_READ:
            if (receivedEvent->status == AIR_PATCH_SUCCESS) {
                if ([proprietaryDelegate respondsToSelector:@selector(MyPeripheral:didReceiveMemoryAddress:length:data:)]) {
                    short length = 0;
                    [returnEvent getBytes:&length range:NSMakeRange(4, 1)];
                    [proprietaryDelegate MyPeripheral:self didReceiveMemoryAddress:[returnEvent subdataWithRange:NSMakeRange(2, 2)] length:length data:[returnEvent subdataWithRange:NSMakeRange(5, length)]];
                }
            }
            break;
            /*case AIR_PATCH_COMMAND_READ:
             if (receivedEvent->status == AIR_PATCH_SUCCESS) {
             [_transmit decodeReliableBurstTransmitEvent:returnEvent];
             }
             break;*/
        default:
            break;
    }
    
}

- (void)setWeihgtScaleFeature:(NSData *)data {
    if (!weightParser) {
        weightParser = [[ISWeightParser alloc] init];
    }
    [weightParser decodeFeature:data];
}

- (void)saveWeihgtScaleData:(NSData *)data {
    if (!weightParser) {
        weightParser = [[ISWeightParser alloc] init];
    }
    NSDictionary *weight = [weightParser decodeData:data];
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = appDelegate.managedObjectContext;
    WeightData *info = [NSEntityDescription insertNewObjectForEntityForName:@"WeightData" inManagedObjectContext:context];
    info.weight = weight[@"weight"];
    info.height = weight[@"height"];
    info.bmi = weight[@"bmi"];
    info.userID = weight[@"user_id"];
    info.timestamp = weight[@"time"];
    info.weightUnit = weight[@"unit_weight"];
    info.heightUnit = weight[@"unit_height"];
    NSError *error;
    if (![context save:&error]) {
        NSArray* detailedErrors = [[error userInfo] objectForKey:NSDetailedErrorsKey];
        if(detailedErrors != nil && [detailedErrors count] > 0) {
            for(NSError* detailedError in detailedErrors) {
                NSLog(@"  DetailedError: %@", [detailedError userInfo]);
            }
        }
        else {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
    }
    if (transDataDelegate && [transDataDelegate respondsToSelector:@selector(MyPeripheral:didUpdateWeihgtScaleData:error:)]) {
        [transDataDelegate MyPeripheral:self didUpdateWeihgtScaleData:weight error:nil];
    }
    
}

- (void)saveBatteryLevel:(NSInteger)level {
    if (transDataDelegate && [transDataDelegate respondsToSelector:@selector(MyPeripheral:didUpdateBatteryLevel:error:)]) {
        [transDataDelegate MyPeripheral:self didUpdateBatteryLevel:level error:nil];
    }
}

- (void)setSensorLocation:(NSData *)data {
    if (transDataDelegate && [transDataDelegate respondsToSelector:@selector(MyPeripheral:didUpdateSensorLocation:error:)]) {
        uint8_t *location;
        [data getBytes:&location length:data.length];
        [transDataDelegate MyPeripheral:self didUpdateSensorLocation:(SensorLocation)location error:nil];
    }
}

- (void)saveHeartRateData:(NSData *)data {
    if (!heartRateParser) {
        heartRateParser = [[ISHeartRateParser alloc] init];
    }
    NSDictionary *heartRate = [heartRateParser decodeData:data];
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = appDelegate.managedObjectContext;
    HeartRateData *info = [NSEntityDescription insertNewObjectForEntityForName:@"HeartRateData" inManagedObjectContext:context];
    info.heartRateValue = heartRate[@"heart_rate"];
    info.energyExpended = heartRate[@"energy_expended"];
    info.sensorContactStatus = heartRate[@"sensor_contact_status"];
    info.rr = heartRate[@"RR_Interval"];
    info.timestamp = [NSDate date];
    NSError *error;
    if (![context save:&error]) {
        NSArray* detailedErrors = [[error userInfo] objectForKey:NSDetailedErrorsKey];
        if(detailedErrors != nil && [detailedErrors count] > 0) {
            for(NSError* detailedError in detailedErrors) {
                NSLog(@"  DetailedError: %@", [detailedError userInfo]);
            }
        }
        else {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
    }
    if (transDataDelegate && [transDataDelegate respondsToSelector:@selector(MyPeripheral:didUpdateHeartRateData:error:)]) {
        [transDataDelegate MyPeripheral:self didUpdateHeartRateData:heartRate error:nil];
    }
}

- (void)resetEnergyExpended {
    if (_heartRateControlPoint) {
        uint8_t t = 1;
        NSData *data = [[NSData alloc] initWithBytes:&t length:1];
        [peripheral writeValue:data forCharacteristic:_heartRateControlPoint type:CBCharacteristicWriteWithResponse];
    }
}

- (void)setTemperatureType:(NSData *)data {
    if (transDataDelegate && [transDataDelegate respondsToSelector:@selector(MyPeripheral:didUpdateTemperatureType:error:)]) {
        uint8_t *type;
        [data getBytes:&type length:data.length];
        [transDataDelegate MyPeripheral:self didUpdateTemperatureType:(TemperatureType)type error:nil];
    }
}

- (void)saveTemperatureData:(NSData *)data isIntermediate:(BOOL)isIntermediate {
    if (!healthThermometerParser) {
        healthThermometerParser = [[ISHTDataParser alloc] init];
    }
    NSDictionary *health = [healthThermometerParser decodeData:data];
    if (!isIntermediate) {
        AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
        NSManagedObjectContext *context = appDelegate.managedObjectContext;
        HealthThermometerData *info = [NSEntityDescription insertNewObjectForEntityForName:@"HealthThermometerData" inManagedObjectContext:context];
        info.value = health[@"temperature"];
        info.type = health[@"temperature_type"];
        info.unit = health[@"uint"];
        info.timestamp = health[@"timestamp"];
        NSError *error;
        if (![context save:&error]) {
            NSArray* detailedErrors = [[error userInfo] objectForKey:NSDetailedErrorsKey];
            if(detailedErrors != nil && [detailedErrors count] > 0) {
                for(NSError* detailedError in detailedErrors) {
                    NSLog(@"  DetailedError: %@", [detailedError userInfo]);
                }
            }
            else {
                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
            }
        }
    }
    if (transDataDelegate && [transDataDelegate respondsToSelector:@selector(MyPeripheral:didUpdateTemperatureData:isIntermediate:error:)]) {
        [transDataDelegate MyPeripheral:self didUpdateTemperatureData:health isIntermediate:isIntermediate error:nil];
    }
}

- (void)setMeasurementInterval:(NSInteger)interval {
    NSMutableData *data = [NSMutableData data];
    uint16_t temp = (uint16_t)interval;
    [data appendBytes:&temp length:sizeof(temp)];
    if (_healthManufatureInterval) {
        [self.peripheral writeValue:data forCharacteristic:_healthManufatureInterval type:CBCharacteristicWriteWithResponse];
    }
}

- (void)updateMeasurementInterval:(NSInteger)interval {
    if (transDataDelegate && [transDataDelegate respondsToSelector:@selector(MyPeripheral:didUpdateMeasurementInterval:error:)]) {
        [transDataDelegate MyPeripheral:self didUpdateMeasurementInterval:interval error:nil];
    }
}

- (void)setBloodPressureFeature:(NSData *)data {
    if (!bloodPressureParser) {
        bloodPressureParser = [[ISBloodPressureDataParser alloc] init];
    }
    [bloodPressureParser decodeFeature:data];
    
}

- (void)saveBloodPressureData:(NSData *)data isIntermediateCuffPressure:(BOOL)isIntermediate {
    if (!bloodPressureParser) {
        bloodPressureParser = [[ISBloodPressureDataParser alloc] init];
    }
    NSDictionary *health = [bloodPressureParser decodeData:data];
    if (!isIntermediate) {
        AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
        NSManagedObjectContext *context = appDelegate.managedObjectContext;
        BloodPressureData *info = [NSEntityDescription insertNewObjectForEntityForName:@"BloodPressureData" inManagedObjectContext:context];
        info.systolic = health[@"systolic"];
        info.diastolic = health[@"diastolic"];
        info.map = health[@"map"];
        info.unit = health[@"uint"];
        info.timestamp = health[@"timestamp"];
        info.pulseRate = health[@"pulse_rate"];
        info.userID = health[@"user_id"];
        info.status = health[@"ststus"];
        NSError *error;
        if (![context save:&error]) {
            NSArray* detailedErrors = [[error userInfo] objectForKey:NSDetailedErrorsKey];
            if(detailedErrors != nil && [detailedErrors count] > 0) {
                for(NSError* detailedError in detailedErrors) {
                    NSLog(@"  DetailedError: %@", [detailedError userInfo]);
                }
            }
            else {
                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
            }
        }
    }
    if (transDataDelegate && [transDataDelegate respondsToSelector:@selector(MyPeripheral:didUpdateBloodPressureData:isIntermediateCuffPressure:error:)]) {
        [transDataDelegate MyPeripheral:self didUpdateBloodPressureData:health isIntermediateCuffPressure:isIntermediate error:nil];
    }
    
}

- (void)setGlucoseFeature:(NSData *)data {
    if (!glucoseParser) {
        glucoseParser = [[ISGlucoseDataParser alloc] init];
    }
    [glucoseParser decodeFeature:data];
}

- (void)saveGlucoseData:(NSData *)data {
    if (!glucoseParser) {
        glucoseParser = [[ISGlucoseDataParser alloc] init];
    }
    NSDictionary *health = [glucoseParser decodeData:data];
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = appDelegate.managedObjectContext;
    GlucoseData *info = [NSEntityDescription insertNewObjectForEntityForName:@"GlucoseData" inManagedObjectContext:context];
    info.sequence_number = health[@"sequence_number"];
    info.base_time = health[@"base_time"];
    info.uint = health[@"uint"];
    info.concentration = health[@"concentration"];
    info.type = health[@"type"];
    info.sample_location = health[@"sample_location"];
    info.status = health[@"status"];
    info.context = health[@"context"];
    NSError *error;
    if (![context save:&error]) {
        NSArray* detailedErrors = [[error userInfo] objectForKey:NSDetailedErrorsKey];
        if(detailedErrors != nil && [detailedErrors count] > 0) {
            for(NSError* detailedError in detailedErrors) {
                NSLog(@"  DetailedError: %@", [detailedError userInfo]);
            }
        }
        else {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
    }
    if (transDataDelegate && [transDataDelegate respondsToSelector:@selector(MyPeripheral:didUpdateGlucoseData:error:)]) {
        [transDataDelegate MyPeripheral:self didUpdateGlucoseData:health error:nil];
    }
    
}

- (void)saveGlucoseContextData:(NSData *)data {
    if (!glucoseParser) {
        glucoseParser = [[ISGlucoseDataParser alloc] init];
    }
    NSDictionary *health = [glucoseParser decodeContextData:data];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"GlucoseData" inManagedObjectContext:appDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    NSSortDescriptor* sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"base_time" ascending:NO];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"sequence_number == %@", health[@"sequence_number"]];
    [fetchRequest setPredicate:predicate];
    NSArray *temp = [appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:nil];
    if ([temp count]>0) {
        NSManagedObjectContext *context = appDelegate.managedObjectContext;
        GlucoseData *info = [temp objectAtIndex:0];
        info.carbohydrateID = health[@"carbohydrateID"];
        info.carbohydrate = health[@"carbohydrate"];
        info.meal = health[@"meal"];
        info.tester = health[@"tester"];
        info.health = health[@"health"];
        info.exercise_duration = health[@"exercise_duration"];
        info.exercise_intensity = health[@"exercise_intensity"];
        info.medicationID = health[@"medicationID"];
        info.medication = health[@"medication"];
        info.medication_uint = health[@"medication_uint"];
        info.hbA1c = health[@"HbA1c"];
        NSError *error;
        if (![context save:&error]) {
            NSArray* detailedErrors = [[error userInfo] objectForKey:NSDetailedErrorsKey];
            if(detailedErrors != nil && [detailedErrors count] > 0) {
                for(NSError* detailedError in detailedErrors) {
                    NSLog(@"  DetailedError: %@", [detailedError userInfo]);
                }
            }
            else {
                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
            }
        }
    }
    if (transDataDelegate && [transDataDelegate respondsToSelector:@selector(MyPeripheral:didUpdateGlucoseContextData:error:)]) {
        [transDataDelegate MyPeripheral:self didUpdateGlucoseContextData:health error:nil];
    }
}

- (void)executeGlucoseOp:(GlucoseOpCode)op operator:(GlucoseOperator)Operator andOperand:(NSArray *)operand {
    if (!glucoseParser) {
        glucoseParser = [[ISGlucoseDataParser alloc] init];
    }
    NSData *data = [glucoseParser encodeOp:op operator:Operator andOperand:operand];
    if (data && _glucoseRecordAccessControlPoint) {
        [peripheral writeValue:data forCharacteristic:_glucoseRecordAccessControlPoint type:CBCharacteristicWriteWithResponse];
    }
}

- (void)updateGlucoseOpResponse:(NSData *)data {
    if (!glucoseParser) {
        glucoseParser = [[ISGlucoseDataParser alloc] init];
    }
    NSDictionary *dis = [glucoseParser decodeOp:data];
    if (dis) {
        switch ([dis[@"OP"] integerValue]) {
            case GlucoseOpCode_ResponseCode:
                if (transDataDelegate && [transDataDelegate respondsToSelector:@selector(MyPeripheral:didExecuteGlucoseOp:error:)]) {
                    [transDataDelegate MyPeripheral:self didExecuteGlucoseOp:[dis[@"ResponseOP"] integerValue] error:dis[@"Error"]];
                }
                break;
            case GlucoseOpCode_Number_of_StoredRecords:
                if (transDataDelegate && [transDataDelegate respondsToSelector:@selector(MyPeripheral:didUpdateGlucoseDataNumber:error:)]) {
                    [transDataDelegate MyPeripheral:self didUpdateGlucoseDataNumber:[dis[@"Number"] integerValue] error:nil];
                }
                break;
            default:
                break;
        }
    }
}

@end

