//
//  ISGlucoseSettingController.m
//  Health
//
//  Created by Rick on 2014/12/16.
//  Copyright (c) 2014年 Rick. All rights reserved.
//

#import "ISGlucoseSettingController.h"
#import "HKMyPeripheral.h"
#import "AppDelegate.h"
#import "UUID.h"
#import "UIActionSheet+BlockExtensions.h"
#import "UIAlertView+BlockExtensions.h"

@interface ISGlucoseSettingController () {
    UISegmentedControl *unit;
    UISwitch *healthKitSwitch;
}

@end

@implementation ISGlucoseSettingController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (_peripheral) {
        [_peripheral.peripheral setNotifyValue:YES forCharacteristic:_peripheral.glucoseRecordAccessControlPoint];
        [_peripheral executeGlucoseOp:GlucoseOpCode_ReportNumber_of_StoredRecords operator:GlucoseOperator_AllRecords andOperand:nil];
    }
    self.title = @"Settings";
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    unit = [[UISegmentedControl alloc] initWithItems:@[@"kg/L",@"mol/L"]];
    unit.frame = CGRectMake(0.0f, 0.0f, 100.0f, 30.0f);
    unit.selectedSegmentIndex = [[NSUserDefaults standardUserDefaults] integerForKey:@"glucoseUnit"];
    [unit addTarget:self action:@selector(setUnit:) forControlEvents:UIControlEventValueChanged];
#ifdef USE_HEALTH_KIT
    healthKitSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 50.0f, 30.0f)];
    [healthKitSwitch addTarget:self action:@selector(setUseHealthKit:) forControlEvents:UIControlEventValueChanged];
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if ([appDelegate isHealthKitAvailable]) {
        healthKitSwitch.on = [[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"%@HealthKit",UUIDSTR_GLUCOSE_SERVICE]];
    }
#endif

 }

- (void)dealloc {
    [_peripheral.peripheral setNotifyValue:NO forCharacteristic:_peripheral.glucoseRecordAccessControlPoint];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setUnit:(UISegmentedControl *)segment {
    [[NSUserDefaults standardUserDefaults] setObject:@(segment.selectedSegmentIndex) forKey:@"glucoseUnit"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)setUseHealthKit:(UISwitch *)sender {
    [[NSUserDefaults standardUserDefaults] setObject:@(sender.on) forKey:[NSString stringWithFormat:@"%@HealthKit",UUIDSTR_GLUCOSE_SERVICE]];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)setNumber:(NSInteger)number {
    _number = number;
    [self.tableView reloadData];
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return 5;
            break;
        case 1: {
#ifdef USE_HEALTH_KIT
            AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
            if ([appDelegate isHealthKitAvailable]) {
                return 2;
            }
#endif
            return 1;
        }
        default:
            break;
    }
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    switch (indexPath.section) {
        case 0:
            switch (indexPath.row) {
                case 0:
                    cell.textLabel.text = @"Report stored records";
                    cell.accessoryView = nil;
                    break;
                case 1:
                    cell.textLabel.text = @"Delete stored records";
                    cell.accessoryView = nil;
                    break;
                case 2:
                    cell.textLabel.text = @"Abort operation";
                    cell.accessoryView = nil;
                    break;
                case 3:
                    cell.textLabel.text = @"Report number of stored records";
                    cell.accessoryView = nil;
                    break;
                case 4:
                    cell.textLabel.text = [NSString stringWithFormat:@"Records number:%ld",(long)_number];
                    cell.accessoryView = nil;
                    break;
                    
                default:
                    break;
            }
            break;
        case 1:
            switch (indexPath.row) {
                case 0:
                    cell.textLabel.text = @"Unit";
                    cell.accessoryView = unit;
                    break;
                case 1:
                    cell.textLabel.text = @"HealthKit";
                    cell.accessoryView = healthKitSwitch;
                    break;
                default:
                    break;
            }
        default:
            break;
    }
     // Configure the cell...
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.section) {
        case 0:
            switch (indexPath.row) {
                case 0: {
                    UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:@"Choose Operator" completionBlock:^(NSUInteger buttonIndex, UIActionSheet *actionSheet) {
                        if (buttonIndex == [actionSheet cancelButtonIndex]) {
                            return;
                        }
                        NSUInteger actionSheetIndex = buttonIndex+1;
                        switch (actionSheetIndex) {
                            case GlucoseOperator_Less_or_Equal:
                            case GlucoseOperator_Greater_or_Equal:
                            {
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Set number" message:@"" completionBlock:^(NSUInteger buttonIndex, UIAlertView *alertView) {
                                    if (buttonIndex != [alertView cancelButtonIndex]) {
                                        NSInteger number = [[alertView textFieldAtIndex:0].text integerValue];
                                        if (number >= _number || number <0) {
                                            return;
                                        }
                                        else {
                                            [_peripheral executeGlucoseOp:GlucoseOpCode_ReportStoredRecords operator:actionSheetIndex andOperand:@[@(number)]];
                                        }
                                    }
                                } cancelButtonTitle:@"Cancel" otherButtonTitles:@"Send",nil];
                                alert.alertViewStyle = UIAlertViewStylePlainTextInput;
                                [alert textFieldAtIndex:0].keyboardType = UIKeyboardTypeNumberPad;
                                [alert show];
                            }
                                break;
                            case GlucoseOperator_WithinRange:
                            {
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Set number" message:@"" completionBlock:^(NSUInteger buttonIndex, UIAlertView *alertView) {
                                    if (buttonIndex != [alertView cancelButtonIndex]) {
                                        NSInteger numberFrom = [[alertView textFieldAtIndex:0].text integerValue];
                                        NSInteger numberTo = [[alertView textFieldAtIndex:1].text integerValue];
                                        if (numberFrom >= _number || numberFrom <0) {
                                            return;
                                        }
                                        else if (numberTo >= _number || numberTo <0 ) {
                                            return;
                                        }
                                        else {
                                            [_peripheral executeGlucoseOp:GlucoseOpCode_ReportStoredRecords operator:actionSheetIndex andOperand:@[@(numberFrom),@(numberTo)]];
                                        }
                                    }
                                } cancelButtonTitle:@"Cancel" otherButtonTitles:@"Send",nil];
                                alert.alertViewStyle = UIAlertViewStyleLoginAndPasswordInput;
                                [alert textFieldAtIndex:0].keyboardType = UIKeyboardTypeNumberPad;
                                [alert textFieldAtIndex:0].placeholder = @"From";
                                [alert textFieldAtIndex:1].keyboardType = UIKeyboardTypeNumberPad;
                                [alert textFieldAtIndex:1].placeholder = @"To";
                                [alert textFieldAtIndex:1].secureTextEntry = NO;
                                [alert show];
                            }
                                break;
                            case GlucoseOperator_AllRecords:
                            case GlucoseOperator_LastRecord:
                            case GlucoseOperator_FirstRecord:
                                [_peripheral executeGlucoseOp:GlucoseOpCode_ReportStoredRecords operator:actionSheetIndex andOperand:nil];
                                break;
                            default:
                                break;
                        }
                        
                    } cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"All records",
                                             @"Less than or equal to",@"Greater than or equal to",
                                             @"Within range of (inclusive)",@"First record",
                                             @"Last record",nil];
                    [action showInView:self.view];
                }
                    break;
                case 1: {
                    UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:@"Choose Operator" completionBlock:^(NSUInteger buttonIndex, UIActionSheet *actionSheet) {
                        if (buttonIndex == [actionSheet cancelButtonIndex]) {
                            return;
                        }
                        NSUInteger actionSheetIndex = buttonIndex+1;
                        switch (actionSheetIndex) {
                            case GlucoseOperator_Less_or_Equal:
                            case GlucoseOperator_Greater_or_Equal:
                            {
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Set number" message:@"" completionBlock:^(NSUInteger buttonIndex, UIAlertView *alertView) {
                                    if (buttonIndex != [alertView cancelButtonIndex]) {
                                        NSInteger number = [[alertView textFieldAtIndex:0].text integerValue];
                                        if (number >= _number || number <0) {
                                            return;
                                        }
                                        else {
                                            [_peripheral executeGlucoseOp:GlucoseOpCode_DeleteStoredRecords operator:actionSheetIndex andOperand:@[@(number)]];
                                        }
                                        
                                    }
                                } cancelButtonTitle:@"Cancel" otherButtonTitles:@"Send",nil];
                                alert.alertViewStyle = UIAlertViewStylePlainTextInput;
                                [alert textFieldAtIndex:0].keyboardType = UIKeyboardTypeNumberPad;
                                [alert show];
                            }
                                break;
                            case GlucoseOperator_WithinRange:
                            {
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Set number" message:@"" completionBlock:^(NSUInteger buttonIndex, UIAlertView *alertView) {
                                    if (buttonIndex != [alertView cancelButtonIndex]) {
                                        NSInteger numberFrom = [[alertView textFieldAtIndex:0].text integerValue];
                                        NSInteger numberTo = [[alertView textFieldAtIndex:1].text integerValue];
                                        if (numberFrom >= _number || numberFrom <0) {
                                            return;
                                        }
                                        else if (numberTo >= _number || numberTo <0 ) {
                                            return;
                                        }
                                        else {
                                            [_peripheral executeGlucoseOp:GlucoseOpCode_DeleteStoredRecords operator:actionSheetIndex andOperand:@[@(numberFrom),@(numberTo)]];
                                        }
                                    }
                                } cancelButtonTitle:@"Cancel" otherButtonTitles:@"Send",nil];
                                alert.alertViewStyle = UIAlertViewStyleLoginAndPasswordInput;
                                [alert textFieldAtIndex:0].keyboardType = UIKeyboardTypeNumberPad;
                                [alert textFieldAtIndex:0].placeholder = @"From";
                                [alert textFieldAtIndex:1].keyboardType = UIKeyboardTypeNumberPad;
                                [alert textFieldAtIndex:1].placeholder = @"To";
                                [alert textFieldAtIndex:1].secureTextEntry = NO;
                                [alert show];
                            }
                                break;
                            case GlucoseOperator_AllRecords:
                            case GlucoseOperator_LastRecord:
                            case GlucoseOperator_FirstRecord:
                                [_peripheral executeGlucoseOp:GlucoseOpCode_DeleteStoredRecords operator:actionSheetIndex andOperand:nil];
                                break;
                            default:
                                break;
                        }
 
                    } cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"All records",
                                             @"Less than or equal to",@"Greater than or equal to",
                                             @"Within range of (inclusive)",@"First record",
                                             @"Last record",nil];
                    [action showInView:self.view];

                }
                    break;
                case 2:
                    [_peripheral executeGlucoseOp:GlucoseOpCode_AbortOperation operator:GlucoseOperator_NA andOperand:nil];
                    break;
                case 3:
                    [_peripheral executeGlucoseOp:GlucoseOpCode_ReportNumber_of_StoredRecords operator:GlucoseOperator_AllRecords andOperand:nil];
                    break;
                default:
                    break;
            }
            break;
            
        default:
            break;
    }
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
