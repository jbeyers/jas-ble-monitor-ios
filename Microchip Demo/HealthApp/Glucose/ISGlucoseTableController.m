//
//  ISGlucoseTableController.m
//  Health
//
//  Created by Rick on 2014/12/16.
//  Copyright (c) 2014年 Rick. All rights reserved.
//

#import "ISGlucoseTableController.h"
#import "ISGlucoseCell.h"
#import "GlucoseData.h"
#import "AppDelegate.h"
#import "HKMyPeripheral.h"

@interface ISGlucoseTableController () {
    NSMutableArray *data;
    NSDateFormatter *formater;
}

@end

@implementation ISGlucoseTableController

- (void)viewDidLoad {
    [super viewDidLoad];
    data = [[NSMutableArray alloc] init];
    [self.tableView registerClass:[ISGlucoseCell class] forCellReuseIdentifier:@"Cell"];
    self.title = @"History";
    formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"GlucoseData" inManagedObjectContext:appDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    NSSortDescriptor* sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"base_time" ascending:NO];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    NSArray *temp = [appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:nil];
    [data removeAllObjects];
    [data addObjectsFromArray:temp];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [data count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 270.0f;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ISGlucoseCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    GlucoseData *dis = [data objectAtIndex:indexPath.row];
    cell.sequenceNumber.text = [NSString stringWithFormat:@"Sequence Number:%ld",(long)[dis.sequence_number integerValue]];
    cell.baseTime.text = [formater stringFromDate:dis.base_time];
    cell.concentration.text = [NSString stringWithFormat:@"%.2f\n%@",[dis.concentration floatValue],dis.uint];
    NSString *type_s = @"Reserved";
    switch ([dis.type integerValue]) {
        case GlucoseType_CapillaryWholeBlood:
            type_s = @"Capillary Whole blood";
            break;
        case GlucoseType_CapillaryPlasma:
            type_s = @"Capillary Plasma";
            break;
        case GlucoseType_VenousWholeBlood:
            type_s = @"Venous Whole blood";
            break;
        case GlucoseType_VenousPlasma:
            type_s = @"Venous Plasma";
            break;
        case GlucoseType_ArterialWholeBlood:
            type_s = @"Arterial Whole blood";
            break;
        case GlucoseType_ArterialPlasma:
            type_s = @"Arterial Plasma";
            break;
        case GlucoseType_UndeterminedWholeBlood:
            type_s = @"Undetermined Whole blood";
            break;
        case GlucoseType_UndeterminedPlasma:
            type_s = @"Undetermined Plasma";
            break;
        case GlucoseType_ISF:
            type_s = @"Interstitial Fluid (ISF)";
            break;
        case GlucoseType_ControlSolution:
            type_s = @"Control Solution";
            break;
        default:
            break;
    }
    cell.type.text = [NSString stringWithFormat:@"Type:%@",type_s];
    // Configure the cell...
    NSString *location = @"Reserved";
    switch ([dis.sample_location integerValue]) {
        case GlucoseSensorLocation_Finger:
            location = @"Finger";
            break;
        case GlucoseSensorLocation_AST:
            location = @"Alternate Site Test";
            break;
        case GlucoseSensorLocation_Earlobe:
            location = @"Earlobe";
            break;
        case GlucoseSensorLocation_ControlSolution:
            location = @"Control solution";
            break;
        case GlucoseSensorLocation_NotAvailable:
            location = @"Value not available";
            break;
        default:
            break;
    }
    cell.sampleLocation.text = [NSString stringWithFormat:@"Sample Location\n%@",location];
    cell.sensorStatus.text = [NSString stringWithFormat:@"Sensor Status:%@",dis.status];
    cell.context.text = [NSString stringWithFormat:@"Context:%@",[dis.context boolValue]?@"YES":@"NO"];
    NSString *carbohydrate_s = @"Reserved";
    switch ([dis.carbohydrateID integerValue]) {
        case 1:
            carbohydrate_s = @"Breakfast";
            break;
        case 2:
            carbohydrate_s = @"Lunch";
            break;
        case 3:
            carbohydrate_s = @"Dinner";
            break;
        case 4:
            carbohydrate_s = @"Snack";
            break;
        case 5:
            carbohydrate_s = @"Drink";
            break;
        case 6:
            carbohydrate_s = @"Supper";
            break;
        case 7:
            carbohydrate_s = @"Brunch";
            break;
            
        default:
            break;
    }
    cell.carbohydrateID.text = [NSString stringWithFormat:@"Carbohydrate:%@",carbohydrate_s];
    cell.carbohydrate.text = [NSString stringWithFormat:@"%.3fkg",[dis.carbohydrate floatValue]];
    NSString *meal_s = @"Reserved";
    switch ([dis.meal integerValue]) {
        case 1:
            meal_s = @"Preprandial (before meal)";
            break;
        case 2:
            meal_s = @"Postprandial (after meal)";
            break;
        case 3:
            meal_s = @"Fasting";
            break;
        case 4:
            meal_s = @"Casual(snacks, drinks, etc.)";
            break;
        case 5:
            meal_s = @"Bedtime";
            break;
        default:
            break;
    }
    cell.meal.text = [NSString stringWithFormat:@"Meal:%@",meal_s];
    NSString *tester_s = @"Reserved";
    switch ([dis.tester integerValue]) {
        case 1:
            tester_s = @"Self";
            break;
        case 2:
            tester_s = @"Health Care Professional";
            break;
        case 3:
            tester_s = @"Lab test";
            break;
        case 15:
            tester_s = @"Not available";
            break;
        default:
            break;
    }
    cell.tester.text = [NSString stringWithFormat:@"Tester:%@",tester_s];
    NSString *health_s = @"Reserved";
    switch ([dis.health integerValue]) {
        case 1:
            health_s = @"Minor health issues";
            break;
        case 2:
            health_s = @"Major health issues";
            break;
        case 3:
            health_s = @"During menses";
            break;
        case 4:
            health_s = @"Under stress";
            break;
        case 5:
            health_s = @"No health issues";
            break;
        case 15:
            health_s = @"Not available";
            break;
        default:
            break;
    }
    cell.health.text = [NSString stringWithFormat:@"Health:%@",health_s];
    cell.exercise_duration.text = [NSString stringWithFormat:@"Exercise Duration:%ld",(long)[dis.exercise_duration integerValue]];
    cell.exercise_intensity.text = [NSString stringWithFormat:@"Intensity:%ld%%",(long)[dis.exercise_intensity integerValue]];
    NSString *medication_s = @"Reserved";
    switch ([dis.medicationID integerValue]) {
        case 1:
            medication_s = @"Rapid acting insulin";
            break;
        case 2:
            medication_s = @"Short acting insulin";
            break;
        case 3:
            medication_s = @"Intermediate acting insulin";
            break;
        case 4:
            medication_s = @"Long acting insulin";
            break;
        case 5:
            medication_s = @"Pre-mixed insulin";
            break;
        default:
            break;
    }
    cell.medicationID.text = [NSString stringWithFormat:@"Medication ID:%@",medication_s];
    cell.medication.text = [NSString stringWithFormat:@"Medication:%.4f%@",[dis.medication floatValue],dis.medication_uint];
    cell.hbA1c.text = [NSString stringWithFormat:@"HbA1c:%ld%%",(long)[dis.hbA1c integerValue]];
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
