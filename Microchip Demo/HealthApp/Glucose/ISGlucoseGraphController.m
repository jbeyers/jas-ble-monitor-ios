//
//  ISGlucoseGraphController.m
//  Health
//
//  Created by Rick on 2014/12/16.
//  Copyright (c) 2014年 Rick. All rights reserved.
//

#import "ISGlucoseGraphController.h"
#import "BEMSimpleLineGraphView.h"
#import "AppDelegate.h"
#import "UUID.h"
#import "ISGlucoseTableController.h"
#import "GlucoseData.h"

@interface ISGlucoseGraphController ()<BEMSimpleLineGraphDataSource, BEMSimpleLineGraphDelegate>{
    NSMutableArray *data;
    NSDateFormatter *formater;
}


@end

@implementation ISGlucoseGraphController
- (void)loadView {
    if ([[UIScreen mainScreen] bounds].size.height > 480.0f) {
        [super loadView];
    }
    else {
        UIScrollView *view = [[UIScrollView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        view.contentSize = CGSizeMake(320.0f, 568.0f);
        //view.contentInset = UIEdgeInsetsMake(-64.0f, 0, 0, 0);
        self.view = view;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"History";
    self.view.backgroundColor = [UIColor whiteColor];
    data = [[NSMutableArray alloc] init];
    formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:@"yyyy/MM/dd"];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"GlucoseData" inManagedObjectContext:appDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    NSSortDescriptor* sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"base_time" ascending:NO];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
#ifdef USE_HEALTH_KIT
    if ([appDelegate isHealthKitAvailable] && [[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"%@HealthKit",UUIDSTR_GLUCOSE_SERVICE]]) {
        HKQuantityType *weightType = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierBloodGlucose];
        NSSortDescriptor *timeSortDescriptor = [[NSSortDescriptor alloc] initWithKey:HKSampleSortIdentifierEndDate ascending:YES];
        HKSampleQuery *query = [[HKSampleQuery alloc] initWithSampleType:weightType predicate:nil limit:HKObjectQueryNoLimit sortDescriptors:@[timeSortDescriptor] resultsHandler:^(HKSampleQuery *query, NSArray *results, NSError *error) {
            if  (results) {
                [data addObjectsFromArray:results];
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIScrollView *graphScroll = (UIScrollView *)[self.view viewWithTag:1];
                    CGFloat width = 32+50*data.count;
                    if (width < 320.0f) width = 320.0f;
                    graphScroll.contentSize = CGSizeMake(width+5, 200);
                    BEMSimpleLineGraphView *myGraph = (BEMSimpleLineGraphView *)[[graphScroll subviews] objectAtIndex:0];
                    myGraph.frame = CGRectMake(0, 0, width, 200);
                    [myGraph reloadGraph];
                });
            }
            else {
                NSLog(@"HealthKit Error : %@",error.description);
            }
        }];
        [appDelegate.healthStore executeQuery:query];
    }
    else {
#endif
        NSArray *temp = [appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:nil];
        [data addObjectsFromArray:temp];
#ifdef USE_HEALTH_KIT
    }
#endif
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    UIScrollView *graphScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 60, 320, 200)];
    graphScroll.tag = 1;
    //graphScroll.delegate = self;
    [self.view addSubview:graphScroll];
    CGFloat width = 32+50*data.count;
    if (width < 320.0f) width = 320.0f;
    graphScroll.contentSize = CGSizeMake(width+5, 200);
    BEMSimpleLineGraphView *myGraph = [[BEMSimpleLineGraphView alloc] initWithFrame:CGRectMake(0, 0, width, 200)];
    myGraph.delegate = self;
    myGraph.dataSource = self;
    myGraph.colorTop = [UIColor purpleColor];
    myGraph.colorBottom = [UIColor purpleColor];
    myGraph.colorLine = [UIColor whiteColor];
    myGraph.colorXaxisLabel = [UIColor whiteColor];
    myGraph.colorYaxisLabel = [UIColor whiteColor];
    myGraph.widthLine = 3.0;
    myGraph.enableTouchReport = YES;
    myGraph.enablePopUpReport = YES;
    //myGraph.enableBezierCurve = YES;
    myGraph.enableYAxisLabel = YES;
    myGraph.autoScaleYAxis = YES;
    myGraph.alwaysDisplayDots = NO;
    myGraph.enableReferenceAxisLines = YES;
    myGraph.enableReferenceAxisFrame = YES;
    myGraph.animationGraphStyle = BEMLineAnimationNone;
    myGraph.animationGraphEntranceTime = 0;
    myGraph.tag = 1;
    myGraph.userInteractionEnabled = YES;
    [graphScroll addSubview:myGraph];
    
#ifdef USE_HEALTH_KIT
    if (![appDelegate isHealthKitAvailable] || ![[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"%@HealthKit",UUIDSTR_GLUCOSE_SERVICE]]) {
#endif
        UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithTitle:@"Detail" style:UIBarButtonItemStylePlain target:self action:@selector(showTable:)];
        self.navigationItem.rightBarButtonItem = right;
#ifdef USE_HEALTH_KIT
    }
#endif
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    

}

- (void)showTable:(id)sender {
    ISGlucoseTableController *hsitory = [[ISGlucoseTableController alloc] initWithStyle:UITableViewStylePlain];
    [self.navigationController pushViewController:hsitory animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - SimpleLineGraph Data Source

- (NSInteger)numberOfPointsInLineGraph:(BEMSimpleLineGraphView *)graph {
    return data.count?data.count:0;
}

- (CGFloat)lineGraph:(BEMSimpleLineGraphView *)graph valueForPointAtIndex:(NSInteger)index {
#ifdef USE_HEALTH_KIT
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if ([appDelegate isHealthKitAvailable] && [[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"%@HealthKit",UUIDSTR_GLUCOSE_SERVICE]]) {
        HKQuantitySample *quantitySample = data[index];
        HKQuantity *quantity = quantitySample.quantity;
        if ([[NSUserDefaults standardUserDefaults] integerForKey:@"glucoseUnit"] == 0) {
            return [quantity doubleValueForUnit:[[HKUnit gramUnitWithMetricPrefix:HKMetricPrefixKilo] unitDividedByUnit:[HKUnit literUnit]]];
        }
        else {
            return [quantity doubleValueForUnit:[[HKUnit moleUnitWithMolarMass:180.15] unitDividedByUnit:[HKUnit literUnit]]];
        }
    }
    else {
#endif
        GlucoseData *dis = [data objectAtIndex:index];
        if ([[NSUserDefaults standardUserDefaults] integerForKey:@"glucoseUnit"] == 0) {
            if ([dis.uint isEqualToString:@"mol/L"]) {
                return ([dis.concentration floatValue])*180.15;
            }
            return [dis.concentration floatValue];
        }
        else {
            if ([dis.uint isEqualToString:@"kg/L"]) {
                return ([dis.concentration floatValue])*5.551;
            }
            return [dis.concentration floatValue];
        }
#ifdef USE_HEALTH_KIT
    }
#endif
    return 0;
}

#pragma mark - SimpleLineGraph Delegate
- (NSString *)popUpSuffixForlineGraph:(BEMSimpleLineGraphView *)graph {
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"glucoseUnit"] == 0) {
        return @"kg/L";
    }
    else {
        return @"mol/L";
    }
}

- (NSInteger)numberOfGapsBetweenLabelsOnLineGraph:(BEMSimpleLineGraphView *)graph {
    return 1;
}

- (NSString *)lineGraph:(BEMSimpleLineGraphView *)graph labelOnXAxisForIndex:(NSInteger)index {
#ifdef USE_HEALTH_KIT
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if ([appDelegate isHealthKitAvailable] && [[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"%@HealthKit",UUIDSTR_GLUCOSE_SERVICE]]) {
        HKQuantitySample *quantitySample = data[index];
        return [formater stringFromDate:quantitySample.startDate];
    }
#endif
    GlucoseData *dis = [data objectAtIndex:index];
    return [formater stringFromDate:dis.base_time];
}

- (void)lineGraph:(BEMSimpleLineGraphView *)graph didTouchGraphWithClosestIndex:(NSInteger)index {
    
}

- (void)lineGraph:(BEMSimpleLineGraphView *)graph didReleaseTouchFromGraphWithClosestIndex:(CGFloat)index {
    
}

- (void)lineGraphDidFinishLoading:(BEMSimpleLineGraphView *)graph {
    
}
@end
