//
//  ISGlucoseCell.h
//  Health
//
//  Created by Rick on 2014/12/16.
//  Copyright (c) 2014年 Rick. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISGlucoseCell : UITableViewCell
@property (strong) UILabel *sequenceNumber;
@property (strong) UILabel *baseTime;
@property (strong) UILabel *concentration;
@property (strong) UILabel *type;
@property (strong) UILabel *sampleLocation;
@property (strong) UILabel *sensorStatus;
@property (strong) UILabel *context;
@property (strong) UILabel *carbohydrateID;
@property (strong) UILabel *carbohydrate;
@property (strong) UILabel *meal;
@property (strong) UILabel *tester;
@property (strong) UILabel *health;
@property (strong) UILabel *exercise_duration;
@property (strong) UILabel *exercise_intensity;
@property (strong) UILabel *medicationID;
@property (strong) UILabel *medication;
@property (strong) UILabel *hbA1c;

@end
