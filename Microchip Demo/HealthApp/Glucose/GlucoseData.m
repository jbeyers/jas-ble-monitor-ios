//
//  GlucoseData.m
//  Health
//
//  Created by Rick on 2014/12/25.
//  Copyright (c) 2014年 Rick. All rights reserved.
//

#import "GlucoseData.h"


@implementation GlucoseData

@dynamic base_time;
@dynamic carbohydrate;
@dynamic carbohydrateID;
@dynamic concentration;
@dynamic context;
@dynamic exercise_duration;
@dynamic exercise_intensity;
@dynamic hbA1c;
@dynamic health;
@dynamic meal;
@dynamic medication;
@dynamic medication_uint;
@dynamic medicationID;
@dynamic sample_location;
@dynamic sequence_number;
@dynamic status;
@dynamic tester;
@dynamic type;
@dynamic uint;

@end
