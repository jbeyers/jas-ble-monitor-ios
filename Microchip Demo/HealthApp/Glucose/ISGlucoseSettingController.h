//
//  ISGlucoseSettingController.h
//  Health
//
//  Created by Rick on 2014/12/16.
//  Copyright (c) 2014年 Rick. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HKMyPeripheral;

@interface ISGlucoseSettingController : UITableViewController
@property (nonatomic,strong)HKMyPeripheral *peripheral;
@property (nonatomic)NSInteger number;
@end
