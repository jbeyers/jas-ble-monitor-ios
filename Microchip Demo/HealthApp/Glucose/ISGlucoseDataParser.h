//
//  ISGlucoseDataParser.h
//  Health
//
//  Created by Rick on 2014/12/16.
//  Copyright (c) 2014年 Rick. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HKMyPeripheral.h"
typedef struct _GLUCOSE_MANUFACTURE_FLAG {
    uint8_t time_offset:1;
    uint8_t concentration:1;
    uint8_t units:1;
    uint8_t sensor_status:1;
    uint8_t context_information :1;
    uint8_t reserved:3;
}__attribute__((packed)) GLUCOSE_MANUFACTURE_FLAG;

typedef struct _GLUCOSE_MANUFACTURE_CONTEXT_FLAG {
    uint8_t carbohydrate:1;
    uint8_t meal:1;
    uint8_t tester_health:1;
    uint8_t duration_and_intensity:1;
    uint8_t medication:1;
    uint8_t medication_units:1;
    uint8_t HbA1c:1;
    uint8_t extended_flags :1;
}__attribute__((packed)) GLUCOSE_MANUFACTURE_CONTEXT_FLAG;

typedef struct _GLUCOSE_FEATURE {
    uint16_t low_battery_detection:1;
    uint16_t sensor_malfunction_detection:1;
    uint16_t sensor_sample_size:1;
    uint16_t sensor_strip_insertion_error:1;
    uint16_t sensor_strip_type_error:1;
    uint16_t sensor_result_high_low_detection:1;
    uint16_t sensor_temperature_high_low_detection:1;
    uint16_t sensor_read_interrupt_detection:1;
    uint16_t general_device_fault:1;
    uint16_t time_fault:1;
    uint16_t multiple_bond:1;
    uint16_t reserved:5;
}__attribute__((packed)) GLUCOSE_FEATURE;

typedef struct _GLUCOSE_STATUS {
    uint16_t low_battery_detection:1;
    uint16_t sensor_malfunction_detection:1;
    uint16_t sensor_sample_size:1;
    uint16_t sensor_strip_insertion_error:1;
    uint16_t sensor_strip_type_error:1;
    uint16_t sensor_result_high_detection:1;
    uint16_t sensor_result_low_detection:1;
    uint16_t sensor_temperature_high_detection:1;
    uint16_t sensor_temperature_low_detection:1;
    uint16_t sensor_read_interrupt_detection:1;
    uint16_t general_device_fault:1;
    uint16_t time_fault:1;
    uint16_t reserved:4;
}__attribute__((packed)) GLUCOSE_STATUS;

@interface ISGlucoseDataParser : NSObject
- (void)decodeFeature:(NSData *)data;
- (NSDictionary *)decodeData:(NSData *)data;
- (NSDictionary *)decodeContextData:(NSData *)data;
- (NSData *)encodeOp:(GlucoseOpCode)op operator:(GlucoseOperator)Operator andOperand:(NSArray *)operand;
- (NSDictionary *)decodeOp:(NSData *)data;
@end
