//
//  ISGlucoseDataParser.m
//  Health
//
//  Created by Rick on 2014/12/16.
//  Copyright (c) 2014年 Rick. All rights reserved.
//

#import "ISGlucoseDataParser.h"
#import "ISWeightParser.h"
#import "CovertTools.h"

@interface ISGlucoseDataParser() {
    GLUCOSE_FEATURE *feature;
    NSDateFormatter *formater;
}
@end

@implementation ISGlucoseDataParser
- (id)init {
    self = [super init];
    if (self) {
        feature = nil;
        formater = [[NSDateFormatter alloc] init];
        [formater setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    }
    return self;
}

- (void)dealloc {
    free(feature);
}

- (void)decodeFeature:(NSData *)data {
    uint16_t *buffer = malloc(sizeof(uint16_t));
    [data getBytes:buffer length:[data length]];
    feature = (GLUCOSE_FEATURE *)buffer;
}

- (NSDictionary *)decodeData:(NSData *)data {
    uint8_t buffer[1];
    [data getBytes:&buffer length:1];
    GLUCOSE_MANUFACTURE_FLAG *flag = (GLUCOSE_MANUFACTURE_FLAG *)buffer;
    uint16_t sequence_number;
    [data getBytes:&sequence_number range:NSMakeRange(1, 2)];
    NSDate *base_time = [NSDate date];
    uint8_t time_buffer[7];
    [data getBytes:&time_buffer range:NSMakeRange(3, 7)];
    TIMESTAMP *t = (TIMESTAMP *)time_buffer;
    int pointer = 10;
    NSString *t_s = [NSString stringWithFormat:@"%d-%d-%d %d:%d:%d",t->year,t->month,t->day,t->hour,t->minute,t->second];
    NSDate *_time = [formater dateFromString:t_s];
    if (_time) {
        base_time = _time;
    }
    if (flag->time_offset == 1) {
        uint16_t time_offset;
        [data getBytes:&time_offset range:NSMakeRange(pointer, 2)];
        pointer+=2;
        uint32_t time = time_offset*60;
        base_time = [base_time dateByAddingTimeInterval:time];
    }

    NSString *uint = @"";
    float concentration = 0.0f;
    uint16_t type = 0;
    uint16_t sample_location = 0;

    if (flag->concentration == 1) {
        uint16_t temp_data;
        [data getBytes:&temp_data range:NSMakeRange(pointer, 2)];
        pointer+=2;
        concentration = [CovertTools covertSFLOAT:temp_data];
        if (flag->units == 0) {
            uint = @"kg/L";
        }
        else {
            uint = @"mol/L";
        }
        [data getBytes:&type range:NSMakeRange(pointer, 1)];
        type = type & 0x0f;
        [data getBytes:&sample_location range:NSMakeRange(pointer, 1)];
        sample_location = sample_location >> 4;
        pointer++;
    }
    NSString *status_s;
    if (flag->sensor_status == 1) {
        uint16_t b[1];
        [data getBytes:&b range:NSMakeRange(pointer, 2)];
        GLUCOSE_STATUS *status = (GLUCOSE_STATUS *)b;
        status_s = [NSString stringWithFormat:@"%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@",
                    feature->low_battery_detection?@(status->low_battery_detection):@"X",
                    feature->sensor_malfunction_detection?@(status->sensor_malfunction_detection):@"X",
                    feature->sensor_sample_size?@(status->sensor_sample_size):@"X",
                    feature->sensor_strip_insertion_error?@(status->sensor_strip_insertion_error):@"X",
                    feature->sensor_strip_type_error?@(status->sensor_strip_type_error):@"X",
                    feature->sensor_result_high_low_detection?@(status->sensor_result_high_detection):@"X",
                    feature->sensor_result_high_low_detection?@(status->sensor_result_low_detection):@"X",
                    feature->sensor_temperature_high_low_detection?@(status->sensor_temperature_high_detection):@"X",
                    feature->sensor_temperature_high_low_detection?@(status->sensor_temperature_low_detection):@"X",
                    feature->sensor_read_interrupt_detection?@(status->sensor_read_interrupt_detection):@"X",
                    feature->general_device_fault?@(status->general_device_fault):@"X",
                    feature->time_fault?@(status->time_fault):@"X",
                    feature->multiple_bond?@"O":@"X"
                    ];
    }
    else {
        status_s = [NSString stringWithFormat:@"X|X|X|X|X|X|X|X|X|X|X|X|%@",
                    feature->multiple_bond?@"O":@"X"
                    ];
    }
    
    return @{@"sequence_number":@(sequence_number),
             @"base_time":base_time,
             @"uint":uint,
             @"concentration":@(concentration),
             @"type":@(type),
             @"sample_location":@(sample_location),
             @"status":status_s,
             @"context":@(flag->context_information)
             };
}

- (NSDictionary *)decodeContextData:(NSData *)data {
    uint8_t buffer[1];
    [data getBytes:&buffer length:1];
    GLUCOSE_MANUFACTURE_CONTEXT_FLAG *flag = (GLUCOSE_MANUFACTURE_CONTEXT_FLAG *)buffer;
    uint16_t sequence_number;
    [data getBytes:&sequence_number range:NSMakeRange(1, 2)];
    int pointer = 3;
    if (flag->extended_flags == 1) {
        pointer++;
    }
    uint8_t carbohydrateID = 0;
    float carbohydrate = 0.0f;
    if (flag->carbohydrate == 1) {
        [data getBytes:&carbohydrateID range:NSMakeRange(pointer, 1)];
        uint16_t temp_data;
        [data getBytes:&temp_data range:NSMakeRange(pointer, 2)];
        pointer+=3;
        carbohydrate = [CovertTools covertSFLOAT:temp_data];
    }
    uint8_t meal = 0;
    if (flag->meal == 1) {
        [data getBytes:&meal range:NSMakeRange(pointer, 1)];
        pointer++;
    }
    uint16_t tester = 0;
    uint16_t health = 0;
    if (flag->tester_health == 1) {
        [data getBytes:&tester range:NSMakeRange(pointer, 1)];
        tester = tester & 0x0f;
        [data getBytes:&health range:NSMakeRange(pointer, 1)];
        health = health >> 4;
        pointer++;
    }
    uint16_t exercise_duration = 0;
    uint8_t exercise_intensity = 0;
    if (flag->duration_and_intensity == 1) {
        [data getBytes:&exercise_duration range:NSMakeRange(pointer, 2)];
        pointer+=2;
        [data getBytes:&exercise_intensity range:NSMakeRange(pointer, 1)];
        pointer++;
    }
    
    float medication = 0.0f;
    uint8_t medicationID = 0;
    NSString *medication_uint = @"";
    if (flag->medication == 1) {
        [data getBytes:&medicationID range:NSMakeRange(pointer, 1)];
        pointer++;
        uint16_t temp_data;
        [data getBytes:&temp_data range:NSMakeRange(pointer, 2)];
        pointer+=2;
        medication = [CovertTools covertSFLOAT:temp_data];
        if (flag->medication_units == 0) {
            medication_uint = @"kg";
        }
        else {
            medication_uint = @"L";
        }
    }
    float HbA1c = 0.0f;
    if (flag->HbA1c == 1) {
        uint16_t temp_data;
        [data getBytes:&temp_data range:NSMakeRange(pointer, 2)];
        pointer+=2;
        HbA1c = [CovertTools covertSFLOAT:temp_data];
    }
    return @{@"sequence_number":@(sequence_number),
             @"carbohydrateID":@(carbohydrateID),
             @"carbohydrate":@(carbohydrate),
             @"meal":@(meal),
             @"tester":@(tester),
             @"health":@(health),
             @"exercise_duration":@(exercise_duration),
             @"exercise_intensity":@(exercise_intensity),
             @"medicationID":@(medicationID),
             @"medication":@(medication),
             @"medication_uint":medication_uint,
             @"HbA1c":@(HbA1c)
             };
}

- (NSData *)encodeOp:(GlucoseOpCode)op operator:(GlucoseOperator)Operator andOperand:(NSArray *)operand {
    BOOL needOperator = NO;
    NSMutableData *data = [NSMutableData data];
    switch (op) {
        case GlucoseOpCode_ReportStoredRecords:
        case GlucoseOpCode_DeleteStoredRecords:
        case GlucoseOpCode_ReportNumber_of_StoredRecords:
            needOperator = YES;
            break;
        case GlucoseOpCode_AbortOperation:
            needOperator = NO;
            break;
        default:
            return nil;
            break;
    }
    [data appendBytes:&op length:sizeof(op)];
    if (needOperator) {
        [data appendBytes:&Operator length:sizeof(Operator)];
        if (operand) {
            uint8_t type = 1;
            [data appendBytes:&type length:sizeof(type)];
            for (NSNumber *parameter in operand) {
                uint16_t temp = [parameter unsignedShortValue];
                [data appendBytes:&temp length:sizeof(temp)];
            }
        }
    }
    else {
        GlucoseOperator temp = GlucoseOperator_NA;
        [data appendBytes:&temp length:sizeof(temp)];
    }
    return data;
}

- (NSDictionary *)decodeOp:(NSData *)data {
    uint8_t opCode;
    [data getBytes:&opCode length:1];
    switch (opCode) {
        case GlucoseOpCode_Number_of_StoredRecords:
        case GlucoseOpCode_ResponseCode:
            break;
        default:
            return nil;
            break;
    }
    NSError *error;
    if (opCode == GlucoseOpCode_ResponseCode) {
        uint8_t response;
        uint8_t responseOP;
        [data getBytes:&responseOP range:NSMakeRange(2, 1)];
        [data getBytes:&response range:NSMakeRange(3, 1)];
        if (response != GlucoseResponseCode_Success) {
            error = [[NSError alloc] initWithDomain:@"Glucose" code:response userInfo:@{@"responseOP":@(responseOP),@"response":@(response)}];
        }
        if (error) {
            return @{@"OP":@(GlucoseOpCode_ResponseCode),
                     @"ResponseOP":@(responseOP),
                     @"Error":error};

        }
        else {
            return @{@"OP":@(GlucoseOpCode_ResponseCode),
                     @"ResponseOP":@(responseOP)};
        }
    }
    else {
        uint16_t number;
        [data getBytes:&number range:NSMakeRange(2, 2)];
        return @{@"OP":@(GlucoseOpCode_Number_of_StoredRecords),
                 @"Number":@(number)};
    }
    return nil;
}
@end
