//
//  ISGlucoseCell.m
//  Health
//
//  Created by Rick on 2014/12/16.
//  Copyright (c) 2014年 Rick. All rights reserved.
//

#import "ISGlucoseCell.h"

@implementation ISGlucoseCell

- (void)awakeFromNib {
    // Initialization code
}


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _concentration = [[UILabel alloc] initWithFrame:CGRectMake(5.0f, 10.0f, 110.0f, 60.0f)];
        //_concentration.text = @"100.0\nmol/L";
        _concentration.font = [UIFont systemFontOfSize:16.0f];
        _concentration.numberOfLines = 2;
        _concentration.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_concentration];
        _sequenceNumber = [[UILabel alloc] initWithFrame:CGRectMake(120.0f, 25.0f, 210.0f, 18.0f)];
        //_sequenceNumber.text = @"Sequence Number:65535";
        _sequenceNumber.font = [UIFont systemFontOfSize:16.0f];
        [self.contentView addSubview:_sequenceNumber];
        _type = [[UILabel alloc] initWithFrame:CGRectMake(10.0f, 85.0f, 310.0f, 18.0f)];
        //_type.text = @"Type:Undetermined Whole blood";
        _type.font = [UIFont systemFontOfSize:16.0f];
        [self.contentView addSubview:_type];
        _sensorStatus = [[UILabel alloc] initWithFrame:CGRectMake(10.0f, 105.0f, 300.0f, 18.0f)];
        //_sensorStatus.text = @"Sensor Status:X|X|X|X|X|X|X|X|X|X|X|X|X";
        _sensorStatus.font = [UIFont systemFontOfSize:16.0f];
        [self.contentView addSubview:_sensorStatus];
        _context = [[UILabel alloc] initWithFrame:CGRectMake(120.0f, 5.0f, 300.0f, 18.0f)];
        //_context.text = @"Context:YES";
        _context.font = [UIFont systemFontOfSize:16.0f];
        [self.contentView addSubview:_context];
        _baseTime = [[UILabel alloc] initWithFrame:CGRectMake(120.0f, 45.0f, 220.0f, 18.0f)];
        //_baseTime.text = @"2014/10/10 12:12:12";
        _baseTime.font = [UIFont systemFontOfSize:16.0f];
        [self.contentView addSubview:_baseTime];
        _sampleLocation = [[UILabel alloc] initWithFrame:CGRectMake(10.0f, 65.0f, 300.0f, 18.0f)];
        //_sampleLocation.text = @"Sample Location:Control solution";
        _sampleLocation.font = [UIFont systemFontOfSize:16.0f];
        [self.contentView addSubview:_sampleLocation];
        _carbohydrateID = [[UILabel alloc] initWithFrame:CGRectMake(10.0f, 125.0f, 220.0f, 18.0f)];
        //_carbohydrateID.text = @"Carbohydrate:Breakfast";
        _carbohydrateID.font = [UIFont systemFontOfSize:16.0f];
        _carbohydrateID.textColor = [UIColor grayColor];
        [self.contentView addSubview:_carbohydrateID];
        _carbohydrate = [[UILabel alloc] initWithFrame:CGRectMake(200.0f, 125.0f, 220.0f, 18.0f)];
        //_carbohydrate.text = @"0.01kg";
        _carbohydrate.font = [UIFont systemFontOfSize:16.0f];
        _carbohydrate.textColor = [UIColor grayColor];
        [self.contentView addSubview:_carbohydrate];
        _meal = [[UILabel alloc] initWithFrame:CGRectMake(10.0f, 145.0f, 320.0f, 18.0f)];
        //_meal.text = @"Meal:Preprandial (before meal)";
        _meal.font = [UIFont systemFontOfSize:16.0f];
        _meal.textColor = [UIColor grayColor];
        [self.contentView addSubview:_meal];
        _tester = [[UILabel alloc] initWithFrame:CGRectMake(10.0f, 165.0f, 320.0f, 18.0f)];
        //_tester.text = @"Tester:Health Care Professional";
        _tester.font = [UIFont systemFontOfSize:16.0f];
        _tester.textColor = [UIColor grayColor];
        [self.contentView addSubview:_tester];
        _health = [[UILabel alloc] initWithFrame:CGRectMake(10.0f, 185.0f, 320.0f, 18.0f)];
        //_health.text = @"Health:Minor health issues";
        _health.font = [UIFont systemFontOfSize:16.0f];
        _health.textColor = [UIColor grayColor];
        [self.contentView addSubview:_health];
        _exercise_duration = [[UILabel alloc] initWithFrame:CGRectMake(10.0f, 205.0f, 320.0f, 18.0f)];
        //_exercise_duration.text = @"Exercise Duration:65535";
        _exercise_duration.font = [UIFont systemFontOfSize:16.0f];
        _exercise_duration.textColor = [UIColor grayColor];
        [self.contentView addSubview:_exercise_duration];
        _exercise_intensity = [[UILabel alloc] initWithFrame:CGRectMake(200.0f, 205.0f, 220.0f, 18.0f)];
        //_exercise_intensity.text = @"Intensity:100%";
        _exercise_intensity.font = [UIFont systemFontOfSize:16.0f];
        _exercise_intensity.textColor = [UIColor grayColor];
        [self.contentView addSubview:_exercise_intensity];
        _medicationID = [[UILabel alloc] initWithFrame:CGRectMake(10.0f, 225.0f, 320.0f, 18.0f)];
        //_medicationID.text = @"Medication ID:Intermediate acting insulin";
        _medicationID.font = [UIFont systemFontOfSize:16.0f];
        _medicationID.textColor = [UIColor grayColor];
        [self.contentView addSubview:_medicationID];
        _medication = [[UILabel alloc] initWithFrame:CGRectMake(10.0f, 245.0f, 320.0f, 18.0f)];
        //_medication.text = @"Medication:0.001kg";
        _medication.font = [UIFont systemFontOfSize:16.0f];
        _medication.textColor = [UIColor grayColor];
        [self.contentView addSubview:_medication];
        _hbA1c = [[UILabel alloc] initWithFrame:CGRectMake(200.0f, 245.0f, 320.0f, 18.0f)];
        //_hbA1c.text = @"HbA1c:100%";
        _hbA1c.font = [UIFont systemFontOfSize:16.0f];
        _hbA1c.textColor = [UIColor grayColor];
        [self.contentView addSubview:_hbA1c];
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
