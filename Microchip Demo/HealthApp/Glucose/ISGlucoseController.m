//
//  ISGlucoseController.m
//  Health
//
//  Created by Rick on 2014/12/16.
//  Copyright (c) 2014年 Rick. All rights reserved.
//

#import "ISGlucoseController.h"
#import "HKConnectViewController.h"
#import "HKCBController.h"
#import "AppDelegate.h"
#import "ISGlucoseGraphController.h"
#import "ISGlucoseTableController.h"
#import "ISGlucoseSettingController.h"
#import "MNMToast.h"

@interface ISGlucoseController ()<HKCBControllerDelegate,MyPeripheralDelegate> {
    HKConnectViewController *scan;
    ISGlucoseSettingController *setting;
    UIButton *scan_but;
    UIImageView *connect_icon;
    HKMyPeripheral *connect_peripheral;
    UILabel *unit;
    UILabel *value;
    UILabel *type;
    UILabel *time;
    UILabel *sampleLocation;
    UILabel *sensorStstus;
    NSDateFormatter *formater;
}


@end

@implementation ISGlucoseController
- (void)loadView {
    if ([[UIScreen mainScreen] bounds].size.height > 480.0f) {
        [super loadView];
    }
    else {
        UIScrollView *view = [[UIScrollView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        view.contentSize = CGSizeMake(320.0f, 568.0f);
        view.contentInset = UIEdgeInsetsMake(-64.0f, 0, 0, 0);
        self.view = view;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
#ifdef USE_HEALTH_KIT
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if ([appDelegate isHealthKitAvailable]) {
        [appDelegate.healthStore requestAuthorizationToShareTypes:[NSSet setWithObjects:[HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierBloodGlucose], nil] readTypes:[NSSet setWithObjects:[HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierBloodGlucose], nil] completion:^(BOOL success, NSError *error) {
            if (success) {
                NSLog(@"Authorization Success");
            }
        }];
    }
#endif
    CGRect rect = [[UIScreen mainScreen]bounds];
    self.title = @"Glucose";
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:@selector(dismiss)];
    UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithTitle:@"Settings" style:UIBarButtonItemStylePlain target:self action:@selector(showSetting:)];
    self.navigationItem.rightBarButtonItem = right;
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    scan = [[HKConnectViewController alloc] initWithNibName:nil bundle:nil];
    scan.uuids = @[UUIDSTR_GLUCOSE_SERVICE];
    scan.serviceUuids = @[[CBUUID UUIDWithString:UUIDSTR_DEVICE_INFO_SERVICE],[CBUUID UUIDWithString:UUIDSTR_GLUCOSE_SERVICE]];
    scan.delegate = self;
    UIView *info_bg = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, rect.size.width, 350.0f)];
    info_bg.backgroundColor = [UIColor purpleColor];
    [self.view addSubview:info_bg];
    unit = [[UILabel alloc] initWithFrame:CGRectMake(230.0f, 70.0f, 100.0f, 32.0f)];
    //unit.text = @"kg/L";
    unit.textColor = [UIColor whiteColor];
    unit.textAlignment = NSTextAlignmentCenter;
    [info_bg addSubview:unit];
    value = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 130.0f, rect.size.width, 150.0f)];
    value.numberOfLines = 3;
    //value.text = @"0";
    value.font = [UIFont boldSystemFontOfSize:90.0f];
    value.textColor = [UIColor whiteColor];
    value.textAlignment = NSTextAlignmentCenter;
    [info_bg addSubview:value];
    
    connect_icon = [[UIImageView alloc] initWithFrame:CGRectMake(10.0f, 70.0f, 40.0f, 40.0f)];
    connect_icon.image = [UIImage imageNamed:@"blood"];
    connect_icon.alpha = 0.5;
    [info_bg addSubview:connect_icon];
    UIButton *history = [UIButton buttonWithType:UIButtonTypeCustom];
    history.frame = info_bg.bounds;
    [history addTarget:self action:@selector(showGraph:) forControlEvents:UIControlEventTouchUpInside];
    [info_bg addSubview:history];
    sampleLocation = [[UILabel alloc] initWithFrame:CGRectMake(5.0f, 300.0f, 315.0f, 50.0f)];
    //sampleLocation.text = @"Sample Location\nAlternate Site Test";
    sampleLocation.numberOfLines = 2;
    sampleLocation.font = [UIFont systemFontOfSize:15.0f];
    sampleLocation.textColor = [UIColor whiteColor];
    sampleLocation.textAlignment = NSTextAlignmentLeft;
    [info_bg addSubview:sampleLocation];
    sensorStstus = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 300.0f, 315.0f, 50.0f)];
    sensorStstus.numberOfLines = 2;
    sensorStstus.font = [UIFont systemFontOfSize:15.0f];
    //sensorStstus.text = @"Ststus\nX|X|X|X|X|X|X|X|X|X|X|X|X";
    sensorStstus.textColor = [UIColor whiteColor];
    sensorStstus.textAlignment = NSTextAlignmentRight;
    [info_bg addSubview:sensorStstus];

    UIView *type_bg = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 350.0f, rect.size.width/2, 150.0f)];
    type_bg.backgroundColor = [UIColor colorWithRed:1.000 green:0.435 blue:0.812 alpha:1.000];
    [self.view addSubview:type_bg];
    UILabel *type_l = [[UILabel alloc] initWithFrame:CGRectMake(5.0f, 5.0f, 100.0f, 30.0f)];
    type_l.textColor = [UIColor whiteColor];
    type_l.text = @"Type";
    [type_bg addSubview:type_l];
    type = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, rect.size.width/2, 60.0f)];
    type.center = CGPointMake(80.0f, 75.0f);
    //type.text = @"Capillary Whole blood";
    type.font = [UIFont boldSystemFontOfSize:24.0f];
    type.adjustsFontSizeToFitWidth = YES;
    type.textColor = [UIColor whiteColor];
    type.textAlignment = NSTextAlignmentCenter;
    [type_bg addSubview:type];

    UIView *time_bg = [[UIView alloc] initWithFrame:CGRectMake(rect.size.width/2, 350.0f, rect.size.width/2, 150.0f)];
    time_bg.backgroundColor = [UIColor colorWithRed:0.349 green:0.881 blue:0.705 alpha:1.000];
    [self.view addSubview:time_bg];
    time = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 160.0f, 60.0f)];
    time.center = CGPointMake(80.0f, 75.0f);
    //time.text = @"2014/10/01\n18:00:00";
    time.font = [UIFont boldSystemFontOfSize:24.0f];
    time.textColor = [UIColor whiteColor];
    time.textAlignment = NSTextAlignmentCenter;
    time.numberOfLines = 2;
    time.lineBreakMode = NSLineBreakByWordWrapping;
    [time_bg addSubview:time];
  
    UIView *scan_bg = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 500.0f, rect.size.width, 68.0f)];
    scan_bg.backgroundColor = [UIColor colorWithRed:0.18 green:0.50 blue:0.72 alpha:1.0];
    [self.view addSubview:scan_bg];
    scan_but = [UIButton buttonWithType:UIButtonTypeCustom];
    [scan_but setTitle:@"Scan" forState:UIControlStateNormal];
    scan_but.titleLabel.font = [UIFont boldSystemFontOfSize:24.0f];
    scan_but.frame = CGRectMake(0.0f, 0.0f, rect.size.width, 68.0f);
    scan_but.center = CGPointMake(rect.size.width/2, 34.0f);
    scan_but.tag = 0;
    [scan_bg addSubview:scan_but];
    [scan_but addTarget:self action:@selector(scan) forControlEvents:UIControlEventTouchUpInside];
    formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:@"yyyy-MM-dd\nHH:mm:ss"];
    [self resetText];

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    NSString *uuid_string = [[NSUserDefaults standardUserDefaults] objectForKey:UUIDSTR_BLOOD_PRESSURE_SERVICE];
    if (uuid_string) {
        NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:uuid_string];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            //[scan connectDeviceWithIdentifier:uuid];
        });
    }
    if (setting) {
        setting = nil;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dismiss {
    [self dismissViewControllerAnimated:YES completion:^{
        NSLog(@"dismissViewControllerAnimated");
        [scan release];
    }];
}

- (void)scan {
    if ([[scan_but titleForState:UIControlStateNormal] isEqualToString:@"Scan"]) {
        [self.navigationController pushViewController:scan animated:YES];
    }
    if (connect_peripheral) {
        [scan disconnectDevice:connect_peripheral];
        connect_peripheral = nil;
        connect_icon.alpha = 0.5;
        [scan_but setTitle:@"Scan" forState:UIControlStateNormal];
    }
}

- (void)resetText {
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"glucoseUnit"] == 0) {
        unit.text = @"kg/L";
    }
    else {
        unit.text = @"mol/L";
    }
    value.text = @"0";
    time.text = @"";
    sensorStstus.text = @"";
    sampleLocation.text = @"";
    type.text = @"";
}

- (void)showGraph:(id)sender {
    ISGlucoseGraphController *view = [[ISGlucoseGraphController alloc] init];
    //ISGlucoseTableController *view = [[ISGlucoseTableController alloc] init];
    [self.navigationController pushViewController:view animated:YES];
}

- (void)showSetting:(id)sender {
    setting = [[ISGlucoseSettingController alloc] initWithStyle:UITableViewStyleGrouped];
    setting.peripheral = connect_peripheral;
    [self.navigationController pushViewController:setting animated:YES];
    /*dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [MNMToast showWithText:@"Get Context Data" autoHidding:YES priority:MNMToastPriorityNormal completionHandler:nil tapHandler:nil];

    });*/
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)didUpdatePeripheralList:(NSArray *)peripherals {
    
}

- (void)didConnectPeripheral:(HKMyPeripheral *)peripheral {
    if (peripheral.connectStaus == HK_MYPERIPHERAL_CONNECT_STATUS_CONNECTED) {
        connect_icon.alpha = 1.0f;
        [scan_but setTitle:@"Disconnect" forState:UIControlStateNormal];
    }
    else {
        [scan_but setTitle:@"Scan" forState:UIControlStateNormal];
        connect_icon.alpha = 0.5f;
    }
    connect_peripheral = peripheral;
    peripheral.transDataDelegate = self;
    [self resetText];
}

- (void)didDisconnectPeripheral:(HKMyPeripheral *)peripheral {
    [scan_but setTitle:@"Scan" forState:UIControlStateNormal];
    connect_icon.alpha = 0.5f;
    peripheral.transDataDelegate = nil;
}

- (void)MyPeripheral:(HKMyPeripheral *)peripheral didUpdateGlucoseData:(NSDictionary *)data error:(NSError *)error {
    NSLog(@"%@",data);
    unit.text = data[@"uint"];
    value.text = [NSString stringWithFormat:@"%.2f",[data[@"concentration"] floatValue]];
    NSString *location = @"Reserved";
    switch ([data[@"sample_location"] integerValue]) {
        case GlucoseSensorLocation_Finger:
            location = @"Finger";
            break;
        case GlucoseSensorLocation_AST:
            location = @"Alternate Site Test";
            break;
        case GlucoseSensorLocation_Earlobe:
            location = @"Earlobe";
            break;
        case GlucoseSensorLocation_ControlSolution:
            location = @"Control solution";
            break;
        case GlucoseSensorLocation_NotAvailable:
            location = @"Value not available";
            break;
        default:
            break;
    }
    sampleLocation.text = [NSString stringWithFormat:@"Sample Location\n%@",location];
    sensorStstus.text = [NSString stringWithFormat:@"Status\n%@",data[@"status"]];
    NSString *type_s = @"Reserved";
    switch ([data[@"type"] integerValue]) {
        case GlucoseType_CapillaryWholeBlood:
            type_s = @"Capillary Whole blood";
            break;
        case GlucoseType_CapillaryPlasma:
            type_s = @"Capillary Plasma";
            break;
        case GlucoseType_VenousWholeBlood:
            type_s = @"Venous Whole blood";
            break;
        case GlucoseType_VenousPlasma:
            type_s = @"Venous Plasma";
            break;
        case GlucoseType_ArterialWholeBlood:
            type_s = @"Arterial Whole blood";
            break;
        case GlucoseType_ArterialPlasma:
            type_s = @"Arterial Plasma";
            break;
        case GlucoseType_UndeterminedWholeBlood:
            type_s = @"Undetermined Whole blood";
            break;
        case GlucoseType_UndeterminedPlasma:
            type_s = @"Undetermined Plasma";
            break;
        case GlucoseType_ISF:
            type_s = @"Interstitial Fluid (ISF)";
            break;
        case GlucoseType_ControlSolution:
            type_s = @"Control Solution";
            break;
        default:
            break;
    }
    type.text = type_s;
    time.text = [formater stringFromDate:data[@"base_time"]];
#ifdef USE_HEALTH_KIT
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if ([appDelegate isHealthKitAvailable]) {
        HKUnit *Unit;
        if ([data[@"uint"] isEqualToString:@"kg/L"]) {
            Unit = [[HKUnit gramUnitWithMetricPrefix:HKMetricPrefixKilo] unitDividedByUnit:[HKUnit literUnit]];
        }
        else {
            Unit = [[HKUnit moleUnitWithMolarMass:180.15] unitDividedByUnit:[HKUnit literUnit]];
        }
        HKQuantity *quantity = [HKQuantity quantityWithUnit:Unit doubleValue:[data[@"concentration"] doubleValue]];
        HKQuantityType *QuantityType = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierBloodGlucose];
        
        NSDate *now = data[@"base_time"];
        
        HKQuantitySample *sample = [HKQuantitySample quantitySampleWithType:QuantityType quantity:quantity startDate:now endDate:now];
        
        [appDelegate.healthStore saveObject:sample withCompletion:^(BOOL success, NSError *error) {
            if (!success) {
                NSLog(@"An error occured saving the temperature sample %@. In your app, try to handle this gracefully. The error was: %@.", sample, error);
            }
        }];
    }
#endif

}

- (void)MyPeripheral:(HKMyPeripheral *)peripheral didUpdateGlucoseContextData:(NSDictionary *)data error:(NSError *)error {
    [MNMToast showWithText:@"Get Context Data" autoHidding:YES priority:MNMToastPriorityNormal completionHandler:nil tapHandler:nil];
}

- (void)MyPeripheral:(HKMyPeripheral *)peripheral didUpdateGlucoseDataNumber:(NSInteger)number error:(NSError *)error {
    if (setting) {
        [setting setNumber:number];
    }
}

- (void)MyPeripheral:(HKMyPeripheral *)peripheral didExecuteGlucoseOp:(GlucoseOpCode)op error:(NSError *)error {
    if (error) {
        [MNMToast showWithText:@"Execute Error" autoHidding:YES priority:MNMToastPriorityNormal completionHandler:nil tapHandler:nil];
        NSLog(@"Error %@",error);
    }
    else {
        [MNMToast showWithText:@"Execute Success" autoHidding:YES priority:MNMToastPriorityNormal completionHandler:nil tapHandler:nil];
    }
}

@end
