//
//  GlucoseData.h
//  Health
//
//  Created by Rick on 2014/12/25.
//  Copyright (c) 2014年 Rick. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface GlucoseData : NSManagedObject

@property (nonatomic, retain) NSDate * base_time;
@property (nonatomic, retain) NSNumber * carbohydrate;
@property (nonatomic, retain) NSNumber * carbohydrateID;
@property (nonatomic, retain) NSNumber * concentration;
@property (nonatomic, retain) NSNumber * context;
@property (nonatomic, retain) NSNumber * exercise_duration;
@property (nonatomic, retain) NSNumber * exercise_intensity;
@property (nonatomic, retain) NSNumber * hbA1c;
@property (nonatomic, retain) NSNumber * health;
@property (nonatomic, retain) NSNumber * meal;
@property (nonatomic, retain) NSNumber * medication;
@property (nonatomic, retain) NSString * medication_uint;
@property (nonatomic, retain) NSNumber * medicationID;
@property (nonatomic, retain) NSNumber * sample_location;
@property (nonatomic, retain) NSNumber * sequence_number;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSNumber * tester;
@property (nonatomic, retain) NSNumber * type;
@property (nonatomic, retain) NSString * uint;

@end
