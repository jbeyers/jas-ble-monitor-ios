//
//  HeartRateData.h
//  Health
//
//  Created by Rick on 2014/11/4.
//  Copyright (c) 2014年 Rick. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface HeartRateData : NSManagedObject

@property (nonatomic, retain) NSNumber * energyExpended;
@property (nonatomic, retain) NSNumber * heartRateValue;
@property (nonatomic, retain) id rr;
@property (nonatomic, retain) NSNumber * sensorContactStatus;
@property (nonatomic, retain) NSDate * timestamp;

@end

@interface ArrayTransformer : NSValueTransformer

@end