//
//  ISHeartRateParser.m
//  Weight Scale
//
//  Created by Rick on 2014/10/30.
//  Copyright (c) 2014年 Rick. All rights reserved.
//

#import "ISHeartRateParser.h"

@implementation ISHeartRateParser
- (NSDictionary *)decodeData:(NSData *)data {
    uint8_t buffer[1];
    [data getBytes:&buffer length:1];
    NSNumber *heartRateValue;
    HEART_RATE_FLAG *flag = (HEART_RATE_FLAG *)buffer;
    int pointer = 1;
    if (flag->format == 0) {
        uint8_t value;
        [data getBytes:&value range:NSMakeRange(1, 1)];
        pointer+=1;
        heartRateValue = @(value);
    }
    else {
        uint16_t value;
        [data getBytes:&value range:NSMakeRange(1, 2)];
        pointer+=2;
        heartRateValue = @(value);
    }
    uint16_t energy;
    if (flag->energy_expended == 1) {
        [data getBytes:&energy range:NSMakeRange(pointer, 2)];
        pointer+=2;
    }
    else {
        energy = 0;
    }
    NSMutableArray *RR_interval = [NSMutableArray array];
    if (flag->RR_interval == 1) {
        for (; pointer < [data length]; pointer+=2) {
            uint16_t rr;
            [data getBytes:&rr range:NSMakeRange(pointer, 2)];
            [RR_interval addObject:@(rr/1024.0f)];
        }
    }
    
    return @{@"heart_rate":heartRateValue,
             @"energy_expended":@(energy),
             @"RR_Interval":RR_interval,
             @"sensor_contact_status":@(flag->contact_status)};
}

@end
