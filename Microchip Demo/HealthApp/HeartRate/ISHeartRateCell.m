//
//  ISHeartRateCell.m
//  Health
//
//  Created by Rick on 2014/11/5.
//  Copyright (c) 2014年 Rick. All rights reserved.
//

#import "ISHeartRateCell.h"

@implementation ISHeartRateCell

- (void)awakeFromNib {
    // Initialization code
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _heartRate = [[UILabel alloc] initWithFrame:CGRectMake(5.0f, 10.0f, 110.0f, 60.0f)];
        //_heartRate.text = @"100 BPS";
        _heartRate.font = [UIFont systemFontOfSize:22.0f];
        _heartRate.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_heartRate];
        _energy = [[UILabel alloc] initWithFrame:CGRectMake(120.0f, 5.0f, 110.0f, 18.0f)];
        //_energy.text = @"2000 J";
        _energy.font = [UIFont systemFontOfSize:16.0f];
        [self.contentView addSubview:_energy];
        _sensorContactStatus = [[UILabel alloc] initWithFrame:CGRectMake(230.0f, 5.0f, 110.0f, 18.0f)];
        //_sensorContactStatus.text = @"Status:0";
        _sensorContactStatus.font = [UIFont systemFontOfSize:16.0f];
        [self.contentView addSubview:_sensorContactStatus];
        _RR = [[UILabel alloc] initWithFrame:CGRectMake(120.0f, 30.0f, 200.0f, 18.0f)];
        //_RR.text = @"0.1111,0.1111,0.1111,0.1111,0.1111";
        _RR.font = [UIFont systemFontOfSize:16.0f];
        _RR.adjustsFontSizeToFitWidth = YES;
        [self.contentView addSubview:_RR];
        _time = [[UILabel alloc] initWithFrame:CGRectMake(120.0f, 55.0f, 220.0f, 18.0f)];
        //_time.text = @"2014/10/10 12:12:12";
        _time.font = [UIFont systemFontOfSize:16.0f];
        [self.contentView addSubview:_time];
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
