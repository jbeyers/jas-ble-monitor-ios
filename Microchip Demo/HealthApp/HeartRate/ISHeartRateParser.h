//
//  ISHeartRateParser.h
//  Weight Scale
//
//  Created by Rick on 2014/10/30.
//  Copyright (c) 2014年 Rick. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef struct _HEART_RATE_FLAG {
    uint8_t format:1;
    uint8_t contact_status:2;
    uint8_t energy_expended:1;
    uint8_t RR_interval:1;
    uint8_t reserved:3;
}__attribute__((packed)) HEART_RATE_FLAG;

@interface ISHeartRateParser : NSObject
- (NSDictionary *)decodeData:(NSData *)data;

@end
