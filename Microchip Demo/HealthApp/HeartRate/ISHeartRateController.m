//
//  ISHeartRateController.m
//  Weight Scale
//
//  Created by Rick on 2014/10/30.
//  Copyright (c) 2014年 Rick. All rights reserved.
//

#import "ISHeartRateController.h"
#import "HKConnectViewController.h"
#import "HKCBController.h"
#import "MPPlot.h"
#import "AppDelegate.h"
#import "ISHeartRateGraphController.h"
#import "ISHeartRateSettingController.h"

@interface ISHeartRateController ()<HKCBControllerDelegate,MyPeripheralDelegate> {
    HKConnectViewController *scan;
    UIButton *scan_but;
    UIImageView *connect_icon;
    HKMyPeripheral *connect_peripheral;
    MPPlot *graph;
    UILabel *unit;
    UILabel *value;
    UILabel *energy;
    UILabel *sensorLocation;
}

@end

@implementation ISHeartRateController

- (void)loadView {
    if ([[UIScreen mainScreen] bounds].size.height > 480.0f) {
        [super loadView];
    }
    else {
        UIScrollView *view = [[UIScrollView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        view.contentSize = CGSizeMake(320.0f, 568.0f);
        //view.contentInset = UIEdgeInsetsMake(-64.0f, 0, 0, 0);
        self.view = view;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
#ifdef USE_HEALTH_KIT
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if ([appDelegate isHealthKitAvailable]) {
        [appDelegate.healthStore requestAuthorizationToShareTypes:[NSSet setWithObjects:[HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierHeartRate], nil] readTypes:[NSSet setWithObjects:[HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierHeartRate], nil] completion:^(BOOL success, NSError *error) {
            if (success) {
                NSLog(@"Authorization Success");
            }
        }];
    }
#endif
    CGRect rect = [[UIScreen mainScreen]bounds];
    self.title = @"Heart Rate";
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:@selector(dismiss)];
    UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithTitle:@"Settings" style:UIBarButtonItemStylePlain target:self action:@selector(showSetting:)];
    self.navigationItem.rightBarButtonItem = right;
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    scan = [[HKConnectViewController alloc] initWithNibName:nil bundle:nil];
    scan.uuids = @[UUIDSTR_HEART_RATE_SERVICE];
    scan.serviceUuids = @[[CBUUID UUIDWithString:UUIDSTR_DEVICE_INFO_SERVICE],[CBUUID UUIDWithString:UUIDSTR_HEART_RATE_SERVICE]];
    scan.delegate = self;
    UIView *info_bg = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, rect.size.width, 350.0f)];
    info_bg.backgroundColor = [UIColor colorWithRed:1.000 green:0.435 blue:0.812 alpha:1.000];
    [self.view addSubview:info_bg];
    unit = [[UILabel alloc] initWithFrame:CGRectMake(250.0f, 70.0f, 100.0f, 32.0f)];
    unit.text = @"BPS";
    unit.textColor = [UIColor whiteColor];
    unit.textAlignment = NSTextAlignmentCenter;
    [info_bg addSubview:unit];
    value = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 160.0f, rect.size.width, 100.0f)];
    value.text = @"0";
    value.font = [UIFont boldSystemFontOfSize:90.0f];
    value.textColor = [UIColor whiteColor];
    value.textAlignment = NSTextAlignmentCenter;
    [info_bg addSubview:value];
    energy = [[UILabel alloc] initWithFrame:CGRectMake(5.0f, 325.0f, 315.0f, 25.0f)];
    //energy.text = @"0 J";
    //energy.font = [UIFont boldSystemFontOfSize:23.0f];
    energy.textColor = [UIColor whiteColor];
    energy.textAlignment = NSTextAlignmentLeft;
    [info_bg addSubview:energy];
    sensorLocation = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 300.0f, 315.0f, 50.0f)];
    sensorLocation.numberOfLines = 2;
    //sensorLocation.text = @"Sensor Location\nOther";
    //sensorLocation.font = [UIFont boldSystemFontOfSize:23.0f];
    sensorLocation.textColor = [UIColor whiteColor];
    sensorLocation.textAlignment = NSTextAlignmentRight;
    [info_bg addSubview:sensorLocation];

    connect_icon = [[UIImageView alloc] initWithFrame:CGRectMake(10.0f, 70.0f, 40.0f, 40.0f)];
    connect_icon.image = [UIImage imageNamed:@"heart-white.png"];
    connect_icon.alpha = 0.5;
    [info_bg addSubview:connect_icon];
    UIButton *history = [UIButton buttonWithType:UIButtonTypeCustom];
    history.frame = info_bg.bounds;
    [history addTarget:self action:@selector(showGraph:) forControlEvents:UIControlEventTouchUpInside];
    [info_bg addSubview:history];

    UIView *polt_bg = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 350.0f, rect.size.width, 150.0f)];
    polt_bg.backgroundColor = [UIColor colorWithRed:0.522 green:1.000 blue:0.766 alpha:1.000];
    [self.view addSubview:polt_bg];
    graph =[MPPlot plotWithType:MPPlotTypeBars frame:CGRectMake(0, 0, 320, 150)];
    //graph.waitToUpdate = YES;
    /*[graph setAlgorithm:^CGFloat(CGFloat x) {
        return (arc4random()%1024)/1024.0;
    } numberOfPoints:10];*/
    graph.graphColor = [UIColor whiteColor];
    graph.detailTextColor = [UIColor blackColor];
    [polt_bg addSubview:graph];
    
    UIView *scan_bg = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 500.0f, rect.size.width, 68.0f)];
    scan_bg.backgroundColor = [UIColor colorWithRed:0.290 green:0.828 blue:1.000 alpha:1.000];
    [self.view addSubview:scan_bg];
    scan_but = [UIButton buttonWithType:UIButtonTypeCustom];
    [scan_but setTitle:@"Scan" forState:UIControlStateNormal];
    scan_but.titleLabel.font = [UIFont boldSystemFontOfSize:24.0f];
    scan_but.frame = CGRectMake(0.0f, 0.0f, 320.0f, 68.0f);
    scan_but.center = CGPointMake(rect.size.width/2, 34.0f);
    scan_but.tag = 0;
    [scan_bg addSubview:scan_but];
    [scan_but addTarget:self action:@selector(scan) forControlEvents:UIControlEventTouchUpInside];

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    NSString *uuid_string = [[NSUserDefaults standardUserDefaults] objectForKey:UUIDSTR_HEART_RATE_SERVICE];
    if (uuid_string) {
        NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:uuid_string];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            //[scan connectDeviceWithIdentifier:uuid];
        });
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dismiss {
    [self dismissViewControllerAnimated:YES completion:^{
        NSLog(@"dismissViewControllerAnimated");
        [scan release];
    }];
}

- (void)scan {
    if ([[scan_but titleForState:UIControlStateNormal] isEqualToString:@"Scan"]) {
        [self.navigationController pushViewController:scan animated:YES];
    }
    if (connect_peripheral) {
        [scan disconnectDevice:connect_peripheral];
        connect_peripheral = nil;
        connect_icon.alpha = 0.5;
        connect_icon.image = [UIImage imageNamed:@"heart-white.png"];
        [scan_but setTitle:@"Scan" forState:UIControlStateNormal];
    }
}

- (void)resetText {
    value.text = @"0";
    energy.text = @"";
    sensorLocation.text = @"";
    graph.values = @[];
}

- (void)showGraph:(id)sender {
    ISHeartRateGraphController *view = [[ISHeartRateGraphController alloc] init];
    [self.navigationController pushViewController:view animated:YES];
}

- (void)showSetting:(id)sender {
    ISHeartRateSettingController *view = [[ISHeartRateSettingController alloc] initWithStyle:UITableViewStyleGrouped];
    view.peripheral = connect_peripheral;
    [self.navigationController pushViewController:view animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)didUpdatePeripheralList:(NSArray *)peripherals {
    
}

- (void)didConnectPeripheral:(HKMyPeripheral *)peripheral {
    if (peripheral.connectStaus == HK_MYPERIPHERAL_CONNECT_STATUS_CONNECTED) {
        connect_icon.alpha = 1.0f;
        connect_icon.image = [UIImage imageNamed:@"heart-red.png"];
        [scan_but setTitle:@"Disconnect" forState:UIControlStateNormal];
    }
    else {
        [scan_but setTitle:@"Scan" forState:UIControlStateNormal];
        connect_icon.alpha = 0.5f;
        connect_icon.image = [UIImage imageNamed:@"heart-white.png"];
    }
    connect_peripheral = peripheral;
    peripheral.transDataDelegate = self;
    [self resetText];
}

- (void)didDisconnectPeripheral:(HKMyPeripheral *)peripheral {
    [scan_but setTitle:@"Scan" forState:UIControlStateNormal];
    connect_icon.alpha = 0.5f;
    connect_icon.image = [UIImage imageNamed:@"heart-white.png"];
    peripheral.transDataDelegate = nil;
}

- (void)MyPeripheral:(HKMyPeripheral *)peripheral didUpdateSensorLocation:(SensorLocation)location error:(NSError *)error {
    switch (location) {
        case SensorLocation_Other:
            sensorLocation.text = @"Sensor Location\nOther";
            break;
        case SensorLocation_Chest:
            sensorLocation.text = @"Sensor Location\nChest";
            break;
        case SensorLocation_Wrist:
            sensorLocation.text = @"Sensor Location\nWrist";
            break;
        case SensorLocation_Finger:
            sensorLocation.text = @"Sensor Location\nFinger";
            break;
        case SensorLocation_Hand:
            sensorLocation.text = @"Sensor Location\nHand";
            break;
        case SensorLocation_EarLobe:
            sensorLocation.text = @"Sensor Location\nEarLobe";
            break;
        case SensorLocation_Foot:
            sensorLocation.text = @"Sensor Location\nFoot";
            break;
        default:
            sensorLocation.text = @"Sensor Location\nOther";
            break;
    }
}

- (void)MyPeripheral:(HKMyPeripheral *)peripheral didUpdateHeartRateData:(NSDictionary *)data error:(NSError *)error {
    value.text = [NSString stringWithFormat:@"%ld",(long)[data[@"heart_rate"] integerValue]];
    energy.text = [NSString stringWithFormat:@"%ld J",(long)[data[@"energy_expended"] integerValue]];
    graph.values = data[@"RR_Interval"];
}
@end
