//
//  HeartRateData.m
//  Health
//
//  Created by Rick on 2014/11/4.
//  Copyright (c) 2014年 Rick. All rights reserved.
//

#import "HeartRateData.h"


@implementation HeartRateData

@dynamic energyExpended;
@dynamic heartRateValue;
@dynamic rr;
@dynamic sensorContactStatus;
@dynamic timestamp;

@end

@implementation ArrayTransformer

+ (Class)transformedValueClass
{
    return [NSArray class];
}

+ (BOOL)allowsReverseTransformation
{
    return YES;
}

- (id)transformedValue:(id)value
{
    return [NSKeyedArchiver archivedDataWithRootObject:value];
}

- (id)reverseTransformedValue:(id)value
{
    return [NSKeyedUnarchiver unarchiveObjectWithData:value];
}

@end
