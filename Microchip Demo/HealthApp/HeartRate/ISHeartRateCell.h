//
//  ISHeartRateCell.h
//  Health
//
//  Created by Rick on 2014/11/5.
//  Copyright (c) 2014年 Rick. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISHeartRateCell : UITableViewCell
@property (strong) UILabel *heartRate;
@property (strong) UILabel *energy;
@property (strong) UILabel *sensorContactStatus;
@property (strong) UILabel *RR;
@property (strong) UILabel *time;

@end
