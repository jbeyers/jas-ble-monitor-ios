//
//  ISHeartRateSettingController.h
//  Health
//
//  Created by Rick on 2014/11/6.
//  Copyright (c) 2014年 Rick. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MyPeripheral;
@interface ISHeartRateSettingController : UITableViewController
@property (nonatomic,strong)MyPeripheral *peripheral;
@end
