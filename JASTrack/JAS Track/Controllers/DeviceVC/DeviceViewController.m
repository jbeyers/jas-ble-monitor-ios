//
//  DeviceViewController.m
//  JAS Track
//
//  Created by Yuriy on 2/15/19.
//  Copyright © 2019 Yuriy. All rights reserved.
//

#import "DeviceViewController.h"
#import "DeviceInfoCell.h"
#import "DeviceTimerCell.h"
#import "DeviceSessionCell.h"
#import "UsageDataViewController.h"
#import "CBController.h"
#import "NSData+Conversion.h"
#import "DeviceInfo.h"
#import "CommonHelper.h"
#import "NSString+Extension.h"

#define kRowType        @"row-type"
#define kRowTitle       @"row-title"
#define kRowValue       @"row-value"
#define kDeviceInfo     @"device-info"
#define kHoldTimer      @"hold-timer"
#define kSessionStatus  @"session-status"


#define BM77SPP_HW @"353035305f535050"
#define BM77SPP_FW @"32303233303230"
#define BM77SPP_AD 0xd97e

#define BLETR_HW @"353035305f424c455452"
#define BLETR_FW @"32303032303130"
#define BLETR_AD 0x4833

@interface DeviceViewController () <MyPeripheralDelegate, ReliableBurstDataDelegate, CBControllerDelegate>
{
    CBCharacteristicWriteType writeType;
}

@property (weak, nonatomic) IBOutlet UILabel *lblBattery;
@property (weak, nonatomic) IBOutlet UILabel *lblTemp;
@property (weak, nonatomic) IBOutlet UILabel *lblRotation;
@property (weak, nonatomic) IBOutlet UILabel *lblProximity1;
@property (weak, nonatomic) IBOutlet UILabel *lblProximity2;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblStretches;
@property (weak, nonatomic) IBOutlet UILabel *lblHoldTime;
@property (weak, nonatomic) IBOutlet UITextView *tvSessionStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblProduct;
@property (weak, nonatomic) IBOutlet UILabel *lblSN;
@property (weak, nonatomic) IBOutlet UILabel *lblRevision;

@property (weak, nonatomic) IBOutlet UILabel *lblValidPackets;
@property (weak, nonatomic) IBOutlet UILabel *lblReceivedACKs;
@property (weak, nonatomic) IBOutlet UILabel *lblDroppedPacket;
@property (weak, nonatomic) IBOutlet UILabel *lblSentPackets;
@property (weak, nonatomic) IBOutlet UILabel *lblSentACKs;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *snLoadingIndicator;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeightConstraint;

@property (weak, nonatomic) IBOutlet UIButton *btnDisConnect;
@property (weak, nonatomic) IBOutlet UIButton *btnGetUsageData;
@property (weak, nonatomic) IBOutlet UIButton *btnSetupMonitor;
@property (weak, nonatomic) IBOutlet UIButton *btnCalibrateClock;
@property (weak, nonatomic) IBOutlet UIView *calibrateView;
@property (weak, nonatomic) IBOutlet UIView *calibrateAlertView;

@property (nonatomic, retain) NSString *hardwareRevision;
@property (nonatomic, retain) NSString *firmwareRevision;
@property (nonatomic, retain) DeviceInfo *deviceInfo;
@property (nonatomic, retain) NSString *settingSerialNumber;
@property BOOL settingISLeft;
@property BOOL didRetriedRequest;
@end

@implementation DeviceViewController {
    NSMutableString *content;
    NSTimer *deviceTimer;
    NSString *currentCalibrate;
    BOOL didReceivedSetTimeResponse;
    BOOL isPatch;
    BOOL sentSN;
}

@synthesize connectedPeripheral;
@synthesize deviceInfo;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden = YES;

    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        CGFloat screenHeight = screenSize.height > screenSize.width ? screenSize.height : screenSize.width;
        CGFloat height = screenHeight - 270; //270 is topbar height
        _contentViewHeightConstraint.constant = height;
    }
    
    content = [[NSMutableString alloc] initWithCapacity:100008];

    deviceInfo = [DeviceInfo sharedInstance];
    deviceInfo.myPeripheral = connectedPeripheral;

    writeType = CBCharacteristicWriteWithResponse;

    if (_cbController == nil) {
        _cbController = [[CBController alloc] init];
    }

    deviceInfo.sessionStatusMA = [NSMutableArray new];

    _tvSessionStatus.layer.cornerRadius = 4.0f;
    _tvSessionStatus.layer.borderWidth = 1.0f;
    _tvSessionStatus.layer.borderColor = [UIColor grayColor].CGColor;
    _calibrateAlertView.layer.cornerRadius = 8.0;
    _calibrateView.backgroundColor = [UIColor clearColor];
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:gestureRecognizer];
    
    [self updateDeviceInfo];
}

- (void)updateUI {
    self.view.backgroundColor = UIColor.whiteColor;
    [CommonHelper applySketchShadow:_calibrateAlertView.layer color:[UIColor darkGrayColor] alpha:0.5 x:0 y:4 blur:8 spread:0];
    
    if (@available(iOS 12.0, *)) {
       if (self.traitCollection.userInterfaceStyle == UIUserInterfaceStyleDark) { //Dark Mode
           self.view.backgroundColor = UIColor.blackColor;
           for (UIButton *btn in @[_btnDisConnect, _btnGetUsageData, _btnSetupMonitor, _btnCalibrateClock]) {
               btn.backgroundColor = UIColor.whiteColor;
               [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
           }
           _snLoadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
           _tvSessionStatus.textColor = [UIColor whiteColor];
           _logoImageView.image = [UIImage imageNamed:@"dark_logo.png"];
           [CommonHelper applySketchShadow:_calibrateAlertView.layer color:[UIColor blackColor] alpha:0.8 x:0 y:4 blur:8 spread:0];
       } else {
           self.view.backgroundColor = UIColor.whiteColor;
           for (UIButton *btn in @[_btnDisConnect, _btnGetUsageData, _btnSetupMonitor, _btnCalibrateClock]) {
               btn.backgroundColor = UIColor.blackColor;
               [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
           }
           _snLoadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
           _tvSessionStatus.textColor = [UIColor blackColor];
           _logoImageView.image = [UIImage imageNamed:@"v_logo.png"];
       }
    }
}

- (void)dismissKeyboard
{
    [self.view endEditing:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    _cbController.delegate = self;

    connectedPeripheral.deviceInfoDelegate = self;
    connectedPeripheral.proprietaryDelegate = self;
    connectedPeripheral.transDataDelegate = self;

    if (isPatch) {
        [connectedPeripheral setTransDataNotification:TRUE];
    }
    else {
        [connectedPeripheral readHardwareRevision];
    }
    connectedPeripheral.transmit.delegate = self;

    [self updateDeviceInfo];
    [self updateSessionInfos];
    
#if MOCK
    deviceInfo.serialNumber = @"00A1234";
    __weak DeviceViewController *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf updateDeviceInfo];
    });
#endif
}

- (void)dealloc {
    if (deviceTimer != nil) {
        [deviceTimer invalidate];
        deviceTimer = nil;
    }
}

- (void)updateDeviceInfo {
    NSString *batteryInfo;
    
    if (deviceInfo.batteryVoltage < LOWER_BATEERY_BELOW) {
        batteryInfo = [NSString stringWithFormat:@"BATTERY: %.01f (LOW BATTERY)", deviceInfo.batteryVoltage];
    } else {
        batteryInfo = [NSString stringWithFormat:@"BATTERY: %.01f", deviceInfo.batteryVoltage];
    }
    
    _lblBattery.text = batteryInfo;
    
    
    _lblRevision.text = [NSString stringWithFormat:@"FIRMWARE REVISION: %ld", deviceInfo.firmwareRevision];
    _lblTemp.text = [NSString stringWithFormat:@"TEMPERATURE: %ld", deviceInfo.temperature];
    _lblRotation.text = [NSString stringWithFormat:@"ROTATION: %ld", deviceInfo.rotation];
    _lblProximity1.text = [NSString stringWithFormat:@"PROXIMITY: %@", deviceInfo.proximity1 ? @"Yes" : @"No"];
    _lblProximity2.text = [NSString stringWithFormat:@"KNOB ROTATION: %@", deviceInfo.proximity2 ? @"Clockwise" : @"Counterclockwise"];
    _lblDate.text = [NSString stringWithFormat:@"DATE: %@", [deviceInfo getDisplayDateString]];
    _lblTime.text = [NSString stringWithFormat:@"TIME: %@", [deviceInfo getDisplayTimeString]];
    _lblStretches.text = [NSString stringWithFormat:@"STRETCHES: %@",[NSString stringWithFormat:@"%ld", deviceInfo.stretches]];
    _lblHoldTime.text = [deviceInfo getDisplayHoldTime];
    
    if (deviceInfo.serialNumber == nil) {
        _lblProduct.text = @"PRODUCT: ";
        _lblSN.text = @"SERIAL NUMBER: ";
        _snLoadingIndicator.hidden = NO;
    } else {
        _lblProduct.text = [NSString stringWithFormat:@"PRODUCT: %@", [deviceInfo getProduct]];
        _lblSN.text = [NSString stringWithFormat:@"SERIAL NUMBER: %@", deviceInfo.serialNumber];
        _snLoadingIndicator.hidden = YES;
    }
    
    [self updatePacketsInfo];
    
    #if MOCK
        deviceInfo.firmwareRevision = 4063;
    
        _lblBattery.text = @"BATTERY: 2.8";
        
    _lblRevision.text = [NSString stringWithFormat:@"FIRMWARE REVISION: %ld", deviceInfo.firmwareRevision];
        _lblTemp.text = @"TEMPERATURE: 77";
        _lblRotation.text = @"ROTATION: 347";
        _lblProximity1.text = [NSString stringWithFormat:@"PROXIMITY: %@", deviceInfo.proximity1 ? @"Yes" : @"No"];
        _lblProximity2.text = [NSString stringWithFormat:@"KNOB ROTATION: %@", deviceInfo.proximity2 ? @"Clockwise" : @"Counterclockwise"];
        _lblDate.text = @"DATE: 1/1/19";
        _lblTime.text = @"TIME: 01:16:09";
        _lblStretches.text = @"STRETCHES: 4";
        _lblHoldTime.text = [deviceInfo getDisplayHoldTime];
    #endif
}

- (void)updatePacketsInfo {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.lblReceivedACKs.text = [[NSString stringWithFormat:@"Received ACKs: %ld", self.deviceInfo.receivedACKsCount] uppercaseString];
        self.lblDroppedPacket.text = [[NSString stringWithFormat:@"Dropped Packets: %ld", self.deviceInfo.droppedPacketsCount] uppercaseString];
        self.lblValidPackets.text = [[NSString stringWithFormat:@"Valid Packets: %ld", self.deviceInfo.validPacketsCount] uppercaseString];
        self.lblSentPackets.text = [[NSString stringWithFormat:@"Sent Packets: %ld", self.deviceInfo.sentPacketsCount] uppercaseString];
        self.lblSentACKs.text = [[NSString stringWithFormat:@"Sent ACKs: %ld", self.deviceInfo.sentACKsCount] uppercaseString];
    });
    
}

- (void)updateSessionInfos {
    NSString *string = @"";
    
    for (NSString *info in deviceInfo.sessionStatusMA) {
        if ([string isEqualToString:@""]) {
            string = info;
        } else {
            string = [NSString stringWithFormat:@"%@ \n%@", string, info];
        }
    }
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:string];

    UIColor *textviewColor = [UIColor blackColor];
    if (@available(iOS 12.0, *)) {
        if (self.traitCollection.userInterfaceStyle == UIUserInterfaceStyleDark) {
            textviewColor = [UIColor whiteColor];
        }
    }
    
    [attrString addAttribute:NSForegroundColorAttributeName value:textviewColor range:NSMakeRange(0, string.length)];
    _tvSessionStatus.attributedText = attrString;
    
    [self scrollTextViewToBottom:_tvSessionStatus];
}

-(void)scrollTextViewToBottom:(UITextView *)textView {
    if(textView.text.length > 0 ) {
        NSRange bottom = NSMakeRange(textView.text.length -1, 1);
        [textView scrollRangeToVisible:bottom];
    }
    
}

#pragma Mark - Define Actions here
- (IBAction)onDisconnectAction:(id)sender {
    NSData *responseData = [NSString getDataFromHexString:JAS_COMMAND_DISCONNECT_REQUEST];
    if (responseData != nil) {
        [self sendTransparentData:responseData isACK: NO];
    }
    
    [_cbController disconnectDevice:connectedPeripheral];
    
    [self performSelector:@selector(popToRootPage) withObject:nil afterDelay:0.5];
}

- (IBAction)onGetUsageDataAction:(id)sender {
    if (self.deviceInfo.serialNumber == nil) {
        [self showAlertWithErrorMessage:@"Please wait until loading a serial number of your device."];
        return;
    }
    UsageDataViewController *dataVC = [self.storyboard instantiateViewControllerWithIdentifier:@"UsageDataViewController"];
    dataVC.connectedPeripheral = self.connectedPeripheral;
    dataVC.cbController = self.cbController;
    dataVC.deviceInfo = self.deviceInfo;
    [self.navigationController pushViewController:dataVC animated:true];
}

- (IBAction)onSetupMonitorAction:(id)sender {
    _settingSerialNumber = @"";
    _settingISLeft = NO;
    
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Set Serial Number"
                                          message:nil
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 200, 40)];
    textField.translatesAutoresizingMaskIntoConstraints = NO;
    textField.borderStyle = UITextBorderStyleRoundedRect;
    textField.placeholder = @"Ex - 12A1234";
    textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    textField.autocorrectionType = UITextAutocorrectionTypeNo;
    textField.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    [alertController.view addSubview:textField];

    [NSLayoutConstraint activateConstraints:@[
            [textField.topAnchor constraintEqualToAnchor:alertController.view.topAnchor constant:50],
            [textField.leadingAnchor constraintEqualToAnchor:alertController.view.leadingAnchor constant:12],
            [textField.trailingAnchor constraintEqualToAnchor:alertController.view.trailingAnchor constant:-12],
            [textField.heightAnchor constraintEqualToConstant:35]
                                              ]];
    
    UILabel *label = [[UILabel alloc] init];
    label.text = @"Left Hand Device";
    label.textColor = UIColor.grayColor;
    label.tag = 0;
    [label sizeToFit];
    
    UISwitch *orderSwitch = [[UISwitch alloc] init];
    orderSwitch.on = NO;
    [orderSwitch setOn:NO animated:YES];
    
    [orderSwitch.layer setValue:label forKey:@"label"];
    
    UIStackView *stackView = [[UIStackView alloc] init];
    stackView.axis = UILayoutConstraintAxisHorizontal;
    stackView.spacing = 8;
    
    [stackView addArrangedSubview:label];
    [stackView addArrangedSubview:orderSwitch];
    
    [alertController.view addSubview:stackView];
    
    // center the stack in the alert
    [stackView.trailingAnchor constraintEqualToAnchor:alertController.view.trailingAnchor constant:-12].active = YES;
    
    // turn off the autoresizing mask or things get weird
    stackView.translatesAutoresizingMaskIntoConstraints = NO;
    
    // use a topAnchor constraint to place the stackview, just below the title
    // TODO:  figure out how to get the height of the alert title (use 64 for now)
    [stackView.topAnchor constraintEqualToAnchor:textField.bottomAnchor constant:8].active = YES;
    
    // layout now to set the view bounds so far - NOTE this does not include the action buttons
    [alertController.view layoutIfNeeded];

    CGFloat height = alertController.view.bounds.size.height + 104 + stackView.bounds.size.height;
    [alertController.view.heightAnchor constraintEqualToConstant:height].active = YES;
    
    __weak DeviceViewController *weakSelf = self;
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   NSString *serialNumber = textField.text;
                                   //NSLog(@"Serial Number %@", serialNumber);
                                   
                                   if (serialNumber.length != 7) {
                                       [weakSelf showAlertWithErrorMessage:@"Invalid Serial Number!!"];
                                   } else {
                                       weakSelf.didRetriedRequest = NO;
                                       [weakSelf setupSerialNumber:serialNumber isLeft:orderSwitch.on];
                                   }
                                   
                               }];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)setupSerialNumber:(NSString *)serialNumber isLeft:(BOOL)left {
    //Serial Number Packet
    if (serialNumber.length == 7) {
        _settingSerialNumber = serialNumber;
        _settingISLeft = left;
        
        NSString *product = [serialNumber substringWithRange:NSMakeRange(0, 2)];
        if (![CommonHelper isOnlyNumbers:product]) {
            [self showAlertWithErrorMessage:@"Invalid Serial Number!!"];
            return;
        }
        
        NSString *revision = [serialNumber substringWithRange:NSMakeRange(2, 1)].uppercaseString;
        if (![CommonHelper isOnlyLetters:revision]) {
            [self showAlertWithErrorMessage:@"Invalid Serial Number!!"];
            return;
        }
        
        NSString *uniqueId1 = [serialNumber substringWithRange:NSMakeRange(3, 2)];
        if (![CommonHelper isOnlyNumbers:uniqueId1]) {
            [self showAlertWithErrorMessage:@"Invalid Serial Number!!"];
            return;
        }
        
        NSString *uniqueId2 = [serialNumber substringWithRange:NSMakeRange(5, 2)];
        if (![CommonHelper isOnlyNumbers:uniqueId2]) {
            [self showAlertWithErrorMessage:@"Invalid Serial Number!!"];
            return;
        }
        
        int asciiCode = [revision characterAtIndex:0];
        // JEB - NEED CODE FOR LEFT AT IN THE FIRST 2 0, SHOULD BE 00 for not checked, and 01 for checkeds
        NSString* setSNPacketRequest = [NSString stringWithFormat:@"CCC0%@%X%@%@%@0000C0", product, asciiCode, uniqueId1, uniqueId2, left ? @"01" : @"00"];
        
        NSData *resData = [NSString getDataFromHexString:setSNPacketRequest];
        if (resData != nil) {
            [self sendTransparentData:resData isACK:NO];
        }
        
        //Set timer for 5 secs
        didReceivedSetTimeResponse = NO;
        sentSN = NO;
        
        [self.loadingIndicator setLoadingText:@"Setting up device..."];
        self.loadingIndicator.visibility = YES;
        deviceTimer = [NSTimer scheduledTimerWithTimeInterval:JAS_WAITING_TIME_WITH_RETRY target:self selector:@selector(timeoutForSerialNumber) userInfo:nil repeats:NO];
    } else {
        [self setTime];
    }
}

- (void)timeoutForSerialNumber {
    if (!didReceivedSetTimeResponse) {
        if (_didRetriedRequest == NO) {
            _didRetriedRequest = YES;
            
            [self setupSerialNumber:_settingSerialNumber isLeft:_settingISLeft];
            return;
        }
        
        //time out
        self.loadingIndicator.visibility = NO;
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Session Time Out" message:nil
                                                                preferredStyle:UIAlertControllerStyleAlert];
        __weak DeviceViewController *weakSelf = self;
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            weakSelf.didRetriedRequest = NO;
            
            [weakSelf setupSerialNumber:weakSelf.settingSerialNumber isLeft:weakSelf.settingISLeft];
        }];
        [alert addAction:action];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                             handler:^(UIAlertAction *action) {
                                                             }];
        [alert addAction:cancelAction];
        
        [self runInMainThread:^{
            [self presentViewController:alert animated:YES completion:nil];
        }];
    
        [deviceTimer invalidate];
        deviceTimer = nil;
    }
}

- (void)timeoutForSetTime {
    if (!didReceivedSetTimeResponse) {
        if (_didRetriedRequest == NO) {
            _didRetriedRequest = YES;
            
            [self setTime];
            return;
        }
        
        //time out
        self.loadingIndicator.visibility = NO;
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Session Time Out" message:nil
                                                                preferredStyle:UIAlertControllerStyleAlert];
        __weak DeviceViewController *weakSelf = self;
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            weakSelf.didRetriedRequest = NO;
            
            [weakSelf setTime];
        }];
        [alert addAction:action];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                             handler:^(UIAlertAction *action) {
                                                             }];
        [alert addAction:cancelAction];
        
        [self runInMainThread:^{
            [self presentViewController:alert animated:YES completion:nil];
        }];
    }
    
    [deviceTimer invalidate];
    deviceTimer = nil;
}

- (void)setTime {
    //Time Packet
    NSDate *now = [NSDate date];
    
    unsigned units = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [calendar components:units fromDate:now];
    
    int month = (int)[components month];
    int day = (int)[components day];
    int hour = (int)[components hour];
    int minute = (int)[components minute];
    int second = (int)[components second];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yy"];
    NSString *yearString = [df stringFromDate:[NSDate date]];
    
    NSLog(@" year %@ - month %d - day %d - hour %d - minute %d - second %d", yearString, month, day, hour, minute, second);
    
    NSString* setTimePacketRequestLog = [NSString stringWithFormat: @"Set Time: %d/%d/%@ - %d:%d:%d", month, day, yearString, hour, minute, second];

    [deviceInfo.sessionStatusMA addObject: setTimePacketRequestLog];
    //
    //__weak DeviceViewController *weakSelf = self;
    //dispatch_async(dispatch_get_main_queue(), ^{
    //    [weakSelf updateSessionInfos];
    //});
    
    NSString* setTimePacketRequest = [NSString stringWithFormat:@"CC50%@%02d%02d%02d%02d%02d0050", yearString, month,day, hour, minute, second];
    NSData *responseData = [NSString getDataFromHexString:setTimePacketRequest];
    if (responseData != nil) {
        [self sendTransparentData:responseData isACK:NO];
    }
    

    [deviceInfo.sessionStatusMA addObject: setTimePacketRequest];
    
    __weak DeviceViewController *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf updateSessionInfos];
    });
    
    //Set timer for 5 secs
    didReceivedSetTimeResponse = NO;
    sentSN = YES;
    
    [deviceTimer invalidate];
    deviceTimer = [NSTimer scheduledTimerWithTimeInterval:JAS_WAITING_TIME_WITH_RETRY target:self selector:@selector(timeoutForSetTime) userInfo:nil repeats:NO];
}

- (void)timeoutSetTime {
    if (!didReceivedSetTimeResponse) {
        //time out
        self.loadingIndicator.visibility = NO;
        [self showAlertWithErrorMessage:@"Session Time Out"];
    }
    
    [deviceTimer invalidate];
    deviceTimer = nil;
}

- (IBAction)onIncrement:(id)sender {
    didReceivedSetTimeResponse = NO;
    _didRetriedRequest = NO;
    
    [self calibrateClock:JAS_CALIBRATE_CLOCK_INCREMENT];
}
- (IBAction)onDecrement:(id)sender {
    didReceivedSetTimeResponse = NO;
    _didRetriedRequest = NO;
    [self calibrateClock:JAS_CALIBRATE_CLOCK_DECREMENT];
}
- (IBAction)onSet:(id)sender {
//    didReceivedSetTimeResponse = NO;
//    _didRetriedRequest = NO;
    [self calibrateClock:JAS_CALIBRATE_CLOCK_SET];
    _calibrateView.hidden = YES;
}

- (IBAction)onCancel:(id)sender {
    _calibrateView.hidden = YES;
    [self calibrateClock:JAS_CALIBRATE_CLOCK_CANCEL];
}

- (IBAction)calibrateClockAction:(id)sender {
    //send configure packet
    didReceivedSetTimeResponse = NO;
    _didRetriedRequest = NO;
    
    _calibrateView.hidden = NO;
    
    [self calibrateClock:JAS_CALIBRATE_CLOCK_CONFIGURE];
}

- (void)calibrateClock:(NSString *)sPackets {
    currentCalibrate = sPackets;
    NSData *responseData = [NSString getDataFromHexString:sPackets];
    if (responseData != nil) {
        [self sendTransparentData:responseData isACK: NO];
    }
    
    if ([currentCalibrate isEqualToString:JAS_CALIBRATE_CLOCK_INCREMENT] ||
        [currentCalibrate isEqualToString:JAS_CALIBRATE_CLOCK_DECREMENT]) {
        [self.loadingIndicator setLoadingText:@"Setting up device..."];
        self.loadingIndicator.visibility = YES;
        
        [deviceTimer invalidate];
        deviceTimer = [NSTimer scheduledTimerWithTimeInterval:JAS_WAITING_TIME_WITH_RETRY target:self selector:@selector(timeoutForCalibrate) userInfo:nil repeats:NO];
    } else if ([currentCalibrate isEqualToString:JAS_CALIBRATE_CLOCK_CONFIGURE]) {
        [self.loadingIndicator setLoadingText:@"Please wait..."];
        self.loadingIndicator.visibility = YES;
        
        [deviceTimer invalidate];
        deviceTimer = [NSTimer scheduledTimerWithTimeInterval:JAS_WAITING_TIME_WITH_RETRY target:self selector:@selector(timeoutForCalibrate) userInfo:nil repeats:NO];
    }
    
}

- (void)timeoutForCalibrate {
    if (!didReceivedSetTimeResponse) {
        if (_didRetriedRequest == NO) {
            _didRetriedRequest = YES;
            
            [self calibrateClock:currentCalibrate];
            return;
        }
        
        //time out
        self.loadingIndicator.visibility = NO;
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Session Time Out" message:nil
                                                                preferredStyle:UIAlertControllerStyleAlert];
        __weak DeviceViewController *weakSelf = self;
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            weakSelf.didRetriedRequest = NO;
            
            [weakSelf calibrateClock:currentCalibrate];
        }];
        [alert addAction:action];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                             handler:^(UIAlertAction *action) {
                                                             }];
        [alert addAction:cancelAction];
        
        [self runInMainThread:^{
            [self presentViewController:alert animated:YES completion:nil];
        }];
    
        [deviceTimer invalidate];
        deviceTimer = nil;
    }
}

// CBCharacteristic Management
- (void)sendTransparentData:(NSData *)data isACK: (BOOL)isACK {
    NSLog(@"sendTransparentData:%@", data);
    
    //
    if (!isACK) {
        deviceInfo.sentPacketsCount ++;
        [self updatePacketsInfo];
    }
    //-end
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self->connectedPeripheral sendTransparentData:data type:self->writeType];
    });
    
}

#pragma Mark - MyPeripheralDelegate
- (void)MyPeripheral:(MyPeripheral *)peripheral didReceiveTransparentData:(NSData *)data {
    
    if ([data length] > 0) {
        NSString *strData = [data hexadecimalString];
        NSLog(@"didReceiveTransparentData  %@", strData);
        // Make sure that the packet is the correct size
        // SHOULD WE MAKE 10 and 20 #defines or globals?
        int index = 0;
        NSLog(@"packet length: %ld", strData.length);
        while (index <= (strData.length - JAS_PACKET_STRING_LENGTH)) {
            if (strData.length < index + JAS_PACKET_STRING_LENGTH) {
                break;
            }
            
            NSString *str = [strData substringWithRange:NSMakeRange(index, JAS_PACKET_STRING_LENGTH)];
            
            index += JAS_PACKET_STRING_LENGTH;
         
            if (str.length == JAS_PACKET_STRING_LENGTH)
            {
                const char *chars = [str UTF8String];
                unsigned packet[JAS_PACKET_LENGTH] = {};
                int i = 0;
                int j = 0;
                
                char byteChars[4] = {'\0','\0','\0'};
                
                // JEB - populate an array for easier manipulation
                while (i < JAS_PACKET_STRING_LENGTH) {
                    byteChars[0] = chars[i++];
                    byteChars[1] = chars[i++];
                    packet[j++] = (unsigned int) strtoul(byteChars, NULL, 16);
                }
                
                //TODO: - Justin: implement checksum here
                if (packet[1] == packet[9]) {
                    deviceInfo.validPacketsCount++;
                    [self updatePacketsInfo];
                }
                //-end
                
                if (packet[0] == JAS_COMMAND_PACKET_FORMAT) {
                    //NSLog(@"Start Byte Found");
                    // Check packet type
                    if ((packet[1] & JAS_COMMAND_MASK) == JAS_SESSION_PACKET)
                    {
                        //NSLog(@"Decoding Session Packet");
                        // Check hold / stretch type
                        if ((packet[1] & JAS_COMMAND_STREATCH) == JAS_COMMAND_STREATCH) {
                            NSLog(@"Stretch Detected");     // This should be in the log on screen #3, but only on change not every packet
                            deviceInfo.detectedStretch = YES;
                            if (deviceInfo.sessionStatusMA.count == 0 || ![deviceInfo.sessionStatusMA.lastObject isEqualToString:@"Stretch Detected"]) {
                                [deviceInfo.sessionStatusMA addObject:@"Stretch Detected"];
                                
                                __weak DeviceViewController *weakSelf = self;
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [weakSelf updateSessionInfos];
                                });
                            }
                        }
                        if ((packet[1] & JAS_COMMAND_HOLD) == JAS_COMMAND_HOLD) {
                            NSLog(@"Hold Detected");        // This should be in the log on screen #3, but only on change not every packet
                            deviceInfo.detectedHold = YES;
                            if (deviceInfo.sessionStatusMA.count == 0 || ![deviceInfo.sessionStatusMA.lastObject isEqualToString:@"Hold Detected"]) {
                                [deviceInfo.sessionStatusMA addObject:@"Hold Detected"];
                                
                                __weak DeviceViewController *weakSelf = self;
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [weakSelf updateSessionInfos];
                                });
                            }
                        }
                        if (((packet[1] & JAS_COMMAND_SESSION) == JAS_COMMAND_SESSION_COMPLETE) && (packet[3] == 06)) {
                            //NSLog(@"Session Complete");        // This should be in the log on screen #3, but only on change not every packet
                            deviceInfo.completeSession = YES;
                            if (deviceInfo.sessionStatusMA.count == 0 || ![deviceInfo.sessionStatusMA.lastObject isEqualToString:@"Session Complete"]) {
                                [deviceInfo.sessionStatusMA addObject:@"Session Complete"];
                                
                                __weak DeviceViewController *weakSelf = self;
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [weakSelf updateSessionInfos];
                                });
                            }
                        }
                        
                        // Check proximity
                        if ((packet[1] & JAS_COMMAND_PROXIMITY_ONE) == JAS_COMMAND_PROXIMITY_ONE) {
                            //NSLog(@"Proximity 1 True");
                            deviceInfo.proximity1 = YES;
                        } else {
                            //NSLog(@"Proximity 1 False");
                            deviceInfo.proximity1 = NO;
                        }
                        if ((packet[1] & JAS_COMMAND_PROXIMITY_TWO) == JAS_COMMAND_PROXIMITY_TWO) {
                            //NSLog(@"Proximity 2 True");
                            deviceInfo.proximity2 = YES;
                        } else {
                            //NSLog(@"Proximity 2 False");
                            deviceInfo.proximity2 = NO;
                        }
                        
                        // Get Battery Voltage
                        float batteryVoltage = (float) packet[2]/10;
                        //NSLog(@"Voltage = %f", batteryVoltage);
                        deviceInfo.batteryVoltage = batteryVoltage;
                        
                        // Get Stretch count
                        uint stretches = packet[3];
                        //NSLog(@"Stretches = %i", stretches);
                        deviceInfo.stretches = stretches;
                        
                        // Get Curent Hold time
                        uint holdTime = (packet[4] << 8) + packet[5];
                        //NSLog(@"Seconds to hold: %i", holdTime); // THIS NEEDS TO BE CONVERTED AND DISPLAYED IN THE TIMER!
                        deviceInfo.holdTimeSeconds = holdTime;
                        
                        // Get Rotation
                        int rotation = (int)((packet[6] << 8) + packet[7]);
                        //NSLog(@"Rotation = %i", rotation);
                        deviceInfo.rotation = rotation;
                        
                        // Get Temperature
                        int temperature = (int)packet[8];
                        //NSLog(@"Temperature = %i", temperature);
                        deviceInfo.temperature = temperature;
                        // For now ignore checksum, not implemented
                        

                        
                        __weak DeviceViewController *weakSelf = self;
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [weakSelf updateDeviceInfo];
                        });
                    }
                    else if ((packet[1] & JAS_COMMAND_MASK) == JAS_TIME_PACKET)
                    {
                        //NSLog(@"Decoding Time Packet");
                        
                        // Get time
                        int year = (int)((((packet[2] >> 4) & 0x0F) * 10) + (packet[2] & 0x0F));
                        int month = (int)((((packet[3] >> 4) & 0x01) * 10) + (packet[3] & 0x0F));
                        int date = (int)((((packet[4] >> 4) & 0x03) * 10) + (packet[4] & 0x0F));
                        int hour = (int)((((packet[5] >> 4) & 0x03) * 10) + (packet[5] & 0x0F));
                        int minutes = (int)((((packet[6] >> 4) & 0x07) * 10) + (packet[6] & 0x0F));
                        int seconds = (int)((((packet[7] >> 4) & 0x07) * 10) + (packet[7] & 0x0F));
                        
                        //NSLog(@"Device Time: %i/%i/%i  %i:%i:%i", month, date, year, hour, minutes, seconds);
                        deviceInfo.year = year;
                        deviceInfo.month = month;
                        deviceInfo.day = date;
                        deviceInfo.hour = hour;
                        deviceInfo.minute = minutes;
                        deviceInfo.second = seconds;
                        // Ignore checksum and byte 8
                        
                        __weak DeviceViewController *weakSelf = self;
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [weakSelf updateDeviceInfo];
                        });
                    }
                    else if ((packet[1] & JAS_COMMAND_MASK) == JAS_SERIAL_PACKET) //change serial_packet value with what you want,  i used GET_SERIAL.
                    {
                        //NSLog(@"Decoding Serial Number Packet");
                        
                        //Get serial number here
                        // Get time
                        int product = (int)(packet[2] >> 4) * 10 + (packet[2] & 0x0F);
                        NSString *revision = [NSString stringWithFormat:@"%c", (int)packet[3]];
                        int uid1 = (int)(packet[4] >> 4) * 10 + (packet[4] & 0x0F);
                        int uid2 = (int)(packet[5] >> 4) * 10 + (packet[5] & 0x0F);
                        int left = (int)packet[6];
                        int swRev = (int)((packet[7] << 8) + packet[8]);
                        
                        // set firmware revision from device
                        deviceInfo.firmwareRevision = swRev;
                        
                        // NEED CODE HERE FOR ADDING L if left != 0
                        
                        NSString *serialNumber = [NSString stringWithFormat:@"%02i%@%02i%02i", product, revision, uid1, uid2];
                        if (left != 0){
                            serialNumber = [serialNumber stringByAppendingString:SERIAL_NUMBER_LEFT_SUFFIX];
                        }
                        //NSLog(@"Serial Number %@", serialNumber);
                        deviceInfo.serialNumber = serialNumber;
                        
                        __weak DeviceViewController *weakSelf = self;
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [weakSelf updateDeviceInfo];
                        });
                    }
                    else if ((packet[1] & JAS_COMMAND_MASK) == JAS_SN_SET_PACKET)
                    {
                        // YURIY - POP UP HERE FOR "Setup Complete"
                        // You should probably set up a timer when the command is sent and if you dont get a ACK in 5 seconds display "Command Timed Out" - this could be the default for all commands sent
                        _didRetriedRequest = NO;
                        
                        //TODO:- Justin - check it
                        //deviceInfo.sentACKsCount ++;
                        //[self updatePacketsInfo];
                        //-end
                        
                        [self setTime];
                    }
                    else if ((packet[1] & JAS_COMMAND_MASK) == JAS_SETUP_COMPLETE_RESPONSE)
                    {
                        // YURIY - POP UP HERE FOR "Setup Complete"
                        // You should probably set up a timer when the command is sent and if you dont get a ACK in 5 seconds display "Command Timed Out" - this could be the default for all commands sent
                        __weak DeviceViewController *weakSelf = self;
                        didReceivedSetTimeResponse = YES;
                        
                        //TODO:- Justin - check it
                        //deviceInfo.sentACKsCount ++;
                        //[self updatePacketsInfo];
                        //-end
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            weakSelf.loadingIndicator.visibility = NO;
                            [weakSelf showAlertWithTitle:@"Setup Monitor Completed!" message:nil actionTitle:@"Ok"];
                        });
                        
                    }
                    else if ((packet[1] & JAS_COMMAND_MASK) == JAS_CALIBRATE_CLOCK_RESPONSE)
                    {
                        NSLog(@"Calibrate Clock Sent!");
                        __weak DeviceViewController *weakSelf = self;
                        didReceivedSetTimeResponse = YES;
                        
                        //TODO:- Justin - check it
                        //deviceInfo.sentACKsCount ++;
                        //[self updatePacketsInfo];
                        //-end
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            weakSelf.loadingIndicator.visibility = NO;
                        });
                    }
                    else if ((packet[1] & JAS_COMMAND_MASK) == JAS_DISCONNECT_RESPONSE)
                    {
                        // THIS IS A SUCESSFUL DISCONNECT, I DONT THINK WE NEED A POPUP, JUST IGNORE
                    }
                    else if ((packet[1] & JAS_COMMAND_MASK) == JAS_COMMAND_REPONSE_FAILED)
                    {
                        // Received NACK, and Increase Dropped Packet Count
                        //TODO:- Justin - check it
                        deviceInfo.droppedPacketsCount++;
                        [self updatePacketsInfo];
                        //-end
                        return;
//                        [self showAlertWithErrorMessage:@"Command Failed!"];
                    }
                    else if ((packet[1] & JAS_COMMAND_MASK) == JAS_COMMAND_ACK)
                    {
                        // Increase Received Packet Count
                        //TODO:- Justin - check it
                        deviceInfo.receivedACKsCount++;
                        [self updatePacketsInfo];
                    }
                    
                    // Once firmware sends checksum, we will test currently not implemented
                    if (TRUE)
                    {
                        // Send ACK if checksum is valid
                        //uint8_t responsePacket[10] = {0xCC, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03};

                        //TODO:- Justin - check it
                        deviceInfo.sentACKsCount ++;
                        [self updatePacketsInfo];
                        //-end
                        
                        NSData *responseData = [NSString getDataFromHexString:JAS_COMMAND_ACK_REQUEST];
                        if (responseData != nil) {
                            [self sendTransparentData:responseData isACK:YES];
                        }
                    }
                    else
                    {
                        // send NACK if checksum is invalid
                        //                    uint8_t responsePacket[10] = {0xCC, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04};

                        NSData *responseData = [NSString getDataFromHexString:JAS_COMMAND_NACK_REQUEST];
                        if (responseData != nil) {
                            [self sendTransparentData:responseData isACK:YES];
                        }
                    }
                    
                }
                else
                {
                    NSLog(@"Missing Start Byte");
                }
            }
            else
            {
                NSLog(@"Invalid packet size: %lu", (unsigned long)str.length);
            }
        }
        
        
    }
}

- (void)MyPeripheral:(MyPeripheral *)peripheral didSendTransparentDataStatus:(NSError *)error {
    NSLog(@"DidSendTransparentDataStatus");
}

- (void)MyPeripheral:(MyPeripheral *)peripheral didUpdateTransDataNotifyStatus:(BOOL)notify{
    NSLog(@"DidUpdateTransDataNotifyStatus = %@",notify==true?@"true":@"false");
}

- (void)MyPeripheral:(MyPeripheral *)peripheral didUpdateHardwareRevision:(NSString *)hardwareRevision error:(NSError *)error {
    if (!error) {
        NSLog(@"Hardware = %@",hardwareRevision);
        
        _hardwareRevision = hardwareRevision;
        
        if ([hardwareRevision hasPrefix:[NSString stringFromHexString:BM77SPP_HW]]) {
            //BM77SPP
            [connectedPeripheral readFirmwareRevision];
        }
        else if ([hardwareRevision hasPrefix:[NSString stringFromHexString:BLETR_HW]]) {
            //BLETR
            [connectedPeripheral readFirmwareRevision];
        }
        else {
            isPatch = YES;
            [connectedPeripheral setTransDataNotification:TRUE];
        }
    }
    
}

- (void)MyPeripheral:(MyPeripheral *)peripheral didUpdateFirmwareRevision:(NSString *)firmwareRevision error:(NSError *)error {
    if (!error) {
        NSLog(@"firmwareRevision = %@",firmwareRevision);
        _firmwareRevision = firmwareRevision;
        if ([firmwareRevision hasPrefix:[NSString stringFromHexString:BM77SPP_FW]]) {
            //BM77SPP
            [connectedPeripheral readMemoryValue:BM77SPP_AD length:2];
        }
        else if ([firmwareRevision hasPrefix:[NSString stringFromHexString:BLETR_FW]]) {
            //BLETR
            [connectedPeripheral readMemoryValue:BLETR_AD length:2];
        }
        else {
            isPatch = YES;
            [connectedPeripheral setTransDataNotification:TRUE];
        }
    }
    
}

- (void)MyPeripheral:(MyPeripheral *)peripheral didReceiveMemoryAddress:(NSData *)address length:(short)length data:(NSData *)data {
    NSLog(@"%@ = %@",address,data);
    
    unsigned short int add;
    [address getBytes:&add length:2];
    add = NSSwapBigShortToHost(add);
    unsigned short int d;
    [data getBytes:&d length:length];
    d = NSSwapBigShortToHost(d);
    if ([_hardwareRevision hasPrefix:[NSString stringFromHexString:BM77SPP_HW]] && [_firmwareRevision hasPrefix:[NSString stringFromHexString:BM77SPP_FW]] && add == BM77SPP_AD) {
        if (d != 0x0017) {
            char w[2];
            w[0] = 0x00;
            w[1] = 0x17;
            [connectedPeripheral writeMemoryValue:BM77SPP_AD length:2 data:w];
        }
        else {
            isPatch = YES;
            [connectedPeripheral setTransDataNotification:TRUE];
        }
    }
    else if ([_hardwareRevision hasPrefix:[NSString stringFromHexString:BLETR_HW]] && [_firmwareRevision hasPrefix:[NSString stringFromHexString:BLETR_FW]] && add == BLETR_AD) {
        if (d != 0x17) {
            char w[2];
            w[0] = 0x00;
            w[1] = 0x17;
            [connectedPeripheral writeMemoryValue:BLETR_AD length:2 data:w];
        }
        else {
            isPatch = YES;
            [connectedPeripheral setTransDataNotification:TRUE];
        }
    }
}

- (void)MyPeripheral:(MyPeripheral *)peripheral didWriteMemoryAddress:(NSError *)error {
    if (!error) {
        isPatch = YES;
        [connectedPeripheral setTransDataNotification:TRUE];
    }
}

- (void)reliableBurstData:(ReliableBurstData *)reliableBurstData didSendDataWithCharacteristic:(CBCharacteristic *)transparentDataWriteChar {
    NSLog(@"reliableBurstData:didSendDataWithCharacteristic:");
}

- (void)popToRootPage {
    [self.navigationController popViewControllerAnimated:true];
}

#pragma Mark - CBControllerDelegate
- (void)CBController:(CBController *)cbController didDisconnectedPeripheral:(MyPeripheral *)myPeripheral {
    NSLog(@"didDisconnectedPeripheral");   
}

@end
