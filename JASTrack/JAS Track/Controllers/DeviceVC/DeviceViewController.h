//
//  DeviceViewController.h
//  JAS Track
//
//  Created by Yuriy on 2/15/19.
//  Copyright © 2019 Yuriy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CBController.h"
#import "DeviceInfo.h"

NS_ASSUME_NONNULL_BEGIN

typedef struct _RX_DATA_TIME {

    NSDate *sinceDate;    NSTimeInterval transmit_time;
    int trail_time;
}RX_DATA_TIME;


@interface DeviceViewController : BaseViewController
@property (nonatomic, retain)MyPeripheral *connectedPeripheral;
@property (nonatomic, retain) CBController *cbController;

@end

NS_ASSUME_NONNULL_END
