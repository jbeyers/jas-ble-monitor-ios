//
//  BaseViewController.h
//  JAS Track
//
//  Created by Yuriy on 2/13/19.
//  Copyright © 2019 Yuriy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActivityIndicatorHelper.h"

@interface BaseViewController : UIViewController

@property (nonatomic, readonly) ActivityIndicatorHelper* _Nonnull loadingIndicator;
- (UIView * _Nullable)getBottomView;
- (CGFloat)getPaddingBetweenBottomViewAndKeyboard;
- (void)runInMainThread:(dispatch_block_t) block;
- (void)keyboardWillBeShown:(nonnull NSNotification *)notification;
- (void)keyboardWillBeHidden:(nonnull NSNotification *)notification;
- (void)updateUI;
- (void)setButton:(nonnull UIButton *)button enabled:(BOOL)enabled;

- (void)showAlertWithError:(nonnull NSError *)error;
- (void)showAlertWithErrorMessage:(nonnull NSString *)error;
- (void)showAlertWithTitle:(nullable NSString *)title message:(nullable NSString *)message actionTitle:(nullable NSString *)actionTitle;
- (void)showAlertWithoutAction:(nullable NSString *)title message:(nullable NSString *)message;
- (void)showNoInternetConnectionAlert;
- (void)showNoInternetConnectionAlertWith:(void (^ __nonnull)(UIAlertAction* _Nonnull action))actionHandler;
- (void)showAlertWithAction:(nullable NSString *)title message:(nullable NSString *)message actionHandler: (void (^ __nonnull)(UIAlertAction* _Nonnull action))actionHandler;
@end
