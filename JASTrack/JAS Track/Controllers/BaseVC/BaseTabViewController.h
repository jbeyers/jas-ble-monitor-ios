//
//  BaseTabViewController.h
//  JAS Track
//
//  Created by Yuriy on 2/23/19.
//  Copyright © 2019 Yuriy. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseTabViewController : UITabBarController <UITabBarControllerDelegate>

@end

NS_ASSUME_NONNULL_END
