//
//  BaseTabViewController.m
//  JAS Track
//
//  Created by Yuriy on 2/23/19.
//  Copyright © 2019 Yuriy. All rights reserved.
//

#import "BaseTabViewController.h"
#import "HomeViewController.h"

@interface BaseTabViewController ()

@end

@implementation BaseTabViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.delegate = self;
    
    [UITabBarItem.appearance setTitleTextAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:17]
                                                      } forState:UIControlStateNormal];
    for(UITabBarItem * tabBarItem in self.tabBar.items){
        //tabBarItem.title = @"";
        tabBarItem.image = nil;
        
    }
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
    NSInteger selectedIndex = tabBarController.selectedIndex;
    
    if (selectedIndex == 0) {
        UINavigationController *nav = (UINavigationController *)viewController;
        if ([nav.navigationItem.title isEqualToString:@"HOME"]) {
            return NO;
        }
    }
    
    return YES;
}

@end
