//
//  SupportViewController.m
//  JAS Track
//
//  Created by crane on 7/1/19.
//  Copyright © 2019 Yuriy. All rights reserved.
//

#import "SupportViewController.h"
#import "UUID.h"
#import <MessageUI/MessageUI.h>

@interface SupportViewController () <UITableViewDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, retain) NSMutableArray *contactMA;
@end

@implementation SupportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    _contactMA = [[NSMutableArray alloc] initWithObjects:CONTACT_INFO_ADDRESS_BR, CONTACT_INFO_PHONE_NUMBER, CONTACT_INFO_EMAIL, nil];
}

- (void)updateUI {
    self.view.backgroundColor = UIColor.whiteColor;
    if (@available(iOS 12.0, *)) {
       if (self.traitCollection.userInterfaceStyle == UIUserInterfaceStyleDark) { //Dark Mode
           self.view.backgroundColor = UIColor.blackColor;
       }
    }
}

#pragma Mark - UITableViewDelegate, UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _contactMA.count;
}


- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    NSAttributedString *attStr = [[NSAttributedString alloc] initWithString:[_contactMA objectAtIndex:indexPath.row]];
    cell.textLabel.attributedText = attStr;
    cell.textLabel.numberOfLines = 0;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == 1) { // phone call
        [self callPhone];
    } else if (indexPath.row == 2) { //email
        [self _contactUs];
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    NSMutableString *helpTip = [NSMutableString new];
    [helpTip appendString:@"\n\n"];
    
    NSString *focusListTitle = FCC_ID;
    [helpTip appendFormat:@"%@\n", [focusListTitle uppercaseStringWithLocale:[NSLocale currentLocale]]];
    
    [helpTip appendString:SUPPORT_INFO];
    return helpTip;
    
}

- (void)callPhone {
    NSString *phoneNumber = [@"tel://" stringByAppendingString:CONTACT_INFO_PHONE_NUMBER];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}

#pragma Mark - Email Configuration
- (void)_contactUs {
    if ([MFMailComposeViewController canSendMail] == NO)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Email Unavailable"
                                                                       message:@"Please configure your email account and try again."
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * _Nonnull action) {
                                                         }];
        [alert addAction:okButton];
        [self presentViewController:alert animated:YES completion:nil];
        
        return;
    }
    
    
    MFMailComposeViewController *mailController = [[MFMailComposeViewController alloc] init];
    [mailController setToRecipients:@[CONTACT_INFO_EMAIL]];
    [mailController setSubject:@"Contact US"];
    [mailController setMessageBody:@"" isHTML:YES];
    
    mailController.mailComposeDelegate = self;
    
    //#ifdef IPAD
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        mailController.modalPresentationStyle = UIModalPresentationFormSheet;
    }
    
    [self presentViewController:mailController animated:YES completion:nil];
}

#pragma mark MFMailComposeViewControllerDelegate Methods
- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error
{
    [controller dismissViewControllerAnimated:YES completion:nil];
}

@end
