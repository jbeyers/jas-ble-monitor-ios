//
//  UsageDataViewController.m
//  JAS Track
//
//  Created by Yuriy on 2/18/19.
//  Copyright © 2019 Yuriy. All rights reserved.
//

#import "UsageDataViewController.h"
#import <UIKit/UIKit.h>
#import <ZMJGanttChart/ZMJGanttChart.h>
#import "SessionCell.h"
#import "UsageData.h"
#import "NSData+Conversion.h"
#import "NSString+Extension.h"
#import <CoreText/CoreText.h>
#import <QuartzCore/QuartzCore.h>
#import "CommonHelper.h"
#define PDF_LEFT_MARGIN 20
#define PDF_TOP_MARGIN  60
#define COLUMN_COUNT     5
#define PDF_WIDTH       612
#define PDF_HEIGHT      792
#define PDF_SPACING     8

@import Charts;

@interface UsageDataViewController () <SpreadsheetViewDelegate, SpreadsheetViewDataSource, ChartViewDelegate, MyPeripheralDelegate, ReliableBurstDataDelegate, CBControllerDelegate>
@property (weak, nonatomic) IBOutlet BarChartView *chartView;
@property (weak, nonatomic) IBOutlet BarChartView *groupBarChartView;
@property (weak, nonatomic) IBOutlet UILabel *totalMinutesLabel;

@property (weak, nonatomic) IBOutlet UIView *romContainer;
@property (weak, nonatomic) IBOutlet UILabel *productLabel;
@property (weak, nonatomic) IBOutlet UILabel *snLabel;
@property (weak, nonatomic) IBOutlet UIView *usageContainer;

@property (weak, nonatomic) IBOutlet UIView *spreadSheetContentView;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIButton *btnDisconnect;
@property (weak, nonatomic) IBOutlet UIButton *btnClear;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;

@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;

@property (nonatomic, strong) SpreadsheetView *spreadsheetView;
@property (nonatomic, retain) NSMutableArray *itemsMA;
@property (nonatomic, retain) ChartYAxis *leftAxis;

@property CGSize currentSize;
@end

@implementation UsageDataViewController {
    BOOL didReceivedClearDataResponse;
    BOOL didCalledDoubleTimes;
    NSTimer *dataTimer;
    CGFloat currentPDFContentHeight;
    NSInteger currentPageNumber;
    NSMutableArray *headersMA;
}

@synthesize itemsMA;
@synthesize connectedPeripheral;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden = YES;

    itemsMA = [NSMutableArray new];
    
    _currentSize = self.view.bounds.size;

    self.spreadsheetView.delegate = self;
    self.spreadsheetView.dataSource = self;

    _spreadSheetContentView.layer.borderWidth = 1.0f;
    _spreadSheetContentView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    [self.spreadsheetView registerClass:[SessionCell class]   forCellWithReuseIdentifier:@"SessionCell"];
    [self.spreadsheetView registerClass:[SessionHeaderCell class]   forCellWithReuseIdentifier:@"SessionHeaderCell"];
    
    [self configureSerialNumber];

//    [self buildChartView];
    [self buildGroupChartView];
    [self updateData];
    
    headersMA = [self buildCellItems];
}

- (void)updateUI {
    self.view.backgroundColor = UIColor.whiteColor;
    if (@available(iOS 12.0, *)) {
       if (self.traitCollection.userInterfaceStyle == UIUserInterfaceStyleDark) { //Dark Mode
           self.view.backgroundColor = UIColor.blackColor;
           for (UIButton *btn in @[_btnDisconnect, _btnClear, _btnSave]) {
               btn.backgroundColor = UIColor.whiteColor;
               [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
           }
           UIImage *image = [[UIImage imageNamed:@"ic-back"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
           [_btnBack setImage:image forState:UIControlStateNormal];
           _btnBack.tintColor = [UIColor whiteColor];
           
           _logoImageView.image = [UIImage imageNamed:@"dark_logo.png"];
       } else {
           self.view.backgroundColor = UIColor.whiteColor;
           for (UIButton *btn in @[_btnDisconnect, _btnClear, _btnSave]) {
               btn.backgroundColor = UIColor.blackColor;
               [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
           }
           UIImage *image = [[UIImage imageNamed:@"ic-back"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
           [_btnBack setImage:image forState:UIControlStateNormal];
           _btnBack.tintColor = [UIColor blackColor];
           
           _logoImageView.image = [UIImage imageNamed:@"v_logo.png"];
       }
    }
    
    if (@available(iOS 12.0, *)) {
       if (self.traitCollection.userInterfaceStyle == UIUserInterfaceStyleDark) { //Dark Mode
           _spreadSheetContentView.layer.borderColor = [UIColor darkGrayColor].CGColor;
       } else {
           _spreadSheetContentView.layer.borderColor = [UIColor lightGrayColor].CGColor;
       }
    } else {
        _spreadSheetContentView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    }
    
    [self updateData];
}

- (void)configureSerialNumber {
    if (_deviceInfo.serialNumber == nil) {
        _productLabel.text = @"Product: ";
        _snLabel.text = @"Serial Number: ";
    } else {
        _productLabel.text = [NSString stringWithFormat:@"Product: %@", [_deviceInfo getProduct]];
        _snLabel.text = [NSString stringWithFormat:@"Serial Number: %@", _deviceInfo.serialNumber];
    }
}

- (void)useDummyData {
    [itemsMA removeAllObjects];
    
    UsageData *data = [UsageData new];
    data.month = 6;
    data.day = 1;
    data.rotation1 = 40;
    data.rotation2 = 88;
    data.rom = 12;
    [itemsMA addObject:data];
    
    data = [UsageData new];
    data.month = 6;
    data.day = 2;
    data.rotation1 = 49;
    data.rotation2 = 18;
    data.rom = 22;
    [itemsMA addObject:data];
    
    data = [UsageData new];
    data.month = 6;
    data.day = 3;
    data.rotation1 = 2;
    data.rotation2 = 68;
    data.rom = 90;
    [itemsMA addObject:data];
    
    data = [UsageData new];
    data.month = 6;
    data.day = 4;
    data.rotation1 = 120;
    data.rotation2 = 68;
    data.rom = 23;
    [itemsMA addObject:data];
    
    data = [UsageData new];
    data.month = 6;
    data.day = 5;
    data.rotation1 = 90;
    data.rotation2 = 61;
    data.rom = 67;
    [itemsMA addObject:data];
    
    data = [UsageData new];
    data.month = 6;
    data.day = 6;
    data.rotation1 = 100;
    data.rotation2 = 21;
    data.rom = 11;
    [itemsMA addObject:data];
    
    data = [UsageData new];
    data.month = 6;
    data.day = 7;
    data.rotation1 = 88;
    data.rotation2 = 65;
    data.rom = 56;
    [itemsMA addObject:data];
    
    data = [UsageData new];
    data.month = 6;
    data.day = 6;
    data.rotation1 = 30;
    data.rotation2 = 5;
    data.rom = 90;
    [itemsMA addObject:data];
    
    data = [UsageData new];
    data.month = 6;
    data.day = 8;
    data.rotation1 = 100;
    data.rotation2 = 21;
    data.rom = 100;
    [itemsMA addObject:data];
    
    data = [UsageData new];
    data.month = 6;
    data.day = 9;
    data.rotation1 = 150;
    data.rotation2 = 80;
    data.rom = 2;
    [itemsMA addObject:data];
    
    data = [UsageData new];
    data.month = 6;
    data.day = 10;
    data.rotation1 = 50;
    data.rotation2 = 51;
    data.rom = 87;
    [itemsMA addObject:data];
    
    [self updateData];
}

- (void)viewDidAppear:(BOOL)animated {
    _cbController.delegate = self;
    
    connectedPeripheral.deviceInfoDelegate = self;
    connectedPeripheral.proprietaryDelegate = self;
    connectedPeripheral.transDataDelegate = self;
    
    [connectedPeripheral setTransDataNotification:TRUE];
    
    connectedPeripheral.transmit.delegate = self;
 
#if MOCK
    [self useDummyData];
#else
    didCalledDoubleTimes = NO;
    [self sendGetUsageDataPacket];
#endif
    
}

- (void)sendGetUsageDataPacket {
    __weak UsageDataViewController *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.loadingIndicator setLoadingText:@"Getting usage data..."];
        weakSelf.loadingIndicator.visibility = YES;
    });
    
    [itemsMA removeAllObjects];
    
    NSData *responseData = [NSString getDataFromHexString:JAS_COMMAND_GET_DATA_REQUEST];
    if (responseData != nil) {
        [self sendTransparentData:responseData];
    }
    
    didReceivedClearDataResponse = NO;
    dataTimer = [NSTimer scheduledTimerWithTimeInterval:JAS_WAITING_TIME_WITH_RETRY target:self selector:@selector(timeoutGetData) userInfo:nil repeats:NO];
}

- (void)timeoutGetData {
    if (!didReceivedClearDataResponse) {
        if (!didCalledDoubleTimes) {
            didCalledDoubleTimes = YES;
            
            [self sendGetUsageDataPacket];
            return;
        }
        
        //time out
        self.loadingIndicator.visibility = NO;
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Session Time Out" message:nil
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            didCalledDoubleTimes = NO;
            
            [self sendGetUsageDataPacket];
        }];
        [alert addAction:action];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                             handler:^(UIAlertAction *action) {
                                                                 [self.navigationController popViewControllerAnimated:YES];
                                                             }];
        [alert addAction:cancelAction];
        
        [self runInMainThread:^{
            [self presentViewController:alert animated:YES completion:nil];
        }];
    }
    
    [dataTimer invalidate];
    dataTimer = nil;
}

- (void)sendTransparentData:(NSData *)data {
    [connectedPeripheral sendTransparentData:data type:CBCharacteristicWriteWithResponse];
}

#pragma Mark - MyPeripheralDelegate
- (void)MyPeripheral:(MyPeripheral *)peripheral didReceiveTransparentData:(NSData *)data {
    
    if ([data length] > 0) {
        NSString *str = [data hexadecimalString];
        NSLog(@"didReceiveTransparentData  %@", str);
        // Make sure that the packet is the correct size
        // SHOULD WE MAKE 10 and 20 #defines or globals?
        if (str.length == JAS_PACKET_STRING_LENGTH)
        {
            const char *chars = [str UTF8String];
            unsigned packet[JAS_PACKET_LENGTH] = {};
            int i = 0;
            int j = 0;
            
            char byteChars[4] = {'\0','\0','\0'};
            
            // JEB - populate an array for easier manipulation
            while (i < JAS_PACKET_STRING_LENGTH) {
                byteChars[0] = chars[i++];
                byteChars[1] = chars[i++];
                packet[j++] = (unsigned int) strtoul(byteChars, NULL, 16);
            }
            
            if (packet[0] == JAS_COMMAND_PACKET_FORMAT) {
                NSLog(@"Start Byte Found");
                // Check packet type
                if ((packet[1] & JAS_COMMAND_MASK) == JAS_DATA_PACKET)
                {
                    NSLog(@"Decoding Data Packet");
                    
                    //Create UsageData
                    UsageData *uData = [[UsageData alloc] init];
                    
                    // Ignore extra data in command byte
                    
                    // Get Month & Date
                    int month = (int)((((packet[2] >> 4) & 0x01) * 10) + (packet[2] & 0x0F));
                    int date = (int)((((packet[3] >> 4) & 0x03) * 10) + (packet[3] & 0x0F));
                    NSLog(@"Record Date: %i/%i", month, date);
                    
                    uData.month = month;
                    uData.day = date;
                    
                    // Get rotation1 - it is likley same with clockwise minutes
                    int rotation1 = (int)packet[4];
                    NSLog(@"clockwise minutes: %i", rotation1);
                    uData.rotation1 = rotation1;
                    
                    // Get rotation2 - it is likley same with counter clockwise minutes
                    int rotation2 = (int)packet[5];
                    NSLog(@"counter clockwise minutes: %i", rotation2);
                    uData.rotation2 = rotation2;
                    
                    // Get range of motion
                    int rom = (int)((packet[6] << 8) + packet[7]);
                    NSLog(@"range of motion: %i", rom);
                    uData.rom = rom;
                    
                    if (itemsMA == nil) {
                        itemsMA = [NSMutableArray new];
                    }
                    
                    [itemsMA addObject:uData];
                    
                    didReceivedClearDataResponse = YES;
                    __weak UsageDataViewController *weakSelf = self;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        weakSelf.loadingIndicator.visibility = NO;
                        [weakSelf updateData];
                    });
                }
                else if ((packet[1] & JAS_COMMAND_MASK) == JAS_CLEAR_DATA_RESPONSE)
                {
                    // YURIY - POP UP HERE FOR "Data Cleared"
                    NSLog(@"Data Cleared");
                    didReceivedClearDataResponse = YES;
                    __weak UsageDataViewController *weakSelf = self;
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        weakSelf.loadingIndicator.visibility = NO;
                        [weakSelf showAlertWithTitle:@"Data Cleared Successfully" message:nil actionTitle:@"Ok"];
                    });
                }
                else if ((packet[1] & JAS_COMMAND_MASK) == JAS_DISCONNECT_RESPONSE)
                {
                    // THIS IS A SUCESSFUL DISCONNECT, I DONT THINK WE NEED A POPUP, JUST IGNORE
                }
                else if ((packet[1] & JAS_COMMAND_MASK) == JAS_COMMAND_REPONSE_FAILED)
                {
                    [self showAlertWithErrorMessage:@"Command Failed!"];
                }
                
                // Once firmware sends checksum, we will test currently not implemented
                if (TRUE)
                {
                    // Send ACK if checksum is valid
                    
                    NSData *responseData = [NSString getDataFromHexString:JAS_COMMAND_ACK_REQUEST];
                    if (responseData != nil) {
                        [self sendTransparentData:responseData];
                    }
                }
                else
                {
                    // send NACK if checksum is invalid
                   
                    NSData *responseData = [NSString getDataFromHexString:JAS_COMMAND_NACK_REQUEST];
                    if (responseData != nil) {
                        [self sendTransparentData:responseData];
                    }
                }
                
            }
            else
            {
                NSLog(@"Missing Start Byte");
            }
        }
        else
        {
            NSLog(@"Invalid packet size: %lu", (unsigned long)str.length);
        }
    }
}

- (void)MyPeripheral:(MyPeripheral *)peripheral didSendTransparentDataStatus:(NSError *)error {
    NSLog(@"DidSendTransparentDataStatus");
}

- (void)MyPeripheral:(MyPeripheral *)peripheral didUpdateTransDataNotifyStatus:(BOOL)notify{
    NSLog(@"DidUpdateTransDataNotifyStatus = %@",notify==true?@"true":@"false");
}

- (void)popToRootPage {
    [self.navigationController popToRootViewControllerAnimated:true];
}

#pragma Mark - CBControllerDelegate
- (void)CBController:(CBController *)cbController didDisconnectedPeripheral:(MyPeripheral *)myPeripheral {
    NSLog(@"didDisconnectedPeripheral");
}

#pragma Mark - Define Actions
- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)disconnectAction:(id)sender {
    NSData *responseData = [NSString getDataFromHexString:JAS_COMMAND_DISCONNECT_REQUEST];
    if (responseData != nil) {
        [self sendTransparentData:responseData];
    }
    
    [_cbController disconnectDevice:connectedPeripheral];
    [self performSelector:@selector(popToRootPage) withObject:nil afterDelay:0.5];
}

- (IBAction)clearDataAction:(id)sender {
    [self showAlertWithAction:@"Clear Data" message:@"Are you sure you want to clear data?" actionHandler:^(UIAlertAction * _Nonnull action) {
        [self clearData];
    }];
}

- (void)clearData {
    NSData *responseData = [NSString getDataFromHexString:JAS_COMMAND_CLEAR_DATA_REQUEST];
    if (responseData != nil) {
        [self sendTransparentData:responseData];
    }
    
    __weak UsageDataViewController *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.loadingIndicator setLoadingText:@"Setting up device..."];
        weakSelf.loadingIndicator.visibility = YES;
    });
    
    didReceivedClearDataResponse = NO;
    dataTimer = [NSTimer scheduledTimerWithTimeInterval:JAS_WAITING_FOR_RESPONSE_TIME target:self selector:@selector(timeoutSetTime) userInfo:nil repeats:NO];
}

- (void)timeoutSetTime {
    if (!didReceivedClearDataResponse) {
        //time out
        self.loadingIndicator.visibility = NO;
        [self showAlertWithErrorMessage:@"Session Time Out"];
    }
    
    [dataTimer invalidate];
    dataTimer = nil;
}


- (IBAction)saveDataAction:(id)sender {
    if (itemsMA.count == 0){
        return;
    }
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle: UIAlertControllerStyleActionSheet];
    UIAlertAction *actionCsv = [UIAlertAction actionWithTitle:@"Create a CSV" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self createCSV];
    }];
    [actionSheet addAction:actionCsv];
    
    UIAlertAction *actionPdf = [UIAlertAction actionWithTitle:@"Create a PDF" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self createPdf];
    }];
    [actionSheet addAction:actionPdf];
    
    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Cancel"style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [actionSheet addAction:actionCancel];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        [actionSheet setModalPresentationStyle:UIModalPresentationPopover];
        
        UIPopoverPresentationController *popPresenter = [actionSheet
                                                         popoverPresentationController];
        
        UIButton *btn = (UIButton *)sender;
        popPresenter.sourceView = btn;
        popPresenter.sourceRect = btn.bounds;
        popPresenter.permittedArrowDirections = 0;
    }
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

-(void)createCSV
{
    NSMutableString *csvString = [[NSMutableString alloc]initWithCapacity:0];
    
    NSString *product = [self.deviceInfo getProduct];
    NSString *sn = self.deviceInfo.serialNumber;
    
    [csvString appendString:@"PRODUCT"];
    [csvString appendString:@", "];
    [csvString appendString:product];
    [csvString appendString:@", "];
    [csvString appendString:@"\n"];
    [csvString appendString:@"SERIAL NUMBER"];
    [csvString appendString:@", "];
    [csvString appendString:sn];
    [csvString appendString:@"\n"];
    
    [csvString appendString:@"Treatment Dates"];
    [csvString appendString:@", "];
    UsageData *fData = itemsMA.firstObject;
    UsageData *lData = itemsMA.lastObject;
    if (fData != nil) {
        [csvString appendString:[NSString stringWithFormat:@"%d/%d", fData.month, fData.day]];
    }
    [csvString appendString:@", "];
    if (lData != nil) {
        [csvString appendString:[NSString stringWithFormat:@"%d/%d", lData.month, lData.day]];
    }
    
    [csvString appendString:@"\n"];
    [csvString appendString:@", "];
    [csvString appendString:@", "];
    
    [csvString appendString:@"\n"];
    
    for (int i = 0; i < headersMA.count; i++) {
        NSString *header = [headersMA objectAtIndex:i];
        header = [header stringByReplacingOccurrencesOfString:@"\n"
        withString:@" "];
        if (![csvString isEqualToString:@""] && i > 0) {
            [csvString appendString:@", "];
        }
        [csvString appendString:header];
    }
    [csvString appendString:@"\n"];
    
    for (UsageData *data in itemsMA) {
        [csvString appendString:[NSString stringWithFormat:@"%02d/%02d, %d, %d, %d, %0.02f\n",data.month, data.day, data.rotation1, data.rotation2, (data.rotation1 + data.rotation2), data.rom * [self.deviceInfo getGearingRatio]]];
    }

    NSDate *date = [NSDate date];
    long ti = (long)[date timeIntervalSince1970];
    NSString *fileName = [NSString stringWithFormat:@"%@%@_%ld.csv", FILE_NAME_PREFIX, self.deviceInfo.serialNumber, ti];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, fileName];
    if ( [csvString writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:nil] ) {
        [self showAlertWithTitle:@"CSV Saved" message:@"Saved files are available in My Files." actionTitle:@"OK"];
    }
}


- (void)createPdf {
    currentPageNumber = 0;
    
    //Get Creating Date
    NSDate *date = [NSDate date];
    long ti = (long)[date timeIntervalSince1970];
    NSString *fileName = [NSString stringWithFormat:@"%@%@_%ld.pdf", FILE_NAME_PREFIX, self.deviceInfo.serialNumber, ti];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, fileName];
    
    // Create the PDF context using the default page size of 612 x 792.
    UIGraphicsBeginPDFContextToFile(filePath, CGRectZero, nil);
    // Mark the beginning of a new page.
    UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, PDF_WIDTH, PDF_HEIGHT), nil);
    [self drawPageNumber:++currentPageNumber];
    currentPDFContentHeight = 0;
    //Draw Header
    [self drawHeader: date];
    
//     [self drawChart];
    [self drawGroupChart];
    //Draw spread sheet
    int yOrigin = currentPDFContentHeight + 20;
    
    int rowHeight = 40;
    int columnWidth = (612 - PDF_LEFT_MARGIN * 2) / COLUMN_COUNT;
    
    int spaceHeight = PDF_HEIGHT - yOrigin - PDF_TOP_MARGIN;
    int count = spaceHeight / rowHeight;
    
    if (count == 1) {
        [self drawTableView:count - 1 columnWidth:columnWidth rowHeight:rowHeight];
    } else if (count < itemsMA.count + 1) {
        int numberOfRows = count;
        int numberOfColumns = COLUMN_COUNT;
        
        [self drawTableAt:CGPointMake(PDF_LEFT_MARGIN, yOrigin) withRowHeight:rowHeight andColumnWidth:columnWidth andRowCount:numberOfRows andColumnCount:numberOfColumns];
        
        [self drawTableDataAt:CGPointMake(PDF_LEFT_MARGIN, yOrigin) withRowHeight:rowHeight andColumnWidth:columnWidth andRowCount:numberOfRows andColumnCount:numberOfColumns fromIndex:0 toIndex:count - 1];
        
        [self drawTableView:count - 1 columnWidth:columnWidth rowHeight:rowHeight];
    } else {
        int numberOfRows = (int)(itemsMA.count) + 1;
        int numberOfColumns = COLUMN_COUNT;
        
        [self drawTableAt:CGPointMake(PDF_LEFT_MARGIN, yOrigin) withRowHeight:rowHeight andColumnWidth:columnWidth andRowCount:numberOfRows andColumnCount:numberOfColumns];
        
        [self drawTableDataAt:CGPointMake(PDF_LEFT_MARGIN, yOrigin) withRowHeight:rowHeight andColumnWidth:columnWidth andRowCount:numberOfRows andColumnCount:numberOfColumns fromIndex:0 toIndex:(int)itemsMA.count];
    }
   
    // Close the PDF context and write out the contents.
    UIGraphicsEndPDFContext();
    
    [self showAlertWithTitle:@"PDF Saved" message:@"Saved files are available in My Files." actionTitle:@"OK"];
}

- (void)drawHeader: (NSDate *)date {
    //Draw JAS Logo
    UIImage *logoImg = [UIImage imageNamed:@"v_logo.png"];
    int logoHeight = 140;
    [self drawImage:logoImg rect:CGRectMake(PDF_LEFT_MARGIN, PDF_TOP_MARGIN, logoHeight * 415 / 306, logoHeight)];
    currentPDFContentHeight = PDF_TOP_MARGIN + logoHeight + 50;
    
    //Draw Created Date
    NSString *dateStr = [CommonHelper stringWithDate:date];
    CGSize dateSize = [CommonHelper sizeWithString:dateStr withFont:[UIFont systemFontOfSize:14]];
     [self drawText:dateStr inFrame:CGRectMake(PDF_WIDTH - dateSize.width, 30, dateSize.width, dateSize.height)];
    
    //Draw Contact Info
    CGFloat infoY = PDF_TOP_MARGIN + 60;
    CGFloat infoLeadingPosition = PDF_LEFT_MARGIN + logoHeight * 415 / 306 + 40;
    CGFloat infoWidth = PDF_WIDTH - PDF_LEFT_MARGIN - infoLeadingPosition;
    
    [self drawText:CONTACT_INFO_ADDRESS inFrame:CGRectMake(infoLeadingPosition, infoY, infoWidth / 3 + 20, 50)];
    infoY += 20;
    [self drawText:CONTACT_INFO_PHONE_NUMBER inFrame:CGRectMake(infoLeadingPosition, infoY, infoWidth, 20)];
    infoY += 20;
    [self drawText:CONTACT_INFO_EMAIL inFrame:CGRectMake(infoLeadingPosition, infoY, infoWidth, 20)];
    
    //Draw Product, SerialNumber, Total usages
    
    [self drawText:self.productLabel.text inFrame:CGRectMake(PDF_LEFT_MARGIN, currentPDFContentHeight, self.productLabel.bounds.size.width, self.productLabel.bounds.size.height)];
    currentPDFContentHeight += self.productLabel.bounds.size.height + PDF_SPACING;
    
    [self drawText:self.snLabel.text inFrame:CGRectMake(PDF_LEFT_MARGIN, currentPDFContentHeight, self.snLabel.bounds.size.width, self.snLabel.bounds.size.height)];
    currentPDFContentHeight += self.snLabel.bounds.size.height + PDF_SPACING;
    
    [self drawText:self.totalMinutesLabel.text inFrame:CGRectMake(PDF_LEFT_MARGIN, currentPDFContentHeight, self.totalMinutesLabel.bounds.size.width, self.totalMinutesLabel.bounds.size.height)];
    currentPDFContentHeight += self.totalMinutesLabel.bounds.size.height + PDF_SPACING;

    NSMutableString *dateRange = [[NSMutableString alloc]initWithCapacity:0];
    [dateRange appendString:@"Treatment Dates: "];

    UsageData *fData = itemsMA.firstObject;
    UsageData *lData = itemsMA.lastObject;
    if (fData != nil) {
        [dateRange appendString:[NSString stringWithFormat:@"%d/%d", fData.month, fData.day]];
    }
    [dateRange appendString:@" - "];
    if (lData != nil) {
        [dateRange appendString:[NSString stringWithFormat:@"%d/%d", lData.month, lData.day]];
    }
    
    [self drawText:dateRange inFrame:CGRectMake(PDF_LEFT_MARGIN, currentPDFContentHeight, 200, self.totalMinutesLabel.bounds.size.height)];
    currentPDFContentHeight += self.totalMinutesLabel.bounds.size.height + PDF_SPACING;
}

- (void)drawTableView: (int)currentIndex columnWidth:(int)columnWidth rowHeight:(int)rowHeight {

    // Mark the beginning of a new page.
    UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, PDF_WIDTH, PDF_HEIGHT), nil);
    [self drawPageNumber:++currentPageNumber];
    currentPDFContentHeight = 0;
    
    int spaceHeight = PDF_HEIGHT - 2 * PDF_TOP_MARGIN;
    int count = spaceHeight / rowHeight;
    int numberOfColumns = COLUMN_COUNT;
    if (count < (itemsMA.count - currentIndex + 1)) {
        int numberOfRows = count;
        
        [self drawTableAt:CGPointMake(PDF_LEFT_MARGIN, PDF_TOP_MARGIN) withRowHeight:rowHeight andColumnWidth:columnWidth andRowCount:numberOfRows andColumnCount:numberOfColumns];
        
        [self drawTableDataAt:CGPointMake(PDF_LEFT_MARGIN, PDF_TOP_MARGIN) withRowHeight:rowHeight andColumnWidth:columnWidth andRowCount:numberOfRows andColumnCount:numberOfColumns fromIndex:currentIndex toIndex:currentIndex + count - 1];
        
        [self drawTableView:currentIndex + count - 1 columnWidth:columnWidth rowHeight:rowHeight];
    } else {
        int numberOfRows = (int)(itemsMA.count) - currentIndex + 1;
        
        [self drawTableAt:CGPointMake(PDF_LEFT_MARGIN, PDF_TOP_MARGIN) withRowHeight:rowHeight andColumnWidth:columnWidth andRowCount:numberOfRows andColumnCount:numberOfColumns];
        
        [self drawTableDataAt:CGPointMake(PDF_LEFT_MARGIN, PDF_TOP_MARGIN) withRowHeight:rowHeight andColumnWidth:columnWidth andRowCount:numberOfRows andColumnCount:numberOfColumns fromIndex:currentIndex toIndex:(int)itemsMA.count];
    }
}

- (void)drawChart {
    int yOrigin = currentPDFContentHeight;
    
    if (currentPDFContentHeight > PDF_HEIGHT) {
        currentPDFContentHeight = PDF_TOP_MARGIN;
        UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, PDF_WIDTH, PDF_HEIGHT), nil);
        [self drawPageNumber:++currentPageNumber];
        yOrigin = PDF_TOP_MARGIN;
    } else {
        currentPDFContentHeight += 20;
        int spaceHeight = PDF_HEIGHT - currentPDFContentHeight - PDF_TOP_MARGIN;
        if (spaceHeight < self.romContainer.bounds.size.height) {
            UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, PDF_WIDTH, PDF_HEIGHT), nil);
            [self drawPageNumber:++currentPageNumber];
            yOrigin = PDF_TOP_MARGIN;
        }
    }
    
    //Daily Rom
    CGFloat width = PDF_WIDTH - 2 * PDF_LEFT_MARGIN;
    CGFloat height = width * self.romContainer.bounds.size.height / self.romContainer.bounds.size.width;
    
    [self drawView:self.romContainer rect:CGRectMake(PDF_LEFT_MARGIN, yOrigin, width, height)];
    currentPDFContentHeight += height;
}

- (void)drawGroupChart {
    int yOrigin = currentPDFContentHeight;
    //Daily Rom
    CGFloat width = PDF_WIDTH - 2 * PDF_LEFT_MARGIN;
    //CC/CCW
    CGFloat height = width * self.usageContainer.bounds.size.height / self.usageContainer.bounds.size.width;
    
    if ((currentPDFContentHeight + height + PDF_SPACING) > PDF_HEIGHT) {
        currentPDFContentHeight = PDF_TOP_MARGIN;
        UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, PDF_WIDTH, PDF_HEIGHT), nil);
        [self drawPageNumber:++currentPageNumber];
        yOrigin = PDF_TOP_MARGIN;
    } else {
        currentPDFContentHeight += 20;
        int spaceHeight = PDF_HEIGHT - currentPDFContentHeight - PDF_TOP_MARGIN;
        if (spaceHeight < self.usageContainer.bounds.size.height) {
            UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, PDF_WIDTH, PDF_HEIGHT), nil);
            [self drawPageNumber:++currentPageNumber];
            yOrigin = PDF_TOP_MARGIN;
        }
    }
    
    [self drawView:self.usageContainer rect:CGRectMake(PDF_LEFT_MARGIN, yOrigin, width, height)];
    currentPDFContentHeight += height;
}

- (void)dealloc {
    if (dataTimer != nil) {
        [dataTimer invalidate];
        dataTimer = nil;
    }
}

- (NSMutableArray *)buildCellItems {

    NSString *rot1 = [[self.deviceInfo getRotationOne] uppercaseString];
    
    if (rot1 == nil || [rot1 isEqualToString:@""]) {
        rot1 = @"-";
    } else {
        rot1 = [NSString stringWithFormat:@"%@\n(Minutes)", [[self.deviceInfo getRotationOne] uppercaseString]];
    }
    
    NSString *rot2 = [[self.deviceInfo getRotationTwo] uppercaseString];
    if (rot2 == nil || [rot2 isEqualToString:@""]) {
        rot2 = @"-";
    } else {
        rot2 = [NSString stringWithFormat:@"%@\n(Minutes)", [[self.deviceInfo getRotationTwo] uppercaseString]];
    }
    
    return [[NSMutableArray alloc] initWithObjects:
                 @"DATE",
                 rot1,
                 rot2,
                 @"TOTAL USAGE\n(Minutes)",
                 @"ROM Treated", nil];
}

- (void)buildGroupChartView {
    _groupBarChartView.delegate = self;
    
    _groupBarChartView.highlightFullBarEnabled = NO;
    _groupBarChartView.highlightPerTapEnabled = NO;
    
    _groupBarChartView.chartDescription.enabled = YES;
    _groupBarChartView.dragEnabled = NO;
    [_groupBarChartView setScaleEnabled:NO];
    _groupBarChartView.pinchZoomEnabled = NO;
    _groupBarChartView.drawBarShadowEnabled = NO;
    _groupBarChartView.drawGridBackgroundEnabled = NO;
    
    ChartLegend *legend = _groupBarChartView.legend;
    legend.horizontalAlignment = ChartLegendHorizontalAlignmentCenter;
    legend.verticalAlignment = ChartLegendVerticalAlignmentBottom;
    legend.orientation = ChartLegendOrientationHorizontal;
    legend.drawInside = NO;
    legend.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:10.f];
    legend.textColor = UIColor.grayColor;
    legend.yOffset = 10.0;
    legend.xOffset = 10.0;
    legend.yEntrySpace = 0.0;
    
    ChartXAxis *xAxis = _groupBarChartView.xAxis;
    xAxis.labelPosition = XAxisLabelPositionBothSided;
    xAxis.drawAxisLineEnabled = YES;
    xAxis.drawGridLinesEnabled = NO;
    xAxis.drawLabelsEnabled = NO;

    NSNumberFormatter *leftAxisFormatter = [[NSNumberFormatter alloc] init];
    leftAxisFormatter.minimumFractionDigits = 0;
    leftAxisFormatter.maximumFractionDigits = 1;
    leftAxisFormatter.negativeSuffix = @"";
    leftAxisFormatter.positiveSuffix = @"";
    
    ChartYAxis *leftAxis = _groupBarChartView.leftAxis;
    leftAxis.labelFont = [UIFont systemFontOfSize:9.f];
    leftAxis.labelCount = 5;
    leftAxis.labelTextColor = [UIColor grayColor];

    leftAxis.valueFormatter = [[ChartDefaultAxisValueFormatter alloc] initWithFormatter:leftAxisFormatter];
    leftAxis.labelPosition = YAxisLabelPositionOutsideChart;
    leftAxis.spaceTop = 0.0;
    leftAxis.axisMinimum = 0.0;
    
    ChartYAxis *rightAxis = _groupBarChartView.rightAxis;
    rightAxis.enabled = YES;
    rightAxis.drawAxisLineEnabled = YES;
    rightAxis.drawGridLinesEnabled = NO;
    rightAxis.drawLabelsEnabled = NO;
    
}

- (void)updateGroupChartData
{
    float groupSpace = 0.1f;
    float barSpace = 0.01f;
    float barWidth = 0.2f;
    
    NSMutableArray *yVals1 = [[NSMutableArray alloc] init];
    NSMutableArray *yVals2 = [[NSMutableArray alloc] init];
    
    int groupCount = (int)itemsMA.count;
    int start = 0;
    int end = start + groupCount;
    
    for (int i = start; i < end; i++)
    {
        UsageData *data = [itemsMA objectAtIndex:i];
        [yVals1 addObject:[[BarChartDataEntry alloc]
                           initWithX:i
                           y: data.rotation1 ]];
        
        [yVals2 addObject:[[BarChartDataEntry alloc]
                           initWithX:i
                           y: data.rotation2]];
        
    }
    
    BarChartDataSet *set1 = nil, *set2 = nil;
    
    set1 = [[BarChartDataSet alloc] initWithValues:yVals1 label:[self.deviceInfo getRotationOne]];
    [set1 setColor:[UIColor colorWithRed:6/255.f green:115/255.f blue:199/255.f alpha:1.f]];

    set2 = [[BarChartDataSet alloc] initWithValues:yVals2 label:[self.deviceInfo getRotationTwo]];
    [set2 setColor:[UIColor colorWithRed:198/255.f green:32/255.f blue:74/255.f alpha:1.f]];

    set1.drawIconsEnabled = NO;
    set1.drawValuesEnabled = NO;
    set2.drawIconsEnabled = NO;
    set2.drawValuesEnabled = NO;
    
    NSMutableArray *dataSets = [[NSMutableArray alloc] init];
    [dataSets addObject:set1];
    [dataSets addObject:set2];
    
    
    BarChartData *data = [[BarChartData alloc] initWithDataSets:dataSets];

    data.barWidth = barWidth;
    
    _groupBarChartView.xAxis.axisMinimum = start;
    
    _groupBarChartView.xAxis.axisMaximum = start + [data groupWidthWithGroupSpace:groupSpace barSpace: barSpace] * groupCount;
    
    [data groupBarsFromX: start groupSpace: groupSpace barSpace: barSpace];
    
    _groupBarChartView.data = data;
    
}

- (void)buildChartView {
    _chartView.delegate = self;
    
    [self setupBarLineChartView:_chartView];
    
    _chartView.drawValueAboveBarEnabled = YES;
    _chartView.drawBarShadowEnabled = NO;
    _chartView.highlightFullBarEnabled = NO;
    _chartView.highlightPerTapEnabled = NO;
    
    _chartView.chartDescription.enabled = NO;
    
    _chartView.drawGridBackgroundEnabled = NO;
    
    _chartView.dragEnabled = NO;
    [_chartView setScaleEnabled:NO];
    _chartView.pinchZoomEnabled = NO;
    
    ChartXAxis *xAxis = _chartView.xAxis;
    xAxis.labelPosition = XAxisLabelPositionBothSided;
    xAxis.drawAxisLineEnabled = YES;
    xAxis.drawGridLinesEnabled = NO;
    xAxis.drawLabelsEnabled = NO;
    
    NSNumberFormatter *leftAxisFormatter = [[NSNumberFormatter alloc] init];
    leftAxisFormatter.minimumFractionDigits = 0;
    leftAxisFormatter.maximumFractionDigits = 1;
    leftAxisFormatter.negativeSuffix = @"";
    leftAxisFormatter.positiveSuffix = @"";
    
    ChartYAxis *leftAxis = _chartView.leftAxis;
    leftAxis.labelFont = [UIFont systemFontOfSize:9.f];
    leftAxis.labelCount = 5;
    leftAxis.labelTextColor = UIColor.grayColor;
    
    leftAxis.valueFormatter = [[ChartDefaultAxisValueFormatter alloc] initWithFormatter:leftAxisFormatter];
    leftAxis.labelPosition = YAxisLabelPositionOutsideChart;
    leftAxis.spaceTop = 0.0;
    leftAxis.axisMinimum = 0.0;
    leftAxis.axisMaximum = 1.0;
    
    self.leftAxis = leftAxis;
    
    ChartYAxis *rightAxis = _chartView.rightAxis;
    rightAxis.enabled = YES;
    rightAxis.drawAxisLineEnabled = YES;
    rightAxis.drawGridLinesEnabled = NO;
    rightAxis.drawLabelsEnabled = NO;
    
    ChartLegend *l = _chartView.legend;
    
    l.horizontalAlignment = ChartLegendHorizontalAlignmentLeft;
    l.verticalAlignment = ChartLegendVerticalAlignmentBottom;
    l.orientation = ChartLegendOrientationHorizontal;
    l.drawInside = YES;
    l.form = ChartLegendFormSquare;
    l.formSize = 0.0;
    l.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:12.f];
    l.textColor = UIColor.grayColor;

    l.xEntrySpace = 0.0;
}

- (void)setupBarLineChartView:(BarLineChartViewBase *)chartView
{
    
}

- (void)updateData {
    [self updateGroupChartData];
    [self updateChartData];
    [self.spreadsheetView reloadData];
    
    int totalRotations = 0;
    for (UsageData *uData in itemsMA) {
        totalRotations += uData.rotation1 + uData.rotation2;
    }
    
    _totalMinutesLabel.text = [NSString stringWithFormat:@"Total Minutes: %d", totalRotations];
}

- (void)updateChartData
{
    [self setDataCount:1.0];
}

- (void)setDataCount:(double)minRange
{
    NSMutableArray *yVals = [[NSMutableArray alloc] init];
    NSInteger count = itemsMA.count;
    
    double maxYAxis = minRange;
    
    for (NSInteger i = 0; i < count; i++)
    {
        UsageData *uData = [itemsMA objectAtIndex:i];
        double val = 0;
        val = uData.rom * [self.deviceInfo getGearingRatio];
        
        maxYAxis = MAX(val, maxYAxis);
        
        [yVals addObject:[[BarChartDataEntry alloc] initWithX:i y:val]];
    }
    
    if (maxYAxis > minRange) {
        [self.leftAxis setAxisMaximum:maxYAxis];
        [self.leftAxis resetCustomAxisMax];
    }
    
    BarChartDataSet *set1 = nil;
    if (_chartView.data.dataSetCount > 0)
    {
        set1 = (BarChartDataSet *)_chartView.data.dataSets[0];
        set1.values = yVals;
        [_chartView.data notifyDataChanged];
        [_chartView notifyDataSetChanged];
    }
    else
    {
        set1 = [[BarChartDataSet alloc] initWithValues:yVals label:@""];
        [set1 setColors:@[[UIColor colorWithRed:6/255.f green:115/255.f blue:199/255.f alpha:1.f]]];
        set1.drawIconsEnabled = NO;
        set1.drawValuesEnabled = NO;
        
        NSMutableArray *dataSets = [[NSMutableArray alloc] init];
        [dataSets addObject:set1];
        
        BarChartData *data = [[BarChartData alloc] initWithDataSets:dataSets];
        data.barWidth = 0.9f;
        
        _chartView.data = data;
    }
}


- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    if (@available(iOS 11.0, *)) {
        self.spreadsheetView.frame = self.spreadSheetContentView.safeAreaLayoutGuide.layoutFrame;
    } else {
        //Fallback on earlier version
        self.spreadsheetView.frame = self.spreadSheetContentView.bounds;
    }
    
    if (@available(iOS 12.0, *)) {
       if (self.traitCollection.userInterfaceStyle == UIUserInterfaceStyleDark) { //Dark Mode
           self.spreadsheetView.gridStyle = [GridStyle style:GridStyle_solid width:0.5 color:[UIColor darkGrayColor]];
       } else {
           self.spreadsheetView.gridStyle = [GridStyle style:GridStyle_solid width:0.5 color:[UIColor lightGrayColor]];
       }
    } else {
        self.spreadsheetView.gridStyle = [GridStyle style:GridStyle_solid width:0.5 color:[UIColor lightGrayColor]];
    }
}

#pragma mark - getters
- (SpreadsheetView *)spreadsheetView {
    if (!_spreadsheetView) {
        _spreadsheetView = ({
            SpreadsheetView *ssv = [SpreadsheetView new];
            ssv.bounces = false;
            
            if (@available(iOS 12.0, *)) {
               if (self.traitCollection.userInterfaceStyle == UIUserInterfaceStyleDark) { //Dark Mode
                   ssv.gridStyle = [GridStyle style:GridStyle_solid width:0.5 color:[UIColor darkGrayColor]];
               } else {
                   ssv.gridStyle = [GridStyle style:GridStyle_solid width:0.5 color:[UIColor lightGrayColor]];
               }
            } else {
                ssv.gridStyle = [GridStyle style:GridStyle_solid width:0.5 color:[UIColor lightGrayColor]];
            }
            
            ssv.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            [self.spreadSheetContentView addSubview:ssv];
            
            ssv;
        });
    }
    return _spreadsheetView;
}

#pragma Mark - SpreadsheetViewDataSource
- (NSInteger)numberOfColumns:(SpreadsheetView *)spreadsheetView {
    return COLUMN_COUNT;
}

- (NSInteger)numberOfRows:(SpreadsheetView *)spreadsheetView {
    return itemsMA.count + 1;
}

- (CGFloat)spreadsheetView:(SpreadsheetView *)spreadsheetView widthForColumn:(NSInteger)column {
    return (CGFloat) (_currentSize.width - 6)/ 5;
}

- (CGFloat)spreadsheetView:(SpreadsheetView *)spreadsheetView heightForRow:(NSInteger)row {
    return 40;
}

- (NSInteger)frozenRows:(SpreadsheetView *)spreadsheetView {
    return 1;
}

- (ZMJCell *)spreadsheetView:(SpreadsheetView *)spreadsheetView cellForItemAt:(NSIndexPath *)indexPath {
    NSInteger column = indexPath.column;
    NSInteger row    = indexPath.row;
    NSString *item = @"";
    
    if (row == 0) {
        item = [headersMA objectAtIndex:column];
        
        SessionHeaderCell *cell = (SessionHeaderCell *)[spreadsheetView dequeueReusableCellWithReuseIdentifier:@"SessionHeaderCell" forIndexPath:indexPath];
        cell.label.attributedText = [[NSMutableAttributedString alloc] initWithString:item];
        return cell;

    }
    
    
    
    UsageData *uData = nil;

    if (itemsMA.count > row - 1) {
        uData = [itemsMA objectAtIndex:(row - 1)];
    }
    
    item = [self getItemFromColumn:uData column:(int)column];
    
    SessionCell *cell = (SessionCell *)[spreadsheetView dequeueReusableCellWithReuseIdentifier:@"SessionCell" forIndexPath:indexPath];
    cell.label.text = item;
    
    return cell;
}

- (NSString *)getItemFromColumn:(UsageData *)uData column:(int)column {
    NSString *item = @"";
    
    if (uData != nil) {
        switch (column) {
            case 0:
                item = [NSString stringWithFormat:@"%02d/%02d", uData.month, uData.day];
                break;
            case 1:
                item = [NSString stringWithFormat:@"%d", uData.rotation1];
                break;
            case 2:
                item = [NSString stringWithFormat:@"%d", uData.rotation2];
                break;
            case 3:
                item = [NSString stringWithFormat:@"%d",(uData.rotation1 + uData.rotation2)];
                break;
            case 4:
                item = [NSString stringWithFormat:@"%.02f", uData.rom * [self.deviceInfo getGearingRatio]];
                break;
                
            default:
                break;
        }
    }
    
    return item;
}

#pragma mark - ChartViewDelegate

- (void)chartValueSelected:(ChartViewBase * __nonnull)chartView entry:(ChartDataEntry * __nonnull)entry highlight:(ChartHighlight * __nonnull)highlight
{
    NSLog(@"chartValueSelected");
}

- (void)chartValueNothingSelected:(ChartViewBase * __nonnull)chartView
{
    NSLog(@"chartValueNothingSelected");
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    __weak UsageDataViewController *weakSelf = self;
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context)
     {
         weakSelf.currentSize = size;
         [weakSelf.spreadsheetView reloadData];
         if (@available(iOS 11.0, *)) {
             weakSelf.spreadsheetView.frame = weakSelf.spreadSheetContentView.safeAreaLayoutGuide.layoutFrame;
         } else {
             //Fallback on earlier version
             weakSelf.spreadsheetView.frame = weakSelf.spreadSheetContentView.bounds;
         }
         
     } completion:^(id<UIViewControllerTransitionCoordinatorContext> context)
     {}];
}

- (void)willTransitionToTraitCollection:(UITraitCollection *)newCollection withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [super willTransitionToTraitCollection:newCollection withTransitionCoordinator:coordinator];
}

//Rendering PDF Content Here
-(void)drawText:(NSString*)textToDraw inFrame:(CGRect)frameRect
{
    CFStringRef stringRef = (__bridge CFStringRef)textToDraw;
    // Prepare the text using a Core Text Framesetter
    CFAttributedStringRef currentText = CFAttributedStringCreate(NULL, stringRef, NULL);
    CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString(currentText);
    
    
    CGMutablePathRef framePath = CGPathCreateMutable();
    CGPathAddRect(framePath, NULL, frameRect);
    
    // Get the frame that will do the rendering.
    CFRange currentRange = CFRangeMake(0, 0);
    CTFrameRef frameRef = CTFramesetterCreateFrame(framesetter, currentRange, framePath, NULL);
    CGPathRelease(framePath);
    
    // Get the graphics context.
    CGContextRef    currentContext = UIGraphicsGetCurrentContext();
    
    // Put the text matrix into a known state. This ensures
    // that no old scaling factors are left in place.
    CGContextSetTextMatrix(currentContext, CGAffineTransformIdentity);
    
    
    // Core Text draws from the bottom-left corner up, so flip
    // the current transform prior to drawing.
    CGContextTranslateCTM(currentContext, 0, frameRect.origin.y*2);
    CGContextScaleCTM(currentContext, 1.0, -1.0);
    
    // Draw the frame.
    CTFrameDraw(frameRef, currentContext);
    
    CGContextScaleCTM(currentContext, 1.0, -1.0);
    CGContextTranslateCTM(currentContext, 0, (-1)*frameRect.origin.y*2);
    
    
    CFRelease(frameRef);
    CFRelease(framesetter);
}

-(void)drawTableAt:(CGPoint)origin
     withRowHeight:(int)rowHeight
    andColumnWidth:(int)columnWidth
       andRowCount:(int)numberOfRows
    andColumnCount:(int)numberOfColumns

{
    
    for (int i = 0; i <= numberOfRows; i++) {
        
        int newOrigin = origin.y + (rowHeight*i);
        
        
        CGPoint from = CGPointMake(origin.x, newOrigin);
        CGPoint to = CGPointMake(origin.x + (numberOfColumns*columnWidth), newOrigin);
        
        [self drawLineFromPoint:from toPoint:to];
        
        
    }
    
    for (int i = 0; i <= numberOfColumns; i++) {
        
        int newOrigin = origin.x + (columnWidth*i);
        
        
        CGPoint from = CGPointMake(newOrigin, origin.y);
        CGPoint to = CGPointMake(newOrigin, origin.y +(numberOfRows*rowHeight));
        
        [self drawLineFromPoint:from toPoint:to];
        
        
    }
}

-(void)drawTableDataAt:(CGPoint)origin
         withRowHeight:(int)rowHeight
        andColumnWidth:(int)columnWidth
           andRowCount:(int)numberOfRows
        andColumnCount:(int)numberOfColumns
             fromIndex:(int)fromIndex
               toIndex:(int)toIndex
{
    int padding = 10;
    
    for (int j = 0; j < headersMA.count; j++)
    {
    
        int newOriginX = origin.x + (j*columnWidth);
        int newOriginY = origin.y + rowHeight;
        
        CGRect frame = CGRectMake(newOriginX + padding, newOriginY + padding, columnWidth, rowHeight);
        
        NSString* item = [headersMA objectAtIndex:j];
        item = [item stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
        [self drawText:item inFrame:frame];
    }
    
    for(int i = fromIndex; i < toIndex; i++)
    {
        if (i >= itemsMA.count || i < 0) {
            continue;
        }
        
        UsageData* data = [itemsMA objectAtIndex:i];
        
        for (int j = 0; j < COLUMN_COUNT; j++)
        {
            
            int newOriginX = origin.x + (j*columnWidth);
            int newOriginY = origin.y + ((i + 2 - fromIndex)*rowHeight);
            
            CGRect frame = CGRectMake(newOriginX + padding, newOriginY + padding, columnWidth, rowHeight);
            
            
            [self drawText:[self getItemFromColumn:data column:j] inFrame:frame];
        }
    }
    
    currentPDFContentHeight += (rowHeight + padding) * numberOfRows;
}

-(void)drawLineFromPoint:(CGPoint)from toPoint:(CGPoint)to
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetLineWidth(context, 2.0);
    
    CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
    
    CGFloat components[] = {0.2, 0.2, 0.2, 0.3};
    
    CGColorRef color = CGColorCreate(colorspace, components);
    
    CGContextSetStrokeColorWithColor(context, color);
    
    
    CGContextMoveToPoint(context, from.x, from.y);
    CGContextAddLineToPoint(context, to.x, to.y);
    
    CGContextStrokePath(context);
    CGColorSpaceRelease(colorspace);
    CGColorRelease(color);
    
}

-(void)drawView:(UIView *)view rect:(CGRect)rect
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, NO, 0.0f);
    [view drawViewHierarchyInRect:view.bounds afterScreenUpdates:NO];
    UIImage *snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
    [snapshotImage drawInRect:rect];
}

- (void)drawPageNumber: (NSInteger) pageNumber {
    NSString *pageNumberString = [NSString stringWithFormat:@"%ld", pageNumber];
    UIFont *theFont = [UIFont systemFontOfSize:12];
    
    CGSize pageNumberStringSize = [CommonHelper sizeWithString:pageNumberString withFont:theFont];
    CGRect stringRenderingRect = CGRectMake(PDF_WIDTH - 40, PDF_HEIGHT - 40, pageNumberStringSize.width, pageNumberStringSize.height);
    
    [pageNumberString drawInRect:stringRenderingRect withAttributes:@{NSFontAttributeName: theFont}];
}

- (void)drawImage:(UIImage *)image rect:(CGRect)rect
{
    [image drawInRect:rect];
}
@end
