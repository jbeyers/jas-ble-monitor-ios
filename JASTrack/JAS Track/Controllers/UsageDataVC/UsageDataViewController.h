//
//  UsageDataViewController.h
//  JAS Track
//
//  Created by Yuriy on 2/18/19.
//  Copyright © 2019 Yuriy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CBController.h"
#import "DeviceInfo.h"

NS_ASSUME_NONNULL_BEGIN

@interface UsageDataViewController : BaseViewController
@property (nonatomic, retain) MyPeripheral *connectedPeripheral;
@property (nonatomic, retain) CBController *cbController;
@property (nonatomic, retain) DeviceInfo *deviceInfo;
@end

NS_ASSUME_NONNULL_END
