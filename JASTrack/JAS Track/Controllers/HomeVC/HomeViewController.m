//
//  HomeViewController.m
//  JAS Track
//
//  Created by Yuriy on 2/13/19.
//  Copyright © 2019 Yuriy. All rights reserved.
//

#import "HomeViewController.h"
#import "DeviceViewController.h"
#import "CBController.h"
#import "DeviceInfo.h"
#import "UsageDataViewController.h"

#if UVC
#import "UVCeed-Swift.h"
#endif

@interface HomeViewController () <CBControllerDelegate, CBPeripheralManagerDelegate>
@property (weak, nonatomic) IBOutlet UIView *scanningView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loader;

@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;

@property (weak, nonatomic) IBOutlet UILabel *scanningLab;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *lblNoFound;
@property (nonatomic, retain) CBController *cbController;
@property (nonatomic, retain) NSArray *signalImages;
@property (nonatomic, retain) UIRefreshControl *refreshControl;
@end

@implementation HomeViewController {
    NSMutableArray *connectedDeviceInfo;
    DeviceInfo *deviceInfo;
    MyPeripheral *controlPeripheral;
//    NSMutableDictionary  *deviceRecord;
    BOOL isPushed;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _scanningView.hidden = YES;
    self.navigationController.navigationBarHidden = YES;

    _cbController = [[CBController alloc] init];
    
    _refreshControl = [[UIRefreshControl alloc]init];
    [_refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    _refreshControl.tintColor = self.view.backgroundColor;
    
    if (@available(iOS 10.0, *)) {
        self.tableView.refreshControl = _refreshControl;
    } else {
        [self.tableView addSubview:_refreshControl];
    }
    
    self.signalImages = [[NSArray alloc] initWithObjects:
                            [UIImage imageNamed:@"SignalOne"],
                            [UIImage imageNamed:@"SignalTwo"],
                            [UIImage imageNamed:@"SignalThree"],
                            [UIImage imageNamed:@"SignalFour"],
                            [UIImage imageNamed:@"SignalFive"], nil];
    
    connectedDeviceInfo = [NSMutableArray new];
    
//    NSDictionary *record = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceRecord"];
//    if (record == nil) {
//        deviceRecord = [[NSMutableDictionary alloc] init];
//    }
//    else {
//        deviceRecord = [record mutableCopy];
//    }
//
//
//    deviceInfo = [[DeviceInfo alloc]init];
}

- (void)updateUI {
    self.view.backgroundColor = UIColor.whiteColor;
    self.loader.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    
    if (@available(iOS 12.0, *)) {
       if (self.traitCollection.userInterfaceStyle == UIUserInterfaceStyleDark) { //Dark Mode
           self.loader.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
           self.view.backgroundColor = UIColor.blackColor;
#if UVC
           _logoImageView.image = [UIImage imageNamed:@"UVCeed.png"];
#else
           _logoImageView.image = [UIImage imageNamed:@"dark_logo.png"];
#endif
           
       } else { // Light Mode
           self.loader.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
#if UVC
          _logoImageView.image = [UIImage imageNamed:@"UVCeed.png"];
#else
          _logoImageView.image = [UIImage imageNamed:@"v_logo.png"];
#endif
       }
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    isPushed = false;
    
    [_cbController setDelegate:self];
    
    if([connectedDeviceInfo count] == 0) {
        [_cbController configureTransparentServiceUUID:nil txUUID:nil rxUUID:nil];
        [_cbController configureDeviceInformationServiceUUID:nil UUID2:nil];
    }
    
    [self performSelector:@selector(startScan) withObject:nil afterDelay:0.1];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self stopScan];
}

- (void)refreshTable {
    //TODO: refresh your data
    if (_cbController.devicesList != nil) {
        [_cbController.devicesList removeAllObjects];
        [_tableView reloadData];
    }
    [_refreshControl endRefreshing];
    [self performSelector:@selector(startScan) withObject:nil afterDelay:0.1];
}

- (void)startScan {  
    [_cbController startScanWithUUID:nil];
    _scanningView.hidden = NO;
}

- (void)stopScan {
    [_cbController stopScan];
    _scanningView.hidden = YES;
}

#pragma Mark - CBControllerDelegate
- (void)CBController:(CBController *)cbController startedDiscovered:(BOOL)isStarted {
    _scanningView.hidden = !isStarted;
    
    if (!isStarted) {
        NSString *status = [_cbController getBLEHardwareErrorState];
        [self showAlertWithTitle:nil message:status actionTitle:nil];        
    }
}

- (void)CBController:(CBController *)cbController didUpdateDiscoveredPeripherals:(NSArray *)peripherals {
    [self displayDevicesList];
    [_tableView reloadData];
}

- (void)CBController:(CBController *)cbController didDisconnectedPeripheral:(MyPeripheral *)myPeripheral {
    NSLog(@"updateMyPeripheralForDisconnect");
    if (myPeripheral == controlPeripheral) {
//        [NSTimer scheduledTimerWithTimeInterval:0.03 target:self selector:@selector(popToRootPage) userInfo:nil repeats:NO];
    }
    
    for (int idx =0; idx< [connectedDeviceInfo count]; idx++) {
        DeviceInfo *tmpDeviceInfo = [connectedDeviceInfo objectAtIndex:idx];
        if (tmpDeviceInfo.myPeripheral == myPeripheral) {
            [connectedDeviceInfo removeObjectAtIndex:idx];
            NSLog(@"updateMyPeripheralForDisconnect1");
            break;
        }
    }
    
    [self displayDevicesList];
    
    if(cbController.isScanning == YES){
        [self stopScan];
        [self startScan];
        [_tableView reloadData];
    }
}

- (void)CBController:(CBController *)cbController didConnectedPeripheral:(MyPeripheral *)myPeripheral {
    NSLog(@"updateMyPeripheralForNewConnected");
    DeviceInfo *tmpDeviceInfo = [[DeviceInfo alloc]init];
    tmpDeviceInfo.myPeripheral = myPeripheral;
    tmpDeviceInfo.myPeripheral.connectStatus = myPeripheral.connectStatus;
    
    controlPeripheral = myPeripheral;
    
//    [deviceRecord setObject:[myPeripheral advName] forKey:[myPeripheral uuidString]];
//    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
//    [def setObject:deviceRecord forKey:@"deviceRecord"];
//    [def synchronize];
    
    /*Connected List Filter*/
    bool b = FALSE;
    for (int idx =0; idx< [connectedDeviceInfo count]; idx++) {
        DeviceInfo *tmpDeviceInfo = [connectedDeviceInfo objectAtIndex:idx];
        if (tmpDeviceInfo.myPeripheral == myPeripheral) {
            b = TRUE;
            break;
        }
    }
    if (!b) {
        [connectedDeviceInfo addObject:tmpDeviceInfo];
    }
    else{
        NSLog(@"Connected List Filter!");
    }
    
    [self displayDevicesList];
    
    [self performSelector:@selector(displayDeviceDetails) withObject:nil afterDelay:0.5];
}

- (void) displayDevicesList {
    if ([_cbController.devicesList count] == 0) {
        _lblNoFound.hidden = NO;
    } else {
        _lblNoFound.hidden = YES;
    }
    [_tableView reloadData];
}

- (void)disconnectAllDevices {
    
    for (DeviceInfo *devInfo in connectedDeviceInfo) {
        [_cbController disconnectDevice:devInfo.myPeripheral];
    }
}

- (IBAction)actionButtonCancelConnect:(id)sender {
    int idx = (int)[sender tag];
    MyPeripheral *tmpPeripheral = [_cbController.devicesList objectAtIndex:idx];
    tmpPeripheral.connectStatus = MYPERIPHERAL_CONNECT_STATUS_IDLE;
    [_cbController.devicesList replaceObjectAtIndex:idx withObject:tmpPeripheral];
    
    for (int idx =0; idx< [_cbController.connectingList count]; idx++) {
        MyPeripheral *tmpConnectingPeripheral = [_cbController.connectingList objectAtIndex:idx];
        if (tmpConnectingPeripheral == tmpPeripheral) {
            [_cbController.connectingList removeObjectAtIndex:idx];
            break;
        }
    }
    
    [_cbController disconnectDevice:tmpPeripheral];
    [self displayDevicesList];
    
}

// DataSource methods
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    #if MOCK
    return 1;
    #endif
    return [_cbController.devicesList count];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell;
    
    cell = [tableView dequeueReusableCellWithIdentifier:@"devicesList"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"devicesList"];
    }
    
    #if MOCK
    cell.textLabel.text = @"Mock";
    cell.detailTextLabel.text = @"connecting...";
    UIView *RSSIView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 90, 25)];
    UIImageView *ivPower = [[UIImageView alloc] initWithFrame:CGRectMake(0, 2, 25, 20)];
    ivPower.contentMode = UIViewContentModeScaleAspectFit;
    ivPower.image = [self.signalImages objectAtIndex:[self getRSSIsignalIndex:[NSNumber numberWithInt:-30]]];
    [RSSIView addSubview:ivPower];

    UILabel *lblPower = [[UILabel alloc] initWithFrame:CGRectMake(30, 0, 60, 25)];
    lblPower.font = [UIFont systemFontOfSize:12.f];
    lblPower.text = [NSString stringWithFormat:@"%d dBm", -30];
    [RSSIView addSubview:lblPower];

    cell.accessoryView  = RSSIView;
    if (cell.textLabel.text == nil)
        cell.textLabel.text = @"Unknow";
    #else
    
    MyPeripheral *tmpPeripheral = [_cbController.devicesList objectAtIndex:indexPath.row];
    cell.textLabel.text = tmpPeripheral.advName;
    cell.detailTextLabel.text = @"";
    cell.accessoryView = nil;
    if (tmpPeripheral.connectStatus == MYPERIPHERAL_CONNECT_STATUS_CONNECTING) {
        cell.detailTextLabel.text = @"connecting...";
    }
    
    UIView *RSSIView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 90, 25)];
    UIImageView *ivPower = [[UIImageView alloc] initWithFrame:CGRectMake(0, 2, 25, 20)];
    ivPower.contentMode = UIViewContentModeScaleAspectFit;
    ivPower.image = [self.signalImages objectAtIndex:[self getRSSIsignalIndex:tmpPeripheral.oRSSI]];
    [RSSIView addSubview:ivPower];
    
    UILabel *lblPower = [[UILabel alloc] initWithFrame:CGRectMake(30, 0, 60, 25)];
    lblPower.font = [UIFont systemFontOfSize:12.f];
    lblPower.text = [NSString stringWithFormat:@"%d dBm", [tmpPeripheral.oRSSI intValue]];
    [RSSIView addSubview:lblPower];
    
    cell.accessoryView  = RSSIView;
    if (cell.textLabel.text == nil)
        cell.textLabel.text = @"Unknow";
    #endif

    return cell;
}

-(int)getRSSIsignalIndex:(NSNumber *)rssi
{
    
    int index = 0;
    int rssiInt = [rssi intValue];
    
    if (rssiInt == 127) {     // value of 127 reserved for RSSI not available
        index = 0;
    }
    else if (rssiInt <= -84) {
        index = 0;
    }
    else if (rssiInt <= -72) {
        index = 1;
    }
    else if (rssiInt <= -60) {
        index = 2;
    }
    else if (rssiInt <= -48) {
        index = 3;
    }
    else {
        index = 4;
    }
    
    return index;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    NSString *title = @"Discovered Devices:";
    return title;
}


-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    #if MOCK
    [self displayDeviceDetails];
    return;
    #else
    int count = (int)[_cbController.devicesList count];
    if ((count != 0) && count > indexPath.row) {
        MyPeripheral *tmpPeripheral = [_cbController.devicesList objectAtIndex:indexPath.row];
        if (tmpPeripheral.connectStatus != MYPERIPHERAL_CONNECT_STATUS_IDLE) {
            //NSLog(@"Device is not idle - break");
            return;
        }
        
        [self stopScan];
        [_cbController connectDevice:tmpPeripheral];
        
        [self displayDevicesList];
        
        controlPeripheral = tmpPeripheral;
    }
    #endif
}

- (void)displayDeviceDetails {
    if (isPushed)
        return;
    
    isPushed = true;
   
#if UVC
    NSString *controllerId = @"VisionObjectRecognitionViewController";
    VisionObjectRecognitionViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:controllerId];
    
//    NSString *controllerId = @"UVCARCameraViewController";
//    UVCARCameraViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:controllerId];
//
    vc.connectedPeripheral = controlPeripheral;
    vc.cbController = _cbController;

    [self.navigationController pushViewController:vc animated:true];
#else
    DeviceViewController *deviceVC = [self.storyboard instantiateViewControllerWithIdentifier:@"DeviceViewController"];
    deviceVC.connectedPeripheral = controlPeripheral;
    deviceVC.cbController = _cbController;

    [self.navigationController pushViewController:deviceVC animated:true];
#endif
}

@end
