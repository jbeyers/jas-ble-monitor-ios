//
//  FilesViewController.m
//  JAS Track
//
//  Created by Crane on 6/5/19.
//  Copyright © 2019 Yuriy. All rights reserved.
//

#import "FilesViewController.h"
#import "FilesCell.h"
#import "UUID.h"
#import "CommonHelper.h"
#import <MessageUI/MessageUI.h>
#import <QuickLook/QuickLook.h>

@interface FilesViewController () <UITableViewDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate, QLPreviewControllerDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *noLabel;

@property (nonatomic, retain) NSMutableArray *files;
@property (nonatomic, retain) NSURL *previewItem;
@end

@implementation FilesViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    _files = [NSMutableArray new];
}

- (void)updateUI {
    self.view.backgroundColor = UIColor.whiteColor;
    if (@available(iOS 12.0, *)) {
       if (self.traitCollection.userInterfaceStyle == UIUserInterfaceStyleDark) { //Dark Mode
           self.view.backgroundColor = UIColor.blackColor;
       }
    }
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self loadFiles];
}

- (void)loadFiles {
    self.loadingIndicator.visibility = true;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);

    NSArray* dirs = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[paths objectAtIndex:0]
                                                                        error:NULL];
    NSMutableArray *files = [[NSMutableArray alloc] init];
    [dirs enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSString *filename = (NSString *)obj;
        NSString *extension = [[filename pathExtension] lowercaseString];
        if ([extension isEqualToString:@"pdf"] || [extension isEqualToString:@"csv"]) {
            [files addObject:[[paths objectAtIndex:0] stringByAppendingPathComponent:filename]];
        }
    }];
    
    NSArray *sortedArray;
    sortedArray = [files sortedArrayUsingComparator:^NSComparisonResult(NSString* a, NSString* b) {
        double t1 = [self getTimestampFromPath:a];
        double t2 = [self getTimestampFromPath:b];
            
        return t1 < t2;
    }];
    
    _files = [[NSMutableArray alloc] initWithArray:sortedArray];
    
    self.loadingIndicator.visibility = false;
    [self reload];
}

- (void)reload {
    _noLabel.hidden = _files.count > 0;
    _tableView.hidden = _files.count == 0;
    
    [_tableView reloadData];
}

-(double) getTimestampFromPath: (NSString *) path {
    NSString *aName = [[path lastPathComponent] stringByDeletingPathExtension];

   NSArray *names = [aName componentsSeparatedByString:@"_"];
   if (names.count == 3) {
       NSString *timestampStr = [names objectAtIndex:2];
       return [timestampStr doubleValue];
   }
    
    return 0;
}
                   
- (void)shareFile:(UIButton *)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Are you sure you want to share it?" message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSString *path = [self.files objectAtIndex:sender.tag];
        [self shareTheFile:path];
    }];
    [alert addAction:actionOk];
    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alert addAction:actionCancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)trashFile:(UIButton *)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Are you sure you want to delete it?" message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
   
    UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSString *path = [self.files objectAtIndex:sender.tag];
        [self removeFile:path];
    }];
    [alert addAction:actionOk];
    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alert addAction:actionCancel];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}


- (void)shareTheFile:(NSString *)path {
    MFMailComposeViewController *mailController = [[MFMailComposeViewController alloc]init];
    mailController.mailComposeDelegate = self;
    
    NSData *fileData = [NSData dataWithContentsOfFile:path];
    NSString *mimeType;
    if ([CommonHelper isCSV:path]) {
        mimeType = @"text/csv";
    } else {
        mimeType = @"application/pdf";
    }
    

    [mailController addAttachmentData:fileData mimeType:mimeType fileName:[path lastPathComponent]];
    
    mailController.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentViewController:mailController animated:YES completion:nil];
}


- (void)removeFile:(NSString *)path {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSError *error;
    if ([fileManager removeItemAtPath:path error:&error] == NO) {
        NSLog(@"Failed to remove the path %@", path);
        return;
    }
    
    [self showAlertWithTitle:@"File Deleted!" message:nil actionTitle:@"Ok"];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self loadFiles];
    });
    
}

- (void)configureCell:(FilesCell *) cell filePath:(NSString *)path {
    NSString *fileName = [[path lastPathComponent] stringByDeletingPathExtension];
    
    NSArray *names = [fileName componentsSeparatedByString:@"_"];
    if (names.count == 3) {
        NSString *timestampStr = [names objectAtIndex:2];
        if (timestampStr != nil) {
            double timestamp = [timestampStr doubleValue];
            cell.lblDate.text = [CommonHelper timestampToDate:timestamp];
            cell.lblTime.text = [CommonHelper timestampToTime:timestamp];
        }
        
        NSString *sn = [names objectAtIndex:1];
        cell.lblType.text = sn;
    }
    
    
    UIImage *thumbnail;
    if ([CommonHelper isCSV:path]) {
        thumbnail = [UIImage imageNamed:@"excel-thumb"];
        
    } else if ([CommonHelper isPDF:path]) {
        thumbnail = [UIImage imageNamed:@"pdf-thumb"];
    }
    
    cell.ivThumbnail.image = thumbnail;
}

#pragma Mark - UITableViewDelegate, UITableViewDataSource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FilesCell *cell = (FilesCell *)[tableView dequeueReusableCellWithIdentifier:@"FilesCell"];
    if (cell == nil) {
        cell = (FilesCell *)[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FilesCell"];
    }
    
    cell.btnShare.tag = indexPath.row;
    cell.btnTrash.tag = indexPath.row;
    
    [cell.btnShare addTarget:self action:@selector(shareFile:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnTrash addTarget:self action:@selector(trashFile:) forControlEvents:UIControlEventTouchUpInside];
    
    [self configureCell:cell filePath:_files[indexPath.row]];
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _files.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString *path = _files[indexPath.row];
    _previewItem = [NSURL fileURLWithPath:path];
    
    QLPreviewController *previewConntroller = [[QLPreviewController alloc] init];
    previewConntroller.dataSource = self;
    [self presentViewController:previewConntroller animated:YES completion:nil];
}

#pragma Mark - MFMailComposeViewControllerDelegate
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    [controller dismissViewControllerAnimated:YES completion:nil];
}

#pragma Mark - QLPreviewConntrollerDataSource
- (NSInteger)numberOfPreviewItemsInPreviewController:(QLPreviewController *)controller {
    return 1;
}

- (id<QLPreviewItem>)previewController:(QLPreviewController *)controller previewItemAtIndex:(NSInteger)index {
    return self.previewItem;
}
@end

