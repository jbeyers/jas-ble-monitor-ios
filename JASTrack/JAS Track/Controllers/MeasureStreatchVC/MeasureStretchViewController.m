//
//  MeasureStretchViewController.m
//  JAS
//
//  Created by Joost Visser on 2/12/16.
//  Copyright © 2016 Bonutti Technologies Inc. All rights reserved.
//

#import "MeasureStretchViewController.h"
#import "GaugeView.h"
#import <CoreMotion/CoreMotion.h>

enum MEASURESTATE {
    IDLE,
    INMEASURING,
    DONE,
};

@interface MeasureStretchViewController ()
@property (weak, nonatomic) IBOutlet GaugeView *gaugeView;
@property (weak, nonatomic) IBOutlet UIView *buttonView;
@property (weak, nonatomic) IBOutlet UIButton *btnButton;
@property (weak, nonatomic) IBOutlet UILabel *lblButtonTitle;

@property (nonatomic, strong) CMMotionManager* motionManager;
@property (nonatomic, strong) CADisplayLink* motionDisplayLink;

@property (assign) enum MEASURESTATE state;
@property (strong, nonatomic) NSNumber *lastAngle;
@property (strong, nonatomic) NSNumber *measureStartAngle;
@property (strong, nonatomic) NSNumber *measureEndAngle;
@end

@implementation MeasureStretchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initModel];
    [self initView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self startMotion];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self stopMotion];
}


#pragma mark Initialize
- (void)initView {
    // Button white background and circle
    _btnButton.layer.cornerRadius = _btnButton.frame.size.width / 2.f;
    _btnButton.layer.masksToBounds = YES;
    
    UIImage *image;
    UIColor *color = [UIColor whiteColor];
    
    CGRect rect = CGRectMake(0, 0, 100, 100);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [_btnButton setBackgroundImage:image forState:UIControlStateNormal];
}

- (void)updateUI {
    self.view.backgroundColor = [UIColor colorWithRed:230.0/255 green:230.0/255 blue:230.0/255 alpha:1.0];
    
    if (@available(iOS 12.0, *)) {
       if (self.traitCollection.userInterfaceStyle == UIUserInterfaceStyleDark) { //Dark Mode
           self.view.backgroundColor = [UIColor blackColor];
       } else { // Light Mode
          self.view.backgroundColor = [UIColor colorWithRed:230.0/255 green:230.0/255 blue:230.0/255 alpha:1.0];
       }
    }
}

- (void)initModel {
    _state = IDLE;
    
    _lastAngle = nil;
    _measureStartAngle = nil;
    _measureEndAngle = nil;
}

#pragma mark Actions
- (IBAction)back:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)measureStretchButton:(id)sender {
    switch (_state) {
        case IDLE: // go to measuring
            _lblButtonTitle.text = @"Take";
            _lblButtonTitle.textColor = [UIColor colorWithRed:0.94f green:0.15f blue:0.19f alpha:1.0f];
            
            _state = INMEASURING;
            
            _measureStartAngle = _lastAngle;
            break;
        case INMEASURING: // go to done
            _lblButtonTitle.text = @"Refresh";
            _lblButtonTitle.textColor = [UIColor colorWithRed:0.25f green:0.53f blue:0.98f alpha:1.0f];
            
            _state = DONE;
            
            _measureEndAngle = _lastAngle;
            
            float angle = [_measureEndAngle doubleValue] - [_measureStartAngle doubleValue];
            angle = (angle < 0)? angle + 2 * M_PI: angle;
            [_gaugeView refreshWithAngle:[NSNumber numberWithDouble:angle]];
            
            [self stopMotion];
            
            break;
        case DONE: // go to start
            _lblButtonTitle.text = @"Start";
            _lblButtonTitle.textColor = [UIColor colorWithRed:0.22f green:0.78f blue:0.29f alpha:1.0f];
            
            _state = IDLE;
            
            _lastAngle = nil;
            _measureStartAngle = nil;
            _measureEndAngle = nil;
            
            [self startMotion];
            
            break;
        default:
            break;
    }
}

#pragma mark CoreMotion
- (void)motionRefresh:(id)sender {
    double x = _motionManager.deviceMotion.gravity.x;
    double y = _motionManager.deviceMotion.gravity.y;
    
    if (x == 0.f && y == 0.f) {
        return;
    }
    
    double rotation = atan2(_motionManager.deviceMotion.gravity.x, _motionManager.deviceMotion.gravity.y) + M_PI;
    _lastAngle = [NSNumber numberWithDouble:rotation];
    
    switch (_state) {
        case IDLE: {
            [_gaugeView refreshWithAngle:_lastAngle];
        }
            break;
        case INMEASURING: {
            float angle = [_lastAngle doubleValue] - [_measureStartAngle doubleValue];
            angle = (angle < 0)? angle + 2 * M_PI: angle;
            [_gaugeView refreshWithAngle:[NSNumber numberWithDouble:angle]];
        }
            break;
        case DONE:
            
            break;
        default:
            break;
    }
}

- (void) startMotion {
    _motionManager = [[CMMotionManager alloc] init];
    _motionManager.deviceMotionUpdateInterval = 0.02;  // 50 Hz
    
    _motionDisplayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(motionRefresh:)];
    [_motionDisplayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    
    if ([_motionManager isDeviceMotionAvailable]) {
        [_motionManager startDeviceMotionUpdates];
    }
}

- (void) stopMotion {
    if (_motionDisplayLink != nil) {
        [_motionDisplayLink invalidate];
        _motionDisplayLink = nil;
    }
    if ([_motionManager isDeviceMotionAvailable]) {
        [_motionManager stopDeviceMotionUpdates];
        _motionManager = nil;
    }
}

@end
