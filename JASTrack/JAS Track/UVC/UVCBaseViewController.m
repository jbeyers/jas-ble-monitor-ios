//
//  UVCBaseViewController.m
//  UVC Cleaner
//
//  Created by crane on 15.04.2020.
//  Copyright © 2020 Yuriy. All rights reserved.
//

#import "UVCBaseViewController.h"
#import "UsageData.h"

#define BM77SPP_HW @"353035305f535050"
#define BM77SPP_FW @"32303233303230"
#define BM77SPP_AD 0xd97e

#define BLETR_HW @"353035305f424c455452"
#define BLETR_FW @"32303032303130"
#define BLETR_AD 0x4833

@import Charts;

@interface UVCBaseViewController () <MyPeripheralDelegate, ReliableBurstDataDelegate, CBControllerDelegate, ChartViewDelegate>
@property (nonatomic, retain) NSString *hardwareRevision;
@property (nonatomic, retain) NSString *firmwareRevision;
@property (nonatomic, retain) ChartYAxis *leftAxis;
@property (nonatomic, retain) NSMutableArray *itemsMA;
@end

@implementation UVCBaseViewController {
    BOOL isPatch;
}

@synthesize connectedPeripheral;
@synthesize itemsMA;

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    [self buildChartView];
//    [self useDummyData];
}

- (void)viewDidAppear:(BOOL)animated {
    _cbController.delegate = self;

    connectedPeripheral.deviceInfoDelegate = self;
    connectedPeripheral.proprietaryDelegate = self;
    connectedPeripheral.transDataDelegate = self;

    if (isPatch) {
        [connectedPeripheral setTransDataNotification:TRUE];
    }
    else {
        [connectedPeripheral readHardwareRevision];
    }
    connectedPeripheral.transmit.delegate = self;
}

- (void)sendTransparentData:(NSData *)data {
    double delayInSeconds = 0.3;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self->connectedPeripheral sendTransparentData:data type:CBCharacteristicWriteWithResponse];
    });
}

- (void)sendUVCCameraTurnOn:(BOOL) isTurnOn {
    NSData *responseData = [NSString getDataFromHexString: isTurnOn ? UVC_TREAT_ON : UVC_TREAT_OFF];
    if (responseData != nil) {
        [self sendTransparentData:responseData];
    }
}

#pragma Mark - MyPeripheralDelegate
- (void)MyPeripheral:(MyPeripheral *)peripheral didReceiveTransparentData:(NSData *)data {

}

- (void)MyPeripheral:(MyPeripheral *)peripheral didSendTransparentDataStatus:(NSError *)error {
    NSLog(@"DidSendTransparentDataStatus");
}

- (void)MyPeripheral:(MyPeripheral *)peripheral didUpdateTransDataNotifyStatus:(BOOL)notify{
    NSLog(@"DidUpdateTransDataNotifyStatus = %@",notify==true?@"true":@"false");
}

#pragma Mark - CBControllerDelegate
- (void)CBController:(CBController *)cbController didDisconnectedPeripheral:(MyPeripheral *)myPeripheral {
    NSLog(@"didDisconnectedPeripheral");
}

- (void)MyPeripheral:(MyPeripheral *)peripheral didUpdateHardwareRevision:(NSString *)hardwareRevision error:(NSError *)error {
    if (!error) {
        NSLog(@"Hardware = %@",hardwareRevision);
        
        _hardwareRevision = hardwareRevision;
        
        if ([hardwareRevision hasPrefix:[NSString stringFromHexString:BM77SPP_HW]]) {
            //BM77SPP
            [connectedPeripheral readFirmwareRevision];
        }
        else if ([hardwareRevision hasPrefix:[NSString stringFromHexString:BLETR_HW]]) {
            //BLETR
            [connectedPeripheral readFirmwareRevision];
        }
        else {
            isPatch = YES;
            [connectedPeripheral setTransDataNotification:TRUE];
        }
    }
    
}

- (void)MyPeripheral:(MyPeripheral *)peripheral didUpdateFirmwareRevision:(NSString *)firmwareRevision error:(NSError *)error {
    if (!error) {
        NSLog(@"firmwareRevision = %@",firmwareRevision);
        _firmwareRevision = firmwareRevision;
        if ([firmwareRevision hasPrefix:[NSString stringFromHexString:BM77SPP_FW]]) {
            //BM77SPP
            [connectedPeripheral readMemoryValue:BM77SPP_AD length:2];
        }
        else if ([firmwareRevision hasPrefix:[NSString stringFromHexString:BLETR_FW]]) {
            //BLETR
            [connectedPeripheral readMemoryValue:BLETR_AD length:2];
        }
        else {
            isPatch = YES;
            [connectedPeripheral setTransDataNotification:TRUE];
        }
    }
    
}

- (void)MyPeripheral:(MyPeripheral *)peripheral didReceiveMemoryAddress:(NSData *)address length:(short)length data:(NSData *)data {
    NSLog(@"%@ = %@",address,data);
    
    unsigned short int add;
    [address getBytes:&add length:2];
    add = NSSwapBigShortToHost(add);
    unsigned short int d;
    [data getBytes:&d length:length];
    d = NSSwapBigShortToHost(d);
    if ([_hardwareRevision hasPrefix:[NSString stringFromHexString:BM77SPP_HW]] && [_firmwareRevision hasPrefix:[NSString stringFromHexString:BM77SPP_FW]] && add == BM77SPP_AD) {
        if (d != 0x0017) {
            char w[2];
            w[0] = 0x00;
            w[1] = 0x17;
            [connectedPeripheral writeMemoryValue:BM77SPP_AD length:2 data:w];
        }
        else {
            isPatch = YES;
            [connectedPeripheral setTransDataNotification:TRUE];
        }
    }
    else if ([_hardwareRevision hasPrefix:[NSString stringFromHexString:BLETR_HW]] && [_firmwareRevision hasPrefix:[NSString stringFromHexString:BLETR_FW]] && add == BLETR_AD) {
        if (d != 0x17) {
            char w[2];
            w[0] = 0x00;
            w[1] = 0x17;
            [connectedPeripheral writeMemoryValue:BLETR_AD length:2 data:w];
        }
        else {
            isPatch = YES;
            [connectedPeripheral setTransDataNotification:TRUE];
        }
    }
}

- (void)MyPeripheral:(MyPeripheral *)peripheral didWriteMemoryAddress:(NSError *)error {
    if (!error) {
        isPatch = YES;
        [connectedPeripheral setTransDataNotification:TRUE];
    }
}

- (BarChartView *)getBarChatView {
    return nil;
}

- (void)buildChartView {
    BarChartView * _chartView = [self getBarChatView];
    if (_chartView == nil) {
        return;
    }
    
    _chartView.delegate = self;
    
    _chartView.drawValueAboveBarEnabled = YES;
    _chartView.drawBarShadowEnabled = NO;
    _chartView.highlightFullBarEnabled = NO;
    _chartView.highlightPerTapEnabled = NO;
    
    _chartView.chartDescription.enabled = NO;
    
    _chartView.drawGridBackgroundEnabled = NO;
    
    _chartView.dragEnabled = NO;
    [_chartView setScaleEnabled:NO];
    _chartView.pinchZoomEnabled = NO;
    
    ChartXAxis *xAxis = _chartView.xAxis;
    xAxis.labelPosition = XAxisLabelPositionBothSided;
    xAxis.drawAxisLineEnabled = YES;
    xAxis.drawGridLinesEnabled = NO;
    xAxis.drawLabelsEnabled = NO;
    
    NSNumberFormatter *leftAxisFormatter = [[NSNumberFormatter alloc] init];
    leftAxisFormatter.minimumFractionDigits = 0;
    leftAxisFormatter.maximumFractionDigits = 1;
    leftAxisFormatter.negativeSuffix = @"";
    leftAxisFormatter.positiveSuffix = @"";
    
    ChartYAxis *leftAxis = _chartView.leftAxis;
    leftAxis.labelFont = [UIFont systemFontOfSize:9.f];
    leftAxis.labelCount = 5;
    leftAxis.labelTextColor = UIColor.grayColor;
    
    leftAxis.valueFormatter = [[ChartDefaultAxisValueFormatter alloc] initWithFormatter:leftAxisFormatter];
    leftAxis.labelPosition = YAxisLabelPositionOutsideChart;
    leftAxis.spaceTop = 0.0;
    leftAxis.axisMinimum = 0.0;
    leftAxis.axisMaximum = 1.0;
    
    self.leftAxis = leftAxis;
    
    ChartYAxis *rightAxis = _chartView.rightAxis;
    rightAxis.enabled = YES;
    rightAxis.drawAxisLineEnabled = YES;
    rightAxis.drawGridLinesEnabled = NO;
    rightAxis.drawLabelsEnabled = NO;
    
    ChartLegend *l = _chartView.legend;
    
    l.horizontalAlignment = ChartLegendHorizontalAlignmentLeft;
    l.verticalAlignment = ChartLegendVerticalAlignmentBottom;
    l.orientation = ChartLegendOrientationHorizontal;
    l.drawInside = YES;
    l.form = ChartLegendFormSquare;
    l.formSize = 0.0;
    l.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:12.f];
    l.textColor = UIColor.grayColor;

    l.xEntrySpace = 0.0;
}

- (void)updateChartData
{
    [self setDataCount:1.0];
}

- (void)setDataCount:(double)minRange
{
    BarChartView * _chartView = [self getBarChatView];
    if (_chartView == nil) {
        return;
    }
    
    NSMutableArray *yVals = [[NSMutableArray alloc] init];
    NSInteger count = itemsMA.count;
    
    double maxYAxis = minRange;
    
    for (NSInteger i = 0; i < count; i++)
    {
        UsageData *uData = [itemsMA objectAtIndex:i];
        double val = 0;
        val = uData.rom;
        
        maxYAxis = MAX(val, maxYAxis);
        
        [yVals addObject:[[BarChartDataEntry alloc] initWithX:i y:val]];
    }
    
    if (maxYAxis > minRange) {
        [self.leftAxis setAxisMaximum:maxYAxis];
        [self.leftAxis resetCustomAxisMax];
    }
    
    BarChartDataSet *set1 = nil;
    if (_chartView.data.dataSetCount > 0)
    {
        set1 = (BarChartDataSet *)_chartView.data.dataSets[0];
        set1.values = yVals;
        [_chartView.data notifyDataChanged];
        [_chartView notifyDataSetChanged];
    }
    else
    {
        set1 = [[BarChartDataSet alloc] initWithValues:yVals label:@""];
        [set1 setColors:@[[UIColor colorWithRed:6/255.f green:115/255.f blue:199/255.f alpha:1.f]]];
        set1.drawIconsEnabled = NO;
        set1.drawValuesEnabled = NO;
        
        NSMutableArray *dataSets = [[NSMutableArray alloc] init];
        [dataSets addObject:set1];
        
        BarChartData *data = [[BarChartData alloc] initWithDataSets:dataSets];
        data.barWidth = 0.9f;
        
        _chartView.data = data;
    }
}


- (void)useDummyData {
    itemsMA = [[NSMutableArray alloc] init];
//    [itemsMA removeAllObjects];
    
    UsageData *data = [UsageData new];
    data.month = 6;
    data.day = 1;
    data.rotation1 = 40;
    data.rotation2 = 88;
    data.rom = 12;
    [itemsMA addObject:data];
    
    data = [UsageData new];
    data.month = 6;
    data.day = 2;
    data.rotation1 = 49;
    data.rotation2 = 18;
    data.rom = 22;
    [itemsMA addObject:data];
    
    data = [UsageData new];
    data.month = 6;
    data.day = 3;
    data.rotation1 = 2;
    data.rotation2 = 68;
    data.rom = 90;
    [itemsMA addObject:data];
    
    data = [UsageData new];
    data.month = 6;
    data.day = 4;
    data.rotation1 = 120;
    data.rotation2 = 68;
    data.rom = 23;
    [itemsMA addObject:data];
    
    data = [UsageData new];
    data.month = 6;
    data.day = 5;
    data.rotation1 = 90;
    data.rotation2 = 61;
    data.rom = 67;
    [itemsMA addObject:data];
    
    data = [UsageData new];
    data.month = 6;
    data.day = 6;
    data.rotation1 = 100;
    data.rotation2 = 21;
    data.rom = 11;
    [itemsMA addObject:data];
    
    data = [UsageData new];
    data.month = 6;
    data.day = 7;
    data.rotation1 = 88;
    data.rotation2 = 65;
    data.rom = 56;
    [itemsMA addObject:data];
    
    data = [UsageData new];
    data.month = 6;
    data.day = 6;
    data.rotation1 = 30;
    data.rotation2 = 5;
    data.rom = 90;
    [itemsMA addObject:data];
    
    data = [UsageData new];
    data.month = 6;
    data.day = 8;
    data.rotation1 = 100;
    data.rotation2 = 21;
    data.rom = 100;
    [itemsMA addObject:data];
    
    data = [UsageData new];
    data.month = 6;
    data.day = 9;
    data.rotation1 = 150;
    data.rotation2 = 80;
    data.rom = 2;
    [itemsMA addObject:data];
    
    data = [UsageData new];
    data.month = 6;
    data.day = 10;
    data.rotation1 = 50;
    data.rotation2 = 51;
    data.rom = 87;
    [itemsMA addObject:data];
    
    [self updateChartData];
}
@end
