//
//  UVCViewController.swift
//  UVC Cleaner
//
//  Created by crane on 12.04.2020.
//  Copyright © 2020 Yuriy. All rights reserved.
//

import UIKit
import AVFoundation
import Vision

class UVCViewController: UVCBaseViewController, AVCaptureVideoDataOutputSampleBufferDelegate {
    var bufferSize: CGSize = .zero
    var rootLayer: CALayer! = nil
    var isCleanModeSelected: Bool = false
    private var isTurnedOn: Bool?
    
    enum CleanMode: String {
        case deepClean = "Deep Clean"
        case quickClean = "Quick Clean"
        case manual = "Manual"
    }
    
    @IBOutlet weak var ivCide: UIImageView!
    @IBOutlet weak var lblTreatCompleted: UILabel!
    @IBOutlet weak var btnClean: UIButton!
    @IBOutlet weak var lblDetermineActive: UILabel!
    @IBOutlet weak private var previewView: UIView!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblDisinfectionLevel: UILabel!
    @IBOutlet weak var graphView: UIView!
    @IBOutlet weak var treatBtnView: UIView!
    @IBOutlet weak var treatBtnLabel: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    
    private let session = AVCaptureSession()
    private var previewLayer: AVCaptureVideoPreviewLayer! = nil
    private let videoDataOutput = AVCaptureVideoDataOutput()
    
    let START_DISINFECTION = CGFloat(88)
    var currentCleanMode: CleanMode = .deepClean
    var disinfectionLevel: CGFloat = 0
    var timer: Timer?
    var isTreating: Bool = false
    
    private let videoDataOutputQueue = DispatchQueue(label: "VideoDataOutput", qos: .userInitiated, attributes: [], autoreleaseFrequency: .workItem)
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        // to be implemented in the subclass
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupAVCapture()
        
        configure()
    }
        
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        previewLayer.frame = rootLayer.bounds
    }
    
    private func configure() {
        let longTapRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(recognizer:)))
        treatBtnView.addGestureRecognizer(longTapRecognizer)
        
        treatBtnLabel.textColor = .white
        self.lblTreatCompleted.isHidden = true
        
        lblDistance.text = "Distance To Scanner: 2.5\""
                
        lblDetermineActive.isHidden = true
        setDisinfectionLevel(level: 0)
        updateCleanButtonUi()
        updateTreatButton()
    }
    
    @objc func handleLongPress(recognizer: UIGestureRecognizer) {
        switch recognizer.state {
        case .began:
            isTreating = true
            lblDetermineActive.isHidden = false
            updateTreatButton()
            startTimer()
        case .ended:
            isTreating = false
            lblDetermineActive.isHidden = true
            updateTreatButton()
            reset()
        default:
            break
        }
    }
    
    private func startTimer() {
        disinfectionLevel = CGFloat(START_DISINFECTION)
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateDisinfection), userInfo: nil, repeats: true)
    }
    
    @objc func updateDisinfection() {
        if isTreating, let isTurnedOn = isTurnedOn, isTurnedOn == true {
            disinfectionLevel += 1

            if disinfectionLevel >= 100 {
                disinfectionLevel = 99.9
                completeTreat()
                endTimer()
            }
            
            setDisinfectionLevel(level: disinfectionLevel)
        }
    }
    
    private func endTimer() {
        timer?.invalidate()
        timer = nil
    }
    
    private func reset() {
        self.lblTreatCompleted.isHidden = true
        setDisinfectionLevel(level: 0)
        endTimer()
    }
    
    private func updateTreatButton() {
        treatBtnView.layer.borderColor = UIColor.white.cgColor
        treatBtnView.layer.borderWidth = 4
        treatBtnView.layer.cornerRadius = 16
        
        treatBtnView.backgroundColor = isTreating ? UIColor(red: 1.0/255, green: 45.0/255, blue: 105.0/255, alpha: 1.0) : UIColor(red: 5.0/255, green: 67.0/255, blue: 124.0/255, alpha: 1.0)
        treatBtnLabel.text = (isTreating ? "Release to stop treatment" : "PRESS AND HOLD TO TREAT").uppercased()
    }
    
    private func setDisinfectionLevel(level: CGFloat) {  // level must be percent value
        lblDisinfectionLevel.text = String.init(format: "Estimated Disinfection Level: %.01f%", level)
        
        progressView.setProgress(Float(level / 100), animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func handleObjectConfidence(_ confidence: CGFloat) {
        var turnedOn: Bool = false
        
        if confidence > 0.75 {
            lblDetermineActive.text = "UVC Paused"
            
            turnedOn = false
            
        } else {
            lblDetermineActive.text = "UVC Active"
            
            turnedOn = true
        }
        
        if let isTurnedOn = isTurnedOn {
            if isTurnedOn != turnedOn {
                self.isTurnedOn = turnedOn
                self.sendUVCCameraTurn(on: turnedOn)
            }
        } else {
            self.isTurnedOn = turnedOn
            self.sendUVCCameraTurn(on: turnedOn)
        }
    }
    
    func setupAVCapture() {
        var deviceInput: AVCaptureDeviceInput!
        
        // Select a video device, make an input
        let videoDevice = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera], mediaType: .video, position: .back).devices.first
        do {
            deviceInput = try AVCaptureDeviceInput(device: videoDevice!)
        } catch {
            print("Could not create video device input: \(error)")
            return
        }
        
        session.beginConfiguration()
        session.sessionPreset = .vga640x480 // Model image size is smaller.
        
        // Add a video input
        guard session.canAddInput(deviceInput) else {
            print("Could not add video device input to the session")
            session.commitConfiguration()
            return
        }
        session.addInput(deviceInput)
        if session.canAddOutput(videoDataOutput) {
            session.addOutput(videoDataOutput)
            // Add a video data output
            videoDataOutput.alwaysDiscardsLateVideoFrames = true
            videoDataOutput.videoSettings = [kCVPixelBufferPixelFormatTypeKey as String: Int(kCVPixelFormatType_420YpCbCr8BiPlanarFullRange)]
            videoDataOutput.setSampleBufferDelegate(self, queue: videoDataOutputQueue)
        } else {
            print("Could not add video data output to the session")
            session.commitConfiguration()
            return
        }
        let captureConnection = videoDataOutput.connection(with: .video)
        // Always process the frames
        captureConnection?.isEnabled = true
        do {
            try  videoDevice!.lockForConfiguration()
            let dimensions = CMVideoFormatDescriptionGetDimensions((videoDevice?.activeFormat.formatDescription)!)
            bufferSize.width = CGFloat(dimensions.width)
            bufferSize.height = CGFloat(dimensions.height)
            videoDevice!.unlockForConfiguration()
        } catch {
            print(error)
        }
        session.commitConfiguration()
        previewLayer = AVCaptureVideoPreviewLayer(session: session)
        previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        rootLayer = previewView.layer
        previewLayer.frame = rootLayer.bounds
        rootLayer.addSublayer(previewLayer)
    }
    
    @IBAction func onQuickCleanAction(_ sender: UIButton) {
        if currentCleanMode == .deepClean {
            currentCleanMode = .quickClean
        } else if currentCleanMode == .quickClean {
            currentCleanMode = .manual
        } else {
            currentCleanMode = .deepClean
        }
        
        updateCleanButtonUi()
    }
    
    @IBAction func onCancel(_ sender: Any) {
        navigationController?.popViewController(animated: false)
    }
    
    func updateCleanButtonUi() {
        btnClean.setTitle(currentCleanMode.rawValue, for: .normal)
    }
    
    func completeTreat() {
        DispatchQueue.main.async {
            self.lblDetermineActive.isHidden = true
            self.lblTreatCompleted.isHidden = false
//            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//                self.lblTreatCompleted.isHidden = true
//            }
        }
    }
    
    // MARK: - Camera Configuration
    func startCaptureSession() {
        session.startRunning()
    }
    
    // Clean up capture setup
    func teardownAVCapture() {
        previewLayer.removeFromSuperlayer()
        previewLayer = nil
    }
    
    func captureOutput(_ captureOutput: AVCaptureOutput, didDrop didDropSampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        // print("frame dropped")
    }
    
    public func exifOrientationFromDeviceOrientation() -> CGImagePropertyOrientation {
        let curDeviceOrientation = UIDevice.current.orientation
        let exifOrientation: CGImagePropertyOrientation
        
        switch curDeviceOrientation {
        case UIDeviceOrientation.portraitUpsideDown:  // Device oriented vertically, home button on the top
            exifOrientation = .left
        case UIDeviceOrientation.landscapeLeft:       // Device oriented horizontally, home button on the right
            exifOrientation = .upMirrored
        case UIDeviceOrientation.landscapeRight:      // Device oriented horizontally, home button on the left
            exifOrientation = .down
        case UIDeviceOrientation.portrait:            // Device oriented vertically, home button on the bottom
            exifOrientation = .up
        default:
            exifOrientation = .up
        }
        return exifOrientation
    }
}
