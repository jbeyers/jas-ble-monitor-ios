//
//  UVCARCameraViewController.swift
//  UVC Cleaner
//
//  Created by crane on 21.04.2020.
//  Copyright © 2020 Yuriy. All rights reserved.
//

import UIKit
import ARKit
import Vision

class UVCARCameraViewController: UVCBaseViewController, ARSCNViewDelegate, ARSessionDelegate {

    var isCleanModeSelected: Bool = false
    var isDistanceMode: Bool = false
    private var isTurnedOn: Bool?
    var bufferSize: CGSize = .zero
    var rootLayer: CALayer! = nil
    
    // planes
    var dictPlanes = [ARPlaneAnchor: Plane]()
    // start node
    var startNode: SCNNode?
    private var detectionOverlay: CALayer! = nil
    
    // Vision parts
    private var requests = [VNRequest]()
    
    // distance label
    @IBOutlet weak var lblMeasurementDetails : UILabel!
    @IBOutlet weak var btnClean: UIButton!
    @IBOutlet weak var lblDetermineActive: UILabel!
    @IBOutlet weak private var previewView: UIView!
    @IBOutlet weak var sceneView: ARSCNView!
    @IBOutlet weak var centerDotView: UIView!
    @IBOutlet weak var btnDistanceMode: UIButton!
    var currentBuffer: CVPixelBuffer?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        centerDotView.layer.cornerRadius = 10
        centerDotView.clipsToBounds = true
        lblMeasurementDetails.textAlignment = .center
        
        // setup scene
        self.setupScene()
        rootLayer = sceneView.layer
//        previewLayer.frame = rootLayer.bounds
//        rootLayer.addSublayer(previewLayer)
        
        // setup Vision parts
//        setupLayers()
//        updateLayerGeometry()
//        setupVision()
    }
  
    @IBAction func onQuickCleanAction(_ sender: UIButton) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "VisionObjectRecognitionViewController") as? VisionObjectRecognitionViewController {
            vc.connectedPeripheral = self.connectedPeripheral
            vc.cbController = self.cbController
            
            navigationController?.pushViewController(vc, animated: false)
        }
//        isCleanModeSelected = !isCleanModeSelected
//
//        updateCleanButtonUi()
    }
    
    @IBAction func onViewModeChangedAction(_ sender: Any) {
    }
    
    
    private func updateCleanButtonUi() {
        btnClean.setTitle(isCleanModeSelected ? "Deep Clean Mode" : "Quick Clean Mode", for: .normal)
    }
    
    func setupScene()
    {
        // set delegate - ARSCNViewDelegate
        self.sceneView.delegate = self

        self.sceneView.autoenablesDefaultLighting = true
        // debug points
//        self.sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints, ARSCNDebugOptions.showWorldOrigin]
        
        // create new scene
        let scene = SCNScene()
        self.sceneView.scene = scene
        
        sceneView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(viewDidTap(_:))))
    }
    
    @objc func viewDidTap(_ gesture: UITapGestureRecognizer) {

        let touchPoint = gesture.location(in: self.sceneView)
        
        
        if let position = self.doHitTestOnExistingPlanes(touchPoint) {
            // add node at hit-position
            let node = self.nodeWithPosition(position)
            if let start = startNode {
                start.removeFromParentNode()
            }
            
            sceneView.scene.rootNode.addChildNode(node)
            
            // set start node
            startNode = node
        }
    }
    
    func doHitTestOnExistingPlanes(_ point: CGPoint) -> SCNVector3? {
        // hit-test of view's center with existing-planes
        let results = sceneView.hitTest(point,
                                        types: .existingPlaneUsingExtent)
        // check if result is available
        if let result = results.first {
            // get vector from transform
            let hitPos = self.positionFromTransform(result.worldTransform)
            return hitPos
        }
        return nil
    }
    
    // get position 'vector' from 'transform'
    func positionFromTransform(_ transform: matrix_float4x4) -> SCNVector3 {
        return SCNVector3Make(transform.columns.3.x,
                              transform.columns.3.y,
                              transform.columns.3.z)
    }
    
    // add dot node with given position
    func nodeWithPosition(_ position: SCNVector3) -> SCNNode {
        // create sphere geometry with radius
        let sphere = SCNSphere(radius: 0.003)
        // set color
        sphere.firstMaterial?.diffuse.contents = UIColor(red: 255/255.0,
                                                         green: 153/255.0,
                                                         blue: 83/255.0,
                                                         alpha: 1)
        // set lighting model
        sphere.firstMaterial?.lightingModel = .constant
        sphere.firstMaterial?.isDoubleSided = true
        // create node with 'sphere' geometry
        let node = SCNNode(geometry: sphere)
        node.position = position
        
        return node
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // set up session
        self.setupARSession()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Pause the view's session
        sceneView.session.pause()
    }
    
    // setup AR Session
    func setupARSession()
    {
        let configuration = ARWorldTrackingConfiguration()
        // set to detect horizontal planes
        configuration.planeDetection = .horizontal
        configuration.isAutoFocusEnabled = true
        // run the configuration
        self.sceneView.session.delegate = self
        self.sceneView.session.run(configuration)
    }

    // Vision
    @discardableResult
    func setupVision() -> NSError? {
        // Setup Vision parts
        let error: NSError! = nil
        
        guard let modelURL = Bundle.main.url(forResource: "YOLOv3", withExtension: "mlmodelc") else {
            return NSError(domain: "VisionObjectRecognitionViewController", code: -1, userInfo: [NSLocalizedDescriptionKey: "Model file is missing"])
        }
        do {
            let visionModel = try VNCoreMLModel(for: MLModel(contentsOf: modelURL))
            let objectRecognition = VNCoreMLRequest(model: visionModel, completionHandler: { (request, error) in
                DispatchQueue.main.async(execute: {
                    // perform all the UI updates on the main queue
                    if let results = request.results {
                        self.drawVisionRequestResults(results)
                    }
                })
            })
            self.requests = [objectRecognition]
        } catch let error as NSError {
            print("Model loading went wrong: \(error)")
        }
        
        return error
    }
    
    func drawVisionRequestResults(_ results: [Any]) {
        CATransaction.begin()
        CATransaction.setValue(kCFBooleanTrue, forKey: kCATransactionDisableActions)
        detectionOverlay.sublayers = nil // remove all the old recognized objects
        for observation in results where observation is VNRecognizedObjectObservation {
            guard let objectObservation = observation as? VNRecognizedObjectObservation else {
                continue
            }
            // Select only the label with the highest confidence.
            let topLabelObservation = objectObservation.labels[0]
            if topLabelObservation.identifier == "person" {
                let objectBounds = VNImageRectForNormalizedRect(objectObservation.boundingBox, Int(bufferSize.width), Int(bufferSize.height))
                
                NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: "objectConfidence"), object: nil, userInfo: ["confidence": Float(topLabelObservation.confidence)])
                
                let shapeLayer = self.createRoundedRectLayerWithBounds(objectBounds)
                
                let textLayer = self.createTextSubLayerInBounds(objectBounds,
                                                                identifier: topLabelObservation.identifier,
                                                                confidence: topLabelObservation.confidence)
                shapeLayer.addSublayer(textLayer)
                detectionOverlay.addSublayer(shapeLayer)
            } else {
                NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: "objectConfidence"), object: nil, userInfo: ["confidence": 0])
            }
        }
        
        if results.count == 0 {
            NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: "objectConfidence"), object: nil, userInfo: ["confidence": 0])
        }
        
        self.updateLayerGeometry()
        CATransaction.commit()
    }
    
    func setupLayers() {
        detectionOverlay = CALayer() // container layer that has all the renderings of the observations
        detectionOverlay.name = "DetectionOverlay"
        detectionOverlay.bounds = CGRect(x: 0.0,
                                         y: 0.0,
                                         width: bufferSize.width,
                                         height: bufferSize.height)
        detectionOverlay.position = CGPoint(x: rootLayer.bounds.midX, y: rootLayer.bounds.midY)
        rootLayer.addSublayer(detectionOverlay)
    }
    
    func updateLayerGeometry() {
        let bounds = rootLayer.bounds
        var scale: CGFloat
        
        let xScale: CGFloat = bounds.size.width / bufferSize.height
        let yScale: CGFloat = bounds.size.height / bufferSize.width
        
        scale = fmax(xScale, yScale)
        if scale.isInfinite {
            scale = 1.0
        }
        CATransaction.begin()
        CATransaction.setValue(kCFBooleanTrue, forKey: kCATransactionDisableActions)
        
        // rotate the layer into screen orientation and scale and mirror
        detectionOverlay.setAffineTransform(CGAffineTransform(rotationAngle: CGFloat(.pi / 2.0)).scaledBy(x: scale, y: -scale))
        // center the layer
        detectionOverlay.position = CGPoint(x: bounds.midX, y: bounds.midY)
        
        CATransaction.commit()
        
    }
    
    func createTextSubLayerInBounds(_ bounds: CGRect, identifier: String, confidence: VNConfidence) -> CATextLayer {
        let textLayer = CATextLayer()
        textLayer.name = "Object Label"
        let formattedString = NSMutableAttributedString(string: String(format: "\(identifier)\nConfidence:  %d", Int(confidence * 100)))
        let largeFont = UIFont(name: "Helvetica", size: 24.0)!
        formattedString.addAttributes([NSAttributedString.Key.font: largeFont], range: NSRange(location: 0, length: identifier.count))
        textLayer.string = formattedString
        textLayer.bounds = CGRect(x: 0, y: 0, width: bounds.size.height - 10, height: bounds.size.width - 10)
        textLayer.position = CGPoint(x: bounds.midX, y: bounds.midY)
        textLayer.shadowOpacity = 0.7
        textLayer.shadowOffset = CGSize(width: 2, height: 2)
        textLayer.foregroundColor = CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: [0.0, 0.0, 0.0, 1.0])
        textLayer.contentsScale = 2.0 // retina rendering
        // rotate the layer into screen orientation and scale and mirror
        textLayer.setAffineTransform(CGAffineTransform(rotationAngle: CGFloat(.pi / 2.0)).scaledBy(x: 1.0, y: -1.0))
        return textLayer
    }
    
    func createRoundedRectLayerWithBounds(_ bounds: CGRect) -> CALayer {
        let shapeLayer = CALayer()
        shapeLayer.bounds = bounds
        shapeLayer.position = CGPoint(x: bounds.midX, y: bounds.midY)
        shapeLayer.name = "Found Object"
        shapeLayer.backgroundColor = CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: [1.0, 1.0, 1.0, 0.4])
        shapeLayer.cornerRadius = 7
        return shapeLayer
    }
    
    private let visionQueue = DispatchQueue(label: "com.viseo.ARML.visionqueue")
    // MARK: - ARSessionDelegate
    func session(_ session: ARSession, didUpdate frame: ARFrame) {
        currentBuffer = frame.capturedImage

        
        // We return early if currentBuffer is not nil or the tracking state of camera is not normal
//        guard let buffer = currentBuffer, case .normal = frame.camera.trackingState else {
//            return
//        }
//
//        let requestHandler = VNImageRequestHandler(cvPixelBuffer: buffer, orientation: .right)
//
//        // We perform our CoreML Requests asynchronously.
//        visionQueue.async {
//            // Run our CoreML Request
//            do {
//                try requestHandler.perform(self.requests)
//            } catch {
//                print(error)
//            }
//        }
        
    }
    
    public func exifOrientationFromDeviceOrientation() -> CGImagePropertyOrientation {
//        let curDeviceOrientation = UIDevice.current.orientation
//        let exifOrientation: CGImagePropertyOrientation
//
//        switch curDeviceOrientation {
//        case UIDeviceOrientation.portraitUpsideDown:  // Device oriented vertically, home button on the top
//            exifOrientation = .left
//        case UIDeviceOrientation.landscapeLeft:       // Device oriented horizontally, home button on the right
//            exifOrientation = .upMirrored
//        case UIDeviceOrientation.landscapeRight:      // Device oriented horizontally, home button on the left
//            exifOrientation = .down
//        case UIDeviceOrientation.portrait:            // Device oriented vertically, home button on the bottom
//            exifOrientation = .up
//        default:
//            exifOrientation = .up
//        }
//        return exifOrientation
        
        return .up
    }
    
    // MARK: - ARSCNViewDelegate
    
    // line-node
    var line_node: SCNNode?
    
    // renderer callback method
    func renderer(_ renderer: SCNSceneRenderer,
                  updateAtTime time: TimeInterval) {

        DispatchQueue.main.async {
            // get current hit position
            // and check if start-node is available
            guard let camera = self.sceneView.pointOfView?.worldPosition,
                let start = self.startNode, let pointOfView = self.sceneView.pointOfView else {
                return
            }
            
            
            let mat = pointOfView.transform
            let dir = SCNVector3(-1 * mat.m31 * 0.1, -1 * mat.m32 * 0.1, -1 * mat.m33 * 0.1)
            let currentPosition = pointOfView.position + (dir * 0.1)
            
            self.line_node?.removeFromParentNode()
            
            let line = self.lineFrom(vector: start.position, toVector: currentPosition)
            let lineNode = SCNNode(geometry: line)
            lineNode.geometry?.firstMaterial?.diffuse.contents = UIColor.red
            self.sceneView.scene.rootNode.addChildNode(lineNode)
            self.line_node = lineNode
            
            // distance-string
            let desc = self.getDistanceStringBeween(pos1: camera,
                                                    pos2: start.position)
            
            DispatchQueue.main.async {
                self.lblMeasurementDetails.text = desc
            }
        }
    }
    
    func renderer(_ renderer: SCNSceneRenderer, willRenderScene scene: SCNScene, atTime time: TimeInterval){
//            glLineWidth(10)
    }
    
    // draw line-node between two vectors
    func getDrawnLineFrom(pos1: SCNVector3,
                          toPos2: SCNVector3) -> SCNNode {
        
        let line = lineFrom(vector: pos1, toVector: toPos2)
        let lineInBetween1 = SCNNode(geometry: line)
        return lineInBetween1
    }
    
    // get line geometry between two vectors
    func lineFrom(vector vector1: SCNVector3,
                  toVector vector2: SCNVector3) -> SCNGeometry {
        
        let indices: [Int32] = [0, 1]
        let source = SCNGeometrySource(vertices: [vector1, vector2])
        let element = SCNGeometryElement(indices: indices,
                                         primitiveType: .line)
        return SCNGeometry(sources: [source], elements: [element])
    }
    
    /**
     Distance string
     */
    func getDistanceStringBeween(pos1: SCNVector3?,
                                 pos2: SCNVector3?) -> String {
        
        if pos1 == nil || pos2 == nil {
            return "0"
        }
        let d = self.distanceBetweenPoints(A: pos1!, B: pos2!)
        
        var result = ""
//
//        let meter = stringValue(v: Float(d), unit: "meters")
//        result.append(meter)
//        result.append("\n")
//
//        let f = self.foot_fromMeter(m: Float(d))
//        let feet = stringValue(v: Float(f), unit: "feet")
//        result.append(feet)
//        result.append("\n")
//
//        let inch = self.Inch_fromMeter(m: Float(d))
//        let inches = stringValue(v: Float(inch), unit: "inch")
//        result.append(inches)
//        result.append("\n")
//
        let cm = self.CM_fromMeter(m: Float(d))
        let cms = stringValue(v: Float(cm), unit: "cm")
        result.append(cms)
        
        return result
    }
    
    /**
     Distance between 2 points
     */
    func distanceBetweenPoints(A: SCNVector3, B: SCNVector3) -> CGFloat {
        let l = sqrt(
            (A.x - B.x) * (A.x - B.x)
                +   (A.y - B.y) * (A.y - B.y)
                +   (A.z - B.z) * (A.z - B.z)
        )
        return CGFloat(l)
    }
    
    /**
     String with float value and unit
     */
    func stringValue(v: Float, unit: String) -> String {
        let s = String(format: "%.1f %@", v, unit)
        return s
    }
    
    /**
     Inch from meter
     */
    func Inch_fromMeter(m: Float) -> Float {
        let v = m * 39.3701
        return v
    }
    
    /**
     centimeter from meter
     */
    func CM_fromMeter(m: Float) -> Float {
        let v = m * 100.0
        return v
    }
    
    /**
     feet from meter
     */
    func foot_fromMeter(m: Float) -> Float {
        let v = m * 3.28084
        return v
    }
    
    /**
     Called when a new node has been mapped to the given anchor.
     */
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        print("--> did add node")
        
        DispatchQueue.main.async {
            if let planeAnchor = anchor as? ARPlaneAnchor {

                // create plane with the "PlaneAnchor"
                let plane = Plane(anchor: planeAnchor)
                // add to the detected
                node.addChildNode(plane)
                // add to dictionary
                self.dictPlanes[planeAnchor] = plane
            }
        }
    }
    
    /**
     Called when a node will be updated with data from the given anchor.
     */
    func renderer(_ renderer: SCNSceneRenderer, willUpdate node: SCNNode, for anchor: ARAnchor) { }
    
    /**
     Called when a node has been updated with data from the given anchor.
     */
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        DispatchQueue.main.async {
            if let planeAnchor = anchor as? ARPlaneAnchor {
                // get plane with anchor
                let plane = self.dictPlanes[planeAnchor]
                // update
                plane?.updateWith(planeAnchor)
            }
        }
    }
    
    /**
     Called when a mapped node has been removed from the scene graph for the given anchor.
     */
    func renderer(_ renderer: SCNSceneRenderer, didRemove node: SCNNode, for anchor: ARAnchor) {        
        if let planeAnchor = anchor as? ARPlaneAnchor {
            self.dictPlanes.removeValue(forKey: planeAnchor)
        }
    }
}
