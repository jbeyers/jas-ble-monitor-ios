//
//  HandDetector.swift
//  UVC Cleaner
//
//  Created by crane on 22.04.2020.
//  Copyright © 2020 Yuriy. All rights reserved.
//

import CoreML
import Vision

public class HandDetector {
    // MARK: - Variables
    private let visionQueue = DispatchQueue(label: "com.viseo.ARML.visionqueue")

    // MARK: - Public functions
    public func performDetection(predictionRequest: VNCoreMLRequest, inputBuffer: CVPixelBuffer, completion: @escaping (_ outputBuffer: CVPixelBuffer?, _ error: Error?) -> Void) {
        // Right orientation because the pixel data for image captured by an iOS device is encoded in the camera sensor's native landscape orientation
        let requestHandler = VNImageRequestHandler(cvPixelBuffer: inputBuffer, orientation: .right)

        // We perform our CoreML Requests asynchronously.
        visionQueue.async {
            // Run our CoreML Request
            do {
                try requestHandler.perform([predictionRequest])
               
                guard let observation = predictionRequest.results?.first as? VNPixelBufferObservation else {
                    completion(nil, nil)
                    return
                }

                // The resulting image (mask) is available as observation.pixelBuffer
                completion(observation.pixelBuffer, nil)
            } catch {
                completion(nil, error)
            }
        }
    }
}
