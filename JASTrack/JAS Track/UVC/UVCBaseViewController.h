//
//  UVCBaseViewController.h
//  UVC Cleaner
//
//  Created by crane on 15.04.2020.
//  Copyright © 2020 Yuriy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "CBController.h"
#import "NSData+Conversion.h"
#import "DeviceInfo.h"
#import "CommonHelper.h"
#import "NSString+Extension.h"

NS_ASSUME_NONNULL_BEGIN
@import Charts;

@interface UVCBaseViewController : BaseViewController
@property (nonatomic, retain)MyPeripheral *connectedPeripheral;
@property (nonatomic, retain) CBController *cbController;

- (void)sendTransparentData:(NSData *)data;
- (void)sendUVCCameraTurnOn:(BOOL) isTurnOn;
- (BarChartView *)getBarChatView ;
@end

NS_ASSUME_NONNULL_END
