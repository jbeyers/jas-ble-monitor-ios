//
//  CommonHelper.h
//  JAS Track
//
//  Created by Crane on 3/1/19.
//  Copyright © 2019 Yuriy. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CommonHelper : NSObject
+ (NSString *)stringWithDate:(NSDate *) date;
+ (CGSize) sizeWithString:(NSString *)string withFont:(UIFont *)font;
+ (NSString *)timestampToDate:(double)timestamp;
+ (NSString *)timestampToTime:(double)timestamp;
+ (NSString *)extentionFromPath:(NSString *)path;
+ (BOOL)isCSV:(NSString *)path;
+ (BOOL)isPDF:(NSString *)path;
+ (BOOL)isOnlyNumbers:(NSString *)string;
+ (BOOL)isOnlyLetters:(NSString *)string;
+(void)applySketchShadow:(CALayer *)layer
                   color:(UIColor*) color
                   alpha:(CGFloat) alpha
                       x: (CGFloat) x
                       y: (CGFloat) y
                    blur: (CGFloat) blur
                  spread: (CGFloat) spread;
@end

NS_ASSUME_NONNULL_END
