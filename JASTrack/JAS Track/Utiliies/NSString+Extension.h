//
//  NSString+Extension.h
//  JAS Track
//
//  Created by Crane on 3/1/19.
//  Copyright © 2019 Yuriy. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (NSString_Extension)
+ (NSString *)stringFromHexString:(NSString *)hexString;
+ (NSMutableData *)getDataFromHexString:(NSString*)command;
@end

NS_ASSUME_NONNULL_END
