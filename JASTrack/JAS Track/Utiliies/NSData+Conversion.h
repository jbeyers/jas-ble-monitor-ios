//
//  NSData+Conversion.h
//  JAS Track
//
//  Created by Crane on 2/26/19.
//  Copyright © 2019 Yuriy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (NSData_Conversion)

#pragma mark - String Conversion
- (NSString *)hexadecimalString;

@end
