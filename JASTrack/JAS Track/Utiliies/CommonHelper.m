//
//  CommonHelper.m
//  JAS Track
//
//  Created by Crane on 3/1/19.
//  Copyright © 2019 Yuriy. All rights reserved.
//

#import "CommonHelper.h"

@implementation CommonHelper
+ (NSString *)stringWithDate:(NSDate *) date {
    return [NSDateFormatter localizedStringFromDate:date dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterShortStyle];
}

+ (CGSize) sizeWithString:(NSString *)string withFont:(UIFont *)font {
    return [string sizeWithAttributes:@{NSFontAttributeName: font}];
}

+ (NSString *)timestampToDate:(double)timestamp {
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timestamp];
    return [NSDateFormatter localizedStringFromDate:date dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterNoStyle];
}

+ (NSString *)timestampToTime:(double)timestamp {
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timestamp];
    return [NSDateFormatter localizedStringFromDate:date dateStyle:NSDateFormatterNoStyle timeStyle:NSDateFormatterShortStyle];
}

+ (NSString *)extentionFromPath:(NSString *)path {
    return [path pathExtension];
}

+ (BOOL)isCSV:(NSString *)path {
    NSString *extension = [path pathExtension];
    if (extension != nil && [extension isEqualToString:@"csv"]) {
        return true;
    }
    
    return false;
}

+(void)applySketchShadow:(CALayer *)layer
                   color:(UIColor*) color
                   alpha:(CGFloat) alpha
                       x: (CGFloat) x
                       y: (CGFloat) y
                    blur: (CGFloat) blur
                  spread: (CGFloat) spread {
   layer.shadowColor = color.CGColor;
   layer.shadowOpacity = alpha;
   layer.shadowOffset = CGSizeMake(x, y);
   layer.shadowRadius = blur / 2.0;
   
   if (spread == 0) {
      layer.shadowPath = nil;
   } else {
      CGFloat dx = -spread;
      CGRect rect = CGRectInset(layer.bounds, dx, dx);
      layer.shadowPath = [UIBezierPath bezierPathWithRect:rect].CGPath;
   }
}

+ (BOOL)isPDF:(NSString *)path {
    NSString *extension = [path pathExtension];
    if (extension != nil && [extension isEqualToString:@"pdf"]) {
        return true;
    }
    
    return false;
}

+ (BOOL)isOnlyNumbers:(NSString *)string {
    NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    if ([string rangeOfCharacterFromSet:notDigits].location == NSNotFound)
    {
        // newString consists only of the digits 0 through 9
        return true;
    }
    
    return false;
}

+ (BOOL)isOnlyLetters:(NSString *)string {
    //Create character set
    NSCharacterSet *validChars = [NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"];
    
    //Invert the set
    validChars = [validChars invertedSet];
    
    //Check against that
    NSRange  range = [string rangeOfCharacterFromSet:validChars];
    if (NSNotFound != range.location) {
        
        //invalid chars found
        return false;
    }
    
    return true;
}
@end
