//
//  GaugeView.m
//  JAS
//
//  Created by Joost Visser on 2/12/16.
//  Copyright © 2016 Bonutti Technologies Inc. All rights reserved.
//

#import "GaugeView.h"

@interface GaugeView()
@property (strong, nonatomic) CATextLayer *textLayer1;
@property (strong, nonatomic) CATextLayer *textLayer2;
@property (strong, nonatomic) CAShapeLayer *needleLayer;
@property (strong, nonatomic) NSNumber *angle;

@property (assign) float centerX;
@property (assign) float centerY;
@property (assign) float radius;
@property (assign) CGRect fullRect;
@property (assign) float ellipseLineWidth;
@property (assign) float divisionLineWidth;
@property (assign) float divisionLineLong;
@property (strong, nonatomic) NSString *fontFamily;
@property (assign) float divisionFontSize;
@property (assign) float crossLineWidth;
@property (assign) float needleLineWidth;
@property (assign) float text1FontSize;
@property (assign) float text2FontSize;
@end

@implementation GaugeView

-(void)awakeFromNib {
    [super awakeFromNib];
    [self initModel];
    [self initView];
}

#pragma mark Initialize
- (void)initModel {
    _angle = [NSNumber numberWithDouble: 0];
}

- (void)initView {
    // gauge view round corner
    [self layoutIfNeeded];
    self.layer.cornerRadius = self.frame.size.width / 2.0f;
    self.layer.masksToBounds = YES;
    
    // draw Background
    _centerX = self.bounds.size.width / 2.0;
    _centerY = self.bounds.size.height / 2.0;
    _radius = fmin(self.frame.size.width, self.frame.size.height) / 2;
    _fullRect = CGRectMake(_centerX - _radius, _centerY - _radius, 2 * _radius, 2 * _radius);
    
    _ellipseLineWidth = 2.f;
    _divisionLineWidth = 1.f;
    _divisionLineLong = 4.f;
    _fontFamily = @"ArialMT";
    _divisionFontSize = 15.f;
    _crossLineWidth = 0.3f;
    _needleLineWidth = 1.5f;
    _text1FontSize = 28.f;
    _text2FontSize = 22.f;
    
    // ellipse
    {
        CAShapeLayer *shapeLayer = [CAShapeLayer layer];
        shapeLayer.path = [[UIBezierPath bezierPathWithOvalInRect:_fullRect] CGPath];
        shapeLayer.lineWidth = _ellipseLineWidth;
        shapeLayer.strokeColor = [[UIColor blackColor] CGColor];
        shapeLayer.fillColor = [[UIColor whiteColor] CGColor];
        [self.layer addSublayer:shapeLayer];
    }
    
    // divisions
    {
        CAShapeLayer *shapeLayer = [CAShapeLayer layer];
        shapeLayer.lineWidth = _divisionLineWidth;
        shapeLayer.strokeColor = [[UIColor blackColor] CGColor];
        
        UIBezierPath *path = [UIBezierPath bezierPath];
        int totalTicks = 36;
        for (int i = 0; i < totalTicks; i++) {
            float x1 = _radius - _divisionLineLong;
            float y1 = 0;
            
            float x2= _radius;
            float y2 = 0;
            
            float angle = 2 * M_PI / totalTicks * i;
            
            float x1Rotated = x1*cos(angle) - y1*sin(angle);
            float y1Rotated = x1*sin(angle) + y1*cos(angle);
            
            float x2Rotated = x2*cos(angle) - y2*sin(angle);
            float y2Rotated = x2*sin(angle) + y2*cos(angle);
            
            [path moveToPoint:CGPointMake(x1Rotated + _centerX, y1Rotated + _centerY)];
            [path addLineToPoint:CGPointMake(x2Rotated + _centerX, y2Rotated + _centerY)];
        }
        [path closePath];
        
        shapeLayer.path = [path CGPath];
        
        [self.layer addSublayer:shapeLayer];
    }
    
    // cross line
    {
        CAShapeLayer *shapeLayer = [CAShapeLayer layer];
        shapeLayer.lineWidth = _crossLineWidth;
        shapeLayer.strokeColor = [[UIColor grayColor] CGColor];
        
        UIBezierPath *path = [UIBezierPath bezierPath];
        
        [path moveToPoint:CGPointMake(_centerX - _radius, _centerY)];
        [path addLineToPoint:CGPointMake(_centerX + _radius, _centerY)];
        
        [path moveToPoint:CGPointMake(_centerX, _centerY - _radius)];
        [path addLineToPoint:CGPointMake(_centerX, _centerY + _radius)];
        
        [path closePath];
        
        shapeLayer.path = [path CGPath];
        
        [self.layer addSublayer:shapeLayer];
    }
    
    // division Labels
    {
        {
            NSString *text = [NSString stringWithFormat:@"0%@", @"\u00B0"];
            CGSize size = [text sizeWithAttributes:@{NSFontAttributeName:[UIFont fontWithName:_fontFamily size:_divisionFontSize]}];
            CGRect rc = CGRectMake(_centerX + _radius - _divisionLineLong - size.width, _centerY - size.height/2, size.width, size.height);
            
            CATextLayer *label = [[CATextLayer alloc] init];
            [label setFont:(__bridge CFTypeRef _Nullable)(_fontFamily)];
            [label setFontSize:_divisionFontSize];
            [label setFrame:rc];
            [label setString:text];
            [label setForegroundColor:[[UIColor blackColor] CGColor]];
            [self.layer addSublayer:label];
        }
        
        {
            NSString *text = [NSString stringWithFormat:@"90%@", @"\u00B0"];
            CGSize size = [text sizeWithAttributes:@{NSFontAttributeName:[UIFont fontWithName:_fontFamily size:_divisionFontSize]}];
            CGRect rc = CGRectMake(_centerX - size.width/2, _centerY - _radius + _divisionLineLong, size.width, size.height);
            
            CATextLayer *label = [[CATextLayer alloc] init];
            [label setFont:(__bridge CFTypeRef _Nullable)(_fontFamily)];
            [label setFontSize:_divisionFontSize];
            [label setFrame:rc];
            [label setString:text];
            [label setForegroundColor:[[UIColor blackColor] CGColor]];
            [self.layer addSublayer:label];
        }
        
        {
            NSString *text = [NSString stringWithFormat:@"180%@", @"\u00B0"];
            CGSize size = [text sizeWithAttributes:@{NSFontAttributeName:[UIFont fontWithName:_fontFamily size:_divisionFontSize]}];
            CGRect rc = CGRectMake(_centerX - _radius + _divisionLineLong, _centerY - size.height/2, size.width, size.height);
            
            CATextLayer *label = [[CATextLayer alloc] init];
            [label setFont:(__bridge CFTypeRef _Nullable)(_fontFamily)];
            [label setFontSize:_divisionFontSize];
            [label setFrame:rc];
            [label setString:text];
            [label setForegroundColor:[[UIColor blackColor] CGColor]];
            [self.layer addSublayer:label];
        }
        
        {
            NSString *text = [NSString stringWithFormat:@"270%@", @"\u00B0"];
            CGSize size = [text sizeWithAttributes:@{NSFontAttributeName:[UIFont fontWithName:_fontFamily size:_divisionFontSize]}];
            CGRect rc = CGRectMake(_centerX - size.width/2, _centerY + _radius - _divisionLineLong - size.height, size.width, size.height);
            
            CATextLayer *label = [[CATextLayer alloc] init];
            [label setFont:(__bridge CFTypeRef _Nullable)(_fontFamily)];
            [label setFontSize:_divisionFontSize];
            [label setFrame:rc];
            [label setString:text];
            [label setForegroundColor:[[UIColor blackColor] CGColor]];
            [self.layer addSublayer:label];
        }
    }

    // draw needle with angle
    {
        _needleLayer = [CAShapeLayer layer];
        _needleLayer.lineWidth = _needleLineWidth;
        _needleLayer.strokeColor = [[UIColor blackColor] CGColor];
        [self.layer addSublayer:_needleLayer];
        
        [self drawNeedleWithAngle:_angle];
    }
    
    // draw degree label
    {
        {
            CGRect rc = CGRectMake(_centerX - 50, _centerY  - 30, 100, 30);
            
            _textLayer1 = [[CATextLayer alloc] init];
            [_textLayer1 setFont:(__bridge CFTypeRef _Nullable)(_fontFamily)];
            [_textLayer1 setFontSize:_text1FontSize];
            [_textLayer1 setFrame:rc];
            [_textLayer1 setAlignmentMode:kCAAlignmentCenter];
            [_textLayer1 setForegroundColor:[[UIColor blackColor] CGColor]];
            [self.layer addSublayer:_textLayer1];
            
            [self drawText1WithAngle:_angle];
        }
        
        {
            CGRect rc = CGRectMake(_centerX - 50, _centerY, 100, 30);
            
            _textLayer2 = [[CATextLayer alloc] init];
            [_textLayer2 setFont:(__bridge CFTypeRef _Nullable)(_fontFamily)];
            [_textLayer2 setFontSize:_text2FontSize];
            [_textLayer2 setFrame:rc];
            [_textLayer2 setAlignmentMode:kCAAlignmentCenter];
            [_textLayer2 setForegroundColor:[[UIColor blackColor] CGColor]];
            [self.layer addSublayer:_textLayer2];
            
            [self drawText2WithAngle:_angle];
        }
    }
}

- (void)drawNeedleWithAngle:(NSNumber *)angle {
    float angleD = [angle doubleValue];
    angleD = -angleD;
    
    float x2= _radius;
    float y2 = 0;
    
    float x2Rotated = x2*cos(angleD) - y2*sin(angleD);
    float y2Rotated = x2*sin(angleD) + y2*cos(angleD);
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    [path moveToPoint:CGPointMake(_centerX, _centerY)];
    [path addLineToPoint:CGPointMake(_centerX + _radius, _centerY)];
    
    [path moveToPoint:CGPointMake(_centerX, _centerY)];
    [path addLineToPoint:CGPointMake(_centerX + x2Rotated, _centerY + y2Rotated)];
    
    [path closePath];
    
    _needleLayer.path = [path CGPath];
}

- (void)drawText1WithAngle:(NSNumber *)angle {
    float angleD = [angle doubleValue];
    int degree = angleD / (2 * M_PI) * 360;
    NSString *text = [NSString stringWithFormat:@"%d%@", degree, @"\u00B0"];
    [_textLayer1 setString:text];
}

- (void)drawText2WithAngle:(NSNumber *)angle {
    float angleD = [angle doubleValue];
    int degree = angleD / (2 * M_PI) * 360;
    degree = 360 - degree;
    NSString *text = [NSString stringWithFormat:@"%d%@", degree, @"\u00B0"];
    [_textLayer2 setString:text];
}

- (void)refreshWithAngle:(NSNumber *)angle {
    _angle = angle;

    [self drawNeedleWithAngle:angle];
    [self drawText1WithAngle:angle];
    [self drawText2WithAngle:angle];
}

@end
