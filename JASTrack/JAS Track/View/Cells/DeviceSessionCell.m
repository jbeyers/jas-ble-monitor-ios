//
//  DeviceSessionCell.m
//  JAS Track
//
//  Created by Yuriy on 2/15/19.
//  Copyright © 2019 Yuriy. All rights reserved.
//

#import "DeviceSessionCell.h"

@implementation DeviceSessionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)displaySessionStatus:(NSMutableArray *)sessionInfos value:(DeviceInfo *)info {
    NSLayoutYAxisAnchor *topAnchor = _sessionContentView.topAnchor;
    for (NSDictionary *rowD in sessionInfos) {
        UILabel *lblSession = [UILabel new];
        lblSession.translatesAutoresizingMaskIntoConstraints = false;
        lblSession.font = [UIFont systemFontOfSize:14.0f];
        [_sessionContentView addSubview:lblSession];
        
        SessionStatus rowValue = [rowD[@"row-value"] integerValue];
        NSString *sValue = @"";
        
        switch (rowValue) {
            case SessionStatusComplete:
                sValue = info.completeSession ? @"YES" : @"NO";
                break;
            case SessionStatusStretch:
                sValue = info.detectedStretch ? @"YES" : @"NO";
                break;
            case SessionStatusHold:
                sValue = info.detectedHold ? @"YES" : @"NO";
                break;
            default:
                break;
        }
        
        lblSession.text = [NSString stringWithFormat:@"%@: %@", rowD[@"row-title"], sValue];
        
        [NSLayoutConstraint activateConstraints:@[
                                                  [lblSession.topAnchor constraintEqualToAnchor:topAnchor constant:8.0f],
                                                  [lblSession.leadingAnchor constraintEqualToAnchor:_sessionContentView.leadingAnchor constant:16.0f],
                                                  [lblSession.trailingAnchor constraintEqualToAnchor:_sessionContentView.trailingAnchor constant:-16.0f],
                                                  [lblSession.heightAnchor constraintGreaterThanOrEqualToConstant:14.0f]
                                                  ]];
        
        topAnchor = lblSession.bottomAnchor;
    }
    
    [NSLayoutConstraint activateConstraints:@[
                                              [topAnchor constraintEqualToAnchor: _sessionContentView.bottomAnchor constant:-8.0f]
                                              ]];
    
    _sessionContentView.layer.cornerRadius = 8.0f;
    _sessionContentView.layer.borderColor = [UIColor grayColor].CGColor;
    _sessionContentView.layer.borderWidth = 1.0;
    
    [self layoutIfNeeded];
}
@end
