//
//  SessionCell.h
//  JAS Track
//
//  Created by Crane on 2/19/19.
//  Copyright © 2019 Yuriy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ZMJGanttChart/ZMJGanttChart.h>

@interface SessionCell : ZMJCell
@property (nonatomic, strong) UILabel *label;
@end

@interface SessionHeaderCell : ZMJCell
@property (nonatomic, strong) UILabel *label;
@end
