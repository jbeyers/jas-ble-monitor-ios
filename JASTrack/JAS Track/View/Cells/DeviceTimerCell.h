//
//  DeviceTimerCell.h
//  JAS Track
//
//  Created by Yuriy on 2/15/19.
//  Copyright © 2019 Yuriy. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DeviceTimerCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;

@end

NS_ASSUME_NONNULL_END
