//
//  SessionCell.m
//  JAS Track
//
//  Created by Crane on 2/19/19.
//  Copyright © 2019 Yuriy. All rights reserved.
//

#import "SessionCell.h"
#import <ZMJGanttChart/ZMJGanttChart.h>

@implementation SessionCell: ZMJCell

- (UILabel *)label {
    if (!_label) {
        _label = [UILabel new];
    }
    return _label;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.label.frame = self.bounds;
        self.label.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        self.label.font = [UIFont systemFontOfSize:12.0f];
        self.label.textAlignment = NSTextAlignmentCenter;
        
        [self.contentView addSubview:self.label];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        
    }
    return self;
}

@end

@implementation SessionHeaderCell: ZMJCell

- (UILabel *)label {
    if (!_label) {
        _label = [UILabel new];
    }
    return _label;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithWhite:0.95f alpha:1.0];
        
        if (@available(iOS 12.0, *)) {
           if (self.traitCollection.userInterfaceStyle == UIUserInterfaceStyleDark) { //Dark Mode
               self.backgroundColor = [UIColor colorWithWhite:0.95f alpha:0.3];
           }
        }
        
        self.label.frame = self.bounds;
        self.label.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        self.label.font = [UIFont boldSystemFontOfSize:10.0f];
        self.label.textAlignment = NSTextAlignmentCenter;
        self.label.numberOfLines = 0;
        
        [self.contentView addSubview:self.label];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.label.frame = self.bounds;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        
    }
    return self;
}

@end
