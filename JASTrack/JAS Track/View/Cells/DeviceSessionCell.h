//
//  DeviceSessionCell.h
//  JAS Track
//
//  Created by Yuriy on 2/15/19.
//  Copyright © 2019 Yuriy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DeviceInfo.h"

NS_ASSUME_NONNULL_BEGIN

@interface DeviceSessionCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIView *sessionContentView;
- (void)displaySessionStatus:(NSMutableArray *)sessionInfos value:(DeviceInfo *)info;
@end

NS_ASSUME_NONNULL_END
