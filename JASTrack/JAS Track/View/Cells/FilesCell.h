//
//  FilesCell.h
//  JAS Track
//
//  Created by Crane on 6/14/19.
//  Copyright © 2019 Yuriy. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FilesCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *ivThumbnail;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblType;
@property (weak, nonatomic) IBOutlet UIButton *btnShare;
@property (weak, nonatomic) IBOutlet UIButton *btnTrash;

@end

NS_ASSUME_NONNULL_END
