//
//  DeviceInfoCell.h
//  JAS Track
//
//  Created by Yuriy on 2/15/19.
//  Copyright © 2019 Yuriy. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DeviceInfoCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblDetailsInfo;

@end

NS_ASSUME_NONNULL_END
