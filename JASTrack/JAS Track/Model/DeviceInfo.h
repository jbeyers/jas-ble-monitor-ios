//
//  DeviceInfo.h
//  JAS Track
//
//  Created by Yuriy on 2/29/19.
//  Copyright © 2019 Yuriy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CBController.h"

typedef enum : NSUInteger {
    DeviceInfoTypeBattery = 0,
    DeviceInfoTypeTemperature,
    DeviceInfoTypeRotation,
    DeviceInfoTypeProximity1,
    DeviceInfoTypeProximity2,
    DeviceInfoTypeDate,
    DeviceInfoTypeTime,
    DeviceInfoTypeStretches,
    DeviceInfoTypeHoldTimer
} DeviceInfoType;

typedef enum : NSUInteger {
    SessionStatusComplete = 0,
    SessionStatusStretch,
    SessionStatusHold
} SessionStatus;

@interface DeviceInfo : NSObject

+ (DeviceInfo *)sharedInstance;
@property(retain) MyPeripheral *myPeripheral;

//Properties
///Device Info
@property CGFloat batteryVoltage;
@property NSInteger temperature;
@property NSInteger rotation;
@property BOOL proximity1;
@property BOOL proximity2;
@property NSUInteger stretches;
@property NSUInteger holdTimeSeconds;
///Session Status
@property BOOL detectedStretch;
@property BOOL detectedHold;
@property BOOL completeSession;
///Date Time
@property NSInteger year;
@property NSInteger month;
@property NSInteger day;
@property NSInteger hour;
@property NSInteger minute;
@property NSInteger second;
@property NSMutableArray *sessionStatusMA;
@property NSString *serialNumber;
@property NSInteger firmwareRevision;
@property NSInteger receivedACKsCount;
@property NSInteger droppedPacketsCount;
@property NSInteger validPacketsCount;
@property NSInteger sentPacketsCount;
@property NSInteger sentACKsCount;

//APIs
- (NSString *)getDisplayHoldTime;
- (NSString*)getDisplayDateString;
- (NSString*)getDisplayTimeString;
- (NSString*)getProduct;
- (double)getGearingRatio;
- (NSString *)getRotationOne;
- (NSString *)getRotationTwo;
- (BOOL) isBetaFeature ;
@end
