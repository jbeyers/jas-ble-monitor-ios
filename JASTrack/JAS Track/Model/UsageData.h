//
//  UsageData.h
//  JAS Track
//
//  Created by Yuriy on 3/4/19.
//  Copyright © 2019 Yuriy. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UsageData : NSObject
@property int year;
@property int month;

@property int day;@property int rotation1;
@property int rotation2;
@property int rom;

@end

NS_ASSUME_NONNULL_END
