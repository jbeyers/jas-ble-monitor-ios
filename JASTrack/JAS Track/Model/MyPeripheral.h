//
//  MyPeripheral.h
//  BLETR
//
//  Created by D500 user on 13/5/30.
//  Copyright (c) 2013年 ISSC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "ReliableBurstData.h"

//JAS PACKET LENGTH
#define JAS_PACKET_STRING_LENGTH        20
#define JAS_PACKET_LENGTH               10
#define JAS_WAITING_FOR_RESPONSE_TIME   30.0  //SECONDS
#define JAS_WAITING_TIME_WITH_RETRY     3

//PACKET TYPE
#define JAS_SESSION_PACKET              0x10
#define JAS_DATA_PACKET                 0x20
#define JAS_TIME_PACKET                 0xA0
#define JAS_SERIAL_PACKET               0xB0

//COMMAND
#define JAS_COMMAND_PACKET_FORMAT       0xCC
#define JAS_COMMAND_MODE_MASK           0x0F
#define JAS_COMMAND_MASK                0xF0
#define JAS_COMMAND_STREATCH            0x08
#define JAS_COMMAND_HOLD                0x04
#define JAS_COMMAND_SESSION             0x0C
#define JAS_COMMAND_SESSION_COMPLETE    0x00
#define JAS_COMMAND_PROXIMITY_ONE       0x02
#define JAS_COMMAND_PROXIMITY_TWO       0x01
#define JAS_COMMAND_ACK                 0x30

//JAS RESPONSE
#define JAS_SETUP_COMPLETE_RESPONSE     0x50
#define JAS_DISCONNECT_RESPONSE         0x90
#define JAS_CLEAR_DATA_RESPONSE         0x80
#define JAS_COMMAND_REPONSE_FAILED      0x40
#define JAS_SN_SET_PACKET               0xC0

//Calibrate Clock
#define JAS_CALIBRATE_CLOCK_CONFIGURE   @"CCE000000000000000E0"
#define JAS_CALIBRATE_CLOCK_INCREMENT   @"CCE000000000000040E0"
#define JAS_CALIBRATE_CLOCK_DECREMENT   @"CCE000000000000020E0"
#define JAS_CALIBRATE_CLOCK_SET         @"CCE000000000000010E0"
#define JAS_CALIBRATE_CLOCK_CANCEL      @"CCE000000000000080E0"
#define JAS_CALIBRATE_CLOCK_RESPONSE    0xE0

//JAS REQUEST
#define JAS_COMMAND_ACK_REQUEST         @"cc300000000000000030"
#define JAS_COMMAND_NACK_REQUEST        @"CC400000000000000040"
#define JAS_COMMAND_CLEAR_DATA_REQUEST  @"CC800000000000000080"
#define JAS_COMMAND_GET_DATA_REQUEST    @"CC700000000000000070"
#define JAS_COMMAND_DISCONNECT_REQUEST  @"CC900000000000000090"
#define JAS_COMMAND_SET_TIME_REQUEST    @"CC500000000000000050"

#define AIR_PATCH_COMMAND_VENDOR_MP_ENABLE      0x03
#define AIR_PATCH_COMMAND_XMEMOTY_READ          0x08
#define AIR_PATCH_COMMAND_XMEMOTY_WRITE         0x09
#define AIR_PATCH_COMMAND_E2PROM_READ           0x0a
#define AIR_PATCH_COMMAND_E2PROM_WRITE          0x0b
#define AIR_PATCH_COMMAND_READ_HW_VERSION       0x12
#define AIR_PATCH_COMMAND_READ_FW_VERSION       0x16
#define AIR_PATCH_COMMAND_READ                  0x24

//UVC Cleaner
#define UVC_TREAT_ON                        @"ccF100000000000000F1"
#define UVC_TREAT_OFF                       @"ccF000000000000000F0"

#define AIR_PATCH_SUCCESS   0x00
#define AIR_PATCH_INVALID_PARAMETERS      0x02

#define AIR_PATCH_ACTION_CHANGE_DEVICE_NAME     0X01
#define AIR_PATCH_ACTION_READ_ADVERTISE_DATA1   0X02
#define AIR_PATCH_ACTION_READ_ADVERTISE_DATA2   0X03
#define AIR_PATCH_ACTION_UPDATE_ADVERTISE_DATA  0X04
#define AIR_PATCH_ACTION_CHANGE_DEVICE_NAME_MEMORY     0X05
#define AIR_PATCH_ACTION_READ                   0X24

#define ADVERTISE_DATA_TYPE_COMPLETE_DEVICE_NAME 0X09
#define ADVERTISE_DATA_TYPE_SHORTEN_DEVICE_NAME  0X08

enum {
    MYPERIPHERAL_CONNECT_STATUS_IDLE = 0,
    MYPERIPHERAL_CONNECT_STATUS_CONNECTING,
    MYPERIPHERAL_CONNECT_STATUS_CONNECTED,
};

enum {
    UPDATE_PARAMETERS_STEP_PREPARE = 0,
    UPDATE_PARAMETERS_STEP_CHECK_RESULT,
};
typedef struct _AIR_PATCH_COMMAND_FORMAT
{
    unsigned char commandID;
    char parameters[19];
}__attribute__((packed)) AIR_PATCH_COMMAND_FORMAT;

typedef struct _AIR_PATCH_EVENT_FORMAT
{
    char    status;
    unsigned char commandID;
    char parameters[16];
}__attribute__((packed)) AIR_PATCH_EVENT_FORMAT;


typedef struct _WRITE_EEPROM_COMMAND_FORMAT
{
    unsigned char addr[2];
    unsigned char length;
    char    data[16];
}__attribute__((packed)) WRITE_EEPROM_COMMAND_FORMAT;

typedef struct _CONNECTION_PARAMETER_FORMAT
{
    unsigned char status;
    unsigned short minInterval;
    unsigned short maxInterval;
    unsigned short latency;
    unsigned short connectionTimeout;
}__attribute__((packed)) CONNECTION_PARAMETER_FORMAT;


@protocol MyPeripheralDelegate;
@interface MyPeripheral : NSObject
{
@private
    char    advData[25];
    char    deviceName[16];
    NSMutableArray *queuedTask;
    short   deviceNameMemoryAddr;
}
@property(assign) id<MyPeripheralDelegate> transDataDelegate;
@property(assign) id<MyPeripheralDelegate> proprietaryDelegate;
@property(assign) id<MyPeripheralDelegate> deviceInfoDelegate;

@property (retain) CBPeripheral *peripheral;
@property(assign) uint8_t connectStatus;
@property(assign) NSNumber *oRSSI;
@property (assign) BOOL canSendData;
@property (assign) BOOL isReadAddress;
@property (assign) BOOL isReadHardVersion;
@property (readonly) NSString *uuidString;
@property(readonly) NSString *advName;
@property (readonly) NSString *deviceName;
@property (readonly) NSData *manuData;
@property (readonly) NSArray *serviceUUIDs;
@property (readonly) NSDictionary *serviceData;
@property (copy) NSDictionary *advertiseData;

//DIS
@property(retain) CBCharacteristic *manufactureNameChar;
@property(retain) CBCharacteristic *modelNumberChar;
@property(retain) CBCharacteristic *serialNumberChar;
@property(retain) CBCharacteristic *hardwareRevisionChar;
@property(retain) CBCharacteristic *firmwareRevisionChar;
@property(retain) CBCharacteristic *softwareRevisionChar;
@property(retain) CBCharacteristic *systemIDChar;
@property(retain) CBCharacteristic *certDataListChar;
@property(retain) CBCharacteristic *specificChar1;
@property(retain) CBCharacteristic *specificChar2;

//Proprietary
@property(retain) CBCharacteristic *airPatchChar;
@property(retain) CBCharacteristic *transparentDataWriteChar;
@property(retain) CBCharacteristic *transparentDataReadChar;
@property(retain) CBCharacteristic *connectionParameterChar;
@property(assign) uint8_t   updateConnectionParameterStep;
@property(readonly) ReliableBurstData *transmit;

@property(assign) CONNECTION_PARAMETER_FORMAT connectionParameter;
@property(assign) CONNECTION_PARAMETER_FORMAT backupConnectionParameter;


@property(assign) BOOL    vendorMPEnable;
@property(assign) short   airPatchAction;
@property(assign) BOOL    isNotifying;


- (CONNECTION_PARAMETER_FORMAT *)retrieveBackupConnectionParameter;
- (void)updateBackupConnectionParameter:(CONNECTION_PARAMETER_FORMAT *)parameter;
- (BOOL)compareBackupConnectionParameter:(CONNECTION_PARAMETER_FORMAT *)parameter;

- (void)checkConnectionParameterStatus;
- (void)sendVendorMPEnable;
- (void)updateAirPatchEvent: (NSData *)returnEvent;
- (void)writeE2promValue: (short)address length:(short)length data:(char *)data;
- (void)readE2promValue: (short)address length:(short)length;
- (void)writeMemoryValue: (short)address length:(short)length data:(char *)data;
- (void)readMemoryValue: (short)address length:(short)length;
- (CBCharacteristicWriteType)sendTransparentData:(NSData *)data type:(CBCharacteristicWriteType)type;
- (void)setTransDataNotification:(BOOL)notify;

- (NSError *)setMaxConnectionInterval:(unsigned short)maxInterval connectionTimeout:(unsigned short)connectionTimeout connectionLatency:(unsigned short)connectionLatency;
- (void)checkIsAllowUpdateConnectionParameter;
- (void)changePeripheralName: (NSString *)name;
- (void)readBtAddress;

- (void)readManufactureName;
- (void)readModelNumber;
- (void)readSerialNumber;
- (void)readHardwareRevision;
- (void)readFirmwareRevision;
- (void)readSoftwareRevison;
- (void)readSystemID;
- (void)readCertificationData;
- (void)readSpecificUUID1;
- (void)readSpecificUUID2;

@end


@protocol MyPeripheralDelegate<NSObject>
@optional
- (void)MyPeripheral:(MyPeripheral *)peripheral didUpdateConnectionParameterAllowStatus:(BOOL)status;
- (void)MyPeripheral:(MyPeripheral *)peripheral didUpdateConnectionParameterStatus:(BOOL)status interval:(unsigned short)interval timeout:(unsigned short)timeout  latency:(unsigned short)latency;

- (void)MyPeripheral:(MyPeripheral *)peripheral didChangePeripheralName:(NSError *)error;

- (void)MyPeripheral:(MyPeripheral *)peripheral didReceiveTransparentData:(NSData *)data;
- (void)MyPeripheral:(MyPeripheral *)peripheral didReceiveMemoryAddress:(NSData *)address length:(short)length data:(NSData *)data;
- (void)MyPeripheral:(MyPeripheral *)peripheral didReceiveBtAddress:(NSData *)data;
- (void)MyPeripheral:(MyPeripheral *)peripheral didWriteMemoryAddress:(NSError *)error;
- (void)MyPeripheral:(MyPeripheral *)peripheral didSendTransparentDataStatus:(NSError *) error;
- (void)MyPeripheral:(MyPeripheral *)peripheral didUpdateTransDataNotifyStatus:(BOOL)notify;

- (void)MyPeripheral:(MyPeripheral *)peripheral didUpdateManufactureName:(NSString *)name error:(NSError *)error;
- (void)MyPeripheral:(MyPeripheral *)peripheral didUpdateModelNumber:(NSString *)modelNumber error:(NSError *)error;

- (void)MyPeripheral:(MyPeripheral *)peripheral didUpdateSerialNumber:(NSString *)serialNumber error:(NSError *)error;
- (void)MyPeripheral:(MyPeripheral *)peripheral didUpdateHardwareRevision:(NSString *)hardwareRevision error:(NSError *)error;
- (void)MyPeripheral:(MyPeripheral *)peripheral didUpdateFirmwareRevision:(NSString *)firmwareRevision error:(NSError *)error;
- (void)MyPeripheral:(MyPeripheral *)peripheral didUpdateSoftwareRevision:(NSString *)softwareRevision error:(NSError *)error;
- (void)MyPeripheral:(MyPeripheral *)peripheral didUpdateSystemId:(NSData *)systemId error:(NSError *)error;
- (void)MyPeripheral:(MyPeripheral *)peripheral didUpdateIEEE_11073_20601:(NSData *)IEEE_11073_20601 error:(NSError *)error;
- (void)MyPeripheral:(MyPeripheral *)peripheral didUpdateSpecificUUID1:(NSData *)value error:(NSError *)error;
- (void)MyPeripheral:(MyPeripheral *)peripheral didUpdateSpecificUUID2:(NSData *)value error:(NSError *)error;
@end
