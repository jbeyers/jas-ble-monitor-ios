//
//  ActivityIndicatorHelper.m
//  JAS Track
//
//  Created by Yuriy on 2/29/19.
//  Copyright © 2019 Yuriy. All rights reserved.
//

#import "ActivityIndicatorHelper.h"
#import "MBProgressHUD.h"

@interface ActivityIndicatorHelper() {
    MBProgressHUD *hud;
}

@end

@implementation ActivityIndicatorHelper

- (id)initWithController:(UIViewController *)controller {
    self = [[ActivityIndicatorHelper alloc] init];

    hud = [[MBProgressHUD alloc] initWithView:controller.view];
    hud.label.text = @"Loading…";
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.removeFromSuperViewOnHide = NO;

    [controller.view addSubview:hud];

    self.visibility = NO;

    return self;
}

- (void)setVisibility:(BOOL)visibility {
    _visibility = visibility;

    if ( _visibility ) {
        [hud bringSubviewToFront:hud.superview];
        [hud showAnimated:NO];
    } else {
        [hud hideAnimated:YES];
    }

    [UIApplication sharedApplication].networkActivityIndicatorVisible = _visibility;
}

- (void)setLoadingText:(NSString *)text {
    hud.label.text = text;
}
@end
