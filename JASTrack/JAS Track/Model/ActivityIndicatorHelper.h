//
//  ActivityIndicatorHelper.h
//  JAS Track
//
//  Created by Yuriy on 2/29/19.
//  Copyright © 2019 Yuriy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityIndicatorHelper : NSObject
- (id)initWithController:(UIViewController *)controller;
@property (nonatomic, assign) BOOL visibility;
- (void)setLoadingText:(NSString *)text;
@end
