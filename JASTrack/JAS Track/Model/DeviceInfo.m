//
//  DeviceInfo.m
//  JAS Track
//
//  Created by Yuriy on 2/29/19.
//  Copyright © 2019 Yuriy. All rights reserved.
//
#define SNTable @[ \
@{@"product": @"JAS EZ Elbow", @"sn_prefix": @"12", @"gear_ratio": @"0.0486111111", @"left" : @{@"cw" : @"Extension", @"ccw" : @"Flexion"}, @"right" : @{@"cw" : @"Flexion", @"ccw" : @"Extension"}, @"universal" : @{@"cw" : @"", @"ccw" : @""}}, \
@{@"product": @"JAS EZ Wrist", @"sn_prefix": @"07", @"gear_ratio": @"0.0777777778", @"left" : @{@"cw" : @"Extension", @"ccw" : @"Flexion"}, @"right" : @{@"cw" : @"Flexion", @"ccw" : @"Extension"}, @"universal" : @{@"cw" : @"", @"ccw" : @""}}, \
@{@"product": @"JAS EZ Knee Extension", @"sn_prefix": @"16", @"gear_ratio": @"0.0347222222", @"left" : @{@"cw" : @"Flexion", @"ccw" : @"Extension"}, @"right" : @{@"cw" : @"Extension", @"ccw" : @"Flexion"}, @"universal" : @{@"cw" : @"", @"ccw" : @""}}, \
@{@"product": @"JAS EZ Knee Flexion", @"sn_prefix": @"19", @"gear_ratio": @"0.00361111111", @"left" : @{@"cw" : @"", @"ccw" : @""}, @"right" : @{@"cw" : @"", @"ccw" : @""}, @"universal" : @{@"cw" : @"Extension", @"ccw" : @"Flexion"}}, \
@{@"product": @"JAS EZ Pro/Sup", @"sn_prefix": @"20", @"gear_ratio": @"0.0141666667", @"left" : @{@"cw" : @"Supination", @"ccw" : @"Pronation"}, @"right" : @{@"cw" : @"Supination", @"ccw" : @"Pronation"}, @"universal" : @{@"cw" : @"", @"ccw" : @""}}, \
@{@"product": @"JAS EZ Shoulder Body", @"sn_prefix": @"25", @"gear_ratio": @"0.00805555556", @"left" : @{@"cw" : @"External Rotation", @"ccw" : @"Internal Rotation"}, @"right" : @{@"cw" : @"Internal Rotation", @"ccw" : @"External Rotation"}, @"universal" : @{@"cw" : @"", @"ccw" : @""}}, \
@{@"product": @"JAS EZ Shoulder Floor", @"sn_prefix": @"22", @"gear_ratio": @"0.00805555556", @"left" : @{@"cw" : @"External Rotation", @"ccw" : @"Internal Rotation"}, @"right" : @{@"cw" : @"Internal Rotation", @"ccw" : @"External Rotation"}, @"universal" : @{@"cw" : @"", @"ccw" : @""}}, \
@{@"product": @"JAS EZ Ankle", @"sn_prefix": @"26", @"gear_ratio": @"0.00333333333", @"left" : @{@"cw" : @"Dorsiflexion", @"ccw" : @"Plantarflexion"}, @"right" : @{@"cw" : @"Dorsiflexion", @"ccw" : @"Plantarflexion"}, @"universal" : @{@"cw" : @"", @"ccw" : @""}}, \
@{@"product": @"JAS SPS Elbow Wide", @"sn_prefix": @"00", @"gear_ratio": @"0.0177777778", @"left" : @{@"cw" : @"Extension", @"ccw" : @"Flexion"}, @"right" : @{@"cw" : @"Extension", @"ccw" : @"Flexion"}, @"universal" : @{@"cw" : @"", @"ccw" : @""}}, \
@{@"product": @"JAS SPS Elbow Narrow", @"sn_prefix": @"11", @"gear_ratio": @"0.0147222222", @"left" : @{@"cw" : @"Extension", @"ccw" : @"Flexion"}, @"right" : @{@"cw" : @"Extension", @"ccw" : @"Flexion"}, @"universal" : @{@"cw" : @"", @"ccw" : @""}}, \
@{@"product": @"JAS SPS Wrist Large", @"sn_prefix": @"18", @"gear_ratio": @"0.00777777778", @"left" : @{@"cw" : @"", @"ccw" : @""}, @"right" : @{@"cw" : @"", @"ccw" : @""}, @"universal" : @{@"cw" : @"Extension", @"ccw" : @"Flexion"}}, \
@{@"product": @"JAS SPS Wrist Small", @"sn_prefix": @"17", @"gear_ratio": @"0.00680555556", @"left" : @{@"cw" : @"", @"ccw" : @""}, @"right" : @{@"cw" : @"", @"ccw" : @""}, @"universal" : @{@"cw" : @"Extension", @"ccw" : @"Flexion"}}, \
@{@"product": @"JAS SPS Knee Med.", @"sn_prefix": @"23", @"gear_ratio": @"0.00138888889", @"left" : @{@"cw" : @"Extension", @"ccw" : @"Flexion"}, @"right" : @{@"cw" : @"Extension", @"ccw" : @"Flexion"}, @"universal" : @{@"cw" : @"Extension", @"ccw" : @"Flexion"}}, \
@{@"product": @"JAS SPS Knee Ex. Long", @"sn_prefix": @"02", @"gear_ratio": @"0.00388888889", @"left" : @{@"cw" : @"Extension", @"ccw" : @"Flexion"}, @"right" : @{@"cw" : @"Extension", @"ccw" : @"Flexion"}, @"universal" : @{@"cw" : @"Extension", @"ccw" : @"Flexion"}}, \
@{@"product": @"JAS SPS Pro/Sup", @"sn_prefix": @"06", @"gear_ratio": @"0.015", @"left" : @{@"cw" : @"Supination", @"ccw" : @"Pronation"}, @"right" : @{@"cw" : @"Supination", @"ccw" : @"Pronation"}, @"universal" : @{@"cw" : @"", @"ccw" : @""}}, \
@{@"product": @"JAS SPS Shoulder", @"sn_prefix": @"05", @"gear_ratio": @"0.0136111111", @"left" : @{@"cw" : @"Internal Rotation", @"ccw" : @"External Rotation"}, @"right" : @{@"cw" : @"Internal Rotation", @"ccw" : @"External Rotation"}, @"universal" : @{@"cw" : @"", @"ccw" : @""}}, \
@{@"product": @"JAS SPS Ankle", @"sn_prefix": @"04", @"gear_ratio": @"0.0141203704", @"left" : @{@"cw" : @"", @"ccw" : @""}, @"right" : @{@"cw" : @"", @"ccw" : @""}, @"universal" : @{@"cw" : @"Plantarflexion", @"ccw" : @"Dorsiflexion"}}, \
]

#define BETA_FEATURE_FIRMWARE_NUMBER 4063
#define BETA_FEATURE_STRING @"BETA"

#import "DeviceInfo.h"
static DeviceInfo *sharedInstance = nil;

@implementation DeviceInfo

@synthesize myPeripheral;

+ (DeviceInfo *)sharedInstance
{
    if (sharedInstance == nil)
    {
        sharedInstance = [[DeviceInfo alloc] init];
    }
    
    return sharedInstance;
}


- (id)init {
    self = [super init];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    self.batteryVoltage = 0.0;
    self.temperature = 0;
    self.sessionStatusMA = [NSMutableArray new];
}

- (NSString *)getDisplayHoldTime {
    NSString *holdTime;
    
    int min = (int)self.holdTimeSeconds / 60;
    int sec = (int)self.holdTimeSeconds % 60;
    
    holdTime = [NSString stringWithFormat:@"%02d:%02d", min, sec];
    
    return holdTime;
}

- (NSString*)getDisplayDateString {
    return [NSString stringWithFormat:@"%ld/%ld/%ld", self.month, self.day, self.year];
}

- (NSString*)getDisplayTimeString {
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", self.hour, self.minute, self.second];
}

- (NSString*)getProduct {
    for (NSDictionary *dic in SNTable) {
        NSString *prefix = dic[@"sn_prefix"];
        if ([self.serialNumber hasPrefix:prefix]) {
            return dic[@"product"];
        }
    }
    
    return nil;
}

- (double)getGearingRatio {
    for (NSDictionary *dic in SNTable) {
        NSString *prefix = dic[@"sn_prefix"];
        if ([self.serialNumber hasPrefix:prefix]) {
            NSString *sGear = dic[@"gear_ratio"];
            
            return [sGear doubleValue];
        }
    }
    
    return 0;
}

- (NSString *)getRotationOne {
    for (NSDictionary *dic in SNTable) {
        NSString *prefix = dic[@"sn_prefix"];
        if ([self.serialNumber hasPrefix:prefix]) {
            NSString *roation1 = @"";
            
            if ([self.serialNumber hasSuffix:SERIAL_NUMBER_LEFT_SUFFIX]) {
                NSDictionary *left = dic[@"left"];
                roation1 = left[@"cw"];
            } else {
                NSDictionary *left = dic[@"right"];
                roation1 = left[@"cw"];
            }
            if ([roation1 isEqualToString:@""]) {
                NSDictionary *left = dic[@"universal"];
                roation1 = left[@"cw"];
            }
            
            if ([self isBetaFeature]) {
                return [NSString stringWithFormat:@"%@\n%@", roation1, BETA_FEATURE_STRING];
            }
            return roation1;
        }
    }
    
    return nil;
}

- (NSString *)getRotationTwo {
    for (NSDictionary *dic in SNTable) {
        NSString *prefix = dic[@"sn_prefix"];
        if ([self.serialNumber hasPrefix:prefix]) {
            NSString *roation1 = @"";
            
            if ([self.serialNumber hasSuffix:SERIAL_NUMBER_LEFT_SUFFIX]) {
                NSDictionary *left = dic[@"left"];
                roation1 = left[@"ccw"];
            } else {
                NSDictionary *left = dic[@"right"];
                roation1 = left[@"ccw"];
            }
            if ([roation1 isEqualToString:@""]) {
                NSDictionary *left = dic[@"universal"];
                roation1 = left[@"ccw"];
            }
            if ([self isBetaFeature]) {
                return [NSString stringWithFormat:@"%@\n%@", roation1, BETA_FEATURE_STRING];
            }
            return roation1;
        }
    }
    
    return nil;
}

- (BOOL) isBetaFeature {
    return self.firmwareRevision == BETA_FEATURE_FIRMWARE_NUMBER;
}

@end


